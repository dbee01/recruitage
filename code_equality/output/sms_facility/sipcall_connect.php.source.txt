<?php

/***************************************************************************
*                            sipcall_connect.php
*                            -------------------
* @begin                : Saturday, Feb 28, 2007
* @copyright            : (C) 2007 Recruitage.com
* @email                : daraburke78@gmail.com
* @description          : Log into sipdiscount, grab the cookies and then send a call order to that address ...
*
***************************************************************************/

// set user agent
$agent = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)";

// set referrer
$referrer = "http://www.sipdiscount.com/en/index.html";

// create a cookie jar to store cookies ...
$cookie_jar = "../tmp/cookiejar.txt";

// set your sip account username and password
$user="siptester22";
$pass="siptest2";

// call details ...
$to_call = "008211";
$from_call = "008210";

// LOGIN TO SIPDISCOUNT AND STORE THE SESSION COOKIE

$ch = curl_init();
if (!$ch) {
  die ("Cannot initialize a new cURL handle\n");
}

set the login page...
//curl_setopt($ch, CURLOPT_URL,"https://myaccount.sipdiscount.com/clx/index.php");

curl_setopt($ch, CURLOPT_URL,"https://myaccount.sipdiscount.com/clx/loginpanel.php?title=Sign+in&name=User+name%3A&password=Password%3A&submit=sign+in&lang=en&problems=Problems+signing+in%3F&forgot=Forgot+your+password%3F");

// store cookies
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
curl_setopt($ch, CURLOPT_USERAGENT, $agent);
curl_setopt($ch, CURLOPT_REFERER, $referrer);
curl_setopt($ch, CURLOPT_USERPWD, $user.':'.$pass);

// this returns the value of curl instead of printing it to the page
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_VERBOSE);
curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&pass=$pass&submit=sign in");

// don' verify ssl host
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);

//ob_start();      // prevent any output

$data = curl_exec ($ch);

// Data is returned on success, error code on failure
if (is_int ($data)) {
  die ("cURL error: " . curl_error ($ch) . "\n");
}

//ob_end_clean();  // stop preventing output

curl_close ($ch);
unset($ch);


// SHOULD BE LOGGED IN WITH COOKIE AT THIS STAGE

// PLACE A CALL

// referrer
$referrer = "https://myaccount.sipdiscount.com/clx/webcalls2.php";

$ch = curl_init();          

// this is where I'll send my call request
curl_setopt($ch, CURLOPT_URL,"https://myaccount.sipdiscount.com/clx/webcalls2.php");

// look like a browser
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_REFERER, $referrer);
curl_setopt($ch, CURLOPT_USERAGENT, $agent);

// print this out 0=yes 1=no
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
 
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_jar);
//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);

curl_setopt($ch, CURLOPT_POSTFIELDS, array(
					   'action'=>'initcall',
					   'panel'=>'',
					   'anrphonenr'=>$to_call,
					   'bnrphonenr'=>$from_call
					   ));

curl_exec ($ch);
curl_close ($ch);


?>
