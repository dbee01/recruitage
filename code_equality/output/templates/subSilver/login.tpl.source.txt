<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>Login {SITENAME}</title>

<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />

<script language='Javascript' type='text/javascript' src='js/common.js' ></script>

<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

</head>


<body>

<div style="display:none" name="cache_images" >

	<img src="images/etk-icons/mail_reply35.png" height="" width="" alt="Reply to Mail" />
	<img src="images/etk-icons/delete-19.png" height="" width="" alt="Delete" />

</div>

<div class='container'>
<div class='titleblock'>
<h1 class='style9' align='right'>EnglishTeachingKorea.com </h1>
<p class='style1 style13'>Recruiters helping recruiters </p>

</div>

<div>
<ul class='navbar'>
  <li><a href='#' class='style5 nav'><strong>Home</strong></a></li>
  <li><a href='#' class='style5 nav style1'><strong>Jobs</strong></a></li>
  <li><a href='#' class='style5 nav style1'><strong>Korea</strong></a></li>
  <li><a href='#' class='style5 nav style1'><strong>Visa</strong></a></li>

  <li><a href='#' class='style5 nav style1'><strong>Links</strong></a></li>
  <li><a href='#' class='style5 nav style1'><strong>Contact_Us</strong></a></li>
</ul>
</div>

<div class='content'>
<blockquote>

<!-- BEGIN RECRUITER_LOGIN -->
<form action="login.php" method="post" enctype="application/x-www-form-urlencoded">
  <p class='style5' align='center'>{L_RECRUITER_LOGIN}</p>
  <table cellpadding='2' cellspacing='2' align='center' >
  	<tr><td>Name:</td><td><input type='text' name='username' maxlength='16'  /></td></tr>
   <tr><td>Password:</td><td><input type='password' name='password' maxlength='16' /></td></tr>
   <tr><td colspan='2' align='center'>
   	<input type='submit' name='Submit' value='Enter' />
      <input type='hidden' name='login' />    
   </td></tr>
  </table>
  <br />
<!-- END RECRUITER_LOGIN -->

<!-- BEGIN SITE_LOGIN_ERROR -->

  <p class='style5' align='center'>{L_SITE_LOGIN_ERROR}</p>
  
<!-- END SITE_LOGIN_ERROR -->
     
  <p>&nbsp;</p>
</form>

</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 EnglishTeachingKorea.com </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
