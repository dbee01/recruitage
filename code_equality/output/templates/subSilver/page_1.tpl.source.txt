<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>{SITENAME} Intro</title>
 <meta http-equiv="content-type" content="text/html;charset=utf-8" >
 <link rel="stylesheet" href="css_page/smooth_blue.css" type="text/css" >

 <!--[if IE]> <link rel="stylesheet" href="css_page/smooth_blueIE.css" type="text/css" > <![endif]-->
 <!--[if IE 7 ]> <link rel="stylesheet" href="css_page/smooth_blueIE7.css" type="text/css" > <![endif]-->

<script type='text/javascript' src='js/callme_now.js' ></script>

<script type='text/javascript'>
// place all the on-page variables here....
	var valid = {CALLBACK_VALID};
    
</script>

</head>
<body>
<div id="pagewidth" >
  <div id="header" >
 
    <div class="navbar"> 
        <ul id="nav">
            <li id="nav_home"><a href="{PAGE_1_LINK}">{PAGE_1_NAME}</a></li>
            <li id="nav_jobs"><a href="{PAGE_2_LINK}">{PAGE_2_NAME}</a></li>
            <li id="nav_korea"><a href="{PAGE_3_LINK}">{PAGE_3_NAME}</a></li>
            <li id="nav_visa"><a href="{PAGE_4_LINK}">{PAGE_4_NAME}</a></li>
            <li id="nav_links"><a href="{PAGE_5_LINK}">{PAGE_5_NAME}</a></li>
            <li id="nav_contact"><a href="{CONTACT_LINK}">{PAGE_5_NAME}</a></li>
        </ul>
    </div>
    
    <div class="logo">&nbsp;</div>
    
    <div id="top_bar">
     &nbsp;
    </div>
  
  </div>

  <div id="wrapper" class="clearfix" > 

    <div id="twocols" class="clearfix"> 

      <div class="david_bar" >

        <div class="david_pic" ></div>
 
	<div class="heading_card" >

          <div id="main_text">
   
             <span class="why_wait">
       		{L_SLOGAN}
             </span>
  
             <div class="main_first_row">
	       <div class="first_text"></div>
	     </div>
	    
             <div class="main_second_row">
	       <p>{L_CONTENT_HEADER2}</p>
	     </div>

	  </div>

	</div>

      </div>
  
       <div id="maincol" >

		<div class="thumb-box" >

			<div class="thumb-cell" >

				<a href='{L_LIVE_CHAT_LINK}' >
					<span class="thumb-chat" ></span>
				</a>

				<div class="thumb-right">
				<a href='{L_LIVE_CHAT_LINK}' >
					<span class="thumb-title" >{L_LIVE_CHAT}:<br />	</span>
				</a>
					<span class="thumb-text" >{L_LIVE_CHAT_TEXT}</span>
				</div>
				
			</div>

			<div class="thumb-cell" >

				<a href='https://myaccount.voipbuster.com/images/callmenowbutton.gif' onclick='callMeNow(); return false' >
					<span class="thumb-callback" ></span>
				</a>

				<div class="thumb-right">
				<a href='{L_CALLBACK_LINK}' onclick='callMeNow(); return false' >
					<span class="thumb-title" >{L_CALLBACK}<br />	</span>
				</a>
					<span class="thumb-text" >{L_CALLBACK_TEXT}</span>
				</div>
				
			</div>

			<div class="thumb-cell" >

				<a href='{L_TEACHER_GUIDE_LINK}' >
					<span class="thumb-guide">&nbsp;</span>
				</a>

				<div class="thumb-right">
					<a href='{L_TEACHER_GUIDE_LINK}' >	
					<span class="thumb-title">{L_TEACHER_GUIDE}:<br /></span>
					</a>			
					<span class="thumb-text" >{L_TEACHER_GUIDE_TEXT}</span>	
				</div>
			</div>

			<div class="thumb-cell" >

				<a href='{L_GAMES_DATABASE_LINK}' >
				<span class="thumb-askgari" ></span>
				</a>				

				<div class="thumb-right">	
					<a href='{L_GAMES_DATABASE_LINK}' >
					<span class="thumb-title">{L_GAMES_DATABASE}:<br /></span>			
					</a>
					<span class="thumb-text" >{L_GAMES_DATABASE_TEXT}</span>	
				</div>
			</div>

			<div class="thumb-cell" >
				<a href='{L_PHOTO_GALLERY_LINK}' >
				<span class="thumb-gallery" ></span>
				</a>

				<div class="thumb-right">	
					<a href='{L_PHOTO_GALLERY_LINK}' >
					<span class="thumb-title">{L_PHOTO_GALLERY}:<br /></span>
					</a>			
					<span class="thumb-text" >{L_PHOTO_GALLERY_TEXT}</span>	
				</div>
			</div>
			<div class="thumb-cell" >
				<a href='{L_MOVIE_LINK}' style='position:relative' >
				<span class="thumb-movie" style='position:relative' ></span>
				</a>

				<div class="thumb-right">	
					<a href='{L_MOVIE_LINK}' >
					<span class="thumb-title">{L_KOREA_MOVIE}:<br /></span>
					</a>			
					<span class="thumb-text" >{L_MOVIE_TEXT}</span>	
				</div>
			</div>
		</div>
       
       </div>

       <div id="rightcol" >
	 
	<a class="apply_now" href='{L_APPLY_ONLINE_LINK}'>
	 <span style="display:block;" >&nbsp;</span>
	</a>

         <div class="right_table_top">
	  &nbsp;
         </div>
       
         <div class="right_col_main">
           <div class="testamonials_table">
	      <div class="testamonials_title">Testimonials</div>

              <div class="testamonials_box">
               
	       <div id="testamonials_inner_box">
	
                 <div class="testamonials_text">

                   <div class="testamonials_pic1">
		    &nbsp;
                   </div>

	          {L_TESTAMONIALS_1}
	         </div>

               </div>

 	         <div class="testamonials_more"  >
                   <a  href='front_form.php?mode=testamonials_page1'>more</a>
                 </div>
	
	      </div>

	      <div class="testamonials_box">

	       <div class="testamonials_text">

	          <div class="testamonials_pic2">
	            &nbsp;
	       	  </div>

	         {L_TESTAMONIALS_2}
	       </div>
		
 	       <div class="testamonials_more"  >
                 <a  href='front_form.php?mode=testamonials_page1'>more</a>
               </div>

	      </div>
           </div>
	 </div>

         <div class="right_table_bottom">
           &nbsp;
         </div>
       </div>
    </div> 

    <div id="leftcol" >
      <div class="left_title">{L_TITLE}</div>

      <div id="left_boxes">
        <div class="JobsNews"><img src="images/jobsandnews.gif" alt="jobsandnews" /></div>

        <div class="news_boxes">

          <div class="news_box_text">

	      <div class="news_box_date">{L_NEWS_DATE_1}</div>
            
              <div class="news_box_second">{L_NEWS_CONTENT_1}</div>

          </div>

          <div class="news_box_more" ><a href='front_form.php?mode=jobs_news1'>more</a></div> 

        </div>

        <div class="news_boxes">

          <div class="news_box_text">

              <div class="news_box_date">{L_NEWS_DATE_2}</div>

              <div class="news_box_second">{L_NEWS_CONTENT_2}</div> 

          </div> 

          <div class="news_box_more" >
              <a href='front_form.php?mode=jobs_news2'>more</a>
         </div> 	     

        </div>

        <div class="news_boxes">

          <div class="news_box_text">

              <div class="news_box_date">{L_NEWS_DATE_3}</div>

              <div class="news_box_second">{L_NEWS_CONTENT_3}</div>  

          </div> 

          <div class="news_box_more" ><a href='front_form.php?mode=jobs_news3'>more</a></div> 

        </div>

	<div class="JobsNews_bottom"><img src="images/jobsandnews-bottom.gif" alt="bottom news image" /></div>
      </div>

      <div class="newsletter">
        <div class="newsletter-top" >&nbsp;</div>
          <div class="newsletter-body" >
            <span class="newsletter-title" >{L_NEWSLETTER}</span><br /> 
	    <table>
	      <tr>
  		<td><label>{L_NAME}:</label></td>
                <td><input type='text' size='17' name='cp_name' /></td>
              </tr>
	      <tr>
		<td><label>{L_EMAIL}:</label>
	        <td><input type='text' size='17' name='cp_email_address' /></td>
	      </tr>
	    </table>
          </div>
	<div class="newsletter-bottom">&nbsp;</div>
      </div>
     
    </div>

  </div>

  <div id="footer" >
    <div class="bottom_bar">
      <div class="contact">
       <span><a class="contact_flag" href='login.php'><img src="{CONTACT_FLAG}" alt="country_flag" /></a></span>    
	 <div class="contact_details" >
	   <a href='page_6.php' >{L_EMAIL}</a><br />
           <a href='https://myaccount.voipbuster.com/images/callmenowbutton.gif' onclick='callMeNow(); return false' >{L_PHONE}</a>
	 </div>
      </div>

      <div class='copyright' >
        <p class='style14'> {SITENAME} &copy; 2005 </p>
      </div>

    </div>
  </div>

</div>

 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-4";
  urchinTracker();
 </script>

</body>
</html>
