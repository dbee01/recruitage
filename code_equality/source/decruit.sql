-- MySQL dump 10.10
--
-- Host: localhost    Database: teachers
-- ------------------------------------------------------
-- Server version	5.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_access`
--

DROP TABLE IF EXISTS `auth_access`;
CREATE TABLE `auth_access` (
  `group_id` mediumint(8) NOT NULL default '0',
  `forum_id` smallint(5) unsigned NOT NULL default '0',
  `auth_view` tinyint(1) NOT NULL default '0',
  `auth_read` tinyint(1) NOT NULL default '0',
  `auth_post` tinyint(1) NOT NULL default '0',
  `auth_reply` tinyint(1) NOT NULL default '0',
  `auth_edit` tinyint(1) NOT NULL default '0',
  `auth_delete` tinyint(1) NOT NULL default '0',
  `auth_sticky` tinyint(1) NOT NULL default '0',
  `auth_announce` tinyint(1) NOT NULL default '0',
  `auth_vote` tinyint(1) NOT NULL default '0',
  `auth_pollcreate` tinyint(1) NOT NULL default '0',
  `auth_attachments` tinyint(1) NOT NULL default '0',
  `auth_mod` tinyint(1) NOT NULL default '0',
  KEY `group_id` (`group_id`),
  KEY `forum_id` (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `config`
--
  
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
LOCK TABLES `config` WRITE;
INSERT INTO `config` VALUES ('config_id','1'),('board_disable',''),('sitename',''),('site_desc',''),('cookie_name',''),('cookie_path','/'),('cookie_domain',''),('cookie_secure',''),('session_length','10000'),('allow_html',''),('allow_html_tags','b,i,u,pre'),('allow_sig','1'),('allow_namechange',''),('allow_theme_create',''),('allow_avatar_remote',''),('allow_avatar_upload',''),('enable_confirm',''),('allow_autologin','1'),('max_autologin_time',''),('override_user_style',''),('posts_per_page','15'),('max_poll_options','10'),('max_inbox_privmsgs','50'),('max_savebox_privmsgs','50'),('board_email_sig',''),('board_email',''),('smtp_delivery',''),('smtp_host',''),('smtp_username',''),('smtp_password',''),('sendmail_fix',''),('require_activation',''),('flood_interval','15'),('board_email_form',''),('avatar_filesize','6144'),('avatar_max_width','80'),('avatar_max_height','80'),('avatar_path','images/avatars'),('avatar_gallery_path','images/avatars/gallery'),('smilies_path','images/smiles'),('default_style','1'),('default_dateformat','D M d, Y g:i a'),('board_timezone',''),('prune_enable','1'),('privmsg_disable',''),('gzip_compress',''),('coppa_fax',''),('coppa_mail',''),('record_online_users',''),('record_online_date',''),('server_name',''),('server_port','80'),('script_path','/'),('version','.0.18'),('board_startdate','1143199211'),('default_lang','standard'),('Google Bookmark','http://www.Google.com');
UNLOCK TABLES;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

--
-- Table structure for table `employee_tracker`
--

DROP TABLE IF EXISTS `employee_tracker`;
CREATE TABLE `employee_tracker` (
  `user` int(11) NOT NULL default '0',
  `time` int(15) NOT NULL default '0',
  `timezone` text NOT NULL,
  `action` text NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `free_campaigns`
--

DROP TABLE IF EXISTS `free_campaigns`;
CREATE TABLE `free_campaigns` (
  `id` smallint(6) NOT NULL auto_increment,
  `campaign_name` varchar(50) NOT NULL default '',
  `frequency` tinyint(4) NOT NULL default '0',
  `run_tally` int(11) NOT NULL,
  `script` varchar(110) NOT NULL default '',
  `notes` text,
  `ad_amount` varchar(100) default NULL,
  `ad_title` varchar(100) default NULL,
  `ad_copy` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `gamesdatabase`
--

DROP TABLE IF EXISTS `gamesdatabase`;
CREATE TABLE `gamesdatabase` (
  `recordID` int(11) NOT NULL default '0',
  `Name` varchar(50) default NULL,
  `Age` varchar(50) default NULL,
  `Size` varchar(50) default NULL,
  `Keyword` varchar(50) default NULL,
  `Example` longtext,
  `Tips` longtext,
  `Description` longtext,
  `Help` varchar(50) default NULL,
  `Rate` int(11) default NULL,
  `Totalrate` int(11) default NULL,
  `Votes` int(11) default NULL,
  `Comment` longtext,
  PRIMARY KEY  (`recordID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `job_calendar`
--

DROP TABLE IF EXISTS `job_calendar`;
CREATE TABLE `job_calendar` (
  `cal_id` int(11) NOT NULL auto_increment,
  `school` varchar(40) NOT NULL default '',
  `contact` varchar(40) NOT NULL default '',
  `email` varchar(60) NOT NULL default '',
  `organization` varchar(80) NOT NULL default '',
  `leaving_name_1` varchar(50) NOT NULL default '',
  `leaving_date_1` date NOT NULL default '0000-00-00',
  `leaving_name_2` varchar(50) NOT NULL default '',
  `leaving_date_2` date NOT NULL default '0000-00-00',
  `leaving_name_3` varchar(50) NOT NULL default '',
  `leaving_date_3` date NOT NULL default '0000-00-00',
  `leaving_name_4` varchar(50) NOT NULL default '',
  `leaving_date_4` date NOT NULL default '0000-00-00',
  `leaving_name_5` varchar(50) NOT NULL default '',
  `leaving_date_5` date NOT NULL default '0000-00-00',
  `leaving_name_6` varchar(50) NOT NULL default '',
  `leaving_date_6` date NOT NULL default '0000-00-00',
  `leaving_name_7` varchar(50) NOT NULL default '',
  `leaving_date_7` date NOT NULL default '0000-00-00',
  `leaving_name_8` varchar(50) NOT NULL default '',
  `leaving_date_8` date NOT NULL default '0000-00-00',
  `leaving_name_9` varchar(50) NOT NULL default '',
  `leaving_date_9` date NOT NULL default '0000-00-00',
  `leaving_name_10` varchar(50) NOT NULL default '',
  `leaving_date_10` date NOT NULL default '0000-00-00',
  `leaving_name_11` varchar(50) NOT NULL default '',
  `leaving_date_11` date NOT NULL default '0000-00-00',
  `leaving_name_12` varchar(50) NOT NULL default '',
  `leaving_date_12` date NOT NULL default '0000-00-00',
  `leaving_name_13` varchar(50) NOT NULL default '',
  `leaving_date_13` date NOT NULL default '0000-00-00',
  `leaving_name_14` varchar(50) NOT NULL default '',
  `leaving_date_14` date NOT NULL default '0000-00-00',
  `leaving_name_15` varchar(50) NOT NULL default '',
  `leaving_date_15` date NOT NULL default '0000-00-00',
  `leaving_name_16` varchar(50) NOT NULL default '',
  `leaving_date_16` date NOT NULL default '0000-00-00',
  `leaving_name_17` varchar(50) NOT NULL default '',
  `leaving_date_17` date NOT NULL default '0000-00-00',
  `leaving_name_18` varchar(50) NOT NULL default '',
  `leaving_date_18` date NOT NULL default '0000-00-00',
  `leaving_name_19` varchar(50) NOT NULL default '',
  `leaving_date_19` date NOT NULL default '0000-00-00',
  `leaving_name_20` varchar(50) NOT NULL default '',
  `leaving_date_20` date NOT NULL default '0000-00-00',
  `leaving_name_21` varchar(50) NOT NULL default '',
  `leaving_date_21` date NOT NULL default '0000-00-00',
  `leaving_name_22` varchar(50) NOT NULL default '',
  `leaving_date_22` date NOT NULL default '0000-00-00',
  `leaving_name_23` varchar(50) NOT NULL default '',
  `leaving_date_23` date NOT NULL default '0000-00-00',
  `leaving_name_24` varchar(50) NOT NULL default '',
  `leaving_date_24` date NOT NULL default '0000-00-00',
  `leaving_name_25` varchar(50) NOT NULL default '',
  `leaving_date_25` date NOT NULL default '0000-00-00',
  `leaving_name_26` varchar(50) NOT NULL default '',
  `leaving_date_26` date NOT NULL default '0000-00-00',
  `leaving_name_27` varchar(50) NOT NULL default '',
  `leaving_date_27` date NOT NULL default '0000-00-00',
  `leaving_name_28` varchar(50) NOT NULL default '',
  `leaving_date_28` date NOT NULL default '0000-00-00',
  `leaving_name_29` varchar(50) NOT NULL default '',
  `leaving_date_29` date NOT NULL default '0000-00-00',
  `leaving_name_30` varchar(50) NOT NULL default '',
  `leaving_date_30` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`cal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `knowledge_base`
--

DROP TABLE IF EXISTS `knowledge_base`;
CREATE TABLE `knowledge_base` (
  `knowledge_rec_id` smallint(6) NOT NULL auto_increment,
  `knowledge_section` varchar(50) NOT NULL,
  `knowledge_title` varchar(150) NOT NULL default '',
  `knowledge_text` text,
  PRIMARY KEY  (`knowledge_rec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `linkstats`
--

DROP TABLE IF EXISTS `linkstats`;
CREATE TABLE `linkstats` (
  `linkstats_name` varchar(100) default NULL,
  `linkstats_pos` int(11) default NULL,
  `linkstats_keywords` varchar(50) NOT NULL default '',
  UNIQUE KEY `linkstats_name` (`linkstats_name`,`linkstats_keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
CREATE TABLE `mail` (
  `mail_id` int(11) NOT NULL auto_increment,
  `date` date NOT NULL default '0000-00-00',
  `name` varchar(50) NOT NULL default '',
  `email` varchar(70) NOT NULL default '',
  `email_from` varchar(50) default NULL,
  `subject` varchar(70) NOT NULL default '',
  `text` text NOT NULL,
  `headers` varchar(100) NOT NULL default '',
  `sent` tinyint(1) NOT NULL,
  PRIMARY KEY  (`mail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `mail_memory`
--

DROP TABLE IF EXISTS `mail_memory`;
CREATE TABLE `mail_memory` (
  `email` varchar(50) NOT NULL default '',
  `message` text NOT NULL,
  PRIMARY KEY  (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `paid_campaigns`
--

DROP TABLE IF EXISTS `paid_campaigns`;
CREATE TABLE `paid_campaigns` (
  `id` smallint(6) NOT NULL auto_increment,
  `campaign_name` varchar(50) NOT NULL default '',
  `cost` smallint(6) NOT NULL default '0',
  `currency` tinyint(4) NOT NULL default '0',
  `ad_page` varchar(100) default NULL,
  `script` varchar(110) default NULL,
  `notes` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `recruiter_jobs`
--

DROP TABLE IF EXISTS `recruiter_jobs`;
CREATE TABLE `recruiter_jobs` (
  `contact` varchar(30) NOT NULL default '',
  `school` varchar(30) NOT NULL default '',
  `arrival` date default NULL,
  `email` varchar(30) NOT NULL default '',
  `recruiter_notes` text NOT NULL,
  `school_description` text NOT NULL,
  `city` char(3) NOT NULL default '',
  `website_address` varchar(30) NOT NULL default '',
  `accomodation` tinyint(3) NOT NULL default '0',
  `foreign_teacher` text NOT NULL,
  `kindergarten` varchar(5) NOT NULL default '',
  `elementary` varchar(5) NOT NULL default '',
  `middle_school` varchar(5) NOT NULL default '',
  `high_school` varchar(5) NOT NULL default '',
  `adults` varchar(5) NOT NULL default '',
  `contract` varchar(40) NOT NULL default '',
  `picupload` varchar(40) NOT NULL default '',
  `ticket_money` varchar(4) NOT NULL default '',
  `salary` char(3) NOT NULL default '',
  `number_teachers` varchar(4) NOT NULL default '',
  `teacher_description` text NOT NULL,
  `gender` char(3) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `recruiter_jobs_id` varchar(36) NOT NULL default '0',
  `user_reg_id` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `registry`
--

DROP TABLE IF EXISTS `registry`;
CREATE TABLE `registry` (
  `reply_text0_title` varchar(50) NOT NULL default '',
  `reply_text0` text NOT NULL,
  `reply_text1_title` varchar(50) NOT NULL default '',
  `reply_text1` text NOT NULL,
  `reply_text2_title` varchar(50) NOT NULL default '',
  `reply_text2` text NOT NULL,
  `reply_text3_title` varchar(50) NOT NULL default '',
  `reply_text3` text NOT NULL,
  `reply_text4_title` varchar(50) NOT NULL default '',
  `reply_text4` text NOT NULL,
  `reply_text5_title` varchar(50) NOT NULL default '',
  `reply_text5` text NOT NULL,
  `reply_text6_title` text NOT NULL,
  `reply_text6` text NOT NULL,
  `reply_text7_title` text NOT NULL,
  `reply_text7` text NOT NULL,
  `reply_text8_title` text NOT NULL,
  `reply_text8` text NOT NULL,
  `reply_text9_title` text NOT NULL,
  `reply_text9` text NOT NULL,
  `reply_text10_title` text NOT NULL,
  `reply_text10` text NOT NULL,
  `reply_text11_title` text NOT NULL,
  `reply_text11` text NOT NULL,
  `reply_link0` text NOT NULL,
  `reply_link_text0` text NOT NULL,
  `reply_link1` text NOT NULL,
  `reply_link_text1` text NOT NULL,
  `reply_link2` text NOT NULL,
  `reply_link_text2` text NOT NULL,
  `reply_link3` text NOT NULL,
  `reply_link_text3` text NOT NULL,
  `reply_link4` text NOT NULL,
  `reply_link_text4` text NOT NULL,
  `reply_link5` text NOT NULL,
  `reply_link_text5` text NOT NULL,
  `autocontrol_thanks_text` text NOT NULL,
  `autocontrol_thanks_close` text NOT NULL,
  `autocontrol_thanks_title` text NOT NULL,
  `resume_upload_text` text NOT NULL,
  `resume_error_text` text NOT NULL,
  `recruiter_form_text` text NOT NULL,
  `recruiter_thanks_text` text NOT NULL,
  `recruiter_error_text` text NOT NULL,
  `school_thanks_text` text NOT NULL,
  `school_error_text` text NOT NULL,
  `contact_us_heading` varchar(60) NOT NULL default '',
  `contact_us_text` text,
  `reply_text` text NOT NULL,
  `teacher_recruiter_posresponse_email` text NOT NULL,
  `teacher_school_posresponse_email` text NOT NULL,
  `etk_teacher_posresponse_email` text NOT NULL,
  `etk_teacher_school_email` text NOT NULL,
  `etk_teacher_recruiter_email` text NOT NULL,
  `candidate_fail_email` text NOT NULL,
  `employer_reminder` text,
  `fixed_date1` date default NULL,
  `fixed_title1` varchar(60) NOT NULL default '',
  `fixed_text1` text NOT NULL,
  `fixed_date2` date default NULL,
  `fixed_title2` varchar(60) NOT NULL default '',
  `fixed_text2` text NOT NULL,
  `fixed_date3` date default NULL,
  `fixed_title3` varchar(60) NOT NULL default '',
  `fixed_text3` text NOT NULL,
  `fixed_date4` date default NULL,
  `fixed_title4` varchar(60) NOT NULL default '',
  `fixed_text4` text NOT NULL,
  `fixed_date5` date default NULL,
  `fixed_title5` varchar(60) NOT NULL default '',
  `fixed_text5` text NOT NULL,
  `fixed_date6` date default NULL,
  `fixed_title6` varchar(60) NOT NULL default '',
  `fixed_text6` text NOT NULL,
  `fixed_date7` date default NULL,
  `fixed_title7` varchar(60) NOT NULL default '',
  `fixed_text7` text NOT NULL,
  `fixed_date8` date default NULL,
  `fixed_title8` varchar(60) NOT NULL default '',
  `fixed_text8` text NOT NULL,
  `fixed_date9` date default NULL,
  `fixed_title9` varchar(60) NOT NULL default '',
  `fixed_text9` text NOT NULL,
  `fixed_date10` date default NULL,
  `fixed_title10` varchar(60) NOT NULL default '',
  `fixed_text10` text NOT NULL,
  `fixed_date11` date default NULL,
  `fixed_title11` varchar(60) NOT NULL default '',
  `fixed_text11` text NOT NULL,
  `fixed_date12` date default NULL,
  `fixed_title12` varchar(60) NOT NULL default '',
  `fixed_text12` text NOT NULL,
  `fixed_date13` date default NULL,
  `fixed_title13` varchar(60) NOT NULL default '',
  `fixed_text13` text NOT NULL,
  `fixed_date14` date default NULL,
  `fixed_title14` varchar(60) NOT NULL default '',
  `fixed_text14` text NOT NULL,
  `fixed_date15` date default NULL,
  `fixed_title15` varchar(60) NOT NULL default '',
  `fixed_text15` text NOT NULL,
  `time_title1` varchar(60) NOT NULL default '',
  `time_text1` text NOT NULL,
  `time_title2` varchar(60) NOT NULL default '',
  `time_text2` text NOT NULL,
  `time_title3` varchar(60) NOT NULL default '',
  `time_text3` text NOT NULL,
  `time_title4` varchar(60) NOT NULL default '',
  `time_text4` text NOT NULL,
  `time_title5` varchar(60) NOT NULL default '',
  `time_text5` text NOT NULL,
  `time_title6` varchar(60) NOT NULL default '',
  `time_text6` text NOT NULL,
  `time_title7` varchar(60) NOT NULL default '',
  `time_text7` text NOT NULL,
  `time_title8` varchar(60) NOT NULL default '',
  `time_text8` text NOT NULL,
  `time_title9` varchar(60) NOT NULL default '',
  `time_text9` text NOT NULL,
  `birthday_subject` varchar(60) default NULL,
  `birthday_message` text,
  `can0_upload_resource` tinyint(1) default NULL,
  `can0_upload_resource_sec` tinyint(1) default NULL,
  `can0_upload_resource_check` varchar(1) default NULL,
  `can0_upload_name` tinyint(1) default NULL,
  `can0_upload_name_sec` char(1) default NULL,
  `can0_upload_name_check` tinyint(1) default NULL,
  `can0_upload_birthday` tinyint(1) default NULL,
  `can0_upload_birthday_sec` char(1) default NULL,
  `can0_upload_birthday_check` tinyint(1) default NULL,
  `can0_upload_nationality` tinyint(1) default NULL,
  `can0_upload_nationality_check` tinyint(1) default NULL,
  `can0_upload_nationality_sec` char(1) default NULL,
  `can0_upload_email` tinyint(1) default NULL,
  `can0_upload_email_sec` char(1) default NULL,
  `can0_upload_email_check` tinyint(1) default NULL,
  `can0_upload_telephone` tinyint(1) default NULL,
  `can0_upload_telephone_sec` char(1) default NULL,
  `can0_upload_telephone_check` tinyint(1) default NULL,
  `can0_upload_ringtime` tinyint(1) default NULL,
  `can0_upload_ringtime_sec` char(1) default NULL,
  `can0_upload_ringtime_check` tinyint(1) default NULL,
  `can0_upload_location` tinyint(1) default NULL,
  `can0_upload_location_sec` char(1) default NULL,
  `can0_upload_location_check` tinyint(1) default NULL,
  `can0_upload_arrival` tinyint(1) default NULL,
  `can0_upload_arrival_sec` char(1) default NULL,
  `can0_upload_arrival_check` tinyint(1) default NULL,
  `can0_upload_gender` tinyint(1) default NULL,
  `can0_upload_gender_sec` char(1) default NULL,
  `can0_upload_gender_check` tinyint(1) default NULL,
  `can0_upload_inkorea` tinyint(1) default NULL,
  `can0_upload_inkorea_sec` char(1) default NULL,
  `can0_upload_inkorea_check` tinyint(1) default NULL,
  `can0_upload_referer` tinyint(1) default NULL,
  `can0_upload_referer_sec` char(1) default NULL,
  `can0_upload_referer_check` tinyint(1) default NULL,
  `can0_upload_education` tinyint(1) default NULL,
  `can0_upload_education_sec` char(1) default NULL,
  `can0_upload_education_check` tinyint(1) default NULL,
  `can0_upload_experience` tinyint(1) default NULL,
  `can0_upload_experience_sec` char(1) default NULL,
  `can0_upload_experience_check` tinyint(1) default NULL,
  `can0_upload_introduction_sec` char(1) default NULL,
  `can0_upload_introduction_check` tinyint(1) default NULL,
  `can0_upload_introduction` tinyint(1) default NULL,
  `can0_upload_picture_sec` char(1) default NULL,
  `can0_upload_picture_check` tinyint(1) default NULL,
  `can0_upload_picture` tinyint(1) default NULL,
  `can0_upload_youtube` tinyint(4) default NULL,
  `can0_upload_youtube_check` tinyint(4) default NULL,
  `can0_upload_youtube_sec` tinyint(4) default NULL,
  `job0_upload_contact` tinyint(1) default NULL,
  `job0_upload_contact_check` tinyint(1) default NULL,
  `job0_upload_contact_sec` tinyint(1) default NULL,
  `job0_upload_school` tinyint(1) default NULL,
  `job0_upload_school_sec` tinyint(1) default NULL,
  `job0_upload_school_check` tinyint(1) default NULL,
  `job0_upload_arrival` tinyint(1) default NULL,
  `job0_upload_arrival_sec` tinyint(1) default NULL,
  `job0_upload_arrival_check` tinyint(1) default NULL,
  `job0_upload_email` tinyint(1) default NULL,
  `job0_upload_email_check` tinyint(1) default NULL,
  `job0_upload_email_sec` tinyint(1) default NULL,
  `job0_upload_school_description` tinyint(1) default NULL,
  `job0_upload_school_description_check` tinyint(1) default NULL,
  `job0_upload_school_description_sec` tinyint(1) default NULL,
  `job0_upload_city` tinyint(1) default NULL,
  `job0_upload_city_check` tinyint(1) default NULL,
  `job0_upload_city_sec` tinyint(1) default NULL,
  `job0_upload_website_address` tinyint(1) default NULL,
  `job0_upload_website_address_sec` tinyint(1) default NULL,
  `job0_upload_website_address_check` tinyint(1) default NULL,
  `job0_upload_accomodation` tinyint(1) default NULL,
  `job0_upload_accomodation_check` tinyint(1) default NULL,
  `job0_upload_accomodation_sec` tinyint(1) default NULL,
  `job0_upload_foreign_teacher` tinyint(1) default NULL,
  `job0_upload_foreign_teacher_check` tinyint(1) default NULL,
  `job0_upload_foreign_teacher_sec` tinyint(1) default NULL,
  `job0_upload_contract` tinyint(1) default NULL,
  `job0_upload_contract_check` tinyint(1) default NULL,
  `job0_upload_contract_sec` tinyint(1) default NULL,
  `job0_upload_picupload` tinyint(1) default NULL,
  `job0_upload_picupload_check` tinyint(1) default NULL,
  `job0_upload_picupload_sec` tinyint(1) default NULL,
  `job0_upload_ticket_money` tinyint(1) default NULL,
  `job0_upload_ticket_money_check` tinyint(1) default NULL,
  `job0_upload_ticket_money_sec` tinyint(1) default NULL,
  `job0_upload_salary` tinyint(1) default NULL,
  `job0_upload_salary_check` tinyint(1) default NULL,
  `job0_upload_salary_sec` tinyint(1) default NULL,
  `job0_upload_students` tinyint(1) default NULL,
  `job0_upload_students_check` tinyint(1) default NULL,
  `job0_upload_students_sec` tinyint(1) default NULL,
  `job0_upload_number_teachers` tinyint(1) default NULL,
  `job0_upload_number_teachers_check` tinyint(1) default NULL,
  `job0_upload_number_teachers_sec` tinyint(1) default NULL,
  `job0_upload_teacher_description` tinyint(1) default NULL,
  `job0_upload_teacher_description_check` tinyint(1) default NULL,
  `job0_upload_teacher_description_sec` tinyint(1) default NULL,
  `job0_upload_gender` tinyint(1) default NULL,
  `job0_upload_gender_check` tinyint(1) default NULL,
  `job0_upload_gender_sec` tinyint(1) default NULL,
  `job0_upload_phone` tinyint(1) default NULL,
  `job0_upload_phone_check` tinyint(1) default NULL,
  `job0_upload_phone_sec` tinyint(1) default NULL,
  `job0_upload_postal_address` tinyint(1) default NULL,
  `job0_upload_postal_address_check` tinyint(1) default NULL,
  `job0_upload_postal_address_sec` tinyint(1) default NULL,
  `job1_upload_contact` tinyint(1) default NULL,
  `job1_upload_contact_check` tinyint(1) default NULL,
  `job1_upload_contact_sec` tinyint(1) default NULL,
  `job1_upload_school` tinyint(1) default NULL,
  `job1_upload_school_check` tinyint(1) default NULL,
  `job1_upload_school_sec` tinyint(1) default NULL,
  `job1_upload_arrival` tinyint(1) default NULL,
  `job1_upload_arrival_check` tinyint(1) default NULL,
  `job1_upload_arrival_sec` tinyint(1) default NULL,
  `job1_upload_email` tinyint(1) default NULL,
  `job1_upload_email_check` tinyint(1) default NULL,
  `job1_upload_email_sec` tinyint(1) default NULL,
  `job1_upload_school_description` tinyint(1) default NULL,
  `job1_upload_school_description_check` tinyint(1) default NULL,
  `job1_upload_school_description_sec` tinyint(1) default NULL,
  `job1_upload_city` tinyint(1) default NULL,
  `job1_upload_city_check` tinyint(1) default NULL,
  `job1_upload_city_sec` tinyint(1) default NULL,
  `job1_upload_website_address` tinyint(1) default NULL,
  `job1_upload_website_address_check` tinyint(1) default NULL,
  `job1_upload_website_address_sec` tinyint(1) default NULL,
  `job1_upload_accomodation` tinyint(1) default NULL,
  `job1_upload_accomodation_check` tinyint(1) default NULL,
  `job1_upload_accomodation_sec` tinyint(1) default NULL,
  `job1_upload_foreign_teacher` tinyint(1) default NULL,
  `job1_upload_foreign_teacher_check` tinyint(1) default NULL,
  `job1_upload_foreign_teacher_sec` tinyint(1) default NULL,
  `job1_upload_contract` tinyint(1) default NULL,
  `job1_upload_contract_check` tinyint(1) default NULL,
  `job1_upload_contract_sec` tinyint(1) default NULL,
  `job1_upload_picupload` tinyint(1) default NULL,
  `job1_upload_picupload_check` tinyint(1) default NULL,
  `job1_upload_picupload_sec` tinyint(1) default NULL,
  `job1_upload_ticket_money` tinyint(1) default NULL,
  `job1_upload_ticket_money_check` tinyint(1) default NULL,
  `job1_upload_ticket_money_sec` tinyint(1) default NULL,
  `job1_upload_salary` tinyint(1) default NULL,
  `job1_upload_salary_check` tinyint(1) default NULL,
  `job1_upload_salary_sec` tinyint(1) default NULL,
  `job1_upload_number_teachers` tinyint(1) default NULL,
  `job1_upload_number_teachers_check` tinyint(1) default NULL,
  `job1_upload_number_teachers_sec` tinyint(1) default NULL,
  `job1_upload_teacher_description` tinyint(1) default NULL,
  `job1_upload_teacher_description_check` tinyint(1) default NULL,
  `job1_upload_teacher_description_sec` tinyint(1) default NULL,
  `job1_upload_gender` tinyint(1) default NULL,
  `job1_upload_gender_check` tinyint(1) default NULL,
  `job1_upload_gender_sec` tinyint(1) default NULL,
  `job1_upload_students` tinyint(1) default NULL,
  `job1_upload_students_check` tinyint(1) default NULL,
  `job1_upload_students_sec` tinyint(1) default NULL,
  `update_leads_hash` varchar(32) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `reminder`
--

DROP TABLE IF EXISTS `reminder`;
CREATE TABLE `reminder` (
  `remno` int(10) unsigned NOT NULL auto_increment,
  `text` varchar(200) NOT NULL default '',
  `rem_time` int(11) NOT NULL default '0',
  `sms` varchar(25) default NULL,
  `sms_sent` tinyint(1) NOT NULL,
  `email` varchar(50) default NULL,
  `email_sent` tinyint(1) NOT NULL,
  `third_party` tinyint(1) NOT NULL,
  `third_party_sent` tinyint(1) NOT NULL,
  `call_from` text,
  `call_to` text,
  `sent` tinyint(1) NOT NULL default '0',
  `user_reg_date` int(11) default NULL,
  PRIMARY KEY  (`remno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `school_jobs`
--

DROP TABLE IF EXISTS `school_jobs`;
CREATE TABLE `school_jobs` (
  `contact` varchar(19) NOT NULL default '',
  `school` varchar(29) NOT NULL default '',
  `arrival` date default NULL,
  `recruiter_notes` text NOT NULL,
  `email` varchar(40) NOT NULL default '',
  `phone` varchar(40) NOT NULL default '',
  `foreign_teacher` varchar(200) NOT NULL default '',
  `school_description` text NOT NULL,
  `postal_address` varchar(200) NOT NULL default '',
  `city` smallint(6) NOT NULL default '0',
  `website_address` varchar(40) NOT NULL default '',
  `ticket_money` tinyint(1) NOT NULL default '0',
  `accomodation` tinyint(4) NOT NULL default '0',
  `salary` smallint(6) NOT NULL default '0',
  `kindergarten` tinyint(1) NOT NULL default '0',
  `elementary` tinyint(1) NOT NULL default '0',
  `middle_school` tinyint(1) NOT NULL default '0',
  `high_school` tinyint(1) NOT NULL default '0',
  `adults` tinyint(1) NOT NULL default '0',
  `number_teachers` tinyint(4) NOT NULL default '0',
  `teacher_description` text NOT NULL,
  `gender` varchar(4) NOT NULL default '',
  `contract` varchar(60) NOT NULL default '',
  `picupload` varchar(60) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `school_jobs_id` varchar(36) NOT NULL default '',
  `calendar_date` varchar(10) NOT NULL default '',
  `user_reg_id` int(11) default NULL,
  PRIMARY KEY  (`school_jobs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sent_match`
--

DROP TABLE IF EXISTS `sent_match`;
CREATE TABLE `sent_match` (
  `teacher_id` varchar(36) NOT NULL default '',
  `job_id` varchar(36) NOT NULL default '',
  `teacher_response` tinyint(1) default NULL,
  `job_response` tinyint(1) default NULL,
  `teacher_notes` text NOT NULL,
  `job_notes` text,
  `deleted` tinyint(1) NOT NULL default '0',
  `success` tinyint(4) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `session_id` char(32) NOT NULL default '',
  `session_user_id` mediumint(8) NOT NULL default '0',
  `session_start` int(11) NOT NULL default '0',
  `session_time` int(11) NOT NULL default '0',
  `session_ip` char(8) NOT NULL default '0',
  `session_page` int(11) NOT NULL default '0',
  `session_logged_in` tinyint(1) NOT NULL default '0',
  `session_admin` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`session_id`),
  KEY `session_user_id` (`session_user_id`),
  KEY `session_id_ip_user_id` (`session_id`,`session_ip`,`session_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `sessions_keys`
--

DROP TABLE IF EXISTS `sessions_keys`;
CREATE TABLE `sessions_keys` (
  `key_id` varchar(32) NOT NULL default '0',
  `user_id` mediumint(8) NOT NULL default '0',
  `last_ip` varchar(8) NOT NULL default '0',
  `last_login` int(11) NOT NULL default '0',
  PRIMARY KEY  (`key_id`,`user_id`),
  KEY `last_login` (`last_login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `site_text_zone`
--

DROP TABLE IF EXISTS `site_text_zone`;
CREATE TABLE `site_text_zone` (
  `zone_name` smallint(6) default NULL,
  `heading_text` text,
  `page1_title` text,
  `page1_text` text,
  `page2_title` text,
  `page2_text` text,
  `page3_title` text,
  `page3_text` text,
  `page4_title` text,
  `page4_text` text,
  `page5_title` text,
  `page5_text` text,
  `news1_title` text,
  `news1_text` text,
  `news2_title` text,
  `news2_text` text,
  `news3_title` text,
  `news3_text` text,
  `testamonials1_text` text,
  `testamonials2_text` text,
  `email` text,
  `phone` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `site_tracker`
--

DROP TABLE IF EXISTS `site_tracker`;
CREATE TABLE `site_tracker` (
  `visitor` int(11) NOT NULL auto_increment,
  `ip` int(10) unsigned default NULL,
  `sites` mediumtext,
  `time` datetime default NULL,
  PRIMARY KEY  (`visitor`),
  UNIQUE KEY `ip` (`ip`,`sites`(450))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `statslog`
--

DROP TABLE IF EXISTS `statslog`;
CREATE TABLE `statslog` (
  `stats_id` int(11) NOT NULL auto_increment,
  `ip` varchar(25) default NULL,
  `accesstime` datetime default NULL,
  `method` varchar(15) default NULL,
  `path` varchar(150) default NULL,
  `protocol` varchar(50) default NULL,
  `status` varchar(40) default NULL,
  `bytes` varchar(19) default NULL,
  `referer` varchar(60) default NULL,
  `agent` varchar(30) default NULL,
  PRIMARY KEY  (`stats_id`),
  UNIQUE KEY `ip` (`ip`,`accesstime`,`method`,`path`,`protocol`,`status`,`bytes`,`referer`,`agent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `name` varchar(50) NOT NULL default '',
  `birthyear` smallint(6) NOT NULL default '0',
  `nationality` varchar(20) NOT NULL default '',
  `email` varchar(50) NOT NULL default '',
  `aftercare` tinyint(1) default NULL,
  `country_code` varchar(8) NOT NULL default '',
  `phone` varchar(22) NOT NULL default '',
  `calltime` varchar(40) NOT NULL default '',
  `from_time` varchar(4) NOT NULL default '',
  `until_time` varchar(4) NOT NULL default '',
  `timezone` char(3) NOT NULL default '',
  `location` smallint(6) NOT NULL,
  `status` tinyint(4) NOT NULL default '0',
  `arrival` date default NULL,
  `gender` varchar(8) NOT NULL default '',
  `in_korea` varchar(10) NOT NULL default '',
  `inkorea` mediumtext NOT NULL,
  `university_education` mediumtext NOT NULL,
  `experience` mediumtext NOT NULL,
  `introduction` text NOT NULL,
  `advertise` varchar(50) NOT NULL default '',
  `ip` varchar(17) default NULL,
  `recruiter_notes` text NOT NULL,
  `pic_upload` varchar(60) NOT NULL default '',
  `youtube` text,
  `submit_time` timestamp NULL default NULL,
  `start_date` date NOT NULL,
  `teacher_id` varchar(36) NOT NULL default '',
  `recruiter` varchar(20) default NULL,
  `date_of_birth` date default NULL,
  PRIMARY KEY  (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `tel_contact`
--

DROP TABLE IF EXISTS `tel_contact`;
CREATE TABLE `tel_contact` (
  `tel_id` mediumint(9) NOT NULL auto_increment,
  `telephone` varchar(22) default NULL,
  `organization` varchar(50) NOT NULL default '',
  `title` varchar(6) NOT NULL,
  `contact` varchar(30) NOT NULL default '',
  `location` varchar(50) NOT NULL default '',
  `email` varchar(40) NOT NULL default '',
  `notes` varchar(800) default NULL,
  `initial_contact` varchar(50) default NULL,
  `date` date NOT NULL default '0000-00-00',
  `xpress_resume` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`tel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
CREATE TABLE `themes` (
  `themes_id` mediumint(8) unsigned NOT NULL auto_increment,
  `template_name` varchar(30) NOT NULL default '',
  `style_name` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`themes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` mediumint(9) NOT NULL auto_increment,
  `user_active` tinyint(1) default '1',
  `username` varchar(25) NOT NULL default '',
  `user_password` varchar(32) NOT NULL default '',
  `user_session_time` int(11) NOT NULL default '0',
  `user_session_page` smallint(5) NOT NULL default '0',
  `user_lastvisit` int(11) NOT NULL default '0',
  `user_login_tries` smallint(6) NOT NULL default '0',
  `user_last_login_try` smallint(6) NOT NULL default '0',
  `user_regdate` int(11) NOT NULL default '0',
  `user_level` tinyint(4) default '0',
  `user_posts` mediumint(8) unsigned NOT NULL default '0',
  `user_style` tinyint(4) default NULL,
  `user_lang` varchar(255) default NULL,
  `user_dateformat` varchar(14) NOT NULL default 'd M Y H:i',
  `user_new_privmsg` smallint(5) unsigned NOT NULL default '0',
  `user_unread_privmsg` smallint(5) unsigned NOT NULL default '0',
  `user_last_privmsg` int(11) NOT NULL default '0',
  `user_emailtime` int(11) default NULL,
  `user_viewemail` tinyint(1) default NULL,
  `user_attachsig` tinyint(1) default NULL,
  `user_allowhtml` tinyint(1) default '1',
  `user_allowbbcode` tinyint(1) default '1',
  `user_allowsmile` tinyint(1) default '1',
  `user_allowavatar` tinyint(1) NOT NULL default '1',
  `user_allow_pm` tinyint(1) NOT NULL default '1',
  `user_allow_viewonline` tinyint(1) NOT NULL default '1',
  `user_notify` tinyint(1) NOT NULL default '1',
  `user_notify_pm` tinyint(1) NOT NULL default '0',
  `user_popup_pm` tinyint(1) NOT NULL default '0',
  `user_rank` int(11) default '0',
  `user_avatar` varchar(100) default NULL,
  `user_avatar_type` tinyint(4) NOT NULL default '0',
  `user_email` varchar(255) default NULL,
  `user_timezone` varchar(30) default NULL,
  `user_icq` varchar(15) default NULL,
  `user_website` varchar(100) default NULL,
  `user_from` varchar(100) default NULL,
  `user_sig` text,
  `user_sig_bbcode_uid` varchar(10) default NULL,
  `user_aim` varchar(255) default NULL,
  `user_mobile` varchar(25) default NULL,
  `user_phone_mode` varchar(18) default NULL,
  `user_callback` tinyint(1) default NULL,
  `user_sip` tinyint(1) default NULL,
  `user_call_to` tinyint(1) default NULL,
  `user_show_email` tinyint(1) NOT NULL,
  `user_show_number` tinyint(1) NOT NULL,
  `user_yim` varchar(255) default NULL,
  `user_msnm` varchar(255) default NULL,
  `user_occ` varchar(100) default NULL,
  `user_introduction` text,
  `user_education` text,
  `user_experience` text,
  `user_interests` varchar(255) default NULL,
  `user_actkey` varchar(32) default NULL,
  `user_newpasswd` varchar(32) default NULL,
  PRIMARY KEY  (`user_id`),
  KEY `user_session_time` (`user_session_time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `vanity_index`
--

DROP TABLE IF EXISTS `vanity_index`;
CREATE TABLE `vanity_index` (
  `vanity_url` varchar(30) NOT NULL,
  `backend_url` varchar(80) NOT NULL,
  UNIQUE KEY `index_van` (`vanity_url`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

