<?php

/**
 * This is a page that will handle the livechat facility for the site
 *                               w.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 **/

// anti-hacker thing
define('IN_DECRUIT', true);

$root_path = './';

include($root_path . 'extension.inc');
include($root_path . 'common.'.$phpEx);

// and NO I don't know why this is called w.php ...

// do an access check here to make sure they belong 
// clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
access_check(GUEST);

$page= $_GET['name'];
if ( $page == '' ){ $page='example';}
$sid= $_GET['sid'];

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//

// template for the chat_client
$template->set_filenames(array('body' => 'chat_client.tpl'));

/* spam-keywords & block ip's */
$spam[] = "wojo.com";
$spam[] = "dick";

$blockip[] = "72.60.167.89";
 
// security first
$_GET=array_map("input_check",$_GET);

$msg = $_GET['m'];
$name = $_GET['name'];

// add the '#' here to make it a real color
$color = $_GET['color'];
$color = $color ;

/* Preparing the Content */
$fn = "chat.txt";

// can't have the string empty at the start or it won't connect
$string = "<!-- EMPTY_TEXT -->\n";
$count_string = strlen($string);

$maxlines = 20;

if (isset($_GET['chat']))
{

  if ( isset($_GET['clear']) )
    {
      // open to write, write and close...
      $handle = fopen ($fn, 'a'); 

      // don't completely empty the file...
      ftruncate($handle,$count_string);
      fclose($handle);

      // notify the database that the chat client is now closed
      $sql = "UPDATE config SET config_value = 0 WHERE config_name = 'chat_status'";

      if ( ! ( $result = mysql_query($sql) ) )
	{
	  message_die(GENERAL_ERROR,'Cannot reach chat status ... ','',__LINE__,__FILE__,$sql);
	}

    }
  else if ($msg != "") 
    {

      // open the contents of the file, store it in an array based on \n 
      // then add the new content to it and put it back into the file again...
      $handle = fopen ($fn, 'r'); 
      $chattext = fread($handle, filesize($fn)); fclose($handle);

      // seperate messages into newlines...
      $arr1 = explode("\n", $chattext);

      // check whether the array is larger than maxlines...
      if (count($arr1) > $maxlines)
      {

	/* Pruning */
	$arr1 = array_reverse($arr1);

	for ($i=0; $i<$maxlines; $i++) 
	  { 
	    $arr2[$i] = $arr1[$i]; 
	  }

	$arr2 = array_reverse($arr2);

      } 
      else 
	{
	  $arr2 = $arr1;
	}

      // implode the text again...
      $chattext = implode("\n", $arr2);

       /* Spam Control Part 2 */
      // if the message repeats itself kill it...
      if (substr_count($chattext, $msg) > 2) { die(); }

      /* Write */
      $out = $chattext . "<span style='color:" . $color . "'>" . $name . "</span>: ". $msg ;

      $out = str_replace("\'", "'", trim($out));
      $out = str_replace("\\\"", "\"", $out);

      // add the newline on here...
      $out = $out . "\n" ;

      // open to write, write and close...
      $handle = fopen ($fn, 'w'); 
      fwrite ($handle, $out);  
      fclose($handle);

    }

}
else {

  // output the various variables ...
  $template->assign_vars(array('CHAT_TITLE'=>'Chat Room'));

  // update the db, tell it that the client is now open....
  $sql = "UPDATE config SET config_value = 1 WHERE config_name = 'chat_status'";

  if ( ! ( $result = mysql_query($sql) )  )
    {
      message_die(GENERAL_ERROR,'Cannot reach chat status ... ','',__LINE__,__FILE__,$sql);
    }

  // set up a clear option to erase the board
  $clear_text = "<input type='button' value='clear' onclick='clear_chat();' style='cursor:pointer;border:1px solid gray;'>";

  // give users and admins some extra options...
  if ( $userdata['user_level'] >= USER )
  {
      $template->assign_vars(array('CLEAR_TEXT'=>$clear_text));
  }  

}

//
// Generate the page
//
$template->pparse('body');


?>