<?php

/**
 * This class provides a central point for the board to get values that are used again and again (mostly in forms). They provide an easy repository so eg. a list of countries in and option box will come out on one form in the same order as they do on another. Also, this class and form_functions make sure that the db codes are persistant  1 = Austrailia, 2 = South Africa, 3 = Canada ... that kinda thing
 *
 *                             form_sections.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 ***************************************************************************/

class form_sections {

  //  global $userdata ;

  /**
   * Basic constructor
   *
   */
  function form_sections()
  {

    global $lang, $userdata ;
    
    $this->lang = $lang;
    $this->userdata = $userdata;

  }

  /**
   * Output the country options, select one of them if you wish.
   *
   */
  function get_country_options($select_text='')
  {

    global $lang;

    if ( $select_text != '' )
      {

	switch($select_text)
	  {
	  case (1) : $country_selected1 = "selected='selected'" ; break ;
	  case (2) : $country_selected2 = "selected='selected'" ; break ;
	  case (3) : $country_selected3 = "selected='selected'" ; break ;
	  case (4) : $country_selected4 = "selected='selected'" ; break ;
	  case (5) : $country_selected5 = "selected='selected'" ; break ;
	  case (6) : $country_selected6 = "selected='selected'" ; break ;
	  case (7) : $country_selected7 = "selected='selected'" ; break ;
	  default : break;
	  }

      }

    $options="
 	 <option value='-1'>[Select]</option>
 	 <option $country_selected1 value='".AUSTRALIA_CODE."'>{$lang['Australia']}</option>
 	 <option $country_selected2 value='".CANADA_CODE."'>{$lang['Canada']}</option>
 	 <option $country_selected3 value='".IRELAND_CODE."'>{$lang['Ireland']}</option>
 	 <option $country_selected4 value='".NEW_ZEALAND_CODE."'>{$lang['New_zealand']}</option>
 	 <option $country_selected5 value='".UNITED_STATES_CODE."'>{$lang['United_states']}</option>
 	 <option $country_selected6 value='".UNITED_KINGDOM_CODE."'>{$lang['United_kingdom']}</option>
 	 <option $country_selected7 value='".SOUTH_AFRICA_CODE."'>{$lang['South_africa']}</option>
      ";
   
     return $options;

  }

  /**
   * Output the location options, select one of them if you wish.
   *
   */
  function get_location_options($select_text='')
  {

    global $lang;

    // if we've preselected one already,
    // then chose it here
    if ( $select_text != '' )
      {

	switch ($select_text)
	  {
	  case (1) : $city_selected1 = "selected='selected'" ; break ;
	  case (2) : $city_selected2 = "selected='selected'" ; break ;
	  case (3) : $city_selected3 = "selected='selected'" ; break ;
	  case (4) : $city_selected4 = "selected='selected'" ; break ;
	  case (5) : $city_selected5 = "selected='selected'" ; break ;
	  case (6) : $city_selected6 = "selected='selected'" ; break ;
	  case (7) : $city_selected7 = "selected='selected'" ; break ;
	  case (8) : $city_selected8 = "selected='selected'" ; break ;
	  default : ; break  ;
	  }

      }

    $locations="
	 <option value='-1'>[Select]</option>
	 <option $city_selected1 value='".ANY."'>{$lang['Any']}</option>
	 <option $city_selected2 value='".SEOUL."' >{$lang['Seoul']}</option>
	 <option $city_selected3 value='".DAEGU."'>{$lang['Daegu']}</option>
	 <option $city_selected4 value='".BUSAN."'>{$lang['Busan']}</option>
	 <option $city_selected5 value='".INCHEON."'>{$lang['Incheon']}</option>
	 <option $city_selected6 value='".GWANGJU."'>{$lang['Gwangju']}</option>
	 <option $city_selected7 value='".ULSAN."'>{$lang['Ulsan']}</option>
	 <option $city_selected9 value='".ILSAN."'>{$lang['Ilsan']}</option>
       ";

    return $locations ;

  }

  /**
   * Output the organization type for job reminder form
   *
   */
  function get_organization_list()
  {
    
    global $lang ; 

    $organization_type = "<option value='".HAGWON_CODE."'>{$lang['Hagwon']}</option>
	                  <option value='".PUBLIC_SCHOOL_CODE."'>{$lang['Public_school']}</option>
	                  <option value='".UNIVERSITY_CODE."'>{$lang['University']}</option>
	                  <option value='".LANGUAGE_INSTITUTE_CODE."'>{$lang['Language_institute']}</option>";

    return $organization_type ;

  }
  
  /**
   * Output the email helper list
   *
   */
  function get_helper_list()
  {

    $email_helper_list = "
      <tr>
       <td colspan='2' align='center' >
        <a href=\"javascript:place_email('{REPLY_TITLE0}');\">{REPLY_TITLE0}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE1}');\">{REPLY_TITLE1}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE2}');\">{REPLY_TITLE2}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE3}');\">{REPLY_TITLE3}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE4}');\">{REPLY_TITLE4}</a>
        <a href=\"javascript:place_email('{REPLY_TITLE5}');\">{REPLY_TITLE5}</a>
       </td>
      </tr>
      <tr>
       <td colspan='2' align='center' >
        <a href=\"javascript:place_email('{REPLY_TITLE6}');\">{REPLY_TITLE6}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE7}');\">{REPLY_TITLE7}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE8}');\">{REPLY_TITLE8}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE9}');\">{REPLY_TITLE9}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_TITLE10}');\">{REPLY_TITLE10}</a>
        <a href=\"javascript:place_email('{REPLY_TITLE11}');\">{REPLY_TITLE11}</a>
       </td>
      </tr>
      <tr>
       <td colspan='2' align='center' >
        <a href=\"javascript:place_email('{REPLY_LINK0}');\">{REPLY_LINK0}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_LINK1}');\">{REPLY_LINK1}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_LINK2}');\">{REPLY_LINK2}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_LINK3}');\">{REPLY_LINK3}</a>&nbsp;&nbsp;
        <a href=\"javascript:place_email('{REPLY_LINK4}');\">{REPLY_LINK4}</a>
        <a href=\"javascript:place_email('{REPLY_LINK5}');\">{REPLY_LINK5}</a>
       </td>
      </tr>
    ";
    
    return $email_helper_list ;

  }

  /**
   *  Output the contact bar for the email sections
   *
   */
  function get_contact_search()
  {

    // it doesn't see this $lang for some reason...
    global $lang;

    $contact_search = "
    <tr>
    <td colspan='2' align='center'>{$lang['School_search']}:  
     <a href=\"javascript:sndReq('school_search','A');\">A</a>
     <a href=\"javascript:sndReq('school_search','B');\">B</a>
     <a href=\"javascript:sndReq('school_search','C');\">C</a>
     <a href=\"javascript:sndReq('school_search','D');\">D</a>
     <a href=\"javascript:sndReq('school_search','E');\">E</a>
     <a href=\"javascript:sndReq('school_search','F');\">F</a>
     <a href=\"javascript:sndReq('school_search','G');\">G</a>
     <a href=\"javascript:sndReq('school_search','H');\">H</a>
     <a href=\"javascript:sndReq('school_search','I');\">I</a>
     <a href=\"javascript:sndReq('school_search','J');\">J</a>
     <a href=\"javascript:sndReq('school_search','K');\">K</a>
     <a href=\"javascript:sndReq('school_search','L');\">L</a>
     <a href=\"javascript:sndReq('school_search','M');\">M</a>
     <a href=\"javascript:sndReq('school_search','N');\">N</a>
     <a href=\"javascript:sndReq('school_search','O');\">O</a>
     <a href=\"javascript:sndReq('school_search','P');\">P</a>
     <a href=\"javascript:sndReq('school_search','Q');\">Q</a>
     <a href=\"javascript:sndReq('school_search','R');\">R</a>
     <a href=\"javascript:sndReq('school_search','S');\">S</a> 
     <a href=\"javascript:sndReq('school_search','T');\">T</a>
     <a href=\"javascript:sndReq('school_search','U');\">U</a>
     <a href=\"javascript:sndReq('school_search','V');\">V</a>
     <a href=\"javascript:sndReq('school_search','W');\">W</a>
     <a href=\"javascript:sndReq('school_search','X');\">X</a>
     <a href=\"javascript:sndReq('school_search','Y');\">Y</a>
     <a href=\"javascript:sndReq('school_search','Z');\">Z</a>
    </td>
  </tr> 
 <tr>
    <td colspan='2' align='center'>{$lang['Contact_search']}:
     <a href=\"javascript:sndReq('contact_search','A');\">A</a>
     <a href=\"javascript:sndReq('contact_search','B');\">B</a>
     <a href=\"javascript:sndReq('contact_search','C');\">C</a>
     <a href=\"javascript:sndReq('contact_search','D');\">D</a>
     <a href=\"javascript:sndReq('contact_search','E');\">E</a>
     <a href=\"javascript:sndReq('contact_search','F');\">F</a>
     <a href=\"javascript:sndReq('contact_search','G');\">G</a>
     <a href=\"javascript:sndReq('contact_search','H');\">H</a>
     <a href=\"javascript:sndReq('contact_search','I');\">I</a>
     <a href=\"javascript:sndReq('contact_search','J');\">J</a>
     <a href=\"javascript:sndReq('contact_search','K');\">K</a>
     <a href=\"javascript:sndReq('contact_search','L');\">L</a>
     <a href=\"javascript:sndReq('contact_search','M');\">M</a>
     <a href=\"javascript:sndReq('contact_search','N');\">N</a>
     <a href=\"javascript:sndReq('contact_search','O');\">O</a>
     <a href=\"javascript:sndReq('contact_search','P');\">P</a>
     <a href=\"javascript:sndReq('contact_search','Q');\">Q</a>
     <a href=\"javascript:sndReq('contact_search','R');\">R</a>
     <a href=\"javascript:sndReq('contact_search','S');\">S</a> 
     <a href=\"javascript:sndReq('contact_search','T');\">T</a>
     <a href=\"javascript:sndReq('contact_search','U');\">U</a>
     <a href=\"javascript:sndReq('contact_search','V');\">V</a>
     <a href=\"javascript:sndReq('contact_search','W');\">W</a>
     <a href=\"javascript:sndReq('contact_search','X');\">X</a>
     <a href=\"javascript:sndReq('contact_search','Y');\">Y</a>
     <a href=\"javascript:sndReq('contact_search','Z');\">Z</a>
    </td>
  </tr> 
    <tr>
     <td colspan='2' align='center'>LOCATION SEARCH:
       <form method=\"post\" enctype=\"multipart/form-data\" action=\"form.php?mode=school_form\" name='area_search' >
	<select name='location' id='location1'>
	 <option value='-1'>[Select]</option>
	 <option value='".ANY."'>{$lang['Any']}</option>
	 <option value='".SEOUL."'>{$lang['Seoul']}</option>
	 <option value='".DAEGU."'>{$lang['Daegu']}</option>
	 <option value='".BUSAN."'>{$lang['Busan']}</option>
	 <option value='".INCHEON."'>{$lang['Incheon']}</option>
	 <option value='".GWANGJU."'>{$lang['Gwangju']}</option>
	 <option value='".ULSAN."'>{$lang['Ulsan']}</option>
	 <option value='".ILSAN."'>{$lang['Ilsan']}</option>
	 </select>&nbsp;
	<input type='submit' name='area_search' value='Search' />
      </form>
    </td>
  </tr>
";

    return $contact_search ;

  }

  /**
   *  Used to output a list of months progressively beginning from this month.
   *
   */
  function get_progressive_months()
  {

    global $lang ;
    
    // lets output the creeping arrival selection
    $month_0 = date('F',time());
    $month_1 = date('F',strtotime("+1 month"));
    $month_2 = date('F',strtotime("+2 month"));
    $month_3 = date('F',strtotime("+3 month"));
    $month_4 = date('F',strtotime("+4 month"));
    $month_5 = date('F',strtotime("+5 month"));
    $month_6 = date('F',strtotime("+6 month"));
    $month_7 = date('F',strtotime("+7 month"));
    $month_8 = date('F',strtotime("+8 month"));
    $month_9 = date('F',strtotime("+9 month"));
    $month_10 = date('F',strtotime("+10 month"));
    $month_11 = date('F',strtotime("+11 month"));

    // filter this thorugh a language and string_to_db code filter
    // return the month code in three's ...eg. January = 1-3
    for ( $i = 0 ; $i < 12 ; $i++ ) 
      {

	$$month = $month_.'$i';

	switch ( $$month )
	  {
	  case ('January') : $month_code_0 = 3 ; $month_0 = $lang['January'] ;  $break;
	  case ('February') : $month_code_1 = 6 ; $month_1 = $lang['February'] ;  $break;
	  case ('March') : $month_code_2 = 9  ; $month_2 = $lang['March'] ;  $break;
	  case ('April') : $month_code_3 = 12  ; $month_3 = $lang['April'] ;  $break;
	  case ('May') : $month_code_4 = 15  ; $month_4 = $lang['May'] ;  $break;
	  case ('June') : $month_code_5 = 18 ; $month_5 = $lang['June'] ;  $break;
	  case ('July') : $month_code_6 = 21 ; $month_6 = $lang['July'] ;  $break;
	  case ('August') : $month_code_7 = 24 ; $month_7 = $lang['August'] ;  $break;
	  case ('September') : $month_code_8 = 27  ; $month_8 = $lang['September'] ;  $break;
	  case ('October') : $month_code_9 = 30 ; $month_9 = $lang['October'] ;  $break;
	  case ('November') : $month_code_10 = 33 ; $month_10 = $lang['November'] ;  $break;
	  case ('December') : $month_code_11 = 36 ; $month_11 = $lang['December'] ;  $break;
	  default : break ;
	  }

      }

    $prog_months = "
                     <option value='".ANY_MONTH_CODE."' >{$lang['Any']}</option>
                     <option value='$month_code_0' >$month_0</option>
                     <option value='$month_code_1' >$month_1</option>
                     <option value='$month_code_2' >$month_2</option>
                     <option value='$month_code_3' >$month_3</option>
                     <option value='$month_code_4' >$month_4</option>
                     <option value='$month_code_5' >$month_5</option>
                     <option value='$month_code_6' >$month_6</option>
	             <option value='$month_code_7' >$month_7</option>	
	             <option value='$month_code_8' >$month_8</option>	
	             <option value='$month_code_9' >$month_9</option>	
	             <option value='$month_code_10' >$month_10</option>	
	             <option value='$month_code_11' >$month_11</option>	
                      ";

    return $prog_months;

  }

  /**
   * Get a list of months
   *
   */
  function get_month_list()
  {

    global $lang ;

    $months_list = " <option value='".JANUARY_MONTH_CODE."'>{$lang['January']}</option>
	             <option value='".FEBRUARY_MONTH_CODE."'>{$lang['February']}</option>
	             <option value='".MARCH_MONTH_CODE."'>{$lang['March']}</option>
	             <option value='".APRIL_MONTH_CODE."'>{$lang['April']}</option>
	             <option value='".MAY_MONTH_CODE."'>{$lang['May']}</option>
	             <option value='".JUNE_MONTH_CODE."'>{$lang['June']}</option>
	             <option value='".JULY_MONTH_CODE."'>{$lang['July']}</option>
	             <option value='".AUGUST_MONTH_CODE."'>{$lang['August']}</option>
	             <option value='".SEPTEMBER_MONTH_CODE."'>{$lang['September']}</option>
	             <option value='".OCTOBER_MONTH_CODE."'>{$lang['October']}</option>
	             <option value='".NOVEMBER_MONTH_CODE."'>{$lang['November']}</option>
	             <option value='".DECEMBER_MONTH_CODE."'>{$lang['December']}</option>";

    return $months_list ;
  }

  /**
   *  This is a normal number options but it's one off for the teacher resume
   *
   */
  function get_number_options_one()
  {

    for ($i=1980, $j=81 ;$i>1904;$i--,$j--)
      {
	$options.="<option value='$j' >$i</option>";
      }

    return $options ;

  }

  /**
   * Normal number options menu
   *
   */
  function get_number_options($from,$till)
  {

    for ($i=$from ;$i<=$till;$i++)
      {
	$options.="<option value='$i' >$i</option>";
      }

    return $options ;

  }

  /** 
   * Get telephone zones
   *
   */
  function get_telephone_codes($select_text='')
  {

    global $lang ;

    if ( $select_text != '' )
      {
       
	switch ( $select_text )
	  {
	  case (AUSTRALIA_PHONE_CODE) : $code_selected1 = "selected='selected'" ; break;
	  case (CANADA_PHONE_CODE) : $code_selected2 = "selected='selected'" ; break;
	  case (CHINA_PHONE_CODE) : $code_selected3 = "selected='selected'" ; break;
	  case (UNITED_KINGDOM_PHONE_CODE) : $code_selected4 = "selected='selected'" ; break;
	  case (INDIA_PHONE_CODE) : $code_selected5 = "selected='selected'" ; break;
	  case (INDONESIA_PHONE_CODE) : $code_selected6 = "selected='selected'" ; break;
	  case (IRELAND_PHONE_CODE) : $code_selected7 = "selected='selected'" ; break;
	  case (SOUTH_KOREA_PHONE_CODE) : $code_selected8 = "selected='selected'" ; break;
	  case (MEXICO_PHONE_CODE) : $code_selected8 = "selected='selected'" ; break;
	  case (PHILLIPINES_PHONE_CODE) : $code_selected10 = "selected='selected'" ; break;
	  case (THAILAND_PHONE_CODE) : $code_selected11 = "selected='selected'" ; break;
	  case (UNITED_STATES_PHONE_CODE) : $code_selected12 = "selected='selected'" ; break;
	  case (NEW_ZEALAND_PHONE_CODE) : $code_selected13 = "selected='selected'" ; break;
	  case (SOUTH_AFRICA_PHONE_CODE) : $code_selected14 = "selected='selected'" ; break;
	  case (SPAIN_PHONE_CODE) : $code_selected15 = "selected='selected'" ; break;
	  default: break;
	  }

      }

    $telephone_codes = "<option $code_selected0 value=''>{$lang['Other']}</option>
                        <option $code_selected1 value='".AUSTRALIA_PHONE_CODE."'>{$lang['Australia']}</option>
	 		<option $code_selected2 value='".CANADA_PHONE_CODE."'>{$lang['Canada']}</option>
	 		<option $code_selected3 value='".CHINA_PHONE_CODE."'>{$lang['China']}</option>
	 		<option $code_selected4 value='".UNITED_KINGDOM_PHONE_CODE."'>{$lang['United_kingdom']}</option>
	 		<option $code_selected5 value='".INDIA_PHONE_CODE."'>{$lang['India']}</option>
	 		<option $code_selected6 value='".INDONESIA_PHONE_CODE."'>{$lang['Indonesia']}</option>
	 		<option $code_selected7 value='".IRELAND_PHONE_CODE."'>{$lang['Ireland']}</option>
	 		<option $code_selected8 value='".SOUTH_KOREA_PHONE_CODE."'>{$lang['South_korea']}</option>
	 		<option $code_selected9 value='".MEXICO_PHONE_CODE."'>{$lang['Mexico']}</option>
	 		<option $code_selected10 value='".PHILLIPINES_PHONE_CODE."'>{$lang['Phillipines']}</option>
	 		<option $code_selected11 value='".THAILAND_PHONE_CODE."'>{$lang['Thailand']}</option>	 		
	 		<option $code_selected12 value='".UNITED_STATES_PHONE_CODE."'>{$lang['United_states']}</option>
	 		<option $code_selected13 value='".NEW_ZEALAND_PHONE_CODE."'>{$lang['New_zealand']}</option>
	 		<option $code_selected14 value='".SOUTH_AFRICA_PHONE_CODE."'>{$lang['South_africa']}</option>
	 		<option $code_selected15 value='".SPAIN_PHONE_CODE."'>{$lang['Spain']}</option>";

    return $telephone_codes ;

  }

  /**
   * Get timezone options
   *
   */ 
  function get_timezone_options($select_text='')
  {
    
    global $lang ;

    if ($select_text != '')
      {

	switch ($select_text)
	  {
	  case (1) : $timezone1_selected = "selected='selected'" ; break ;
	  case (2) : $timezone2_selected = "selected='selected'" ; break ;
	  case (3) : $timezone3_selected = "selected='selected'" ; break ;
	  case (4) : $timezone4_selected = "selected='selected'" ; break ;
	  case (5) : $timezone5_selected = "selected='selected'" ; break ;
	  case (6) : $timezone6_selected = "selected='selected'" ; break ;
	  case (7) : $timezone7_selected = "selected='selected'" ; break ;
	  case (8) : $timezone8_selected = "selected='selected'" ; break ;
	  case (9) : $timezone9_selected = "selected='selected'" ; break ;
	  case (10) : $timezone10_selected = "selected='selected'" ; break ;
	  case (11) : $timezone11_selected = "selected='selected'" ; break ;
	  case (12) : $timezone12_selected = "selected='selected'" ; break ;
	  case (13) : $timezone13_selected = "selected='selected'" ; break ;
	  default : break ;
	  }

      }

      $timezone_countries ="
	 	   <option value='-1'>[Select]</option>
	 	   <option $timezone1_selected value='".ALASKA_TIMEZONE_CODE."'>{$lang['Alaska_zone_text']}</option>
	 	   <option $timezone2_selected value='".LOS_ANGELES_TIMEZONE_CODE."'>{$lang['Los_angeles_zone_text']}</option>
	 	   <option $timezone3_selected value='".DENVER_TIMEZONE_CODE."'>{$lang['Denver_zone_text']}</option>
	 	   <option $timezone4_selected value='".DALLAS_TIMEZONE_CODE."'>{$lang['Dallas_zone_text']}</option>
	 	   <option $timezone5_selected value='".NEW_YORK_TIMEZONE_CODE."'>{$lang['New_york_zone_text']}</option>
	 	   <option $timezone6_selected value='".NEW_FOUNDLAND_TIMEZONE_CODE."'>{$lang['New_foundland_zone_text']}</option>
	           <option $timezone7_selected value='".LONDON_TIMEZONE_CODE."'>{$lang['London_zone_text']}</option>
	           <option $timezone8_selected value='".BERLIN_TIMEZONE_CODE."'>{$lang['Berlin_zone_text']}</option>
	           <option $timezone9_selected value='".BANGKOK_TIMEZONE_CODE."'>{$lang['Bangkok_zone_text']}</option>
	           <option $timezone10_selected value='".PERTH_TIMEZONE_CODE."'>{$lang['Perth_zone_text']}</option>
	 	   <option $timezone11_selected value='".SEOUL_TIMEZONE_CODE."'>{$lang['Seoul_zone_text']}</option>
	           <option $timezone12_selected value='".SYDNEY_TIMEZONE_CODE."'>{$lang['Sydney_zone_text']}</option>
	 	   <option $timezone13_selected value='".AUCKLAND_TIMEZONE_CODE."'>{$lang['Auckland_zone_text']}</option>
                ";

    return $timezone_countries ;

  }

  /**
   * Get hours for the timezone, preselect one if needed.
   *
   */
  function get_hours_timezone($select_text='')
  {

    global $lang ;

    if ( $select_text != '' )
      {

	switch ( $select_text )
	  {
	  case (1) :  $hours_selected1 = "selected='selected'" ; break;
	  case (2) :  $hours_selected2 = "selected='selected'" ; break;
	  case (3) :  $hours_selected3 = "selected='selected'" ; break;
	  case (4) :  $hours_selected4 = "selected='selected'" ; break;
	  case (5) :  $hours_selected5 = "selected='selected'" ; break;
	  case (6) :  $hours_selected6 = "selected='selected'" ; break;
	  case (7) :  $hours_selected7 = "selected='selected'" ; break;
	  case (8) :  $hours_selected8 = "selected='selected'" ; break;
	  case (9) :  $hours_selected9 = "selected='selected'" ; break;
	  case (10) :  $hours_selected10 = "selected='selected'" ; break;
	  case (11) :  $hours_selected11 = "selected='selected'" ; break;
	  case (12) :  $hours_selected12 = "selected='selected'" ; break;
	  case (13) :  $hours_selected13 = "selected='selected'" ; break;
	  case (14) :  $hours_selected14 = "selected='selected'" ; break;
	  case (15) :  $hours_selected15 = "selected='selected'" ; break;
	  case (16) :  $hours_selected16 = "selected='selected'" ; break;
	  case (17) :  $hours_selected17 = "selected='selected'" ; break;
	  case (18) :  $hours_selected18 = "selected='selected'" ; break;
	  case (19) :  $hours_selected19 = "selected='selected'" ; break;
	  case (20) :  $hours_selected20 = "selected='selected'" ; break;
	  case (21) :  $hours_selected21 = "selected='selected'" ; break;
	  case (22) :  $hours_selected22 = "selected='selected'" ; break;
	  case (23) :  $hours_selected23 = "selected='selected'" ; break;
	  case (24) :  $hours_selected24 = "selected='selected'" ; break;
	  default : break ;
	  }

      }

    $hours_timezone = " 
                        <option value='0'>{$lang['Select']}</option>
	 		<option $hours_selected1 value='1'>1:00</option>
	 		<option $hours_selected2 value='2'>2:00</option>
	 		<option $hours_selected3 value='3'>3:00</option>
	 		<option $hours_selected4 value='4'>4:00</option>
	 		<option $hours_selected5 value='5'>5:00</option>
	 		<option $hours_selected6 value='6'>6:00</option>
	 		<option $hours_selected7 value='7'>7:00</option>
	 		<option $hours_selected8 value='8'>8:00</option>
	 		<option $hours_selected9 value='9'>9:00</option>
			<option $hours_selected10 value='10'>10:00</option>
			<option $hours_selected11 value='11'>11:00</option>
			<option $hours_selected12 value='12'>12:00</option>	 		
	 		<option $hours_selected13 value='13'>13:00</option>
	 		<option $hours_selected14 value='14'>14:00</option>
	 		<option $hours_selected15 value='15'>15:00</option>
	 		<option $hours_selected16 value='16'>16:00</option>
	 		<option $hours_selected17 value='17'>17:00</option>
	 		<option $hours_selected18 value='18'>18:00</option>
	 		<option $hours_selected19 value='19'>19:00</option>
	 		<option $hours_selected20 value='20'>20:00</option>
	 		<option $hours_selected21 value='21'>21:00</option>
	 		<option $hours_selected22 value='22'>22:00</option>
	 		<option $hours_selected23 value='23'>23:00</option>
	 		<option $hours_selected24 value='24'>24:00</option>
                    ";

    return $hours_timezone;

  }

  /**
   * Get the timezone minutes...
   *
   */
  function get_minute_timezone()
  {

    // minutes for the timetable..
    $min_timezone_home = "<option {TIMEZONE_MIN_SELECTED00} value='00'>00</option>
	 		<option {TIMEZONE_MIN_SELECTED05} value='05'>05</option>
	 		<option {TIMEZONE_MIN_SELECTED10} value='10'>10</option>
	 		<option {TIMEZONE_MIN_SELECTED15} value='15'>15</option>
	 		<option {TIMEZONE_MIN_SELECTED20} value='20'>20</option>
	 		<option {TIMEZONE_MIN_SELECTED25} value='25'>25</option>
	 		<option {TIMEZONE_MIN_SELECTED30} value='30'>30</option>
	 		<option {TIMEZONE_MIN_SELECTED35} value='35'>35</option>
	 		<option {TIMEZONE_MIN_SELECTED40} value='40'>40</option>
			<option {TIMEZONE_MIN_SELECTED45} value='45'>45</option>
			<option {TIMEZONE_MIN_SELECTED50} value='50'>50</option>
			<option {TIMEZONE_MIN_SELECTED55} value='55'>55</option>	 		
                   ";

    return $min_timezone_home ;
  }


  /**
   * Get salary list, preselect one if needed.
   *
   */
  function get_salary_list($select_text='')
  {

    global $lang ;

    if ($select_text != '')
      {
	
	switch ($select_text)
	  {
	  case (1) : $salary_selected1 = "selected='selected'" ; break ;
	  case (2) : $salary_selected2 = "selected='selected'" ; break ;
	  case (3) : $salary_selected3 = "selected='selected'" ; break ;
	  case (4) : $salary_selected4 = "selected='selected'" ; break ;
	  case (5) : $salary_selected5 = "selected='selected'" ; break ;
	  case (6) : $salary_selected6 = "selected='selected'" ; break ;
	  case (7) : $salary_selected7 = "selected='selected'" ; break ;
	  case (8) : $salary_selected8 = "selected='selected'" ; break ;
	  case (9) : $salary_selected9 = "selected='selected'" ; break ;
	  case (10) : $salary_selected10 = "selected='selected'" ; break ;
	  case (11) : $salary_selected11 = "selected='selected'" ; break ;
	  case (12) : $salary_selected12 = "selected='selected'" ; break ;
	  default: break ;
	  }

      }

    $salary_list = "<option value='-1'>{$lang['Select']}</option>
	       <option $salary_selected1 value='".SALARY_1."'>{$lang['Salary_1']}</option>
	       <option $salary_selected2 value='".SALARY_2."'>{$lang['Salary_2']}</option>
	       <option $salary_selected3 value='".SALARY_3."'>{$lang['Salary_3']}</option>
	       <option $salary_selected4 value='".SALARY_4."'>{$lang['Salary_4']}</option>
	       <option $salary_selected5 value='".SALARY_5."'>{$lang['Salary_5']}</option>
	       <option $salary_selected6 value='".SALARY_6."'>{$lang['Salary_6']}</option>
	       <option $salary_selected7 value='".SALARY_7."'>{$lang['Salary_7']}</option>
	       <option $salary_selected8 value='".SALARY_8."'>{$lang['Salary_8']}</option>
	       <option $salary_selected9 value='".SALARY_9."'>{$lang['Salary_9']}</option>
	       <option $salary_selected10 value='".SALARY_10."'>{$lang['Salary_10']}</option>
	       <option $salary_selected11 value='".SALARY_11."'>{$lang['Salary_11']}</option>
	       <option $salary_selected12 value='".SALARY_12."'>{$lang['Salary_12']}</option>";

    return $salary_list ;

  }

  /**
   * Get accomodation list, preselect one if needed
   *
   */
  function get_accomodation_list($select_text='')
  {

    global $lang ;

    if ( $select_text != '' ) 
      {

	switch ($select_text)
	  {
	  case (1) : $selected_accomodation1 ; break ;
	  case (2) : $selected_accomodation2 ; break ;
	  default : break ;
	  }

      }

    $accomodation_block = "<option $selected_accomodation1 value='".ACCOMODATION_SINGLE_CODE."'>{$lang['Single']}</option>
                           <option $selected_accomodation2 value='".ACCOMODATION_DOUBLE_CODE."'>{$lang['Double']}</option>
                            ";

    return $accomodation_block ;

  }

  /**
   * Get number of teachers, preselect one if necessary
   *
   */ 
  function get_number_teachers_list($select_text='')
  {

    global $lang ;

    if ( $select_text != '' ) 
      {

	switch ($select_text)
	  {
	  case (1) : $selected_teachers1 ; break ;
	  case (2) : $selected_teachers2 ; break ;
	  case (3) : $selected_teachers3 ; break ;
	  case (4) : $selected_teachers4 ; break ;
	  case (5) : $selected_teachers5 ; break ;
	  case (6) : $selected_teachers6 ; break ;
	  case (7) : $selected_teachers7 ; break ;
	  case (8) : $selected_teachers8 ; break ;
	  case (9) : $selected_teachers9 ; break ;
	  case (10) : $selected_teachers10 ; break ;
	  default : break ;
	  }

      }

    $number_teachers_block = "<option $selected_teachers1 value='1'>1</option>
                              <option $selected_teachers2 value='2'>2</option>
                              <option $selected_teachers3 value='3'>3</option>
                              <option $selected_teachers4 value='4'>4</option>
                              <option $selected_teachers5 value='5'>5</option>
                              <option $selected_teachers6 value='6'>6</option>
                              <option $selected_teachers7 value='7'>7</option>
                              <option $selected_teachers8 value='8'>8</option>
                              <option $selected_teachers9 value='9'>9</option>
                              <option $selected_teachers10 value='10'>10</option>
                            ";

    return $number_teachers_block ;

  }

  /**
   * Get arrival list, preslect one if necessary 
   *
   */ 
  function get_arrival_list($select_text='')
  {

    global $lang ;

    if ( $select_text != '' )
      {
	
	switch ( $select_text )
	  {
	  case ('01-05') : $arrival1_selected="selected='selected'" ; break ;
	  case ('01-15') : $arrival2_selected="selected='selected'" ; break ;
	  case ('01-25') : $arrival3_selected="selected='selected'" ; break ;
	  case ('02-05') : $arrival4_selected="selected='selected'" ; break ;
	  case ('02-15') : $arrival5_selected="selected='selected'" ; break ;
	  case ('02-25') : $arrival6_selected="selected='selected'" ; break ;
	  case ('03-05') : $arrival7_selected="selected='selected'" ; break ;
	  case ('03-15') : $arrival8_selected="selected='selected'" ; break ;
	  case ('03-25') : $arrival9_selected="selected='selected'" ; break ;
	  case ('04-05') : $arrival10_selected="selected='selected'" ; break ;
	  case ('04-15') : $arrival11_selected="selected='selected'" ; break ;
	  case ('04-25') : $arrival12_selected="selected='selected'" ; break ;
	  case ('05-05') : $arrival13_selected="selected='selected'" ; break ;
	  case ('05-15') : $arrival14_selected="selected='selected'" ; break ;
	  case ('05-25') : $arrival15_selected="selected='selected'" ; break ;
	  case ('06-05') : $arrival16_selected="selected='selected'" ; break ;
	  case ('06-15') : $arrival17_selected="selected='selected'" ; break ;
	  case ('06-25') : $arrival18_selected="selected='selected'" ; break ;
	  case ('07-05') : $arrival19_selected="selected='selected'" ; break ;
	  case ('07-15') : $arrival20_selected="selected='selected'" ; break ;
	  case ('07-25') : $arrival21_selected="selected='selected'" ; break ;
	  case ('08-05') : $arrival22_selected="selected='selected'" ; break ;
	  case ('08-15') : $arrival23_selected="selected='selected'" ; break ;
	  case ('08-25') : $arrival24_selected="selected='selected'" ; break ;
	  case ('09-05') : $arrival25_selected="selected='selected'" ; break ;
	  case ('09-15') : $arrival26_selected="selected='selected'" ; break ;
	  case ('09-25') : $arrival27_selected="selected='selected'" ; break ;
	  case ('10-05') : $arrival28_selected="selected='selected'" ; break ;
	  case ('10-15') : $arrival29_selected="selected='selected'" ; break ;
	  case ('10-25') : $arrival30_selected="selected='selected'" ; break ;
	  case ('11-05') : $arrival31_selected="selected='selected'" ; break ;
	  case ('11-15') : $arrival32_selected="selected='selected'" ; break ;
	  case ('11-25') : $arrival33_selected="selected='selected'" ; break ;
	  case ('12-05') : $arrival34_selected="selected='selected'" ; break ;
	  case ('12-15') : $arrival35_selected="selected='selected'" ; break ;
	  case ('12-25') : $arrival36_selected="selected='selected'" ; break ;
	  default: break ;
	  }

      }

    $teacher_arrival_list="
		 <option $arrival_selected value='-1'>{$lang['Select']}</option>
		 <option $arrival1_selected value='".START_OF_JANUARY."'>{$lang['Start_of_january']}</option>
		 <option $arrival2_selected value='".MIDDLE_OF_JANUARY."'>{$lang['Middle_of_january']}</option>
		 <option $arrival3_selected value='".END_OF_JANUARY."'>{$lang['End_of_january']}</option>
		 <option $arrival4_selected value='".START_OF_FEBRUARY."'>{$lang['Start_of_february']}</option>
		 <option $arrival5_selected value='".MIDDLE_OF_FEBRUARY."'>{$lang['Middle_of_february']}</option>
		 <option $arrival6_selected value='".END_OF_FEBRUARY."'>{$lang['End_of_february']}</option>
		 <option $arrival7_selected value='".START_OF_MARCH."'>{$lang['Start_of_march']}</option>
		 <option $arrival8_selected value='".MIDDLE_OF_MARCH."'>{$lang['Middle_of_march']}</option>
		 <option $arrival9_selected value='".END_OF_MARCH."'>{$lang['End_of_march']}</option>
		 <option $arrival10_selected value='".START_OF_APRIL."'>{$lang['Start_of_april']}</option>
		 <option $arrival11_selected value='".MIDDLE_OF_APRIL."'>{$lang['Middle_of_april']}</option>
		 <option $arrival12_selected value='".END_OF_APRIL."'>{$lang['End_of_april']}</option>
		 <option $arrival13_selected value='".START_OF_MAY."'>{$lang['Start_of_may']}</option>
		 <option $arrival14_selected value='".MIDDLE_OF_MAY."'>{$lang['Middle_of_may']}</option>
		 <option $arrival15_selected value='".END_OF_MAY."'>{$lang['End_of_may']}</option>
		 <option $arrival16_selected value='".START_OF_JUNE."'>{$lang['Start_of_june']}</option>
		 <option $arrival17_selected value='".MIDDLE_OF_JUNE."'>{$lang['Middle_of_june']}</option>
		 <option $arrival18_selected value='".END_OF_JUNE."'>{$lang['End_of_june']}</option>
		 <option $arrival19_selected value='".START_OF_JULY."'>{$lang['Start_of_july']}</option>
		 <option $arrival20_selected value='".MIDDLE_OF_JULY."'>{$lang['Middle_of_july']}</option>
		 <option $arrival21_selected value='".END_OF_JULY."'>{$lang['End_of_july']}</option>
		 <option $arrival22_selected value='".START_OF_AUGUST."'>{$lang['Start_of_august']}</option>
		 <option $arrival23_selected value='".MIDDLE_OF_AUGUST."'>{$lang['Middle_of_august']}</option>
		 <option $arrival24_selected value='".END_OF_AUGUST."'>{$lang['End_of_august']}</option>
		 <option $arrival25_selected value='".START_OF_SEPTEMBER."'>{$lang['Start_of_september']}</option>
		 <option $arrival26_selected value='".MIDDLE_OF_SEPTEMBER."'>{$lang['Middle_of_september']}</option>
		 <option $arrival27_selected value='".END_OF_SEPTEMBER."'>{$lang['End_of_september']}</option>
		 <option $arrival28_selected value='".START_OF_OCTOBER."'>{$lang['Start_of_october']}</option>
		 <option $arrival29_selected value='".MIDDLE_OF_OCTOBER."'>{$lang['Middle_of_october']}</option>
		 <option $arrival30_selected value='".END_OF_OCTOBER."'>{$lang['End_of_october']}</option>
		 <option $arrival31_selected value='".START_OF_NOVEMBER."'>{$lang['Start_of_november']}</option>
		 <option $arrival32_selected value='".MIDDLE_OF_NOVEMBER."'>{$lang['Middle_of_november']}</option>
		 <option $arrival33_selected value='".END_OF_NOVEMBER."'>{$lang['End_of_november']}</option>
		 <option $arrival34_selected value='".START_OF_DECEMBER."'>{$lang['Start_of_december']}</option>
		 <option $arrival35_selected value='".MIDDLE_OF_DECEMBER."'>{$lang['Middle_of_december']}</option>
		 <option $arrival36_selected value='".END_OF_DECEMBER."'>{$lang['End_of_december']}</option>
       ";
    
    return $teacher_arrival_list;

  }

}




?>