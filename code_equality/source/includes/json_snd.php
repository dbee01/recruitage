<?php

/**
 * This is a curl script class to post our ads to various remote servers...
 *
 *                              json_snd.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

// class to wrap all the xhr json requests before 
// sending them to the server
class json_snd {

  var $emails_update;
  var $recruiters_update;
  var $schools_update;
  var $teachers_update;
  var $matches_update;
  var $leads_update;
  var $reminders_update;

  // constructor
  function __construct($emails_arr='',$recruiters_arr='',$schools_arr='',$teachers_arr='',$matches_arr='',$leads_arr='',$reminders_arr='')
  {

    $this->emails_update=$emails_arr;
    $this->recruiters_update=$recruiters_arr;
    $this->schools_update=$schools_arr;
    $this->teachers_update=$teachers_arr;
    $this->matches_update=$matches_arr;
    $this->leads_update=$leads_arr;
    $this->reminders_update=$reminders_arr;

  }

}

?>