<?php


/**
 * Take a list of email addresses and basically filter all incoming resumes and send them  straight to our list of selected email addresses, once they are verified matches.
 *
 *                             xpress_resume.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

class xpress_resume
{


  // city variables
  var $any=ANY, $seoul=SEOUL, $daegu=DAEGU, $busan=BUSAN, $incheon=INCHEON, $gwangju=GWANGJU, $ulsan=ULSAN, $ilsan=ILSAN ;

  var $any_array = array();
  var $seoul_array = array() ; 
  var $daegu_array = array() ;
  var $busan_array = array() ;
  var $incheon_array = array() ;
  var $gwangju_array = array() ;
  var $ulsan_array = array() ;
  var $ilsan_array = array() ;

  // put headers in here
  var $subject_text;
  var $headers;
  var $email_text;
  var $location;

  /**
   * Constructor, open db get details
   *
   */
  function xpress_resume()
  {

    global $db; 
    global $lang ;
    global $board_config ;

    $this->subject_text= $lang['Candidate_resume'] . " ... " . $board_config['sitename'];

    // Any contacts ...
    $sql_any = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->any";
 
    if ( !( $result_any = $db->sql_query($sql_any) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_any);
    }
     
    while ( $row_any = $db->sql_fetchrow($result_any) )
    {
     
      $this->any_array[]=$row_any ;
    }

    // Seoul contacts ...
    $sql_seoul = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->seoul";

    if ( !( $result_seoul = $db->sql_query($sql_seoul) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_seoul);
    }
     
    while ( $row_seoul = $db->sql_fetchrow($result_seoul) )
    {
      $this->seoul_array[]=$row_seoul ;
    }
    
    // Daegu contacts ...
    $sql_daegu = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->daegu";

    if ( !( $result_daegu = $db->sql_query($sql_daegu) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_daegu);
    }
     
    while ( $row_daegu = $db->sql_fetchrow($result_daegu) )
    {
      $this->daegu_array[]=$row_daegu ;
    }
	
    // Busan contacts ...
    $sql_busan = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->busan";

    if ( !( $result_busan = $db->sql_query($sql_busan) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_busan);
    }
     
    while ( $row_busan = $db->sql_fetchrow($result_busan) )
    {
      $this->busan_array[]=$row_busan ;
    }
	
    // Incheon contacts ...
    $sql_incheon = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->incheon";

    if ( !( $result_incheon = $db->sql_query($sql_incheon) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_incheon);
    }
     
    while ( $row_incheon = $db->sql_fetchrow($result_incheon) )
    {
      $this->incheon_array[]=$row_incheon ;
    }
	
    // Gwangju contacts ...
    $sql_gwangju = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->gwangju";

    if ( !( $result_gwangju = $db->sql_query($sql_gwangju) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_gwangju);
    }
     
    while ( $row_gwangju = $db->sql_fetchrow($result_gwangju) )
    {
      $this->gwangju_array[]=$row_gwangju ;
    }
	
    // Ulsan contacts ...
    $sql_ulsan = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->ulsan";

    if ( !( $result_ulsan = $db->sql_query($sql_ulsan) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_ulsan);
    }
     
    while ( $row_ulsan = $db->sql_fetchrow($result_ulsan) )
    {
      $this->ulsan_array[]=$row_ulsan ;
    }

    // Ilsan contacts ...
    $sql_ilsan = "SELECT email,contact FROM tel_contact WHERE xpress_resume = 1 AND location = $this->ilsan";

    if ( !( $result_ilsan = $db->sql_query($sql_ilsan) ) )
    {
        message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql_ilsan);
    }
     
    while ( $row_ilsan = $db->sql_fetchrow($result_ilsan) )
    {
      $this->ilsan_array[]=$row_ilsan ;
    }
   
	
  }

  /**
   * Filter the resumes, send them out according to location
   *
   */
  function resume_filter($resume_id, $resume_name, $resume_age, $resume_country, $resume_location, $resume_start, $location)
  {
   

    ( $resume_location==1 ) ? $resume_location=$lang['In_korea'] : $resume_location=$lang['Not_in_korea'] ; 
	
    $this->headers = 'From: ' . $board_config['board_email'] . "\r\n" .
   'Reply-To: ' . $board_config['board_email'] . "\r\n" .
   'X-Mailer: PHP/' . phpversion();

    switch ($location)
      {
 
         case ($this->any) :  

	 for( $i=0 ; $i<=count($this->any_array);$i+=2 )
	   {
              $contact_name = $this->any_array[$i]['contact'];
	      $email_address = $this->any_array[$i]['email'];
	   }

	 ; break;


       case ($this->seoul) :  

	 for( $i=0 ; $i<=count($this->seoul_array);$i+=2 )
	   {
              $contact_name = $this->seoul_array[$i]['contact'];
	      $email_address = $this->seoul_array[$i]['email'];
	   }

	 ; break ;


       case ($this->daegu) :  

	 for( $i=0 ; $i<=count($this->daegu_array);$i+=2 )
	   {
              $contact_name = $this->daegu_array[$i]['contact'];
	      $email_address = $this->daegu_array[$i]['email'];
	   }

	 ; break ;
	

       case ($this->busan) :  

	 for( $i=0 ; $i<=count($this->busan_array);$i+=2 )
	   {
              $contact_name = $this->busan_array[$i]['contact'];
	      $email_address = $this->busan_array[$i]['email'];
	   }

	 ; break ;
	

       case ($this->gwangju) :  

	 for( $i=0 ; $i<=count($this->gwangju_array);$i+=2 )
	   {
              $contact_name = $this->gwangju_array[$i]['contact'];
	      $email_address = $this->gwangju_array[$i]['email'];
	   }

	 ; break ;
	
	
       case ($this->incheon) :  

	 for( $i=0 ; $i<=count($this->incheon_array);$i+=2 )
	   {
              $contact_name = $this->incheon_array[$i]['contact'];
	      $email_address = $this->incheon_array[$i]['email'];
	   }

	 ; break ;


       case ($this->ulsan) :  

	  for( $i=0 ; $i<=count($this->ulsan_array);$i+=2 )
	   {
              $contact_name = $this->ulsan_array[$i]['contact'];
	      $email_address = $this->ulsan_array[$i]['email'];
    	  
	   }

	 ; break ;
	

       case ($this->ilsan) :  

	  for( $i=0 ; $i<=count($this->ilsan_array);$i+=2 )
	   {
	      $contact_name = $this->ilsan_array[$i]['contact'];
	      $email_address = $this->ilsan_array[$i]['email'];
      	   }

	 ; break ;

      }

   $location=location($location);
   $this->email_text="Hi $contact_name,

This is " . $userdata['username'] . " from " . $board_config['sitename'] . ". Please have a look at this resume, $resume_name has just applied to our recruitment company and is interested in teaching in $location. 

Name: $resume_name
Age : $resume_age
Country : $resume_country
Location : $resume_location
Start Date : $resume_start

http://www.englishteachingkorea.com/gallery.php?id=$resume_id&sid=

Thanks and all the best,

David

EnglishTeachingKorea.com

";

   if ( mail($email_address,$this->subject_text,$this->email_text,$this->headers,SENDMAIL))
   {
     echo 'Mail Sent';
   }else
   {
     echo 'Mail Not Sent';
   }


  }

}

?>
