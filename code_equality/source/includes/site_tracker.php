<?php

/**
 * Examine a user's browser cache. Check if they've hit a competitor site.
 *
 *                             site_tracker.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

class site_tracker {

  /**
   * Build a site_tracker constructor
   *
   */
  function site_tracker()
  {
    // constructor
  }

  /**
   * Store the info on the competitor sites in the site tracker table ...
   *
   */
  function sites_store($id)     
  {

     $ip = $_SERVER['REMOTE_ADDR'];

     $str = parse_str($id);

     if ( $id == '' )
       {
	 return ;
       }
     
     $time = date('Y-m-d H:i:s') ;

     $sql = "INSERT IGNORE INTO " . SITE_TRACKER . " (ip,sites,time) VALUES (INET_ATON('$ip'),'$id','$time')" ;

     if ( ! ( $result = mysql_query($sql) ) )
     {
     	message_die(GENERAL_ERROR,'Could not connect to database for status change','',__LINE__,__FILE__,$sql);
     }

  }

  /**
   * Examine the site data.
   *
   */
  function sites_info()
  {

    global $db ;
    global $board_config;

    $tracker_sites = Array();
    $tracker_results = Array();

    foreach ($board_config as $key => $value)
      {

	if ( stristr($key,'site_pick') )
	{

	    $tracker_sites[] = $value ;
	}

      }
    
    foreach ($tracker_sites as $sites)
      {

	    $sql = "SELECT COUNT(*) c FROM " . SITE_TRACKER . " WHERE sites LIKE '%$sites%' ";

	    if( !($result=$db->sql_query($sql)))
	      {
		message_die(GENERAL_MESSAGE,'Problem with tracker');
	      }

	    while($row=$db->sql_fetchrow($result))
	      {
		
		$tracker_results[$sites]++;

	      }

      }

    return $tracker_results;

  }
  

}
?>