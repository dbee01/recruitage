<?php

/**
 * This page is an include put into recruiter form it renders the page an update page for the recruiter instead of a job page.
 *
 *                          recruiter_form_select.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
  die("Hacking attempt");
}


if($row['kindergarten']){ $template->assign_var('RECRUITER_STUDENTS_KINDERGARTEN_SELECTED' , "checked" ) ; }
if($row['elementary']){ $template->assign_var('RECRUITER_STUDENTS_ELEMENTARY_SELECTED' , "checked" ) ; }
if($row['middle_school']){ $template->assign_var('RECRUITER_STUDENTS_MIDDLE_SCHOOL_SELECTED' , "checked" ) ; } 		
if($row['high_school']){ $template->assign_var('RECRUITER_STUDENTS_HIGH_SCHOOL_SELECTED' , "checked" ) ; }
if($row['adults']){ $template->assign_var('RECRUITER_STUDENTS_ADULTS_SELECTED' , "checked" ) ;  }
 		
switch ( $row['month'] )
{
 case (1) : $template->assign_var('RECRUITER_MONTH_SELECTED_1' , "selected='selected'" ) ; break ; 		
 case (2) : $template->assign_var('RECRUITER_MONTH_SELECTED_2' , "selected='selected'" ) ; break ;
 case (3) : $template->assign_var('RECRUITER_MONTH_SELECTED_3' , "selected='selected'" ) ; break ; 		
 case (4) : $template->assign_var('RECRUITER_MONTH_SELECTED_4' , "selected='selected'" ) ; break ; 		
 case (5) : $template->assign_var('RECRUITER_MONTH_SELECTED_5' , "selected='selected'" ) ; break ; 		
 case (6) : $template->assign_var('RECRUITER_MONTH_SELECTED_6' , "selected='selected'" ) ; break ; 		
 case (7) : $template->assign_var('RECRUITER_MONTH_SELECTED_7' , "selected='selected'" ) ; break ; 		
 case (8) : $template->assign_var('RECRUITER_MONTH_SELECTED_8' , "selected='selected'" ) ; break ; 		
 case (9) : $template->assign_var('RECRUITER_MONTH_SELECTED_9' , "selected='selected'" ) ; break ; 		
 case (10) : $template->assign_var('RECRUITER_MONTH_SELECTED_10' , "selected='selected'" ) ; break ; 		
 case (11) : $template->assign_var('RECRUITER_MONTH_SELECTED_11' , "selected='selected'" ) ; break ; 		
 case (12) : $template->assign_var('RECRUITER_MONTH_SELECTED_12' , "selected='selected'" ) ; break ; 		 		
 default : break ; 
}
		
switch ($row['monthtime'])
{
 case (1) : $template->assign_var('RECRUITER_MONTHTIME_SELECTED_1' , "selected='selected'" ) ; break ; 		
 case (2) : $template->assign_var('RECRUITER_MONTHTIME_SELECTED_2' , "selected='selected'" ) ; break ;
 case (3) : $template->assign_var('RECRUITER_MONTHTIME_SELECTED_3' , "selected='selected'" ) ; break ; 		 		 		
 default : break ; 
}
  		
switch ($row['city'])
{
 case (1) : $template->assign_var('RECRUITER_CITY_SELECTED_1' , "selected='selected'" ) ; break ; 		
 case (2) : $template->assign_var('RECRUITER_CITY_SELECTED_2' , "selected='selected'" ) ; break ;
 case (3) : $template->assign_var('RECRUITER_CITY_SELECTED_3' , "selected='selected'" ) ; break ;
 case (4) : $template->assign_var('RECRUITER_CITY_SELECTED_4' , "selected='selected'" ) ; break ; 
 case (5) : $template->assign_var('RECRUITER_CITY_SELECTED_5' , "selected='selected'" ) ; break ; 
 case (6) : $template->assign_var('RECRUITER_CITY_SELECTED_6' , "selected='selected'" ) ; break ; 
 case (7) : $template->assign_var('RECRUITER_CITY_SELECTED_7' , "selected='selected'" ) ; break ; 
 case (8) : $template->assign_var('RECRUITER_CITY_SELECTED_8' , "selected='selected'" ) ; break ;  		 		 		
 default : break ; 
}
		
switch ($row['accomodation'])
{
 case (1) : $template->assign_var('RECRUITER_ACCOMODATION_SELECTED_1' , "selected='selected'" ) ; break ;
 case (2) : $template->assign_var('RECRUITER_ACCOMODATION_SELECTED_2' , "selected='selected'" ) ; break ;
}
		
switch ( $row['salary'])
{
 case (1) : $template->assign_var('RECRUITER_SALARY_SELECTED_1' , "selected='selected'" ) ; break ; 		
 case (2) : $template->assign_var('RECRUITER_SALARY_SELECTED_2' , "selected='selected'" ) ; break ;
 case (3) : $template->assign_var('RECRUITER_SALARY_SELECTED_3' , "selected='selected'" ) ; break ; 		
 case (4) : $template->assign_var('RECRUITER_SALARY_SELECTED_4' , "selected='selected'" ) ; break ; 		
 case (5) : $template->assign_var('RECRUITER_SALARY_SELECTED_5' , "selected='selected'" ) ; break ; 		
 case (6) : $template->assign_var('RECRUITER_SALARY_SELECTED_6' , "selected='selected'" ) ; break ; 		
 case (7) : $template->assign_var('RECRUITER_SALARY_SELECTED_7' , "selected='selected'" ) ; break ; 		
 case (8) : $template->assign_var('RECRUITER_SALARY_SELECTED_8' , "selected='selected'" ) ; break ; 		
 case (9) : $template->assign_var('RECRUITER_SALARY_SELECTED_9' , "selected='selected'" ) ; break ; 		
 case (10) : $template->assign_var('RECRUITER_SALARY_SELECTED_10' , "selected='selected'" ) ; break ; 		
 case (11) : $template->assign_var('RECRUITER_SALARY_SELECTED_11' , "selected='selected'" ) ; break ; 		
 case (12) : $template->assign_var('RECRUITER_SALARY_SELECTED_12' , "selected='selected'" ) ; break ; 		 		
 default : break ; 
}
		
switch ( $row['number_teachers'])
{
 case (1) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_1' , "selected='selected'" ) ; break ; 		
 case (2) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_2' , "selected='selected'" ) ; break ;
 case (3) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_3' , "selected='selected'" ) ; break ; 		
 case (4) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_4' , "selected='selected'" ) ; break ; 		
 case (5) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_5' , "selected='selected'" ) ; break ; 		
 case (6) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_6' , "selected='selected'" ) ; break ; 		
 case (7) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_7' , "selected='selected'" ) ; break ; 		
 case (8) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_8' , "selected='selected'" ) ; break ; 		
 case (9) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_9' , "selected='selected'" ) ; break ; 		
 case (10) : $template->assign_var('RECRUITER_NUMBER_TEACHERS_10' , "selected='selected'" ) ; break ;  		 		
 default : break ; 
}
		
?>