<?php

/**
 * This class will use SOAP requests to get our SERP position.
 *
 *                             search_engine.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

// If your do not have access to php.ini, set the include path to use the Pear Class.
// ini_set( 'include_path', '.:/home/yourname/pear:/usr/local/lib/php');
// PEAR:SOAP Cient Class
require_once('SOAP/Client.php');

// make this script into a class and switch the MSN bit from PHP soap to NuSoap
class search_engine 
{

  // build your construtor here
  function search_engine()
  {

    // script does not expire.
    set_time_limit(0);

    $this->myUrl = 'englishteachingkorea.com';
    $this->results = '100' ;
    $this->google_key = 'lxyF/51QFHItlziTysdmi3cM6IM/4I7R' ;
    $this->yahoo_key = '987345123' ;
    $this->msn_key = 'A6E150B422C6589922693533360EA5881D9DAF0B' ;
    $this->loop = 'true';

  }

  // find the google position...
  function google_serp($keywords)
  {

    // various variables
    $start = 0;
    $maxResults = 10;
    $filter = false;
    $restrict = '';
    $safeSearch = false;
    $lr = '';
    $ie = '';
    $oe = '';
    $output = '';

    // keep doing google searches until they respond
    while ($this->loop) 
     {

       // Instantiate the SOAP_WSDL Class. 
       $wsdl = new SOAP_WSDL('googleapi/GoogleSearch.wsdl');
    
       // Get the WSDL Proxy Class 
       $soapclient = $wsdl->getProxy();

       // Start Google API query
       $result = $soapclient->doGoogleSearch($this->google_key,$keywords,$start,$maxResults,$filter,$restrict,$safeSearch,$lr,$ie,$oe);

       // If error occurs
       if (PEAR::isError($result)) 
 	{

 	  $message = $result->message;

 	  echo "An error occured: $message<p>";

 	  // pause for 15 secs
 	  sleep(15);

 	  // if too much query, quit loop
 	  if (preg_match("/Daily limit/i", $message)) 
 	    {
 	      $this->loop = false;
 	    }
 	  // if invalid key, quit loop
 	  else if (preg_match("/Invalid/i", $message))
 	    {
 	      $this->loop = false;
 	    }
 	  // if normal timeout message, continue
 	  else {
 	    	    echo "Retrying in 15 secs. <br/>";
 	  }

 	}
       // if no errors, check occurrence of your url in the results.
       else 
 	{

 	  $desc = "";

 	  // Step up the starting record for the next loop
 	  $start += $maxResults;

 	  // the actual ranking
 	  $position = $start- $maxResults;

 	  // Search for url
 	  return $this->urlSearch($position, $result, $keywords);

	}
     }
  }

  // find your url in the google response
  function urlSearch($position, &$result,$keywords) 
    {

      // Get total results from the search
      $count =$result->estimatedTotalResultsCount;

      // Get the resultElements class
      $elements = $result->resultElements;
      
      // if searched results reach limit set by you, quit loop
      if ($position == $this->results) 
	{

	  // echo "You have restricted the search results to $resultLimit";
	  $this->loop = false;

	}
      else if ($count > 0) 
	{

	  foreach ($elements as $item) 
	    {

	      $position++;

	      // if the url is found, display it and quit loop
	      if (preg_match("/$this->myUrl/i", $item->URL)) 
		{


	      echo preg_match("/$this->myUrl/i",$item->URL);
		  $size = $item->cachedSize;
		  $title = $item->title;
		  $url = $item->URL;
		  $snippet = $item->snippet;

		  $desc .= "<p>$title [Cache Size: $size]<br/> <a href=\"$url\" target=\"_blank\">$url</a><br />";
		  $desc .= "$snippet</p>";

		  $google_serp_result = "Google position is #{$position} for keywords $keywords";
		  
		  if (isset($position))
		    {
		      return $google_serp_result ;
		    }
		  else 
		    {
		      return "Google position is #NA for keywords $keywords";
		    }

		  // break out of the while loop
         	  $this->loop = false;			

		}

	    }
	}
    // if google returns no result, quit loop
      else 
	{
	  $output = "No results found for the keyword \"$keywords\"";
	  $this->loop = false;
	}
    }// end of Url function


  function yahoo_serp($keywords)
  {

    // fictional URL to an existing file with no data in it (ie. 0 byte file)
    $ur="http://search.yahooapis.com/WebSearchService/V1/webSearch?appid=".$this->yahoo_key."&query=".$keywords."&results=".$this->results;

    $curl = curl_init();
  
    curl_setopt($curl, CURLOPT_URL, $ur);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);

    // execute and return string (this should be an empty string '')
    $str = curl_exec($curl);

    curl_close($curl);

    // the value of $str is actually bool(true), not empty string ''
    $yahoo = explode("\n\n",$str);

    foreach ( $yahoo as $key => $value )
      {
    
	if (stristr($value,$this->myUrl))
	  {
	    
	    $yahoo_serp_result = 'Yahoo position is #'.++$key.' for keywords '.$keywords ;
	    return $yahoo_serp_result;

	  }

      }

  }

// Do the MSN thing here ...
// The NuSOAP better working code is here
// http://www.cricava.com/blogs/mariano.php?blog=6&title=msn_search_api_php_client&more=1&c=1&tb=1&pb=1

  function msn_serp($keywords)
  {

    $request = array('Request' => array(
					'AppID' => $this->msn_key,
					'Query' => $keywords,
					'CultureInfo' => 'en-US',
					'SafeSearch' => 'Strict',
					'Flags' => '',
					'Location' => '',
					'Requests' => array(
							    'SourceRequest' => array(
										     'Source' => 'Web',
										     'Offset' => 0,
										     'Count' => $this->results,
										     'ResultFields' => 'All'))));

    $soapClient = new SoapClient("http://soap.search.msn.com/webservices.asmx?wsdl");
    $result = $soapClient->Search($request);

    for ( $i=0 ; $i < count($result->Response->Responses->SourceResponse[0]->Results->Result) ; $i++ )
      {

	if (stristr($result->Response->Responses->SourceResponse[0]->Results->Result[$i]->Url,$this->myUrl))
	  {
	    $j = ++$i ;
	    
	    $msn_serp_result = "MSN position is #$j for keywords $keywords";
	    return $msn_serp_result;
	  }
      }

  }  

}
?>

