<?php

/**
 * This is a class to read the action list stored in the cookie of the recruiters browser. It will parse out the actions and store them in a database in a suitable format. It will also pull these values out and present them in a proper format to the administrator, who will then be able to select and search through them...
 *
 *                             action_monitor.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

class action_monitor {

  /**
   * action_monitor constructor
   *
   */
  function action_monitor()
  {
    // constructor
  }

  /**
   * cookie_reader: basically examines the user's cookies and stores any actions that the user has made in the last 15mins or so
   *
   */
  function cookie_reader($cookie)
  {

    global $db;

    // take in cookie from XMLHttpRequest
    // parse out the values and store in db
    // user + time + action + id
    $arr = Array();
    $arr = explode('&lt;-ETK-&gt;',$cookie);

    // the first and last are unused
    array_shift($arr);
    array_pop($arr);

    // count through the cookie and insert values into db..
    for ( $i=0 ; $i < (count($arr)/5)  ; $i++ )
    {

          $j=($i*5);
	  
	  $sql = "INSERT INTO employee_tracker (user,time,timezone,action,data) VALUES ('${arr[$j]}','${arr[++$j]}','${arr[++$j]}','${arr[++$j]}','${arr[++$j]}')";

	  if (!($result = $db->sql_query($sql)) ) 
	    {
	      message_die(CRITICAL_ERROR,'Problem with the employee tracker table','',__LINE__,__FILE__,$sql);
	    }       

    }

  }

}

?>