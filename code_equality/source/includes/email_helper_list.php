<?php

/**
 *  Fill out the email helper list
 *
 *                            email_helper_list.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}
        
// all this does is an array string replace, mostly because I haven't seen any viable
// subtemplate workaround hack 
$search_array = array('{REPLY_TITLE0}','{REPLY_TITLE1}','{REPLY_TITLE2}',
		      '{REPLY_TITLE3}','{REPLY_TITLE4}','{REPLY_TITLE5}');

$replace_array = array($page_maker['reply_text0_title'],$page_maker['reply_text1_title'],
		       $page_maker['reply_text2_title'],$page_maker['reply_text3_title'],
		       $page_maker['reply_text3_title'],$page_maker['reply_text5_title']);

$email_helper_list = str_replace($search_array,$replace_array,$email_helper_list);

// get the name from the name value
$_GET['name']=first_name_getter($_GET['name']);

$template->assign_vars( 
		       array( 
			     'EMAIL_HELPER_LIST' =>$email_helper_list,
			     'REPLY_TEXT0'=>reply_text_replace($page_maker['reply_text0'],$_GET['name']),
			     'REPLY_TITLE0'=>$page_maker['reply_text0_title'],
			     'REPLY_TEXT1'=>reply_text_replace($page_maker['reply_text1'],$_GET['name']),
			     'REPLY_TITLE1'=>$page_maker['reply_text1_title'],
			     'REPLY_TEXT2'=>reply_text_replace($page_maker['reply_text2'],$_GET['name']),
			     'REPLY_TITLE2'=>$page_maker['reply_text2_title'],
			     'REPLY_TEXT3'=>reply_text_replace($page_maker['reply_text3'],$_GET['name']),
			     'REPLY_TITLE3'=>$page_maker['reply_text3_title'],
			     'REPLY_TEXT4'=>reply_text_replace($page_maker['reply_text4'],$_GET['name']),
			     'REPLY_TITLE4'=>$page_maker['reply_text4_title'],
			     'REPLY_TEXT5'=>reply_text_replace($page_maker['reply_text5'],$_GET['name']),
			     'REPLY_TITLE5'=>$page_maker['reply_text5_title'],
			     'REPLY_TEXT6'=>reply_text_replace($page_maker['reply_text6'],$_GET['name']),
			     'REPLY_TITLE6'=>$page_maker['reply_text6_title'],
			     'REPLY_TEXT7'=>reply_text_replace($page_maker['reply_text7'],$_GET['name']),
			     'REPLY_TITLE7'=>$page_maker['reply_text7_title'],
			     'REPLY_TEXT8'=>reply_text_replace($page_maker['reply_text8'],$_GET['name']),
			     'REPLY_TITLE8'=>$page_maker['reply_text8_title'],
			     'REPLY_TEXT9'=>reply_text_replace($page_maker['reply_text9'],$_GET['name']),
			     'REPLY_TITLE9'=>$page_maker['reply_text9_title'],
			     'REPLY_TEXT10'=>reply_text_replace($page_maker['reply_text10'],$_GET['name']),
			     'REPLY_TITLE10'=>$page_maker['reply_text10_title'],
			     'REPLY_TEXT11'=>reply_text_replace($page_maker['reply_text11'],$_GET['name']),
			     'REPLY_TITLE11'=>$page_maker['reply_text11_title'],
			     'REPLY_LINK0'=>reply_text_replace($page_maker['reply_link0'],$_GET['name']),
			     'REPLY_LINK_TEXT0'=>$page_maker['reply_link_text0'],
			     'REPLY_LINK1'=>$page_maker['reply_link1'],
			     'REPLY_LINK_TEXT1'=>$page_maker['reply_link_text1'],
			     'REPLY_LINK2'=>$page_maker['reply_link2'],
			     'REPLY_LINK_TEXT2'=>$page_maker['reply_link_text2'],
			     'REPLY_LINK3'=>$page_maker['reply_link3'],
			     'REPLY_LINK_TEXT3'=>$page_maker['reply_link_text3'],
			     'REPLY_LINK4'=>$page_maker['reply_link4'],
			     'REPLY_LINK_TEXT4'=>$page_maker['reply_link_text4'],
			     'REPLY_LINK5'=>$page_maker['reply_link5'],
			     'REPLY_LINK_TEXT5'=>$page_maker['reply_link_text5']
			     ) 
		       );

?>