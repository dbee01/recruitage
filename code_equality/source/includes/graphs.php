<?php

/**
 * This graphs class is used to build graphs to show the hit usage and visitor stats
 *
 *                               graphs.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

// this page is going to be called on it's own
Header( 'Content-type: image/gif');

// include the config file here for the db stuff
require('../config.php');

class graphs {  

  // establish width, height and text
  var $im_width ;
  var $im_height ;
  var $title_text ;
  var $image ;
  var $xaxis_gap ;
  var $yaxis_gap ;
  var $titlefont;
  var $x_border ;
  var $y_border ;
  var $graph_height ;
  var $graph_width;
  var $places;
  var $xaxis_font ;
  var $keywords ;
  var $search_array ;
  var $xbar ;
  var $bw_scale ;
  var $hit_scale ;
  var $unique_visitor_scale ;

  // Allocate some colors
  var $white ;
  var $red ;
  var $grey ;
  var $black ;
  var $blue ;
    
  /**
   * Graphs constructor will take in alot of parameters
   *
   */
  function graphs($title_par,$im_height_par,$im_width_par,$x_border_par,$y_border_par,$xaxis_progression_par,$yaxis_lines,$graph_height_par,$graph_width_par,$places)
  {

    // draw background image
    $this->image = imagecreate($im_width_par,$im_height_par);

    // allocate colors first color set, sets the color of the imag
    $this->grey = ImageColorAllocate($this->image,200,200,200);
    $this->white = ImageColorAllocate($this->image,255,255,255);
    $this->red = ImageColorAllocate($this->image,255,0,0);

    $this->black = ImageColorAllocate($this->image,0,0,0);
    $this->blue = ImageColorAllocate($this->image,0,0,255);

    // image width and height
    $this->im_width = $im_width_par ; // 300
    $this->im_height = $im_height_par ; // 221

    // set the borders
    $this->x_border = $x_border_par;
    $this->y_border = $y_border_par;
      
    // set y-axis text position 
    $this->yaxis_text_pos=$im_height_par - 17;

    // scale your bar graphs ...
    $this->bw_scale=1000;
    $this->hit_scale=80000;
    $this->unique_visitor_scale=1000;

    // height and width
    $this->graph_height = $graph_height_par ;
    $this->graph_width = $graph_width_par ;

    // progression for the bars
    $this->xbar_gap = 5 ;

    // set x-axis and y-axisprogression
    $this->xaxis_gap=$xaxis_progression_par;
    $this->yaxis_gap=$this->graph_height / $yaxis_lines ;

    // how many places does the graph cover 
    $this->places = $places ;

    global $dbhost;
    global $dbuser;
    global $dbpasswd;
    global $dbname;      

    // assign the global db variables
    $this->dbhost = $dbhost;
    $this->dbuser = $dbuser;
    $this->dbpasswd = $dbpasswd;
    $this->dbname = $dbname;

    $link = mysql_connect($this->dbhost, $this->dbuser, $this->dbpasswd)
      or die('Could not connect: ' . mysql_error());

    mysql_select_db($this->dbname) or die('Could not select database');
      

    // title text
    $this->title_text = $title_par ;

    // Create the imagestring title ...
    $this->titlefont = 3;

    // Create the x-axis font
    $this->xaxis_font = 1;
      
  }

  /**
   * Read the coordinates from in from a file
   *
   */
  function read_coordinates($search_array,$keywords)
  {

    // store the search array, that'll help you sub group the results later on
    $this->search_array = $search_array ;

    // store the keywords
    $this->keywords = $keywords ;

    // make array to store the results
    $point_arr = array();
       
    foreach( $search_array as $query )
      {

	// the values would look like ... seo_google_apr_06 : 1
	$sql = "SELECT * FROM " . LINKSTATS . " WHERE linkstats_name LIKE '$query%' AND linkstats_keywords = '$this->keywords' " ;

	$result = mysql_query($sql) or die('Query failed: ' . mysql_error());

	while ($line = mysql_fetch_array($result, MYSQL_NUM)) 
	  {
	    $point_arr[] = $line[0] ;
	    $point_arr[] = $line[1] ;
	  }

      }

    return $point_arr;

  }

  /**
   * Read the logs for bw, hits, unique visitors
   *
   */
  function read_logs()
  {

    $sql_logs = "SELECT linkstats_name, linkstats_pos FROM " . LINKSTATS . " WHERE linkstats_name LIKE 'log_%' ";

    // the values would look like ... seo_google_apr_06 : 1
    $result_log = mysql_query($sql_logs) or die('Query failed: ' . mysql_error());
      
    // store the log results in array
    $log_arr = array();

    while ($line_log = mysql_fetch_array($result_log, MYSQL_NUM)) 
      {
	$log_arr[] = $line_log[0] ;
	$log_arr[] = $line_log[1] ;
      }

    return $log_arr;

  }

  /**
   * Draw the bar graph...
   *
   */
  function bar_graph($stat_array)
  {

    //    array(6) { [0]=>  string(13) "log_bw_oct_06" [1]=>  string(3) "269" [2]=>  string(15) "log_hits_oct_06" [3]=>  string(5) "24499" [4]=>  string(26) "log_unique_visitors_oct_06" [5]=>  string(3) "622" }    

    // start the first bar 
    $xbar = 10;
    $ybar = $this->graph_height - 100 ;

    // OUTLINE THE X_AXIS find the dates from every 6th element, 
    for ($i=0 ; $i<(count($stat_array)); $i+=6 )
      {
	// match the date
	preg_match('/log_[a-z]*_(.*)/',$stat_array[$i],$matches);
	$y_label = $matches[1];

	// draw the x-axis string
	imagestring($this->image, $this->titlefont, ($x_pos + 5), ($this->graph_height - 35), $y_label , $this->black);
	$x_pos += 60 ;	

	// scale your bw ... max is say 500MB
	$bw = -(($stat_array[$i+1]/$this->bw_scale)*160 - 180);

	// scale your hits ...
	$hits = -(($stat_array[$i+5]/$this->hit_scale)*160 - 180);

	// scale your unique visitors ...
	$unique_visitor = -(($stat_array[$i+3]/$this->unique_visitor_scale)*160 - 180);

	// draw the graph lines
	imagefilledrectangle($this->image,$xbar,$bw,($xbar+$this->xbar_gap),180,$this->red);
	$xbar += $this->xbar_gap ;
	imagefilledrectangle($this->image,$xbar,$hits,($xbar+$this->xbar_gap),180,$this->blue);
	$xbar += $this->xbar_gap ;
	imagefilledrectangle($this->image,$xbar,$unique_visitor,($xbar+$this->xbar_gap),180,$this->black);
	$xbar += 50 ;
      }
    
    // pixel-width of title
    $txtsz = imagefontwidth($this->titlefont) * strlen($this->title_text);
    // height margin
    $hmargin = 10;
    // xsize
    $xsize = $this->graph_width ;

    $xpos = (int)($hmargin + ($xsize - $txtsz)/2); // center the title
    $xpos = max(1, $xpos); // force positive coordinates
    $ypos = 3; // distance from top

    imagestring($this->image, $this->titlefont, $xpos, $ypos, $this->title_text , $this->black);

    // output the GIF to the browser and free up memory
    ImageGIF($this->image);
    ImageDestroy($this->image);
    
  }

  /** 
   * Draw a line graph 
   *
   */ 
  function line_graph($point_array)
  {

    // number of engines ...
    $no_of_engines = count($search_terms)+1;
    
    // keeping these as fairly standard
    $date_height=$this->graph_height + 10;

    // Create an initial grey rectangle for us to draw on
    $graph_width=$this->im_width ;

    //      echo "$this->image,$this->x_border,$this->y_border,$this->graph_width,$this->graph_height,$this->grey";

    // Fill the image in with a rectangle
    ImageFilledRectangle($this->image,$this->x_border,$this->y_border,$graph_width,$this->graph_height,$this->grey);

    // set the yaxis progression gap
    $line_height = $this->yaxis_gap;

    // Draw the horizontal lines and y labels
    for ($i=(($this->y_border)) ; $i<=$this->graph_height ; $i+=$line_height )
      {
	ImageLine($this->image,$this->x_border,$i,$this->graph_width,$i,$this->black);
      }

    // you might want to feed this array into the function
    $y_labels = array('1st','5th','10th','15th','20th','25th','30th','35th','40th','45th');
      
    // set y position ...
    $y_pos =  $this->yaxis_gap ; 
 
    // PLACE Y-LABELS along the y-axis
    foreach ($y_labels as $label)
      {
	// you want to put the y-labels a little under the corresponding line
	imagestring($this->image, $this->titlefont, 0, ($y_pos - ($line_height/2) ), $label , $this->black);
	$y_pos += $this->yaxis_gap ;
      }
      
    // PLOT POINTS OF THE GRAPH
    $x_axis = $this->x_border ;

    // select colors here
    $color_array = array($this->blue,$this->black,$this->red);
	 
    // use the keywords in the search array to sub group
    foreach ($this->search_array as $search_text)
      {

	// bring all the points into subarrays 
	// just google, just yahoo etc ...
	$sub_array = array();

	for ( $i=0; $i<count($point_array) ; $i++ )
	  {

	    if (stristr($point_array[$i],$search_text))
	      {
		$sub_array[] = $point_array[$i] ;
		$sub_array[] = $point_array[($i+1)] ;
	      }

	  }

	for ($i=1;$i<(count($sub_array)-1);$i+=2)
	  {

	    // adjust for ratio of height graph and border ...
	    $y0 = ( $sub_array[$i] * ( $this->im_height / $this->places ) + $this->y_border -2 ) ;
	    $y1 = ( $sub_array[$i +2] * ( $this->im_height / $this->places ) + $this->y_border -2) ;
	      
	    ImageLine($this->image,$x_axis,$y0,($x_axis + $this->xaxis_gap),$y1 ,$color_array[$w++]);
	    $x_axis += $this->xaxis_gap ;
	      
	  }

	// assign the bottom labels ...
	// first date is $month_width across
	$x_axis=$this->x_border;

      }
      
    // go through array and feed out ...
    for( $i=0 ; $i<(count($sub_array)); $i+=2 )
      {

	preg_match('/seo_[a-z]*_(.*)/',$sub_array[$i],$matches);
	imagestring($this->image, $this->xaxis_font, ( $x_axis - 13 ), $this->yaxis_text_pos, $matches[1], $this->black);
	$x_axis += $this->xaxis_gap ;

      }
      
    // reset the xaxis for the next function
    $x_axis = 0 ;

    // set the title of the graph ...

    // pixel-width of title
    $txtsz = imagefontwidth($this->titlefont) * strlen($this->title_text);
    // height margin
    $hmargin = 10;
    // xsize
    $xsize = $this->graph_width ;

    $xpos = (int)($hmargin + ($xsize - $txtsz)/2); // center the title
    $xpos = max(1, $xpos); // force positive coordinates
    $ypos = 3; // distance from top

    imagestring($this->image, $this->titlefont, $xpos, $ypos, $this->title_text , $this->black);

    // output the GIF to the browser and free up memory
    ImageGIF($this->image);
    ImageDestroy($this->image);

  }

  /** 
   * Destruct this object
   *
   */
  function __destruct() 
  {
    // this will destruct object when all references to it are gone

  }

}

/**
 * If it's called with a mode, then we actually build some graphs
 *
 */
if (isset($_GET['mode']))
{

  $search_engines = array("seo_google","seo_yahoo","seo_msn");
  $keywords = $_GET['keywords'];

  // title, im_width, im_height, x_border, y_border, xaxis_progress, yaxis_lines, graph_height, graph_width
  // graph object ...

  switch($_GET['mode'])
    {

      // to track the seo position and graph it...
    case ('seo_track') : 
      $title = "$keywords ... google(Black), yahoo(Red), MSN(Blue)";
      $seo_graph = new graphs($title,240,300,30,20,40,10,220,300,50);
      $search_arr = $seo_graph->read_coordinates($search_engines,$keywords);
      $seo_graph->line_graph($search_arr);
      break;

      // to track site usage, BW per month ...
    case ('log_track') : 
      $bar_graph = new graphs("bar graph",200,600,30,20,40,10,220,300,50);
      $search_arr = $bar_graph->read_logs();
      $bar_graph->bar_graph($search_arr);
      break; 

    default : break ;

    }

}



?>