<?php

/**
 * This class handles all the mail for the application
 *
 *                               imailer.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

require_once 'includes/attachment.php';

class imailer {

  // imap connection details ...
  var $server ;
  var $user;
  var $pass;
  var $conn = "" ;
 
  /**
   * Build the constructor, it needs to be given a user_reg to find the vhost mailbox. Also it needs imap strings...
   *
   */
  function imailer($user_reg = '')
  {

    global $imap_server;
    global $imap_user;
    global $imap_pass;
    global $userdata;
    global $board_config;

    // virtual mailbox path
    $interface = new interface_handler();    

    $user_id == '' ? $vhost=$userdata['username'] : $vhost= $interface->user_reg_to_name($user_reg)  ;

    $virtual_mail_path = '/var/spool/mail/vhosts/'.strtolower($board_config['sitename']).'/'.$vhost;

    $this->server = $imap_server.$virtual_mail_path;

    $this->user = $imap_user ;
    $this->pass = $imap_pass ;

    // log into the imap server
    if ( ( $this->conn = imap_open($this->server, $this->user, $this->pass) ) == FALSE )
      {
	echo 'Error opening imap object' ;
      }

  }

  /**
   * Take a mail_id, figure out mime type and encoding, and whether there is an attachment. Return all processed data..
   *
   */
  function mail_parser($mid)
  {
    
    $mbox=$this->conn;

    $struct = imap_fetchstructure($mbox, $mid, FT_UID);

    $parts = $struct->parts;

    // setting this to 1 cause I'm getting php errors
    $i = 1;

    /* Simple message, only 1 piece */
    if (!$parts) 
      { 
	$attachment = array(); /* No attachments */
	$content = imap_body($mbox, $mid, FT_UID); 	
      } 
    else 
      { /* Complicated message, multiple parts */

	$endwhile = false;
      
	$stack = array(); /* Stack while parsing message */
	$content = "";    /* Content of message */
	$attachment = array(); /* Attachments */
      
	// put every single part of the messgae into an array called parts
        while (!$endwhile) 
	  {
	    // parts of the struct object
	    if (!$parts[$i]) 
	      {
		// not sure about this part
		if (count($stack) > 0) 
		  {
		    $parts = $stack[count($stack)-1]["p"];
		    $i    = $stack[count($stack)-1]["i"] + 1;
		    array_pop($stack);
		  } 
		else 
		  {
		    $endwhile = true;
		  }
	      }

	    // store the attachment numbers in a partstring
	    if (!$endwhile) 
	      {
	     	  /* Create message part first (example '1.2.3') */
	     	  $partstring = "";
	     	  foreach ($stack as $s) 
		    {
		      $partstring .= ($s["i"]+1) . ".";
	     	    }
	     	  $partstring .= ($i+1);

	     	  // if that part is an attachment, store it in the attachment file
	     	  if (strtoupper($parts[$i]->disposition) == "ATTACHMENT") { /* Attachment */

	     	    $attachment[] = array("filename" => $mid.(imap_utf8($parts[$i]->parameters[0]->value)),
	     				  "type" => $parts[$i]->type,
	     				  "encoding" => $parts[$i]->encoding,
	     				  "filedata" => imap_fetchbody($mbox, $mid, $partstring, FT_UID)
	     				  );

		    --$i; 

		    // determine attachment characteristics
    	     	    switch($attachment[($i)]["type"])
	     	      {

 	     	        case (0) : $attachment[$i]["type"] = "text/" ; break;
 	     	        case (1) : $attachment[$i]["type"] = "multipart/"  ; break;
 	     	        case (2) : $attachment[$i]["type"] = "message/"  ; break;
 	     	        case (3) : $attachment[$i]["type"] = "application/"  ; break;
 	     	        case (4) : $attachment[$i]["type"] = "audio/"  ; break;
 	     	        case (5) : $attachment[$i]["type"] = "image/"  ; break;
 	     	        case (6) : $attachment[$i]["type"] = "video/"  ; break;
 	     		case (7) : $attachment[$i]["type"] = "other/" ; break;

 	     	        default: break;

 	     	      }

 	     	    switch($attachment[$i]["encoding"])
 	     	      {

	     	        case (1) : $attachment[$i]["filedata"] = imap_8bit($attachment[$i]["filedata"])  ; break;
 	     	        case (2) : $attachment[$i]["filedata"] = imap_binary($attachment[$i]["filedata"])  ; break;
 	     	        case (3) : $attachment[$i]["filedata"] = imap_base64($attachment[$i]["filedata"])  ; break;
 	     	        case (4) : $attachment[$i]["filedata"] = quoted_printable($attachment[$i]["filedata"])  ; break;
	 
 	     	        default: break;

 	     	      }

	     	    if ( $attachment[$i]["type"] == "image/" )
	     	      {
	     		    $im = imagecreatefromstring($attachment[$i]["filename"]);
                            imagejpeg($im, $attachment[$i]["filename"]);
			    
			    if (!copy($attachment[$i]["filename"], $file)) 
			      {
				//echo "failed to copy image $file...\n";
			      }
	    	      }

		    $i++;

	    	  } 
		  /* A multipart plain message... */
	          elseif (strtoupper($parts[$i]->subtype) == "PLAIN") 
	     	  { 

	    	    $content .= imap_fetchbody($mbox, $mid, $partstring, FT_UID);

		    // if content is base64
		    if ($parts[$i]->encoding == 3)
		      {
			$content = imap_base64($content) ;
		      }
	     	  }

	     	}

          	// clean the file, decode it ...
	     	if ($parts[$i]->parts) 
	     	{
	    	  $stack[] = array("p" => $parts, "i" => $i);
	     	  $parts = $parts[$i]->parts;
	     	  $i = 0;
	     	} 
	        else 
	    	{
	     	  $i++;
	    	}

	  } /* while */
      } /* complicated message */


      $ret_array = array();
      $ret_array[] = $content;
 
      for($i=0;$i<count($attachment);$i++)
      {

          $attachment[$i]['filename']=preg_replace('/ /','',$attachment[$i]['filename']);
          $filename=$attachment[$i]['filename'];
      	  $ret_array[] = $attachment[$i];

	  // transfer file into the uploads folder...
	  $file = "uploads/".$filename ;

   	  if (!$handle = fopen($file, 'w')) {
   	    echo "Cannot open file ($file)";
   	    exit;
   	  }

   	  if (fwrite($handle, $attachment[$i]["filedata"]) === FALSE) {
     	    echo "Cannot write to file ($file)";
     	    exit;
     	  }
  
     	  fclose($handle);

      }   

      return $ret_array;
    
  }
  
  /** 
   * Grab the email info from the mailbox, give a uid.
   * 
   * 
   */
  function email_reply($id)
  {

    global $userdata; 

    // grab the mail headers
    $mailHeader = @imap_headerinfo($userdata['username'], $id, FT_UID);

    $from = $mailHeader->from;

    // handy way to extract human readable info from headerinfo object
    // foreach ($from as $id => $object)
    // {

    $fromname = $object->personal;
    $fromaddress = $object->mailbox . "@" . $object->host;

    // }

    //      find out if we've had an email from that person before.
    $emails = imap_search($this->conn,"FROM $fromaddress");

    // find the structure of this message
    $struct = imap_fetchstructure($this->conn, $id);

    $parts = $struct->parts;

    if ($struct->encoding == 3)
      {
	// pull out the email text
	$body = imap_base64(imap_fetchbody($this->conn,$id,1,FT_UID));
      }
    else {
	$body = imap_fetchbody($this->conn,$id,1,FT_UID);
    }

    $body = nl2br(strip_tags($body));

    return $body ;

  }

  /**
   * Read the body of the email
   *
   */
  function read_body($id)
  {

    // read the body of the sequence id, not the message id
    $body = nl2br(strip_tags(imap_body($this->conn,$id,FT_UID)));

    return $body ;
   
  }

  /**
   * Pull the headers of the emails out, display them on control_panel
   *
   */
  function find_headers()
  {

    $headers = @imap_headers($this->conn) ;

    $numEmails = sizeof($headers);

    // for each email in the box
    for($i = 1; $i < $numEmails+1; $i++)
      {

	$mailHeader = @imap_headerinfo($this->conn, $i, 80, 80);

	// use a unique id instead of a sequence number
        $id .= imap_uid($this->conn,$mailHeader->Msgno) . '<br>' ;

	$from .= imap_utf8($mailHeader->fromaddress) . '<br>' ;
	$subject .= imap_utf8(strip_tags($mailHeader->fetchsubject)) . '<br>' ;
	$date .= $mailHeader->date . '<br>' ;
      }

    // return it all in a concatenated array
    $mail_array = array('from'=>$from, 'subject'=>$subject, 'date'=>$date, 'message_id'=>$id );

    return $mail_array ;

  }

  /**
   * Take a uid and delete that email
   *
   */
  function delete_message($id)
  {

    // delete that uid
    if ( $result = imap_delete($this->conn, $id, FT_UID) )
      {

	if ( imap_expunge ($this->conn) )
	  {
	    echo "Deleteded Message $id";
	  }
	else {
	  
	  return 'Could not expunge message' ;
	  
	}
	
      }
    else {
 
      return 'Could not delete message';

    }

  }

  /** 
   * Destruct this object
   *
   */
  function __destruct() 
  {
  
    imap_close($this->conn);
  
  } 

}

?>