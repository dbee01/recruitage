<?php

/**
 * Crappy imported function. Rewrite this.
 *
 *                               xml2array.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

/* Usage
 Grab some XML data, either from a file, URL, etc. however you want. Assume storage in $strYourXML;

 $objXML = new xml2Array();
 $arrOutput = $objXML->parse($strYourXML);
 print_r($arrOutput); //print it out, or do whatever!
 
*/

// replace this with a native xml handler function...

class xml2Array {
  
  var $arrOutput = array();
  var $resParser;
  var $strXmlData;
  
  /**
   * Parse the XML input string
   *
   */
  function parse($strInputXML) {
  
    $this->resParser = xml_parser_create ();
    xml_set_object($this->resParser,$this);
    xml_set_element_handler($this->resParser, "tagOpen", "tagClosed");
          
    xml_set_character_data_handler($this->resParser, "tagData");
      
    $this->strXmlData = xml_parse($this->resParser,$strInputXML );

    if(!$this->strXmlData) {
      die(sprintf("XML error: %s at line %d",
		  xml_error_string(xml_get_error_code($this->resParser)),
		  xml_get_current_line_number($this->resParser)));
    }
                          
    xml_parser_free($this->resParser);
          
    return $this->arrOutput;
  }

  /**
   * Open the tags
   *
   */
  function tagOpen($parser, $name, $attrs) {
    $tag=array("name"=>$name,"attrs"=>$attrs);
    array_push($this->arrOutput,$tag);
  }

  /**
   * Parse the tag data
   *
   */
  function tagData($parser, $tagData) {         
    $last_element=count($this->arrOutput)-1;         
    $this->arrOutput[$last_element]['children'][] = array("textnode",$tagData);     
  }
    
  /**
   * Tag closed
   *
   */
  function tagClosed($parser, $name) {
    $this->arrOutput[count($this->arrOutput)-2]['children'][] = $this->arrOutput[count($this->arrOutput)-1];
    array_pop($this->arrOutput);
  }

}

?>