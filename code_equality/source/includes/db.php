<?php

/**
 * Build the db classes and make a connection
 *
 *                                  db.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}

// include code here for the db wrappers...
include($phpbb_root_path . 'db/mysql.'.$phpEx);

// Conect to the logging db ...
$db_log = new sql_db($db_log_host,$db_log_user,$db_log_passwd,$db_log_name,false);

if(!$db_log->db_connect_id)
{
	message_die(GENERAL_MESSAGE, "Could not connect to the database");
}

// Make the regular database connection...
$db = new sql_db($dbhost, $dbuser, $dbpasswd, $dbname, false);

if(!$db->db_connect_id)
{
	message_die(CRITICAL_ERROR, "Could not connect to the database");
}

?>