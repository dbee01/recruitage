<?php

/**
 * This does alot of the heavy lifting, both in terms of the matching from the control_panel, but also handles all the 'predictive intelligence'. It will take all the forms sent to candidates and clients and it will update the db automatically
 *
 *                          control_panel_match.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
  die("Hacking attempt");
}

// this'll set the date variable for all the notes_append dates
$na_date = date('M j'); 

// This bit is up to the recruiter to decide whether or not it's worth sending the
// job to begin with ... The matches come up on the control_panel but for some reason the
// recruiter knows that it's not worth trying ...
if(isset($_POST['sent_match_delete']))
{
  
  // security check..
  $_POST = array_map("input_check",$_POST);

  @extract($_POST);

  // take job value from school or recruiter and name them job_id
  if ( isset($school_jobs_id) )
    {
      $job_id = $school_jobs_id ;
    }
  else if ( isset($recruiter_jobs_id) )
    {
      $job_id = $recruiter_jobs_id ;
    }  
    
  $sql="INSERT INTO " . SENT_MATCH . " ( teacher_id , job_id, deleted ) VALUES ('$teacher_id', '$job_id', 1)";
       
  if(!($result=$db->sql_query($sql)))
    {
      message_die(GENERAL_ERROR,'Could not connect to sent match table','',__LINE__,__FILE__,$sql);
    }
		
  return ;

  // I guess the 'not interested' email would go about here...

}

// handle recruiter response form. Will the recruiter accept the job candidate ?
// add note to recruiter notes, update the sent match table
if(isset($_POST['recruiter_response_form']))
{

  $_POST = array_map("input_check",$_POST);

  @extract($_POST);

  if($recruiter_response_form == 1)
    {
		
      $notes_text = $recruiter_name.$lang['Wants_interview'].$teacher_name;
      $notes_append->append_teachers($teacher_id,$notes_text);
      $notes_append->append_recruiters($recruiter_jobs_id,$notes_text);
	
    }
  else if($recruiter_response_form == 0)
    {
		
      $notes_text = "$recruiter_name".$lang['Not_interested']."$teacher_name";
      $notes_append->append_teachers($teacher_id,$notes_text);
      $notes_append->append_recruiters($recruiter_jobs_id,$notes_text);
	
    }
	
  // if there is a positive response but the recruiter didn't leave a comment.
  // then add a comment anyways to know that the response has been successful
  if ( ( $recruiter_response_form == 1 ) && ( $job_notes == "" )  )
    {
      $job_notes=$lang['Interested'];
    }

  // has this school give the user a manual indication that 
  // they are intersted in the school. No sent match has then been sent.
  if ( isset($_POST['from_recruiter_gallery']) )
    {

      // special id_to_name function. I don't want to take name info from client side
      $teacher_name = id_to_name($teacher_id,'teachers');
      $recruiter_name = id_to_name($recruiter_jobs_id,'school_jobs');
	    
      $teacher_notes = $teacher_name . $lang['Interested'] . $recruiter_name ; 
	  
      // if this match is already in the sent_match,
      // if the school has already expressed an interest
      // then just update it
      $sql = "SELECT * FROM " . SENT_MATCH . " WHERE teacher_id = '$teacher_id' AND job_id = '$recruiter_jobs_id' ";

      if(!($result=$db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql);
	}

      // is it in the sent_match table already ?
      $row = $db->sql_fetchrow($result);

      if ( $row == '' )
	{
	  // if it's sent from the recruiter gallery - then the original sending email was never sent out
	  // and so we insert the value instead of updating it.
	  $sql_sent_match="INSERT INTO " . SENT_MATCH . " (job_response, job_notes, deleted, teacher_id, job_id) 
                    VALUES ( '$recruiter_response_form', '$job_notes', 0, '$teacher_id', '$recruiter_jobs_id' )";

	}
      else 
	{

	  // the teacher has responded positively to the job offer ...
	  // also, reset deleted here, so even if we've deleted this match before. It can
	  // still be resurrected...
	  $sql_sent_match="UPDATE " . SENT_MATCH . " SET job_response = $recruiter_response_form, 
                                                       job_notes = '$job_notes', deleted = '0'
			                           WHERE teacher_id = '$teacher_id'  AND job_id = '$recruiter_jobs_id'";			 

	}
	  
      $redirect_page = "/control_panel.".$phpEx;

    }
  else 
    {
	    
      // the sent match has been added already by the user, 
      // so we're just going to update it to a +/-
      $sql_sent_match="UPDATE " . SENT_MATCH . "
			       SET job_response = $recruiter_response_form ,  
				   job_notes = '$job_notes' ,
				   success = $recruiter_response_form,
                                   deleted = 0
		 	       WHERE teacher_id = '$teacher_id'
		 	       AND job_id = '$recruiter_jobs_id'";
	    
      $redirect_page = "/form.".$phpEx."?mode=thanks" ;

    }


  if(!($result=$db->sql_query($sql_sent_match)))
    {
      message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql_sent_match);
	
    }
	
  redirect(append_sid($redirect_page, true));

}

// handle school response form. Will the school accept the candidate ?
// append note to recruiter notes, update the sent_match table
if(isset($_POST['school_response_form']))
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);

  if( $school_response_form==1 )
    {
		
      $notes_text = $school_name.$lang['Wants_interview'].$teacher_name;
      $notes_append->append_teachers($teacher_id,$notes_text);
      $notes_append->append_schools($school_jobs_id,$notes_text);
	
    }
  else if( $school_response_form == 0 )
    {
		
      $notes_text = $school_name.$lang['Not_interested'].$teacher_name;
      $notes_append->append_teachers($teacher_id,$notes_text);
      $notes_append->append_schools($school_jobs_id,$notes_text);
	
    }

  // if there is a positive response but the school didn't leave a comment.
  // then add a comment anyways to know that the response has been successful
  if ( ( $school_response_form == 1 ) && ( $job_notes == "" )  )
    {
      $job_notes=$lang['Interested'];
    }

  // has this school give the user a manual indication that 
  // they are intersted in the school. No sent match has then been sent.
  if ( isset($_POST['from_school_gallery']) )
    {

      // special id_to_name function. I don't want to take name info from client side
      $teacher_name = id_to_name($teacher_id,'teachers');
      $school_name = id_to_name($school_jobs_id,'school_jobs');
	    
      $job_notes = $school_name . $lang['Interested'] . $teacher_name ; 
	  
      // if this match is already in the sent_match,
      // if the school has already expressed an interest
      // then just update it
      $sql = "SELECT * FROM " . SENT_MATCH . " WHERE teacher_id = '$teacher_id' AND job_id = '$school_jobs_id' ";

      if(!($result=$db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql);
	}

      // is it in the sent_match table already ?
      $row = $db->sql_fetchrow($result);

      if ( $row == '' )
	{
	  // if it's sent from the school gallery - then the original sending email was never sent out
	  // and so we insert the value instead of updating it.
	  $sql_sent_match="INSERT INTO " . SENT_MATCH . " (job_response, job_notes, deleted, teacher_id, job_id) 
                    VALUES ( '$school_response_form', '$job_notes', 0, '$teacher_id', '$school_jobs_id' )";

	}
      else 
	{

	  // the teacher has responded positively to the job offer ...
	  // also, reset deleted here, so even if we've deleted this match before. It can
	  // still be resurrected...
	  $sql_sent_match="UPDATE " . SENT_MATCH . " SET job_response = $school_response_form, 
                                                       job_notes = '$job_notes', deleted = '0'
			                           WHERE teacher_id = '$teacher_id'  AND job_id = '$school_jobs_id'";			 

	}

      $redirect_page = "/control_panel.".$phpEx;
	    
    }
  else   
    {


      // the sent_match from the user has already been stored, we are just
      // going to update the value here to indicate a +/-
      $sql_sent_match="UPDATE " . SENT_MATCH . "
			       SET job_response = $school_response_form ,  
				   job_notes = '$job_notes' ,
				   success = $school_response_form,
                                   deleted = 0
		 	       WHERE teacher_id = '$teacher_id'
		 	       AND job_id = '$school_jobs_id'";

      $redirect_page = "/form.".$phpEx."?mode=thanks" ;

    }


  if(!($result=$db->sql_query($sql_sent_match)))
    {
      message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql_sent_match);
    }
	

  redirect(append_sid($redirect_page, true));

}

// handles the teacher response when the teacher is offered a recruiter job
// or the teacher has expressed an interest in the recruiter job
// Is the teacher interested in this recruiter's job ?
// if so, update sent_match, select all info on that recruiter job, mail the recruiter, add notes 
if(isset($_POST['teacher_response_form']) && isset($_POST['recruiter_jobs_id']) )
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);

  // special consideration if this is from the teacher gallery,
  // need to build up the teacher notes ourselves
  if ( isset($_POST['from_gallery']) )
    {

      // special id_to_name function. I don't want to take name info from client side
      $teacher_name = id_to_name($teacher_id,'teachers');
      $recruiter_name = id_to_name($recruiter_jobs_id,'recruiter_jobs');
	    
      $teacher_notes = $teacher_name . $lang['Interested'] . $recruiter_name ; 

      // if this match is already in the sent_match,
      // if the school has already expressed an interest
      // then just update it
      $sql = "SELECT * FROM " . SENT_MATCH . " WHERE teacher_id = '$teacher_id' AND job_id = '$recruiter_jobs_id' ";

      if(!($result=$db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql);
	}

      // is it in the sent_match table already ?
      $row = $db->sql_fetchrow($result);

      if ( $row == '' )
	{
	  // if it's sent from the teacher gallery - then the original sending email was never sent out
	  // and so we insert the value instead of updating it.
	  $sql_sent_match="INSERT INTO " . SENT_MATCH . " (teacher_response, teacher_notes, deleted, teacher_id, job_id) 
                    VALUES ( '$teacher_response_form', '$teacher_notes', 0, '$teacher_id', '$recruiter_jobs_id' )";

	}
      else 
	{
	 
	  $sql_sent_match="UPDATE " . SENT_MATCH . " SET teacher_response = $teacher_response_form, teacher_notes = '$teacher_notes', deleted = '0'
			   WHERE teacher_id = '$teacher_id'
			   AND job_id = '$recruiter_jobs_id'";			 

	}
    }
  else 
    {

      // the teacher has responded positively to the job offer ...
      // also, reset deleted here, so even if we've deleted this match before. It can
      // still be resurrected...
      $sql_sent_match="UPDATE " . SENT_MATCH . " SET teacher_response = $teacher_response_form, 
                           teacher_notes = '$teacher_notes', deleted = '0'
			   WHERE teacher_id = '$teacher_id'
			   AND job_id = '$recruiter_jobs_id'";			 

    }


  // if the teacher doesn't give any comment with his reply, then insert one
  if ( $teacher_notes=="" )
    { 
      $teacher_notes=$lang['Interested']  ;
    }

  if(!($result=$db->sql_query($sql_sent_match)))
    {
      message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql_sent_match);
    }

  if($teacher_response_form==1)
    {

      $sql = "SELECT * FROM " . RECRUITER_JOBS . " WHERE recruiter_jobs_id = '$recruiter_jobs_id'";	
	
      if(!($result=$db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR,'Could not connect to sent_matches table',__LINE__,__FILE__,$sql);
	}
	
      while( $recruiter_val = $db->sql_fetchrow($result) )
	{
	  $recruiter_val['contact']=preg_replace('/ /','_',$recruiter_val['contact']);

	  // send the positive response to the recruiter .... that the teacher has accepted a position.
	  $text_recruiters = $page_maker['teacher_recruiter_posresponse_email'];

	  // filters the admin_cp values ...
	  $text_recruiters = str_replace('{RECRUITER_NAME}',$recruiter_val['contact'],$text_recruiters);
	  $text_recruiters = str_replace('{TEACHER_NAME}',$teacher_name,$text_recruiters);
	  $text_recruiters = str_replace('{RECRUITER_SCHOOL}', $recruiter_val['school'],$text_recruiters);
	  $text_recruiters = str_replace('{TEACHER_ID}',$teacher_id,$text_recruiters);
	  $text_recruiters = str_replace('{RECRUITER_JOB_ID}',$recruiter_val['recruiter_jobs_id'],$text_recruiters);
	  $text_recruiters = str_replace('{USERNAME}',$userdata['username'],$text_recruiters);

	  $subject_recruiters = $lang['Job_view'];
	  $email = $recruiter_val['email'];
			
	  // update notes. Leave the school value out of the loop so that it can be used below
	  $recruiter_loop_val = $recruiter_val['school'];

	  $notes_text = $teacher_name . $lang['Interested'] . $recruiter_val['school'] ; 
	  $notes_append->append_teachers($teacher_id,$notes_text);

	  $jobs_id=$recruiter_val['recruiter_jobs_id'];

	  // this bit doesn't work
	  $notes_append->append_recruiters($jobs_id,$notes_text);

	}	

    }
  else if($teacher_response_form==0)
    {

      $notes_text = $teacher_name . $lang['Not_interested']. $recruiter_val['school'] ;
      $notes_append->append_teachers($teacher_id,$notes_text);

      $jobs_id=$recruiter_val['recruiter_jobs_id'];

      // this bit doesn't work
      $notes_append->append_recruiters($jobs_id,$notes_text);

    }

  redirect(append_sid("/form.".$phpEx."?mode=thanks", true));

}

// handles the teacher response when the teacher is offered a school job
// or the teacher has expressed an interest in the school job
// Is the teacher interested in this school's job ?
// if so, update sent_match, select all info on that school job, mail the school, add notes 
if(isset($_POST['teacher_response_form']) && isset($_POST['school_jobs_id']) )
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);

  // special consideration if this is from the teacher gallery,
  // need to build up the teacher notes ourselves
  if ( isset($_POST['from_gallery']) )
    {

      // special id_to_name function. I don't want to take name info from client side
      $teacher_name = id_to_name($teacher_id,'teachers');
      $school_name = id_to_name($school_jobs_id,'school_jobs');
	    
      $teacher_notes = $teacher_name . $lang['Interested'] . $school_name ; 

      // if this match is already in the sent_match,
      // if the school has already expressed an interest
      // then just update it
      $sql = "SELECT * FROM " . SENT_MATCH . " WHERE teacher_id = '$teacher_id' AND job_id = '$school_jobs_id' ";

      if(!($result=$db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql);
	}

      // is it in the sent_match table already ?
      $row = $db->sql_fetchrow($result);

      if ( $row == '' )
	{
	  // if it's sent from the teacher gallery - then the original sending email was never sent out
	  // and so we insert the value instead of updating it.
	  $sql_sent_match="INSERT INTO " . SENT_MATCH . " (teacher_response, teacher_notes, deleted, teacher_id, job_id) 
                    VALUES ( '$teacher_response_form', '$teacher_notes', 0, '$teacher_id', '$school_jobs_id' )";

	}
      else 
	{

	  // the teacher has responded positively to the job offer ...
	  // also, reset deleted here, so even if we've deleted this match before. It can
	  // still be resurrected...
	  $sql_sent_match="UPDATE " . SENT_MATCH . " SET teacher_response = $teacher_response_form, 
                                                       teacher_notes = '$teacher_notes', deleted = '0'
			                           WHERE teacher_id = '$teacher_id'  AND job_id = '$school_jobs_id'";			 

	}
	    
    }
  else   
    {

      // the teacher has responded positively to the job offer ...
      // also, reset deleted here, so even if we've deleted this match before. It can
      // still be resurrected...
      $sql_sent_match="UPDATE " . SENT_MATCH . " SET teacher_response = $teacher_response_form, 
                                                       teacher_notes = '$teacher_notes', deleted = '0'
			                           WHERE teacher_id = '$teacher_id'  AND job_id = '$school_jobs_id'";			 

    }

  // if the teacher doesn't give any comment with his reply, then insert one
  if ( $teacher_notes=="" )
    { 
      $teacher_notes=$lang['Interested']  ;
    }
	 
  if(!($result=$db->sql_query($sql_sent_match)))
    {
      message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql_sent_match);
    }

  // if it's a positive response from the teacher, then email the school
  if($teacher_response_form==1)
    {

      $sql = "SELECT * FROM " . SCHOOL_JOBS . " WHERE school_jobs_id = '$school_jobs_id'";	
	
      if(!($result=$db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR,'Could not connect to sent_matches table','',__LINE__,__FILE__,$sql);
	}
	
      while( $school_val = $db->sql_fetchrow($result) )
	{

	  $school_val['contact']=preg_replace('/ /','_',$school_val['contact']);

	  $text_school = $page_maker['teacher_school_posresponse_email'];

	  // filter these values from admin_cp
	  $arr0 = array('{SCHOOL_CONTACT}','{TEACHER_NAME}','{SCHOOL_NAME}',
			'{TEACHER_ID}','{SCHOOL_JOBS_ID}','{USERNAME}');
			
	  $arr1 = array($school_val['contact'],$teacher_name,$school_val['school'],
			$teacher_id,$school_val['school_jobs_id'],$userdata['username']);

	  $text_school = str_replace($arr0,$arr1,$text_school);

	  $subject_school = $lang['Job_view'];
	  $email = $school_val['email'];
			
	  // email the school to tell them that the teacher is interested
	  // who is the recruiter responsible for that school ?
	  $recruiter_email = email_from_regdate($school_val['user_reg_id']);

	  $headers = 'From: ' . $recruiter_email . "\r\n" .
	    'Reply-To: ' . $recruiter_email . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();

	  mail($email,$subject_school,$text_school,$headers );

	  // update notes. Leave the school value out of the loop so that it can be used below
	  $school_loop_val = $school_val['school'];
       
	  $notes_text = $teacher_name . $lang['Interested']. $school_val['school'] ; 
	  $notes_append->append_teachers($teacher_id,$notes_text);

	  // assign this value before putting it into a function
	  $jobs_id = $school_val['school_jobs_id'];

	  // this bit doesn't work
	  $notes_append->append_schools($jobs_id,$notes_text);

	}	

    } 
  // if it's a negative response then just add to notes and leave
  else if($teacher_response_form==0)
    {
      $notes_text = $teacher_name . $lang['Not_interested']. $school_val['school'];
      $notes_append->append_teachers($teacher_id,$notes_text);

      // assign this value before putting it into a function
      $jobs_id = $school_val['school_jobs_id'];

      $notes_append->append_schools($jobs_id,$notes_text);
    }
	
  redirect(append_sid("/form.".$phpEx."?mode=thanks", true));

}
// This will take the initial submit from a match between a recruiter and a candidate that the etk_recruiter
// has chosen to follow through on from the control panel. It will immediately update the teacher/recruiter
// recruiter notes, then it will mail the details of the recruiter_job to the teacher. It will then enter the 
// match in to the sent_match table so that the same match doesn't come up again.
if(isset($_POST['sent_match_recruiter']))
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);

  // this is my double send catch. Don't send the same 
  // job to the same teacher more than once		
  $sql = "SELECT COUNT(*) AS row_count FROM " . SENT_MATCH . " WHERE teacher_id ='$teacher_id' AND job_id = '$recruiter_jobs_id' ";
	
  if( !($result = $db->sql_query($sql) ) )
    {
      message_die(CRITICAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }
	
  $previous_array = $db->sql_fetchrow($result);

  if( $previous_array['row_count'] > 0 )
    {
      exit ;
    }
	
  // Find the name and school belonging to the ID's and make a not about it...
  $sql_teachers = "SELECT * FROM " . TEACHERS . " WHERE teacher_id = '$teacher_id'"; 
  $sql_recruiters = "SELECT * FROM " . RECRUITER_JOBS . " WHERE recruiter_jobs_id = '$recruiter_jobs_id' ";
	
  if( !( $result_teachers = $db->sql_query($sql_teachers)) | !($result_recruiters = $db->sql_query($sql_recruiters)) )
    {
      message_die(CRITICAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql_teachers);
    }
	
  $row_recruiters = $db->sql_fetchrow($result_recruiters);
  $row_teachers = $db->sql_fetchrow($result_teachers);
  $teachers_name=explode(" ",$row_teachers['name']);	
		 
  $teacher_notes = $lang['Sent_details'] . $row_teachers['name'] . $lang['About'] . $row_recruiters['school'] ;
  $recruiter_notes= $lang['Sent_details'] . $row_teachers['name'] . $lang['About'] . $row_recruiters['school'] ;	

  $text_teachers = $page_maker['etk_teacher_recruiter_email'];

  // filter these values from admin_cp
  $arr0 = array('{TEACHER_NAME}','{TEACHER_MATCH_NOTES}','{RECRUITER_JOB_ID}',
		'{TEACHER_ID}','{USERNAME}');
			
  $arr1 = array($teachers_name[0],$teacher_match_notes,$recruiter_jobs_id,
		$teacher_id,$userdata['username']);

  $text_teachers = str_replace($arr0,$arr1,$text_teachers);

  $subject_teachers = $lang['Job_view'];

  $email = $row_teachers['email'];

  // get the administrator's email, the admin should deal with parter-recruiters
  $admin_email = get_admin_email();

  $headers = 'From: ' . $admin_email . "\r\n" .
    'Reply-To: ' . $admin_email . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

  mail($email,$subject_teachers,$text_teachers,$headers);
	
  $notes_append->append_teachers($teacher_id,$teacher_notes);
  $notes_append->append_recruiters($recruiter_jobs_id,$recruiter_notes);
	
  $sql = "INSERT INTO " . SENT_MATCH . " (teacher_id, job_id) VALUES ('$teacher_id', '$recruiter_jobs_id') ";
	
  if( !($result = $db->sql_query($sql) ) )
    {
      message_die(CRITICAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }

  exit ;

}
// This will take the initial submit from a match between a client and a candidate that the recruiter
// has chosen to follow through on from the control panel. It will immediately update the teacher/school
// recruiter notes, then it will mail the details of the client to the teacher. It will then enter the 
// match in to the sent_match table so that the same match doesn't come up again.
if(isset($_POST['sent_match_school']))
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);

  // this is my double send catch. Don't send the same 
  // job to the same teacher more than once		
  $sql = "SELECT COUNT(*) AS row_count FROM " . SENT_MATCH . " WHERE teacher_id ='$teacher_id' AND job_id = '$school_jobs_id' ";
	
  if( !($result = $db->sql_query($sql) ) )
    {
      message_die(CRITICAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }
	
  $previous_array = $db->sql_fetchrow($result);

  if( $previous_array['row_count'] > 0 )
    {
      exit ;
    }

  // find out the teacher and school id's and names, then send the 
  // job to the teacher and make a note of it.
  $sql_teachers = "SELECT * FROM " . TEACHERS . " WHERE teacher_id = '$teacher_id'"; 
  $sql_schools = "SELECT * FROM " . SCHOOL_JOBS . " WHERE school_jobs_id = '$school_jobs_id' ";
	
  if( !( $result_teachers = $db->sql_query($sql_teachers)) | !($result_schools = $db->sql_query($sql_schools)) )
    {
      message_die(CRITICAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }
	
  $row_schools = $db->sql_fetchrow($result_schools);
  $row_teachers = $db->sql_fetchrow($result_teachers);

  $teachers_name=explode(" ",$row_teachers['name']);	
		 
  // put the date in here, there was a problem with putting it in with the append_notes

  $teacher_notes = $lang['Sent_details'] . $row_teachers['name'] . $lang['About'] . $row_schools['school'] ;
  $school_notes= $lang['Sent_details'] . $row_teachers['name'] . $lang['About'] . $row_schools['school'] ;	 
	
  $text_teachers = $page_maker['etk_teacher_school_email'];

  // filter these values from admin_cp
  $arr0 = array('{TEACHER_NAME}','{TEACHER_MATCH_NOTES}','{SCHOOL_JOB_ID}',
		'{TEACHER_ID}','{USERNAME}');
			
  $arr1 = array($teachers_name[0],$teacher_match_notes,$school_jobs_id,
		$teacher_id,$userdata['username']);

  $text_teachers = str_replace($arr0,$arr1,$text_teachers);
	
  $subject_teachers = $lang['Job_view'];
  $email = $row_teachers['email'];
  
  // the email should come from the person who is dealing with that school
  $recruiter_email = email_from_regdate($row_schools['user_reg_id']);
	
  $headers = 'From: ' . $recruiter_email . "\r\n" .
    'Reply-To: ' . $recruiter_email . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

  mail($email,$subject_teachers,$text_teachers,$headers);
	
  $notes_append->append_teachers($teacher_id,$teacher_notes);
  $notes_append->append_schools($school_jobs_id,$school_notes);
	
  $sql = "INSERT INTO " . SENT_MATCH . " (teacher_id, job_id) VALUES ('$teacher_id', '$school_jobs_id') ";

  if( !($result = $db->sql_query($sql) ) )
    {
      message_die(GENERAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }
	
  exit ;

}
// this is for the success match completed. If it looks like I have made a match, then I want to enter the 
// teacher into our aftercare program. Whereby they get sent regular emails, happy birthday emails, chuseok is coming
// emails, your holidays are coming emails ... that sort of thing....
if(isset($_POST['success_match_completed']))
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);

  // set the success match to one 
  $sql = "UPDATE " . SENT_MATCH . " SET success = 1 AND deleted = 0 WHERE teacher_id = '$teacher_id' AND job_id = '$job_id'";

  // adjust the aftercare variable on the teacher's resume
  $sql_aftercare = "UPDATE " . SENT_MATCH . " SET aftercare = 1 WHERE teacher_id = '$teacher_id'";

  if( !($result = $db->sql_query($sql) ) && !($result_aftercare = $db->sql_query($sql_aftercare)  ) )
    {
      message_die(GENERAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }
  

}
// this is used when the intial teachers interest in a position turns out to be non-reciprocted 
// the school or recruiter are 'interested in another candidate' ... or indeed - the teacher isn't
// interested in the position...
if(isset($_POST['success_match_failed']))
{

  $_POST = array_map("input_check",$_POST);
  @extract($_POST);
    
  // make sure we notify a teacher if they're interest in a 
  // school has been rebuffed. School here means either recruiter
  // or just a school btw. Only notify teacher if they had expressed 
  // interest to begin with.	
  $sql = "SELECT COUNT(*) AS row_count FROM " . SENT_MATCH . " WHERE teacher_id ='$teacher_id' AND job_id = '$job_id' AND teacher_response = 1";
	
  if( !($result = $db->sql_query($sql) ) )
    {
      message_die(CRITICAL_ERROR,'Problem updating the matching table','',__LINE__,__FILE__,$sql);
    }

  $previous_array = $db->sql_fetchrow($result);

  $table_id == 'school' ? $table_id = 'school_jobs' : $table_id = 'recruiter_jobs' ;

  $fail_teacher = name_getter(id_to_name($teacher_id,'teachers'));
  $fail_school = id_to_name($job_id,$table_id);

  $recruiter_reg_id = job_id_to_reg_id($job_id,$table_id);
  $recruiter_email = email_from_regdate($recruiter_reg_id);

  // if the teacher had responded positively then email them
  // else just skip out ... and update the db
  if( $_POST['failed_mail'] == '1' )
    {

      $text_candidate_fail = $page_maker['candidate_fail_email'];
      // filter these values from admin_cp
      $arr0 = array('{FAIL_TEACHER}','{FAIL_SCHOOL}','{USERNAME}');
			
      $arr1 = array($fail_teacher,$fail_school,$userdata['username']);

      $text_candidate_fail = str_replace($arr0,$arr1,$text_candidate_fail);
 
      $subject_candidate_fail = $fail_school.$lang['Interested_other'];
      $email_candidate_fail = id_to_email($teacher_id,'teachers');
			
      $headers = 'From: ' . $recruiter_email . "\r\n" .
	'Reply-To: ' . $recruiter_email . "\r\n" .
	'X-Mailer: PHP/' . phpversion();

      mail($email_candidate_fail,$subject_candidate_fail,$text_candidate_fail,$headers);
	  
    }
	
  $notes = $fail_teacher . $lang['Not_interested'] . $fail_school;

  // append to the notes...
  $notes_append->append_teachers($teacher_id,$notes);
  $notes_append->append_schools($school_jobs_id,$notes);

  $sql = "UPDATE " . SENT_MATCH . " SET deleted=1 WHERE teacher_id='$teacher_id' AND job_id='$job_id'" ;
       
  if ( !( $result_success = $db->sql_query($sql) ) )
    {
      message_die(GENERAL_ERROR,'Problem selecting successful match','',__LINE__,__FILE__,$sql);
    }

  exit ;

}
	
?>	