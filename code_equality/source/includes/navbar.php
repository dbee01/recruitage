<?php

/**
 *  Navbar is an include file that will handle all the navigation bar output for the page...
 *
 *                                navbar.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
  die("Hacking attempt");
}


$start_navbar="
<script type='text/javascript' src='js/chrome.js'></script>

<div class='chromestyle' id='chromemenu'>
<ul>
<li><a href='control_panel.php'>Home</a></li>
<li><a href='control_panel.php' rel='dropmenu1'>Control Panel</a></li>
<li><a href='Page_1.php' rel='dropmenu2'>Front Site</a></li>
<li><a href='form.php?mode=school_form' rel='dropmenu3'>Applications</a></li>
<li><a href='form.php?mode=school_contact_form' rel='dropmenu4'>Contact</a></li>
<li><a href='form.php?mode=reminder' rel='dropmenu5'>Other</a></li>
<li><a href='form.php?mode=admin_form' rel='dropmenu6'>Admin</a></li>
</ul>
</div>

<!--1st drop down menu --> 
<div id='dropmenu1' class='dropmenudiv'>
<a href='control_panel.php?mode=cp_intro'>Intro</a>
<a href='control_panel.php?mode=cp_teachers'>Teachers</a>
<a href='control_panel.php?mode=cp_jobs'>Jobs</a>
<a href='control_panel.php?mode=cp_leads'>Matches</a>
<a href='control_panel.php?mode=cp_all'>All</a>
</div>

<!--2nd drop down menu --> 
<div id='dropmenu2' class='dropmenudiv'>
<a href='page_1.php'>Page_1</a>
<a href='page_2.php'>Page_2</a>
<a href='page_3.php'>Page_3</a>
<a href='page_4.php'>Page_4</a>
<a href='page_5.php'>Page_5</a>
<a href='form.php?mode=contact_us_form'>Contact_Us</a>
<a href='pic_gallery/index.html'>Picture Gallery</a>
</div>

<!--3rd drop down menu --> 
<div id='dropmenu3' class='dropmenudiv'>
<a href='form.php?mode=teacher_resume_form'>Teacher Resume Form</a>
<a href='form.php?mode=school_form'>School Application Form</a>
<a href='form.php?mode=recruiter_form'>Recruiter Application Form</a>
</div>


<!--4th drop down menu --> 
<div id='dropmenu4' class='dropmenudiv' style='width: 150px;'>
<a href='form.php?mode=school_contact_form'>School Contact Form</a>
<a href='form.php?mode=email_form'>Email Form</a>
<a href='form_select.php?mode=sms_interface'>SMS Texting</a>
<a href='job_calendar.php'>Teacher Reminder</a>
<a href='job_calendar.php?mode=email'>Job Reminder</a>
<a href='#' onclick=\"window.open('w.php','','scrollbars=yes,menubar=no,resizable=yes,toolbar=no,location=no,status=no,left=20,top=20,width=480,height=270,resizable=0');\"  >Chat Client</a>
</div>

<!--5th drop down menu --> 
<div id='dropmenu5' class='dropmenudiv' style='width: 150px;'>
<a href='form.php?mode=reminder'>Reminder</a>
<a href='form.php?mode=recruiter_resume'>Recruiter Resume</a>
<a href='form_select.php?mode=history'>History</a>
<a href='form.php?mode=bookmarks'>Websites</a>
<a href='form_select.php?mode=knowledge_base'>Knowledge Base</a>
<a href=\"https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=David%40EnglishTeachingKorea.com&item_name=EnglishTeachingKorea+Recruitment+Services&item_number=Recruitment+Fee&amount=1000.00&no_shipping=2&return=http%3A%2F%2Fwww.EnglishTeachingKorea.com%2Fform.php%3Fmode%3Dthanks&no_note=1&currency_code=USD&lc=IE&bn=PP-BuyNowBF&submit=Submit+Query\" >Payment Gateway</a>
<a href='form.php?mode=timetable_form'>Timezone</a>
</div>

<span style='' >
$paypal_link
</span>
";

$form_navbar ="
<!--6th drop down menu --> 
<div id='dropmenu6' class='dropmenudiv' style='width: 150px;'>
<a href='form_select.php?mode=admin_action_monitor'>Employee Logging</a>
<a href='form_select.php?mode=add_recruiter'>Add Recruiter</a>
<a href='form_select.php?mode=recruitment_agreement'>Recruitment Agreement</a>
<a href='form_select.php?mode=admin_marketing_stats'>Marketing Stats</a>
<a href='form_select.php?mode=admin_marketing_campaigns'>Marketing Campaigns</a>
<a href='form.php?mode=admin_email_temp'>Email Helper Text</a>
<a href='form.php?mode=admin_auto_page'>System Page Responses</a>
<a href='form.php?mode=admin_auto_email'>System Email Responses</a>
<a href='form.php?mode=admin_time_aftercare'>Fixed Time Aftercare</a>
<a href='form.php?mode=admin_form_aftercare'>Variable Time Aftercare</a>
<a href='form.php?mode=admin_form_website'>Website Text</a>
<a href='form_select.php?mode=admin_form_select'>Form Selection</a>
</div>
";

$end_navbar="
<script type='text/javascript'>
cssdropdown.startchrome('chromemenu')
</script>
";


$candidate_navbar="<div>
<ul class='navbar'>
  <li><a href='/page_1.php' class='style5 nav'><strong>Home</strong></a></li>
  <li><a href='/page_2.php' class='style5 nav style1'><strong>Jobs</strong></a></li>
  <li><a href='/page_3.php' class='style5 nav style1'><strong>Korea</strong></a></li>
  <li><a href='/page_4.php' class='style5 nav style1'><strong>Visa</strong></a></li>
  <li><a href='/page_5.php' class='style5 nav style1'><strong>Links</strong></a></li>
  <li><a href='/page_6.php' class='style5 nav style1'><strong>Contact</strong></a></li>
</ul>
</div>
";

$school_navbar="<div>
<ul class='navbar'>
  <li><a href='/page_1.php' class='style5 nav'><strong>Home</strong></a></li>
  <li><a href='/form.php?mode=school_form' class='style5 nav style1'><strong>School Job Entry</strong></a></li>
  <li><a href='/form.php?mode=recruiter_form' class='style5 nav style1'><strong>Recruiter Job Entry</strong></a></li>
  <li><a href='/job_calendar.php?mode=email' class='style5 nav style1'><strong>Job Reminder</strong></a></li>
  <li><a href='/job_calendar.php' class='style5 nav style1'><strong>Teacher Reminder</strong></a></li>
  <li><a href='/page_6.php' class='style5 nav style1'><strong>Contact</strong></a></li>
</ul>
</div>
";

$etk_frontpage="<div id='menu'>
		 <a href='page_1.php'>Home</a>
		 <a href='page_2.php'>Jobs</a>
		 <a href='page_3.php'>Korea</a>
		 <a href='page_4.php'>Visa</a>
		 <a href='page_5.php'>Links</a>
		 <a href='' onclick=\"window.open('form.php?mode=contact_us_form','','scrollbars=yes,menubar=no,resizable=yes,toolbar=no,location=no,status=no');\">Contact</a>
		</div>
";

// make a navbar for the app user / recruiter
$user_navbar = $start_navbar . $end_navbar ;

// make a navbar for the admin
$admin_navbar = $start_navbar . $form_navbar . $end_navbar ;

$this_page=$HTTP_SERVER_VARS['PHP_SELF'];

if ( ( stristr($this_page,'index.php') != '' ) | ( stristr($this_page,'jobs.php') != '' )   | ( stristr($this_page,'korea.php') != '' ) | ( stristr($this_page,'links.php') != '' ) | ( stristr($this_page,'visa.php') != '' )  )
{
  $template->assign_var('NAVBAR',$etk_frontpage);
}
else if ( ( stristr($this_page,'recruiter_panel.php') != '' ) || ( stristr($this_page,'job_calendar.php') != '' ) || ( stristr($this_page,'mode=recruiter_form') != '' )  ) {
 
  // do the front page for the school
  $template->assign_var('NAVBAR',$school_navbar);

}
else
{

  if ( $userdata['session_logged_in'] && ($userdata['user_level'] == ADMIN ) )
  {
      $template->assign_var('NAVBAR',$admin_navbar);
  }
  else if ( $userdata['session_logged_in'] && ($userdata['user_level'] == USER ) )
  {
      $template->assign_var('NAVBAR',$user_navbar);
  }
  else
  {
      $template->assign_var('NAVBAR',$candidate_navbar);

  }

}


?>