<?php

/**
 *  This class will try to bring together many of the various phone issues
 *
 *                               phone.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

class phone
{

  // terminate the voip call to the user's phone if necessary
  var $user_mobile;
  var $user_phone_mode;

  // instatiate the object...
  function phone()
  {

    // find user phone mode ...
    global $userdata, $db;

    // find out which phone mode we are using ...
    $sql = "SELECT user_phone_mode FROM users WHERE user_id = ".$userdata['user_id'];

    if ( !( $result = $db->sql_query($sql) ) )
      {
	message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql);
      }

    while ( $row = $db->sql_fetchrow($result) )
      {

	@extract($row);
	
	// which phone mode are we using ? callback ? skype ? xten ?
	$user_phone_mode = $row['user_phone_mode'];

      }

    $this->user_phone_mode = $user_phone_mode ;


    // find the user mobile number ...
    // find out which phone mode we are using ...
    $sql = "SELECT user_mobile FROM users WHERE user_id = ".$userdata['user_id'];

    if ( !( $result = $db->sql_query($sql) ) )
      {
	message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql);
      }
 
    while ( $row = $db->sql_fetchrow($result) )
      {

	@extract($row);
	
	// which phone mode are we using ? callback ? skype ? xten ?
	$user_mobile = $row['user_mobile'];

      }

    $this->user_mobile = $user_mobile;

  }

  // get the user phone mode ...
  function user_phone_mode()
  {

    global $userdata, $db;

    // find out which phone mode we are using ...
    $sql = "SELECT user_phone_mode FROM users WHERE user_id = ".$userdata['user_id'];

    if ( !( $result = $db->sql_query($sql) ) )
      {
	message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql);
      }
 
    while ( $row = $db->sql_fetchrow($result) )
      {

	@extract($row);
	
	// which phone mode are we using ? callback ? skype ? xten ?
	$user_phone_mode = $row['user_phone_mode'];

      }
    
    return $user_phone_mode;

  }

  // get the user's mobile phone number ...
  function user_mobile_number()
  {

    global $userdata, $db;

    // find out which phone mode we are using ...
    $sql = "SELECT user_mobile FROM users WHERE user_id = ".$userdata['user_id'];

    if ( !( $result = $db->sql_query($sql) ) )
      {
	message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql);
      }
 
    while ( $row = $db->sql_fetchrow($result) )
      {

	@extract($row);
	
	// which phone mode are we using ? callback ? skype ? xten ?
	$user_mobile = $row['user_mobile'];

      }

    // echo back the phone mode
    return $user_mobile;

  }

  // take a call to number and construct a call back link scenario
  function callback_link($callto)
  {

    return "<a href='#' onclick=\"callback('$callto');return false;\" >Callback</a>";

  }

  // take a call to number and construct a call back link scenario
  function skype_link($callto)
  {

    // chop off the two leading zeros
    $callto = substr(2,$callto);
    
    return "<a href='skype:+${callto}?call' >Skype</a>";

  }


  // do a skype conference call
  function skype_conf($call1,$call2)
  {

    // chop off the two leading zeros
    $call1 = substr(2,$call1);
    $call2 = substr(2,$call2);
    
    return "<a href='skype:${call1};+${call2}?call' >Skype Conference</a>";

  }

  // take a call to number and construct a call back link scenario
  function xten_link($callto)
  {

    return "<a href='sip:${callto}'>Call</a>";

  }

  function do_callback($id)
  {

    global $userdata;

    $call_to = $id ;
    $call_from = $userdata['user_mobile'];

    // build object and do callback
    $voip = new voipbuster();
    $voip->login();

    $voip->callback($call_from,$call_to);

  }

  // try an chop the phone number down so that it fits international format...
  function phone_chopper($phone)
  {

    // if it has 0000 at the start, then someone added a country code twice
    if ($phone[0]=='0' && $phone[1]=='0' && $phone[2]=='0' && $phone[3]=='0' )
    {
      $phone = substr($phone,2);
    }

    if ($phone[0]=='0' && $phone[1]=='0' && $phone[2]=='0' )
      {
	$phone = substr($phone,1);
      }

    if ($phone[0]=='0' && $phone[1] !='0' )
      {

	$phone = substr($phone,1);
      }

    return $phone ;

  }

  // return the sms link...
  function sms_link($callto)
  {

    return "<a href='/form_select.php?mode=sms_interface&number=$callto' >SMS</a>";

  }

  // book a third party call to be connected by the server
  function book_third_party($teacher_id,$recruiter_jobs_id,$school_jobs_id,$time,$user_reg_date)
  {
    
    global $notes;
    global $time;

    // are we dealing with the recruiter or schools table ?
    switch ( -1 )
      {
      case ( $recruiter_jobs_id ) : $job_id = $recruiter_jobs_id ; $table = 'recruiter_jobs' ; break ;
      case ( $school_jobs_id ) :  $job_id = $school_jobs_id ; $table = 'school_jobs'   ; break ;
      default : exit ;
      }

    // update the teacher notes...
    $teacher_name = id_to_name($teacher_id,'teachers');
    $job_name = id_to_name($job_id,$table);

    // check for the notes...
    $teacher_notes = "Call arranged between $teacher_name and $job_name for $date";
    $job_notes = "Call arranged between $job_name and $teacher_name for $date";

    // append the teachers notes
    $notes->append_teachers('teachers',$teacher_notes);

    // append the job notes
    switch ( $table )
      {
      case 'recruiter_jobs' : $notes->append_recruiters($recruiter_jobs_id,$job_notes); break;
      case 'school_jobs' : $notes->append_schools($school_jobs_id,$job_notes); break;
      default : echo 'Could not update the notes third party table'; break;
      }
    
    // add the third party call to the reminders table
    $text = 'Just standard text to signify that this is a third party call reminder';

    // TAKE A STRING INPUT IN mktime format (18,0,0) 
    $rem_time = $appoint_time . ',' . $reminder_date ; 

    // verify that this is a third party call
    $third_party = 1 ;

    $timestamp = $time->return_stamp($rem_time);

    if ( strtotime(date('H:i')) < strtotime($timestamp) ) 
      { 
	message_die(GENERAL_MESSAGE,'That time has now passed, pick something for later on ...');				
      }

    // insert reminder into the database...
    $sql="INSERT INTO reminder ( text , rem_time, sms, email, third_party, user_reg_date ) 
           VALUES ('$text', '$timestamp', '$sms', '$email', $third_party, $user_reg_date )";	

    if (!($result = $db->sql_query($sql)))
      {
	message_die(CRITICAL_ERROR, 'Cannot connect to database for reminder', '', __LINE__,__FILE__,$sql);
      }	

    return 'Third party call notes and insertion done';


  }

}

?>