<?php

/**
 * This is a curl script class to post our ads to various remote servers...
 *
 *                              job_post.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

define('IN_DECRUIT', true);

$phpbb_root_path = './';

include($phpbb_root_path . 'extension.inc');
require_once($phpbb_root_path .'common.'.$phpEx);

// post the job ads to the various websites here.
// this replaces the bash script that did the same job.
class job_post 
{

  var $agent, $cookie_jar, $referrer, $user, $pass;

  function job_post()
  {

    global $lang;
    echo 'here';
    $this->lang = $lang ;
    $this->email = '';
    $this->contact = '';

    // set user agent
    $this->agent = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)";

    // create a cookie jar to store cookies ...
    $this->cookie_jar = "../tmp/cookiejar.txt";

  }

  // post ad on worknplay.co.kr
  function worknplay()
  {

    global $db;

    // the campaign name has to have worknplay in it...
    $sql = "SELECT campaign_name, ad_title, ad_amount, ad_copy FROM free_campaigns WHERE campaign_name LIKE 'worknplay%'";

    if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem grabbing the campaign data','',__LINE__,__FILE__,$sql);
    }

    while ( $row = $db->sql_fetchrow($result)  )
      {

	// set the variables ...
	$ad_title = $row['ad_title'];
	$ad_copy = $row['ad_copy'];

	// set the worknplay specific salary
	$this->salary = '0000000';

	// set referer
	$referer = "http://www.worknplay.co.kr";
	
	// set user and pass
	$this->user = '';
	$this->pass = '';

        #### Login to worknplay ######

	// create curl object
	$ch = curl_init();

	if (!$ch) {
	  die ("Cannot initialize a new cURL handle\n");
	}

	// set the post fields ...
	curl_setopt($ch, CURLOPT_POSTFIELDS,"__EVENTTARGET=_ctl3%3A_ctl0%3AbtnLogIn&_ctl3%3A_ctl0%3AtxtUserId=$this->user&_ctl3%3A_ctl0%3AtxtUserPw=$this->pass&ucMainJobOfferList%3AtxtSearchText=");

	// store cookies
	curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
	curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
	curl_setopt($ch, CURLOPT_REFERER, $referer);

	curl_setopt($ch, CURLOPT_URL,"http://www.worknplay.co.kr");

	// this returns the value of curl instead of printing it to the page
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = curl_exec ($ch);

	// Data is returned on success, error code on failure
	if (is_int ($data)) {
	  die ("cURL error: " . curl_error ($ch) . "\n");
	}

	//     curl_close ($ch);
	//     unset($ch);

	#### GET A FORM  ####

	$referer = 'www.worknplay.co.kr/Employer/OfferAdd.aspx';

	$form_viewstate = "__VIEWSTATE=dDwtMzQyNjkzODA4O3Q8O2w8aTwxPjs%2BO2w8dDw7bDxpPDA%2BO2k8ND47aTw2PjtpPDEwPjtpPDIwPjtpPDIyPjtpPDI0PjtpPDI2PjtpPDI4Pjs%2BO2w8dDw7bDxpPDE%2BO2k8Mz47aTw1PjtpPDc%2BO2k8OT47PjtsPHQ8O2w8aTwwPjs%2BO2w8dDw7bDxpPDE%2BO2k8Mz47PjtsPHQ8cDxsPFRleHQ7PjtsPFw8b2JqZWN0IGNsYXNzaWQ9ImNsc2lkOkQyN0NEQjZFLUFFNkQtMTFjZi05NkI4LTQ0NDU1MzU0MDAwMCIgY29kZWJhc2U9Imh0dHA6Ly9kb3dubG9hZC5tYWNyb21lZGlhLmNvbS9wdWIvc2hvY2t3YXZlL2NhYnMvZmxhc2gvc3dmbGFzaC5jYWIjdmVyc2lvbj02LDAsMjksMCIgd2lkdGg9IjM1MCIgaGVpZ2h0PSI2NyJcPlw8cGFyYW0gbmFtZT0ibW92aWUiIHZhbHVlPSIvSW1hZ2UvTWVudVRvcC9sb2dvLnN3ZiJcPlw8cGFyYW0gbmFtZT0icXVhbGl0eSIgdmFsdWU9ImhpZ2giXD5cPHBhcmFtIG5hbWU9InNjYWxlIiB2YWx1ZT0ibm9zY2FsZSJcPlw8cGFyYW0gbmFtZT0ibWVudSIgdmFsdWU9ImZhbHNlIlw%2BXDxlbWJlZCBzcmM9Ii9JbWFnZS9NZW51VG9wL2xvZ28uc3dmIiBxdWFsaXR5PSJoaWdoIiBwbHVnaW5zcGFnZT0iaHR0cDovL3d3dy5tYWNyb21lZGlhLmNvbS9nby9nZXRmbGFzaHBsYXllciIgdHlwZT0iYXBwbGljYXRpb24veC1zaG9ja3dhdmUtZmxhc2giIHdpZHRoPSIzNTAiIGhlaWdodD0iNjciXD5cPC9lbWJlZFw%2BXDwvb2JqZWN0XD47Pj47Oz47dDxwPGw8VGV4dDs%2BO2w8XDxhIGhyZWY9Ii9PcGVuV2luZG93LmFzcHg%2FYmFubmVyU249NTEiXD5cPGltZyBzcmM9Ii9fRmlsZS8yMDA1MDUvdG9wLmdpZiIgd2lkdGg9IjQwMCIgaGVpZ2h0PSI1MCJcPlw8L2FcPjs%2BPjs7Pjs%2BPjs%2BPjt0PDtsPGk8MD47PjtsPHQ8O2w8aTwxPjs%2BO2w8dDxwPGw8VGV4dDs%2BO2w8XDxvYmplY3QgY2xhc3NpZD0iY2xzaWQ6RDI3Q0RCNkUtQUU2RC0xMWNmLTk2QjgtNDQ0NTUzNTQwMDAwIiBjb2RlYmFzZT0iaHR0cDovL2Rvd25sb2FkLm1hY3JvbWVkaWEuY29tL3B1Yi9zaG9ja3dhdmUvY2Ficy9mbGFzaC9zd2ZsYXNoLmNhYiN2ZXJzaW9uPTYsMCwyOSwwIiB3aWR0aD0iODQyIiBoZWlnaHQ9IjU5Ilw%2BXDxwYXJhbSBuYW1lPSJtb3ZpZSIgdmFsdWU9Ii9JbWFnZS9NZW51VG9wL21haW5tZW51LnN3Zj9tYWluPW0yJnN1Yj1zMyJcPlw8cGFyYW0gbmFtZT0icXVhbGl0eSIgdmFsdWU9ImhpZ2giXD5cPHBhcmFtIG5hbWU9InNjYWxlIiB2YWx1ZT0ibm9zY2FsZSJcPlw8cGFyYW0gbmFtZT0ibWVudSIgdmFsdWU9ImZhbHNlIlw%2BXDxlbWJlZCBzcmM9Ii9JbWFnZS9NZW51VG9wL21haW5tZW51LnN3Zj9tYWluPW0yJnN1Yj1zMyIgcXVhbGl0eT0iaGlnaCIgcGx1Z2luc3BhZ2U9Imh0dHA6Ly93d3cubWFjcm9tZWRpYS5jb20vZ28vZ2V0Zmxhc2hwbGF5ZXIiIHR5cGU9ImFwcGxpY2F0aW9uL3gtc2hvY2t3YXZlLWZsYXNoIiB3aWR0aD0iODQyIiBoZWlnaHQ9IjU5Ilw%2BXDwvZW1iZWRcPlw8L29iamVjdFw%2BOz4%2BOzs%2BOz4%2BOz4%2BO3Q8O2w8aTwwPjs%2BO2w8dDw7bDxpPDE%2BOz47bDx0PHA8bDxUZXh0Oz47bDxcPHRyXD5cPHRkIHN0eWxlPSJoZWlnaHQ6NDRweFw7YmFja2dyb3VuZC1pbWFnZTp1cmwoL0ltYWdlL0VtcGxveWVyL2xlZnRfbWVudV9sb2dpbi5naWYpXDsiIGNsYXNzPSJjbSJcPlw8c3BhbiBzdHlsZT0iY29sb3I6I0ZGRkZGRlw7Zm9udC13ZWlnaHQ6Ym9sZFw7Ilw%2BSGksIGRiZWUgXDxhIGhyZWY9Ii9Vc2VyL0xvZ091dC5hc3B4Ilw%2BXDxpbWcgc3JjPSIvSW1hZ2UvQmFzZS9sb2dvdXRiLmdpZiIgYWxpZ249ImFic21pZGRsZSJcPlw8L2FcPlw8L3NwYW5cPlw8L3RkXD5cPC90clw%2BOz4%2BOzs%2BOz4%2BOz4%2BO3Q8O2w8aTwwPjs%2BO2w8dDw7bDxpPDE%2BO2k8Mz47aTw1PjtpPDc%2BO2k8OT47aTwxMT47aTwxMz47aTwxNT47aTwxNz47aTwxOT47PjtsPHQ8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzx0Pjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BO3Q8cDxwPGw8VmlzaWJsZTs%2BO2w8bzxmPjs%2BPjs%2BOzs%2BOz4%2BOz4%2BO3Q8O2w8aTwwPjs%2BO2w8dDw7bDxpPDE%2BOz47bDx0PHA8cDxsPEltYWdlVXJsOz47bDwvSW1hZ2UvQmFzZS9wYWdlX3RvcF9iZzAxLmdpZjs%2BPjs%2BOzs%2BOz4%2BOz4%2BOz4%2BO3Q8dDxwPHA8bDxEYXRhVmFsdWVGaWVsZDtEYXRhVGV4dEZpZWxkOz47bDxTbjtjb2RlTmFtZTs%2BPjs%2BO3Q8aTw3PjtAPFtTZWxlY3RdO0xhbmd1YWdlL0VGTDtBcnQvRW50ZXJ0YWlubWVudDtCdXNpbmVzcy9Db3Jwb3JhdGU7RW5naW5lZXJpbmcvVHJhZGVzO0lUO090aGVyczs%2BO0A8MDsxODg7MTg5OzE5MDsxOTE7MTkyOzE5Mzs%2BPjs%2BOzs%2BO3Q8dDxwPHA8bDxEYXRhVmFsdWVGaWVsZDtEYXRhVGV4dEZpZWxkOz47bDxTbjtjb2RlTmFtZTs%2BPjs%2BO3Q8aTwzPjtAPFtTZWxlY3RdO1VTRDtLUlc7PjtAPDA7MjA4OzIwOTs%2BPjs%2BOzs%2BO3Q8dDxwPHA8bDxEYXRhVmFsdWVGaWVsZDtEYXRhVGV4dEZpZWxkOz47bDxTbjtjb2RlTmFtZTs%2BPjs%2BO3Q8aTw1PjtAPFtTZWxlY3RdO0hvdXI7V2VlaztNb250aDtZZWFyOz47QDwwOzIxMDsyMTE7MjEyOzIxMzs%2BPjs%2BOzs%2BO3Q8dDxwPHA8bDxEYXRhVmFsdWVGaWVsZDtEYXRhVGV4dEZpZWxkOz47bDxTbjtjb2RlTmFtZTs%2BPjs%2BO3Q8aTw1PjtAPFtTZWxlY3RdO0FueTtGdWxsLXRpbWU7UGFydC10aW1lO0ludGVybnNoaXA7PjtAPDA7MTk0OzE5NTsxOTY7MTk3Oz4%2BOz47Oz47dDx0PHA8cDxsPERhdGFWYWx1ZUZpZWxkO0RhdGFUZXh0RmllbGQ7PjtsPFNuO2NvZGVOYW1lOz4%2BOz47dDxpPDEwPjtAPFNlb3VsO0luY2hvbjtHeXVuZ2dpO0J1c2FuO0RhZWd1O0RhZWplb247SmVqdTtVbHNhbjtHd2FuZ2p1O090aGVyOz47QDwxNTc7MTU4OzE1OTsxNjA7MTYxOzQ5ODs0OTk7NTAwOzUwMTs1MDI7Pj47Pjs7Pjt0PHQ8cDxwPGw8RGF0YVZhbHVlRmllbGQ7RGF0YVRleHRGaWVsZDs%2BO2w8U247Y29kZU5hbWU7Pj47Pjt0PGk8Nz47QDxbU2VsZWN0XTtObyBIb3VzaW5nIE9mZmVyZWQ7U2luZ2xlIEhvdXNpbmcgLSBGdXJuaXNoZWQ7U2luZ2xlIEhvdXNpbmcgLSBVbmZ1cm5pc2hlZDtTaGFyZWQgSG91c2luZyAtIEZ1cm5pc2hlZDtTaGFyZWQgSG91c2luZyAtIFVuZnVybmlzaGVkO0hvdXNpbmcgQWxsb3dhbmNlIE9mZmVyZWQ7PjtAPDA7MjE0OzIxNTsyMTY7MjE3OzIxODsyMTk7Pj47Pjs7Pjt0PHQ8cDxwPGw8RGF0YVZhbHVlRmllbGQ7RGF0YVRleHRGaWVsZDs%2BO2w8U247Y29kZU5hbWU7Pj47Pjt0PGk8ND47QDxSb3VuZC10cmlwIEFpcmZhcmU7TWVkaWNhbCBJbnN1cmFuY2U7UGFpZCBWYWNhdGlvbjtZZWFyLWVuZCBCb251czs%2BO0A8MjIwOzIyMTsyMjI7MjIzOz4%2BOz47Oz47dDx0PHA8cDxsPERhdGFWYWx1ZUZpZWxkO0RhdGFUZXh0RmllbGQ7PjtsPFNuO2NvZGVOYW1lOz4%2BOz47dDxpPDU%2BO0A8S2luZGVyZ2FyZGVuO0dyYWRlIFNjaG9vbDtKdW5pb3IgSGlnaCBTY2hvb2w7VW5pdmVyc2l0eTtBZHVsdHM7PjtAPDIyNDsyMjU7MjI2OzIyNzsyMjg7Pj47Pjs7Pjs%2BPjs%2BPjtsPGNibExvY2F0aW9uSm9iOjA7Y2JsTG9jYXRpb25Kb2I6MTtjYmxMb2NhdGlvbkpvYjoyO2NibExvY2F0aW9uSm9iOjM7Y2JsTG9jYXRpb25Kb2I6NDtjYmxMb2NhdGlvbkpvYjo1O2NibExvY2F0aW9uSm9iOjY7Y2JsTG9jYXRpb25Kb2I6NztjYmxMb2NhdGlvbkpvYjo4O2NibExvY2F0aW9uSm9iOjk7Y2JsTG9jYXRpb25Kb2I6OTtjYmxCZW5lZml0OjA7Y2JsQmVuZWZpdDoxO2NibEJlbmVmaXQ6MjtjYmxCZW5lZml0OjM7Y2JsQmVuZWZpdDozO2NibFRlYWNoTGV2ZWw6MDtjYmxUZWFjaExldmVsOjE7Y2JsVGVhY2hMZXZlbDoyO2NibFRlYWNoTGV2ZWw6MztjYmxUZWFjaExldmVsOjQ7Y2JsVGVhY2hMZXZlbDo0Oz4%2BU5CesWSLW3hC%2B%2B2Ko7K2FocUhW8%3D&txtJobTitle=$ad_title&ddlJobCategory=188&ddlSalaryMoney=209&txtSalaryPay=$this->salary&ddlSalaryTime=212&txtOverPay=&txtStartDate=&txtPositionNumber=0&txtWorkHour=&ddlPositionType=195&ddlHouse=0&txtDescription=$ad_copy&txtJobName=$this->contact&txtJobMail=$this->email&btnAdd=Post+Job&__EVENTTARGET=&__EVENTARGUMENT=&rptList%3A_ctl1%3AbtnPay=Post" ;

	// set the post fields ...
	curl_setopt($ch, CURLOPT_POSTFIELDS,$form_viewstate);

	// store cookies
	curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
	curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
	curl_setopt($ch, CURLOPT_REFERER, $referer);

	curl_setopt($ch, CURLOPT_URL,"http://www.worknplay.co.kr/Employer/OfferAdd.aspx");

	// this returns the value of curl instead of printing it to the page
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = curl_exec ($ch);

	// Data is returned on success, error code on failure
	if (is_int ($data)) {
	  die ("cURL error: " . curl_error ($ch) . "\n");
	}

	### TAKE IN THE FORM DATA AND RETURN IT ####

	// now the data here will consist of the viewstate in raw form.
	// take it out and pare it down into normal format
	preg_match('/.*name="__VIEWSTATE".*value=\"([^"]*)\".*/',$data,$matches);
	$viewstate = $matches[1];

	// now, urlencode the data
	$viewstate_code = urlencode($viewstate);

	#### POST THE WAITING AD  ####	

	// the referer ...
	$referer = 'http://www.worknplay.co.kr/Employer/OfferList.aspx';

	// set the post fields ...
	curl_setopt($ch, CURLOPT_POSTFIELDS,"__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=${viewstate_code}&rptList%3A_ctl1%3AbtnPay=PostHTTP/1.1 200 OK");

	// store cookies
	curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
	curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
	curl_setopt($ch, CURLOPT_REFERER, $referer);

	curl_setopt($ch, CURLOPT_URL,"http://www.worknplay.co.kr/Employer/OfferList.aspx");

	// this returns the value of curl instead of printing it to the page
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	$data = curl_exec ($ch);

	// Data is returned on success, error code on failure
	if (is_int ($data)) {
	  die ("cURL error: " . curl_error ($ch) . "\n");
	}

      	curl_close ($ch);
        unset($ch);


      } // end of while

  }

  // post an ad on backpage.co.kr
  function backpage()
  {

    global $db;

    // job section, misc catgegory
    $section = '4373' ;
    $category = '4390' ;

    $sql = "SELECT campaign_name, ad_title, ad_amount, ad_copy 
             FROM free_campaigns 
             WHERE campaign_name LIKE '%backpage%'";

    if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem grabbing the campaign data','',__LINE__,__FILE__,$sql);
    }
	  

    while ( $row = $db->sql_fetchrow($result) )
      {
	
	// set the variables ...
	$ad_title = urlencode($row['ad_title']);
	$ad_copy = urlencode($row['ad_copy']);
	$email = urlencode($this->email);
			
	// set the worknplay specific salary
	$this->salary = '$2300';

	// array of each city in the backpage site
	$city_array = array('albuquerque','atlanta','austin','baltimore','boston','charlotte','chicago','chico'
			    ,'cincinnati','cleveland','columbus','dallas','denver','detroit','ftlauderdale','hartford'
			    ,'houstan','indianapolis','jacksonville','kc','lasvegas','littlerock','longisland','losangeles'
			    ,'memphis','miami','milwaukee','minneapolis','moneterey','nashville','newhaven','newyork'
			    ,'eastbay','oklahoma','orangecounty','orlando','philadelphia','phoenix','pittsburgh'
			    ,'portland','raleigh','reno','rochester','sacramento','saltlakecity','sanantonio','sandiego'
			    ,'sf','sanjose','santafe','seattle','spokane','springfield','stlouis','tampa','toronto','tucson'
			    ,'washingtondc','wilmington');

	$city_code = array ('abq','atl','aus','bwi','bos','clt','chi','cic'
			    ,'cvg','cle','cmh','dal','den','dwt','bpb','hfd'
			    ,'hou','ind','jax','kcp','las','lit','lis','lax'
			    ,'mem','mia','mke','msp','mry','bna','hvn','nyc'
			    ,'ebx','okc','sna','mco','phl','phx','pit'
			    ,'pdx','rdu','rno','roc','smf','slc','sat','sdo'
			    ,'san','sjc','saf','sea','geg','spr','stl','tpa','yyz','tus'
			    ,'iad','ilm');

	for ( $i=0 ; $i<count($city_array) ; $i++ )
	  {

	    // name of server you'll post to
	    $serverName=$city_array[$i].'.backpage.com';
	    $code=$city_code[$i];

	    // set referrer
	    $referer = "http://$serverName/gyrobase/classifieds/PostAd.html/$code/$serverName/?section=4373&category=4390&u=$code&serverName=$serverName";

	    // create curl object
	    $ch = curl_init();

	    if (!$ch) {
	      die ("Cannot initialize a new cURL handle\n");
	    }


### FILL OUT THE FORM AND SEND IT ###

	    // set the post fields ...
	    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
						       'u'=>$code,
						       'serverName'=>$serverName,
						       'section'=>$section,
						       'category'=>$category,
						       'disc'=>'',
						       'firstName'=>'',
						       'lastName'=>'',
						       'address'=>'',
						       'customerCity'=>'',
						       'state'=>'',
						       'zip'=>'',
						       'nextPage'=>'previewAd',
						       'title'=>$row['ad_title'],
						       'ad'=>$row['ad_copy'],
						       'regionOther'=>'Korea',
						       'category2'=>'',
						       'category3'=>'',
						       'salary'=>'$2300',
						       'education'=>'BA Degree',
						       'workStatus'=>'Full-time',
						       'email'=>'David@EnglishTeachingKorea.com',
						       'emailConfirm'=>'David@EnglishTeachingKorea.com',
						       'allowReplies'=>'Show Email',
						       'allowSolicitations'=>'No',
						       'autoRepost'=>'4',
						       'sponsorWeeks'=>'1',
						       'phone'=>'',
						       'name'=>''
						       ));

	    // store cookies
	    curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
	    curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
	    curl_setopt($ch, CURLOPT_REFERER, $referer);

	    $post_field = "http://posting.".$serverName."/gyrobase/classifieds/PostAd.html/$code/".$serverName;

	    curl_setopt($ch, CURLOPT_URL,$post_field);
	
	    // this returns the value of curl instead of printing it to the page
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_POST, 1);

	    $data = curl_exec ($ch);

	    // Data is returned on success, error code on failure
	    if (is_int ($data)) {
	      die ("cURL error: " . curl_error ($ch) . "\n");
	    }

            ##### PICK THE SECTION THAT YOU WILL POST TO ####

	    $referer = "http://$serverName/gyrobase/classifieds/PostAd.html/$code/$serverName/?section=4373&category=4390&u=$code&serverName=$serverName";

	    $post_field = "nextPage=processFreeAd&adKey=www-25db49bf0c17d955a9ac5ea870b1acc3-1164693085&u=$code&serverName=$serverName&section=4373&category=4390&title=$ad_title&regionOther=Korea&salary=%242300&education=BA+Degree&workStatus=Full-time&email=$email&emailConfirm=$email&allowReplies=Anonymous&autoRepost=4&sponsorWeeks=1&ad=$ad_copy";

	    // store cookies
	    curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);
	    curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
	    curl_setopt($ch, CURLOPT_REFERER, $referer);

	    curl_setopt($ch, CURLOPT_POSTFIELDS,$post_field);

	    $posting = "http://posting.".$serverName."/gyrobase/classifieds/PostAd.html/$code/".$serverName;

	    curl_setopt($ch, CURLOPT_URL,$posting);

	    // this returns the value of curl instead of printing it to the page
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_POST, 1);

	    $data = curl_exec ($ch);

	    echo $data;

	    // Data is returned on success, error code on failure
	    if (is_int ($data)) {
	      die ("cURL error: " . curl_error ($ch) . "\n");
	    }

	    curl_close ($ch);
	    unset($ch);

	  }

      }

  }


}




?>