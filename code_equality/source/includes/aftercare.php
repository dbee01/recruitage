<?php

/**
 * This class takes care of our aftercare program. I'll email the teachers individually and by name to help them along at certain times in their tenure here. I will email on birthdays, 4 days before they start, on their 2 week mark, just before chuseok etc ...
 *
 *                              aftercare.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 **/

class aftercare
{

  var $date, $date_arr, $year, $month, $day, $page_maker;
  
  // aftercare program constructor
  function aftercare()
  {               

    // take values from admin panel / db
    global $page_maker;

    $this->page_maker = $page_maker ;

    $this->date = date('Y-m-d');               

    // get the dates
    $this->date_arr = explode('-',$this->date);
    $this->year=$this->date_arr[0];
    $this->month=$this->date_arr[1];
    $this->day=$this->date_arr[2];
  
  }

  /**
  * Time offset from today 
  *
  */
  function time_offset($days, $months)
  {

    $month_time = $months * 30 * 24 * 60 * 60 ;
    $day_time = $days * 24 * 60 * 60 ;
    $firstWeek = time() + $month_time + $day_time ;

    return $firstWeek;

 }
  
  /**
  * start_date_mailer: list of trigger start dates, if today minus a certain period of time is the start_date of anyone in the teacher table then activate the trigger action for that date
  *
  */
  function start_date_mailer()
  {
    
    global $db, $lang;

    // 4 days before start date
     $four_Days_Offset=$this->time_offset(-4,0);
     $four_Days_Date=date('Y-m-d',$four_Days_Offset);

    // start date, welcome to Korea
    $start_date=$this->date;

    // one week into the contract ...
    $one_Week_Offset=$this->time_offset(7,0);
    $one_Week_Date=date('Y-m-d',$one_Week_Offset);

    // one month into the contract ...
    $one_Month_Offset=$this->time_offset(0,1);
    $one_Month_Date=date('Y-m-d',$one_Month_Offset);

    // three months into contract
    $three_Month_Offset=$this->time_offset(0,3);
    $three_Month_Date=date('Y-m-d',$three_Month_Offset);

    // six months into the contract ...
    $six_Month_Offset=$this->time_offset(0,6);
    $six_Month_Date=date('Y-m-d',$six_Month_Offset);

    // nine months into contract
    $nine_Month_Offset=$this->time_offset(0,9);
    $nine_Month_Date=date('Y-m-d',$nine_Month_Offset);

    // eleven months into contract ...
    $eleven_Month_Offset=$this->time_offset(0,11);
    $eleven_Month_Date=date('Y-m-d',$eleven_Month_Offset);

    // 12 months into contract
    $twelve_Month_Offset=$this->time_offset(0,12);
    $twelve_Month_Date=date('Y-m-d',$twelve_Month_Offset);

    // build the $start_date array
    // store the start name, the date in the array
    $start_array = array( 
		       array($this->page_maker['time_title1'],$this->page_maker['time_text1'],$four_Days_Date),
		       array($this->page_maker['time_title2'],$this->page_maker['time_text2'],$start_date), 
		       array($this->page_maker['time_title3'],$this->page_maker['time_text3'],$one_Week_Date), 
		       array($this->page_maker['time_title4'],$this->page_maker['time_text4'],$one_Month_Date), 
                       array($this->page_maker['time_title5'],$this->page_maker['time_text5'],$three_Month_Date), 
		       array($this->page_maker['time_title6'],$this->page_maker['time_text6'],$six_Month_Date),
		       array($this->page_maker['time_title7'],$this->page_maker['time_text7'],$nine_Month_Date), 
		       array($this->page_maker['time_title8'],$this->page_maker['time_text8'],$eleven_Month_Date), 
		       array($this->page_maker['time_title9'],$this->page_maker['time_text9'],$twelve_Month_Date)
	      	       );

    // go through array and see if any of the 
    // magic dates match today's date
    for($i=0;$i<count($start_array);$i++)
      {

	// magic date
	$arr_val=$start_array[$i][2];

	// select all teachers start dates
	$sql="SELECT name,email,recruiter FROM teachers WHERE start_date='$arr_val' AND aftercare=1 ";

	if ( !( $result = $db->sql_query($sql) ) )
	  {
	    message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql);
	  }
 
	// the messages are stored in template form, parse out the data
	while ( $row = $db->sql_fetchrow($result) )
	  {

	    @extract($row);

	    $headers = 'From:'.$recruiter.'@'.$board_config['sitename'] . "\r\n" .
	      'Reply-To:'.$recruiter.'@'. $board_config['sitename'] . "\r\n" .
	      'X-Mailer: PHP/' . phpversion();

	    // get the message subject and text
	    $text = $start_array[$i][1];
	    $subject = $start_array[$i][0];

	    // parse out and replace body text
	    $text = str_replace('{RECRUITER_NAME}',$recruiter,$text);
	    $text = str_replace('{TEACHER_NAME}',$name,$text);

	    // parse out and replace body text
	    $subject = str_replace('{RECRUITER_NAME}',$recruiter,$subject);
	    $subject = str_replace('{TEACHER_NAME}',$name,$subject);

	    // mail all the teachers in the db if today is a trigger date
	    if ( mail($email,$subject,$text,$headers) )
	      {

		// store the email first and add the header
		$mail = new mail_manager();

		// store the mail in the mail memory db 
		$mail->mail_store($text, $email,'');

	      }

	  }
      }
   
  }
  
  /**
   *  birthday_search : search for birthdays that match our present date offset. If we can find them, then send a mail
   *
   */
  function birthday_search()
  {

    global $db, $lang;
  
    // read out of registry table, store in birthdays array
    $birthdays=array( array( $this->page_maker['birthday_subject'], $this->page_maker['birthday_message'] ) );
  
    // match month and day only here. 
    // Obviously year doesn't matter...
    $date_similar="-".$this->month."-".$this->day;
     
    $sql = "select name,email,recruiter from teachers where date_of_birth LIKE '%$date_similar' AND aftercare=1 ";

    if ( !( $result = $db->sql_query($sql) ) )
    {
      message_die(GENERAL_ERROR,'Problem doing a birthday search','',__LINE__,__FILE__,$sql);
    }
   
    while ( $row = $db->sql_fetchrow($result) )
    {

      @extract($row);

      // the email here should be from whoever recruited the teacher...
      $headers = 'From:'.$recruiter.'@englishteachingkorea.com' . "\r\n" .
	'Reply-To:'.$recruiter.'@englishteachingkorea.com' . "\r\n" .
	'X-Mailer: PHP/' . phpversion();

      // get the message subject and text
      $text = $birthdays[0][1];
      $subject = $birthdays[0][0];

      // parse out and replace body text
      $text = str_replace('{RECRUITER_NAME}',$recruiter,$text);
      $text = str_replace('{TEACHER_NAME}',$name,$text);

      // parse out and replace body text
      $subject = str_replace('{RECRUITER_NAME}',$recruiter,$subject);
      $subject = str_replace('{TEACHER_NAME}',$name,$subject);

      if ( mail($email,$subject,$text,$headers) )
	{

	    // store the email first and add the header
	    $mail = new mail_manager();

	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');

	}

    }
 
  }


  /**
   * holiday search: search through for holidays that match our date offset, then email the successful candidates
   *
   */
  function holiday_search()
  {

   global $db, $lang;

    // go through page_maker and put any fixed date holidays into an array
   $holidays = array(
                       array($this->page_maker['fixed_title1'],
			     $this->page_maker['fixed_text1'],
			     $this->page_maker['fixed_date1']),		      
		       array($this->page_maker['fixed_title2'],
			     $this->page_maker['fixed_text2'],
			     $this->page_maker['fixed_date2']),
		       array($this->page_maker['fixed_title3'],
			     $this->page_maker['fixed_text3'],
			     $this->page_maker['fixed_date3']), 
		       array($this->page_maker['fixed_title4'],
			     $this->page_maker['fixed_text4'],
			     $this->page_maker['fixed_date4']), 
		       array($this->page_maker['fixed_title5'],
			     $this->page_maker['fixed_text5'],
			     $this->page_maker['fixed_date5']), 		      
		       array($this->page_maker['fixed_title6'],
			     $this->page_maker['fixed_text6'],
			     $this->page_maker['fixed_date6']),
		       array($this->page_maker['fixed_title7'],
			     $this->page_maker['fixed_text7'],
			     $this->page_maker['fixed_date7']), 
		       array($this->page_maker['fixed_title8'],
			     $this->page_maker['fixed_text8'],
			     $this->page_maker['fixed_date8']), 
		       array($this->page_maker['fixed_title9'],
			     $this->page_maker['fixed_text9'],
			     $this->page_maker['fixed_date9']), 
		       array($this->page_maker['fixed_title10'],
			     $this->page_maker['fixed_text10'],
			     $this->page_maker['fixed_date10']), 
		       array($this->page_maker['fixed_title11'],
			     $this->page_maker['fixed_text11'],
			     $this->page_maker['fixed_date11']), 
		       array($this->page_maker['fixed_title12'],
			     $this->page_maker['fixed_text12'],
			     $this->page_maker['fixed_date12']),
		       array($this->page_maker['fixed_title13'],
			     $this->page_maker['fixed_text13'],
			     $this->page_maker['fixed_date13']),
		       array($this->page_maker['fixed_title14'],
			     $this->page_maker['fixed_text14'],
			     $this->page_maker['fixed_date14']),
		       array($this->page_maker['fixed_title15'],
			     $this->page_maker['fixed_text15'],
			     $this->page_maker['fixed_date15'])
		       );
    
   // for each holiday, check if it compares...
    for($i=0;$i<count($holidays);$i++)
      {

      	if( $this->date == $holidays[$i][2] )
	  {

	    // how do I know what teachers to take out ?
	    $sql = "select name,email,recruiter from teachers where aftercare=1" ;

	    if ( !( $result = $db->sql_query($sql) ) )
	     {
	    	message_die(GENERAL_ERROR,'Problem doing a holiday search','',__LINE__,__FILE__,$sql);
	     }

	    while ( $row = $db->sql_fetchrow($result) )
	      {

		@extract($row);

		// headers have to be done here after the extract.
		$headers = 'From:'.$recruiter.'@'. $board_config['sitename'] . "\r\n" .
	                'Reply-To:'.$recruiter.'@'. $board_config['sitename'] . "\r\n" .
	       	        'X-Mailer: PHP/' . phpversion();

		// parse out and replace body text
		$text = str_replace('{RECRUITER_NAME}',$recruiter,$holidays[$i][1]);
		$text = str_replace('{TEACHER_NAME}',$name,$text);

		// parse out and replace body text
		$subject = str_replace('{RECRUITER_NAME}',$recruiter,$holidays[$i][0]);
		$subject = str_replace('{TEACHER_NAME}',$name,$subject);

		// mail all the teachers in the db if today is a trigger date
		if ( mail($email,$subject,$text,$headers) )
		  {

		    // store the email first and add the header
		    $mail = new mail_manager();

		    // store the mail in the mail memory db 
		    $mail->mail_store($text, $email,'');

		  }

	      }
	  }
      }

  }



}
?>