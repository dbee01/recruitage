<?php

/**
 * This will handle the picture upload it will take the pic from the temp file, check it's extension, and maybe resize it. Resizing can be a memory hog, so I've bumped up mem in php.ini
 *
 *                               picture.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

class picture
{
// Not a true class ... 

       var $save_dir;                    //where file will be saved
       var $file_name="spacer.gif";        //default file name initially
       var $error_message="";            //string to be output if neccesary
       var $width;                        //height of final image
       var $height;                      //width of final image

       function picture($save_directory, $file_array, $max_width, $max_height, $file_name, $candidate_name='')
       {
               
               $this->save_dir = $save_directory;               
               $this->width =    $max_width;
               $this->height =  $max_height;
               
               //--change filename to time - make it unique 
               $temp_filename = $file_array[$file_name]["name"];
               $ext = explode('.',$temp_filename);
               $ext = $ext[count($ext)-1];
               $temp_filename = $candidate_name . time() . "." . $ext;

               //--check that it's a jpeg or gif
               $ext = strtolower($ext);
               
               if (preg_match('/^(gif|jpe?g)$/',$ext)) {

                       // resize in proportion
                       list($width_orig, $height_orig) = getimagesize($file_array[$file_name]['tmp_name']);
                    
                       if ($this->width && ($width_orig < $height_orig)) {
                               $this->width = ($this->height / $height_orig) * $width_orig;
                       } else {
                               $this->height = ($this->width / $width_orig) * $height_orig;
                       }

                       $image_p = imagecreatetruecolor($this->width, $this->height);                       

                       //handle gifs and jpegs separately
                       if($ext=='gif')
                       {
                           $image = imagecreatefromgif($file_array[$file_name]['tmp_name']);                           
                           imagecopyresampled($image_p, $image, 0, 0, 0, 0, $this->width, $this->height, $width_orig, $height_orig);
                           imagegif($image_p, $this->save_dir.$temp_filename, 80);
						     }
                       else
                       {

			 // running out of memory here when resizing the large images...
			 $imageInfo = getimagesize($file_array[$file_name]['tmp_name']);
			 $MB = 1048576;  // number of bytes in 1M
			 $K64 = 65536;    // number of bytes in 64K
			 $TWEAKFACTOR = 1.5;  // Or whatever works for you
			 $memoryNeeded = round( ( $imageInfo[0] * $imageInfo[1]
                                           * $imageInfo['bits']
                                           * $imageInfo['channels'] / 8
                             + $K64
						  ) * $TWEAKFACTOR
						);

			 //ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
			 //Default memory limit is 8MB so well stick with that.
			 //To find out what yours is, view your php.ini file.
			 $memoryLimitMB = 8;
			 $memoryLimit = $memoryLimitMB * $MB;
			 if (function_exists('memory_get_usage') &&
			     memory_get_usage() + $memoryNeeded > $memoryLimit)
			   {

			     $newLimit = $memoryLimitMB + ceil( ( memory_get_usage()
                                           + $memoryNeeded
                                           - $memoryLimit
								  ) / $MB
								);

			     $newLimit = $newLimit+3000000; 

			     ini_set( 'memory_limit', $newLimit . 'M' );
	
			   }


    	                  
			   $image = imagecreatefromjpeg($file_array[$file_name]['tmp_name']);                      
	
			   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $this->width, $this->height, $width_orig, $height_orig);   
                           imagejpeg($image_p, $this->save_dir.$temp_filename, 80);
                       }

                       imagedestroy($image_p);
                       imagedestroy($image);
                       

                       $this->filename=$temp_filename;
                 

               }else{
                       $this->error_message.="<br> file is not a jpeg or gif picture <br>";
               }
       }

}
?>