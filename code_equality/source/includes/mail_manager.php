<?php

/**
 * This mail manager basically processes all the mail for the system it will add mail headers and footers to outgoing mail it will also store mail in the database ...
 *
 *                              mail_manager.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

class mail_manager {

  var $date ;
  var $email ;
  var $footer = "NOTICE: In order to maintain the high standards of EnglishTeachingKorea.com, these emails will be stored in our database and may be monitored to ensure their veracity and goodwill. \x0D\x0A\x0D\x0APlease report any complaints, comments or suggestions about our service to abuse AT EnglishTeachingKorea.com";
  var $header = '/######### ENGLISH TEACHING KOREA - MESSAGE HISTORY ##########/' ;

  /**
   * Mail manager constructor, sets todays date.
   *
   */
  function mail_manager()
  { 
    
    $this->date=date('Y-m-d');

  }

  /**
   * If there is already a mail history then just add the relevant mail text to the database. If not then create a record and then add the relevant mail text... 
   *
   */
  function mail_store( $text, $mail, $candidate_name )
  {
    
    global $db, $userdata;

    if( $candidate_name == '' )
    {
      $candidate_name = $userdata['username'];
    }

    // first check to see if the mail record exists...
    $sql = "SELECT * from " . MAIL_MEMORY . " where email='$mail'";

    if (! ($result = $db->sql_query($sql)) ) 
    {
    	 message_die(CRITICAL_ERROR,'Problem with the mail memory table','',__LINE__,__FILE__,$sql);
    }      

    $row = $db->sql_fetchrow($result) ;
   
    // create record
    if ( $row == '' )
      {
 
	$sql = "INSERT INTO " . MAIL_MEMORY . " (email) VALUES ('$mail') ";

	if (! ($result = $db->sql_query($sql)) ) 
	  {
	    message_die(CRITICAL_ERROR,'Problem with the mail memory table','',__LINE__,__FILE__,$sql);
	  }      

      }

    // now update message with new message
    $text = nl2br($text);
    $text = "\x0D\x0A\x0D\x0A" . "# On " . $this->date . " " . $candidate_name . " wrote \x0D\x0A\x0D\x0A" . $text . "\x0D\x0A\x0D\x0A" . $this->notice ;

    $sql= "UPDATE " . MAIL_MEMORY . " SET message=CONCAT(message,'$text') WHERE email='$mail' " ;

    if (! ($result = $db->sql_query($sql)) ) 
    {
    	 message_die(CRITICAL_ERROR,'Problem with the mail memory table','',__LINE__,__FILE__,$sql);
    }

  }

  // SO I'M GOING TO CHANGE THIS FUNCTION TO BE MAIL OUTGOING (ADD ON THE HISTORY IN THE HEADER AND FOOTER)
  // AND MAIL INCOMING (REMOVE THE HEADER AND THE FOOTER AND ALL THE TEXT IN BETWEEN) AND POSSIBLY THE OTHER
  // NON ESSENTIAL TEXT AS WELL ... INCOMING MAIL FORMAT ... MAIL_HISTORY THEN MESSAGE THEN OUR HISTORY ADD-ON

  /** 
   * Look up email address, add on the mail history for that email address inside of the mail headers
   *
   */
  function mail_process_out($text, $mail)
  {

    global $userdata, $db ;
   
    $sql = "SELECT message FROM " . MAIL_MEMORY . " WHERE email LIKE '$mail' " ;

    if (!($result = $db->sql_query($sql)))
    {
	 message_die(CRITICAL_ERROR, 'Cannot connect to database for mail memory', '', __LINE__,__FILE__,$sql);
    }	
    
    $row = $db->sql_fetchrow($result) ;
    $mail_history=$row['message'];
    $mail_history=br2nl($mail_history);
    $mail_history= "\x0D\x0A\x0D\x0A" . $this->header . "\x0D\x0A\x0D\x0A" . $mail_history . "\x0D\x0A\x0D\x0A" . $this->footer ;
 
    $text .= $mail_history; 

    return $text ;

  }

  /**
   * Check whether this person has had a personal mail response ?  
   *
   */
  function mail_response($mail)
  {

    global $userdata, $db ;
   
    $sql = "SELECT message FROM " . MAIL_MEMORY . " WHERE email LIKE '$mail' " ;

    if (!($result = $db->sql_query($sql)))
    {
	 message_die(CRITICAL_ERROR, 'Cannot connect to database for mail memory', '', __LINE__,__FILE__,$sql);
    }	
    
    $row = $db->sql_fetchrow($result) ;
    $mail_history=$row['message'];

    return $mail_history ;

  }

  /**
   * Take in the text and remove all the history and possibly the gmail, yahoo, hanmail text ...
   *
   */ 
  function mail_process_in($text, $mail)
  {

    global $userdata, $db ;
  
    //    $str_arr = preg_split($this->header, $text, -1, PREG_SPLIT_OFFSET_CAPTURE);
    //    $message1=$str_arr[1][0];
    //    $keywords = preg_split($this->footer, $message1);

    return $text ;
      
  }

  /** 
   * Read all the stored mail history from the db for a certain email.
   *
   */
  function cp_mail_reader($mail)
  {

    global $userdata, $db ;
   
    $sql = "SELECT message FROM " . MAIL_MEMORY . " WHERE email LIKE '$mail' " ;

    if (!($result = $db->sql_query($sql)))
    {
	 message_die(CRITICAL_ERROR, 'Cannot connect to database for mail memory', '', __LINE__,__FILE__,$sql);
    }	
    
    $row = $db->sql_fetchrow($result) ;

    // grab the email history
    $mail_history=$row['message'];
 
    return $mail_history ;

  }

}

?>