<?php

/**
 * Try to handle most of the timezone issues here remember that this needs php > 5.0 
 *
 *                               timezone.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

class timezone
{

  var $now_time='';
  var $user_zone='';
  var $server_zone='';

  /**
   * Build the timezone constructor
   *
   */
  function timezone()
  {
    global $userdata ;
    $this->now_time=time();
    $this->user_zone=$userdata['user_timezone'];
    $this->server_zone= SERVER_TIMEZONE ;

}
  
  /**
   * Take a string in mktime format (hour,min,sec,month,day,year) and return a timestamp for user timezone
   *
   */ 
  function return_stamp($timestring)
  {
   
     putenv($this->user_zone);

     // timestring is clean
     $timearray=explode(',',$timestring);
     $time=mktime( $timearray[0], $timearray[1], $timearray[2], $timearray[3], $timearray[4], $timearray[5] ) ;
     
     putenv($this->server_zone);
    
     return $time ;
  }

  /** 
   * Take a timestamp, a zone, and a format and return time string
   *
   */
  function return_format($timestring, $zone, $format)
  {

    putenv($zone);
    $val=date($format,$timestring);    
    putenv($this->server_zone);
    
    return $val ;
  }

  /**
   * This if for the front page check if the time is valid in Korea for the callback.
   *
   */
  function time_valid($from_time='10',$until_time='23',$timezone='TZ=Asia/Seoul')
  {
    
    // easier to just do this here ...
    putenv($timezone);

    $now_time = date('H',time());

    putenv(SERVER_TIMEZONE);

    // if now_time is between the acceptable hours, then output 0
    if ( ( $now_time > $from_time ) && ( $now_time < $until_time ) )
      {
	return 0;
      }
    else 
      {
	
	// the time we're going to return
	$ret_time = 0;

	// let's try to calculate the time difference here....
	if ( $now_time > $until_time) {  $ret_time = ( 24 - $now_time + $from_time  ) ;  }
	else if ( $now_time < $from_time ) { ( $ret_time = $from_time - $now_time ) ; } 

	// return the time
	return $ret_time;

      }    

  }

  /**
   * Take values from database, ascertain whether now is in time boundaries 
   *
   */
  function time_fetch($from_time,$until_time,$timezone)
  {
    // values will come out of db as is ...
    $timezone=$this->zone_equiv($timezone);
    
    // easier to just do this here ...
    putenv($timezone);
    $from_time=mktime($from_time,0,0);
    $until_time=mktime($until_time,0,0);
    putenv(SERVER_TIMEZONE);
   
    $time_array=array();
    
    // have to compare timestamps here, timestrings won't work
    if ( ( $this->now_time > $from_time ) && ( $this->now_time < $until_time) )
    {
      // convert into timestrings at the last minute
      $from_time=$this->return_format($from_time,$this->user_zone,'H');
      $until_time=$this->return_format($until_time,$this->user_zone,'H');

      // instead of ouputting a now time in red,
      // we'll put a . in front of it ...
      $from_time = 'red'.$from_time;

      $time_array['from_time']=$from_time;
      $time_array['until_time']=$until_time;
      
      return $time_array;

    }   
    else
    {
      // convert to timestrings at the last minute
      $from_time=$this->return_format($from_time,$this->user_zone,'H');
      $until_time=$this->return_format($until_time,$this->user_zone,'H');
      
      $time_array['from_time']=$from_time;
      $time_array['until_time']=$until_time;
      
      return $time_array ;
      
    }

  }
  
  /**
   * Take timestring in mktime format and return hour in server zone
   *
   */
  function format_to_zone($from_time,$timezone,$format)
  {

    putenv($timezone);
    $time=mktime($from_time,0,0);    
    putenv($this->server_zone);

    return date($format,$time);

  }
  
  /**
   * Take timestring in mktime format and return timestamp in server zone
   *
   */
  function zone_in_server_time($timestring,$zone)
  {
    
     putenv($zone);
     $time=mktime( $timestring ) ;
     putenv($this->server_zone);

     return $time ;
  }

  /**
   * Take a timestring in mktime format and return time string in chosen zone tme
   *
   */
  function zone_switch($from_time,$from_zone,$to_zone)
  {

    putenv($from_zone);

    // remember to split up time for mktime
    $from_array=explode(',',$from_time);
    $from_stamp=mktime($from_array[0],$from_array[1]);

    putenv($to_zone);

    $equiv_time=date('D: H:i',$from_stamp);

    putenv($this->server_zone);
    
    return $equiv_time ;
  }

  /**
   * Enumerate last 10 days given a timestamp
   *
   */
  function last_ten_days()
  {

    global $board_config ;

    // function to declare timestamps for day's end and beginning
    putenv($board_config['board_timezone']);

    // timestamp
    $day = date('j');
    $month = date('n');
    $year = date('Y');

    // get timestamp for first second of this day
    // will be used for SQL queries ...
    $stamp0 = mktime(1,0,0,$month,$day,$year);    

    $today = date('l',$stamp0);

    for ($i=1 ; $i < 10 ; $i++ )
      {
	
	$stamp_day = 'stamp_day'.$i ;
	$$stamp_day = $stamp0 - ( $i * ( 60*60*24 ) ) ;
	
      }

    // find the days by name...
    
    $day1 = date('l', ( $stamp_day1 ) );
    $day2 = date('l', ( $stamp_day2 ) );
    $day3 = date('l', ( $stamp_day3 ) );
    $day4 = date('l', ( $stamp_day4 ) );
    $day5 = date('l', ( $stamp_day5 ) );
    $day6 = date('l', ( $stamp_day6 ) );
    $day7 = date('l', ( $stamp_day7 ) );
    $day8 = date('l', ( $stamp_day8 ) );
    $day9 = date('l', ( $stamp_day9 ) );

    $day_arr = array('day2'=>$day2,'day3'=>$day3,'day4'=>$day4,'day5'=>$day5,'day6'=>$day6,'day7'=>$day7,'day8'=>$day8,'day9'=>$day9,
	             'stamp_day0'=>$stamp0,'stamp_day1'=>$stamp_day1,'stamp_day2'=>$stamp_day2,'stamp_day3'=>$stamp_day3,
		     'stamp_day4'=>$stamp_day4,'stamp_day5'=>$stamp_day5,'stamp_day6'=>$stamp_day6,'stamp_day7'=>$stamp_day7,
		     'stamp_day8'=>$stamp_day8,'stamp_day9'=>$stamp_day9);

    putenv(SERVER_TIMEZONE);

    return $day_arr ;
  }

  /*
   * Take a zone and give back the php equiv TZ env var
   *
   */
  function zone_equiv($zone_string)
  {
    switch ($zone_string)
    {
         case '1' : $zone='TZ=America/Anchorage' ;break; 
         case '2': $zone='TZ=America/Los_Angeles'  ;break;
         case '3' : $zone='TZ=America/Edmonton'; break ;
         case '4' : $zone='TZ=America/Winnipeg'; break ;
         case '5' : $zone='TZ=America/Toronto'; break ;
         case '6' : $zone='TZ=America/St_Johns'; break ;
         case '7' : $zone='TZ=Europe/London'; break ;
         case '8' : $zone='TZ=Europe/Berlin'; break ;
         case '9' : $zone='TZ=Asia/Bangkok'; break ;
         case '10' : $zone='TZ=Australia/Perth'; break ;
         case '11' : $zone='TZ=Asia/Seoul'; break ;
         case '12' : $zone='TZ=Australia/Sydney'; break ;
	 case '13' : $zone='TZ=Pacific/Auckland'; break ;
         default: break;
    } 

    return $zone;
  }


  /* 
   * Enter user's timezone, get back an option key for the timetable
   *
   */
  function timezone_to_hour_select($user_zone)
    {

      putenv($user_zone);

      // find the hour in the user's timezone
      $hour=date('H',time()) ;

      putenv($this->server_zone);

      // switch the hour to the form setting on the timetable form
      switch($hour)
	{

	case(1):  $hour_select='{TIMEZONE_HOUR_SELECTED1}' ; break;
	case(2):  $hour_select='{TIMEZONE_HOUR_SELECTED2}' ; break;
	case(3):  $hour_select='{TIMEZONE_HOUR_SELECTED3}' ; break;
	case(4):  $hour_select='{TIMEZONE_HOUR_SELECTED4}' ; break;
	case(5):  $hour_select='{TIMEZONE_HOUR_SELECTED5}' ; break;
	case(6):  $hour_select='{TIMEZONE_HOUR_SELECTED6}' ; break;
	case(7):  $hour_select='{TIMEZONE_HOUR_SELECTED7}' ; break;
	case(8):  $hour_select='{TIMEZONE_HOUR_SELECTED8}' ; break;
	case(9):  $hour_select='{TIMEZONE_HOUR_SELECTED9}' ; break;
	case(10):  $hour_select='{TIMEZONE_HOUR_SELECTED10}' ; break;
	case(11):  $hour_select='{TIMEZONE_HOUR_SELECTED11}' ; break;
	case(12):  $hour_select='{TIMEZONE_HOUR_SELECTED12}' ; break;
	case(13):  $hour_select='{TIMEZONE_HOUR_SELECTED13}' ; break;
	case(14):  $hour_select='{TIMEZONE_HOUR_SELECTED14}' ; break;
	case(15):  $hour_select='{TIMEZONE_HOUR_SELECTED15}' ; break;
	case(16):  $hour_select='{TIMEZONE_HOUR_SELECTED16}' ; break;
	case(17):  $hour_select='{TIMEZONE_HOUR_SELECTED17}' ; break;
	case(18):  $hour_select='{TIMEZONE_HOUR_SELECTED18}' ; break;
	case(19):  $hour_select='{TIMEZONE_HOUR_SELECTED19}' ; break;
	case(20):  $hour_select='{TIMEZONE_HOUR_SELECTED20}' ; break;
	case(21):  $hour_select='{TIMEZONE_HOUR_SELECTED21}' ; break;
	case(22):  $hour_select='{TIMEZONE_HOUR_SELECTED22}' ; break;
	case(23):  $hour_select='{TIMEZONE_HOUR_SELECTED23}' ; break;
	case(24):  $hour_select='{TIMEZONE_HOUR_SELECTED24}' ; break;

	default: break;

	}

      return $hour_select ;

    }


  /**
   * Enter user's timezone, get back an option key for the timetable
   *
   */ 
  function timezone_to_min_select($user_zone)
    {

      putenv($user_zone);

      // find the hour in the user's timezone
      $min= (int) round(date('i',time())) ;
      

      putenv($this->server_zone);

      // switch the hour to the form setting on the timetable form
      switch(true)
	{

	case($min>55):  $min_select='{TIMEZONE_MIN_SELECTED55}' ; break;
	case($min>50):  $min_select='{TIMEZONE_MIN_SELECTED50}' ; break;
	case($min>45):  $min_select='{TIMEZONE_MIN_SELECTED45}' ; break;
	case($min>40):  $min_select='{TIMEZONE_MIN_SELECTED40}' ; break;
	case($min>35):  $min_select='{TIMEZONE_MIN_SELECTED35}' ; break;
	case($min>30):  $min_select='{TIMEZONE_MIN_SELECTED30}' ; break;
	case($min>25):  $min_select='{TIMEZONE_MIN_SELECTED25}' ; break;
	case($min>20):  $min_select='{TIMEZONE_MIN_SELECTED20}' ; break;
	case($min>15):  $min_select='{TIMEZONE_MIN_SELECTED15}' ; break;
	case($min>10):  $min_select='{TIMEZONE_MIN_SELECTED10}' ; break;
	case($min>05):  $min_select='{TIMEZONE_MIN_SELECTED05}' ; break;
	case($min>00):  $min_select='{TIMEZONE_MIN_SELECTED00}' ; break;

	default: break;

	}

      return $min_select ;

    }


  /**
   * Enter user's timezone, get back an option key for the timetable.
   *
   */ 
  function timezone_to_select($timezone)
    {

      // output the timeselect
      $time_select = '';

      switch($timezone)
	{

	case ('TZ=America/Anchorage') : $time_select='{TIMEZONE_HOME_SELECTED1}' ; break;
	case ('TZ=America/Los_Angeles') : $time_select='{TIMEZONE_HOME_SELECTED2}' ; break;
	case ('TZ=America/Edmonton') : $time_select='{TIMEZONE_HOME_SELECTED3}' ; break;
	case ('TZ=America/Winnipeg') : $time_select='{TIMEZONE_HOME_SELECTED4}' ; break;
	case ('TZ=America/Toronto') : $time_select='{TIMEZONE_HOME_SELECTED5}' ; break;
	case ('TZ=America/St_Johns') : $time_select='{TIMEZONE_HOME_SELECTED6}' ; break;
	case ('TZ=Europe/London') : $time_select='{TIMEZONE_HOME_SELECTED7}' ; break;
	case ('TZ=Europe/Berlin') : $time_select='{TIMEZONE_HOME_SELECTED8}' ; break;
	case ('TZ=Asia/Bangkok') : $time_select='{TIMEZONE_HOME_SELECTED9}' ; break;
	case ('TZ=Australia/Perth') : $time_select='{TIMEZONE_HOME_SELECTED10}' ; break;
	case ('TZ=Asia/Seoul') : $time_select='{TIMEZONE_HOME_SELECTED11}' ; break;
	case ('TZ=Australia/Sydney') : $time_select='{TIMEZONE_HOME_SELECTED12}' ; break;
	case ('TZ=Pacific/Auckland') : $time_select='{TIMEZONE_HOME_SELECTED13}' ; break;
	default: break;

	}

      return $time_select ;

    }

}

?>
