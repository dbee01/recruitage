<?php

/**
 * Interface with the voipbuster site... callback, sms, click to call etc...
 *
 *                              voipbuster.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

class voipbuster
{

  // log into sipdiscount, grab the cookies and then send a 
  // call order to that address ...

  // set user agent
  var $agent ;

  // set referrer
  var $referer ;

  // create a cookie jar to store cookies ...
  var $cookie_jar ;

  // set your sip account username and password
  var $user ;
  var $pass ;

  /**
   * Make the constructor
   *
   */
  function voipbuster() 
    {

      // user and pass
      $this->user='';
      $this->pass='';

      // cookie jar
      $this->cookie_jar = "./tmp/cookiejar.txt";

      // referer
      $this->referer = '' ;
      
      // agent
      $this->agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)';

    }

  
  /**
   * Login to the site and store the session cookie
   *
   */
  function login()
    {

      $ch = curl_init();
      
      if (!$ch) {
	die ("Cannot initialize a new cURL handle\n");
      }

      // set the referer
      $this->referer = "https://myaccount.voipbuster.com/clx/index.php?user=$user&pass=$pass";

      //set the login page...
      //curl_setopt($ch, CURLOPT_URL,"https://myaccount.sipdiscount.com/clx/index.php");

      curl_setopt($ch, CURLOPT_URL,"https://myaccount.voipbuster.com/clx/index.php");

      // store cookies
      curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_jar);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);

      curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
      curl_setopt($ch, CURLOPT_REFERER, $this->referer);

      // this returns the value of curl instead of printing it to the page
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);

      //curl_setopt($ch, CURLOPT_VERBOSE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "part=menu&username=$this->user&password=$this->pass");

      // don' verify ssl host
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
      
      $data = curl_exec ($ch);

      // Data is returned on success, error code on failure
      if (is_int ($data)) {
	die ("cURL error: " . curl_error ($ch) . "\n");
      }

    }

  /** 
   * Use this to curl into voipbuster servers and arrange a callback
   *
   */
  function callback($call_from,$call_to)
    {

      $this->referer = 'https://myaccount.voipbuster.com/clx/webcalls2.php?panel=true';

      $ch = curl_init();

      if (!$ch) {
	die ("Cannot initialize a new cURL handle\n");
      }

      //set the login page...
      curl_setopt($ch, CURLOPT_URL,"https://myaccount.voipbuster.com/clx/webcalls2.php");

      // store cookies
      //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);

      curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
      curl_setopt($ch, CURLOPT_REFERER, $this->referer);

      // this returns the value of curl instead of printing it to the page
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_POST, 1);

      //curl_setopt($ch, CURLOPT_VERBOSE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "action=initcall&panel=true&anrphonenr=$call_from&bnrphonenr=$call_to");

      // don' verify ssl host
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);

      $data = curl_exec ($ch);

      // Data is returned on success, error code on failure
      if (is_int ($data)) {
	die ("cURL error: " . curl_error ($ch) . "\n");
      }

      curl_close ($ch);
      unset($ch);

    }

  /** 
   * Send an SMS message...
   *
   */
  function sms($message,$number)
  {

    // urlencode the message
    $message = urlencode($message);

    $this->referer = 'Referer: https://myaccount.voipbuster.com/clx/websms.php';

    $ch = curl_init();
    
    if (!$ch) {
      die ("Cannot initialize a new cURL handle\n");
    }

    //set the login page...
    curl_setopt($ch, CURLOPT_URL,'https://myaccount.voipbuster.com/clx/websms.php');

    // store cookies
    //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_jar);

    curl_setopt($ch, CURLOPT_USERAGENT, $this->agent);
    curl_setopt($ch, CURLOPT_REFERER, $this->referer);
    
    // this returns the value of curl instead of printing it to the page
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_POST, 1);

    //curl_setopt($ch, CURLOPT_VERBOSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "action=send&panel=&message=$message&bnrphonenumber=$number");

    // don' verify ssl host
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);

    $data = curl_exec ($ch);

    // parse the return text for an error string...
    if (  stristr($data,'An error()has occurred')  )
      {
	echo 'Error in sending SMS';
      }
    else
      {
	echo '';
    }

    // Data is returned on success, error code on failure
    if (is_int ($data)) {
      die ("cURL error: " . curl_error ($ch) . "\n");
    }

    curl_close ($ch);
    unset($ch);

  }

}


?>