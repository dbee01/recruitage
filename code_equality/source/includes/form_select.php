<?php

/**
 * There are two form_select.php. This form basically is an add in to the gallery forms. It is used to turn a gallery form into an update form whereby the recruiter can adjust the data of their clients and know what the previous client data value was ....
 *  
 *                            form_select.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}

// recruiter output selected options ...
if (stristr ( $_SERVER['PHP_SELF'] , 'recruiter_gallery.php' ) )
{ 

  // print form flag helps with email decoder for print_form_update 
  $print_form_flag = 1 ;

 if($row['kindergarten']){ $template->assign_var('RECRUITER_STUDENTS_KINDERGARTEN_SELECTED' , "checked" ) ; }
 if($row['elementary']){ $template->assign_var('RECRUITER_STUDENTS_ELEMENTARY_SELECTED' , "checked" ) ; }
 if($row['middle_school']){ $template->assign_var('RECRUITER_STUDENTS_MIDDLE_SCHOOL_SELECTED' , "checked"); } 		   
 if($row['high_school']){ $template->assign_var('RECRUITER_STUDENTS_HIGH_SCHOOL_SELECTED' , "checked" ) ; }
 if($row['adults']){ $template->assign_var('RECRUITER_STUDENTS_ADULTS_SELECTED' , "checked" ) ;  }

 // parse the arrival
 $select_arrival = preg_replace('/^([0-9]*-)/','',$row['arrival']);
 $arrival_block = $form_section->get_arrival_list($select_arrival);
 $template->assign_var("ARRIVAL_BLOCK", $arrival_block );		
		
  // accomodation block
  $accomodation_block = $form_section->get_accomodation_list($row['accomodation']);
  $template->assign_var("ACCOMODATION_BLOCK", $accomodation_block);

  // salary block
  $salary_block = $form_section->get_salary_list($row['salary']);
  $template->assign_var("SALARY_BLOCK", $salary_block);
	
  // location block
  $location_block = $form_section->get_location_options($row['city']);
  $template->assign_var("LOCATION_BLOCK", $location_block);

  // number teachers block
  $number_teachers_block = $form_section->get_number_teachers_list($row['number_teachers']);
  $template->assign_var("NUMBER_TEACHERS_BLOCK", $number_teachers_block);

}

// SCHOOL 
else if ( stristr ( $_SERVER['PHP_SELF'] , 'school_gallery.php' ) )
{

  // print form flag helps with email decoder for print_form_update 
  $print_form_flag = 1 ;

  if($row['kindergarten']){ $template->assign_var('SCHOOL_STUDENTS_KINDERGARTEN_SELECTED' , "checked" ) ; }
  if($row['elementary']){ $template->assign_var('SCHOOL_STUDENTS_ELEMENTARY_SELECTED' , "checked" ) ; }
  if($row['middle_school']){ $template->assign_var('SCHOOL_STUDENTS_MIDDLE_SCHOOL_SELECTED' , "checked" ) ; }
  if($row['high_school']){ $template->assign_var('SCHOOL_STUDENTS_HIGH_SCHOOL_SELECTED' , "checked" ) ; }
  if($row['adults']){ $template->assign_var('SCHOOL_STUDENTS_ADULTS_SELECTED' , "checked" ) ;  }
 		
  // parse the arrival
  $select_arrival = preg_replace('/^([0-9]*-)/','',$row['arrival']);
  $arrival_block = $form_section->get_arrival_list($select_arrival);
  $template->assign_var("ARRIVAL_BLOCK", $arrival_block );

  // location block
  $location_block = $form_section->get_location_options($row['city']);
  $template->assign_var("LOCATION_BLOCK", $location_block);

  // salary block
  $salary_block = $form_section->get_salary_list($row['salary']);
  $template->assign_var("SALARY_BLOCK", $salary_block);
		
  // accomodation block
  $accomodation_block = $form_section->get_accomodation_list($row['accomodation']);
  $template->assign_var("ACCOMODATION_BLOCK", $accomodation_block);
		
  // number teachers block
  $number_teachers_block = $form_section->get_number_teachers_list($row['number_teachers']);
  $template->assign_var("NUMBER_TEACHERS_BLOCK", $number_teachers_block);

}
else if (stristr ( $_SERVER['PHP_SELF'], '/gallery.php' ) )
{      

  // print form flag helps with email decoder for print_form_update 
  $print_form_flag = 1 ;

  $select_birthyear=$row['birthyear'] -= 1899;
  $template->assign_var("TEACHER_BIRTHDAY_SELECTED_".$select_birthyear , "selected='selected'" ) ;    
  
  $nationality_block = $form_section->get_country_options($row['nationality']);
  $template->assign_var("NATIONALITY_BLOCK", $nationality_block);

  $location_block = $form_section->get_location_options($row['location']);
  $template->assign_var("LOCATION_BLOCK", $location_block);
  
  $country_code_block = $form_section->get_telephone_codes($row['country_code']);
  $template->assign_var("COUNTRY_CODES_BLOCK", $country_code_block);

  // output the timezones for the telephones
  $telephone_timezone_block = $form_section->get_timezone_options($row['timezone']);
  $template->assign_var("TELEPHONE_TIMEZONE_BLOCK", $telephone_timezone_block );
		
  // output the arrival 
  $select_arrival = preg_replace('/^([0-9]*-)/','',$row['arrival']);
  $arrival_block = $form_section->get_arrival_list($select_arrival);
  $template->assign_var("ARRIVAL_BLOCK", $arrival_block );

  // get the from time hours
  $from_time_block=$form_section->get_hours_timezone($row['from_time']);
  $template->assign_var("FROM_TIME_BLOCK", $from_time_block);

  // get the until time hours
  $until_time_block=$form_section->get_hours_timezone($row['until_time']);
  $template->assign_var("UNTIL_TIME_BLOCK", $until_time_block);
		
  $select_start_day=$start_day;
  $template->assign_var("TEACHER_STARTDAY_SELECTED_".$select_start_day, "selected='selected'" );
  
  $select_start_month=$start_month;
  $template->assign_var("TEACHER_STARTMONTH_SELECTED_".$select_start_month, "selected='selected'" );

  $select_start_year=$start_year;
  $template->assign_var("TEACHER_STARTYEAR_SELECTED_".$select_start_year, "selected='selected'" );

  $select_gender=$row['gender'];
  $template->assign_var("TEACHER_GENDER_SELECTED_".$select_gender, "selected='selected'");
  
  $select_inkorea=$row['inkorea'];
  $template->assign_var("TEACHER_INKOREA_SELECTED_".$select_inkorea, "selected='selected'" );
		
}
		
?>