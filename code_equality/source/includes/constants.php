<?php

/**
 * Store all the application constants in here
 *
 *                             constants.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}

// Debug Level
//define('DEBUG', 1); // Debugging on
define('DEBUG', 1); // Debugging off

// send mail envelope packer
define('SENDMAIL','-fDavid@EnglishTeachingKorea.com');

// User Levels <- Do not change the values of USER or ADMIN
define('DELETED', -1);
define('ANONYMOUS', -1);

define('GUEST', 0);
define('ADMIN', 2);
define('USER', 1);

// Seperator for JSON seperation
define('SEPERATOR','_-_-');

// Location, location, location
define('ANY',1);
define('SEOUL',2);
define('DAEGU',3);
define('BUSAN',4);
define('INCHEON',5);
define('GWANGJU',6);
define('ULSAN',7);
define('ILSAN',8);

// Database codes for months ...
define('ANY_MONTH_CODE',0);
define('JANUARY_MONTH_CODE',1);
define('FEBRUARY_MONTH_CODE',2);
define('MARCH_MONTH_CODE',3);
define('APRIL_MONTH_CODE',4);
define('MAY_MONTH_CODE',5);
define('JUNE_MONTH_CODE',6);
define('JULY_MONTH_CODE',7);
define('AUGUST_MONTH_CODE',8);
define('SEPTEMBER_MONTH_CODE',9);
define('OCTOBER_MONTH_CODE',10);
define('NOVEMBER_MONTH_CODE',11);
define('DECEMBER_MONTH_CODE',12);

// Database for time of month codes
define('START_OF_JANUARY',1);
define('MIDDLE_OF_JANUARY',2);
define('END_OF_JANUARY',3);
define('START_OF_FEBRUARY',4);
define('MIDDLE_OF_FEBRUARY',5);
define('END_OF_FEBRUARY',6);
define('START_OF_MARCH',7);
define('MIDDLE_OF_MARCH',8);
define('END_OF_MARCH',9);
define('START_OF_APRIL',10);
define('MIDDLE_OF_APRIL',11);
define('END_OF_APRIL',12);
define('START_OF_MAY',13);
define('MIDDLE_OF_MAY',14);
define('END_OF_MAY',15);
define('START_OF_JUNE',16);
define('MIDDLE_OF_JUNE',17);
define('END_OF_JUNE',18);
define('START_OF_JULY',19);
define('MIDDLE_OF_JULY',20);
define('END_OF_JULY',21);
define('START_OF_AUGUST',22);
define('MIDDLE_OF_AUGUST',23);
define('END_OF_AUGUST',24);
define('START_OF_SEPTEMBER',25);
define('MIDDLE_OF_SEPTEMBER',26);
define('END_OF_SEPTEMBER',27);
define('START_OF_OCTOBER',28);
define('MIDDLE_OF_OCTOBER',29);
define('END_OF_OCTOBER',30);
define('START_OF_NOVEMBER',31);
define('MIDDLE_OF_NOVEMBER',32);
define('END_OF_NOVEMBER',33);
define('START_OF_DECEMBER',34);
define('MIDDLE_OF_DECEMBER',35);
define('END_OF_DECEMBER',36);

// Timezone codes
define('ALASKA_TIMEZONE_CODE',1);
define('LOS_ANGELES_TIMEZONE_CODE',2);
define('DENVER_TIMEZONE_CODE',3);
define('DALLAS_TIMEZONE_CODE',4);
define('NEW_YORK_TIMEZONE_CODE',5);
define('NEW_FOUNDLAND_TIMEZONE_CODE',6);
define('LONDON_TIMEZONE_CODE',7);
define('BERLIN_TIMEZONE_CODE',8);
define('BANGKOK_TIMEZONE_CODE',9);
define('PERTH_TIMEZONE_CODE',10);
define('SEOUL_TIMEZONE_CODE',11);
define('SYDNEY_TIMEZONE_CODE',12);
define('AUCKLAND_TIMEZONE_CODE',13);

// Organization database codes
define('HAGWON_CODE',1);
define('PUBLIC_SCHOOL_CODE',2);
define('UNIVERSITY_CODE',3);
define('LANGUAGE_INSTITUTE_CODE',4);

// Country codes ...
define('AUSTRALIA_PHONE_CODE','0061');
define('CANADA_PHONE_CODE','001');
define('CHINA_PHONE_CODE','0086');
define('UNITED_KINGDOM_PHONE_CODE','0044');
define('INDIA_PHONE_CODE','0091');
define('INDONESIA_PHONE_CODE','0062');
define('IRELAND_PHONE_CODE','00353');
define('SOUTH_KOREA_PHONE_CODE','0082');
define('MEXICO_PHONE_CODE','0052');
define('PHILLIPINES_PHONE_CODE','0063');
define('THAILAND_PHONE_CODE','0066');
define('UNITED_STATES_PHONE_CODE','001');
define('NEW_ZEALAND_PHONE_CODE','0064');
define('SOUTH_AFRICA_PHONE_CODE','0027');
define('SPAIN_PHONE_CODE','0034');

// Database codes for the salary levels
define('SALARY_1',1);
define('SALARY_2',2);
define('SALARY_3',3);
define('SALARY_4',4);
define('SALARY_5',5);
define('SALARY_6',6);
define('SALARY_7',7);
define('SALARY_8',8);
define('SALARY_9',9);
define('SALARY_10',10);
define('SALARY_11',11);
define('SALARY_12',12);

// Database codes which define the variables
define('ANY_COUNTRY_CODE',0);
define('AUSTRALIA_CODE',1);
define('CANADA_CODE',2);
define('IRELAND_CODE',3);
define('NEW_ZEALAND_CODE',4);
define('UNITED_STATES_CODE',5);
define('UNITED_KINGDOM_CODE',6);
define('SOUTH_AFRICA_CODE',7);

// Resources Location
define('GUIDE','/EnglishTeachingKorea.pdf');
define('GAMES','/askGARI/gari_processor.php');
define('GALLERY','/pic_gallery/index.html');
define('MOVIE','/movie.mpg');
define('CHAT','/chat_client.html');
define('PAGE1','/page_1.php');
define('PAGE2','/page_2.php');
define('PAGE3','/page_3.php');
define('PAGE4','/page_4.php');
define('PAGE5','/page_5.php');
define('PAGE6','/page_6.php');

// developer IP, you might have to update...
define('DEVELOPER_IP','121.151.2.17');

// Web spiders
define('ALTA_VISTA_UK','AltaVista-Intranet');
define('ALEXA','ia_archiver');
define('DAUM','RaBot');
define('EXCITE','ArchitextSpider');
define('GOOGLE','Googlebot');
define('INKTOMI','Slurp');
define('MSNBOT','MSNBOT/0.1');

// User related
define('USER_ACTIVATION_NONE', 0);
define('USER_ACTIVATION_SELF', 1);
define('USER_ACTIVATION_ADMIN', 2);

define('USER_AVATAR_NONE', 0);
define('USER_AVATAR_UPLOAD', 1);
define('USER_AVATAR_REMOTE', 2);
define('USER_AVATAR_GALLERY', 3);

// Server Timezone
define('SERVER_TIMEZONE','TZ=America/Winnipeg' );

// I'm putting the board timezone in here because the school_cron
// script runs on it's own without a user ...
define('BOARD_TIMEZONE','TZ=Asia/Seoul');

// Error codes
define('GENERAL_MESSAGE', 200);
define('GENERAL_ERROR', 202);
define('CRITICAL_MESSAGE', 203);
define('CRITICAL_ERROR', 204);


// Session parameters
define('SESSION_METHOD_COOKIE', 100);
define('SESSION_METHOD_GET', 101);

// Page numbers for session handling
define('PAGE_INDEX', 0);
define('PAGE_LOGIN', -1);
define('PAGE_CONTROL_PANEL', -2);
define('PAGE_GALLERY', -3);
define('PAGE_FORM', -4);
define('PAGE_FORM_SELECT', -6);
define('PAGE_PAGE1', -7);
define('PAGE_PAGE2', -8);
define('PAGE_PAGE3', -9);
define('PAGE_PAGE4', -10);
define('PAGE_PAGE5', -11);
define('PAGE_PAGE6', -12);
define('PAGE_CALENDAR',-13);
define('PAGE_RECRUITER_GALLERY',-14);
define('PAGE_SCHOOL_GALLERY',-15);
define('PAGE_TOPIC_OFFSET', 5000);


// Table names
define('AUTH_ACCESS_TABLE', $table_prefix.'auth_access');
define('CONFIG', $table_prefix.'config');
define('EMPLOYEE_TRACKER',$table_prefix.'employee_tracker');
define('FREE_CAMPAIGNS',$table_prefix.'free_campaigns');
define('GAMESDATABASE',$table_prefix.'gamesdatabase');
define('JOB_CALENDAR',$table_prefix.'job_calendar');
define('KNOWLEDGE_BASE',$table_prefix.'knowledge_base');
define('LINKSTATS',$table_prefix.'linkstats');
define('MAIL',$table_prefix.'mail');
define('MAIL_MEMORY',$table_prefix.'mail_memory');
define('PAID_CAMPAIGNS',$table_prefix.'paid_campaigns');
define('RECRUITER_JOBS',$table_prefix.'recruiter_jobs');
define('PAGE_MAKER',$table_prefix.'registry');
define('REMINDER',$table_prefix.'reminder');
define('SCHOOL_JOBS',$table_prefix.'school_jobs');
define('SENT_MATCH',$table_prefix.'sent_match');
define('SESSIONS_TABLE',$table_prefix.'sessions');
define('SESSIONS_TABLE_KEYS',$table_prefix.'sessions_keys');
define('SITE_TEXT_ZONE',$table_prefix.'site_text_zone');
define('SITE_TRACKER',$table_prefix.'site_tracker');
define('STATSLOG',$table_prefix.'statslog');
define('TEACHERS',$table_prefix.'teachers');
define('TEL_CONTACT',$table_prefix.'tel_contact');
define('THEMES_TABLE',$table_prefix.'themes');
define('USERS',$table_prefix.'users');
define('USERS_TABLE',$table_prefix.'users');
define('VANITY_INDEX',$table_prefix.'vanity_index');

?>