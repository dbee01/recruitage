<?php

/**
 * This will handle some of the interaction from the interface. Including all of the ajax interaction. Basically, this is the receptor for all the XMLHttpRequests. Many of these requests will be passed off to interface_handler to actually be handled and responded to...
 *                             ajax_cp.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

// security choke point...
$_POST = array_map("input_check",$_POST);
$_GET = array_map("input_check",$_GET);

// you don't need to cache XHR Responses...
header("Expires: -1");
header("Pragma: no-cache");

@extract($_POST);

// what's the sid cookie called ? 
$sid = $_COOKIE[str_replace('.com','',strtolower($board_config['sitename']) . '_sid')];

// Instantiate the object here just to make sure,,,
$inter = new interface_handler();

// Instantiate the phone object ...
$phone = new phone();

// Instantiate the time object
$time = new timezone();

// Instantiate the action monitor 
$action = new action_monitor();

// Instatiate the json_snd class
$json_snd = new json_snd();

// Instantiate the site tracker
$sites = new site_tracker();

// Instantiate the notes append
$notes = new notes_append();

// Search for contact from the school form 
if( isset( $_POST['contact_search'] ) )
{
   

  $inter->name_search($_POST['contact_search']);

}

// Search for organization from the school contact form
if( isset( $_POST['organization_search'] ) )
{
   
  $inter->organization_search($_POST['organization_search']);

}

// Search for telephone from the school contact form
if( isset( $_POST['telephone_search'] ) )
{
   
  $inter->telephone_search($_POST['telephone_search']);

}


// update the knowledge base entry ...
if( isset( $_POST['add_knowledge_base_entry'] ) )
{

  $inter->add_knowledge($_POST['section_choice'],$_POST['section_title'],$_POST['section_text']);

}


// update the knowledge base entry ...
if( isset( $_POST['knowledge_base_entry'] ) )
{

  $inter->knowledge_update($_POST['knowledge_base_entry'],$_POST['knowledge_text']);

}

// process the action monitor values .....
if( isset( $_GET['third_party_call_form'] ) )
{

  $phone->book_third_party($_GET['teacher_id'],$_GET['recruiter_jobs_id'],$_GET['school_jobs_id'],$_GET['time'],$_GET['user_reg_date']);

}

// process the action monitor values .....
if( isset( $_POST['action_monitor'] ) )
{

  $action->cookie_reader($_POST['action_monitor']);

}

// search for email from the school contact form
if( isset( $_POST['email_search'] ) )
{
   
  $inter->email_search($_POST['email_search']);

}

// editing the records on the school contact form
if( isset( $_POST['save_school_entry'] ) )
{
   
  $inter->save_school_entry($_POST);

}

// mark the teacher/job match as being a failure
if( isset( $_POST['success_match_failed'] ) )
{

  $inter->del_match($_POST['teacher_id'],$_POST['job_id']);

}

// mark the teacher/job match as being a failure
if( isset( $_POST['success_match_completed'] ) )
{

  $inter->complete_match($_POST['teacher_id'],$_POST['job_id']);

}

// delete a school entry record in the school table
if( isset( $_POST['del_school_entry'] ) ){
   
  $inter->del_school_entry($_POST['del_school_entry']);

}

// send the sms message ...
if( isset( $_POST['sms_text'] ) ){

  // trim the first two zeros for the sms service
  $to = $_POST['sms_num'];
  $to = substr($to,2);

  $message = $_POST['sms_text'];
  $from = trim($userdata['user_mobile']);

  include_once($phpbb_root_path . 'sms_facility/send.'.$phpEx);

}

// email save text
if( isset( $_POST['email_save_text'] ) ){
 

  $mail_man = new mail_manager ;
  $mail_man->mail_store($text, $mail, $candidate_name);
  
}

// make the email a POST to include user_reg_date
// build a user specific backend connection
// email_id, user_id, email_dete
if( $_GET['action'] == 'email_dete'  ){

  @extract($_GET);
  
  // pass through the message uid
  $inter->email_delete($email_id, $user_id);
  
  exit ;
}


if( isset( $_GET['action'])  ){	
  
  ignore_user_abort();
       
  $action=$_GET['action'];
  $id=$_GET['id'];

  switch ($action)
    {

    case 'css_info':					
      $sites->sites_store($id);
      exit ;
      break;

    case 'teacher_chsts':					
      $inter->teacher_change_stat($id);
      exit ;
      break;

    case 'email_history':					
      $inter->get_email_history($id);
      exit ;
      break;

    case 'callback':					
      $phone->do_callback($id);
      exit ;
      break;

    case 'teacher_dete':
      $inter->teacher_delete_stat($id);
      exit ;
      break;

    case 'recruiter_dete':
      $inter->recruiter_delete_stat($id);
      exit ;
      break;

    case 'school_dete':
      $inter->school_delete_stat($id);
      exit ;
      break;
      	    
    case 'kill_reminder':
      $inter->kill_reminder($id);
      exit ;
      break;

    case 'update_page':	    

       // never cache the update...
       header("Expires: -1");
       header("Pragma: no-cache");

       // get a '_-_-' seperated list of json encoded emails
       $emails_json_array_list = $inter->update_emails($id);
       $recruiters_json_array_list = $inter->update_recruiters($id);
       $schools_json_array_list = $inter->update_schools($id);
       $teachers_json_array_list = $inter->update_teachers($id);
       $matches_json_array_list = $inter->update_matches($id);
       $leads_json_array_list = $inter->update_leads($id);
       $reminders_json_array_list = $inter->update_reminders($id);

      // normal chat poll
      //$update_str .= $inter->update_chat_poll();

      // build an object 
      $json_obj = new json_snd($emails_json_array_list,
			       $recruiters_json_array_list,
			       $schools_json_array_list,
			       $teachers_json_array_list,
			       $matches_json_array_list,
			       $leads_json_array_list,
			       $reminders_json_array_list);

      // send the json encoded object
      echo json_encode($json_obj) ;

      exit;

      break;

    case 'check_number':	    
      $inter->check_number($id);
      exit;
      break;

    case 'contact_search':	    
      $inter->contact_search($id);
      exit;
      break;

    case 'school_search':	    
      $inter->school_search($id);
      exit;
      break;

    default: exit ; 

    }
}


?>