<?php

/**
 * This application interfaces with the logs, for things like user tracking, which pages the user has visited etc.. We store the logs in a database and then search them from here..
 *
 *                           apache_log_parser.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

## 121.151.2.17 - - [03/Oct/2006:22:25:55 -0500] "GET /css_page/chromebg.gif HTTP/1.1" 304 - "http://www.englishteachingkorea.com/css_page/chromestyle.css" "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060731 Ubuntu/dapper-security Firefox/1.5.0.5"

class apache_log_parser
{

  var $db, $db_log;

  /**
   * constructor here, instantiate the db object and the db_log object
   *
   */
  function apache_log_parser()
  {
    
    // instantiate the two global objects
    global $db, $db_log;
    
    $this->db = $db ;
    $this->db_log = $db_log;

  }

  /**
   * take a page and return and array of ips that went to that page
   *
   */ 
  function ips_from_page($page)
  {

    // match the exact page here to save resources
    $sql = "SELECT DISTINCT remote_host FROM englishteachingkorea_access_log WHERE referer LIKE '%$page%' " ;

    if (!($result = $this->db_log->sql_query($sql)))
      {
       	message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
      }

    while ($row = $this->db_log->sql_fetchrow($result))
      {
	$ips_array[] = $row['remote_host'];
      }    

    return $ips_array ;

  }


  /**
   * take a page and return and array of ips that came from that domain this is for the free campaign 
   *
   */
  function ips_from_domain($domain)
  {

    // match the exact page here to save resources
    $sql = "SELECT DISTINCT remote_host FROM englishteachingkorea_access_log WHERE referer LIKE '%$domain%' " ;

    if (!($result = $this->db_log->sql_query($sql)))
      {
       	message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
      }

    while ($row = $this->db_log->sql_fetchrow($result))
      {
	$ips_array[] = $row['remote_host'];
      }    

    return $ips_array ;

  }


  /**
   * take an array of ips, find the number of which hit the special page and return it
   *
   */
  function no_unique_ips($ips_array,$page)
  {

    // find the number of unique ip's that hit that page
    $sql = "SELECT COUNT(*) c FROM englishteachingkorea_access_log WHERE ( remote_host = '$ips_array[0]' ";

    for ( $i = 1 ; $i < count($ips_array) ; $i++ )
      {
	
	$sql .= " OR remote_host = '$ips_array[$i]'"; 

      }

    $sql = substr($sql,0,-1) ; 

    $sql .= "' ) AND request_args = '$page'" ;

    // this should be couched in an if... message die statement,
    // but for some reason it messes it up. So I'll leave it raw
    $result = $this->db_log->sql_query($sql);

    $ip_num = $this->db_log->sql_fetchrow($result);

    return $ip_num['c'] ;

  }

  /** 
   * return the number of successes in the success match table that are in the ip array, the ip array contains ips that came to the site from a campaign ad page ...
   * 
   */
  function ip_success_match($ips_array)
  {

    $sql = "select count(*) c from sent_match, teachers where sent_match.teacher_id = teachers.teacher_id and sent_match.success = 1 and ";

    foreach ( $ips_array as $ip )
      {
	$sql .= " teachers.ip = '$ip' OR";
      }
    
    $sql = substr($sql,0,-3);
    
    $sql .= ' AND success = 1';

    $result = $this->db->sql_query($sql);

    // return number of ips that were in success table from that ad
    $ip = $this->db->sql_fetchrow($result );
    
    return $ip['c'] ;

  }

  /**
   * given an ip address, track the candidate number. Useful for finding specific info about candidate eg. Which page did they visit ?
   *
   */
  function ip_to_candidate_no($ip)
  {

    $sql = "SELECT teacher_id FROM teachers WHERE ip = '$ip'" ;

    if (!($result = $this->db->sql_query($sql)))
      {
       	message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
      }
    
    $id = $this->db->sql_fetchrow($result);

    return $id['teacher_id'] ;

  }

  /**
   * given the candidate id, track the ip
   *
   */
  function candidate_to_ip($id)
  {

    $sql = "SELECT ip FROM teachers WHERE teacher_id = '$id'" ;

    if (!($result = $this->db->sql_query($sql)))
      {
       	message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
      }

    $ip = $this->db->sql_fetchrow($result);

    return $ip['ip'] ;

  }

  /**
   * find whether this ip hit this specific page/resource
   *
   */
  function ip_to_resource($ip,$resource)
  {    

    // match the exact page here to save resources
    $sql = "SELECT DISTINCT remote_host FROM englishteachingkorea_access_log WHERE remote_host = '$ip' AND request_uri LIKE '%$resource%' " ;

    if (!($result = $this->db_log->sql_query($sql)))
      {
       	message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
      }

    if ($row = $this->db_log->sql_fetchrow($result))
      {
	return true;
      }    
    else 
      {
	return false;
      }

  }

}

?>