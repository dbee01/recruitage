<?php

/**
 * This page is an include put into job0 (client) form it renders the page an update page for the recruiter instead of a resume page
 *
 *                            job1_form_picker.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
  die("Hacking attempt");
}


if ( $page_maker["job1_upload_contact"] )  
{

  $page_maker['job1_upload_contact_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.contact',
			       array(
				     'L_CONTACT'=>$lang['Contact'],
				     'CHECK'=>$check
				     )); 
}	


if ( $page_maker["job1_upload_school"] )  
{

  $page_maker['job1_upload_school_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.school',
			       array(
				     'L_SCHOOL'=>$lang['School'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_email"] )  
{

  $page_maker['job1_upload_email_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.email',
			       array(
				     'L_EMAIL'=>$lang['Email'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_arrival"] )  
{

  $page_maker['job1_upload_arrival_check'] ? $check = 'check' : $check = 0 ;

  $arrival_list = $form_section->get_arrival_list();

  $template->assign_block_vars('job1_form.arrival',
			       array(
				     'L_ARRIVAL'=>$lang['Arrival'],
				     'ARRIVAL_LIST'=>$arrival_list,
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_school_description"] ) 
{

  $page_maker['job1_upload_school_description_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.school_description',
			       array(
				     'L_SCHOOL_DESCRIPTION'=>$lang['School_description'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_picupload"] )  
{

  $page_maker['job1_upload_picupload_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.picture',
			       array(
				     'L_PICTURE'=>$lang['Picture'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_city"] )  
{

  $page_maker['job1_upload_city_check'] ? $check = 'check' : $check = 0 ;

  $location_block = $form_section->get_location_options();

  $template->assign_block_vars('job1_form.location',
			       array(
				     'LOCATION_BLOCK'=>$location_block,
				     'L_LOCATION'=>$lang['Location'],
				     'CHECK'=>$check
				     )); 
}	

if ( $page_maker["job1_upload_website_address"] )  
{

  $page_maker['job1_upload_website_address_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.website_address',
			       array(
				     'L_WEBSITE_ADDRESS'=>$lang['Website_address'],
				     'CHECK'=>$check
				     )); 
}	

if ( $page_maker["job1_upload_students"] )  
{

  $page_maker['job1_upload_students_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.students',
			       array(
				     'L_STUDENTS'=>$lang['Students'],
				     'L_KINDERGARTEN'=>$lang['Kindergarten'],
				     'L_ELEMENTARY'=>$lang['Elementary'],
				     'L_MIDDLE_SCHOOL'=>$lang['Middle_school'],
				     'L_HIGH_SCHOOL'=>$lang['High_school'],
				     'L_ADULTS'=>$lang['Adults'],
				     'CHECK'=>$check
				     )); 
}	

if ( $page_maker["job1_upload_ticket_money"] )  
{

  $page_maker['job1_upload_ticket_money_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.ticket_money',
			       array(
				     'L_TICKET_MONEY'=>$lang['Ticket_money'],
				     'CHECK'=>$check
				     )); 
}	

if ( $page_maker["job1_upload_accomodation"] )  
{

  $page_maker['job1_upload_accomodation_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.accomodation',
			       array(
				     'L_ACCOMODATION'=>$lang['Accomodation'],
				     'L_SELECT'=>$lang['Select'],
				     'L_SINGLE'=>$lang['Single'],
				     'L_SHARED'=>$lang['Shared'],
				     'CHECK'=>$check
				     )); 
}	

if ( $page_maker["job1_upload_contract"] )  
{

  $page_maker['job1_upload_contract_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.contract',
			       array(
				     'L_CONTRACT'=>$lang['Contract'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_teacher_description"] )  
{

  $page_maker['job1_upload_teacher_description_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.teacher_description',
			       array(
				     'L_TEACHER_DESCRIPTION'=>$lang['Teacher_description'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_salary"] )  
{

  $page_maker['job1_upload_salary_check'] ? $check = 'check' : $check = 0 ;

  $salary_list = $form_section->get_salary_list();

  $template->assign_block_vars('job1_form.salary',
			       array(
				     'L_SALARY'=>$lang['Salary'],
				     'SALARY_BLOCK'=>$salary_list,
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_number_teachers"] )  
{

  $page_maker['job1_upload_number_teachers_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.num_teachers',
			       array(
				     'L_NUM_TEACHERS'=>$lang['Num_teachers'],
				     'CHECK'=>$check
				     )); 

}	

if ( $page_maker["job1_upload_gender"] )  
{

  $page_maker['job1_upload_gender_check'] ? $check = 'check' : $check = 0 ;

  $template->assign_block_vars('job1_form.gender',
			       array(
				     'L_GENDER'=>$lang['Gender'],
				     'L_ANY'=>$lang['Any'],					 
				     'L_MALE'=>$lang['Male'],
				     'L_FEMALE'=>$lang['Female'],
				     'CHECK'=>$check
				     )); 
}	


?>