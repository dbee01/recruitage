<?php

/**
 * This is just a collection of functions, mostly to do with form parsing. Might just put some of these in form_select instead.
 *
 *                             functions_form.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}


/** 
 * Take a phone number and turn it into a sip link
 *
 */
function phone_to_link($number)
{

  $sip_link = "<a href='sip:".$number."' >Call</a>";

  return $sip_link ;

}

/**
 * Remove the two zeros at the start if it's skype
 *
 */
function remove_skype_zeros($phone)
{

  // skype won't handle the first two 00
  if ( substr($phone,0,2) == '00' )
    {
      $phone = substr($phone,2);
    }

  return $phone ;

}

/**
 *  Function to take a codename and change it to a text zone
 *
 */
function zone_code_to_name($code_name)
{

  global $lang ;

      switch ($code_name)
	{
	case '0' : $zone_name = 'Default'  ; break ;
	case '1' : $zone_name = $lang['Australia']  ; break ;
	case '2' : $zone_name = $lang['Canada']  ; break ;
	case '3' : $zone_name = $lang['China']  ; break ;
	case '4' : $zone_name = $lang['United_kingdom']  ; break ;
	case '5' : $zone_name = $lang['India']  ; break ;
	case '6' : $zone_name = $lang['Indonesia']  ; break ;
	case '7' : $zone_name = $lang['Ireland']  ; break ;
	case '8' : $zone_name = $lang['Mexico']  ; break ;
	case '9' : $zone_name = $lang['Phillipines']  ; break ;
	case '10' : $zone_name = $lang['Thailand']  ; break ;
	case '11' : $zone_name = $lang['United_states']  ; break ;
	case '12' : $zone_name = $lang['New_zealand']  ; break ;
	case '13' : $zone_name = $lang['South_africa']  ; break ;
	case '14' : $zone_name = $lang['Spain']  ; break ;
	case '15' : $zone_name = $lang['South_korea']  ; break ;
	default: $zone_name = 'Default' ; break;
	}

      return $zone_name ;

}

/**
 * Function to switch from the zone name to a code 
 *
 */
function zone_name_to_code($zone_name) 
{

  global $lang;

  // strictly speaking the case here should be $lang['Default'] etc...
  // but this function doesn't see $lang so I'm just going to motor on ...

    switch ($zone_name)
      {
      case ('Default') : $zone_code = 0 ; break ;
      case ('Australia') :  $zone_code = 1  ; break ;
      case ('Canada') : $zone_code = 2  ; break ;
      case ('China') : $zone_code = 3  ; break ;
      case ('United_kingdom') : $zone_code = 4 ; break ;
      case ('India') : $zone_code = 5  ; break ;
      case ('Indonesia') : $zone_code = 6  ; break ;
      case ('Ireland') : $zone_code = 7  ; break ;
      case ('Mexico') : $zone_code = 8  ; break ;
      case ('Phillipines') : $zone_code = 9  ; break ;
      case ('Thailand') : $zone_code = 10  ; break ;
      case ('United_states') : $zone_code = 11  ; break ;
      case ('New_zealand') : $zone_code = 12  ; break ;
      case ('South_africa') : $zone_code = 13  ; break ;
      case ('Spain') : $zone_code = 14  ; break ;
      case ('South_korea') : $zone_code = 15  ; break ;
      default: $zone_code = 0 ; break;
      }

    return $zone_code; 
    
}

/**
 * By entering a code, get back the text
 *
 */
function get_text_by_zone($zone)
{
  
  // SET THIS TO DEFAULT - lATER FIX IT
  $sql = "SELECT * FROM site_text_zone WHERE zone_name = $zone";
   
  if ( ! ( $result = mysql_query($sql) ) )
    {
      message_die(GENERAL_ERROR,'Cannot reach the school jobs table ... ','',__LINE__,__FILE__,$sql);
    }
	  
  // add the country_zone text array, on to the page_maker array
  $row = mysql_fetch_assoc($result);

  return $row ;

}

/**
 * Switch from the location code, to a city
 *
 */
function location($city)
{

	switch ( $city )
		{
			case (1) : $city = 'Any' ; break; 
			case (2) : $city = 'Seoul' ; break; 
			case (3) : $city = 'Daegu' ; break; 
			case (4) : $city = 'Busan' ; break; 
			case (5) : $city = 'Incheon' ; break; 
			case (6) : $city = 'Gwangju' ; break; 
			case (7) : $city = 'Ulsan' ; break; 
			case (8) : $city = 'Ilsan' ; break; 
		 	default : $city = 'Unknown' ; 
		} 		
	return $city ;
}

/**
 * Take a monthtime code and change to a start of / middle of/ end of...
 *
 */
function monthtime($monthtime)
{	
	switch ( $monthtime )
		{
			case (1) : $monthtime = 'Start of'; break ;
		  	case (2) : $monthtime = 'Middle of'; break ; 
			case (3) : $monthtime = 'End of' ; break ;
			default :  $monthtime = 'Unknown' ;   
		}
	return $monthtime ; 
}

/**
 * Take a month code and change to month text
 *
 */
function month($month)
{	
	
	    switch ( $month )
		{
			case (1) : $month = $lang['January'] ;  break; 
		  	case (2) : $month = $lang['February']  ; break; 
			case (3) : $month = $lang['March']  ; break; 
			case (4) : $month = $lang['April']  ; break; 
			case (5) : $month = $lang['May']  ; break; 
			case (6) : $month = $lang['June']  ; break; 
		 	case (7) : $month = $lang['July']  ; break;
			case (8) : $month = $lang['August']  ; break;
			case (9) : $month = $lang['September']  ; break;
			case (10) : $month = $lang['October']  ; break;
			case (11) : $month = $lang['November']  ; break;
			case (12) : $month = $lang['December']  ; break;
			default :  $month = $lang['Unknown'] ;   
		} 
		
	return $month ; 
}

/**
 * Take salary code and output salary figure
 *
 */
function salary($salary)
{
		switch ( $salary )
		{
			case ('1') : $salary = '1.7 M won' ; break; 
			case ('2') : $salary = '1.8 M won' ; break;
			case ('3') : $salary = '1.9 M won' ; break; 
			case ('4') : $salary = '2.0 M won' ; break; 
			case ('5') : $salary = '2.1 M won' ; break; 
			case ('6') : $salary = '2.2 M won' ; break; 
			case ('7') : $salary = '2.3 M won' ; break; 
			case ('8') : $salary = '2.4 M won' ; break; 
			case ('9') : $salary = '2.5 M won' ; break; 
			case ('10') : $salary = '2.6 M won' ; break; 
			case ('11') : $salary = '2.7 M won' ; break; 			 
			case ('12') : $salary = '2.8 M won' ; break; 
		 	default : $salary = 'unknown' ; 
		} 
	return $salary ; 

}

/**
 * Take arrival code and switch it to text
 *
 */
function arrival($arrival)
{	

  global $lang;

  switch ( $arrival )
    {
    case (1) :  $arrival = $lang['Start_of_january'] ; break; 
    case (2) :  $arrival = $lang['Middle_of_january'] ; break;
    case (3) :  $arrival = $lang['End_of_january'] ; break; 
    case (4) :  $arrival = $lang['Start_of_february'] ; break; 
    case (5) :  $arrival = $lang['Middle_of_february'] ; break; 
    case (6) :  $arrival = $lang['End_of_february'] ; break; 
    case (7) :  $arrival = $lang['Start_of_march'] ; break; 
    case (8) :  $arrival = $lang['Middle_of_march'] ; break; 
    case (9) :  $arrival = $lang['End_of_march'] ; break; 
    case (10) : $arrival = $lang['Start_of_april'] ; break; 
    case (11) : $arrival = $lang['Middle_of_april'] ; break; 			 
    case (12) : $arrival = $lang['End_of_april'] ; break; 
    case (13) : $arrival = $lang['Start_of_may'] ; break; 
    case (14) : $arrival = $lang['Middle_of_may'] ; break; 
    case (15) : $arrival = $lang['End_of_may'] ; break; 
    case (16) : $arrival = $lang['Start_of_june'] ; break; 
    case (17) : $arrival = $lang['Middle_of_june'] ; break; 
    case (18) : $arrival = $lang['End_of_june'] ; break; 
    case (19) : $arrival = $lang['Start_of_july'] ; break; 
    case (20) : $arrival = $lang['Middle_of_july'] ; break; 
    case (21) : $arrival = $lang['End_of_july'] ; break; 
    case (22) : $arrival = $lang['Start_of_august'] ; break; 
    case (23) : $arrival = $lang['Middle_of_august'] ; break; 
    case (24) : $arrival = $lang['End_of_august'] ; break; 
    case (25) : $arrival = $lang['Start_of_september'] ; break; 
    case (26) : $arrival = $lang['Middle_of_september'] ; break; 
    case (27) : $arrival = $lang['End_of_september'] ; break; 
    case (28) : $arrival = $lang['Start_of_october'] ; break; 
    case (29) : $arrival = $lang['Middle_of_october'] ; break; 
    case (30) : $arrival = $lang['End_of_october'] ; break; 
    case (31) : $arrival = $lang['Start_of_november'] ; break; 
    case (32) : $arrival = $lang['Middle_of_november']; break; 
    case (33) : $arrival = $lang['End_of_november'] ; break; 
    case (34) : $arrival = $lang['Start_of_december'] ; break; 
    case (35) : $arrival = $lang['Middle_of_december'] ; break; 
    case (36) : $arrival = $lang['End_of_december'] ; break; 
    default :   $arrival = $lang['Unknown'] ; 
    } 
  return $arrival ; 

}

/** 
 * Take in the arrival value in DATE format and translaste it into readable text format ....
 *
 */
function arrival_to_text($arrival)
{	
  
  global $lang ;
  
  // parse it down, take out the year,
  $arrival = preg_replace('/^([0-9]*-)/','',$arrival);

  switch ( $arrival )
    {
    case ('01-05') :  $arrival = $lang['Start_of_january'] ; break; 
    case ('01-15') :  $arrival = $lang['Middle_of_january'] ; break;
    case ('01-25') :  $arrival = $lang['End_of_january'] ; break; 
    case ('02-05') :  $arrival = $lang['Start_of_february'] ; break; 
    case ('02-15') :  $arrival = $lang['Middle_of_february'] ; break; 
    case ('02-25') :  $arrival = $lang['End_of_february'] ; break; 
    case ('03-05') :  $arrival = $lang['Start_of_march'] ; break; 
    case ('03-15') :  $arrival = $lang['Middle_of_march'] ; break; 
    case ('03-25') :  $arrival = $lang['End_of_march'] ; break; 
    case ('04-05') : $arrival = $lang['Start_of_april'] ; break; 
    case ('04-15') : $arrival = $lang['Middle_of_april'] ; break; 			 
    case ('04-25') : $arrival = $lang['End_of_april'] ; break; 
    case ('05-05') : $arrival = $lang['Start_of_may'] ; break; 
    case ('05-15') : $arrival = $lang['Middle_of_may'] ; break; 
    case ('05-25') : $arrival = $lang['End_of_may'] ; break; 
    case ('06-05') : $arrival = $lang['Start_of_june'] ; break; 
    case ('06-15') : $arrival = $lang['Middle_of_june'] ; break; 
    case ('06-25') : $arrival = $lang['End_of_june'] ; break; 
    case ('07-05') : $arrival = $lang['Start_of_july'] ; break; 
    case ('07-15') : $arrival = $lang['Middle_of_july'] ; break; 
    case ('07-25') : $arrival = $lang['End_of_july'] ; break; 
    case ('08-05') : $arrival = $lang['Start_of_august'] ; break; 
    case ('08-15') : $arrival = $lang['Middle_of_august'] ; break; 
    case ('08-25') : $arrival = $lang['End_of_august'] ; break; 
    case ('09-05') : $arrival = $lang['Start_of_september'] ; break; 
    case ('09-15') : $arrival = $lang['Middle_of_september'] ; break; 
    case ('09-25') : $arrival = $lang['End_of_september'] ; break; 
    case ('10-05') : $arrival = $lang['Start_of_october'] ; break; 
    case ('10-15') : $arrival = $lang['Middle_of_october'] ; break; 
    case ('10-25') : $arrival = $lang['End_of_october'] ; break; 
    case ('11-05') : $arrival = $lang['Start_of_november'] ; break; 
    case ('11-15') : $arrival = $lang['Middle_of_november'] ; break; 
    case ('11-25') : $arrival = $lang['End_of_november'] ; break; 
    case ('12-05') : $arrival = $lang['Start_of_december'] ; break; 
    case ('12-15') : $arrival = $lang['Middle_of_december'] ; break; 
    case ('12-25') : $arrival = $lang['End_of_december'] ; break; 
    default :   $arrival = $lang['Unknown'] ; 
    }

  return $arrival ; 

}


/**
 * If the date is more than one month old. Then bump it up to next year. Presumably the candidate means next year at that stage ...
 *
 */
function which_year($date){

  // have one months leeway here, if they enter any date in the last one month,
  // then take it as an error and input it as this year anyway...
  $now_date = date("Y-m-d", mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));  

  // if the proposed date is less than today's date
  // then add on an extra year ....
  if ($date < $now_date)
    {

      // build a date arr
      $date_arr = explode('-',$date);
      
      // add a year
      $new_year = $date_arr[0] + 1 ;
      
      // build date one year from now
      $date = $new_year.'-'.$date_arr[1].'-'.$date_arr[2];

    }

  // return the new date ( or the same date )
  return $date;

}

/**
 * take the numbers from the input forms and translate them into DATE format for mysql...  the which_year is an auto corrector here, it'll bump up a year if the date is more than one month old ..
 *
 */
function arrival_to_date($arrival)
{	
		switch ( $arrival )
		{
			case (1) :  $arrival = which_year(date('Y').'-01-05') ; break; 
			case (2) :  $arrival = which_year(date('Y').'-01-15')  ; break;
			case (3) :  $arrival = which_year(date('Y').'-01-25') ; break; 
			case (4) :  $arrival = which_year(date('Y').'-02-05') ; break; 
			case (5) :  $arrival = which_year(date('Y').'-02-15') ; break; 
			case (6) :  $arrival = which_year(date('Y').'-02-25') ; break; 
			case (7) :  $arrival = which_year(date('Y').'-03-05') ; break; 
			case (8) :  $arrival = which_year(date('Y').'-03-15') ; break; 
			case (9) :  $arrival = which_year(date('Y').'-03-25') ; break; 
			case (10) : $arrival = which_year(date('Y').'-04-05') ; break; 
			case (11) : $arrival = which_year(date('Y').'-04-15') ; break; 			 
			case (12) : $arrival = which_year(date('Y').'-04-25') ; break; 
			case (13) : $arrival = which_year(date('Y').'-05-05') ; break; 
			case (14) : $arrival = which_year(date('Y').'-05-15') ; break; 
			case (15) : $arrival = which_year(date('Y').'-05-25') ; break; 
			case (16) : $arrival = which_year(date('Y').'-06-05') ; break; 
			case (17) : $arrival = which_year(date('Y').'-06-15') ; break; 
			case (18) : $arrival = which_year(date('Y').'-06-25') ; break; 
			case (19) : $arrival = which_year(date('Y').'-07-05') ; break; 
			case (20) : $arrival = which_year(date('Y').'-07-15') ; break; 
			case (21) : $arrival = which_year(date('Y').'-07-25') ; break; 
		 	case (22) : $arrival = which_year(date('Y').'-08-05') ; break; 
		 	case (23) : $arrival = which_year(date('Y').'-08-15') ; break; 
		 	case (24) : $arrival = which_year(date('Y').'-08-25') ; break; 
		 	case (25) : $arrival = which_year(date('Y').'-09-05') ; break; 
		 	case (26) : $arrival = which_year(date('Y').'-09-15') ; break; 
		 	case (27) : $arrival = which_year(date('Y').'-09-25') ; break; 
		 	case (28) : $arrival = which_year(date('Y').'-10-05') ; break; 
		 	case (29) : $arrival = which_year(date('Y').'-10-15') ; break; 
		 	case (30) : $arrival = which_year(date('Y').'-10-25') ; break; 
		 	case (31) : $arrival = which_year(date('Y').'-11-05') ; break; 
		 	case (32) : $arrival = which_year(date('Y').'-11-15') ; break; 
		 	case (33) : $arrival = which_year(date('Y').'-11-25') ; break; 
		 	case (34) : $arrival = which_year(date('Y').'-12-05') ; break; 
		 	case (35) : $arrival = which_year(date('Y').'-12-15') ; break; 
		 	case (36) : $arrival = which_year(date('Y').'-12-25') ; break; 
		 	default :   $arrival = 'unknown' ; 
		} 

	return $arrival ; 

}

/**
 * Take in nation code and return nation string...
 *
 */
function nationality( $nation )
{
	switch ( $nation )
	{
		case (1) : $nation = $lang['Australia'] ;  break; 
	  	case (2) : $nation = $lang['Canada']  ; break; 
		case (3) : $nation = $lang['Ireland']  ; break; 
		case (4) : $nation = $lang['New Zealand']  ; break; 
		case (5) : $nation = $lang['United States']  ; break; 
		case (6) : $nation = $lang['United Kingdom']  ; break; 
	 	case (7) : $nation = $lang['South Africa']  ; break;
		default :  $nation = $lang['Unknown'] ;  
	}
	
	return $nation ; 

}

/**
 * Take in gender code and return string
 *
 */
function gender($gender)
{
	switch ( $gender )
	{
		case (1) : $gender = $lang['Male'] ;  break; 
	  	case (2) : $gender = $lang['Female']  ; break; 
		default :  $gender = $lang['Unknown'] ;  
	}
	
	return $gender ; 
}	

/**
 * Take in korea code and return string
 *
 */
function inkorea($inkorea)
{
	switch ( $inkorea )
	{
		case (1) : $inkorea = $lang['In_korea'] ;  break; 
	  	case (2) : $inkorea = $lang['Not_in_korea']  ; break; 
		default :  $inkorea = $lang['Unknown'] ;   
	}

	return $inkorea ; 
}

/**
 * Just clean the phone number of all non-digits .. split the phone parseing into various functions...
 *
 */
function phone_cleaner($number)
{

  // starting with 00 country code is the international problem
  // not dealt with here ...

  // firstly kill anything that isn't a digit 
  $number = preg_replace('/[^0-9]/', '', $number);
  
  return $number;

}


/** 
 * add the country code to the telephone numbers... check if the number has double 00 at the start though
 *
 */
function phone_country($number,$country)
{

  if ( substr($number,0,2) != '00' )
    {
     
      // country code
      $code = '';

      switch($country)
	{

	case ('korea'): $code='0082' ;break;
	case ('ireland'): $code='00353' ;break;
	case ('united kingdom'): $code='0044' ;break;
	case ('united states'): $code='001' ;break;
	case ('canada'): $code='001' ;break;
	default: break;

	}

    }

  $number = $code . $number ;

  return $number ;

}

/**
 * Phone chopper, take off the area code 0 at the start of a national number ...
 *
 */ 
function phone_chopper($phone)
{

  // if it has 0000 at the start, then someone added a country code twice
  if ($phone[0]=='0' && $phone[1]=='0' && $phone[2]=='0' && $phone[3]=='0' )
    {
      $phone = substr($phone,2);
    }

  if ($phone[0]=='0' && $phone[1]=='0' && $phone[2]=='0' )
    {
      $phone = substr($phone,1);
    }

  if ($phone[0]=='0' && $phone[1] !='0' )
    {

      $phone = substr($phone,1);
    }

  return $phone ;

}

/** 
 * this will output the phone mode 
 * anon vs written ?
 * skype vs sip vs callback ?
 * user_reg_date is here cause I'm calling this by XMLHttpRequest
 * and won't always know explicitly who called it.
 * this is for the phone links on the control_panel tabs
 * close determines whether or not to close the link 
 */
function cp_phone_mode($phone,$user_reg_date,$tab='',$close=0)
{

  // determine the label for the action monitor
  //   switch($tab)
  //     {

  //     case 'recruiter' : $action_label = 'phoneRecruiter' ; break ;
  //     case 'school' : $action_label = 'phoneSchool' ; break ;
  //     case 'teacher' : $action_label = 'phoneTeacher' ; break ;
  //     default: $action_label = 'phoneTeacher' ; break ;

  //     }

  // find the mode of that person
  // there is no db available
      
    $sql = "SELECT user_mobile, user_callback, user_sip, user_call_to, user_show_number FROM users WHERE user_regdate = '$user_reg_date' ";
    
    if ( ! ( $result = mysql_query($sql) ) )
      {
	message_die(GENERAL_ERROR,'Cannot reach the school jobs table ... ','',__LINE__,__FILE__,$sql);
      }
	  
    // choose sip first, if they don't have that enabled, then try skype
    // if they don't have skype enabled - then choose callback
    while( $row = mysql_fetch_assoc($result) )
      {

	if ( $row['user_sip'] )
	  {

	    // $output_text = "<\"sip:$phone\" onclick=\"action_monitor('$action_label','$phone');\">";

	    $output_text = "sip:$phone";

	  }
	else if ( $row['user_call_to'] )
	  {
	    
	    // skype doesn't take the double zero
	    $phone = remove_skype_zeros($phone);

	    // $output_text = "<a href=\"skype:+$phone?call\" onclick=\"action_monitor('$action_label','$phone');\">";

	    $output_text = "skype:+$phone?call" ;
	    
	    // make sure there isn't two ++
	    $output_text = str_replace('++','+',$output_text);

	  }
	else  
	  {

	    $output_text .= $phone ;

	  }

      }
    
    return $output_text;

}	

/**
 * Function will change a specific date to an approx date string
 *
 */
function date2approx($date)
{

 	  $date=explode('-',$date);
	  $year=$date[0];
	  $month= (int) $date[1];
	  $day= (int) $date[2];

	  // do this switch statement to catch unknown values 
 	  switch ($month)
          {
	    case 1 : $value = 0 ; break;
	    case 2 : $value = 3 ; break;
	    case 3 : $value = 6 ; break ;
	    case 4 : $value = 9 ; break ;
	    case 5 : $value = 12 ; break ;
	    case 6 : $value = 15 ; break ;
	    case 7 : $value = 18 ; break ;
	    case 8 : $value = 21 ; break ;
	    case 9 : $value = 24 ; break ;
	    case 10: $value = 27 ; break ;
	    case 11: $value = 30 ; break ;
	    case 12: $value = 33 ; break ;
	    default : $value = -1 ; break; 
 	  } 

	  switch ($day)
	  {
	    case ($day>0  && $day < 11) : $value+=1 ; break ;
	    case ($day>10 && $day < 21) : $value+=2 ; break ;
	    case ($day>20 && $day < 32) : $value+=3 ; break;
	    default : $value = -1 ; break ;
	  }

	  return $value ;

}

/** 
 * Simple break_to_newline
 *
 */	
function br2nl($text)
{
  return  preg_replace('/<br\\s*?\/??>/i', '', $text);
}

/**
 *  Check input strings 
 *
 */
function input_check($arr)
{
  return htmlentities($arr,ENT_QUOTES,'UTF-8');
}

/** 
 * Encode - decode ... takes care of the nl2br problem
 * this seems to be for a textarea -> db -> html page ...
 * called after the input check. It leaves <br>'s in the text.
 *
 */
function endecode($value)
{
    $value = nl2br(htmlentities(br2nl(html_entity_decode($value)),ENT_QUOTES,'UTF-8'));

    return $value ;
}

/**
 * On to the email text page... function will filter reply email text before outputting it  
 *
 */
function reply_text_replace($text,$name)
{

  global $userdata ;

  $search_words = array("{CANDIDATE_NAME}","{USERNAME}");
  $replace_words = array($name,$userdata['username']);

  $text = str_replace($search_words,$replace_words,$text);

  return $text;

}

?>