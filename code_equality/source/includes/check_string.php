<?php

/**
 * This is a one size fits all string checker class
 *
 *                             check_string.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

class check_string {

  /**
   * Basic empty string checker constructor
   *
   */
  function check_string(){}

  /**
   *  Check input strings 
   *
   */
  function input_check($arr)
  {
     return htmlentities($arr,ENT_QUOTES,'UTF-8');
  }

  /**
   *  Check output strings 
   *
   */
  function output_check($arr)
  {
    $value=html_entity_decode($arr,ENT_QUOTES);
	
    return stripslashes($value);
  }

  /**
   *  Strip html tags 
   *
   */
  function strip_html($text)
  {
    return strip_tags($text);
  }

  /**
   *  Decode html entities and sql escape them 
   *
   */
  function sql_true($text)
  {
    $decode_text=html_entity_decode($text,ENT_QUOTES);
    $sql_text=mysql_real_escape_string($decode_text);
    
    return $sql_text ;
  }  

}

?>