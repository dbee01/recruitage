<?php

/**
 * This file will handle the attachments for the email 
 *
 *                             attachment.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 * 
 **/

class attachment 
{

  var $save_dir;                    //where file will be saved
  var $filename="spacer.gif";        //default file name initially
  var $error_message="";            //string to be output if neccesary
  var $width;                        //height of final image
  var $height;                      //width of final image


  /**
   * constructor for the attachment, basically it will take a gif or a jpg image and resize it, then store it.
   *
   */
  function attachment($max_width, $max_height, $file_name, $candidate_name='')
  {
               
    $this->save_dir = '/';               
    $this->width =    $max_width;
    $this->height =  $max_height;
               
    //--change filename to time - make it unique 
    $temp_filename = $file_name;
    $ext = explode('.',$temp_filename);
    $ext = $ext[count($ext)-1];
    $temp_filename = $candidate_name . time() . "." . $ext;

    //--check that it's a jpeg or gif
    $ext = strtolower($ext);
               
    if (preg_match('/^(gif|jpe?g)$/',$ext)) {
      // resize in proportion
      list($width_orig, $height_orig) = getimagesize($file_array[$file_name]['tmp_name']);
                    
      if ($this->width && ($width_orig < $height_orig)) {
	$this->width = ($this->height / $height_orig) * $width_orig;
      } else {
	$this->height = ($this->width / $width_orig) * $height_orig;
      }

      $image_p = imagecreatetruecolor($this->width, $this->height);                       
    					
      //handle gifs and jpegs separately
      if($ext=='gif')
	{
	  $image = imagecreatefromgif($file_array[$file_name]['tmp_name']);                           
	  imagecopyresampled($image_p, $image, 0, 0, 0, 0, $this->width, $this->height, $width_orig, $height_orig);
	  imagegif($image_p, $this->save_dir.$temp_filename, 80);
	}
      else
	{
	  $image = imagecreatefromjpeg($file_array[$file_name]['tmp_name']);                           
	  imagecopyresampled($image_p, $image, 0, 0, 0, 0, $this->width, $this->height, $width_orig, $height_orig);                           
	  imagejpeg($image_p, $this->save_dir.$temp_filename, 80);
	}

      imagedestroy($image_p);
      imagedestroy($image);
                       
                   
      $this->filename=$temp_filename;
                 

    }else{
  
      $this->error_message.="<br> file is not a jpeg or gif picture <br>";

    }
  }

}

?>