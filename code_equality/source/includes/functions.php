<?php

/**
 * An assorted mix of functions are stored here
 *
 *                             constants.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}


/**
 * Used to clean the username from the login
 *
 *
 */
function phpbb_clean_username($username)
{
      $username = substr(htmlspecialchars(str_replace("\'", "'", trim($username))), 0, 25);
      $username = phpbb_rtrim($username, "\\");
      $username = str_replace("'", "\'", $username);
  
      return $username;
}


/**
 * This function is a wrapper for ltrim, as charlist is only supported in php >= 4.1.0
 *
 *
 */
function phpbb_ltrim($str, $charlist = false)
{
   if ($charlist === false)
   {
      return ltrim($str);
   }
      
     $php_version = explode('.', PHP_VERSION);
  
// php version < 4.1.0
   if ((int) $php_version[0] < 4 || ((int) $php_version[0] == 4 && (int) $php_version[1] < 1))
   {
       while ($str{0} == $charlist)
       {
            $str = substr($str, 1);
       }
   }
   else
   {
      $str = ltrim($str, $charlist);
   }
  
      return $str;
}
   
/** 
 * Probably have to remove this
 *
 */
function phpbb_rtrim($str, $charlist = false)
{
    if ($charlist === false)
    {
       return rtrim($str);
    }
       
    $php_version = explode('.', PHP_VERSION);
  
 // php version < 4.1.0
    if ((int) $php_version[0] < 4 || ((int) $php_version[0] == 4 && (int) $php_version[1] < 1))
    {
       while ($str{strlen($str)-1} == $charlist)
       {
           $str = substr($str, 0, strlen($str)-1);
       }
    }
    else
    {
         $str = rtrim($str, $charlist);
    }
 
    return $str;
}



/**
 * Get Userdata, $user can be username or user_id. If force_str is true, the username will be forced.
 *
 *
 */
function get_userdata($user, $force_str = false)
{
	global $db;

	if (!is_numeric($user) || $force_str)
	{
		$user = phpbb_clean_username($user);
	}
	else
	{
		$user = intval($user);
	}

	$sql = "SELECT *
		FROM " . USERS_TABLE . " 
		WHERE ";
	$sql .= ( ( is_integer($user) ) ? "user_id = $user" : "username = '" .  str_replace("\'", "''", $user) . "'" ) . " AND user_id <> " . ANONYMOUS;
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, 'Tried obtaining data for a non-existent user', '', __LINE__, __FILE__, $sql);
	}

	return ( $row = $db->sql_fetchrow($result) ) ? $row : false;
}

/**
 *  Encrypt and decrypt functions based on XOR, maybe store the key somewhere else ?
 *
 *
 */
function x_Encrypt($string, $key='storethiskeysomewhereelse')
{
  for($i=0; $i<strlen($string); $i++)
    {
      for($j=0; $j<strlen($key); $j++)
	{
	  $string[$i] = $string[$i]^$key[$j];
	}
    }

  return $string;
}

/**
 * This works off a onetime pad (not used anymore methinks)
 *
 *
 */
function x_Decrypt($string, $key='storethiskeysomewhereelse')
{
  for($i=0; $i<strlen($string); $i++)
    {
      for($j=0; $j<strlen($key); $j++)
	{
	  $string[$i] = $key[$j]^$string[$i];
	}
    }

  return $string;
}

/**
 * Given the user ip address, find the country
 *
 *
 */ 
function geoip( $remote_ip ){
	    
	require_once("geoip.inc");

	$gi = geoip_open('GeoIP.dat',GEOIP_STANDARD);
	
	$remote_country = geoip_country_name_by_addr($gi, $remote_ip) ;
	
	switch ($remote_country) {
 		case "Australia"          : $user_country='australian' ; break;
 		case "Canada"             : $user_country='canadian' ; break; 
 		case "Ireland"            : $user_country='irish' ; break;
 		case "Korea, Republic of" : $user_country='korean' ; break ;
 		case "New Zealand"        : $user_country='nzish' ; break;
 		case "South Africa"       : $user_country='saish' ; break; 
 		case "United Kingdom"     : $user_country='english' ; break;
 		case "United States"      : $user_country='american' ; break;
 		default                   : $user_country='standard' ; break; 
	}
   
    geoip_close($gi);

    return $user_country ;
}

/**
 * Geoip determining for the page_maker array, will decide which country text to put in each users path 
 *
 */
function geoip_zone() 
{

  global $_SERVER;

  $remote_ip = $_SERVER['REMOTE_ADDR'];

  require_once("geoip.inc");

  $gi = geoip_open('GeoIP.dat',GEOIP_STANDARD);

  $remote_country = geoip_country_name_by_addr($gi, $remote_ip) ;

  geoip_close($gi);

  return $remote_country ;

}

/**
 * This matches a country to a flag image
 *
 */
function flag($country)
{

  switch ($country)
    {

    case "australian" : $flag="images/flags/australia.png" ; break;
    case "canadian" : $flag="images/flags/canada.png" ; break;
    case "irish" : $flag="images/flags/ireland.png" ; break;
    case "korean" : $flag="images/flags/south_korea.png" ; break;
    case "nzish" : $flag="images/flags/newzealand.png" ; break;
    case "saish" : $flag="" ; break;
    case "english" : $flag="images/flags/uk.png" ; break;
    case "american" : $flag="images/flags/usa.png" ; break;
    case "standard" : $flag="images/flags/south_korea.png" ; break;
    default: $flag="images/flags/south_korea.png"; break; 
    }

  return $flag ;
}

/**
 * Initialise user settings on page load
 *
 *
 */
function init_userprefs($userdata)
{
	global $board_config, $theme, $images;
	global $template, $lang, $phpEx, $phpbb_root_path;
	global $nav_links;

	if ( $userdata['user_id'] != ANONYMOUS )
	{
		
		if ( !empty($userdata['user_dateformat']) )
		{
			$board_config['default_dateformat'] = $userdata['user_dateformat'];
		}
		
		if ( isset($userdata['user_timezone']) )
		{
			$board_config['board_timezone'] = $userdata['user_timezone'];
		}
	}

        // change here works for index.php only 
	$remote_ip = $_SERVER['REMOTE_ADDR']; 
	
	$country = geoip($remote_ip); 
	$GLOBALS['userdata']['country'] = $country;	

	//	message_die(GENERAL_MESSAGE,'pa');	

	if ( !file_exists(@phpbb_realpath($phpbb_root_path . 'language/lang_' . $country . '/lang_main.'.$phpEx)) )
	{
		require($phpbb_root_path . 'language/lang_' . 'standard' . '/lang_main.' . $phpEx);
	} else {
		require($phpbb_root_path . 'language/lang_' . $country . '/lang_main.' . $phpEx);
	}
	
	if ( defined('IN_ADMIN') )
	{
		if( !file_exists(@phpbb_realpath($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_admin.'.$phpEx)) )
		{
			$board_config['default_lang'] = 'standard';
		}

		include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_admin.' . $phpEx);
	}

	//
	// Set up style
	//
	if ( !$board_config['override_user_style'] )
	{
		if ( $userdata['user_id'] != ANONYMOUS && $userdata['user_style'] > 0 )
		{
			if ( $theme = setup_style($userdata['user_style']) )
			{
				return;
			}
		}
	}

	$theme = setup_style($board_config['default_style']);

	//
	// Mozilla navigation bar
	// Default items that should be valid on all pages.
	// Defined here to correctly assign the Language Variables
	// and be able to change the variables within code.
	//
	$nav_links['top'] = array ( 
		'url' => append_sid($phpbb_root_path . 'index.' . $phpEx),
		'title' => sprintf($lang['Forum_Index'], $board_config['sitename'])
	);
	$nav_links['search'] = array ( 
		'url' => append_sid($phpbb_root_path . 'search.' . $phpEx),
		'title' => $lang['Search']
	);
	$nav_links['help'] = array ( 
		'url' => append_sid($phpbb_root_path . 'faq.' . $phpEx),
		'title' => $lang['FAQ']
	);
	$nav_links['author'] = array ( 
		'url' => append_sid($phpbb_root_path . 'memberlist.' . $phpEx),
		'title' => $lang['Memberlist']
	);

	return;
}

/**
 * Setup the style, this is a bit of a throwback
 *
 */
function setup_style($style)
{
	global $db, $board_config, $template, $images, $phpbb_root_path;

	$sql = "SELECT *
		FROM " . THEMES_TABLE . "
		WHERE themes_id = $style";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(CRITICAL_ERROR, 'Could not query database for theme info');
	}

	if ( !($row = $db->sql_fetchrow($result)) )
	{
		message_die(CRITICAL_ERROR, "Could not get theme data for themes_id [$style]");
	}

	$template_path = 'templates/' ;
	$template_name = $row['template_name'] ;

	$template = new Template($phpbb_root_path . $template_path . $template_name);

	if ( $template )
	{
		$current_template_path = $template_path . $template_name;
		@include($phpbb_root_path . $template_path . $template_name . '/' . $template_name . '.cfg');

		if ( !defined('TEMPLATE_CONFIG') )
		{
			message_die(CRITICAL_ERROR, "Could not open $template_name template config file", '', __LINE__, __FILE__);
		}

		$img_lang = ( file_exists(@phpbb_realpath($phpbb_root_path . $current_template_path . '/images/lang_' . $board_config['default_lang'])) ) ? $board_config['default_lang'] : 'english';

		while( list($key, $value) = @each($images) )
		{
			if ( !is_array($value) )
			{
				$images[$key] = str_replace('{LANG}', 'lang_' . $img_lang, $value);
			}
		}
	}

	return $row;
}

/**
 * Encode the ip address...
 *
 */
function encode_ip($dotquad_ip)
{
	$ip_sep = explode('.', $dotquad_ip);
	return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
}

/**
 * Decode the ip here...
 *
 */
function decode_ip($int_ip)
{
	$hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
	return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

/**
 * Create date/time from format and timezone
 *
 *
 */
function create_date($format, $gmepoch, $tz)
{
	global $board_config, $lang;
	static $translate;

	if ( empty($translate) && $board_config['default_lang'] != 'english' )
	{
		@reset($lang['datetime']);
		while ( list($match, $replace) = @each($lang['datetime']) )
		{
			$translate[$match] = $replace;
		}
	}

	return ( !empty($translate) ) ? strtr(@gmdate($format, $gmepoch + (3600 * $tz)), $translate) : @gmdate($format, $gmepoch + (3600 * $tz));
}


/**
 * General clean up function, this is for text from emails for example which should be clean ...
 *
 *
 */
function gen_clean($word)
{

  // translate the html entities back first
  $word = html_entity_decode($word,ENT_QUOTES);

  // list the control_chars
  $control_chars = array("'",";","<",">",'"');
  $word = str_replace($control_chars,'',$word);

  $word = trim_arr($word);
  $word = input_check($word);

  return $word ;

}


/**
 * This is general replacement for die(), allows templated
 * output in users (or default) language, etc.
 *
 * $msg_code can be one of these constants:
 *
 * GENERAL_MESSAGE : Use for any simple text message, eg. results 
 * of an operation, authorisation failures, etc.
 *
 * GENERAL ERROR : Use for any error which occurs _AFTER_ the 
 * common.php include and session code, ie. most errors in 
 * pages/functions
 *
 * CRITICAL_MESSAGE : Used when basic config data is available but 
 * a session may not exist, eg. banned users
 *
 * CRITICAL_ERROR : Used when config data cannot be obtained, eg
 * no database connection. Should _not_ be used in 99.5% of cases
 */
function message_die($msg_code, $msg_text = '', $msg_title = '', $err_line = '', $err_file = '', $sql = '')
{
	global $db, $template, $board_config, $theme, $lang, $phpEx, $phpbb_root_path, $nav_links, $gen_simple_header, $images;
	global $userdata, $user_ip, $session_length;
	global $starttime;

	if(defined('HAS_DIED'))
	{
		die("message_die() was called multiple times. This isn't supposed to happen. Was message_die() used in page_tail.php?");
	}
	
	define('HAS_DIED', 1);
	

	$sql_store = $sql;
	
	//
	// Get SQL error if we are debugging. Do this as soon as possible to prevent 
	// subsequent queries from overwriting the status of sql_error()
	//
	if ( DEBUG && ( $msg_code == GENERAL_ERROR || $msg_code == CRITICAL_ERROR ) )
	{
		$sql_error = $db->sql_error();

		$debug_text = '';

		if ( $sql_error['message'] != '' )
		{
			$debug_text .= '<br /><br />SQL Error : ' . $sql_error['code'] . ' ' . $sql_error['message'];
		}

		if ( $sql_store != '' )
		{
			$debug_text .= "<br /><br />$sql_store";
		}

		if ( $err_line != '' && $err_file != '' )
		{
			$debug_text .= '</br /><br />Line : ' . $err_line . '<br />File : ' . basename($err_file);
		}
	}

	if( empty($userdata) && ( $msg_code == GENERAL_MESSAGE || $msg_code == GENERAL_ERROR ) )
	{
		$userdata = session_pagestart($user_ip, PAGE_INDEX);
		init_userprefs($userdata);
	}

	//
	// If the header hasn't been output then do it
	//
	if ( !defined('HEADER_INC') && $msg_code != CRITICAL_ERROR )
	{
		if ( empty($lang) )
		{
			if ( !empty($board_config['default_lang']) )
			{
				include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_main.'.$phpEx);
			}
			else
			{
				include($phpbb_root_path . 'language/lang_english/lang_main.'.$phpEx);
			}
		}

		if ( empty($template) )
		{
			$template = new Template($phpbb_root_path . 'templates/' . $board_config['board_template']);
		}
		if ( empty($theme) )
		{
			$theme = setup_style($board_config['default_style']);
		}

		//
		// Load the Page Header
		//
		if ( !defined('IN_ADMIN') )
		{
//			include($phpbb_root_path . 'includes/page_header.'.$phpEx);
		}
		else
		{
//			include($phpbb_root_path . 'admin/page_header_admin.'.$phpEx);
		}
	}

	switch($msg_code)
	{
		case GENERAL_MESSAGE:
			if ( $msg_title == '' )
			{
				$msg_title = $lang['Information'];
			}
			break;

		case CRITICAL_MESSAGE:
			if ( $msg_title == '' )
			{
				$msg_title = $lang['Critical_Information'];
			}
			break;

		case GENERAL_ERROR:
			if ( $msg_text == '' )
			{
				$msg_text = $lang['An_error_occured'];
			}

			if ( $msg_title == '' )
			{
				$msg_title = $lang['General_Error'];
			}
			break;

		case CRITICAL_ERROR:
			//
			// Critical errors mean we cannot rely on _ANY_ DB information being
			// available so we're going to dump out a simple echo'd statement
			//
			include($phpbb_root_path . 'language/lang_english/lang_main.'.$phpEx);

			if ( $msg_text == '' )
			{
				$msg_text = $lang['A_critical_error'];
			}

			if ( $msg_title == '' )
			{
				$msg_title = 'phpBB : <b>' . $lang['Critical_Error'] . '</b>';
			}
			break;
	}

	//
	// Add on DEBUG info if we've enabled debug mode and this is an error. This
	// prevents debug info being output for general messages should DEBUG be
	// set TRUE by accident (preventing confusion for the end user!)
	//
	if ( DEBUG && ( $msg_code == GENERAL_ERROR || $msg_code == CRITICAL_ERROR ) )
	{
		if ( $debug_text != '' )
		{
			$msg_text = $msg_text . '<br /><br /><b><u>DEBUG MODE</u></b>' . $debug_text;
		}
	}

	if ( $msg_code != CRITICAL_ERROR )
	{
		if ( !empty($lang[$msg_text]) )
		{
			$msg_text = $lang[$msg_text];
		}

		if ( !defined('IN_ADMIN') )
		{
			$template->set_filenames(array(
				'message_body' => 'message_body.tpl')
			);
		}
		else
		{
			$template->set_filenames(array(
				'message_body' => 'admin/admin_message_body.tpl')
			);
		}

		$template->assign_vars(array(
			'MESSAGE_TITLE' => $msg_title,
			'MESSAGE_TEXT' => $msg_text)
		);
		$template->pparse('message_body');

		if ( !defined('IN_ADMIN') )
		{
//			include($phpbb_root_path . 'includes/page_tail.'.$phpEx);
		}
		else
		{
//			include($phpbb_root_path . 'admin/page_footer_admin.'.$phpEx);
		}
	}
	else
	{
		echo "<html>\n<body>\n" . $msg_title . "\n<br /><br />\n" . $msg_text . "</body>\n</html>";
	}

	exit;
}

/**
 * Left-over function...
 *
 */
function phpbb_realpath($path)
{
	global $root_path, $phpEx;

	return (!@function_exists('realpath') || !@realpath($root_path . 'includes/functions.'.$phpEx)) ? $path : @realpath($path);
}

/**
 * Basically just trims and array, used to filter email names...
 *
 */
function trim_arr($trim_arr)
{
  return trim($trim_arr);
}

/**
 * Remove the tags and the enclosing text..
 *
 */
function tag_killer($arr_killer)
{
  return preg_replace('/<.*>/','',$arr_killer);
}

/** 
 * Just your basic redirect function...
 *
 *
 */
function redirect($url)
{
	global $db, $board_config;

	if (!empty($db))
	{
		$db->sql_close();
	}

	if (strstr(urldecode($url), "\n") || strstr(urldecode($url), "\r"))
	{
		message_die(GENERAL_ERROR, 'Tried to redirect to potentially insecure url.');
	}

	$server_protocol = ($board_config['cookie_secure']) ? 'https://' : 'http://';
	$server_name = preg_replace('#^\/?(.*?)\/?$#', '\1', trim($board_config['server_name']));
	$server_port = ($board_config['server_port'] <> 80) ? ':' . trim($board_config['server_port']) : '';
	$script_name = preg_replace('#^\/?(.*?)\/?$#', '\1', trim($board_config['script_path']));
	$script_name = ($script_name == '') ? $script_name : '/' . $script_name;
	$url = preg_replace('#^\/?(.*?)\/?$#', '/\1', trim($url));

	// Redirect via an HTML form for PITA webservers
	if (@preg_match('/Microsoft|WebSTAR|Xitami/', getenv('SERVER_SOFTWARE')))
	{
		header('Refresh: 0; URL=' . $server_protocol . $server_name . $server_port . $script_name . $url);
		echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><meta http-equiv="refresh" content="0; url=' . $server_protocol . $server_name . $server_port . $script_name . $url . '"><title>Redirect</title></head><body><div align="center">If your browser does not support meta redirection please click <a href="' . $server_protocol . $server_name . $server_port . $script_name . $url . '">HERE</a> to be redirected</div></body></html>';
		exit;
	}

	// Behave as per HTTP/1.1 spec for others
	header('Location: ' . $server_protocol . $server_name . $server_port . $script_name . $url);
	exit;
}

/**
 * Find the reg_id of the recruiter that recruited a client
 *
 */
function job_id_to_reg_id($job_id,$table)
{

  global $db ;
  
  switch($table)
  {
  
    case 'recruiter_jobs' : $id_col = 'recruiter_jobs_id'   ; break;
    case 'school_jobs'    : $id_col = 'school_jobs_id'      ; break;
    default: break;
  
  }  
 
  $sql = "SELECT user_reg_id FROM $table WHERE $id_col = '$id' "; 
  
  if( !( $result=$db->sql_query($sql) ) )
  {
	message_die(GENERAL_ERROR,'Problem selecting successful match','',__LINE__,__FILE__,$sql );
  }

  while ( $row = $db->sql_fetchrow($result) )
  {
   
     $non_assoc = array_values($row); 
     $name_from_id = $non_assoc[0] ;
   
    return $name_from_id ;

  }

}

/**
 *  Takes the id of a client/applicant and returns a name 
 *
 */
function id_to_name($id,$table)
{
  global $db ;
  
  switch($table)
  {
  
    case 'teachers'       : $name = 'name'   ;  $id_col = 'teacher_id'          ; break;
    case 'recruiter_jobs' : $name = 'school' ;  $id_col = 'recruiter_jobs_id'   ; break;
    case 'school_jobs'    : $name = 'school' ;  $id_col = 'school_jobs_id'      ; break;
    default: break;
  
  }  
 
  $sql = "SELECT $name FROM $table WHERE $id_col = '$id' "; 
  
  if( !( $result=$db->sql_query($sql) ) )
  {
	message_die(GENERAL_ERROR,'Problem selecting successful match','',__LINE__,__FILE__,$sql );
  }

  while ( $row = $db->sql_fetchrow($result) )
  {
   
     $non_assoc = array_values($row); 
     $name_from_id = $non_assoc[0] ;
   
    return $name_from_id ;

  }
}

/** 
 * This is a security function to check the userlevel of the client, it will cause the program to exit if the client doesn't have sufficent secruity level
 *
 */
function access_check($level)
{

  global $userdata ;

  switch($level)
    {
    case (GUEST) : 
      if ( $userdata['user_level'] < GUEST )
	{
	  message_die(GENEAL_MESSAGE,"Sorry, you don't have access to this part of the site..."); 
	} 
     break ;
    case (USER) : 
      if ( $userdata['user_level'] < USER )
	{
	  message_die(GENEAL_MESSAGE,"Sorry, ${userdata['user_level']} u don't have access to this part of the site..."); 
	}
      break ;
    case (ADMIN) :
       if ( $userdata['user_level'] < ADMIN )
	{
	  message_die(GENEAL_MESSAGE,"Sorry, you don't have access to this part of the site..."); 
	}
      break ;
    default :  
      message_die(GENEAL_MESSAGE,"Sorry, ${userdata['user_level']} y don't have access to this part of the site...");
      break ;
    }

  return ;
}

/**
 * Find an email from a name
 *
 */
function id_to_email($id,$table)
{
  global $db ;
  
  switch($table)
  {
  
    case 'teachers'       :  $id_col = 'teacher_id'          ; break;
    case 'recruiter_jobs' :  $id_col = 'recruiter_jobs_id'   ; break;
    case 'school_jobs'    :  $id_col = 'school_jobs_id'      ; break;
    default: break;
  
  }  
 
  $sql = "SELECT email FROM $table WHERE $id_col = '$id' "; 

  if( !( $result=$db->sql_query($sql) ) )
  {
	message_die(GENERAL_ERROR,'Problem selecting successful match','',__LINE__,__FILE__,$sql );
  }

  while ( $row = $db->sql_fetchrow($result) )
  {

     $non_assoc = array_values($row); 
     $email_from_id = $non_assoc[0] ;
   
    return $email_from_id ;

  }
}

/** 
 * This'll take a name and attempt to get an honorfic or just a regular first name from it...
 *
 */ 
function name_getter($name)
{

  $name_array = explode(" ",$name);

  $val = (  stristr($name,'mr.') || stristr($name,'mr') ||
       stristr($name,'mrs') || stristr($name,'mrs.') ||
       stristr($name,'ms') || stristr($name,'ms.')  ) ;



  if (  stristr($name,'mr.') || stristr($name,'mr') ||
       stristr($name,'mrs') || stristr($name,'mrs.') ||
       stristr($name,'ms') || stristr($name,'ms.')  )
  {

    $name = $name_array[1];

  }
  else{
      
    $name = $name_array[0] . ' ' . $name_array[1] ;

  }

  // this is output heavy, I don't want email addresses tacked on to the end
  // if there is a third part to the name, add it
  //  if ( isset($name_array[2]) )
  //  {
  //    $name = $name . ' ' . $name_array[2] ;
  //  }

  return $name ;

}

/**
 * This'll take a name and attemp to get a first name from it.
 *
 */
function first_name_getter($name)
{

  $name_array = explode(" ",$name);

  if(  stristr($name,'mr.') || stristr($name,'mr') ||
       stristr($name,'mrs') || stristr($name,'mrs.') ||
       stristr($name,'ms') || stristr($name,'ms.')  )
  {
  
    $name = $name_array[1];

  }
  else{
      
    $name = $name_array[0]  ;

  }

  return $name ;

}

/**
 * This'll take a full name and attempt to get a title from it
 *
 */
function title_getter($full_name)
{

  $name_array = explode(" ",$full_name);

  if(  stristr($full_name,'mr.') || stristr($full_name,'mr') || stristr($full_name,'mrs') || stristr($full_name,'mrs.') || stristr($full_name,'ms') || stristr($full_name,'ms.')  )
  {

    $title = $name_array[0];

  }
  else{
      
    $title = '';

  }

  return $title ;

}

/**
 *  Take the values from the db and minus 1 to get the month value to work
 *
 */
function js_calendar_show($arr)
{
  
	foreach ( $arr as $key => $value )
	  {
	    // adjust for the js calendar for putting jan = 0
	    if (stristr($key,'fixed_date'))
	      {
		$value_arr = explode('-',$value);
		$value_arr[1]=$value_arr[1]-1;
		$arr[$key]=implode('-',$value_arr);
	      }
	  }  
	    return $arr ;
}

/**
 * Take the values from the js calendar and add one to the month to store
 *
 */ 
function js_calendar_store($arr)
{
  
	foreach ( $arr as $key => $value )
	  {
	    // adjust for the js calendar for putting jan = 0
	    if (stristr($key,'fixed_date'))
	      {
		$value_arr = explode('-',$value);
		$value_arr[1]=$value_arr[1]+1;
		$arr[$key]=implode('-',$value_arr);
	      }
	  }  

	    return $arr ;

}

/**
 * Simple function to get the board admin's email
 *
 */
function get_admin_email()
{
  
  global $db ;

  $sql = "SELECT user_email FROM users WHERE user_level = " . ADMIN ;

  if ( !($result = $db->sql_query($sql) ) )
    {

      message_die(GENERAL_MESSAGE,'Problem getting the regdate');

    }

  $row = $db->sql_fetchrow($result) ;

  return $row['user_email'];

}

/**
 * Find the user email from the regdate
 *
 */
function email_from_regdate($reg_date)
{

  global $db ;

  $sql = "SELECT user_email FROM " . USERS . " WHERE user_regdate ='$reg_date' " ;

  if ( !($result = $db->sql_query($sql) ) )
    {

      message_die(GENERAL_MESSAGE,'Problem getting the regdate');

    }

  $row = $db->sql_fetchrow($result) ;

  return $row['user_email'];

}

/**
 *  Find the username from the reg_date ...
 *
 */
function regdate($regdate)
{

  global $db ;

  $sql = "SELECT username FROM " . USERS . " WHERE user_regdate='$regdate' AND username != 'Anonymous' " ;

  if ( !($result = $db->sql_query($sql) ) )
    {

      message_die(GENERAL_MESSAGE,'Problem getting the regdate');

    }

  $row = $db->sql_fetchrow($result) ;

  return $row['username'];

}

 /**
  * Take a username and find the reg_date from the users table ...
  *
  */
function name2regdate($user)
{

  global $db ;

  $sql = "SELECT user_regdate FROM users WHERE username='$user' " ;

  if ( !($result = $db->sql_query($sql) ) )
    {

      message_die(GENERAL_MESSAGE,'Problem getting the regdate');

    }

  $row = $db->sql_fetchrow($result) ;

  return $row['user_regdate'];

}

/** 
 * This will take a load of different control_panel actions and translate them  into 'English', some functions are actually tied to the their name. So if you change the language, you might have to change the name of the function also. This will provide a layer of encapsulation for that...
 *
 */
function define_action($action)
{
  
  switch($action)
  {

    case 'phoneTeacher' : $word = "Phone Teacher" ; break ;
    case 'delTeacher' : $word = "Delete Teacher Resume" ; break ;
    case 'phoneRecruiter' : $word = "Phone Recruiter" ; break ;
    case 'delRecruiter' : $word = "Delete Recruiter Job" ; break ;
    case 'phoneSchool' : $word = "Phone School" ; break ;
    case 'delSchool' : $word = "Delete School Job" ; break ; 
    case 'successCompleted' : $word = "Successfully Completed" ; break ; 
    case 'successFailed' : $word = "Success Failed" ; break ; 
    case 'rMatchSent' : $word = "Recruiter Match Sent" ; break ;
    case 'rMatchFailed' : $word = "Recruiter Match Failed" ; break ; 
    case 'sMatchSent' : $word = "School Match Sent" ; break ; 
    case 'sMatchFailed' : $word = "School Match Failed" ; break ; 

    default: $word="Unknown" ;break; 

  }

  return $word ;

}

/** 
 * Take in the action and the id from the control_panel and give back a valid link
 *
 */
function data_format($action,$data)
{

  switch ($action)
  {

  case 'delRecruiter' : $name=id_to_name($data,'recruiter_jobs') ; 
                             $link = "<a href='recruiter_gallery.php?id=".$data."' >$name</a>" ; 
			     break;
  case 'delTeacher' : $name=id_to_name($data,'teachers') ; 
                             $link = "<a href='gallery.php?id=".$data."' >$name</a>" ; 
			     break;
  case 'delSchool' : $name=id_to_name($data,'school_jobs') ; 
                             $link = "<a href='school_gallery.php?id=".$data."' >$name</a>" ; 
			     break;
  case 'successCompleted' :  $teacher_id=preg_replace('/--.*/','',$data);
                             $teacher_name=id_to_name($teacher_id,'teachers'); 
                             $job_id=preg_replace('/.*--/','',$data);
                             $job_name=id_to_name($job_id,'recruiter_jobs'); 
                             $link = "<a href='gallery.php?id=".$teacher_id."' >$teacher_name</a>
                                      <br />
                                      <a href='school_gallery.php?id=".$job_id."' >$job_name</a>" ; 
			     break;
  case 'successFailed' :  $teacher_id=preg_replace('/--.*/','',$data);
                             $teacher_name=id_to_name($teacher_id,'teachers'); 
                             $job_id=preg_replace('/.*--/','',$data);
                             $job_name=id_to_name($job_id,'recruiter_jobs'); 
                             $link = "<a href='gallery.php?id=".$teacher_id."' >$teacher_name</a>
                                      <br />
                                      <a href='school_gallery.php?id=".$job_id."' >$job_name</a>" ; 
			     break;
  case 'rMatchSent' :     $teacher_id=preg_replace('/--.*/','',$data);
                             $teacher_name=id_to_name($teacher_id,'teachers'); 
                             $job_id=preg_replace('/.*--/','',$data);
                             $job_name=id_to_name($job_id,'recruiter_jobs'); 
                             $link = "<a href='gallery.php?id=".$teacher_id."' >$teacher_name</a>
                                      <br />
                                      <a href='recruiter_gallery.php?id=".$job_id."' >$job_name</a>" ; 
			     break;
  case 'rMatchFailed' :  $teacher_id=preg_replace('/--.*/','',$data);
                             $teacher_name=id_to_name($teacher_id,'teachers'); 
                             $job_id=preg_replace('/.*--/','',$data);
                             $job_name=id_to_name($job_id,'recruiter_jobs'); 
                             $link = "<a href='gallery.php?id=".$teacher_id."' >$teacher_name</a>
                                      <br />
                                      <a href='recruiter_gallery.php?id=".$job_id."' >$job_name</a>" ; 
			     break;
  case 'sMatchSent' :  $teacher_id=preg_replace('/--.*/','',$data);
                             $teacher_name=id_to_name($teacher_id,'teachers'); 
                             $job_id=preg_replace('/.*--/','',$data);
                             $job_name=id_to_name($job_id,'school_jobs'); 
                             $link = "<a href='gallery.php?id=".$teacher_id."' >$teacher_name</a>
                                      <br />
                                      <a href='school_gallery.php?id=".$job_id."' >$job_name</a>" ; 
			     break;
  case 'sMatchFailed' :  $teacher_id=preg_replace('/--.*/','',$data);
                             $teacher_name=id_to_name($teacher_id,'teachers'); 
                             $job_id=preg_replace('/.*--/','',$data);
                             $job_name=id_to_name($job_id,'recruiter_jobs'); 
                             $link = "<a href='gallery.php?id=".$teacher_id."' >$teacher_name</a>
                                      <br />
                                      <a href='school_gallery.php?id=".$job_id."' >$job_name</a>" ; 
			     break;
  default: break;
  }

  return $link ;

}

?>