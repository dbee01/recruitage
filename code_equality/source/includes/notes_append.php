<?php

/**
 * Class handles all the notes append and candidate/applicant diary
 *
 *                              notes_append.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 * 
 **/

// call this from the include
$notes_append = new notes_append();

class notes_append {

  var $date ;
  
  /**
   * Build constructor here, set date
   *
   */
  function notes_append()
  {

    $this->date=date('d-M');

  }

  /**
   * Append notes to an applicant
   *
   */
  function append_teachers($id, $notes)
  {

        global $db ;        

        $notes = mysql_real_escape_string($notes);
	$notes = $this->date.': '.$notes.'\n';

    	$sql_select = "UPDATE " . TEACHERS . " SET recruiter_notes = CONCAT(recruiter_notes,'$notes') WHERE teacher_id = '$id'";

	if( !( $result=$db->sql_query($sql_select) ) )
	{
	  message_die(GENERAL_ERROR,'Could not connect to the table to add notes...','',__LINE__,__FILE__,$sql_select );
	}


  }

  /** 
   * Append notes to schools 
   *
   */
  function append_schools($id, $notes)
  {
 
        global $db;

        $notes = mysql_real_escape_string($notes);
	$notes = $this->date.': '.$notes.'\n';

    	$sql_select = "UPDATE " . SCHOOL_JOBS . " SET recruiter_notes = CONCAT(recruiter_notes,'$notes') WHERE school_jobs_id = '$id'";

	if( !( $result=$db->sql_query($sql_select) ) )
	{
	  message_die(GENERAL_ERROR,'Could not connect to the table to add notes...','',__LINE__,__FILE__,$sql_select );
	}


  }

  /**
   * Append to recruiters note
   *
   */
  function append_recruiters($id, $notes)
  {

        global $db ;

        $notes = mysql_real_escape_string($notes);
	$notes = $this->date.': '.$notes.'\n';

        $sql_select = "UPDATE " . RECRUITER_JOBS . " SET recruiter_notes = CONCAT(recruiter_notes,'$notes') WHERE recruiter_jobs_id = '$id'";

	if( !( $result=$db->sql_query($sql_select) ) )
	{
	  message_die(GENERAL_ERROR,'Could not connect to the table to add notes...','',__LINE__,__FILE__,$sql_select);
	}


  }

  /**
   * There has been a match, so update the client notes and applicant notes
   *
   */
  function append_job_match($job_id, $teacher_id, $notes)
  {

        global $db; 
 
        $notes = mysql_real_escape_string($notes);
        $notes = $this->date.': '.$notes.'\n';

	$sql_select = "UPDATE sent_match SET job_notes = CONCAT(job_notes,'$notes') WHERE job_id = '$job_id' AND teacher_id='$teacher_id'";
	
	if( !( $result=$db->sql_query($sql_select) ) )
	{
	  message_die(GENERAL_ERROR,'Could not connect to the table to add notes...','',__LINE__,__FILE__,$sql_select );
	}

  }


  /**
   * There has been a teacher match so append to client and job notes
   *
   */
  function append_teacher_match($teacher_id, $job_id, $notes)
  {

        global $db ;
 
        $notes = mysql_real_escape_string($notes);
	$notes = $this->date.': '.$notes.'\n';

	$sql_select = "UPDATE " . SENT_MATCH . " SET teacher_notes = CONCAT(teacher_notes,'$notes') WHERE teacher_id = '$teacher_id' AND job_id = '$job_id' ";

	if( !( $result=$db->sql_query($sql_select) ) )
	{
	  message_die(GENERAL_ERROR,'Could not connect to the table to add notes...','',__LINE__,__FILE__,$sql_select );
	}


  }


}



?>
