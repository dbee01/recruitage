<?php

/**
 * School gallery is an internet facing page which gives details of a school job it will show different info depend on who is viewing it ... GUEST/USER/ADMIN
 *
 *                             school_gallery.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 **/

// anti_hacker thing
define('IN_DECRUIT', true);

// root path
$root_path = './';

// include these scripts
include($root_path . 'extension.inc');
include($root_path . 'common.'.$phpEx);

$page= $_GET['id'];
if ( $page == '' ){
  $page='example';
}

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_SCHOOL_GALLERY);
init_userprefs($userdata);
//
// End session management

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename']
			     ));


// this comes after the session setup
include($root_path . 'includes/navbar.' .$phpEx);

// are we looking to update this page ? 
// if so (and we are logged in as root)., then output this page as a form
if ( ($_GET['mode']=='print_form') &&  ($userdata['user_level'] == ADMIN ) ){
 	
  $template->set_filenames(array('body' => 'school_print_form.tpl'));

}	
else{

  $template->set_filenames(array('body' => 'school_gallery.tpl'));
}

// output the navbar
include($root_path . 'includes/navbar.'.$phpEx);

// handle the update of recruiter notes here
// simply replace the previous recruiter notes with the new one
if (isset($_POST['recruiter_notes']))
{
	
  $id = $_POST["school_jobs_id"];
  $recruiter_notes = htmlentities($_POST["recruiter_notes"], ENT_QUOTES, 'UTF-8');

  $append = new notes_append();
  $append->append_schools($id,$recruiter_notes);

  redirect(append_sid("/control_panel.$phpEx", true));

}

// handle the request from a teacher response email form here
// the teacher will have to give a yes or a know answer with 
// regards to the school.
if (    isset( $_GET['teacher_response_form'] ) 
	&& isset( $_GET['name'] ) 
	&& isset( $_GET['teacher_id'] ) 
	)
{

  // security choke point
  $_GET = array_map("input_check",$_GET);

  @extract($_GET);   
   
  $template->assign_block_vars('TEACHER_RESPONSE_FORM',array() );
    
  $teacher_name = $_GET['name'];
  $teacher_id = $_GET['teacher_id'];
    
  $template->assign_vars( array( 'SCHOOL_TEACHER_NAME' => $teacher_name, 
				 'SCHOOL_TEACHER_ID' => $teacher_id ) );

}

$sql = "SELECT * FROM school_jobs WHERE school_jobs_id = '$page' ";
			
if ( !($result = $db->sql_query($sql)) )
{
  message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
}	

while ($row = $db->sql_fetchrow($result) )
{
  
  // make this into a function 	  	
  $accomodation=$row['accomodation'] ;
		
  switch ( $accomodation )
    {
    case (1) : $accomodation = 'Single Accomodation'; break ;
    case (2) : $accomodation = 'Shared Accomodation'; break ; 
    default :  $accomodation = '' ;   
    }

			
  ( $row['kindergarten'] ? $kindergarten = 'Kindergarten, ' : $kindergarten = '') ;
  ( $row['elementary'] ? $elementary = 'Elementary, ' : $elementary = '') ;
  ( $row['middle_school'] ? $middle_school = 'Middle-School, ' : $middle_school = '') ;
  ( $row['high_school'] ? $high_school = 'High-School, ' : $high_school = '') ;
  ( $row['adults'] ? $adults = 'Adults, ' : $adults = '') ;

  $students=($kindergarten . $elementary . $middle_school . $high_school . $adults);
  
  // parse these values
  $city_schools=location($row['city']);
  $salary=salary($row['salary']) ;
  $arrival=arrival_to_text($row['arrival']);

  $contract=basename($row['contract']);		
  $print_form='school_gallery.php?mode=print_form&id='.$row['school_jobs_id'];		
  
  // get first name only
  $name=$row['contact'];
  list($first_name,) = explode(' ', $name);

  // Include form select to turn this from a normal school gallery input form 
  // processor, to a school gallery update print form from the control panel
  // form select will highlight the options already chosen by the school
  include($root_path . '/includes/form_select.'.$phpEx);

  // Include the form picker so the owner can pick which parts
  // of the form they want to display.
  $template->assign_block_vars('job0_form',array()); 

  if ( $userdata['session_logged_in'] && ($userdata['user_level'] == ADMIN ) )
    {
      $template->assign_block_vars('ADMIN_LOG_IN',array() );
    }	

  // what about manual attribution of school's interest in teachers ?	  
  if ( $userdata['user_level'] >= 1  )
    {
	
      // manually set who the candidates are intersted in.
      // the candidates should reply by form, but some aren't clever enough to fill out a form
      $sql_teacher = "SELECT name, teacher_id FROM teachers WHERE status != -1";
	    
      if ( !($result_teacher = $db->sql_query($sql_teacher))  )
	{
	  message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql_teacher);
	}	    

      $template->assign_block_vars('job0_form.interest_select',array(
								     'INTEREST_SELECT'=>$lang['Interest_select'],
								     'L_BOOK_CALL'=>$lang['Book_call']
								     )
				   );
		    
      while($row_teacher = $db->sql_fetchrow($result_teacher))
	{
	  
	  $template->assign_block_vars('job0_form.interest_select.teacher_interest_select',array(
												 'TEACHER_NAME'=>$row_teacher['name'],
												 'TEACHER_ID'=>$row_teacher['teacher_id']
												 )
				       ); 
	
	}

    }

  // output the various values depending on whether or not the admin has set them to be output
  if ( ( $page_maker["job0_upload_students"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_students_sec"] ))) 
    {
		    
      $page_maker['job0_upload_students_check'] ? $check = 'check' : $check = 0 ;

		    
      // this is a quick hack. Not sure what I'm going to do with this section
      $template->assign_block_vars('job0_form.students',
				   array(
					 'L_STUDENTS'=>'Students',
					 'STUDENTS'=>$students
					 )); 

    }

  if ( ( $page_maker["job0_upload_city"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_city_sec"] ))) 
    {

      $page_maker['job0_upload_city_check'] ? $check = 'check' : $check = 0 ;

      // this is a quick hack. Not sure what I'm going to do with this section
      $template->assign_block_vars('job0_form.location',
				   array(
					 'L_LOCATION'=>'Location',
					 'LOCATION'=>$city_schools
					 )); 
    }

  if ( ( $page_maker["job0_upload_contact"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_contact_sec"] ))) 
    {

      $page_maker['job0_upload_contact_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.contact',
				   array(
					 'L_CONTACT'=>$lang['Contact'],
					 'SCHOOL_CONTACT'=>$row['contact'],
					 'CHECK'=>$check
					 )); 

    }	
	
  if ( ( $page_maker["job0_upload_school"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_school_sec"] ))) 
    {

      $page_maker['job0_upload_school_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.school',
				   array(
					 'L_SCHOOL'=>$lang['School'],
					 'SCHOOL_NAME'=>$row['school'],
					 'CHECK'=>$check
					 )); 
    }
	
  // bit of  quandary here ... diff between seeing the
  // value on form input and seeing the value
  // when we send the form to people ...
  if ( ( $page_maker["job0_upload_email"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_email_sec"] ))) 
    {

      $page_maker['job0_upload_email_check'] ? $check = 'check' : $check = 0 ;

      // we might have to switch it up for the print_form part
      $email_decode = $row['email'];
      $email_encode = base64_encode($row['email']);

      // if user show email is 1 then output proper email address.
      // otherwise just output the base64 non-visible version.
      if ( !($userdata['user_show_email']) )
	{
	  $email = $email_encode;

	  // always show the decoded email to the print_form_update page
	  if ( $print_form_flag )
	    {
	      $email = $email_decode ;
	      $email_visible = "style='display:none;'";
	    }

	}
      else 
	{
	  $email = $email_decode;
	}

      $template->assign_block_vars('job0_form.email',
				   array('L_EMAIL'=>$lang['Email'],
					 'SCHOOL_FIRST_NAME'=>$first_name,
					 'SCHOOL_EMAIL'=>$email,
					 'SCHOOL_EMAIL_VISIBLE'=>$email_visible,
					 'CHECK'=>$check
					 )); 
    }

  if ( ( $page_maker["job0_upload_phone"] )  && 
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_phone_sec"] ))) 
    {

      $phone = new phone();

      $intl_number = $row['country_code'].$row['phone'];

      $intl_number = $phone->phone_chopper($intl_number);
      $tel_number = $phone->callback_link($intl_number);
      $sms_number = $phone->sms_link($intl_number);

      // get the phone mode 
      $phone_mode = cp_phone_mode($intl_number,$userdata['user_regdate'],'school',1);

      $page_maker['job0_upload_phone_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.phone',
				   array('L_PHONE'=>$lang['Telephone'],
					 'SCHOOL_PHONE'=>$row['phone'],
					 'SCHOOL_CALLBACK'=>$phone_mode,
					 'SCHOOL_SMS'=>$sms_number,
					 'CHECK'=>$check
					 )); 

    }

  if ( ( $page_maker["job0_upload_school_description"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_school_description_sec"] ))) 
    {

      $page_maker['job0_upload_school_description_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.school_description',
				   array('L_SCHOOL_DESCRIPTION'=>$lang['School_description'],
					 'SCHOOL_DESCRIPTION'=>$row['school_description'],
					 'CHECK'=>$check)
				   ); 
    }
	

  if ( ( $userdata['user_level'] >= 1 )) 
    {

      $page_maker['job0_upload_school_notes_check'] ? $check = 'check' : $check = 1 ;

      $template->assign_block_vars('job0_form.recruiter_notes',
				   array(
					 'L_RECRUITER_NOTES'=>$lang['Recruiter_notes'],
					 'RECRUITER_NOTES'=>nl2br($row['recruiter_notes']),
					 'SCHOOL_JOBS_ID'=>$id,
					 'CHECK'=>$check
					 )); 

    }	

  if ( ( $page_maker["job0_upload_arrival"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_arrival_sec"] ))) 
    {

      $page_maker['job0_upload_arrival_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.arrival',
				   array('L_ARRIVAL'=>$lang['Arrival'],
					 'L_ARRIVAL_BLOCK'=>$teacher_arrival_list,
					 'ARRIVAL'=>$arrival,
					 'CHECK' => $check
					 )
				   ); 
    }

  if ( ( $page_maker["job0_upload_employee_ref"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_employee_ref_sec"]=0 ))) 
    {

      $page_maker['job0_upload_employee_ref_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.employee_ref',
				   array('L_EMPLOYEE_REF'=>$lang['Employee_ref'],
					 'EMPLOYEE_REF'=>$row['foreign_teacher'],
					 'CHECK' => $check 
					 )); 
    }

  if ( ( $page_maker["job0_upload_salary"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_salary_sec"] ))) 
    {

      $page_maker['job0_upload_salary_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.salary',
				   array('L_SALARY'=>$lang['Salary_offer'],
					 'SALARY'=>$salary,
					 'SALARY_BLOCK'=>$salary_list,
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( ( $page_maker["job0_upload_number_teachers"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_number_teachers_sec"] ))) 
    {

      $page_maker['job0_upload_number_teachers_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.number_teachers',
				   array('L_NUM_TEACHERS'=>$lang['Number_teachers'],
					 'NUM_TEACHERS'=>$row['number_teachers'],
					 'CHECK'=>$check
					 )); 
    }

  if ( ( $page_maker["job0_upload_accomodation"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_accomodation_sec"] ))) 
    {
	    
      $page_maker['job0_upload_accomodation_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.accomodation',
				   array('L_ACCOMODATION'=>$lang['Accomodation'],
					 'ACCOMODATION'=>$accomodation,
					 'CHECK'=>$check
					 )); 
    }

  if ( ( $page_maker["job0_upload_gender"] )  &&
       (  $userdata['user_level'] >= ( $page_maker["job0_upload_gender_sec"] ))) 
    {

      $page_maker['job0_upload_gender_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('job0_form.gender',
				   array('L_GENDER'=>$lang['Gender'],
					 'GENDER'=>$row['gender'],
					 'CHECK'=>$check
					 )); 
    }

  $template->assign_vars(
			 array( 'SCHOOL_CONTACT' => $row['contact'], 
				'SCHOOL_PRINT_FORM' => $print_form,
				'SCHOOL_NAME' => $row['school'],
				'LOCATION_BLOCK' => $location_block,
				'SCHOOL_JOBS_ID' => $row['school_jobs_id']
				) );
}                                                           
	
$db->sql_freeresult($result);

//
// Generate the page
//
$template->pparse('body');

?>