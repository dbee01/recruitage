<?php

/**
 * Login page  
 *                              login.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

//
// Allow people to reach login page if
// board is shut down
//
define("IN_LOGIN", true);

// anti-hacker
define('IN_DECRUIT', true);

// we can't hard-cache this cause if they fail to log in one time, then 
// the 'sorry you cannot login' thing will always show up
header("Expires: -1");
header("Pragma: no-cache");

$root_path = './';

// these scripts are needed
include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx);

//
// Set page ID for session management
//
$userdata = session_pagestart($user_ip, PAGE_LOGIN);
init_userprefs($userdata);
//
// End session management
//

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename']
			     ));

// session id check
if (!empty($HTTP_POST_VARS['sid']) || !empty($HTTP_GET_VARS['sid']))
{
  $sid = (!empty($HTTP_POST_VARS['sid'])) ? $HTTP_POST_VARS['sid'] : $HTTP_GET_VARS['sid'];
}
else
{
  $sid = '';
}

// set template form
$template->set_filenames(array( 'body' => 'login.tpl'));

// navbar inclusion
include($root_path . 'includes/navbar.'.$phpEx);

// we've been passed a form with login or logout values
if( isset($HTTP_POST_VARS['login']) || isset($HTTP_GET_VARS['login']) || isset($HTTP_POST_VARS['logout']) || isset($HTTP_GET_VARS['logout']) )
{

  // if we have a login $_POST/$_GET form and the user isn't presently logged in
  if( ( isset($HTTP_POST_VARS['login']) || isset($HTTP_GET_VARS['login']) ) && (!$userdata['session_logged_in'] || isset($HTTP_POST_VARS['admin'])) )
    {

      // take the name and password from the form
      $username = isset($HTTP_POST_VARS['username']) ? phpbb_clean_username($HTTP_POST_VARS['username']) : '';
      $password = isset($HTTP_POST_VARS['password']) ? $HTTP_POST_VARS['password'] : '';
		
      // select all that user's details
      $sql = "SELECT user_id, username, user_password, user_active, user_level, user_login_tries, user_last_login_try
			FROM users
			WHERE username = '" . str_replace("\\'", "''", $username) . "'";

      // execute sql statement
      if ( !($result = $db->sql_query($sql)) )
	{
	  echo "Critical Error - can't connect to the database for user data."; 
	}
	  
      // if we get back results
      if( $row = $db->sql_fetchrow($result) )
	{
		
	  // if the user isn't the admin and the board is disabled
	  if( $row['user_level'] != ADMIN && $board_config['board_disable'] )
	    {
	      redirect(append_sid("index.$phpEx", true));
	    }
	  // check the md5sum of the password hash
	  else
	    {

	      // password checks and user is active 
	      if( md5($password) == $row['user_password'] && $row['user_active'] )
		{

		  // is the autologin var from the form set to true
		  $autologin = ( isset($HTTP_POST_VARS['autologin']) ) ? TRUE : 0;

		  // is this the admin ?
		  $admin = (isset($HTTP_POST_VARS['admin'])) ? 1 : 0;

		  // store a session in the session table, get back session id
		  $session_id = session_begin($row['user_id'], $user_ip, PAGE_INDEX, FALSE, $autologin, $admin);
		      
		  if( $session_id )
		    {

      		      // SUCCESSFUL LOGIN
		      redirect("control_panel.$phpEx",true);

		    }
		  else
		    {
						
		      echo ('GENERAL ERROR : session info problem '); 

		    }
		}
	      // give out the password error message
	      else
		{

		  $login_error = "Sorry, there has been a problem with your login <br> Pls contact the <a href='http://www.englishteachingkorea.com/form.php?mode=contact_us_form' >administrator</a> <br> Thanks";

		  $template->assign_block_vars( 'SITE_LOGIN_ERROR' , array() );		
		  $template->assign_var( 'L_SITE_LOGIN_ERROR' , $login_error );

		  $template->pparse('body');                 							

		}
	    }
	}
      else
	{
	  echo 'Critical Error : could not connect to the database' ;
	}
    }
  // we have a login form with $_POST/$_GET login variables, but the user is already logged in 
  else
    {
	  
      $url='control_panel.php?mode=cp_intro';
      redirect($url);

      // $url = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : "index.$phpEx";
      // redirect(append_sid($url, true));

    }
} 
// we've hit the login page, but there isn't any form $_GET or $_POST variables
// lets' just keep them moving. Either send them on, or stop them.
else
{ 

  // if user isn't logged in, then send them to the login page
  if( !($userdata['session_logged_in']) )
    {
	  
      $username = ( $userdata['user_id'] != ANONYMOUS ) ? $userdata['username'] : '';

      // interesting hidden fields here

      $template->assign_block_vars( 'RECRUITER_LOGIN' , array() );
		
      $template->assign_vars(array( 'L_RECRUITER_LOGIN' => $lang['Recruiter_login'] ) );

    } // if they are logged in then send them to intro
  else
    {

      $url='control_panel.php?mode=cp_intro';
      redirect($url);

    }

  // parse this page
  $template->pparse('body');

}

?>