<?php

/***************************************************************************
 *                            recruiter_panel.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * @description          : Send the sms code
 *
 ***************************************************************************/

/* Load sms-api class */
require_once ("sms_api.php");

/* Initialize object */
$mysms = new sms();

/* Send message or print SMS credit balance */
if ($_GET['balance']<>1) {
    $results = $mysms->send($to,$from,$message);
    echo "Sending results: ". $results;
} else {
    $balance = $mysms->getbalance();
    echo "Current SMS balance: ".$balance;
}

?>