<?php

/**
 * Job_calendar page is just a page to store the job reminders that clients can enter from job_calendar.php
 *
 *                               job_calendar.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 **/

define('IN_DECRUIT', true);

$phpbb_root_path = './';

// let's hard-cache all forms ...
header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_CALENDAR);
init_userprefs($userdata);
//
// End session management
//

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename']
			     ));

// set the language here ...
// the job calendar form can be submitted in either language
// the job_calendar.php?mode=email needs to be in english though
if ($_GET['lang'] == 'kr'){

  include($phpbb_root_path . 'language/lang_korean.'.$phpEx);

}else {

  include($phpbb_root_path . 'language/lang_common.'.$phpEx);

}

// set the template
$template->set_filenames(array('body' => 'job_calendar.tpl'));

// set the navbar
include( $phpbb_root_path . 'includes/navbar.' . $phpEx );

// take these sections from the common ground that is $form_section
$organization_block = $form_section->get_organization_list();
$location_block = $form_section->get_location_options();
$salary_block = $form_section->get_salary_list();

$template->assign_vars(array(
			     'SELECT'=>$lang['Select'],
			     'ORGANIZATION_BLOCK'=>$organization_block,
			     'LOCATION_BLOCK'=>$location_block,
			     'SALARY_BLOCK'=>$salary_block,
			     'JOB_CALENDAR_TITLE' => $lang['Job_calendar_title'],
			     'JOB_CALENDAR_EMAIL_TITLE' => $lang['Job_calendar_email_title'],
			     'JOB_CALENDAR_DESCRIPTION' => $lang['Job_calendar_description'],
			     'JOB_CALENDAR_EMAIL_DESCRIPTION' => $lang['Job_calendar_email_description'],
			     'JOB_CALENDAR_SCHOOL' => $lang['Job_calendar_school'],
			     'JOB_CALENDAR_SCHOOL_DESCRIPTION' => $lang['Job_calendar_school_description'],
			     'JOB_CALENDAR_SCHOOL_DESCRIPTION_TEXT' => $lang['Job_calendar_school_description_text'],
      			     'JOB_CALENDAR_CONTACT' => $lang['Job_calendar_contact'],
			     'JOB_CALENDAR_EMAIL' => $lang['Job_calendar_email'],
			     'JOB_CALENDAR_TEACHER_NAME' => $lang['Job_calendar_teacher_name'],
			     'JOB_CALENDAR_NUMBER_TEACHERS'=> $lang['Job_calendar_number_teachers'],
			     'JOB_CALENDAR_TEACHER_NUMBER'=> $lang['Job_calendar_teacher_number'],
			     'JOB_CALENDAR_TEACHER_LEAVING'=> $lang['Job_calendar_teacher_leaving'],
			     'JOB_CALENDAR_CITY' => $lang['Job_calendar_city'],
			     'JOB_CALENDAR_PHONE' => $lang['Job_calendar_phone'],
			     'JOB_CALENDAR_BASE_SALARY' => $lang['Job_calendar_base_salary'],
			     'JOB_CALENDAR_ORGANIZATION' => $lang['Job_calendar_organization']
			     )
		       );

// this is just the general job calendar output form, used mostly by the 
// recruiter and accessed by the control_panel. This isn't part of the 
// email campaign
// probably should have programmed this laterallly...
if( isset($_POST['school']) && !(isset($_POST['email_campaign']))  ){	
	
	$keys=array();
	
	$school = htmlentities($_POST["school"], ENT_QUOTES, 'UTF-8');	
	$contact = htmlentities($_POST["contact"], ENT_QUOTES, 'UTF-8');	
	$email = htmlentities($_POST["email"], ENT_QUOTES, 'UTF-8');	
	$organization = htmlentities($_POST["orgainization"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_1 = htmlentities($_POST["leaving_name_1"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_1 = htmlentities($_POST["leaving_date_1"], ENT_QUOTES, 'UTF-8');		
	$leaving_name_2 = htmlentities($_POST["leaving_name_2"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_2 = htmlentities($_POST["leaving_date_2"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_3 = htmlentities($_POST["leaving_name_3"], ENT_QUOTES, 'UTF-8');		
	$leaving_date_3 = htmlentities($_POST["leaving_date_3"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_4 = htmlentities($_POST["leaving_name_4"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_4 = htmlentities($_POST["leaving_date_4"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_5 = htmlentities($_POST["leaving_name_5"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_5 = htmlentities($_POST["leaving_date_5"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_6 = htmlentities($_POST["leaving_name_6"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_6 = htmlentities($_POST["leaving_date_6"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_7 = htmlentities($_POST["leaving_name_7"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_7 = htmlentities($_POST["leaving_date_7"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_8 = htmlentities($_POST["leaving_name_8"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_8 = htmlentities($_POST["leaving_date_8"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_9 = htmlentities($_POST["leaving_name_9"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_9 = htmlentities($_POST["leaving_date_9"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_10 = htmlentities($_POST["leaving_name_10"], ENT_QUOTES, 'UTF-8');		
	$leaving_date_10 = htmlentities($_POST["leaving_date_10"], ENT_QUOTES, 'UTF-8');		
	$leaving_name_11 = htmlentities($_POST["leaving_name_11"], ENT_QUOTES, 'UTF-8');		
	$leaving_date_11 = htmlentities($_POST["leaving_date_11"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_12 = htmlentities($_POST["leaving_name_12"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_12 = htmlentities($_POST["leaving_date_12"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_13 = htmlentities($_POST["leaving_name_13"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_13 = htmlentities($_POST["leaving_date_13"], ENT_QUOTES, 'UTF-8');		
	$leaving_name_14 = htmlentities($_POST["leaving_name_14"], ENT_QUOTES, 'UTF-8');
	$leaving_name_14 = htmlentities($_POST["leaving_name_14"], ENT_QUOTES, 'UTF-8');
	$leaving_name_15 = htmlentities($_POST["leaving_name_15"], ENT_QUOTES, 'UTF-8');
	$leaving_name_15 = htmlentities($_POST["leaving_name_15"], ENT_QUOTES, 'UTF-8');
	$leaving_name_16 = htmlentities($_POST["leaving_name_16"], ENT_QUOTES, 'UTF-8');
	$leaving_name_16 = htmlentities($_POST["leaving_name_16"], ENT_QUOTES, 'UTF-8');
	$leaving_name_17 = htmlentities($_POST["leaving_name_17"], ENT_QUOTES, 'UTF-8');
	$leaving_name_17 = htmlentities($_POST["leaving_name_17"], ENT_QUOTES, 'UTF-8');
	$leaving_name_18 = htmlentities($_POST["leaving_name_18"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_18 = htmlentities($_POST["leaving_name_18"], ENT_QUOTES, 'UTF-8');
	$leaving_name_19 = htmlentities($_POST["leaving_name_19"], ENT_QUOTES, 'UTF-8');
	$leaving_name_19 = htmlentities($_POST["leaving_name_19"], ENT_QUOTES, 'UTF-8');
	$leaving_name_20 = htmlentities($_POST["leaving_name_20"], ENT_QUOTES, 'UTF-8');
	$leaving_name_20 = htmlentities($_POST["leaving_name_20"], ENT_QUOTES, 'UTF-8');
	$leaving_name_21 = htmlentities($_POST["leaving_name_21"], ENT_QUOTES, 'UTF-8');
	$leaving_name_21 = htmlentities($_POST["leaving_name_21"], ENT_QUOTES, 'UTF-8');
	$leaving_name_22 = htmlentities($_POST["leaving_name_22"], ENT_QUOTES, 'UTF-8');
	$leaving_name_22 = htmlentities($_POST["leaving_name_22"], ENT_QUOTES, 'UTF-8');
	$leaving_name_23 = htmlentities($_POST["leaving_name_23"], ENT_QUOTES, 'UTF-8');
	$leaving_name_23 = htmlentities($_POST["leaving_name_23"], ENT_QUOTES, 'UTF-8');
	$leaving_name_24 = htmlentities($_POST["leaving_name_24"], ENT_QUOTES, 'UTF-8');
	$leaving_name_24 = htmlentities($_POST["leaving_name_24"], ENT_QUOTES, 'UTF-8');
	$leaving_name_25 = htmlentities($_POST["leaving_name_25"], ENT_QUOTES, 'UTF-8');
	$leaving_name_25 = htmlentities($_POST["leaving_name_25"], ENT_QUOTES, 'UTF-8');
	$leaving_name_26 = htmlentities($_POST["leaving_name_26"], ENT_QUOTES, 'UTF-8');
	$leaving_name_26 = htmlentities($_POST["leaving_name_26"], ENT_QUOTES, 'UTF-8');
	$leaving_name_27 = htmlentities($_POST["leaving_name_27"], ENT_QUOTES, 'UTF-8');
	$leaving_name_27 = htmlentities($_POST["leaving_name_27"], ENT_QUOTES, 'UTF-8');
	$leaving_name_28 = htmlentities($_POST["leaving_name_28"], ENT_QUOTES, 'UTF-8');
	$leaving_name_28 = htmlentities($_POST["leaving_name_28"], ENT_QUOTES, 'UTF-8');
	$leaving_name_29 = htmlentities($_POST["leaving_name_29"], ENT_QUOTES, 'UTF-8');						
	$leaving_name_29 = htmlentities($_POST["leaving_name_29"], ENT_QUOTES, 'UTF-8');
	$leaving_name_30 = htmlentities($_POST["leaving_name_30"], ENT_QUOTES, 'UTF-8');
	$leaving_name_30 = htmlentities($_POST["leaving_name_30"], ENT_QUOTES, 'UTF-8');			
	
	$sql="INSERT INTO job_calendar (  school, contact, 
					    email, organization, 
	                                  leaving_name_1, leaving_date_1,
	                                  leaving_name_2, leaving_date_2,
	                                  leaving_name_3, leaving_date_3,
	                                  leaving_name_4, leaving_date_4,
	                                  leaving_name_5, leaving_date_5,
	                                  leaving_name_6, leaving_date_6,
	                                  leaving_name_7, leaving_date_7,
	                                  leaving_name_8, leaving_date_8,
	                                  leaving_name_9, leaving_date_9,
	                                  leaving_name_10, leaving_date_10,
					  leaving_name_11, leaving_date_11,
					  leaving_name_12, leaving_date_12,
					  leaving_name_13, leaving_date_13,
					  leaving_name_14, leaving_date_14,
					  leaving_name_15, leaving_date_15,
					  leaving_name_16, leaving_date_16,
					  leaving_name_17, leaving_date_17,
					  leaving_name_18, leaving_date_18,
					  leaving_name_19, leaving_date_19,
					  leaving_name_20, leaving_date_21,
					  leaving_name_22, leaving_date_22,
					  leaving_name_23, leaving_date_23,
					  leaving_name_24, leaving_date_24,
					  leaving_name_25, leaving_date_25,
					  leaving_name_26, leaving_date_26,
					  leaving_name_27, leaving_date_27,
					  leaving_name_28, leaving_date_28,
					  leaving_name_29, leaving_date_29,
					  leaving_name_30, leaving_date_30 
	                                  ) 
	                                  VALUES ( '$school', '$contact', 
						       '$email', '$organization', 
				                                  '$leaving_name_1', '$leaving_date_1',
				                                  '$leaving_name_2', '$leaving_date_2',
				                                  '$leaving_name_3', '$leaving_date_3',
				                                  '$leaving_name_4', '$leaving_date_4',
				                                  '$leaving_name_5', '$leaving_date_5',
				                                  '$leaving_name_6', '$leaving_date_6',
				                                  '$leaving_name_7', '$leaving_date_7',
				                                  '$leaving_name_8', '$leaving_date_8',
				                                  '$leaving_name_9', '$leaving_date_9',
				                                  '$leaving_name_10', '$leaving_date_10',
								  '$leaving_name_11', '$leaving_date_11',
							          '$leaving_name_12', '$leaving_date_12',
								  '$leaving_name_13', '$leaving_date_13',
								  '$leaving_name_14', '$leaving_date_14',
								  '$leaving_name_15', '$leaving_date_15',
								  '$leaving_name_16', '$leaving_date_16',
								  '$leaving_name_17', '$leaving_date_17',
								  '$leaving_name_18', '$leaving_date_18',
								  '$leaving_name_19', '$leaving_date_19',
								  '$leaving_name_20', '$leaving_date_21',
								  '$leaving_name_22', '$leaving_date_22',
								  '$leaving_name_23', '$leaving_date_23',
								  '$leaving_name_24', '$leaving_date_24',
								  '$leaving_name_25', '$leaving_date_25',
								  '$leaving_name_26', '$leaving_date_26',
								  '$leaving_name_27', '$leaving_date_27',
								  '$leaving_name_28', '$leaving_date_28',
								  '$leaving_name_29', '$leaving_date_29',
								  '$leaving_name_30', '$leaving_date_30'  	  
	           			                         )";
	
	if (!($result = $db->sql_query($sql)))
	{
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
	}
	
	$template->assign_block_vars('JOB_CALENDAR_THANK_YOU',array());


}

// handle form response from employers who wish to respond to the email campaign
// enter the job calendar information into the job calendar table, and enter the school
// information into the school jobs table ...
else if( isset($_POST['school']) && isset($_POST['email_campaign'])  ){	
	
	$keys=array();

	// OH the shame of this piece of code ... get thee back to coder school ...

	$school = htmlentities($_POST["school"], ENT_QUOTES, 'UTF-8');	
	$contact = htmlentities($_POST["contact"], ENT_QUOTES, 'UTF-8');	
	$email = htmlentities($_POST["email"], ENT_QUOTES, 'UTF-8');	
	$organization = htmlentities($_POST["orgainization"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_1 = htmlentities($_POST["leaving_name_1"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_1 = htmlentities($_POST["leaving_date_1"], ENT_QUOTES, 'UTF-8');		
	$leaving_name_2 = htmlentities($_POST["leaving_name_2"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_2 = htmlentities($_POST["leaving_date_2"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_3 = htmlentities($_POST["leaving_name_3"], ENT_QUOTES, 'UTF-8');		
	$leaving_date_3 = htmlentities($_POST["leaving_date_3"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_4 = htmlentities($_POST["leaving_name_4"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_4 = htmlentities($_POST["leaving_date_4"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_5 = htmlentities($_POST["leaving_name_5"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_5 = htmlentities($_POST["leaving_date_5"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_6 = htmlentities($_POST["leaving_name_6"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_6 = htmlentities($_POST["leaving_date_6"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_7 = htmlentities($_POST["leaving_name_7"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_7 = htmlentities($_POST["leaving_date_7"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_8 = htmlentities($_POST["leaving_name_8"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_8 = htmlentities($_POST["leaving_date_8"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_9 = htmlentities($_POST["leaving_name_9"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_9 = htmlentities($_POST["leaving_date_9"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_10 = htmlentities($_POST["leaving_name_10"], ENT_QUOTES, 'UTF-8');		
	$leaving_date_10 = htmlentities($_POST["leaving_date_10"], ENT_QUOTES, 'UTF-8');		
	$leaving_name_11 = htmlentities($_POST["leaving_name_11"], ENT_QUOTES, 'UTF-8');		
	$leaving_date_11 = htmlentities($_POST["leaving_date_11"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_12 = htmlentities($_POST["leaving_name_12"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_12 = htmlentities($_POST["leaving_date_12"], ENT_QUOTES, 'UTF-8');	
	$leaving_name_13 = htmlentities($_POST["leaving_name_13"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_13 = htmlentities($_POST["leaving_date_13"], ENT_QUOTES, 'UTF-8');		
	$leaving_name_14 = htmlentities($_POST["leaving_name_14"], ENT_QUOTES, 'UTF-8');
	$leaving_date_14 = htmlentities($_POST["leaving_date_14"], ENT_QUOTES, 'UTF-8');
	$leaving_name_15 = htmlentities($_POST["leaving_name_15"], ENT_QUOTES, 'UTF-8');
	$leaving_date_15 = htmlentities($_POST["leaving_date_15"], ENT_QUOTES, 'UTF-8');
	$leaving_name_16 = htmlentities($_POST["leaving_name_16"], ENT_QUOTES, 'UTF-8');
	$leaving_date_16 = htmlentities($_POST["leaving_date_16"], ENT_QUOTES, 'UTF-8');
	$leaving_name_17 = htmlentities($_POST["leaving_name_17"], ENT_QUOTES, 'UTF-8');
	$leaving_date_17 = htmlentities($_POST["leaving_date_17"], ENT_QUOTES, 'UTF-8');
	$leaving_name_18 = htmlentities($_POST["leaving_name_18"], ENT_QUOTES, 'UTF-8');	
	$leaving_date_18 = htmlentities($_POST["leaving_date_18"], ENT_QUOTES, 'UTF-8');
	$leaving_name_19 = htmlentities($_POST["leaving_name_19"], ENT_QUOTES, 'UTF-8');
	$leaving_date_19 = htmlentities($_POST["leaving_date_19"], ENT_QUOTES, 'UTF-8');
	$leaving_name_20 = htmlentities($_POST["leaving_name_20"], ENT_QUOTES, 'UTF-8');
	$leaving_date_20 = htmlentities($_POST["leaving_date_20"], ENT_QUOTES, 'UTF-8');
	$leaving_name_21 = htmlentities($_POST["leaving_name_21"], ENT_QUOTES, 'UTF-8');
	$leaving_date_21 = htmlentities($_POST["leaving_date_21"], ENT_QUOTES, 'UTF-8');
	$leaving_name_22 = htmlentities($_POST["leaving_name_22"], ENT_QUOTES, 'UTF-8');
	$leaving_date_22 = htmlentities($_POST["leaving_date_22"], ENT_QUOTES, 'UTF-8');
	$leaving_name_23 = htmlentities($_POST["leaving_name_23"], ENT_QUOTES, 'UTF-8');
	$leaving_date_23 = htmlentities($_POST["leaving_date_23"], ENT_QUOTES, 'UTF-8');
	$leaving_name_24 = htmlentities($_POST["leaving_name_24"], ENT_QUOTES, 'UTF-8');
	$leaving_date_24 = htmlentities($_POST["leaving_date_24"], ENT_QUOTES, 'UTF-8');
	$leaving_name_25 = htmlentities($_POST["leaving_name_25"], ENT_QUOTES, 'UTF-8');
	$leaving_date_25 = htmlentities($_POST["leaving_date_25"], ENT_QUOTES, 'UTF-8');
	$leaving_name_26 = htmlentities($_POST["leaving_name_26"], ENT_QUOTES, 'UTF-8');
	$leaving_date_26 = htmlentities($_POST["leaving_date_26"], ENT_QUOTES, 'UTF-8');
	$leaving_name_27 = htmlentities($_POST["leaving_name_27"], ENT_QUOTES, 'UTF-8');
	$leaving_date_27 = htmlentities($_POST["leaving_date_27"], ENT_QUOTES, 'UTF-8');
	$leaving_name_28 = htmlentities($_POST["leaving_name_28"], ENT_QUOTES, 'UTF-8');
	$leaving_date_28 = htmlentities($_POST["leaving_date_28"], ENT_QUOTES, 'UTF-8');
	$leaving_name_29 = htmlentities($_POST["leaving_name_29"], ENT_QUOTES, 'UTF-8');					    $leaving_date_29 = htmlentities($_POST["leaving_date_29"], ENT_QUOTES, 'UTF-8');
	$leaving_name_30 = htmlentities($_POST["leaving_name_30"], ENT_QUOTES, 'UTF-8');
	$leaving_date_30 = htmlentities($_POST["leaving_date_30"], ENT_QUOTES, 'UTF-8');			
	
	$sql="INSERT INTO job_calendar (  school, contact, 
					    email, organization, 
	                                  leaving_name_1, leaving_date_1,
	                                  leaving_name_2, leaving_date_2,
	                                  leaving_name_3, leaving_date_3,
	                                  leaving_name_4, leaving_date_4,
	                                  leaving_name_5, leaving_date_5,
	                                  leaving_name_6, leaving_date_6,
	                                  leaving_name_7, leaving_date_7,
	                                  leaving_name_8, leaving_date_8,
	                                  leaving_name_9, leaving_date_9,
	                                  leaving_name_10, leaving_date_10,
					  leaving_name_11, leaving_date_11,
					  leaving_name_12, leaving_date_12,
					  leaving_name_13, leaving_date_13,
					  leaving_name_14, leaving_date_14,
					  leaving_name_15, leaving_date_15,
					  leaving_name_16, leaving_date_16,
					  leaving_name_17, leaving_date_17,
					  leaving_name_18, leaving_date_18,
					  leaving_name_19, leaving_date_19,
					  leaving_name_20, leaving_date_21,
					  leaving_name_22, leaving_date_22,
					  leaving_name_23, leaving_date_23,
					  leaving_name_24, leaving_date_24,
					  leaving_name_25, leaving_date_25,
					  leaving_name_26, leaving_date_26,
					  leaving_name_27, leaving_date_27,
					  leaving_name_28, leaving_date_28,
					  leaving_name_29, leaving_date_29,
					  leaving_name_30, leaving_date_30 
	                                  ) 
	                                  VALUES ( '$school', '$contact', 
						       '$email', '$organization', 
				                                  '$leaving_name_1', '$leaving_date_1',
				                                  '$leaving_name_2', '$leaving_date_2',
				                                  '$leaving_name_3', '$leaving_date_3',
				                                  '$leaving_name_4', '$leaving_date_4',
				                                  '$leaving_name_5', '$leaving_date_5',
				                                  '$leaving_name_6', '$leaving_date_6',
				                                  '$leaving_name_7', '$leaving_date_7',
				                                  '$leaving_name_8', '$leaving_date_8',
				                                  '$leaving_name_9', '$leaving_date_9',
				                                  '$leaving_name_10', '$leaving_date_10',
								  '$leaving_name_11', '$leaving_date_11',
							          '$leaving_name_12', '$leaving_date_12',
								  '$leaving_name_13', '$leaving_date_13',
								  '$leaving_name_14', '$leaving_date_14',
								  '$leaving_name_15', '$leaving_date_15',
								  '$leaving_name_16', '$leaving_date_16',
								  '$leaving_name_17', '$leaving_date_17',
								  '$leaving_name_18', '$leaving_date_18',
								  '$leaving_name_19', '$leaving_date_19',
								  '$leaving_name_20', '$leaving_date_21',
								  '$leaving_name_22', '$leaving_date_22',
								  '$leaving_name_23', '$leaving_date_23',
								  '$leaving_name_24', '$leaving_date_24',
								  '$leaving_name_25', '$leaving_date_25',
								  '$leaving_name_26', '$leaving_date_26',
								  '$leaving_name_27', '$leaving_date_27',
								  '$leaving_name_28', '$leaving_date_28',
								  '$leaving_name_29', '$leaving_date_29',
								  '$leaving_name_30', '$leaving_date_30'  	  
	           			                         )";
	
	if (!($result = $db->sql_query($sql)))
	{
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
	}		

	$phone = htmlentities($_POST['phone'], ENT_QUOTES, 'UTF-8');
	$school_description = htmlentities($_POST['school_description'], ENT_QUOTES, 'UTF-8');
	$city = htmlentities($_POST['city'], ENT_QUOTES, 'UTF-8');
	$salary = htmlentities($_POST['salary'], ENT_QUOTES, 'UTF-8');
 	
	for( $i=0 ; $i<=30 ; $i++ )
	{
	  
	    $name='leaving_name_'.$i;
	    $date='leaving_date_'.$i;

	    // wow, dara remembers variable variables ...
	    $name_val = ${$name};
	    $date_val = ${$date};

	    // change arrival here because the arrival for job calendar is a date,
	    // but the arrival for school jobs is just a rough time (start_of, end_of) etc..	
	    $arrival=date2approx($date_val);
	  
	    // making a calendar time col in school_jobs to differentiate between immediate jobs that could
	    // be posted through the job forms by the recruiter, and these longer term jobs that are likely 
	    // to be entered via a calendar and won't need to show up before 4 months
	    $date_arr=explode('-',$date_val);

	    $date_year=$date_arr[0];
	    $date_month=$date_arr[1];
	    $date_day=$date_arr[2];

	    $job_time=mktime(0,0,0,$date_month,$date_day,$date_year);
	 
	    // if they did actually send a date and a name to be entered into the db
	    if ( $name_val != '' && $date_val != '' )
	    {
	  
	      $sql = "INSERT into school_jobs (school_jobs_id, contact, school, arrival, email, phone, 
					           school_description, city, salary, calendar_date ) 
               VALUES ( UUID() , '$contact', '$school','$arrival', '$email', '$phone', 
                                      '$school_description', '$city', '$salary', '$job_time' ) ";
	      
	      if ( !($result = $db->sql_query($sql)) )
	      {
		  message_die(CRITICAL_ERROR,'Problem updating the teacher jobs table','',__LINE__,__FILE__,$sql);
		  mail($board_config['board_email'],"SCHOOL JOB INPUT ERROR",$name,"From: SCHOOL_JOB $name <$email>");
	      }	
	      else
	      { 
		  mail($board_config['board_email'],"SCHOOL JOB INPUT",$name,"From: SCHOOL_JOB $name <$email>");

	      }

	    }
	
	}

	// output the calendar thank you..
	$template->assign_block_vars('JOB_CALENDAR_THANK_YOU',array());


}
// outputs form when employer follows the email and wants to register their 
// school for an automatic reminder service ....
else if( ( $_GET['mode']=='email' ) )
{

  $template->assign_block_vars('JOB_CALENDAR_EMAIL_FORM',array());

}
// outputs normal form, not a part of the email campaign
// usually accessed by recruiter from the control panel
else{

	$template->assign_block_vars('JOB_CALENDAR_FORM',array());
	
}

// parse the page here...	
$template->pparse('body');

?>