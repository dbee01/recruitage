<?php 

/**
 * Basic first page
 *
 *                               page_1.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

// standard hack prevent 
define('IN_DECRUIT', true); 

$root_path = './'; 

// let's hard-cache this page ...
header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

include_once($root_path . 'extension.inc'); 
include_once($root_path . 'common.'.$phpEx); 
include_once($root_path . 'includes/functions.'.$phpEx); 

// standard session management 
$userdata = session_pagestart($user_ip, PAGE_PAGE1); 
init_userprefs($userdata);    

// assign template 
$template->set_filenames(array( 
        'body' => 'page_1.tpl') 
); 

// basic page values ...
$template->assign_vars(array(
			     'SITENAME'=>$board_config['sitename']
			     ));

// pre-date the news items ...
$two_days = (date('j')-2) .'-'. date('F') ;
$four_days = (date('j')-4) .'-'. date('F') ;

// find the flag and the number ...
$country = $userdata['country'];

// find which flag the user is coming from
$flag = flag($country);

// valid callback time ...
// set it as valid if the time is between 9AM and 12PM Seoul time
$time = new timezone() ;
$valid = $time->time_valid();

$template->assign_vars( array(
  
        'L_TITLE' => "Travel,<br>Adventure<br>Experience",
	'L_SLOGAN' => "What are you waiting for ?",
    
       	'FORM_LINK' => ('form.'.$phpEx),
        'PAGE_1_LINK' => append_sid('page_1.'.$phpEx),
	'PAGE_2_LINK' => append_sid('page_2.'.$phpEx),
	'PAGE_3_LINK' => append_sid('page_3.'.$phpEx),
	'PAGE_4_LINK' => append_sid('page_4.'.$phpEx),
	'PAGE_5_LINK' => append_sid('page_5.'.$phpEx),
	'CONTACT_LINK' => append_sid('page_6.'.$phpEx),

	'L_CONTENT_HEADER1' => html_entity_decode($page_maker['page1_title']),
	'L_CONTENT_HEADER2' => html_entity_decode($page_maker['page1_text']),
     	'L_TESTAMONIALS_1' => html_entity_decode($page_maker['testamonials1_text']),
	'L_TESTAMONIALS_2' => html_entity_decode($page_maker['testamonials2_text']),

	'L_NEWS_DATE_1' =>  date('j-F'),
	'L_NEWS_CONTENT_1' => html_entity_decode($page_maker['news1_text']),

	'L_NEWS_DATE_2' => $two_days,
	'L_NEWS_CONTENT_2' => html_entity_decode($page_maker['news2_text']),

	'L_NEWS_DATE_3' => $four_days,
	'L_NEWS_CONTENT_3' => html_entity_decode($page_maker['news3_text']),

	'L_PHOTO_GALLERY'=> $lang['Photo_gallery'],
	'L_PHOTO_GALLERY_TEXT'=> $lang['Photo_gallery_text'],
	'L_PHOTO_GALLERY_LINK'=> $lang['Photo_gallery_link'],

	'L_APPLY_ONLINE_LINK'=>$lang['Apply_online_link'],

	'L_LIVE_CHAT'=> $lang['Live_chat'],
	'L_LIVE_CHAT_TEXT'=> $lang['Live_chat_text'],
	'L_LIVE_CHAT_LINK'=> $lang['Live_chat_link'],

	'CALLBACK_VALID'=>$valid,
	'CALLBACK_OPEN_AGAIN'=>$open_in,
	'L_CALLBACK'=> $lang['Callback'],
	'L_CALLBACK_TEXT'=> $lang['Callback_text'],
	'L_CALLBACK_LINK'=> $lang['https://myaccount.voipbuster.com/images/callmenowbutton.gif'],

	'L_TEACHER_GUIDE'=> $lang['Teacher_guide'],
	'L_TEACHER_GUIDE_TEXT'=> $lang['Teacher_guide_text'],
	'L_TEACHER_GUIDE_LINK'=> $lang['Teacher_guide_link'],

	'L_KOREA_MOVIE'=> $lang['Movie_guide'],
	'L_MOVIE_TEXT'=> $lang['Movie_text'],
	'L_MOVIE_LINK'=> $lang['Movie_link'],

	'L_GAMES_DATABASE'=> $lang['Games_database'],
	'L_GAMES_DATABASE_TEXT'=> $lang['Games_database_text'],
	'L_GAMES_DATABASE_LINK'=> $lang['Games_database_link'],

	'L_NEWSLETTER' => $lang['NEWSLETTER'],
	'L_NAME' => $lang['Name'],
	'L_EMAIL' => $lang['Email'],

	'CONTACT_FLAG' => $flag,
	'L_PHONE' => $lang['Phone']

  ));

// parse the page
$template->pparse('body'); 

// close the db object
$db->sql_close();

?>