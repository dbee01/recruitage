<?php

/**
 * This file will store basic config values
 *
 *                                config.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 **/


// they can't call this page specifically
if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}

// db
$dbms = '';

$dbhost = 'localhost';
$dbname = '';
$dbuser = '';
$dbpasswd = '';

// Your User ID is:
// Your Password is:
// You can login from http://www.VirtualPhoneLine.com

// voipbuster
// id:
// pass: 

// log
$db_log_host = 'localhost';
$db_log_user = 'log_user';
$db_log_name = 'log_db';
$db_log_passwd = 'log_user';

$table_prefix = '';

// sms
$click_api_number = '';
$click_api_user = '';
$click_api_pass = '';

// imap details 
$imap_server = "{localhost:144/notls}";
$imap_user = "" ;
$imap_pass = "" ;

//define('PHPBB_INSTALLED', true);

?>
