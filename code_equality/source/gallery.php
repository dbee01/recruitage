<?php

/**
 * Gallery.php is an internet facing page that shows the candidate details this page can be overridden, it will show all the details to the recruiter (USER)  but it will only show a selective bits to others (GUESTS). What can be show and can't be show can be toggled from the admin control_panel
 *
 *                                gallery.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * @description          : 
 *
 **/

// anti hacking...
define('IN_DECRUIT', true);

$root_path = './';

// let's hard-cache all forms ...
header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx);
include_once($root_path . 'includes/xpress_resume.'.$phpEx);

$page= $_GET['id'];
if ( $page == '' ){
 $page='example';
}

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_GALLERY);
init_userprefs($userdata);
//
// End session management
//

// get the id
// if it's not there, then assume that this is a vanity page 
// check the database and send a redirection


// security check here, rinse these values through input_check()
$_GET = array_map("input_check",$_GET);
$_POST = array_map("input_check",$_POST);

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename']
			     ));


// output a navbar
include($root_path . 'includes/navbar.' .$phpEx);

// this handles the sheet to return in the event that the request is from a 
// recruiter response form. The recruiter will look at the resume and give 
// a yes or no answer with regards to the employee and their matched job
if (    isset( $_GET['recruiter_response_form'] ) 
     && isset( $_GET['name'] ) 
     && isset( $_GET['recruiter_jobs_id'] ) 
    )
{
    
    @extract($_GET);
    
    // load up template block
    $template->assign_block_vars('RECRUITER_RESPONSE_FORM',array() );
    
    $sql = "SELECT school FROM recruiter_jobs WHERE recruiter_jobs_id = '$recruiter_jobs_id' " ; 
    
    if (!($result = $db->sql_query($sql)))
	 {
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
	 }

    while($row = $db->sql_fetchrow($result))
      {
	$school_name = $row['school'];	
      }
   
    $template->assign_vars( array( 'TEACHER_RECRUITER_NAME' => $name,
    				    'TEACHER_SCHOOL_NAME' => $school_name, 
				     'TEACHER_RECRUITER_JOBS_ID' => $recruiter_jobs_id ) );

}


// this handles the sheet to return in the event that the request is from a 
// school response form. The school will look at the resume and give 
// a yes or no answer with regards to the employee and their matched job
if (    isset( $_GET['school_response_form'] ) 
     && isset( $_GET['name'] ) 
     && isset( $_GET['school_jobs_id'] ) 
    )
{

    @extract($_GET);
    
    $template->assign_block_vars('SCHOOL_RESPONSE_FORM',array() );
    
    $template->assign_vars( array( 'TEACHER_SCHOOL_NAME' => $name,'TEACHER_SCHOOL_JOBS_ID' => $school_jobs_id ) );
}

// handle the updating of the recruiter notes section here.
// this will just replace the previous recruiter notes with a new one.
if (isset($_POST['recruiter_notes']))
{

	$id = $_POST['teacher_id'];
	$recruiter_notes= $_POST['recruiter_notes'];

	$append = new notes_append();
	$append->append_teachers($id,$recruiter_notes);

 	 redirect(append_sid("/control_panel.$phpEx", true));

}

// Start the page for real here. 
// Give everyone access to the resume form...
if( $userdata['user_level'] == GUEST | $userdata['user_level'] == ADMIN  | $userdata['user_level'] == USER )
{

  // All the change_data and print stuff get handled at the bottom of the page

   $sql = "SELECT teacher_id, youtube, name, birthyear, nationality, advertise, email, phone, from_time, until_time, timezone, location, arrival, gender, inkorea, university_education, experience, introduction, recruiter_notes, pic_upload, start_date, country_code
	FROM teachers WHERE  teacher_id= '$page' ";

	if (!($result = $db->sql_query($sql)))
	{
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
	}

	$row = $db->sql_fetchrow($result);

	$teacher_id=$row['teacher_id'];
	$birthyear=$row['birthyear'] += 1899 ;

	// parse these pices of text
	$arrival=arrival_to_text($row['arrival']) ;
	$location=location($row['location']);
	$nationality = nationality($row['nationality']) ;	  
       
	$time_obj = new timezone();
	$timezone = $time_obj->zone_equiv($row['timezone']);

	$gender=gender($row['gender']) ;	
	$inkorea=inkorea($row['inkorea']) ;
	$advertise=$row['advertise'];
	
	// split start_date into start_day, start_week, start_year
	$start_val=explode('-',$row['start_date']);
	$start_year=$start_val[0];
	$start_month=$start_val[1];
	$start_day=$start_val[2];
	
	// get first name only
	$name=$row['name'];

	// split name
	list($first_name,) = explode(' ', $name);

	// show the link for the print out version of the resume
	$print_view_link= append_sid("gallery.php?mode=print_view_form&id=$teacher_id");

	// if it's a logged in admin, then show the special admin block
	if ( $userdata['session_logged_in'] && ($userdata['user_level'] == 2) )
	{
	  $template->assign_block_vars('ADMIN_LOG_IN',array() );
	}	

        // xpress-resume is a service that matches applicants in a certain area, with 
	// clients in a certain area...
	if (isset($_GET['Xpress_resume']))
	  {
	    $year=date('Y');    
	    $age = $year -  $birthyear ;
	 
      	    $concat_text="Sent $first_name xpress_resume\n";
	    $notes_append->append_teachers($teacher_id, $concat_text);
       	    
	    $xpress = new xpress_resume();
	    $xpress->resume_filter($teacher_id,$name,$age,$nationality,$inkorea,$arrival,$row['location']);

	  }

	// put in the owners resume format choice here...
	$template->assign_block_vars('can0_form',array() );	

	// a link to the hardcopy of the resume
	$hard_copy_link = "
                     <a href='#' onclick=\"window.open('/gallery.php?mode=print_view&id=$page','','scrollbars=yes,menubar=no, resizable=yes,toolbar=no,location=no,status=no');\">Print</a>";

	$user_reg = $userdata['user_regdate'];	

	$template->assign_vars( array( 
				      'TEACHER_HARD_COPY_LINK' => $hard_copy_link,
				      'TEACHER_PIC_UPLOAD' => $row['pic_upload'],
				      'THRID_PARTY_CALL' => $lang['Third_party_call'],
				      'INTERESTED' => $lang['Interested'],
				      'USER_REG_DATE'=>$user_reg
				      ) );
	
	// a link to the teacher resume update part 
	// only can change it if you are a user
	if ( ( $userdata['user_level'] == ADMIN ) || ( $userdata['user_level'] == USER )  )
	  {

	    $resume_update_link = "
                       <a href='#' onclick=\"window.open('/gallery.php?mode=change_data&id=$page','','scrollbars=yes,menubar=no, resizable=yes,toolbar=no,location=no,status=no');\">$first_name</a>
";
	  }
	
	$template->assign_vars( array( 'RESUME_UPDATE_LINK' => $resume_update_link ) );

	// if the person is a user or admin, allow them to set the interests here
    	if ( $userdata['user_level'] >= USER  )
	  {
       
	    // output all possibilites if that person is admin
	    if ( $userdata['user_level'] == ADMIN )
	      {

		// manually set who the candidates are intersted in.
		// the candidates should reply by form, but some aren't clever enough to fill out a form
		$sql_school = "SELECT school, school_jobs_id FROM school_jobs WHERE status != -1 ";
		$sql_recruiter = "SELECT school, recruiter_jobs_id FROM recruiter_jobs WHERE status != -1 ";
	    
	      }
	    else {

		// manually set who the candidates are intersted in.
		// the candidates should reply by form, but some aren't clever enough to fill out a form
		$sql_school = "SELECT school, school_jobs_id FROM school_jobs WHERE status != -1 AND user_reg_id = '$user_reg' ";
		$sql_recruiter = "SELECT school, recruiter_jobs_id FROM recruiter_jobs WHERE status != -1 AND user_reg_id = '$user_reg' ";

	    }

	    if ( !($result_school = $db->sql_query($sql_school))  )
	      {
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql_school);
	      }
	    
	    if ( !($result_recruiter = $db->sql_query($sql_recruiter))  )
	      {
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql_recruiter);
	      }

	    // output the bookcall facility options... bookcall will set up a call in the future..
	    $template->assign_block_vars('can0_form.interest_select',array(
									   'INTEREST_SELECTION'=>$lang['Interest_select'],
									   'L_BOOK_CALL' => $lang['Book_call']
									   )
					 );
	    
	    // for every client, output one option here..
	    while($row_school = $db->sql_fetchrow($result_school))
	      {
		

		$template->assign_block_vars('can0_form.interest_select.school_interest_select',array(
									   'SCHOOL'=>$row_school['school'],
									   'SCHOOL_JOBS_ID'=>$row_school['school_jobs_id']
									   )
					     ); 
		
	      }

	    // for every partner-recruiter, output one here..
	    while($row_recruiter = $db->sql_fetchrow($result_recruiter))
	      {

		$template->assign_block_vars('can0_form.interest_select.recruiter_interest_select',array(
									   'SCHOOL'=>$row_recruiter['school'],
									   'RECRUITER_JOBS_ID'=>$row_recruiter['recruiter_jobs_id']
									   )
					     ); 
	 
	      }
	  }
   
	// here we are going to examine the server logs and how they correspond to that particular candidate
	// where has s that candidate been. Which pages have they visited.
	if ( (  $page_maker["can0_upload_resource"]  ) && 
	     (  $userdata['user_level'] >= ( $page_maker["can0_upload_resource_sec"] )  )  )
	  {

	    // output the various resources hits here, who downloaded the book etc...?
	    $logger = new apache_log_parser();

	    // determine the ip address that the candidate uploaded the form from
	    $can_ip = $logger->candidate_to_ip($teacher_id);
	    
	    // find whether candidate downloaded the guide
	    if ( $logger->ip_to_resource($can_ip,GUIDE) )
	      {
		$template->assign_block_vars('can0_form.resource_counter',array('GUIDE'=>'Viewed Guide')); 
	      }
	    
	    // find whether candidate downloaded tax, savings on page2
	    if ( $logger->ip_to_resource($can_ip,PAGE2) )
	      {
		$template->assign_block_vars('can0_form.resource_counter',array('PAGE2'=>'Viewed Page2')); 
	      }
	    
	    // find whether candidate downloaded the gallery
	    if ( $logger->ip_to_resource($can_ip,GALLERY) )
	      {
		$template->assign_block_vars('can0_form.resource_counter',array('GALLERY'=>'Viewed Gallery')); 
	      }
	    
	    // find whether candidate viewed the games
	    if ( $logger->ip_to_resource($can_ip,GAMES) )
	      {
		$template->assign_block_vars('can0_form.resource_counter',array('GAMES'=>'Viewed Games')); 
	      }


	  }

	// find out whether this user is allowed to see various aspects of the form or not...
	if ( (  $page_maker["can0_upload_youtube"] ) && 
	     (  $userdata['user_level'] >= ( $page_maker["can0_upload_youtube_sec"] )  )  )
	  {

	    $template->assign_block_vars('can0_form.youtube',
		     	    array(
				  'YOUTUBE_UPLOAD' => $row['youtube'],
			          'YOUTUBE_VIDEO' => $lang['Youtube']
				  )
		      		 ); 
	  }

	if ( (  $page_maker["can0_upload_picture"] ) && 
	     (  $userdata['user_level'] >= ( $page_maker["can0_upload_picture_sec"] )  )  )
	  {
	    
	    $template->assign_block_vars('can0_form.candidate_picture',
		     	    array(
				  'TEACHER_PIC_UPLOAD' => $row['pic_upload'],
			          'PICTURE_UPLOAD' => $lang['Picture']
				  )
		      		 ); 
	  }

	if ( (  $page_maker["can0_upload_name"] ) && 
	     (  $userdata['user_level'] >= ( $page_maker["can0_upload_name_sec"] )  )  )
	  {
	    
	    $template->assign_block_vars('can0_form.name',
					 array('L_NAME'=>$lang['Name'],
					       'TEACHER_NAME' => $row['name']
					       )
					 ); 
	  }
 
	if ( ( $page_maker["can0_upload_birthday"]  ) && 
       (  $userdata['user_level'] >= ( $page_maker["can0_upload_birthday_sec"] ) ) )
	  {
	    $template->assign_block_vars('can0_form.birthday',
					 array('L_BIRTHDAY'=>$lang['Birthday'],
					       'TEACHER_BIRTHYEAR'=>$birthyear
					       )
					 ); 
	  }
 
	if ( ( $page_maker["can0_upload_nationality"] = 1 ) 
       &&  (  $userdata['user_level'] >= ( $page_maker["can0_upload_nationality_sec"]=0 )))
	  {

	    $template->assign_block_vars('can0_form.nationality',
		     	    array('L_NATIONALITY'=>$lang['Nationality'],
				  'L_SELECT'=>$lang['Select'],
				  'L_AUSTRALIA'=>$lang['Australia'],
				  'L_CANADA'=>$lang['Canada'],
				  'L_IRELAND'=>$lang['Ireland'],
				  'L_NEW_ZEALAND'=>$lang['New_zealand'],
				  'L_UNITED_STATES'=>$lang['United_states'],
				  'L_UNITED_KINGDOM'=>$lang['United_kingdom'],
				  'L_SOUTH_AFRICA'=>$lang['South_africa'],
				  'TEACHER_NATIONALITY'=>$nationality
				  )
		      		 ); 
	  }

	if ( ( $page_maker["can0_upload_email"] ) 
	     &&  (  $userdata['user_level'] >= ( $page_maker["can0_upload_email_sec"] ) )  )
	  {

	    $email_encode = base64_encode($row['email']);
	    $email_decode = $row['email'];

	    // if user show email is 1 then output proper email address.
	    // otherwise just output the base64 non-visible version.
	    if ( !($userdata['user_show_email']) )
	      {
		$email = $email_encode ;
		
		// always show the decoded email to the print_form_update page
		if ( $_GET['mode'] == 'change_data' )
		  {
		    $email = $email_decode ;
		    $email_visible = "style='display:none;'";
		  }

	      }
	    else 
	      {
		$email = $email_decode ;
	      }

	    $template->assign_block_vars('can0_form.email',
					 array('L_EMAIL'=>$lang['Email'],
					       'TEACHER_EMAIL'=>$email,
					       'TEACHER_EMAIL_VISIBLE'=>$email_visible,
					       'TEACHER_FIRST_NAME'=>$first_name
					       )
					 ); 
	  }

	if ( ( $page_maker["can0_upload_telephone"] ) &&  
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_telephone_sec"] ) ) )
	  {

	    // phone number chop and callback
	    //	    $tel_number = $phone->phone_chopper($row['phone']);

	    // phone object
	    $phone = new phone();

	    // this number must be in good shape
	    $local_number = $phone->phone_chopper($row['phone']);

	    // international number 
	    $intl_number = $row['country_code'].$local_number;

	    // not a number - but a string
	    $callback_number = $phone->callback_link($intl_number);

	    // get the phone mode 
	    $phone_mode = cp_phone_mode($intl_number,$userdata['user_regdate'],'teacher');

	    // sms string ...
	    $sms_number = $phone->sms_link($intl_number);

	    $template->assign_block_vars('can0_form.telephone',
				      array('L_PHONE'=>$lang['Telephone'],
					    'L_OTHER'=>$lang['Other'],					    
					    'L_AUSTRALIA'=>$lang['Australia'],
					    'L_CANADA'=>$lang['Canada'],
					    'L_UNITED_KINGDOM'=>$lang['United_kingdom'],
					    'L_CHINA'=>$lang['China'],
					    'L_INDIA'=>$lang['India'],
					    'L_INDONESIA'=>$lang['Indonesia'],
					    'L_IRELAND'=>$lang['Ireland'],
					    'L_SOUTH_KOREA'=>$lang['South_korea'],
					    'L_MEXICO'=>$lang['Mexico'],
					    'L_PHILLIPINES'=>$lang['Phillipines'],
					    'L_THAILAND'=>$lang['Thailand'],
					    'L_UNITED_STATES'=>$lang['United_states'],
					    'L_NEW_ZEALAND'=>$lang['New_zealand'],
					    'L_SOUTH_AFRICA'=>$lang['South_africa'],
					    'L_SPAIN'=>$lang['Spain'],
					    'L_FROM_TIME'=>$lang['From_time'],
					    'L_BETWEEN'=>$lang['Between'],
					    'L_SELECT'=>$lang['Select'],
					    'L_AND'=>$lang['And'],
					    'L_TIMEZONE'=>$lang['Timezone'],
					    'TEACHER_FROM_TIME'=>$row['from_time'],
					    'TEACHER_UNTIL_TIME'=>$row['until_time'],
					    'TEACHER_TIMEZONE'=>$timezone,
					    'TEACHER_PHONE'=>$row['phone'],
					    'TEACHER_SMS'=>$sms_number,
					    'TEACHER_INTL_PHONE'=>$phone_mode
					    )
				      ); 
	  }

	// on second thoughts, users should always be able to adjust the start date
	//	if ( ( $page_maker["can0_upload_start_date"] ) &&
	//     (  $userdata['user_level'] >= ( $page_maker["can0_upload_start_date_sec"] ) ))
	if ( $userdata['user_level'] >= USER )
	{

	    $template->assign_block_vars('can0_form.start_date',
		     	    array('L_START_DATE'=>$lang['Start_date'],
				  'L_DAY'=>$lang['Day'],
				  'L_MONTH'=>$lang['Month'],
				  'L_YEAR'=>$lang['Year'],
				  'TEACHER_START_DATE'=>str_replace('-','/',$row['start_date'])
				  )
		      		 ); 

	}
	
	if ( ( $page_maker["can0_upload_location"] ) &&
	     (  $userdata['user_level'] >= (  $page_maker["can0_upload_location_sec"] ) ) )
	  {

	    $template->assign_block_vars('can0_form.location', 
				     array(
					   'L_LOCATION'=>$lang['Location'],
					   'LOCATION_BLOCK'=>$location_block,
					   'TEACHER_LOCATION'=>$location
					   ) );
		
	  }
 
	if ( ( $page_maker["can0_upload_arrival"] ) &&
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_arrival_sec"] ) ) )
	 
	  {

	    $template->assign_block_vars('can0_form.arrival',
					 array('L_ARRIVAL'=>$lang['Arrival'],
					       'TEACHER_ARRIVAL'=>$arrival
					       )
					 ); 
	  }
	
	if ( ( $page_maker["can0_upload_gender"] ) &&
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_gender_sec"] ) ) )
	  {

	    $template->assign_block_vars('can0_form.gender',
					 array('L_GENDER'=>$lang['Gender'],
					       'TEACHER_GENDER'=>$gender
					       )
					 ); 
	  }
  
	if ( ( $page_maker["can0_upload_inkorea"] ) &&
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_inkorea_sec"] ) )  )
	  {

	    $template->assign_block_vars('can0_form.inkorea',
					 array('L_INKOREA'=>$lang['Inkorea'],
					       'L_SELECT'=>$lang['Select'],
					       'TEACHER_INKOREA'=>$inkorea
					       )
					 ); 
	  }

	if ( ( $page_maker["can0_upload_referer"] = 1 ) &&
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_referer_sec"]=1 ) )  )
	  {

	    $template->assign_block_vars('can0_form.referer',
					 array('L_REFERER'=>$lang['Referer'],
					       'TEACHER_REFERER'=>$advertise
					       )
					 ); 
	  }
 
	if ( ( $userdata['user_level'] >=  1 ) )
	  {
	    $template->assign_block_vars('can0_form.recruiter_notes',
		     	    array('L_RECRUITER_NOTES'=>$lang['Recruiter_notes'],
				  'TEACHER_RECRUITER_NOTES'=>nl2br($row['recruiter_notes']),
				  'TEACHER_ID'=>$teacher_id
				  )
		      		 ); 
	  }

	if ( ( $page_maker["can0_upload_education"]) &&
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_education_sec"] ) )  )
	  {

	    $education = nl2br($row['university_education']) ;
	    $education = str_replace('<br /><br />','<br />',$education);

	    $template->assign_block_vars('can0_form.education',
		       array('L_EDUCATION'=>$lang['Education'],
	     		    'TEACHER_UNIVERSITY_EDUCATION'=> $education
					    )
					 ); 
	  }

	if ( ( $page_maker["can0_upload_experience"] )  &&
	     (  $userdata['user_level'] >=  ( $page_maker["can0_upload_experience_sec"] ) ) )
	  {


	    $experience = nl2br($row['experience']) ;
	    $experience = str_replace('<br /><br />','<br />',$experience);

	    $template->assign_block_vars('can0_form.experience',
					 array('L_EXPERIENCE'=>$lang['Experience'],
					       'TEACHER_EXPERIENCE'=>$experience
					       )
					 ); 
	  }

	if ( ($page_maker["can0_upload_introduction"]) &&
	     (  $userdata['user_level'] >= ( $page_maker["can0_upload_introduction_sec"])))
	  {

	    $introduction = nl2br($row['introduction']) ;
	    $introduction = str_replace('<br /><br />','<br />',$introduction);

	    $template->assign_block_vars('can0_form.introduction',
					 array('L_INTRODUCTION'=>$lang['Introduction'],
					       'TEACHER_INTRODUCTION'=>$introduction
					       )
					 ); 
	  }

	// this is all contained in can0_form_picker now ...	
 	$template->assign_vars( 
 		array( 
 			'TEACHER_ID' => $teacher_id, 
 			'TEACHER_PRINT_VIEW' => $print_view_link,
 			'TEACHER_FIRST_NAME' => $first_name,
 			'USER_SID' => $sid  
 				)
 			);


	$db->sql_freeresult($result);
}
else
{
	
	header("Location: http://www.englishteachingkorea.com/login.php"); 

}
// note here, all the gallery pages are the same
// I'll only change the tpl sheets that they're written on
// this is to update the data
if ( $_GET['mode'] == 'change_data' ){

  $template->set_filenames(array('body' => 'gallery_print_form.tpl'));

  // This is to turn the gallery into a gallery 
  // update form where the recruiter can 
  // already see the prechosen values ...
  // this isn't the same as the /form_select.php
  include($root_path . '/includes/form_select.'.$phpEx);

}
else if ( $_GET['mode'] == 'print_view_form' ){

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);
  
  // I'm changing this from print_view.tpl to gallery_print_view.tpl
  // print view is a normal print out resume sheet. It doesn't look like
  // I'll be needing it though, so I'm going for the resume value change sheet instead.
  $template->set_filenames(array('body' => 'print_view.tpl'));

}	
else {

	$template->set_filenames(array('body' => 'gallery.tpl'));

}


//
// Generate the page
//

$template->pparse('body');

?>