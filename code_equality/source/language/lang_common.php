<?php

/***************************************************************************
 *                             lang_common.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * @description          : common language file..
 *
 ***************************************************************************/

// Cool Language issues
$lang['Can0_upload_form']='TEACHER RESUME FORM';
$lang['School_description']='School Description';
$lang['School']='School';
$lang['Accomodation']='Accomodation';
$lang['Num_teachers']='Number of Teachers';
$lang['Salary']='Salary';
$lang['Salary_offer']='Salary (1st offer)';
$lang['Thank_you']='THANK YOU';
$lang['Close']='This window should close in 10 seconds ...';
$lang['Resume_error_title']='There has been a error with your resume upload ...';
$lang['Name']='Name';
$lang['Subject']='Subject';
$lang['Email']='Email';
$lang['Text']='Text';
$lang['Email_form']='EMAIL FORM';
$lang['Reminder_form']='REMINDER FORM';
$lang['Candidate_email_form']='CANDIDATE EMAIL FORM';
$lang['Timezone']='TIMEZONE';
$lang['Candidate_email_text']='Friendly efficient and timely ...';
$lang['Attachments']='Attachments';
$lang['School_contact_form']='SCHOOL_CONTACT_FORM';
$lang['School_contact_text']='Take the details of the school and email them if possible...';
$lang['Contact_thanks_text']='Thank you for your email. A representative will contact you as soon as possible. ';
$lang['Contact_us_heading']='Contact us Heading';
$lang['Contact_us_text']='Contact us text';
$lang['School_form_text']='Your job has been successfully added to our database.<br> We will be in touch as soon as we have found a suitable candidate. ';
$lang['Applicant_interest']='Applicant Interest';
$lang['Recruiter_interest']='Recruiter Interest';
$lang['Job_interest']='Job Interest';
$lang['Various_forms_text']='Various Forms Text';
$lang['Teacher_school_posresponse_email']='Email that will be sent to the client, if the applicant is interested in their job.';
$lang['Teacher_recruiter_posresponse_email']='Email that will be sent to one of our partner recruiters, if the applicant is interested in their job.';
$lang['Teacher_posresponse_email']='Email that will be sent to the applicant if the client is interested in them.';
$lang['Etk_teacher_posresponse_email']='Email that will be sent to the applicant, if a client has expressed interest in them.';
$lang['Etk_teacher_school_email']='Email that will be sent to the applicant, if our client indicates that they have been accepted. ';
$lang['Etk_teacher_recruiter_email']='Email that will be sent to the applicant, if our partner recruiter indicates that their client has accepted them. ';
$lang['Candidate_fail_email']='Candidate Fail Response';
$lang['Employer_reminder']='Employer Reminder';
$lang['Resume_upload_text']='Resume Upload Text';
$lang['Resume_error_text']='Resume Error Text';
$lang['Recruiter_form_text']='Recruiter Form Text';
$lang['Recruiter_thanks_text']='Recruiter Thanks Text';
$lang['Recruiter_error_text']='Recruiter Error Text';
$lang['School_thanks_text']='School Thanks Text';
$lang['School_error_text']='School Error Text';
$lang['Action_monitor']='Admin Action Monitor';
$lang['Action_monitor_text']='Action monitor to track employees actions on the interface ...';
$lang['Ad_copy']='Ad copy';
$lang['Book_call']='Book Call';
$lang['Interested']='Interested';
$lang['Recruitment_agreement']='Recruitment Agreement';
$lang['Agree']='Agree';
$lang['Not_agree']='Agree';
$lang['Knowledge_base']='Knowledge Base';
$lang['SMS_interface']='SMS Interface';


// Marketing  campaigns
$lang['Stats']='Stats';
$lang['Campaign']='Campaign';
$lang['Name']='Name';
$lang['Ad_page']='Ad Page';
$lang['Ad_title']='Ad Title';
$lang['Ad_copy']='Ad Copy';
$lang['Script']='Script';
$lang['Cost_per_hit']='Cost per hit';
$lang['Cost_per_resume']='Cost per resume';
$lang['Cost_per_success_match']='Cost per success match';
$lang['Cost_per_hit']='Cost per hit';
$lang['Placed']='Placed';
$lang['Hit_return']='Hit Return';
$lang['Hit_resume']='Hit Resume';
$lang['Success_match']='Success Match';
$lang['Success_failed']='Success Failed';
$lang['Notes']='Notes';
$lang['Frequency']='Frequency';
$lang['Script']='Script';


$lang['Today']='Today';
$lang['Yesterday']='Yesterday';
$lang['Automatic_email_texts']='Automatic Email Texts';
$lang['Automatic_email_response']='Automatic Email Response';
$lang['Automatic_email_response_text']="Pre-formatted email responses that the user can select in order to deal with the most form of email replies. These are time savers, so that the user doesn't have to write the most common type of emails over and over again.";
$lang['Automatic_email_text']="Email responses that are sent automatically in response to certain events that are triggered by the user at the control panel. These responses make it easier to stay in contact with your clients and to let them know what is happening in real-time.";
$lang['Various_forms_text_text']="These are various automatic responses that are shown to clients as they upload and respond to various elements of the application. Such as resume upload or job respond.";
$lang['Website_form_title']='Website Form Content';
$lang['Website_form_text']='Content of the website as it appears to the clients. This will give the text of the front page and other pages, including elements like testamonials and news etc..';
$lang['Aftercare_fixed_title']='Aftercare date pre-planner';
$lang['Aftercare_fixed_text']='Aftercare program: user picks certain dates by calendar, then the system will send emails to anyone registered in the aftercare program on that date. This is useful for regularly keeping in contact with clients, and giving them notice before days such as bank holidays, National holidays, festivals etc...';
$lang['Title']='English Teaching Korea';
$lang['Aftercare_time_title']='Aftercare time pre-planner';
$lang['Aftercare_time_text']='Aftercare program: user picks times relative to important dates, such as employee start date, employee birthday etc... for anyone registered in the aftercare program. The system will then send out emails when those dates arrive.';
$lang['Four_day']='4 Days before';
$lang['Same_day']='Same day';
$lang['One_week']='1 Week after';
$lang['One_month']='1 Month after';
$lang['Three_month']='3 Months after';
$lang['Six_month']='6 Months after';
$lang['Nine_month']='9 Months after';
$lang['Eleven_month']='11 Months after';
$lang['Twelve_month']='1 Year after';
$lang['Phone']='Phone';
$lang['Resource']='Resource';
$lang['Email']='Email';
$lang['Reply_text_title']='Reply Text Title';
$lang['Reply_text']='Reply Text';
$lang['MESSAGE']='MESSAGE';
$lang['Message_title']='Message Title';
$lang['Message_text']='Message Text';
$lang['Heading_text']='Heading Text';
$lang['Birthday_subject']='Birthday Subject';
$lang['Birthday_message']='Birthday Message';
$lang['Heading_title']='Heading Title';
$lang['Page1_title']='Page1 Title';
$lang['Page1_text']='Page1 Text';
$lang['Page2_title']='Page2 Title';
$lang['Page2_text']='Page2 Text';
$lang['Page3_title']='Page3 Title';
$lang['Page3_text']='Page3 Text';
$lang['Page4_title']='Page4 Title';
$lang['Page4_text']='Page4 Text';
$lang['Page5_title']='Page5 Title';
$lang['Page5_text']='Page5 Text';
$lang['Testamonials1_title']='Testamonials1';
$lang['Testamonials2_title']='Testamonials2';
$lang['Jobs_and_News']='Jobs and News';
$lang['News1_title']='News1 Title';
$lang['News1_text']='News1 Text';
$lang['News2_title']='News2 Title';
$lang['News2_text']='News2 Text';
$lang['News3_title']='News3 Title';
$lang['News3_text']='News3 Text';
$lang['NEWSLETTER']='NEWSLETTER';
$lang['Photo_gallery']='Photo Gallery';
$lang['Photo_gallery_text']='Check out our gallery of pictures from Korea !';
$lang['Callback']='Phone Callback';
$lang['Callback_text']="Talk to one of our representatives right now. Just enter your phone number and you'll get an instantaneous call back from us free of charge.";
$lang['Live_chat']='Live Chat';
$lang['Live_chat_text']='Chat one-to-one with our professional right now !';
$lang['Movie_guide']='Movie Intro';
$lang['Movie_text']='Coming soon. An introductory movie...';
$lang['']='';
$lang['Teacher_guide']='Guide';
$lang['Teacher_guide_text']=' Download our 22 page Guide';
$lang['Games_database']='Database';
$lang['Games_database_text']='';

// Job calendar stuff ...
$lang['Job_calendar_title']='Employer Reminder Service';
$lang['Job_calendar_description']="Enter some details and we'll remind you when your teacher's leaving date is approaching.";
$lang['Job_calendar_email_title']='Reminder Service';
$lang['Job_calendar_email_description']="Enter some details and we'll remind you when your employee leaving date is approaching.";
$lang['Job_calendar_school']='School';
$lang['Job_calendar_contact']='Contact';
$lang['Job_calendar_email']='Email';
$lang['Job_calendar_teacher_name']='Teacher Name';
$lang['Job_calendar_teacher_number']='Teacher Number';
$lang['Job_calendar_teacher_leaving']='Leaving Date';
$lang['Job_calendar_school_description']='School Description';
$lang['Job_calendar_school_description_text']="
Things for teacher to do in their free time ? <br />
What are the working hours ? Mon-Fri ? No splits ? <br />
Anything of cultural interest in the local area ?  <br />
Can previous teachers recommend the school ?   <br />
Will school pickup teacher from the airport ?  <br /> ";
$lang['Job_calendar_number_teachers']='Number of Teachers';
$lang['Job_calendar_teacher_number']='Teacher Number';
$lang['Job_calendar_city']='City';
$lang['Job_calendar_phone']='Phone';
$lang['Job_calendar_base_salary']='Base Salary';
$lang['Job_calendar_organization']='Organization';

// the cities ...
$lang['Any']='Any';
$lang['Seoul']='Seoul';
$lang['Daegu']='Daegu';
$lang['Busan']='Busan';
$lang['Ilsan']='Ilsan';
$lang['Ulsan']='Ulsan';
$lang['Incheon']='Incheon';
$lang['Gwangju']='Gwangju';
$lang['Other']='Other';

// organization type
$lang['Hagwon']='Hagwon';
$lang['Public_school']='Public School';
$lang['University']='University';
$lang['Language_institute']='Language_institute';

// form inputs
$lang['Select']='Select';

// salary amounts ...
$lang['1700000']='1.7 million won';
$lang['1800000']='1.8 million won';
$lang['1900000']='1.9 million won';
$lang['2000000']='2.0 million won';
$lang['2100000']='2.1 million won';
$lang['2200000']='2.2 million won';
$lang['2300000']='2.3 million won';
$lang['2400000']='2.4 million won';
$lang['2500000']='2.5 million won';
$lang['2600000']='2.6 million won';
$lang['2700000']='2.7 million won';
$lang['2800000']='2.8 million won';

// LINKS ...
$lang['Photo_gallery_link']="pic_gallery/index.html";
$lang['Live_chat_link']="chat_client.html";
$lang['Teacher_guide_link']="";
$lang['Games_database_link']="";
$lang['Apply_online_link']="/form.php?mode=teacher_resume_form";
$lang['Contact_us_link']="/form.php?mode=contact_us_form";
$lang['Movie_link']="/form.php?mode=movie_mode";

$lang['School_search']='School Search';
$lang['Contact_search']='Contact Search';
$lang['Male']='Male';
$lang['Female']='Female';
$lang['Birthday']='Birthday';
$lang['Nationality']='Nationality';
$lang['Between']='Between';
$lang['Select']='Select';
$lang['Default']='Default';
$lang['Australia']='Australia';
$lang['Canada']='Canada';
$lang['Ireland']='Ireland';
$lang['New_zealand']='New Zealand';
$lang['United_states']='United States';
$lang['United_kingdom']='United Kingdom';
$lang['South_africa']='South Africa';
$lang['China']='China';
$lang['India']='India';
$lang['Indonesia']='Indonesia';
$lang['South_korea']='South Korea';
$lang['Mexico']='Mexico';
$lang['Phillipines']='Phillipines';
$lang['Thailand']='Thailand';
$lang['South_africa']='South Africa';
$lang['Spain']='Spain';

// students
$lang['Kindergarten']='Kindergarten';
$lang['Elementary']='Elementary';
$lang['Middle_school']='Middle School';
$lang['High_school']='High School';
$lang['Adults']='Adults';
$lang['Students']='Students';

$lang['Candidate_resume']='Candidate_resume';
$lang['Website_address']='Website Address';
$lang['Employee_ref']='Employee Reference';
$lang['Picture']='Picture';
$lang['Inkorea']='In Korea';
$lang['Not_inkorea']='Not in Korea';
$lang['Is_inkorea']='Is in Korea';
$lang['Education']='Education';
$lang['Experience']='Experience';
$lang['Introduction']='Introduction';
$lang['Start_date']='Start Date';
$lang['Gender']='Gender';
$lang['Youtube']='Youtube';
$lang['Referer']='Referer';
$lang['Telephone']='Telephone';
$lang['Calltime']='Calltime';
$lang['Recruiter_notes']='Recruiter Notes';
$lang['Location']='Location';
$lang['Arrival']='Arrival';
$lang['Month']='Month';
$lang['Ringtime']='Best time to ring:';
$lang['And']='And';
$lang['Any']='Any';
$lang['Single']='Single';
$lang['Shared']='Shared';
$lang['Contract']='Contract';
$lang['Teacher_description']='Teacher Description';
$lang['Number_teachers']='Number Teachers';
$lang['Foreign_teacher']='Foreign_teachers';
$lang['Picupload']='Picupload';
$lang['City']='City';
$lang['Postal_address']='Postal Address';
$lang['Students']='Students';

// add recruiter
$lang['Last_login']='Last Login';
$lang['Anonymous']='Anonymous';
$lang['Skype/Call_to']='Skype/Call_to';
$lang['Voipbuster/Sip']='Voipbuster/Sip';
$lang['Callback']='Callback';
$lang['All']='All';
$lang['Show_email']='Show Email';
$lang['Show_number']='Show Number';
$lang['No_number']='No Number';

// The months ...
$lang['January']='January';
$lang['February']='February';
$lang['March']='March';
$lang['April']='April';
$lang['May']='May';
$lang['June']='June';
$lang['July']='July';
$lang['August']='August';
$lang['September']='September';
$lang['October']='October';
$lang['November']='November';
$lang['December']='December';

// the timezone text
$lang['Alaska_zone_text']='UTC-9 Anchorage';
$lang['Los_angeles_zone_text']='UTC-8 Los Angeles, Dawson';
$lang['Denver_zone_text']='UTC-7 Denver, Edmonton';
$lang['Dallas_zone_text']='UTC-6 Winnipeg, Dallas, Chicago';
$lang['New_york_zone_text']='UTC-5 New York, Toronto';
$lang['New_foundland_zone_text']='UTC-4 Newfoundland, St. Johns';
$lang['London_zone_text']='UTC London, Dublin';
$lang['Berlin_zone_text']='UTC+1 Berlin, Rome';
$lang['Bangkok_zone_text']='UTC+7 Bangkok';
$lang['Perth_zone_text']='UTC+8 Perth, Beijing';
$lang['Seoul_zone_text']='UTC+9 Seoul, Alice Springs';
$lang['Sydney_zone_text']='UTC+10 Sydney, Guam';
$lang['Auckland_zone_text']='UTC+12 Auckland, Fiji';

// salary figures ...
$lang['Salary_1']='1.7 million won';
$lang['Salary_2']='1.8 million won';
$lang['Salary_3']='1.9 million won';
$lang['Salary_4']='2.0 million won';
$lang['Salary_5']='2.1 million won';
$lang['Salary_6']='2.2 million won';
$lang['Salary_7']='2.3 million won';
$lang['Salary_8']='2.4 million won';
$lang['Salary_9']='2.5 million won';
$lang['Salary_10']='2.6 million won';
$lang['Salary_11']='2.7 million won';
$lang['Salary_12']='2.8 million won';

// Exact arrival times ...
$lang['Start_of_january']='Start of January';
$lang['Middle_of_january']='Middle of January';
$lang['End_of_january']='End of January';
$lang['Start_of_february']='Start of February';
$lang['Middle_of_february']='Middle of February';
$lang['End_of_february']='End of February';
$lang['Start_of_march']='Start of March';
$lang['Middle_of_march']='Middle of March';
$lang['End_of_march']='End of March';
$lang['Start_of_april']='Start of April';
$lang['Middle_of_april']='Middle of April';
$lang['End_of_april']='End of April';
$lang['Start_of_may']='Start of May';
$lang['Middle_of_may']='Middle of May';
$lang['End_of_may']='End of May';
$lang['Start_of_june']='Start of June';
$lang['Middle_of_june']='Middle of June';
$lang['End_of_june']='End of June';
$lang['Start_of_july']='Start of July';
$lang['Middle_of_july']='Middle of July';
$lang['End_of_july']='End of July';
$lang['Start_of_august']='Start of August';
$lang['Middle_of_august']='Middle of August';
$lang['End_of_august']='End of August';
$lang['Start_of_september']='Start of September';
$lang['Middle_of_september']='Middle of September';
$lang['End_of_september']='End of September';
$lang['Start_of_october']='Start of October';
$lang['Middle_of_october']='Middle of October';
$lang['End_of_october']='End of October';
$lang['Start_of_november']='Start of November';
$lang['Middle_of_november']='Middle of November';
$lang['End_of_november']='End of November';
$lang['Start_of_december']='Start of December';
$lang['Middle_of_december']='Middle of December';
$lang['End_of_december']='End of December';


$lang['Job_view']='Job View';
$lang['About']=' about ';
$lang['Sent_details']='Sent details to ';
$lang['Interest_select']='Interest Select';
$lang['TEACHER_RESUME_FORM_TITLE']='TEACHER RESUME FORM';
$lang['TEACHER_RESUME_FORM_TEXT']='Please enter your details below and we\'ll get in contact with you as soon as we\'ve found a job which you might be interested in.';
$lang['Wants_interview']=' wants to arrange an interview with ';
$lang['Not_interested']=' is not interested in ';
$lang['Interested']=' is interested in ';
$lang['Interested_other']=' is interested in another candidate ';
$lang['Timetable']='Timetable';
$lang['Timetable_explanation']='To find the time in different regions...';
$lang['From_zone']='From Zone';
$lang['From_time']='From Time';
$lang['To_zone']='To Zone';
$lang['Reset']='Reset';
$lang['Delete']='Delete';
$lang['Submit']='Submit';
$lang['Select']='Select';
$lang['Other']='Other';
$lang['Day']='Day';
$lang['Year']='Year';


$lang['Yes']='Yes';
$lang['No']='No';
$lang['Telephone']='Telephone';
$lang['Contact']='Contact';
$lang['Organization']='Organization';
$lang['Express_resume']='Express Resume';
$lang['Notes']='Notes';
$lang['Ticket_money']='Ticket Money';
$lang['Applicant_description']='Applicant Description';
$lang['Teacher_description']='Teacher Description';



?>