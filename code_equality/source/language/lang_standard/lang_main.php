<?php
//
// Persistent Page set-up
//
$lang['Title'] = '';
$lang['Slogan'] = '';
$lang['Content_header'] = '';

// language elements that are common for my English representation
include($phpbb_root_path . 'language/lang_common.'.$phpEx);

$lang['News_header_1'] = 'Experience' ;
$lang['News_content_1'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate' ; 

$lang['News_header_2'] = 'Conditions' ;
$lang['News_content_2'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate' ; 

$lang['News_header_3'] = 'What can ESL in Korea do for you ?' ; 
$lang['News_content_3'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate' ;

// 
// Links  
// 
$lang['Home'] = 'Home';
$lang['Jobs'] = 'Jobs';
$lang['Korea'] = 'Location';
$lang['Visa'] = 'Visa';
$lang['Links'] = 'Links';
$lang['Contact'] = 'Contact';

//
// Index page set-up
// 
$lang['Paragraph_1'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede.';
$lang['Paragraph_2'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate';
$lang['Paragraph_3'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate';
$lang['Paragraph_4'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate';

//
// Job title page
// 
$lang['Job_content_title'] = 'Job Title';  
$lang['Job_paragraph_1'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate'; 
$lang['Job_paragraph_2'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate.'; 
$lang['Job_paragraph_3'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate.'; 

//
// Korea title page
// 
$lang['Korea_content_title'] = 'Location Title';  
$lang['Korea_paragraph_1'] = ' Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate'; 
$lang['Korea_paragraph_2'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate.'; 
$lang['Korea_paragraph_3'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate.'; 

//
// Visa title page
// 
$lang['Visa_content_title'] = 'Visa';  
$lang['Visa_paragraph_1'] = ' Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate'; 
$lang['Visa_paragraph_2'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate. Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate.'; 
$lang['Visa_paragraph_3'] = 'Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit a. Lobortis vel. Vel vitae, lacus lacinia penatibus hendrerit duis pede, leo volutpat odio, eget tellus nunc aenean et vulputate'; 

//
// Links title page
//
$lang['Links_content_title'] = 'Links:';

$lang['Links_embassy_usa'] = '<div id="country">United States of America</div>
<div id="category">Korean Embassy</div>

<div id="address">
2320 Massachusetts Avenue N.W. Washington, D.C. 20008  ( 202 ) 939-5661 <font size="1"><a href="http://maps.yahoo.com/maps_result?addr=2320+Massachusetts+Avenue+N.W&amp;csz=20008&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="category">Korean Consulates</div>

<div id="address">
 New York: 460 Park Ave., 5th Fl. New York, NY 10022 (646) 674-6073, (212) 692-9120
 460 Park Ave Serves: Connecticut, Delaware, New Jersey, New York, Pennsylvania, West Virginia
<font size="1"><a href="http://maps.yahoo.com/maps_result?addr=460+Park+Ave&amp;csz=10022&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="address">
San Francisco: 3500 Clay Street San Francisco, CA 84118 (415) 921-2251 <br>
Serves: Colorado, Northern California, Utah, Wyoming
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=3500+Clay+Street&amp;csz=84118&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="address">
Los Angeles: 3243 Wilshire Blvd., Los Angeles, CA 90010 (213) 385-9300 <br>
Serves: Arizona, Nevada, New Mexico, South California
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=3243+Wilshire+Blvd.&amp;csz=90010&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="address">
Boston: One Gateway Center 2nd Fl. Newton, MA 02458 (617) 641-2830 <br>
Serves: New Hampshire, Rhode Island, Maine, Massachusetts, Vermont
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=One+Gateway+Center+Newton&amp;csz=02458&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="address">
Chicago: NBC Tower Suite 2700, 455 North Cityfront Plaza Dr. Chicago, IL 60611 (312) 822-9485 <br>
Serves: Illinois, Indiana, Iowa, Kansas, Kentucky, Michigan, Minnesota, Missouri, Nebraska,
North Dakota, Ohio, South Dakota, Wisconsin
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=455+North+Cityfront+Plaza+Dr.&amp;csz=60611&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="address">
Seattle: 2033 Sixth Ave., #1125 Seattle, WA 98121 (206) 441-1011<br>
Serves: Idaho, Montana, Oregon, Washington
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=2033+Sixth+Ave.&amp;csz=98121&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>

<div id="address">

Atlanta: 229 Peachtree St., Suite 500 International Tower Atlanta, GA 30303 (404) 522-1611 <br>
Serves: Alabama, Florida, Georgia, North Carolina, Puerto Rico, South Carolina, Tennessee, Virgin Islands
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=229+Peachtree+St.&amp;csz=30303&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>
<div id="address">
Houston: 1990 Post Oak Blvd., #1250 Houston, TX 77056 (713) 961-0186 <br>
Serves: Arkansas, Louisiana, Oklahoma, Mississippi, Texas
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=1990+Post+Oak+Blvd.&amp;csz=77056&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>
<div id="address">
Honolulu: 2756 Pali Highway Honolulu, Hawaii 96817 (808) 595-6109 <br>
Serves: American Samoa, Hawaii

<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=2756+Pali+Highway+Honolulu&amp;csz=96817&amp;country=us&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>';

$lang['Links_embassy_canada'] = '<div id="country">Canada</div>
<div id="category">Korean Embassy</div>
<div id="address">
150 Boteler Street Ottawa, Ontario, K1N 5A6 (613)244-5010
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=150+Boteler+Street+Ottawa&amp;csz=k1N5A6&amp;country=can&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>
<div id="category">
Korean Consulates
</div>
<div id="address">

Montreal: 1002 Sherbrooke Street West Suite 2500 Montreal, Quebec H3A 3L6 (514) 845-3243/44
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=1002+Sherbrooke+Street+West+Suite+2500+Montreal&amp;csz=H3A3L6&amp;country=can&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>
<div id="address">
Vancouver: 1600-1090 West Georgia St. Vancouver, BC, Canada, V6E 3V7 (604) 685-9577
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=1600-1090+West+Georgia+St.+Vancouver&amp;csz=V6E3V7&amp;country=can&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>
<div id="address">
Toronto: 555 Avenue Road, Toronto, Ontario, Canada, M4V 2J7 (416)920-3809
<br><font size="1"><a href="http://maps.yahoo.com/maps_result?addr=555+Avenue+Road+Toronto&amp;csz=M4V2J7&amp;country=can&amp;new=1&amp;name=&amp;qty=" target="_blank">Click here for a map to this location</a></font>
</div>';

$lang['Links_embassy_australia'] = '<div id="country">
Australia
</div>
<div id="category">
Korean Embassy
</div>
<div id="address">
 113 Empire Circuit, Yarralumla, ACT 2600, Australia (61-2)6270-4100
</div>
<div id="category">
Korean Consulate

</div>
<div id="address">
United Overseas Bank Building Level 8, 32 Martin Place,
Sydney NSW 2000 Australia (61 2 9221 3866 <br>
(G.P.O.Box 1601 Sydney NSW 2001 Australia) 
</div>';

$lang['Links_embassy_nz'] = '<div id="country">
New Zealand
</div>
<div id="category">
Korean Embassy
</div>
<div id="address">
11th Floor, ASB Bank Tower, 2 Hunter Street, Wellington, New Zealand <br>
( P.O.BOX 11-143, Manners Street, Wellington, New Zealand) (64-4)473-9073/4 
</div>

<div id="category">
Korean Consulate
</div>
<div id="address">
350 Queen Street, Auckalnd, New Zealand <br>
( P. O. BOX 5744 Wllesley Street, Auckland, New Zealand )
(64-9)379-4060
</div>
';

$lang['Links_embassy_uk'] = '<div id="country">
United Kindgom
</div>
<div id="address">
60 Buckingham Gate London SW1E 6AJ United Kingdom
((44-(0)171) 227-5505
<br><font size="1"><a href="http://www.maps.msn.com/%28zqxgqv55prqyrj55wwg5la45%29/map.aspx?L=EUR&amp;C=51.49812%2c-0.13611&amp;A=7.16667&amp;P=%7C51.49812%2c-0.13611%7C1%7C60+Buckingham+Gate%2c+London+SW1E+6%7CL1%7C" target="_blank">Click here for a map to this location</a></font>
</div>';

$lang['Links_embassy_SA'] = '<div id="country">
South Africa
</div>
<div id="address">
Greenpark Estates Building No.3,27
George Storrar Drive Groenkloof, Pretoria Republic of South Africa
(27-12)46-2508 
</div>';


$lang['Links_embassy_fukuoka'] = '';

$lang['Links_embassy_osaka'] = '';

$lang['Links_web_title'] = 'Links on the web :';

$lang['Links_web'] = '<div id="address">
  Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elit.  <br>
  <a href="#">Link</a>
</div>
<div id="address">
   Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elite. <br>
   <a href="#">Link</a>
</div>
<div id="address">

    Lorem ipsum dolor sit amet, tempor in nullam per. Risus posuere vitae, amet vel cursus, justo leo rhoncus elite. <br>
    <a href="#">Link</a>
</div>
	
</div>';

$lang['Links_entertainment'] = '';

//
// Errors (not related to a
// specific failure on a page)
//
$lang['Information'] = 'Information';
$lang['Critical_Information'] = 'Critical Information';

$lang['General_Error'] = 'General Error';
$lang['Critical_Error'] = 'Critical Error';
$lang['An_error_occured'] = 'An Error Occurred';
$lang['A_critical_error'] = 'A Critical Error Occurred';

$lang['Admin_reauthenticate'] = 'To administer the board you must re-authenticate yourself.';


//
// That's all, Folks!
// -------------------------------------------------

?>