<?php

/**
 * The control panel is THE main page of the backend. We are going to hard-cache it and then update any info on it by an AJAX XMLHttpRequest. We will let the user then switch between four views of the control_panel themselves by just a simple js switch. This helps make the system seem more responsive... 
 *
 *                                control_panel.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 **/

// let's buffer the output here ...
ob_start();

// hard cache this guy into the future...
header("Expires: Mon, 26 Jul 2027 05:00:00 GMT"); 

// this is just to measure page runtime, production apps will 
// have this disabled
// BEGIN TIME CODE MEASURE //
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$begintime = $time;
// BEGIN TIME CODE MEASURE //

// anti-hacker mechanism
define('IN_DECRUIT', true);
$phpbb_root_path = './';

// include these two always ...
include_once($phpbb_root_path . 'extension.inc');
include_once($phpbb_root_path . 'common.'.$phpEx);

$page= $_GET['name'];
if ( $page == '' ){ $page='example';}

// we will never use $_GET sid's here, it's too unsafe with inexperienced users
// they might provide a link with their session code attached by accident.
$sid= $_GET['sid'];

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_CONTROL_PANEL);
init_userprefs($userdata);
//
// End session management
//

// set a template .tpl file for this page
$template->set_filenames(array('body' => 'control_panel.tpl'));

// set the default view to intro
$intro_view = 'intro';

// essential data here including the page refresh rate which will be called
// by javascript on the page and will fire off an updating AJAX XMLHttpRequest
$template->assign_vars(array(
			     'USER_REGDATE'=>$userdata['user_regdate'],
			     'USER_TIMEZONE'=>$userdata['user_timezone'],
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename'],
			     'SCREEN_REFRESH_RATE'=>'30000',
			     'INTRO_VIEW'=>$intro_view
			     ));

// take all the values from the teachers table...
$sql_teachers = "SELECT * FROM teachers ";
	
if (!($result_teachers = $db->sql_query($sql_teachers)))
{
  message_die(GENERAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql_teachers);
}		

// Handle all ajax control panel tasks. This ajax include will deal with virtually all the AJAX asyncrhonous requests
// from the control_panel and from other pages...
include($phpbb_root_path . 'includes/ajax_cp.'.$phpEx);

// The control_panel_match will basically handle all the 'predictive intelligence' eg. candidateA responded 'yes' to jobB 
// It will also do some of the heavy lifting for the matching ...
// Handle all the control panel stuff here, cause non-logged in people weren't able to respond to the email response forms.
include($phpbb_root_path . 'includes/control_panel_match.'.$phpEx);

// protect this page from prying eyes. Only logged in ADMIN and USER can get passed here...
if ( $userdata['session_logged_in'] && ($userdata['user_level'] == ADMIN ) || ($userdata['user_level'] == USER )  )
{

  // don't view jobs on control panel before 90 days of that job coming due ...
  $job_view_date=time() + ( 90 * 24 * 60 * 60 ) ;
  
  // Handle the navbar here, I'm tired of changing it all the time
  // The navbar that is outputted will depend on the logged-in status of the user
  include($phpbb_root_path . 'includes/navbar.' .$phpEx);

  // Everyone sees a different view, so find out who we are dealing with
  // $user_reg_date is often used as an informal, non-predictible $user_id
  $user_reg_date = $userdata['user_regdate'] ;

  // if they don't have a user reg_date,
  // then they don't see nothing.
  if ( !(isset($user_reg_date)) )
    {
      echo 'you do not have a user regdate';
    }

  // We're going to output all the info for each section regardless of which view the user has asked for..
  // However some views will be hidden depending on which switch the user has called. The user can then 
  // just switch between views and the javascript will adjust the views for him/her.
  switch ( $_GET['mode'] )
    {

      // basic intro view is called for here ... 
      // www.website_name.com/control_panel.php?mode=cp_intro
    case 'cp_intro' :

      // adjust the little tabs on the page, to make it look selected
      $template->assign_vars(array('INTRO_TAB_SELECTED'=>'background-color:white;border-bottom:0px;'));

      // display the reminders tab, hide the teachers tab for this cp_intro view etc...
      $template->assign_vars(array('REMINDERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('EMAILS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('TEACHERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('SCHOOLS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('RECRUITERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('MATCHES_DISPLAY'=>'display:block;'));	     
      $template->assign_vars(array('LEADS_DISPLAY'=>'display:none;'));

      break;

      // teachers view ... /control_panel.php?mode=cp_teachers
    case 'cp_teachers' : 

      $template->assign_vars(array('TEACHERS_TAB_SELECTED'=>'background-color:white;border-bottom:0px;'));

      $template->assign_vars(array('REMINDERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('EMAILS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('TEACHERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('SCHOOLS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('RECRUITERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('MATCHES_DISPLAY'=>'display:none;'));	     
      $template->assign_vars(array('LEADS_DISPLAY'=>'display:none;'));

      break;
      
      // cp_jobs view ... /control_panel.php?mode=cp_jobs
    case 'cp_jobs' : 
	  
      $template->assign_vars(array('JOBS_TAB_SELECTED'=>'background-color:white;border-bottom:0px;'));

      $template->assign_vars(array('REMINDERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('EMAILS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('TEACHERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('SCHOOLS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('RECRUITERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('MATCHES_DISPLAY'=>'display:none;'));	     
      $template->assign_vars(array('LEADS_DISPLAY'=>'display:none;'));


      break;

      // leads tab ... /control_panel.php?mode=cp_leads
    case 'cp_leads' :

      $template->assign_vars(array('LEADS_TAB_SELECTED'=>'background-color:white;border-bottom:0px;'));

      $template->assign_vars(array('REMINDERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('EMAILS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('TEACHERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('SCHOOLS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('RECRUITERS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('MATCHES_DISPLAY'=>'display:none;'));	     
      $template->assign_vars(array('LEADS_DISPLAY'=>'display:block;'));


      break;

      // show them everything ... /control_panel.php?mode=cp_all
    case 'cp_all' :  
	  
      $template->assign_vars(array('ALL_TAB_SELECTED'=>'background-color:white;border-bottom:0px;'));

      // tweak the display
      $template->assign_vars(array('REMINDERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('EMAILS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('TEACHERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('SCHOOLS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('RECRUITERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('MATCHES_DISPLAY'=>'display:block;'));	     
      $template->assign_vars(array('LEADS_DISPLAY'=>'display:block;'));

      break;

    default:

      // no view selected, so just send them out the intro tab.

      // select intro tab
      $template->assign_vars(array('INTRO_TAB_SELECTED'=>'background-color:white;border-bottom:0px;'));

      // select only intro views
      $template->assign_vars(array('REMINDERS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('EMAILS_DISPLAY'=>'display:block;'));
      $template->assign_vars(array('TEACHERS_DISPLAY'=>'display:none;'));	 
      $template->assign_vars(array('SCHOOLS_DISPLAY'=>'display:none;'));
      $template->assign_vars(array('RECRUITERS_DISPLAY'=>'display:none;'));	 
      $template->assign_vars(array('MATCHES_DISPLAY'=>'display:block;'));	     	     
      $template->assign_vars(array('LEADS_DISPLAY'=>'display:none;'));

      ; break;

    }
      	 
  // we ouput the block title and bar from the page reload because they never change .. etc.
  // REMINDERS ....
  // EMAIL ... 
  // TEACHERS ...
  $reminders_head=$inter->update_reminders_head() ; 
  $template->assign_vars(array('REMINDERS_HEAD'=>$reminders_head));

  $emails_head=$inter->update_emails_head() ; 
  $template->assign_vars(array('EMAILS_HEAD'=>$emails_head));

  $teachers_head=$inter->update_teachers_head() ; 
  $template->assign_vars(array('TEACHERS_HEAD'=>$teachers_head));

  $schools_head=$inter->update_schools_head() ; 
  $template->assign_vars(array('SCHOOLS_HEAD'=>$schools_head));

  $recruiters_head=$inter->update_recruiters_head() ; 
  $template->assign_vars(array('RECRUITERS_HEAD'=>$recruiters_head));

  $leads_head=$inter->update_leads_head() ; 
  $template->assign_vars(array('LEADS_HEAD'=>$leads_head));
      
  $matches_head=$inter->update_matches_head() ; 
  $template->assign_vars(array('MATCHES_HEAD'=>$matches_head));


} 
else 
{

  // this page is hard-cached, so we don't want the viewer to every see a 'not logged in' sign, or it might stay in their cache
  // indefinitely, even if they are in fact logged in. Redirect back to the non-cached login page if there is a problem...
  redirect(append_sid("/login.php", true));

}

// Parse and generate the page...
$template->pparse('body');


// END TIMECODE MEASURE //
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$endtime = $time;
$totaltime = ($endtime - $begintime);
//echo 'PHP parsed this page in ' .$totaltime. ' seconds.'; $time = microtime();
// END TIMECODE MEASURE //

// we buffer and gzip the page, and output it here at the end
ob_end_flush();

?>

