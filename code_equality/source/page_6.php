<?php 

/**
 * Sixth Page 
 *                              page_6.php
 *                            -------------------
 *   begin                : Saturday, Feb 28, 2007
 *   copyright            : (C) 2007 Recruitage.com
 *   email                : daraburke78@gmail.com
 *
 **/

// standard hack prevent 
define('IN_DECRUIT', true); 

$root_path = './'; 

// let's hard-cache all forms ...
header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

include_once($root_path . 'extension.inc'); 
include_once($root_path . 'common.'.$phpEx); 
include_once($root_path . 'includes/xml2array.'.$phpEx); 

// standard session management 
$userdata = session_pagestart($user_ip, PAGE_PAGE6); 
init_userprefs($userdata); 

// assign template 
$template->set_filenames(array( 
        'body' => 'page_6.tpl') 
); 

// find the flag and the number ...
$country = $userdata['country'];

// find the flag the user is coming from
$flag = flag($country);

// basic page values ...
$template->assign_vars(array(
			     'SITENAME'=>$board_config['sitename']
			     ));
   
// do an access check here to make sure they belong 
// clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
access_check(GUEST);


// valid callback time ...
// set it as valid if the time is between 9AM and 12PM Seoul time
$time = new timezone() ;

// is it a valid time for a callback ?
$valid = $time->time_valid();

$template->assign_vars(array(
			     'TEACHER_FORM_TITLE'=>$form_title, 
			     'TEACHER_FORM_HEADER'=>$form_header,
			     'TEACHER_FORM_NAME'=>$lang['Name'],
			     'TEACHER_FORM_EMAIL'=>$lang['Email'],
			     'TEACHER_FORM_SUBJECT'=>$lang['Subject'],
			     'TEACHER_FORM_TXT'=>$lang['Text'],
			     'TEACHER_FORM_HEADING'=>$page_maker['contact_us_heading'],
			     'TEACHER_FORM_TEXT'=>$page_maker['contact_us_text'],
			     'RESET'=>$lang['Reset'],
			     'SUBMIT'=>$lang['Submit'],

			     'FORM_LINK' => append_sid('form.'.$phpEx),
			     'PAGE_1_LINK' => append_sid('page_1.'.$phpEx),
			     'PAGE_2_LINK' => append_sid('page_2.'.$phpEx),
			     'PAGE_3_LINK' => append_sid('page_3.'.$phpEx),
			     'PAGE_4_LINK' => append_sid('page_4.'.$phpEx),
			     'PAGE_5_LINK' => append_sid('page_5.'.$phpEx),
			     'CONTACT_LINK' => append_sid('page_6.'.$phpEx),
			     
			     'L_JOB_CONTENT_TITLE' => $page_maker['page2_title'],
			     'L_JOB_PARAGRAPH' => $page_maker['page2_text'],

			     'CALLBACK_VALID'=>$valid,

			     'CONTACT_FLAG' => $flag,
			     'L_EMAIL' => $lang['Email'],
			     'L_PHONE' => $lang['Phone']
			     ) );


// parse the page...
$template->pparse('body'); 

// close the $db object
$db->sql_close();

?>