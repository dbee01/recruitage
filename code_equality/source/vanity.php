<?php

/**
 * Vanity page allows each candidate to pick a vanity url eg.. www.thesite.com/best_candidate_in_the_world.  it will then be redirected to the real url  www.thesite.com/gallery.php?id=0932jf09324r0uagl    
 *
 *                               vanity.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *                                            
 **/

// anti_hacker thing
define('IN_DECRUIT', true);

$root_path = './';

// include these scripts
include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx);
include_once($root_path . 'includes/xpress_resume.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//

// the vanity page will translate normal strings into back-end urls
$referer = ltrim($_SERVER['REDIRECT_URL'],'/');

$sql = "SELECT backend_url FROM vanity_index WHERE vanity_url = '$referer' " ; 

if (!($result = $db->sql_query($sql)))
{
  message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
}

while($row = $db->sql_fetchrow($result))
{

  $backend_url = $row['backend_url'];	 

  header("Location: $backend_url");

}


?>