<?php

/**
 * Recruiter panel may or not be taken out of the development server it basically just shows all the candidates ...
 * 
 *                           recruiter_panel.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

// anti-hacking device
define('IN_DECRUIT', true);

// root path
$root_path = './';

// include these files
include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx);

$page= $_GET['name'];
if ( $page == '' ){
 $page='example';
}
$sid= $_GET['sid'];

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);//
// End session management
//

$template->set_filenames(array('body' => 'recruiter_panel.tpl'));

// maybe include a navbar...
include_once($root_path . 'includes/navbar.'.$phpEx);

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename']
			     ));

// security choke point here
$_GET = array_map("input_check",$_GET);
$_POST = array_map("input_check",$_POST);

// if this is the search page version, then search through the candidates...
if ( isset($_GET['name']) )
{

  @extract($_GET);

  $sql_teachers = "SELECT * FROM teachers WHERE status != -1";

  // this will allow me search the candidates by country or by arrival
  if ( $country != '' ) 
    {
      $sql_teachers .= " AND nationality = '$country' ";
    }
  
  if ( $arrival != '' )
    {
      $sql_teachers .= " AND arrival = '$arrival' ";
    }

  if ( $text_search != '' )
    {
      $sql_teachers .= " AND ( university_education = '%$text_search%' OR experience = '%text_search%' OR introduction = '%text_search%' ) ";
    }

  $sql_teachers .= " ORDER BY arrival";  

}
else
{
	
  $sql_teachers = "SELECT * FROM teachers WHERE status != -1 ORDER BY arrival";  
	
}

// take out the stock values here from the form_section
$country_template = $form_section->get_country_options()  ;
$prog_months = $form_section->get_progressive_months()  ;

// set the basic form variables
$template->assign_vars(array(
			     'COUNTRY_SELECTION'=>$country_template,
			     'PROGRESSIVE_MONTHS'=>$prog_months
			     )
		       );			

if (!($result_teachers = $db->sql_query($sql_teachers)) )
{
  message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql_teachers);
}	
	
	
// start the switch count for the teacher boxes
$switch_count = 0 ;

while ( $row_teachers = $db->sql_fetchrow($result_teachers)  )
{

  // parse these values
  $arrival=arrival($row_teachers['arrival']);
  $city_teachers= location($row_teachers['location']);
  $nationality=nationality($row_teachers['nationality']) ;	
  
  $birthyear=$row_teachers['birthyear'] += 1899 ;
  $birthyear -= date("Y") ; 
  $birthyear *= -1 ;
		  
  // this casts _times to strings ...		
  $teachers_name = $row_teachers['name'];
  $teachers_name = append_sid("$teachers_name", true, true);
  
  $name=$row_teachers['name'];
  list($first_name,) = explode(' ', $name);

  // count out the different box values so we can differentiate between them
  $switch_count++;

  if($row_teachers['status'])
    {
      
      $template->assign_block_vars('TEACHER_CANDIDATES',
				   array( 'TEACHER_ID' => $row_teachers['teacher_id'],
					  'TEACHER_NAME' => $teachers_name,
					  'SWITCH_COUNT'=>$switch_count,
					  'TEACHER_NAME_ONLY' => $first_name,
					  'TEACHER_NATIONALITY' => $nationality,  
					  'TEACHER_CITY' => $city_teachers,
					  'TEACHER_INTRO' => $row_teachers['introduction'],
					  'TEACHER_BIRTHYEAR' => $birthyear,
					  'TEACHER_ARRIVAL' => $arrival, 	
					  'TEACHER_PIC_UPLOAD' => $row_teachers['pic_upload'],  
					  'USER_SID' => $sid
					  )  	
				   );
    }
} 

$db->sql_freeresult($result_teachers);


//
// Generate the page
//
$template->pparse('body');

?>

