<?php

/**
 * School_cron basically searches teh database and if we should be reminding a client that they are going to be looking for a candidate, then send them  a reminder ...
 *
 *                             school_cron.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

// anti-hacker thing
define('IN_DECRUIT', true); 

$root_path = './'; 

// extension has to come first
include_once($root_path . 'extension.inc'); 

// included these scripts...
require_once($root_path . 'common.'.$phpEx); 
include_once($root_path . 'constants.'.$phpEx); 
include_once($root_path . '/includes/picture.'.$phpEx);
include_once($root_path . '/includes/job_post.php'); 

// this will email any teachers who are in our backup email db
// so that the recruiter doesn't have to remember everything
// and can postdate emails ...
error_reporting(E_ALL);
ini_set('display_errors',1);

// put the oveall board headers here
$headers = 'From: David@EnglishTeachingKorea.com ' . "\r\n" .
'Reply-To: David@EnglishTeachingKorea.com ' . "\r\n" .
'X-Mailer: PHP/' . phpversion();

// take the marketing scripts from the admin
// marketing section and run them one-by-one
if ($_GET['mode']='marketing_campaign')
{

  // instantiate the job_post object
  $job_post = new job_post();

  // usually only the free campaigns are automated
  $sql='SELECT * FROM free_campaigns';

  if(!($result=$db->sql_query($sql)))	
    {
      mail($board_config['board_email'],'PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
    }

  // run script flag
  // should we run the script today ?
  $run_script = 0;

  while ( $row = $db->sql_fetchrow($result) )
    {

      // this has to be here
      @extract($row);

      // we don't want to run these everyday, so stagger them 
      switch($frequency)
	{

	  // daily, run_script is set to run, because it's run everyday.
	case ('1') : 
	  $run_script = 1 ; 
	  $run_tally_add = 1 % 1 ; 
	  ; break;
	  
	  // the frequency denotes that this campaign is to be run every second day
	  // if run_tally = 2, set the run_script flag, set run_tally_add back to 0
	  // else just add one to run_tally_add
	case ('2') : 
	  if( $run_tally == 1 ) { $run_script=1 ; $run_tally_add= (++$run_tally) % 2 ; }
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;
       
	  // third day     		     
	case ('3') : 
	  if($run_tally == 2 ) { $run_script=1; $run_tally_add= (++$run_tally) % 3 ; } 
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;

	  // fourth day
	case ('4') :
	  if($run_tally == 3 ) { $run_script=1; $run_tally_add= (++$run_tally) % 4 ; } 
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;

	  // fifth day
	case ('5') : 
	  if($run_tally == 4 ) { $run_script=1; $run_tally_add= (++$run_tally) % 5 ; } 
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;

	  // sixth day
	case ('6') :
	  if($run_tally == 5 ) { $run_script=1; $run_tally_add= (++$run_tally) % 6 ; } 
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;

	  // weekly day
	case ('7') :
	  if($run_tally == 6 ) { $run_script=1; $run_tally_add= (++$run_tally) % 7 ; } 
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;

	  // monthly
	case ('8') :
	  if($run_tally == 28 ) { $run_script=1 ; $run_tally_add= (++$run_tally) % 29 ; } 
	  else { $run_tally_add = (++$run_tally) ; } 
	  ; break;
	  
	}

      $sql_tally = "UPDATE free_campaigns SET run_tally = $run_tally_add WHERE id='$id'";

            
      if(!($result=$db->sql_query($sql_tally)))	
	{
	  mail('admin@englishteachingkorea.com','PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
	}

      // if run_script is true, then run the script
      // figure out what to do with this. Maybe bring script into apache userland
      // better is you chroot'd the whole thing though ...
      if ( $run_script == true )
	{

	  // this is here to qualify the eval statement, and make sure
	  // that we aren't just executing random code...
	  if ( stristr($script,'job_post->') )
	    {
	      eval($script);
	    }
	  
	}
      
    }

}

// post any messages that need to be sent out today that were postdated
// from the control panel.
if ($_GET['mode']=='daily')
{
  // this is the defacto date standard from now on Y-m-d
  // this date format requires leading zeros and full year
  //	$date= date("Y-m-d");

  // switch zones here 
  putenv(BOARD_TIMEZONE);
  $date=date("Y-m-d");
  putenv(SERVER_TIMEZONE);

  $sql = "SELECT * FROM mail WHERE date='$date' AND sent != 1 ";

  if(!($result=$db->sql_query($sql)))	
    {

      mail($board_config['board_email'],'PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
    }

  while ( $row = $db->sql_fetchrow($result) )
    {

      // determine the recruiter email address...
      // these headers are user specific...
      $user_headers = 'From: '. $row['email_from'] . " \r\n" .
      'Reply-To: '. $row['email_from'] . " \r\n" .
      'X-Mailer: PHP/' . phpversion();

      $email= $row['email'];
      $subject= $row['subject'];
      $text= $row['text'];

      // mails away .... !
      if ( mail($email,$subject,$text,$headers) )
	{

	  // store the email first and add the header
	  $mail = new mail_manager();

	  // store the mail in the mail memory db 
	  $mail->mail_store($text, $email,'');

	}

      // mark each message as being sent. Keep track of messages
      // and have a record, (messages may be missed by server downtime)
      $sql_sent = "UPDATE mail SET sent = 1 WHERE mail_id = ".$row['mail_id'];

      if(!($result_sent=$db->sql_query($sql_sent)))	
	{
	  mail($board_confg['board_email'],'PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
	}

    }	

  // clear these variables just in case
  unset($email_from);
  unset($result);
  unset($email);
  unset($subject);
  unset($text);


  // this will send out emails to all schools that are in our database, exactly 2months 
  // before the date on which their teacher departs. This is a sign up free service the
  // recruiter offers the schools.

  // seconds from epoch of two months from today
  putenv(BOARD_TIMEZONE);

  // get timestamp for today
  $today = time() ;

  // get timestamp two months from now
  $two_months_time = $today + (60 * 60 * 24 * 7 * 8) ;

  // db is  in date format, so get Y-m-d two months from now
  $school_date = strftime("%Y-%m-%d",$two_months_time);

  putenv(SERVER_TIMEZONE);

  // go through the table, select all teacher leaving dates are in exactly two
  // months time. Then send off a reminder email to that employer....
  $school_sql = "SELECT * FROM job_calendar WHERE leaving_date_1='$school_date' 
				OR leaving_date_2='$school_date' OR leaving_date_3='$school_date'
				OR leaving_date_4='$school_date' OR leaving_date_5='$school_date'
				OR leaving_date_6='$school_date' OR leaving_date_7='$school_date'
				OR leaving_date_8='$school_date' OR leaving_date_9='$school_date'
				OR leaving_date_10='$school_date' OR leaving_date_11='$school_date'
				OR leaving_date_12='$school_date' OR leaving_date_13='$school_date'
				OR leaving_date_14='$school_date' OR leaving_date_15='$school_date'
				OR leaving_date_16='$school_date' OR leaving_date_17='$school_date'
				OR leaving_date_18='$school_date' OR leaving_date_19='$school_date'
				OR leaving_date_20='$school_date' OR leaving_date_21='$school_date'
				OR leaving_date_22='$school_date' OR leaving_date_23='$school_date'
				OR leaving_date_24='$school_date' OR leaving_date_25='$school_date'
				OR leaving_date_26='$school_date' OR leaving_date_27='$school_date'
				OR leaving_date_28='$school_date' OR leaving_date_29='$school_date'
				OR leaving_date_30='$school_date' 
				 ";

  if(!($result=$db->sql_query($school_sql)))	
    {

      message_die(CRITICAL_ERROR,'Cannot connect to database for calendar function','',__LINE__,__FILE__,$school_sql);
	
    }
   
  while ( $school_row = $db->sql_fetchrow($result) )
    {
	 
      $school= $school_row['school'];
      $contact= $school_row['contact'];
      $email= $school_row['email'];
		
      $subject="Foreign teacher CONTRACT REMINDER from EnglishTeachingKorea.com";

      // store the email first and add the header
      $mail = new mail_manager();
	    
      if($school_row['leaving_date_1'] == $school_date){

	$text = get_text($school_row['leaving_name_1']);

	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }
      if($school_row['leaving_date_2'] == $school_date){ 
	$text = get_text($school_row['leaving_name_2']);

	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }
      if($school_row['leaving_date_3'] == $school_date){ 
	$text = get_text($school_row['leaving_name_3']);
	
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }
      if($school_row['leaving_date_4'] == $school_date){ 
	$text = get_text($school_row['leaving_name_4']);


	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

       
      }

      if($school_row['leaving_date_5'] == $school_date){ 
	$text = get_text($school_row['leaving_name_5']);
 
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }
      if($school_row['leaving_date_6'] == $school_date){ 
	$text = get_text($school_row['leaving_name_6']);

	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_7'] == $school_date){ 
	$text = get_text($school_row['leaving_name_7']);

	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_8'] == $school_date){ 
	$text = get_text($school_row['leaving_name_8']);

	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_9'] == $school_date){ 
	$text = get_text($school_row['leaving_name_9']);
 
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_10'] == $school_date){ 
	$text = get_text($school_row['leaving_name_10']);
  
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_11'] == $school_date){ 
	$text = get_text($school_row['leaving_name_11']);
   
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_12'] == $school_date){ 
	$text = get_text($school_row['leaving_name_12']);
  
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_13'] == $school_date){ 
	$text = get_text($school_row['leaving_name_13']);
  
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_14'] == $school_date){ 
	$text = get_text($school_row['leaving_name_14']);
 
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_15'] == $school_date){ 
	$text = get_text($school_row['leaving_name_15']);
  
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_16'] == $school_date){ 
	$text = get_text($school_row['leaving_name_16']);
    
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_17'] == $school_date){ 
	$text = get_text($school_row['leaving_name_17']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_18'] == $school_date){ 
	$text = get_text($school_row['leaving_name_18']);
	
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_19'] == $school_date){ 
	$text = get_text($school_row['leaving_name_19']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_20'] == $school_date){ 
	$text = get_text($school_row['leaving_name_20']);
     
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_21'] == $school_date){ 
	$text = get_text($school_row['leaving_name_21']);
	
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_22'] == $school_date){ 
	$text = get_text($school_row['leaving_name_22']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_23'] == $school_date){ 
	$text = get_text($school_row['leaving_name_23']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_24'] == $school_date){ 
	$text = get_text($school_row['leaving_name_24']);
     
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_25'] == $school_date){ 
	$text = get_text($school_row['leaving_name_25']);
       
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_26'] == $school_date){ 
	$text = get_text($school_row['leaving_name_26']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_27'] == $school_date){ 
	$text = get_text($school_row['leaving_name_27']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_28'] == $school_date){ 
	$text = get_text($school_row['leaving_name_28']);
      
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_29'] == $school_date){ 
	$text = get_text($school_row['leaving_name_29']);
    
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
      if($school_row['leaving_date_30'] == $school_date){ 
	$text = get_text($school_row['leaving_name_30']);
     
	if ( mail($email,$subject,$text,$headers,SENDMAIL) )
	  {
	    // store the mail in the mail memory db 
	    $mail->mail_store($text, $email,'');
	  }

      }	
												                        	                    	
    }	

  // Run the aftercare object here, once a day
  // this is the thing that doesn't seem to want to run
  include($root_path . 'includes/aftercare.'.$phpEx);

  // call the aftercare and do the aftercare functions
  $after= new aftercare();
  $sta=$after->start_date_mailer();
  $res=$after->birthday_search();
  $hol=$after->holiday_search();

}

// run this bit every 30mins... reminder mail and sms...
// idea here is that there are two platforms that'll trigger the reminder
// the control_panel reminder and this reminder from the crontab.
if ($_GET['mode']=='half_hour')
{

  // switch zones here 
  putenv(BOARD_TIMEZONE);
  // db is  in date format, so get Y-m-d two months from now
  $now_time = time();
  putenv(SERVER_TIMEZONE);

  // reminder is in unixtime stamp format
  $sql = "SELECT * FROM reminder WHERE rem_time < $now_time AND sent != 1 ";

  if(!($result=$db->sql_query($sql)))	
    {
      mail('admin@englishteachingkorea.com','PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
    }

  while ( $row = $db->sql_fetchrow($result) )
    {

      @extract($row);

      if ( $email_sent == 0 )
	{

	  // mails away .... !
	  mail($email,$subject,$text,$headers,SENDMAIL);

	  $sql_sent = "UPDATE reminder SET email_sent = 1 WHERE remno = $remno";

	  if(!($result_sent=$db->sql_query($sql_sent)))	
	    {
	      mail('admin@englishteachingkorea.com','PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
	    }

	}

      // if the sms isn't sent then send it and mark it as being sent
      if ( $sms_sent == 0 )
	{

	  // sms away .... !
	  require_once ('sms_facility/sms_api.'.$phpEx);
	  
	  // make object 
	  $mysms = new sms();	  
	  //establish session
	  	  echo $mysms->session;
	  // you don't have sms done yet...
	  $mysms->send ($sms,'EnglishTeachingKorea', $text);

	  $sql_sent = "UPDATE reminder SET sms_sent = 1 WHERE remno = $remno";

	  if(!($result_sent=$db->sql_query($sql_sent)))	
	    {
	      mail('admin@englishteachingkorea.com','PHP-CRON ERROR','PHP-Cron cannot access database',$headers);
	    }

	}

    }	

}


// Both the computer and the registered user must be able to 
// access the job reminder text ...
function get_text($value){
	
    global $contact,$page_maker ;

    $text = $page_maker['employer_reminder'];

    // parse out {TEACHER_NAME}, put in $contact etc...
    $text = str_replace('{EMPLOYER_NAME}',$contact,$text);
    $text = str_replace('{TEACHER_NAME}',$value,$text);

    return $text ;

}	

?>