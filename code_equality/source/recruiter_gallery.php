<?php

/***************************************************************************
 * The recruiter gallery is an internet facing job listing of our partner recruiters  what a person will see, will depend on their status, different views for USER,GUEST,ADMIN
 *
 *                            recruiter_gallery.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 ***************************************************************************/

// anti-hacking thing
define('IN_DECRUIT', true);

$root_path = './';

include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx);

$page= $_GET['id'];
if ( $page == '' ){
 $page='example';
}

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_RECRUITER_GALLERY);
init_userprefs($userdata);
//
// End session management

if ( $_GET['mode']=='print_form' &&  ($userdata['user_level'] == ADMIN ) ){
 	
 	$flag=1;
	$template->set_filenames(array('body' => 'recruiter_print_form.tpl'));

}	
else{
	
	$template->set_filenames(array('body' => 'recruiter_gallery.tpl'));

}

// include the navbar here
include($root_path . 'includes/navbar.' .$phpEx);

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename']
			     ));

if (isset($_POST['recruiter_notes']))
{
	
	$id = $_POST["recruiter_jobs_id"];
	$recruiter_notes = htmlentities($_POST["recruiter_notes"], ENT_QUOTES, 'UTF-8');
	
	$append = new notes_append();
	$append->append_recruiters($id,$recruiter_notes);

	 redirect(append_sid("/control_panel.$phpEx", true));

}

if (    isset( $_GET['teacher_response_form'] ) 
     && isset( $_GET['name'] ) 
     && isset( $_GET['teacher_id'] ) 
    )
{
	 
	 $_GET = array_map("input_check",$_GET);
	 @extract($_GET);   
   
    $template->assign_block_vars('TEACHER_RESPONSE_FORM',array() );
    
    $teacher_name = $_GET['name'];
    $teacher_id = $_GET['teacher_id'];
    
    $template->assign_vars( array( 'RECRUITER_TEACHER_NAME' => $teacher_name, 
				   'RECRUITER_TEACHER_ID' => $teacher_id ) );

}

   $sql = "SELECT * FROM recruiter_jobs WHERE recruiter_jobs_id = '$page' ";
			
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
	}	
	
	while ( $row = $db->sql_fetchrow($result) )
	{
		
		$accomodation=$row['accomodation'] ;
		
	    switch ( $accomodation )
		{
			case (1) : $accomodation = 'Single Accomodation'; break ;
		  	case (2) : $accomodation = 'Shared Accomodation'; break ; 
			default :  $accomodation = '' ;   
		}

		( $row['kindergarten'] ? $kindergarten = 'Kindergarten' : $kindergarten = '') ;
		( $row['elementary'] ? $elementary = 'Elementary' : $elementary = '') ;
		( $row['middle_school'] ? $middle_school = 'Middle School' : $middle_school = '') ;
		( $row['high_school'] ? $high_school = 'High School' : $high_school = '') ;
		( $row['adults'] ? $adults = 'Adults' : $adults = '') ;
		
		$city_recruiter=location($row['city']);

		$salary=salary($row['salary']);
		$contract=basename($row['contract']);
		$arrival=arrival_to_text($row['arrival']);
		
		// get first name only
		$name=$row['contact']; 
		list($first_name,) = explode(' ', $name);

		$print_form='recruiter_gallery.php?mode=print_form&id='.$row['recruiter_jobs_id'];
 		
	        include($root_path . '/includes/form_select.'.$phpEx);

		// include the form options that the employer has to fill out
       		$template->assign_block_vars('job1_form',array());

	        if($flag){
	 
	        $school_description=str_replace('<br />','',$row['school_description']);
		}else
		{
			$school_description=$row['school_description'];
		}
		
		if ( $userdata['session_logged_in'] && ($userdata['user_level'] == ADMIN ) )
		{
     		$template->assign_block_vars('ADMIN_LOG_IN',array() );
		}	
				
 	if ( ( $page_maker["job1_upload_contact"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_contact_sec"] ))) 
 	  {
	    $page_maker['job1_upload_contact_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.contact',
				    array(
					  'L_CONTACT'=>$lang['Contact'],
					  'RECRUITER_CONTACT'=>$row['contact'],
					  'CHECK'=>$check
					  )); 
 	  }	


 	if ( ( $page_maker["job1_upload_school"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_school_sec"] ))) 
 	  {

	    $page_maker['job1_upload_school_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.school',
				    array(
					  'L_SCHOOL'=>$lang['School'],
					  'RECRUITER_NAME'=>$row['school'],
					  'CHECK'=>$check
					  )); 
 	  }	

 	if ( ( $page_maker["job1_upload_email"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_email_sec"] ))) 
 	  {

	    $page_maker['job1_upload_email_check'] ? $check = 'check' : $check = 0 ;

	    $email_encode = base64_encode($row['email']);
	    $email_decode = $row['email'];

	    // if user show email is 1 then output proper email address.
	    // otherwise just output the base64 non-visible version.
	    if ( !($userdata['user_show_email']) )
	      {
		$email = $email_encode;

		// always show the decoded email to the print_form_update page
		if ( $print_form_flag )
		  {
		    $email = $email_decode ;
		    $email_visible = "style='display:none;'";
		  }

	      }
	    else 
	      {
		$email = $email_decode;
	      }

 	    $template->assign_block_vars('job1_form.email',
				    array(
					  'L_EMAIL'=>$lang['Email'],
					  'RECRUITER_EMAIL'=>$email,
					  'RECRUITER_EMAIL_VISIBLE'=>$email_visible,
					  'RECRUITER_FIRST_NAME'=>$first_name,
					  'CHECK'=>$check
					  )); 

 	  }	

 	if (  $userdata['user_level'] >= 1 ) 
 	  {

	    $page_maker['job1_upload_recruiter_notes_check']  ? $check = 'check' : $check = 1 ;

 	    $template->assign_block_vars('job1_form.recruiter_notes',
				    array(
					  'L_RECRUITER_NOTES'=>$lang['Recruiter_notes'],
					  'RECRUITER_NOTES'=>nl2br($row['recruiter_notes']),
					  'RECRUITER_JOBS_ID'=>$id,
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_arrival"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_arrival_sec"] ))) 
 	  {

	    $page_maker['job1_upload_arrival_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.arrival',
				    array(
					  'L_ARRIVAL'=>$lang['Arrival'],
					  'ARRIVAL'=>$arrival,
					  'ARRIVAL_LIST'=>$teacher_arrival_list,
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_school_description"] )  && 
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_school_description_sec"] ))) 
 	  {

	    $page_maker['job1_upload_school_description_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.school_description',
				    array(
					  'L_SCHOOL_DESCRIPTION'=>$lang['School_description'],
					  'RECRUITER_DESCRIPTION'=>$school_description,
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_picture"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_picture_sec"] ))) 
 	  {

	    $page_maker['job1_upload_picture_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.picture',
				    array(
					  'L_PICTURE'=>$lang['Picture'],
					  'RECRUITER_PICUPLOAD'=>$row['picupload'],
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_city"] )  &&
	     (  $userdata['user_level'] >= ($page_maker["job1_upload_city_sec"] ))) 
 	  {

	    $page_maker['job1_upload_city_check'] ? $check = 'check' : $check = 0 ;


 	    $template->assign_block_vars('job1_form.location',
				    array(
					  'LOCATION_BLOCK'=>$location_block,
					  'LOCATION'=>$city_recruiter,
					  'L_LOCATION'=>$lang['Location'],
					  'CHECK'=>$check
					  )); 
 	  }	

 	if ( ( $page_maker["job1_upload_website_address"] )  && 
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_website_address_sec"] ))) 
 	  {

	    $page_maker['job1_upload_website_address_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.website_address',
				    array(
					  'L_WEBSITE_ADDRESS'=>$lang['Website_address'],
					  'WEBSITE_ADDRESS'=>$row['website_address'],
					  'CHECK'=>$check
					  )); 
 	  }	

	// what about manual attribution of school's interest in teachers ?	  
	if ( $userdata['user_level'] >= 1  )
	  {
	
	    // manually set who the candidates are intersted in.
	    // the candidates should reply by form, but some aren't clever enough to fill out a form
	    $sql_teacher = "SELECT name, teacher_id FROM teachers WHERE status != -1";
	    
	    if ( !($result_teacher = $db->sql_query($sql_teacher))  )
	      {
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql_teacher);
	      }	    

	    $template->assign_block_vars('job1_form.interest_select',array(
									   'INTEREST_SELECTION'=>$lang['Interest_select'],
									   'L_BOOK_CALL'=>$lang['Book_call']
									   )
					 );
	    
	    while($row_teacher = $db->sql_fetchrow($result_teacher))
	      {
		
		$template->assign_block_vars('job1_form.interest_select.teacher_interest_select',array(
												       'TEACHER_NAME'=>$row_teacher['name'],
												       'TEACHER_ID'=>$row_teacher['teacher_id']
												       )
					     ); 
	
	      }

	  }


 	if ( ( $page_maker["job1_upload_students"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_students_sec"] ))) 
 	  {

	    $page_maker['job1_upload_students_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.students',
				    array(
					  'L_STUDENTS'=>$lang['Students'],
					  'L_KINDERGARTEN'=>$lang['Kindergarten'],
					  'KINDERGARTEN'=>$kindergarten,
					  'L_ELEMENTARY'=>$lang['Elementary'],
					  'ELEMENTARY'=>$elementary,
					  'L_MIDDLE_SCHOOL'=>$lang['Middle_school'],
					  'MIDDLE_SCHOOL'=>$middle_school,
					  'L_HIGH_SCHOOL'=>$lang['High_school'],
					  'HIGH_SCHOOL'=>$high_school,
					  'L_ADULTS'=>$lang['Adults'],
					  'ADULTS'=>$adults,
					  'CHECK'=>$check
					  )); 
 	  }	

 	if ( ( $page_maker["job1_upload_ticket_money"] )  
	     && (  $userdata['user_level'] >= ( $page_maker["job1_upload_ticket_money_sec"] ))) 
 	  {

	    $page_maker['job1_upload_ticket_money_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.ticket_money',
				    array(
					  'L_TICKET_MONEY'=>$lang['Ticket_money'],
					  'CHECK'=>$check
					  )); 
 	  }	

 	if ( ( $page_maker["job1_upload_applicant_description"] )  
	     && (  $userdata['user_level'] >= ( $page_maker["job1_upload_applicant_description_sec"] ))) 
 	  {

	    $page_maker['job1_upload_applicant_description_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.applicant_description',
				    array(
					  'L_APPLICANT_DESCRIPTION'=>$lang['Applicant_description'],
					  'RECRUITER_FOREIGN_TEACHER'=>$row['teacher_description'],
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_accomodation"] )  
	     && (  $userdata['user_level'] >= ( $page_maker["job1_upload_accomodation_sec"] ))) 
 	  {

	    $page_maker['job1_upload_accomodation_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.accomodation',
				    array(
					  'L_ACCOMODATION'=>$lang['Accomodation'],
					  'ACCOMODATION'=>$accomodation,
					  'L_SELECT'=>$lang['Select'],
					  'L_SINGLE'=>$lang['Single'],
					  'L_SHARED'=>$lang['Shared'],
					  'CHECK'=>$check
					  )); 
 	  }	

 	if ( ( $page_maker["job1_upload_teacher_description"] )  
	     && (  $userdata['user_level'] >= ( $page_maker["job1_upload_teacher_description_sec"] ))) 
 	  {

	    $page_maker['job1_upload_teacher_description_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.teacher_description',
				    array(
					  'L_TEACHER_DESCRIPTION'=>$lang['Teacher_description'],
					  'TEACHER_DESCRIPTION'=>$row['teacher_description'],
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_contract"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_contract_sec"] )))
 	  {

	    $page_maker['job1_upload_contract_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.contract',
				    array(
					  'L_CONTRACT'=>$lang['Contract'],
					  'RECRUITER_CONTRACT'=>$row['contract'],
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_salary"] )  &&
	     (  $userdata['user_level'] >= ( $page_maker["job1_upload_salary_sec"] ))) 
 	  {

	    $page_maker['job1_upload_salary_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.salary',
				    array(
					  'L_SALARY'=>$lang['Salary'],
					  'SALARY'=>$salary,
					  'SALARY_BLOCK'=>$salary_list,
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_number_teachers"] )  
	     && (  $userdata['user_level'] >= ( $page_maker["job1_upload_number_teachers_sec"] ))) 
 	  {

	    $page_maker['job1_upload_number_teachers_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.num_teachers',
				    array(
					  'L_NUM_TEACHERS'=>$lang['Num_teachers'],
					  'NUM_TEACHERS' => $row['number_teachers'],
					  'CHECK'=>$check
					  )); 

 	  }	

 	if ( ( $page_maker["job1_upload_gender"]  )  
	     && (  $userdata['user_level'] >= ( $page_maker["job1_upload_gender_sec"] ))) 
 	  {

	    $page_maker['job1_upload_gender_check'] ? $check = 'check' : $check = 0 ;

 	    $template->assign_block_vars('job1_form.gender',
				    array(
					  'L_GENDER'=>$lang['Gender'],
					  'GENDER'=>$row['gender'],
					  'L_ANY'=>$lang['Any'],					 
					  'L_MALE'=>$lang['Male'],
					  'L_FEMALE'=>$lang['Female'],
					  'CHECK'=>$check
					  )); 
 	  }	

   	  $template->assign_vars(
    	  			      	array( 'RECRUITER_CONTACT' => $row['contact'], 
 			       		       'RECRUITER_NAME' => $row['school'],
 					       'LOCATION_BLOCK' => $location_block,
         				       'RECRUITER_PRINT_FORM' => $print_form ,
                                                'RECRUITER_JOBS_ID' => $row['recruiter_jobs_id'],
                                                 ) 
                                               );
   } 

	$db->sql_freeresult($result);

//
// Generate the page
//
$template->pparse('body');

?>