<?php

/***************************************************************************
 * Interface_cron is basically just a script that's called from a cront_tab to complete tasks that have to be done on a regular basis
 *
 *                               interface_cron.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *  
 *
 ***************************************************************************/

define('IN_DECRUIT', true);

$root_path = './';

include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx);

// build the interface cron object
$inter = new interface_cron();

// call the reminder service
$inter->reminder_service();

// call the party service
$inter->third_party_reminder();

// interface_cron class will deal with any cron-to-interface interactions ...

class interface_cron(){

  $now_time = ""; 
 
  // constructor
  function interface_cron()
    {
      $this->$now_time = time();
    }

  // see if any reminder events are triggered, if they are triggered
  // then pass it off to the interface_handler which will show it to the USER on the control_panel
  function reminder_service()
    { 
  
      // update the panel reminders ...
      $sql = "SELECT text,rem_time FROM reminder WHERE rem_time < $now_time AND sent != 1 AND third_party != 1";

      if( !( $result=$db->sql_query($sql) ) )
	{
	  message_die(GENERAL_ERROR,'Cannot reach reminders ... ','',__LINE__,__FILE__,$sql);
	}

      while( $message = $db->sql_fetchrow($result) )
	{ 

	  @extract($message) ;

	  // update the reminder
	  $interface = new interface_handler();
	  $interface->show_reminder( $text , $rem_time );

	}
 
    }
 
  // make the third party reminder calls..
  // the recruiter can book a call between the client and the applicant, and we will place it here
  function third_party_reminder()
    { 
  
      // select all calls who's time has passed and who haven't been called yet
      $sql = "SELECT call_from, call_to,rem_time FROM reminder WHERE rem_time < $now_time AND third_party_sent != 1 AND third_party = 1";

      if( !( $result=$db->sql_query($sql) ) )
	{
	  message_die(GENERAL_ERROR,'Cannot reach reminders ... ','',__LINE__,__FILE__,$sql);
	}

      // hand this off to the caller class to arrange the call now
      while( $message = $db->sql_fetchrow($result) )
	{ 
	  @extract($message);

	  // the voipbuster object is a curlscript that will place the call via voipbuster.com
	  $voip = new voipbuster();

	  // login to voipbuster.com
	  $voip->login();
	  $voip->callback($call_from,$call_to);

	}
      
    }
 
}


?>