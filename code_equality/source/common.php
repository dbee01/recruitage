<?php

/**
 * We'll try to define most classes and deal with common tasks here
 * 
 *                                common.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * @description          : We'll try to define most classes and deal with common tasks here
 *
 **/


// they can't call this common.php specifically
if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}

// set the root path
$root_path = './';

// error reporting setting
error_reporting  (E_ERROR | E_WARNING | E_PARSE); // This will NOT report uninitialized variables
set_magic_quotes_runtime(0); // Disable magic_quotes_runtime

// The following code (unsetting globals)
// Thanks to Matt Kavanagh and Stefan Esser for providing feedback as well as patch files

// PHP5 with register_long_arrays off?
if (@phpversion() >= '5.0.0' && (!@ini_get('register_long_arrays') || @ini_get('register_long_arrays') == '0' || strtolower(@ini_get('register_long_arrays')) == 'off'))
{
	$HTTP_POST_VARS = $_POST;
	$HTTP_GET_VARS = $_GET;
	$HTTP_SERVER_VARS = $_SERVER;
	$HTTP_COOKIE_VARS = $_COOKIE;
	$HTTP_ENV_VARS = $_ENV;
	$HTTP_POST_FILES = $_FILES;

	// _SESSION is the only superglobal which is conditionally set
	if (isset($_SESSION))
	{
		$HTTP_SESSION_VARS = $_SESSION;
	}
}

// Kill all the GLOBALS to prevent hacking
if (isset($HTTP_POST_VARS['GLOBALS']) || isset($HTTP_POST_FILES['GLOBALS']) || isset($HTTP_GET_VARS['GLOBALS']) || isset($HTTP_COOKIE_VARS['GLOBALS']))
{
	die("Hacking attempt");
}

// Kill all the HTTP_SESSION_VARS to prevent hacking
if (isset($HTTP_SESSION_VARS) && !is_array($HTTP_SESSION_VARS))
{
	die("Hacking attempt");
}

if (@ini_get('register_globals') == '1' || strtolower(@ini_get('register_globals')) == 'on')
{
	// PHP4+ path
	$not_unset = array('HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_COOKIE_VARS', 'HTTP_SERVER_VARS', 'HTTP_SESSION_VARS', 'HTTP_ENV_VARS', 'HTTP_POST_FILES', 'phpEx', 'phpbb_root_path');

	// Not only will array_merge give a warning if a parameter
	// is not an array, it will actually fail. So we check if
	// HTTP_SESSION_VARS has been initialised.
	if (!isset($HTTP_SESSION_VARS) || !is_array($HTTP_SESSION_VARS))
	{
		$HTTP_SESSION_VARS = array();
	}

	// Merge all into one extremely huge array; unset
	// this later
	$input = array_merge($HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_COOKIE_VARS, $HTTP_SERVER_VARS, $HTTP_SESSION_VARS, $HTTP_ENV_VARS, $HTTP_POST_FILES);

	unset($input['input']);
	unset($input['not_unset']);

	while (list($var,) = @each($input))
	{
		if (!in_array($var, $not_unset))
		{
			unset($$var);
		}
	}

	unset($input);
}

//
// addslashes to vars if magic_quotes_gpc is off
// this is a security precaution to prevent someone
// trying to break out of a SQL statement.
//
// if( !get_magic_quotes_gpc() )
// {
// 	if( is_array($HTTP_GET_VARS) )
// 	{
// 		while( list($k, $v) = each($HTTP_GET_VARS) )
// 		{
// 			if( is_array($HTTP_GET_VARS[$k]) )
// 			{
// 				while( list($k2, $v2) = each($HTTP_GET_VARS[$k]) )
// 				{
// 					$HTTP_GET_VARS[$k][$k2] = addslashes($v2);
// 				}
// 				@reset($HTTP_GET_VARS[$k]);
// 			}
// 			else
// 			{
// 				$HTTP_GET_VARS[$k] = addslashes($v);
// 			}
// 		}
// 		@reset($HTTP_GET_VARS);
// 	}

// 	if( is_array($HTTP_POST_VARS) )
// 	{
// 		while( list($k, $v) = each($HTTP_POST_VARS) )
// 		{
// 			if( is_array($HTTP_POST_VARS[$k]) )
// 			{
// 				while( list($k2, $v2) = each($HTTP_POST_VARS[$k]) )
// 				{
// 					$HTTP_POST_VARS[$k][$k2] = addslashes($v2);
// 				}
// 				@reset($HTTP_POST_VARS[$k]);
// 			}
// 			else
// 			{
// 				$HTTP_POST_VARS[$k] = addslashes($v);
// 			}
// 		}
// 		@reset($HTTP_POST_VARS);
// 	}

// 	if( is_array($HTTP_COOKIE_VARS) )
// 	{
// 		while( list($k, $v) = each($HTTP_COOKIE_VARS) )
// 		{
// 			if( is_array($HTTP_COOKIE_VARS[$k]) )
// 			{
// 				while( list($k2, $v2) = each($HTTP_COOKIE_VARS[$k]) )
// 				{
// 					$HTTP_COOKIE_VARS[$k][$k2] = addslashes($v2);
// 				}
// 				@reset($HTTP_COOKIE_VARS[$k]);
// 			}
// 			else
// 			{
// 				$HTTP_COOKIE_VARS[$k] = addslashes($v);
// 			}
// 		}
// 		@reset($HTTP_COOKIE_VARS);
// 	}
// }

//
// Define some basic configuration arrays this also prevents
// malicious rewriting of language and otherarray values via
// URI params
//
$board_config = array();
$page_maker = array();
$userdata = array();
$theme = array();
$images = array();
$lang = array();
$nav_links = array();
$gen_simple_header = FALSE;

// include all the files and classes we might use here...
include_once($root_path . 'config.'.$phpEx);
include_once($root_path . 'includes/constants.'.$phpEx);
include_once($root_path . 'includes/template.'.$phpEx);
include_once($root_path . 'includes/sessions.'.$phpEx);
//include_once($root_path . 'includes/auth.'.$phpEx);
include_once($root_path . 'includes/functions.'.$phpEx);
include_once($root_path . 'includes/db.'.$phpEx);
include_once($root_path . 'includes/functions_form.'.$phpEx);
include_once($root_path . 'includes/timezone.'.$phpEx);
include_once($root_path . 'includes/imailer.'.$phpEx);
include_once($root_path . 'includes/notes_append.'.$phpEx);
include_once($root_path . 'includes/action_monitor.'.$phpEx);
include_once($root_path . 'includes/json_snd.'.$phpEx);
include_once($root_path . 'includes/site_tracker.'.$phpEx);
include_once($root_path . 'includes/apache_log_parser.'.$phpEx);
include_once($root_path . 'includes/phone.'.$phpEx);
include_once($root_path . 'includes/form_sections.'.$phpEx);

// this class introduces two newlines which mess up my XMLHttpRequest
include_once($root_path . 'includes/voipbuster.'.$phpEx);

// include for control_panel.php ...
include_once($root_path . 'includes/mail_manager.'.$phpEx);
include_once($root_path . 'includes/interface_handler.'.$phpEx);

// We do not need this any longer, unset for safety purposes
unset($dbpasswd);

//
// Obtain and encode users IP
//
// I'm removing HTTP_X_FORWARDED_FOR ... this may well cause other problems such as
// private range IP's appearing instead of the guilty routable IP, tough, don't
// even bother complaining ... go scream and shout at the idiots out there who feel
// "clever" is doing harm rather than good ... karma is a great thing ... :)
//
$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );

$user_ip = encode_ip($client_ip);

//
// Setup page_maker and fill the page_maker array()
// page_maker here will take user-specific settings 
// geoip-text settings, template settings etc...
//
$sql = "SELECT * FROM " . PAGE_MAKER;

if( !($result = $db->sql_query($sql)) )
{
 	message_die(CRITICAL_ERROR, "Could not query config information", "", __LINE__, __FILE__, $sql);
}

/// use page_maker as the registry value setting...
/// clean this and hope it doesn't mess things up... 
$page_maker = $db->sql_fetchrow($result) ;

// build the geoip zone text here... so that it's different for each user 
$remote_country = geoip_zone();

// get the corresponding country code
$country_code = zone_name_to_code($remote_country);

// country row is ..
$country_row = get_text_by_zone($country_code);

// this is where we merge the standard user-specific page-maker settings with the 
// geoip-based setting based on what country that user comes from ...
// so if user is from England, then take standard page_maker settings from PAGE_MAKER_TABLE
// then merge them with the 'England' text ... to give one amalgamated $page_maker array
if ( $country_row == '' ){

  // default is 0
  $default_row = get_text_by_zone(0);

  // row is emtpy so just use the default text
  $page_maker = array_merge($page_maker,$default_row);

}else{

  // row is not empty, so join the settings...
  $page_maker = array_merge($page_maker,$country_row);

}

// basic board information, such as sitename, email, templates etc...
$sql = "SELECT * FROM " . CONFIG;

if( !($result = $db->sql_query($sql)) )
{
	message_die(CRITICAL_ERROR, "Could not query config information", "", __LINE__, __FILE__, $sql);
}

while ( $row = $db->sql_fetchrow($result) )
{
	$board_config[$row['config_name']] = $row['config_value'];
}

//
// If we've set the board to disable, then just exit here...
//
if( $board_config['board_disable'] && !defined("IN_ADMIN") && !defined("IN_LOGIN") )
{
	message_die(GENERAL_MESSAGE, 'Board_disable', 'Information');
}

// lets just put the errors here to save us time on writing it out all the time
// we can call this function anytime in our script as a debugging breakpoint
function errors()
{
  error_reporting(E_ALL);
  ini_set('display_errors',1);
}

// this object is like a SUPER form class. All forms are built from
// this one object, to ensure that we don't have to build repetitive form options
$form_section = new form_sections();


?>