<?php

/**
 * Form_select basically just handles form data. Mostly from the admin control_panel.
 *
 *                              form_select.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 **/

// standard hack prevent
define('IN_DECRUIT', true);

$root_path = './';

// let's send it compressed 
ob_start("ob_gzhandler");

// BEGIN TIME CODE MEASURE //
//$time = microtime();
//$time = explode(' ', $time);
//$time = $time[1] + $time[0];
//$begintime = $time;
// BEGIN TIME CODE MEASURE //

include_once($root_path . 'extension.inc');
include_once($root_path . 'common.'.$phpEx); 
include_once($root_path . 'includes/picture.'.$phpEx);
include_once($root_path . 'includes/xpress_resume.'.$phpEx);
require_once($root_path . 'includes/apache_log_parser.'.$phpEx);

// standard session management
$userdata = session_pagestart($user_ip, PAGE_FORM_SELECT);
init_userprefs($userdata);

// set the template here..
$template->set_filenames(array('body' => 'main_form.tpl') );

// include the user specific navbar
include_once($root_path . 'includes/navbar.' .$phpEx);

// basic page values ...
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename'],
			     'TEACHER_FORM_TITLE'=>$board_config['sitename'],
			     'TEACHER_FORM_HEADER'=>$board_config['site_desc'],
			     'RESET'=>$lang['Reset'],
			     'SUBMIT'=>$lang['Submit']
			     ));

// Security first ...
$_GET = array_map("input_check",$_GET);
$_POST = array_map("input_check",$_POST);

// header of the recruiter email
$email_header = $userdata['username'] . "... ".$board_config['sitename'];

// output the terms of service agreement
// just you and the recruiter are on the same page as to what's expected of them
if ( $_GET['mode'] == 'recruitment_agreement' )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  // take the name from the GET string
  $recruiter_name = $_GET['rec_nam'];
  
  // date the agreement today
  $recruitment_date = date('Y-m-d');

  $template->assign_block_vars('recruitment_agreement',
			       array(
				     'L_RECRUITMENT_AGREEMENT'=>$lang['Recruitment_agreement'],
				     'COMPANY_NAME'=>$board_config['sitename'],
				     'RECRUITER_NAME'=>$recruiter_name,
				     'RECRUITMENT_DATE'=>$recruitment_date,
				     'L_AGREE'=>$lang['Agree'],
				     'L_NOT_AGREE'=>$lang['Not_agree']
				      ) 
			       );  

}

// output the knowledge base ...
if ( $_GET['mode'] == 'knowledge_base' )
{

  global $db ;

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  // 
  $template->assign_block_vars('knowledge_base',
			       array(
			      	      'KNOWLEDGE_BASE_TITLE'=>'Knowledge_base'
				      ) 
			       );  

  // read in all the knowledge base values...
  $sql = "SELECT knowledge_rec_id, knowledge_section, knowledge_title, knowledge_text 
            FROM knowledge_base 
              ORDER BY knowledge_section";

  $sql2 = "SELECT DISTINCT knowledge_section FROM knowledge_base" ;
    
  if ( !($result = $db->sql_query($sql)) || !($result2 = $db->sql_query($sql2)) )
    {
	message_die(CRITICAL_ERROR,'Cannot pull values out of knowledge db','',__LINE__,__FILE__,$sql);
    }

  // fetch out 'knowledge_section' => 'section' * x
  while ($row = $db->sql_fetchrow($result2))
  {
    // get unique sections 
    $uni_section[] = $row['knowledge_section'];
  }  

  // make it into one big array
  while ($row = $db->sql_fetchrow($result))
    {
      $val_arr[] = $row['knowledge_section'];
      $val_arr[] = $row['knowledge_title'];
      $val_arr[] = $row['knowledge_text'];
      $val_arr[] = $row['knowledge_rec_id'];
    }

  // where value is the name of the section
  foreach ( $uni_section as $value )
    {

       // output the sections for the add_section bit...
       $template->assign_block_vars('knowledge_base.knowledge_add_section',
 				   array('ADD_SECTION'=>$value) 
 				   );  

      // remove any blank spaces
      $nb_value = str_replace(' ','_',$value);

      // array name is the name of a distinct section
      $$nb_value = array();

      // for each unique entry in table, sort it into it's section
      for ( $i=0 ; $i<count($val_arr) ; $i++ )
	{

	  if ( $val_arr[$i] == $value ) 
	    {
	      ${$nb_value}[] = $val_arr[++$i];
	      ${$nb_value}[] = $val_arr[++$i];
	      ${$nb_value}[] = $val_arr[++$i];
	    }
	  else 
	    {
	      $i+=3;
	    }

	}

      // incrment id values
      $id = 0;
      
      for ( $k = 0; $k < count(${$nb_value}) ; $k++)
	{
	  
 	  if ( $id == 0 )
 	    {
 	      $template->assign_block_vars('knowledge_base.knowledge_base_section',
 				       array('SECTION'=>$value) 
 				       );  
	    }

 	  $template->assign_block_vars('knowledge_base.knowledge_base_section.knowledge_base_records',
 				       array(
 					     'TITLE'=>${$nb_value}[$k++], 
 					     'TEXT'=>${$nb_value}[$k++],
 					     'REC_ID_TEXT'=>'rec_id_'.${$nb_value}[$k],
 					     'TEXT_ID'=>$nb_value.'_text_id_'.$id
 					     ) 
 				       ); 

	  // increment text id
	  $id++ ;
	}

    }

}
// output the admin form on request, the admin form allows the admin to select 
// which values appear on forms, whether or not those values are obligatory and 
// which level of user can see which information
else if($_GET['mode'] == 'admin_form_select')  
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  $template->assign_block_vars('admin_form_select',array(
							       'L_TITLE'=>$lang['Title'],
							       'L_CAN0_UPLOAD_FORM'=>$lang['Can0_upload_form'],
							       'L_NAME'=>$lang['Name'],
							       'L_BIRTHDAY'=>$lang['Birthday'],
							       'L_NATIONALITY'=>$lang['Nationality'],
							       'L_EMAIL'=>$lang['Email'],
							       'L_TELEPHONE'=>$lang['Telephone'],
							       'L_RINGTIME'=>$lang['Ringtime'],			
							       'L_LOCATION'=>$lang['Location'],
							       'L_YOUTUBE'=>$lang['Youtube'],		
							       'L_ARRIVAL'=>$lang['Arrival'],		
							       'L_GENDER'=>$lang['Gender'],		       
							       'L_INKOREA'=>$lang['Inkorea'],
							       'L_REFERER'=>$lang['Referer'],		  
							       'L_EDUCATION'=>$lang['Education'],		  
							       'L_EXPERIENCE'=>$lang['Experience'],		  
							       'L_INTRODUCTION'=>$lang['Introduction'],		  
							       'L_PICTURE'=>$lang['Picture'],
							       'L_CONTACT'=>$lang['Contact'],
							       'L_STUDENTS'=>$lang['Students'],
							       'L_SCHOOL'=>$lang['School'],
							       'L_ARRIVAL'=>$lang['Arrival'],
							       'L_EMAIL'=>$lang['Email'],
							       'L_SCHOOL_DESCRIPTION'=>$lang['School_description'],
							       'L_CITY'=>$lang['City'],
							       'L_WEBSITE_ADDRESS'=>$lang['Website_address'],
							       'L_ACCOMODATION'=>$lang['Accomodation'],
							       'L_FOREIGN_TEACHER'=>$lang['Foreign_teacher'],
							       'L_CONTRACT'=>$lang['Contract'],
							       'L_PICUPLOAD'=>$lang['Picupload'],
							       'L_TICKET_MONEY'=>$lang['Ticket_money'],
							       'L_SALARY'=>$lang['Salary'],
							       'L_NUMBER_TEACHERS'=>$lang['Number_teachers'],
							       'L_TEACHER_DESCRIPTION'=>$lang['Teacher_description'],
							       'L_RESOURCE'=>$lang['Resource'],
							       'L_PHONE'=>$lang['Phone'],
							       'L_POSTAL_ADDRESS'=>$lang['Postal_address']
							     ));

  // CAN0 form is the applicant form     
  if ($page_maker['can0_upload_resource'] == 1) 
    {  $template->assign_vars(array("RESOURCE_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("RESOURCE_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_resource_check'] == 1) 
    {  $template->assign_vars(array("RESOURCE_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("RESOURCE_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("RESOURCE_SEC_".$page_maker['can0_upload_resource_sec'] => "selected='selected'" ) ); 

  // Name pre-select 
  if ($page_maker['can0_upload_name'] == 1) 
    {  $template->assign_vars(array("NAME_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("NAME_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_name_check'] == 1) 
    {  $template->assign_vars(array("NAME_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("NAME_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("NAME_SEC_".$page_maker['can0_upload_name_sec'] => "selected='selected'" ) ); 

  // Birthday pre-select 
  if ($page_maker['can0_upload_birthday'] == 1) 
    {  $template->assign_vars(array("BIRTHDAY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("BIRTHDAY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_birthday_check'] == 1) 
    {  $template->assign_vars(array("BIRTHDAY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("BIRTHDAY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("BIRTHDAY_SEC_".$page_maker['can0_upload_birthday_sec'] => "selected='selected'" ) ); 

  // Nationality pre-select 
  if ($page_maker['can0_upload_nationality'] == 1) 
    {  $template->assign_vars(array("NATIONALITY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("NATIONALITY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_ringtime_check'] == 1) 
    {  $template->assign_vars(array("NATIONALITY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("NATIONALITY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("NATIONALITY_SEC_".$page_maker['can0_upload_nationality_sec'] => "selected='selected'" ) ); 
							     
  // Email pre-select 
  if ($page_maker['can0_upload_email'] == 1) 
    {  $template->assign_vars(array("EMAIL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("EMAIL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_email_check'] == 1) 
    {  $template->assign_vars(array("EMAIL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("EMAIL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("EMAIL_SEC_".$page_maker['can0_upload_email_sec'] => "selected='selected'" ) ); 

  // Ringtime pre-select 
  if ($page_maker['can0_upload_telephone'] == 1) 
    {  $template->assign_vars(array("TELEPHONE_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("TELEPHONE_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_telephone_check'] == 1) 
    {  $template->assign_vars(array("TELEPHONE_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("TELEPHONE_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("TELEPHONE_SEC_".$page_maker['can0_upload_telephone_sec'] => "selected='selected'" ) ); 

  // Ringtime pre-select 
  if ($page_maker['can0_upload_ringtime'] == 1) 
    {  $template->assign_vars(array("RINGTIME_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("RINGTIME_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_ringtime_check'] == 1) 
    {  $template->assign_vars(array("RINGTIME_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("RINGTIME_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("RINGTIME_SEC_".$page_maker['can0_upload_ringtime_sec'] => "selected='selected'" ) ); 

  // Location pre-select 
  if ($page_maker['can0_upload_location'] == 1) 
    {  $template->assign_vars(array("LOCATION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("LOCATION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_location_check'] == 1) 
    {  $template->assign_vars(array("LOCATION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("LOCATION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("LOCATION_SEC_".$page_maker['can0_upload_location_sec'] => "selected='selected'" ) ); 


  // Youtube pre-select 
  if ($page_maker['can0_upload_youtube'] == 1) 
    {  $template->assign_vars(array("YOUTUBE_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("YOUTUBE_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_youtube_check'] == 1) 
    {  $template->assign_vars(array("YOUTUBE_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("YOUTUBE_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("YOUTUBE_SEC_".$page_maker['can0_upload_youtube_sec'] => "selected='selected'" ) ); 


  // Arrival pre-select 
  if ($page_maker['can0_upload_arrival'] == 1) 
    {  $template->assign_vars(array("ARRIVAL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("ARRIVAL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_arrival_check'] == 1) 
    {  $template->assign_vars(array("ARRIVAL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("ARRIVAL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("ARRIVAL_SEC_".$page_maker['can0_upload_arrival_sec'] => "selected='selected'" ) ); 


  // Gender pre-select 
  if ($page_maker['can0_upload_gender'] == 1) 
    {  $template->assign_vars(array("GENDER_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("GENDER_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_gender_check'] == 1) 
    {  $template->assign_vars(array("GENDER_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("GENDER_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("GENDER_SEC_".$page_maker['can0_upload_gender_sec'] => "selected='selected'" ) ); 

  // InKorea pre-select 
  if ($page_maker['can0_upload_inkorea'] == 1) 
    {  $template->assign_vars(array("INKOREA_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("INKOREA_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_inkorea_check'] == 1) 
    {  $template->assign_vars(array("INKOREA_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("INKOREA_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("INKOREA_SEC_".$page_maker['can0_upload_inkorea_sec'] => "selected='selected'" ) ); 

  // Referer pre-select 
  if ($page_maker['can0_upload_referer'] == 1) 
    {  $template->assign_vars(array("REFERER_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("REFERER_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_referer_check'] == 1) 
    {  $template->assign_vars(array("REFERER_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("REFERER_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("REFERER_SEC_".$page_maker['can0_upload_referer_sec'] => "selected='selected'" ) ); 

  // Education pre-select 
  if ($page_maker['can0_upload_education'] == 1) 
    {  $template->assign_vars(array("EDUCATION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("EDUCATION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_education_check'] == 1) 
    {  $template->assign_vars(array("EDUCATION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("EDUCATION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("EDUCATION_SEC_".$page_maker['can0_upload_education_sec'] => "selected='selected'" ) ); 

  // Experience pre-select 
  if ($page_maker['can0_upload_experience'] == 1) 
    {  $template->assign_vars(array("EXPERIENCE_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("EXPERIENCE_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_experience_check'] == 1) 
    {  $template->assign_vars(array("EXPERIENCE_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("EXPERIENCE_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("EXPERIENCE_SEC_".$page_maker['can0_upload_experience_sec'] => "selected='selected'" ) ); 


  // Introduction pre-select 
  if ($page_maker['can0_upload_introduction'] == 1) 
    {  $template->assign_vars(array("INTRODUCTION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("INTRODUCTION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_introduction_check'] == 1) 
    {  $template->assign_vars(array("INTRODUCTION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("INTRODUCTION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("INTRODUCTION_SEC_".$page_maker['can0_upload_introduction_sec'] => "selected='selected'" ) ); 
  // Picture pre-select 
  if ($page_maker['can0_upload_picture'] == 1) 
    {  $template->assign_vars(array("PICTURE_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("PICTURE_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['can0_upload_picture_check'] == 1) 
    {  $template->assign_vars(array("PICTURE_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("PICTURE_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("PICTURE_SEC_".$page_maker['can0_upload_picture_sec'] => "selected='selected'" ) ); 

  $template->assign_vars(array('TEACHER_FORM_TITLE'=>$form_title));

  // JOB0 form is the your job selection, job1 is the partner recruiter selection
  // Contact pre-select 
  if ($page_maker['job0_upload_contact'] == 1) 
    {  $template->assign_vars(array("JOB0_CONTACT_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_CONTACT_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_contact_check'] == 1) 
    {  $template->assign_vars(array("JOB0_CONTACT_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_CONTACT_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_CONTACT_SEC_".$page_maker['job0_upload_contact_sec'] => "selected='selected'" ) ); 

  // School pre-select 
  if ($page_maker['job0_upload_school'] == 1) 
    {  $template->assign_vars(array("JOB0_SCHOOL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_SCHOOL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_school_check'] == 1) 
    {  $template->assign_vars(array("JOB0_SCHOOL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_SCHOOL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_SCHOOL_SEC_".$page_maker['job0_upload_school_sec'] => "selected='selected'" ) ); 

  // Arrival pre-select 
  if ($page_maker['job0_upload_arrival'] == 1) 
    {  $template->assign_vars(array("JOB0_ARRIVAL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_ARRIVAL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_arrival_check'] == 1) 
    {  $template->assign_vars(array("JOB0_ARRIVAL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_ARRIVAL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_ARRIVAL_SEC_".$page_maker['job0_upload_arrival_sec'] => "selected='selected'" ) ); 

  // Email pre-select 
  if ($page_maker['job0_upload_email'] == 1) 
    {  $template->assign_vars(array("JOB0_EMAIL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_EMAIL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_email_check'] == 1) 
    {  $template->assign_vars(array("JOB0_EMAIL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_EMAIL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_EMAIL_SEC_".$page_maker['job0_upload_email_sec'] => "selected='selected'" ) ); 


  // Phone pre-select 
  if ($page_maker['job0_upload_phone'] == 1) 
    {  $template->assign_vars(array("JOB0_PHONE_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_PHONE_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_phone_check'] == 1) 
    {  $template->assign_vars(array("JOB0_PHONE_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_PHONE_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_PHONE_SEC_".$page_maker['job0_upload_phone_sec'] => "selected='selected'" ) ); 

  // Foreign Teacher pre-select 
  if ($page_maker['job0_upload_foreign_teacher'] == 1) 
    {  $template->assign_vars(array("JOB0_FOREIGN_TEACHER_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_FOREIGN_TEACHER_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_foreign_teacher_check'] == 1) 
    {  $template->assign_vars(array("JOB0_FOREIGN_TEACHER_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_FOREIGN_TEACHER_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_FOREIGN_TEACHER_SEC_".$page_maker['job0_upload_foreign_teacher_sec'] => "selected='selected'" ) ); 

  // School description pre-select 
  if ($page_maker['job0_upload_school_description'] == 1) 
    {  $template->assign_vars(array("JOB0_SCHOOL_DESCRIPTION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_SCHOOL_DESCRIPTION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_school_description_check'] == 1) 
    {  $template->assign_vars(array("JOB0_SCHOOL_DESCRIPTION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_SCHOOL_DESCRIPTION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_SCHOOL_DESCRIPTION_SEC_".$page_maker['job0_upload_school_description_sec'] => "selected='selected'" ) ); 

  // Postal Address pre-select 
  if ($page_maker['job0_upload_postal_address'] == 1) 
    {  $template->assign_vars(array("JOB0_POSTAL_ADDRESS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_POSTAL_ADDRESS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_postal_address_check'] == 1) 
    {  $template->assign_vars(array("JOB0_POSTAL_ADDRESS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_POSTAL_ADDRESS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_POSTAL_ADDRESS_SEC_".$page_maker['job0_upload_postal_address_sec'] => "selected='selected'" ) ); 

  // City pre-select 
  if ($page_maker['job0_upload_city'] == 1) 
    {  $template->assign_vars(array("JOB0_CITY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_CITY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_city_check'] == 1) 
    {  $template->assign_vars(array("JOB0_CITY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_CITY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_CITY_SEC_".$page_maker['job0_upload_city_sec'] => "selected='selected'" ) ); 

  // Website address pre-select 
  if ($page_maker['job0_upload_website_address'] == 1) 
    {  $template->assign_vars(array("JOB0_WEBSITE_ADDRESS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_WEBSITE_ADDRESS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_website_address_check'] == 1) 
    {  $template->assign_vars(array("JOB0_WEBSITE_ADDRESS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_WEBSITE_ADDRESS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_WEBSITE_ADDRESS_SEC_".$page_maker['job0_upload_website_address_sec'] => "selected='selected'" ) ); 

  // Ticket Money pre-select 
  if ($page_maker['job0_upload_ticket_money'] == 1) 
    {  $template->assign_vars(array("JOB0_TICKET_MONEY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_TICKET_MONEY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_ticket_money_check'] == 1) 
    {  $template->assign_vars(array("JOB0_TICKET_MONEY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_TICKET_MONEY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_TICKET_MONEY_SEC_".$page_maker['job0_upload_ticket_money_sec'] => "selected='selected'" ) ); 

  // Students pre-select 
  if ($page_maker['job0_upload_students'] == 1) 
    {  $template->assign_vars(array("JOB0_STUDENTS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_STUDENTS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_students_check'] == 1) 
    {  $template->assign_vars(array("JOB0_STUDENTS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_STUDENTS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_STUDENTS_SEC_".$page_maker['job0_upload_students_sec'] => "selected='selected'" ) ); 

  // Accomodation pre-select 
  if ($page_maker['job0_upload_accomodation'] == 1) 
    {  $template->assign_vars(array("JOB0_ACCOMODATION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_ACCOMODATION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_accomodation_check'] == 1) 
    {  $template->assign_vars(array("JOB0_ACCOMODATION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_ACCOMODATION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_ACCOMODATION_SEC_".$page_maker['job0_upload_accomodation_sec'] => "selected='selected'" ) ); 


  // Salary pre-select 
  if ($page_maker['job0_upload_salary'] == 1) 
    {  $template->assign_vars(array("JOB0_SALARY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_SALARY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_salary_check'] == 1) 
    {  $template->assign_vars(array("JOB0_SALARY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_SALARY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_SALARY_SEC_".$page_maker['job0_upload_salary_sec'] => "selected='selected'" ) ); 


  // Number teachers pre-select 
  if ($page_maker['job0_upload_number_teachers'] == 1) 
    {  $template->assign_vars(array("JOB0_NUMBER_TEACHERS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_NUMBER_TEACHERS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_number_teachers_check'] == 1) 
    {  $template->assign_vars(array("JOB0_NUMBER_TEACHERS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_NUMBER_TEACHERS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_NUMBER_TEACHERS_SEC_".$page_maker['job0_upload_number_teachers_sec'] => "selected='selected'" ) ); 


  // Teacher Description pre-select 
  if ($page_maker['job0_upload_teacher_description'] == 1) 
    {  $template->assign_vars(array("JOB0_TEACHER_DESCRIPTION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_TEACHER_DESCRIPTION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_teacher_description_check'] == 1) 
    {  $template->assign_vars(array("JOB0_TEACHER_DESCRIPTION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_TEACHER_DESCRIPTION_CHECK_OFF"=> "checked='checked'") ); } 

 $template->assign_vars(array("JOB0_TEACHER_DESCRIPTION_SEC_".$page_maker['job0_upload_teacher_description_sec'] => "selected='selected'" ) ); 


  // Gender pre-select 
  if ($page_maker['job0_upload_gender'] == 1) 
    {  $template->assign_vars(array("JOB0_GENDER_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_GENDER_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_gender_check'] == 1) 
    {  $template->assign_vars(array("JOB0_GENDER_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_GENDER_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_GENDER_SEC_".$page_maker['job0_upload_gender_sec'] => "selected='selected'" ) ); 

  // Contract pre-select 
  if ($page_maker['job0_upload_contract'] == 1) 
    {  $template->assign_vars(array("JOB0_CONTRACT_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_CONTRACT_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_contract_check'] == 1) 
    {  $template->assign_vars(array("JOB0_CONTRACT_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_CONTRACT_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_CONTRACT_SEC_".$page_maker['job0_upload_contract_sec'] => "selected='selected'" ) ); 

  // Picupload pre-select 
  if ($page_maker['job0_upload_picupload'] == 1) 
    {  $template->assign_vars(array("JOB0_PICUPLOAD_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_PICUPLOAD_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job0_upload_picupload_check'] == 1) 
    {  $template->assign_vars(array("JOB0_PICUPLOAD_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB0_PICUPLOAD_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB0_PICUPLOAD_SEC_".$page_maker['job0_upload_picupload_sec'] => "selected='selected'" ) ); 


  $template->assign_vars(array('TEACHER_FORM_TITLE'=>$form_title));
							       
  // JOB1 form is the partner recruiter form
  // Contact pre-select 
  if ($page_maker['job1_upload_contact'] == 1) 
    {  $template->assign_vars(array("JOB1_CONTACT_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_CONTACT_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_contact_check'] == 1) 
    {  $template->assign_vars(array("JOB1_CONTACT_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_CONTACT_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_CONTACT_SEC_".$page_maker['job1_upload_contact_sec'] => "selected='selected'" ) ); 

  // School pre-select 
  if ($page_maker['job1_upload_school'] == 1) 
    {  $template->assign_vars(array("JOB1_SCHOOL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_SCHOOL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_school_check'] == 1) 
    {  $template->assign_vars(array("JOB1_SCHOOL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_SCHOOL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_SCHOOL_SEC_".$page_maker['job1_upload_school_sec'] => "selected='selected'" ) ); 

  // Arrival pre-select 
  if ($page_maker['job1_upload_arrival'] == 1) 
    {  $template->assign_vars(array("JOB1_ARRIVAL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_ARRIVAL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_arrival_check'] == 1) 
    {  $template->assign_vars(array("JOB1_ARRIVAL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_ARRIVAL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_ARRIVAL_SEC_".$page_maker['job1_upload_arrival_sec'] => "selected='selected'" ) ); 

  // Email pre-select 
  if ($page_maker['job1_upload_email'] == 1) 
    {  $template->assign_vars(array("JOB1_EMAIL_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_EMAIL_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_email_check'] == 1) 
    {  $template->assign_vars(array("JOB1_EMAIL_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_EMAIL_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_EMAIL_SEC_".$page_maker['job1_upload_email_sec'] => "selected='selected'" ) ); 


  // Foreign Teacher pre-select 
  if ($page_maker['job1_upload_foreign_teacher'] == 1) 
    {  $template->assign_vars(array("JOB1_FOREIGN_TEACHER_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_FOREIGN_TEACHER_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_foreign_teacher_check'] == 1) 
    {  $template->assign_vars(array("JOB1_FOREIGN_TEACHER_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_FOREIGN_TEACHER_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_FOREIGN_TEACHER_SEC_".$page_maker['job1_upload_foreign_teacher_sec'] => "selected='selected'" ) ); 

  // School description pre-select 
  if ($page_maker['job1_upload_school_description'] == 1) 
    {  $template->assign_vars(array("JOB1_SCHOOL_DESCRIPTION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_SCHOOL_DESCRIPTION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_school_description_check'] == 1) 
    {  $template->assign_vars(array("JOB1_SCHOOL_DESCRIPTION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_SCHOOL_DESCRIPTION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_SCHOOL_DESCRIPTION_SEC_".$page_maker['job1_upload_school_description_sec'] => "selected='selected'" ) ); 

  // City pre-select 
  if ($page_maker['job1_upload_city'] == 1) 
    {  $template->assign_vars(array("JOB1_CITY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_CITY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_city_check'] == 1) 
    {  $template->assign_vars(array("JOB1_CITY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_CITY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_CITY_SEC_".$page_maker['job1_upload_city_sec'] => "selected='selected'" ) ); 

  // Website address pre-select 
  if ($page_maker['job1_upload_website_address'] == 1) 
    {  $template->assign_vars(array("JOB1_WEBSITE_ADDRESS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_WEBSITE_ADDRESS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_website_address_check'] == 1) 
    {  $template->assign_vars(array("JOB1_WEBSITE_ADDRESS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_WEBSITE_ADDRESS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_WEBSITE_ADDRESS_SEC_".$page_maker['job1_upload_website_address_sec'] => "selected='selected'" ) ); 

  // Ticket Money pre-select 
  if ($page_maker['job1_upload_ticket_money'] == 1) 
    {  $template->assign_vars(array("JOB1_TICKET_MONEY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_TICKET_MONEY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_ticket_money_check'] == 1) 
    {  $template->assign_vars(array("JOB1_TICKET_MONEY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_TICKET_MONEY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_TICKET_MONEY_SEC_".$page_maker['job1_upload_ticket_money_sec'] => "selected='selected'" ) ); 

  // Accomodation pre-select 
  if ($page_maker['job1_upload_accomodation'] == 1) 
    {  $template->assign_vars(array("JOB1_ACCOMODATION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_ACCOMODATION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_accomodation_check'] == 1) 
    {  $template->assign_vars(array("JOB1_ACCOMODATION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_ACCOMODATION_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_ACCOMODATION_SEC_".$page_maker['job1_upload_accomodation_sec'] => "selected='selected'" ) ); 

  // Salary pre-select 
  if ($page_maker['job1_upload_salary '] == 1) 
    {  $template->assign_vars(array("JOB1_SALARY_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_SALARY_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_salary_check'] == 1) 
    {  $template->assign_vars(array("JOB1_SALARY_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_SALARY_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_SALARY_SEC_".$page_maker['job1_upload_salary_sec'] => "selected='selected'" ) ); 


  // Number teachers pre-select 
  if ($page_maker['job1_upload_number_teachers'] == 1) 
    {  $template->assign_vars(array("JOB1_NUMBER_TEACHERS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_NUMBER_TEACHERS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_number_teachers_check'] == 1) 
    {  $template->assign_vars(array("JOB1_NUMBER_TEACHERS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_NUMBER_TEACHERS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_NUMBER_TEACHERS_SEC_".$page_maker['job1_upload_number_teachers_sec'] => "selected='selected'" ) ); 

  // Students pre-select 
  if ($page_maker['job1_upload_students'] == 1) 
    {  $template->assign_vars(array("JOB1_STUDENTS_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_STUDENTS_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_students_check'] == 1) 
    {  $template->assign_vars(array("JOB1_STUDENTS_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_STUDENTS_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_STUDENTS_SEC_".$page_maker['job1_upload_students_sec'] => "selected='selected'" ) ); 

  // Teacher Description pre-select 
  if ($page_maker['job1_upload_teacher_description'] == 1) 
    {  $template->assign_vars(array("JOB1_TEACHER_DESCRIPTION_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_TEACHER_DESCRIPTION_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_teacher_description_check'] == 1) 
    {  $template->assign_vars(array("JOB1_TEACHER_DESCRIPTION_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_TEACHER_DESCRIPTION_CHECK_OFF"=> "checked='checked'") ); } 

 $template->assign_vars(array("JOB1_TEACHER_DESCRIPTION_SEC_".$page_maker['job1_upload_teacher_description_sec'] => "selected='selected'" ) ); 
  // Gender pre-select 
  if ($page_maker['job1_upload_gender'] == 1) 
    {  $template->assign_vars(array("JOB1_GENDER_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_GENDER_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_gender_check'] == 1) 
    {  $template->assign_vars(array("JOB1_GENDER_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_GENDER_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_GENDER_SEC_".$page_maker['job1_upload_gender_sec'] => "selected='selected'" ) ); 

  // Contract pre-select 
  if ($page_maker['job1_upload_contract'] == 1) 
    {  $template->assign_vars(array("JOB1_CONTRACT_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_CONTRACT_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_contract_check'] == 1) 
    {  $template->assign_vars(array("JOB1_CONTRACT_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_CONTRACT_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_CONTRACT_SEC_".$page_maker['job1_upload_contract_sec'] => "selected='selected'" ) ); 

  // Picupload pre-select 
  if ($page_maker['job1_upload_picupload'] == 1) 
    {  $template->assign_vars(array("JOB1_PICUPLOAD_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_PICUPLOAD_OFF"=>"checked='checked'" ) ); } 

  if ($page_maker['job1_upload_picupload_check'] == 1) 
    {  $template->assign_vars(array("JOB1_PICUPLOAD_CHECK_ON"=>"checked='checked'" ) ); } 
  else 
    {  $template->assign_vars(array("JOB1_PICUPLOAD_CHECK_OFF"=> "checked='checked'") ); } 

  $template->assign_vars(array("JOB1_PICUPLOAD_SEC_".$page_maker['job1_upload_picupload_sec'] => "selected='selected'" ) ); 


  $template->assign_vars(array('TEACHER_FORM_TITLE'=>$form_title));

}
// put the action_monitor in here, will monitor things that employees do
else if ($_GET['mode']=='admin_action_monitor') {

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  @extract($_GET);

  // pull the data corresponding to the value sent 
  // possible values for $values might be user
  // build template for admin panel ...
  // 1 user : 2 date : 3 time : 4 action : 5 name

  // set todays date as the default 
  $date_query = "date = ";

  // pull out a timeframe here that we can use to list the last ten days
  $time = new timezone();
  $day_arr = $time->last_ten_days();

  $template->assign_block_vars('admin_action_monitor',array( 
						        'L_ADMIN_ACTION_MONITOR' => $lang['Action_monitor'],
						        'L_ADMIN_ACTION_MONITOR_TEXT' => $lang['Action_monitor_text'],
							'L_TODAY'=>$lang['Today'],
						        'L_YESTERDAY'=>$lang['Yesterday'],
							'DAY_2'=>$day_arr['day2'],
							'DAY_3'=>$day_arr['day3'],
							'DAY_4'=>$day_arr['day4'],
							'DAY_5'=>$day_arr['day5'],
							'DAY_6'=>$day_arr['day6'],
							'DAY_7'=>$day_arr['day7'],
							'DAY_8'=>$day_arr['day8'],
							'DAY_9'=>$day_arr['day9'],
							'STAMP_DAY0'=>$day_arr['stamp_day0'],
							'STAMP_DAY1'=>$day_arr['stamp_day1'],
							'STAMP_DAY2'=>$day_arr['stamp_day2'],
							'STAMP_DAY3'=>$day_arr['stamp_day3'],
							'STAMP_DAY4'=>$day_arr['stamp_day4'],
							'STAMP_DAY5'=>$day_arr['stamp_day5'],
							'STAMP_DAY6'=>$day_arr['stamp_day6'],
							'STAMP_DAY7'=>$day_arr['stamp_day7'],
							'STAMP_DAY8'=>$day_arr['stamp_day8'],
							'STAMP_DAY9'=>$day_arr['stamp_day9']
                                                        ) );

    // sort out the user bar selection bit ...
    $sql = 'SELECT username FROM users WHERE user_level > 1';

    if (!($result = $db->sql_query($sql)))
    {
      message_die(GENERAL_MESSAGE,'Problem retrieving user from db');
    }

    while ( $row = $db->sql_fetchrow($result) )
      {    

	$name = "<option value='".$row['username']."'>${row['username']}</option>";

	$template->assign_block_vars('admin_action_monitor.admin_monitor_users',array( 'USERS'=>$name ) );

      }

    // OK let's start defining sepcific searches here by taking values from $_GET
    if ( (isset($_GET['users'] ) ) && ( $_GET['users'] != '-1' ) ){ $type = 'user'; }
    if ( (isset($_GET['dates'] ) ) && ( $_GET['users'] == '-1' ) ){ $type = 'date'; } 

    // one day ahead of that time...
    $new_dates = $dates + ( 60 * 60 * 24 );

    // username to regdate
    $users = name2regdate($users);

    // define the query here 
    // default there were no $_GET values ... 
    switch($type)
    {

       case 'user'   : $query = " time BETWEEN '$dates' AND '$new_dates' AND user = $users"   ; break ;
       case 'date'   : $query = " time BETWEEN '$dates' AND '$new_dates' "   ; break ;
       default: $query = "time LIKE '%'" ; break;

    }

    $sql = "SELECT * FROM employee_tracker WHERE $query ";

    if (!($result = $db->sql_query($sql)) )
    {
       	message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
    }		

    while ( $row = $db->sql_fetchrow($result) )
    {

      // make this function ...
      $user = regdate($row['user']);

      // timestamp to date ...
      $show_time = $time->return_format($row['time'],$board_config['board_timezone'],'G i') ;
      
      // action define function 
      $action = define_action($row['action']);

      // data links here, recruiter_id -> link etc..
      $link = data_format($row['action'],$row['data']);
	
      $template->assign_block_vars('admin_action_monitor.admin_action_stats',
				   array( 'USER' => $user,
					  'TIME' => $show_time,
					  'ACTION' => $action,
					  'LINK' => $link
					      )  ); 

    }

}
// this will monitor the marketings stats, this script is really, really slow
else if ($_GET['mode']=='admin_marketing_stats') 
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // Guide hits ...
  $sql = Array();
  $arr_hits = Array();

  // find the hit rates of specfic pages
  $sql[0] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".PAGE1."'";
  $sql[1] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".PAGE2."'";
  $sql[2] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".PAGE3."'";
  $sql[3] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".PAGE4."'";
  $sql[4] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".PAGE5."'";
  $sql[5] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".GUIDE."'";
  $sql[6] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".CHAT."'";
  $sql[7] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".GAMES."'";
  $sql[8] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".MOVIE."'";
  $sql[9] = "SELECT COUNT(request_uri) FROM englishteachingkorea_access_log WHERE request_uri = '".GALLERY."'";

  // find the spider accesstimes ...
  $sql[10] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".ALTA_VISTA_UK."%' ORDER BY time_stamp DESC LIMIT 1 ";
  $sql[11] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".ALEXA."%' ORDER BY time_stamp DESC LIMIT 1 ";
  $sql[12] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".DAUM."%' ORDER BY time_stamp DESC LIMIT 1 ";
  $sql[13] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".EXCITE."%' ORDER BY time_stamp DESC LIMIT 1 ";
  $sql[14] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".GOOGLE."%' ORDER BY time_stamp DESC LIMIT 1 ";
  $sql[15] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".INKTOMI."%' ORDER BY time_stamp DESC LIMIT 1 ";
  $sql[16] = "SELECT FROM_UNIXTIME(time_stamp,'%Y-%M-%D') FROM englishteachingkorea_access_log WHERE agent LIKE '%".MSNBOT."%' ORDER BY time_stamp DESC LIMIT 1 ";
  
  // find the search engine links
  $sql[17] = "SELECT COUNT(referer) FROM englishteachingkorea_access_log WHERE referer LIKE '%google.com%' ";
  $sql[18] = "SELECT COUNT(referer) FROM englishteachingkorea_access_log WHERE referer LIKE '%yahoo.com%' ";
  $sql[19] = "SELECT COUNT(referer) FROM englishteachingkorea_access_log WHERE referer LIKE '%msn.com%' ";
  $sql[20] = "SELECT COUNT(referer) FROM englishteachingkorea_access_log WHERE referer LIKE '%daum.net%' ";

  for ($i=0;$i<count($sql);$i++)
    {

      if (!($result = $db_log->sql_query($sql[$i])))
	{
	  message_die(CRITICAL_ERROR,'Cannot pul values out of stats db','',__LINE__,__FILE__,$sql[$i]);
	}

      while ($row = $db_log->sql_fetchrow($result))
	{

	  switch (true)
	  {
	      case ($i <= 9): $arr_hits[$i] = $row['COUNT(request_uri)'] ; break;
              case ($i <= 16): $arr_hits[$i] = $row["FROM_UNIXTIME(time_stamp,'%Y-%M-%D')"] ; break;
              case ($i <= 20): $arr_hits[$i] = $row['COUNT(referer)'] ; break;
                 default: break;
	  }
	  
	}
    }

  // read in values from linkstats ...
  $sql = "SELECT linkstats_name FROM linkstats";
    
  if (!($result = $db->sql_query($sql)))
    {
	message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql);
    }

  // the array to store all the linkstat values
  $result_set = array();

  // read a whole result set of the dates
  while ($row = $db->sql_fetchrow($result))
    {
      $result_set[] = $row['linkstats_name'];
    }

  // IP, HITS, VISTORS displayed on bar graph
  
  // log format
  $log_month = strtolower(date('M'));
  $log_year = date('y');
  $log_date = $log_month.'_'.$log_year;

  // db date 
  $db_date = date('Y-m');
  $next_month = date('Y-m', mktime(0, 0, 0, date("m")+1, date("d"),  date("Y")));
  $db_date = $db_date . '-00';
  $next_month = $next_month . '-00';

  // number of unique visitors this month ... find number of rows here ...
  $sql_unique_visitors = "select distinct remote_host from englishteachingkorea_access_log where time_stamp between unix_timestamp('$db_date') + 0 and unix_timestamp('$next_month') + 0 group by remote_host";
  
  if (!($result_unique_visitors = $db_log->sql_query($sql_unique_visitors)))
    {
	message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_unique_visitors);
    }

  // find unique visitors to the site ...
  $visitors = $db_log->sql_numrows($result_unique_visitors);

  // total number of hits this month ... find rows here ...
  $sql_hits = "select COUNT(*) from englishteachingkorea_access_log where time_stamp between unix_timestamp('$db_date') + 0 and unix_timestamp('$next_month') + 0 ";

  if (!($result_hits = $db_log->sql_query($sql_hits)))
    {
	message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_hits);
    }

  // find number of hits this month ...
  $hits = $db_log->sql_numrows($result_hits);

  // total bw used this month...
  $sql_bw = "select sum(bytes_sent) from englishteachingkorea_access_log where time_stamp between timestamp('$db_date') + 0 and timestamp('$next_month') + 0";

  if (!($result_bw = $db_log->sql_query($sql_bw)))
    {
      	message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_bw);
    }

  // find bw used this month ...
  $bw_row = $db_log->sql_fetchrow($result_bw);
  $bw = floor($bw_row["sum(bytes)"]/1000000);
  
  // this months log term
  // just usebw here, but really they're all the same
  $log_search_term = 'log_bw_'.$log_date;
  
  // establish the different types of logging...
  $log_categories = array('bw','visitors','hits');

  // if the date and year aren't in the result set, then insert them
  if ( ( array_search($log_search_term,$result_set) !== 0 )  )
    {

      for ($i=0 ; $i<(count($log_categories)) ; $i++ )
      {

	  $name = 'log_'.$log_categories[$i].'_'.$log_date;
	  $position = $$log_categories[$i];
	  
	  $sql_log_insert = "INSERT IGNORE INTO linkstats ( linkstats_name, linkstats_pos ) VALUES ( '$name', $position)" ; 

 	  if (!($result = $db->sql_query($sql_log_insert)))
 	    {
 	      message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_log_insert);
	    }

      }

    }// if the date and year are in the result set. Then replace them
  else 
    {

      for ($i=0 ; $i<(count($log_categories)) ; $i++ )
      {

	  $name = 'log_'.$log_categories[$i].'_'.$log_date;
	  $position = $$log_categories[$i];

	  $sql_seo_update = "UPDATE linkstats SET linkstats_pos = $position WHERE linkstats_name = '$name' " ; 

  	  if (!($result = $db->sql_query($sql_seo_update)) )
  	    {
  	      message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_seo_update);
 	    }

      }

    }

  // SEARCH ENGINE RANKINGS include the search engine rankings
  $search_engines = array("google","yahoo","msn");
  $search_keywords = array("Teaching+English+Korea","English+Teaching+Korea");

  include_once($root_path.'includes/search_engine.'.$phpEx);
  $seo = new search_engine();

  // get the yahoo serp result string...
  $yahoo_serp1 =  $seo->yahoo_serp($search_keywords[0]);
  $yahoo_serp2 =  $seo->yahoo_serp($search_keywords[1]);

  // get the msn serp result string...
  $msn_serp1 =  $seo->msn_serp($search_keywords[0]);
  $msn_serp2 =  $seo->msn_serp($search_keywords[1]);

  // get the google serp result string...
  // this thing is off-line more often than not, so catch the exception

  try {

    //$google_serp1 =  $seo->google_serp($search_keywords[0]);
    //$google_serp2 =  $seo->google_serp($search_keywords[1]);

  }
  catch ( Exception $e )
    {
      // don't bother exiting with an error here, just carry on
      // google seem to be runnning some shoddy web services. 
      // NMP ... Not My Problem
      // message_die(GENERAL_MESSAGE,"Google exception: $e");
      $google_serp1=500;
      $google_serp2=500;

    }
    
  // UPDATE LINKSTATS TABLE update this months seo position records
  
  // seo_file date format
  $seo_date = $log_month;

    // search engines 
  $search_engines = array("google","yahoo","msn");
  $search_keywords = array("Teaching+English+Korea","English+Teaching+Korea");
  $serp_results = array($google_serp1,$google_serp2,$yahoo_serp1,$yahoo_serp2,$msn_serp1,$msn_serp2);

  // do a match on this term, if it's in the db already,
  // then update, if not then enter ...
  $search_term = 'seo_'.$search_engines[0]."_".$seo_date; 

  // if the date and year aren't in the result set, then insert them
  if ( !( array_search($search_term,$result_set) )  )
    {

      for ($i=0 ; $i<(count($search_engines)) ; $i++ )
      {

	  $name = 'seo_'.$search_engines[$i].'_'.$seo_date;

	  // serp results rises by 2
	  $z = $i * 2;
	  
	  // drag the value out of the serp return string ...
	  preg_match('/position is #([0-9]) for keywords./',$serp_results[$z],$matches);
	  $position0 = $matches[1];
	  echo $serp_results[$z].'<br>';

 	  preg_match('/position is #([0-9]) for keywords./',$serp_results[($z+1)],$matches);
	  $position1 = $matches[1];
	  echo $serp_results[($z+1)];

	  if (!(isset($position0))){ $position0 = 500; }

	  $sql_seo_insert = "INSERT IGNORE INTO linkstats ( linkstats_name, linkstats_pos, linkstats_keywords ) VALUES ( '$name', $position0, '$search_keywords[0]'), ('$name',$position1,'$search_keywords[1]')" ; 

 	  if (!($result = $db->sql_query($sql_seo_insert)))
 	    {
 	      message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_seo_insert);
	    }

      }

    }// if the date and year are in the result set. Then replace them
  else 
    {

      for ($i=0 ; $i<(count($search_engines)) ; $i++ )
      {

	  $name = 'seo_'.$search_engines[$i].'_'.$seo_date;

	  // serp results rises by 2
	  $z = $i * 2;
	  
	  // drag the value out of the serp return string ...
	  preg_match('/position is #([0-9]) for keywords./',$serp_results[$z],$matches);
	  $position0 = $matches[1];
	  if ($position0==0){$position0 = 0 ;}

 	  preg_match('/position is #([0-9]) for keywords./',$serp_results[($z+1)],$matches);
	  $position1 = $matches[1];
	  if ($position1==0){$position1 = 0 ;}

	  $sql0 = "UPDATE linkstats SET linkstats_pos = $position0 WHERE linkstats_name = '$name' AND linkstats_keywords = '$search_keywords[0]'" ; 

	  $sql1 = "UPDATE linkstats SET linkstats_pos = $position1 WHERE linkstats_name = '$name' AND linkstats_keywords = '$search_keywords[1]'" ; 

  	  if ( !($result = $db->sql_query($sql0)) && !($result = $db->sql_query($sql1)) )
  	    {
  	      message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql0.'<br><br>'.$sql1);
 	    }

      }

    }
  
    $template->assign_block_vars('admin_marketing_stats',array(
							     'L_STATS'=>'Stats',
							     'L_PAGE1'=>$lang['Page1_title'],
							     'PAGE1'=>$arr_hits[0],
							     'L_PAGE2'=>$lang['Page2_title'],
							     'PAGE2'=>$arr_hits[1],
							     'L_PAGE3'=>$lang['Page3_title'],
							     'PAGE3'=>$arr_hits[2],
							     'L_PAGE4'=>$lang['Page4_title'],
							     'PAGE4'=>$arr_hits[3],
							     'L_PAGE5'=>$lang['Page5_title'],
							     'PAGE5'=>$arr_hits[4],
							     'L_GUIDE'=>'Guide',
							     'GUIDE_HITS'=>$arr_hits[5],
							     'L_CHAT'=>'Chat',
							     'CHAT_HITS'=>$arr_hits[6],
							     'L_GAMES'=>'Games',
							     'GAMES_HITS'=>$arr_hit[7],
							     'L_MOVIE'=>'Movie',
							     'MOVIE_HITS'=>$arr_hits[8],
							     'L_GALLERY'=>'Gallery',
							     'GALLERY_HITS'=>$arr_hits[9],
							     'L_ALTA_VISTA_UK'=>'Alta-vista UK',
							     'ALTA_VISTA'=>$arr_hits[10],
							     'L_ALEXA'=>'Alexa',
							     'ALEXA'=>$arr_hits[11],
							     'L_DAUM'=>'Daum',
							     'DAUM'=>$arr_hits[12],
							     'L_EXCITE'=>'Excite',
							     'EXCITE'=>$arr_hits[13],
							     'L_GOOGLE'=>'Google',
							     'GOOGLE'=>$arr_hits[14],
							     'L_INKTOMI'=>'Inktomi',
							     'INKTOMI'=>$arr_hits[15],
							     'L_MSNBOT'=>'MSNbot',
							     'MSNBOT'=>$arr_hits[16],
							     'GOOGLE_LINKS'=>$arr_hits[17],
							     'YAHOO_LINKS'=>$arr_hits[18],
							     'L_YAHOO'=>'Yahoo',
							     'MSN_LINKS'=>$arr_hits[19],
							     'L_MSN'=>'MSN',
							     'DAUM_LINKS'=>$arr_hits[20],
							     'L_DAUM'=>'Daum',
							     'GOOGLE_SERP1'=>$google_serp1,
							     'YAHOO_SERP1'=>$yahoo_serp1,
							     'MSN_SERP1'=>$msn_serp1,
							     'GOOGLE_SERP2'=>$google_serp2,
							     'YAHOO_SERP2'=>$yahoo_serp2,
							     'MSN_SERP2'=>$msn_serp2
 							     )); 

  // COMPETITOR SITE TRACKING
  $sites = new site_tracker();
  $site_array = $sites->sites_info();
  $site_keys = array_keys($site_array);
  $site_values = array_values($site_array);
  //  message_die(GENERAL_MESSAGE,var_dump($site_array));

  for ($i=0;$i<=count($site_keys);$i++)
    {

      $template->assign_block_vars('admin_marketing_stats.site_tracker',array( 
							     'SITE_TRACK'=>$site_keys[$i],
							     'SITE_NUMBER'=>$site_values[$i]
							    ));
    }

  // find the most popular links to this site....
      $links_sql = "select referer, count(*) c from englishteachingkorea_access_log where referer NOT LIKE '%englishteachingkorea.com%' AND referer NOT LIKE '".DEVELOPER_IP."' AND referer != '\"-\"' group by referer order by c desc LIMIT 20";

      if (!($result = $db_log->sql_query($links_sql)))
	{
	  message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$links_sql);
	}

      // read out the links and put them in numbered count 
      // and links variables ...
      while ($row = $db_log->sql_fetchrow($result))
	{

	  $template->assign_block_vars('admin_marketing_stats.top_links',array( 
									    'TOP_LINK'=>str_replace('"','',$row['referer']),
									    'TOP_LINK_COUNT'=>$row['c']
									       ));
	}
      
}
// set up the sms interface .. allow text messaging
else if ( $_GET['mode'] == 'sms_interface' )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(GUEST);
  
  $number = '';

  // if we have a number then just insert it ...
  if (isset($_GET['number']))
    {
      $number = $_GET['number'];
    }

  // get general telephone blocks
  $telephone_code_block = $form_section->get_telephone_codes();

  $template->assign_block_vars('sms_interface',array( 
						     'L_SMS_INTERFACE'=>$lang['SMS_interface'],
						     'SMS_NUMBER'=>$number,
						     'TELEPHONE_CODE_BLOCK'=>$telephone_code_block
						     )
			       );

}
// do the marketing campaigns here
else if (($_GET['mode']=='admin_marketing_campaigns')) 
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // start up an apache log parser obj
  $log_parser = new apache_log_parser();

  // output the general form variables
  $template->assign_block_vars('admin_marketing_campaign',array(
							     'L_STATS'=>$lang['Stats'],
							     'L_CAMPAIGN'=>$lang['Campaign'],
							     'L_NAME'=>$lang['Name'],
							     'L_AD_PAGE'=>$lang['Ad_page'],
							     'L_AD_TITLE'=>$lang['Ad_title'],
							     'L_AD_COPY'=>$lang['Ad_copy'],
							     'L_SCRIPT'=>$lang['Script'],
							     'L_COST_PER_HIT'=>$lang['Cost_per_hit'],
							     'L_COST_PER_RESUME'=>$lang['Cost_per_resume'],
							     'L_COST_PER_SUCCESS_MATCH'=>$lang['Cost_per_success_match'],
							     'L_COST_PER_PLACED'=>$lang['Cost_per_hit'],
							     'L_PLACED'=>$lang['Placed'],
							     'L_HIT_RETURN'=>$lang['Hit_return'],
							     'L_HIT_RESUME'=>$lang['Hit_resume'],
							     'L_SUCCESS_MATCH'=>$lang['Success_match'],
							     'L_SUCCESS_FAILED'=>$lang['Success_failed'],
							     'L_NOTES'=>$lang['Notes']
							     ));

  $sql_paid = "SELECT * FROM paid_campaigns";

  if (!($result_paid = $db->sql_query($sql_paid)))
    {
      message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_paid);
    }

  // read out the links and put them in numbered count 
  // and links variables ...
  while ( $row = $db->sql_fetchrow($result_paid) )
    {

      // resume input tracking ...
      // find unique ips that came from ad page ...
      $ips_array = $log_parser->ips_from_page($row['ad_page']);

      // hit return is the number of unique visits which reference that page
      $hit_returns = count($ips_array);


      // which of the reference ips came to the resume page...
      $resume_returns = $log_parser->no_unique_ips($ips_array,'?mode=teacher_resume_form&rsub');

      // is there anyone in the success match table that came from this ad
      $success_match = $log_parser->ip_success_match($ips_array)  ;

      // catch divide by zero errors
      if ( $hit_returns == 0 ) 
	{
	  $hit_returns = 1 ;
	}

      // resume returns 
      if ( $resume_returns == 0 ) 
	{
	  $resume_returns = 1 ;
	}
      
      // success match
      if ( $success_match == 0 ) 
	{
	  $success_match = 1 ;
	}
     
      // cost per hit 
      $cost_per_hit = ($row['cost']/$hit_returns);

      // cost per resume 
      $cost_per_resume = ($row['cost']/$resume_returns);

      // cost per success match 
      $cost_per_success_match = ($row['cost']/$success_match);

      // find the currency 
      switch ($row['currency'])
      {
	case (1) : $currency = '$' ; break;
	case (2) : $currency = '€' ; break;
	case (3) : $currency = '₩' ; break;
	default : break; 
      }

      // use this to hide the campaign names. Div can't take spaces
      $campaign_paid_div_name = str_replace(' ','',$row['campaign_name']);

      $template->assign_block_vars('admin_marketing_campaign.marketing_paid_campaign',array(
							     'L_AD_PAGE'=>$lang['Ad_page'],
							     'L_SCRIPT'=>$lang['Script'],
							     'L_COST_OF_AD'=>$lang['Cost of Ad'],
							     'CAMPAIGN_ID'=>$row['id'],
							     'CAMPAIGN_PAID_NAME'=>$row['campaign_name'],
							     'CAMPAIGN_PAID_DIV_NAME'=>$campaign_paid_div_name,
							     'COST_OF_AD'=>$row['cost'],
							     'CURRENCY'=>$currency,
							     'AD_PAGE_RESULT'=>$row['ad_page'],
							     'SCRIPT'=>$row['script'],
							     'HIT_RETURN_RESULT'=>$hit_returns,
							     'HIT_RESUME_RESULT'=>$resume_returns,
							     'COST_PER_HIT'=>$cost_per_hit,
							     'COST_PER_RESUME'=>$cost_per_resume,
							     'SUCCESS_MATCH_RESULT'=>$success_match,
							     'COST_PER_SUCCESS_MATCH'=>$cost_per_success_match,
							     'NOTES'=>$row['notes']
							     ));       

    }

  $sql_free = "SELECT * FROM free_campaigns";

  if (!($result_free = $db->sql_query($sql_free)))
    {
      message_die(CRITICAL_ERROR,'Cannot pull values out of stats db','',__LINE__,__FILE__,$sql_free);
    }

  // read out the links and put them in numbered count 
  // and links variables ...
  while ($row = $db->sql_fetchrow($result_free))
    {
    
      // for the free campaigns the ad page is just the domain name of the site
      // taken from the last switch value of the script ...
      $script=split(' ',$row['script']);
      $domain = $script[1];

      // resume input tracking ...
      // find unique ips that came from ad page ...
      $ips_array = $log_parser->ips_from_domain($domain);

      // hit return is the number of unique visits which reference that page
      $hit_returns = count($ips_array);

      // THIS FUNCTION IS SLO0000000000W 
      // which of the reference ips came to the resume submitted page...
      $resume_returns = $log_parser->no_unique_ips($ips_array,'?mode=teacher_resume_form&rsub');

      // THIS FUNCTION IS DEAD
      // is there anyone in the success match table that came from this ad
      //$match_returns = $log_parser->ip_success_match($ips_array)  ;

      // find the currency 
      switch ($row['frequency'])
      {
	case (1) : $frequency = 'Daily' ; break;
	case (2) : $frequency = 'Second Day' ; break;
	case (3) : $frequency = 'Third Day' ; break;
	case (4) : $frequency = 'Fourth Day' ; break;
	case (5) : $frequency = 'Fifth Day' ; break;
	case (6) : $frequency = 'Sixth Day' ; break;
	case (7) : $frequency = 'Weekly' ; break;
	case (8) : $frequency = 'Monthly' ; break;
	default : break; 
      }

      // use this to hide the campaign names. Div can't take spaces
      $campaign_free_div_name = str_replace(' ','',$row['campaign_name']);

      $template->assign_block_vars('admin_marketing_campaign.marketing_free_campaign',array(
							     'CAMPAIGN_FREE_NAME'=>$row['campaign_name'],
							     'CAMPAIGN_FREE_DIV_NAME'=>$campaign_free_div_name,
							     'L_FREQUENCY'=>$lang['Frequency'],	
      							     'L_SCRIPT'=>$lang['Script'],
      							     'L_AD_TITLE'=>$lang['Ad_title'],
      							     'L_AD_COPY'=>$lang['Ad_copy'],
							     'CAMPAIGN_ID'=>$row['id'],
      							     'SCRIPT'=>$row['script'],
      							     'AD_TITLE'=>$row['ad_title'],
      							     'AD_COPY'=>$row['ad_copy'],
							     'NOTES'=>$row['notes'],
							     'FREQUENCY'=>$frequency,
							     'HIT_RETURN_RESULT'=>$hit_returns,
							     'HIT_RESUME_RESULT'=>$resume_returns,
							     'SUCCESS_MATCH_RESULT'=>$match_returns
							     )); 
    }

}
// add a recruiter here to the system, this is for recruiter info and adding/deleting recruiters
else if (($_GET['mode']=='add_recruiter'))
{

  // check user level
  access_check(USER);

  $sql = "SELECT * FROM users WHERE username != 'Anonymous' ";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a recruiter','',__LINE__,__FILE__,$sql);
    }

  $timezone_block = $form_section->get_timezone_options();

  $template->assign_block_vars('add_recruiter',array(
						     'L_NAME' => $lang['Name'], 
						     'L_INTRODUCTION' => $lang['Introduction'], 
						     'L_EXPERIENCE' => $lang['Experience'], 
						     'L_EDUCATION' => $lang['Education'], 
						     'L_LAST_LOGIN' => $lang['Last_login'], 
						     'TIMEZONE_BLOCK'=>$timezone_block,
						     'L_EMAIL' => $lang['Email'], 
						     'L_LEVEL' => $lang['Level'], 
						     'L_PHONE' => $lang['Phone'], 
						     'L_ACTIVE' => $lang['Active'], 
						     'L_TIMEZONE' => $lang['Timezone'],
						     'L_TELEPHONE' => $lang['Telephone'], 
						     'L_ANONYMOUS' => $lang['Anonymous'],
						     'L_CALL_TO' => $lang['Skype/Call_to'], 
						     'L_SIP' => $lang['Voipbuster/Sip'], 
						     'L_CALLBACK' => $lang['Callback'], 
						     'L_ALL' => $lang['All'], 
						     'L_SHOW_EMAIL' => $lang['Show_email'], 
						     'L_SHOW_NUMBER' => $lang['Show_number'],
						     'L_NO_EMAIL' => $lang['No_email'], 
						     'L_NO_NUMBER' => $lang['No_number']   
						     )
			       );

  // increasing number of forms
  $recruiter_form_number = '0';

  while ($row = $db->sql_fetchrow($result))
    {

      // keep these at '' or they'll carry to the next loop
      $recruiter_checked0 = '';
      $recruiter_checked1 = '';
      $recruiter_checked2 = '';

      // reset all the values for the timezones (this is a waste of code)
      for ( $i = 1 ; $i <= 13 ; $i++ )
	{
	  $timezone_number = 'timezone_selected'.$i;
	  $$timezone_number = '';
	}
      // set the recruiter form number
      $recruiter_resume_form = 'recruiter_resume_form'.$recruiter_form_number ;

      // see if the user is active
      if ( $row['user_active'] )
	{
	  $recruiter_active = "checked='Checked'";
	}

      // do the telephone mode, which telephone modes are they using ? skype/callto, voipbuster/sip, callback

      // call_to is the skype protocol
      if ( $row['user_call_to'] )
	{
	  $recruiter_call_to = "checked='Checked'";
	}

      // call_to is the sip protocol
      if ( $row['user_sip'] )
	{
	  $recruiter_sip = "checked='Checked'";
	}

      // callback will place a call to the users handphone
      if ( $row['user_callback'] )
	{
	  $recruiter_callback = "checked='Checked'";
	}

      // the anonymous section will hide the email and the telephone number from the recruiter...

      // show email to the user ?
      $row['user_show_email'] ? $recruiter_show_email_yes = "checked='Checked'" : $recruiter_show_email_no = "checked='Checked'" ; 

      // show the telephone number to the user ?
      $row['user_show_number'] ? $recruiter_show_number_yes = "checked='Checked'" : $recruiter_show_number_no = "checked='Checked'" ; 

      // check whether the recruiter is active or not...
      // for some reason it's checking more than one value. no idea why .. move on...
      switch ( $row['user_level'] )
	{
	case ('0') :  
	              $recruiter_checked0 = "selected='Selected'"; 
	              break ;
	case ('1') : 
	              $recruiter_checked1 = "selected='Selected'";
		      break ;
	case ('2') :  
	              $recruiter_checked2 = "selected='Selected'"; 
		      break ;
	default: break;
	}


      // sort out the timezone factor... this shouldn't be here... it should be taken care of 
      // in the form_section bit ... 
       switch ( $row['user_timezone'] )
	 {

	 case('TZ=America/Anchorage') :  $timezone_selected1 = "selected='Selected'" ; break ;
	 case('TZ=America/Los_Angeles') :  $timezone_selected2 = "selected='Selected'" ; break ;
	 case('TZ=America/Edmonton') :  $timezone_selected3 = "selected='Selected'" ; break ;
	 case('TZ=America/Winnipeg') :  $timezone_selected4 = "selected='Selected'" ; break ;
	 case('TZ=America/Toronto') :  $timezone_selected5 = "selected='Selected'" ; break ;
	 case('TZ=America/St_Johns') :  $timezone_selected6 = "selected='Selected'" ; break ;
	 case('TZ=Europe/London') :  $timezone_selected7 = "selected='Selected'" ; break ;
	 case('TZ=Europe/Berlin') :  $timezone_selected8 = "selected='Selected'" ; break ;
	 case('TZ=Asia/Bangkok') :  $timezone_selected9 = "selected='Selected'" ; break ;
	 case('TZ=Australia/Perth') :  $timezone_selected10 = "selected='Selected'" ; break ;
	 case('TZ=Asia/Seoul') :  $timezone_selected11 = "selected='Selected'" ; break ;
	 case('TZ=Australia/Sydney') :  $timezone_selected12 = "selected='Selected'" ; break ;
	 case('TZ=Pacific/Auckland') :  $timezone_selected13 = "selected='Selected'" ; break ;
	 default: break;

	 }

       // show the late login time of the recruiter
       $recruiter_last_login = strftime("%T %D",$row['user_lastvisit']);

       $template->assign_block_vars('add_recruiter.recruiter_resume',array(
								      'RECRUITER_NAME'=>$row['username'],
								      'RECRUITER_FORM_NAME'=>$recruiter_resume_form,
								      'RECRUITER_ACTIVE'=>$recruiter_active,
							              'RECRUITER_LAST_LOGIN'=>$recruiter_last_login,
     							              'RECRUITER_LEVEL_CHECKED0'=>$recruiter_checked0,
							              'RECRUITER_LEVEL_CHECKED1'=>$recruiter_checked1,
							              'RECRUITER_LEVEL_CHECKED2'=>$recruiter_checked2,
							              'RECRUITER_CALL_TO'=>$recruiter_call_to,
								      'RECRUITER_SIP'=>$recruiter_sip,
								      'RECRUITER_CALLBACK'=>$recruiter_callback,
							              'RECRUITER_SHOW_EMAIL_YES'=>$recruiter_show_email_yes,
							              'RECRUITER_SHOW_EMAIL_NO'=>$recruiter_show_email_no,
							              'RECRUITER_SHOW_NUMBER_YES'=>$recruiter_show_number_yes,
							              'RECRUITER_SHOW_NUMBER_NO'=>$recruiter_show_number_no,
     							              'RECRUITER_TIMEZONE_SELECTED1'=>$timezone_selected1,
     							              'RECRUITER_TIMEZONE_SELECTED2'=>$timezone_selected2,
     							              'RECRUITER_TIMEZONE_SELECTED3'=>$timezone_selected3,
     							              'RECRUITER_TIMEZONE_SELECTED4'=>$timezone_selected4,
     							              'RECRUITER_TIMEZONE_SELECTED5'=>$timezone_selected5,
     							              'RECRUITER_TIMEZONE_SELECTED6'=>$timezone_selected6,
     							              'RECRUITER_TIMEZONE_SELECTED7'=>$timezone_selected7,
     							              'RECRUITER_TIMEZONE_SELECTED8'=>$timezone_selected8,
     							              'RECRUITER_TIMEZONE_SELECTED9'=>$timezone_selected9,
     							              'RECRUITER_TIMEZONE_SELECTED10'=>$timezone_selected10,
     							              'RECRUITER_TIMEZONE_SELECTED11'=>$timezone_selected11,
     							              'RECRUITER_TIMEZONE_SELECTED12'=>$timezone_selected12,
     							              'RECRUITER_TIMEZONE_SELECTED13'=>$timezone_selected13,
							              'RECRUITER_EMAIL'=>$row['user_email'],
							              'RECRUITER_TIMEZONE'=>$row['user_timezone'],
							              'RECRUITER_ID'=>$row['user_id'],
							              'RECRUITER_EXPERIENCE'=>$row['user_experience'],
							              'RECRUITER_EDUCATION'=>$row['user_education'],
							              'RECRUITER_INTRODUCTION'=>$row['user_introduction'],
      							              'RECRUITER_MOBILE'=>$row['user_mobile'],
								      'RECRUITER_PHONE_MODE'=>$row['user_phone_mode']
								      )
			       );

       // number form
       $recruiter_form_number++;
       
    }

  // add the new resume ...
  $template->assign_block_vars('add_recruiter.add_resume',array());

}
// handle the free campaign form input ...
else if ((isset($_POST['free_campaign_add']))) {

  @extract($_POST);

  $sql = "INSERT INTO free_campaigns (campaign_name,frequency,ad_copy,ad_title,script,notes) 
            VALUES ('$campaign_free_name','$frequency','$ad_copy','$ad_title','$script','$notes')";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a free campaign','',__LINE__,__FILE__,$sql);
    }

  redirect("form_select.".$phpEx."?mode=admin_marketing_campaigns", true);

}
// handle the paid campaign form input ...
else if ((isset($_POST['paid_campaign_add']))) {

  @extract($_POST);

  $sql = "INSERT INTO paid_campaigns (campaign_name,cost,currency,ad_page,script,notes) 
            VALUES ('$campaign_paid_name','$cost_of_ad','$ad_currency','$ad_page','$script','$notes')";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a free campaign','',__LINE__,__FILE__,$sql);
    }

  redirect("form_select.".$phpEx."?mode=admin_marketing_campaigns", true);

}
// handle the free campaign form input ...
else if ((isset($_POST['free_campaign_delete']))) {

  @extract($_POST);

  $sql = "DELETE FROM free_campaigns WHERE id='$campaign_id'";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot delete a free campaign','',__LINE__,__FILE__,$sql);
    }

  redirect("form_select.".$phpEx."?mode=admin_marketing_campaigns", true);

}
// delete a paid campaign ...
else if ((isset($_POST['paid_campaign_delete']))) {

  @extract($_POST);

  $sql = "DELETE FROM paid_campaigns WHERE id='$campaign_id'";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot delete a paid campaign','',__LINE__,__FILE__,$sql);
    }

  redirect("form_select.".$phpEx."?mode=admin_marketing_campaigns", true);

}
// edit an existing free campaign
else if ((isset($_POST['free_campaign_edit']))) {

  @extract($_POST);

  $sql = "UPDATE free_campaigns 
            SET campaign_name='$campaign_free_name', frequency='$frequency', 
                 ad_copy = '$ad_copy', ad_title='$ad_title', script='$script', notes='$notes' 
            WHERE id='$campaign_id'";


  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot edit a free campaign','',__LINE__,__FILE__,$sql);
    }

  redirect("form_select.".$phpEx."?mode=admin_marketing_campaigns", true);

}
// edit an existing free campaign
else if ((isset($_POST['paid_campaign_edit']))) {

  @extract($_POST);

  $sql = "UPDATE paid_campaigns SET campaign_name='$campaign_paid_name', cost='$cost_of_ad', notes='$notes',
                                     currency='$ad_currency', ad_page='$ad_page', script='$script'
                                   WHERE id='$campaign_id'";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot edit a free campaign','',__LINE__,__FILE__,$sql);
    }

  redirect("form_select.".$phpEx."?mode=admin_marketing_campaigns", true);

}
// give me a history prespective so that I can find and adjust jobs and teachers...
else if ($_GET['mode']=='history') {

  $sql_teachers = "SELECT *, DATEDIFF(NOW(),submit_time) as date_diff FROM teachers ORDER BY arrival DESC";  
  $sql_schools = "SELECT name, aftercare, teacher_id, submit_time, arrival, email from teachers";

  if (!($result_teachers = $db->sql_query($sql_teachers)))
    {
      message_die(CRITICAL_ERROR,'Cannot edit a free campaign','',__LINE__,__FILE__,$sql_teachers);
    }

  $template->assign_block_vars('history',array() ); 

  // print the teachers out for history ...
  while ($row = $db->sql_fetchrow($result_teachers))
    {

      // calculate age of teacher 
      $birthyear=$row['birthyear'] += 1899 ;
      $birthyear -= date("Y") ; 
      $birthyear *= -1 ;

      // leave in this phone function for every phone number...
      $phone = phone_cleaner($row['phone']);

      // get the phone mode 
      $phone_mode = cp_phone_mode($phone,$userdata['user_regdate'],'teacher');

      // teachers first name for the candidate email form
      $teacher_fn = explode(' ',$row['name']);
      $teacher_fn = ucfirst($teacher_fn[0]);		

      // gather ring-time suitability
      $ring_time= new timezone();
      $time_array=$ring_time->time_fetch($row['from_time'], $row['until_time'], $row['timezone']);

      // arrival ...
      $arrival = arrival_to_text($row['arrival']);

      // submitted month to show how many resume I've been getting each month
      preg_match('/.....(..).*/','2006-11-06 23:58:53',$matches);

      $submitted=month($matches[1]);

      if ( $row['aftercare'] == 1 )
     	{
	  $aftercare_color = '#00CCFF' ;
     	}
      else 
	{
	  $aftercare_color='';
	}

      $template->assign_block_vars('history.teachers_history',array('NAME'=>$row['name'],
								    'TEACHER_ID'=>$row['teacher_id'],
								    'LOCATION'=>location($row['location']),
								    'EMAIL'=>$row['email'],
								    'PHONE_MODE'=>$phone_mode,
								    'AFTERCARE_COLOR'=>$aftercare_color,
								    'FROM_TIME'=>$time_array['from_time'],
								    'UNTIL_TIME'=>$time_array['until_time'],
								    'BIRTHYEAR'=>$birthyear,
								    'SUBMIT_TIME'=>$row['submit_time'],
								    'SUBMITTED'=>$submitted,
								    'ARRIVAL'=>$arrival
								    )
				   );
    }

}
// update the recruiter resumes ...
// recruiter_resume_edit from the add_recruiter page
else if ( ( isset($_POST['recruiter_resume_edit']) ) && ( !(isset($_POST['Delete'] ) ) ) ) 
{

  @extract($_POST);

  // overload the rec
  if ( isset($_POST['active'] ) )
    {
      $active=1;
    }

    // the checkbox will either arrive as '1' or nothing.
  ( $user_callback == 'on' ? $user_callback = 1 : $user_callback = 0 ) ;

  ( $user_sip == 'on' ? $user_sip = 1 : $user_sip = 0 ) ;

  ( $user_call_to == 'on' ? $user_call_to = 1 : $user_call_to = 0  ) ;

  // check the radio boxes 
  ( $user_show_email == 'on' ? $user_show_email = 1 : $user_show_email = 0  ) ;
  ( $user_show_number == 'on' ? $user_show_number = 1 : $user_show_number = 0  ) ;

  $sql = "UPDATE users SET username = '$name', user_email = '$email', user_timezone='$timezone', 
            user_callback = '$user_callback', user_sip = '$user_sip', user_call_to = '$user_call_to',
            user_show_email = '$user_show_email', user_show_number = '$user_show_number',
            user_experience = '$experience', user_education = '$university_education', 
            user_introduction = '$introduction', user_mobile='$phone',
            user_active='$active', user_level = '$level'  
            WHERE user_id = '$recruiter_resume_edit' ";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a update the resume','',__LINE__,__FILE__,$sql);
    }


}
// update the recruiter resumes ...
// the recruiter_resume_form update section ....
else if ((isset($_POST['recruiter_resume_add']))) {

  @extract($_POST);

  // don't forget the user reg_date
  $reg_date = time();

  $sql = "INSERT INTO users ( username, user_email, user_password, user_timezone, user_experience, user_education, 
                              user_introduction, user_mobile, user_active, user_level, user_regdate )
                 VALUES ( '$name', '$email', MD5('$password'), '$timezone', '$experience', '$university_education', 
                              '$introduction','$phone', '1', '1', '$reg_date' )";


  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a recruiter','',__LINE__,__FILE__,$sql);
    }

}
// delete one of the recrutiers ...
else if ( ( isset($_POST['recruiter_resume_edit']) ) && ( isset($_POST['Delete'] ) ) ) 
{

  @extract($_POST);

  $sql = "DELETE FROM users WHERE user_id = '$recruiter_resume_edit' ";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a update the resume','',__LINE__,__FILE__,$sql);
    }

}



// END TIMECODE MEASURE //
//$time = microtime();
//$time = explode(" ", $time);
//$time = $time[1] + $time[0];
//$endtime = $time;
//$totaltime = ($endtime - $begintime);
//echo 'PHP parsed this page in ' .$totaltime. ' seconds.'; $time = microtime();
// END TIMECODE MEASURE //


// parse the template
$template->pparse('body'); 

// bombs away
ob_end_flush();
						
$db->sql_close();


?>