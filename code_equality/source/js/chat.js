// set up a unique cookie here to store the name 
// and the random color of the client ...
if (!(getCookie('ETK_chat_name')))
{
	// grab a name for the person
	var name = prompt ("What is your name ?","")

	// color possibilites for the clients
	var colors = new Array('#000000','#F8BA60','#37E850','#CFC4B3','#EDEDF9','#FFE800');

	// pick a random number
	var rand = Math.floor(Math.random() * 6);

	// pic a color
	var color = colors[rand];

	if (name != '')
	{
		setCookie('ETK_chat_name',name,100);
		setCookie('ETK_chat_color',color,100);
	}

}

// seems to build two ajax request objects and listeners...
// One for reading ajax requests and one for writing...
// one object will handle the polling of the server to check for new text....
// the other will handle the sending on the clients text to the server...
var http_request=false;
var http_request2=false;
var intUpdate;

// first ajax request... for writing 
function ajax_request(url)
{
	http_request=false;

	if(window.XMLHttpRequest)
	{
		http_request=new XMLHttpRequest();
		
<!--
		if(http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/xml');
		}
-->
	}
	else if(window.ActiveXObject)
	{
		try
		{
			http_request=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e)
		{
			try
			{
				http_request=new ActiveXObject("Microsoft.XMLHTTP");
			}catch(e){}
		}
	}
	if(!http_request)
	{
		alert('Giving up :( Cannot create an XMLHTTP instance');return false;
	}
	
	http_request.onreadystatechange=alertContents;
	http_request.open('GET',url,true);
	http_request.send(null);

}

// respone handler for first object...
function alertContents()
{	
	if(http_request.readyState==4)
	{
		if(http_request.status==200)
		{
			rec_response(http_request.responseText);
		}else{}
	}
}

// second request object ... for reading
function ajax_request2(url)
{
	http_request2=false;

	if(window.XMLHttpRequest)
	{
		http_request2=new XMLHttpRequest();

<!--
		if(http_request2.overrideMimeType)
		{
			http_request2.overrideMimeType('text/xml');
		}
-->

	}else if(window.ActiveXObject)
	{
		try{
			http_request2=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e)
		{try
			{
				http_request2=new ActiveXObject("Microsoft.XMLHTTP");
			}catch(e){}
		}
	}
	if(!http_request2)
	{
		alert('Giving up :( Cannot create an XMLHTTP instance');return false;
	}
	
	http_request2.onreadystatechange=alertContents2;
	http_request2.open('GET',url,true);
	http_request2.send(null);

}

// handle the writing request response
function alertContents2()
{

	if(http_request2.readyState==4)
	{
		if(http_request2.status==200)
		{
			rec_chatcontent(http_request2.responseText);
		}else{}
	}
}

<!-- SCRIPT STARTS HERE -->
// set the update timeout...
// post a "connecting string while we wait for a response from the chat.txt
// file on the server. The chat.txt file must have at least one char in it...
waittime=2000;

// initiate the setTimeout 
intUpdate=window.setTimeout("read_cont()", waittime);
document.getElementById('chatwindow').innerHTML = "connecting...";

// display the message on the screen...
function display_msg(msg1) {

	/* Fill Textarea with the Content */
	document.getElementById('chatwindow').innerHTML = msg1 ;

}

// clear the txt file of all it's contents
// for use after the conversation is over...
function clear_chat()
{

    var str = "w.php?chat=1&clear=1" ;
    document.getElementById('chatwindow').innerHTML = '';
    ajax_request(str);

}

// write the message to the server...including the name and color...
function write_msg(msg1) {

	var str = "w.php?chat=1&m=" + escape(msg1) + "&name=" + unescape(getCookie('ETK_chat_name')) + "&color=" + escape(getCookie('ETK_chat_color')) ;

	ajax_request(str);

}

// send the messsage and blank the box ...
function submit_msg() {
	/* Send My Message */
	write_msg(document.getElementById('chatmsg').value);
	document.getElementById('chatmsg').value="";
}

// ???
function rec_response(str1) {
/* Response From w.php */
}

// take in the contents of the chat.txt file from the server
// parse it and then send it to display_msg()... call read_cont()
// which will time another ajax_request2 to pull more text from the server.
function rec_chatcontent(cont1) {

	if (cont1 != "") 
	{ 
		out1 = "";
		/* Display Last Message First */
		
		while (cont1.indexOf("\n") > -1) {

			out1 = cont1.substr(0, cont1.indexOf("\n")) + "\n" + out1;
			cont1 = cont1.substr(cont1.indexOf("\n") + 1);
		}

		out1 = unescape(out1);

		// if the message we are pulling out isn't the same message that's in the screen 
		// already, then just display it ...
		if (document.getElementById('chatwindow').innerHTML != out1) { display_msg(out1); }

		// continue reading off the server
		intUpdate=window.setTimeout("read_cont()", waittime);
	
	}
}

// prevent buffering... call the ajax request2 obj to 
// drag the info off the server again ...
function read_cont() { 

	/* Prevent Buffering by using ?x=timeinms */
	zeit = new Date();
   
	// change the url everytime to prevent buffering on the client side...
	ms = (zeit.getHours() * 24 * 60 * 1000) + (zeit.getMinutes() * 60 * 1000) + (zeit.getSeconds() * 1000) + zeit.getMilliseconds();
   	ajax_request2("chat.txt?chat=1&x=" + ms); 
   
}
   
// ???
function keyup(arg1) {if (arg1 == 13) {submit_msg(); } }

