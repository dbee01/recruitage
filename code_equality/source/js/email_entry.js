// this will validate the Korean employee's job entry form ...
// it will be called email_entry
function validate() {
// school
if(document.email_entry.school.value == '') { 
  alert('Please fill out school name.')   ;
   document.email_entry.school.focus();
  return false;
}
// contact
if ( document.email_entry.contact.value == '' )
{
 alert ( "Please fill out contact name." );
  document.email_entry.contact.focus();
 return false;
}
//check email field (to make sure it's not blank)
if(document.email_entry.email.value == '') {
 alert('You must enter in your email.');
 document.email_entry.email.focus();
 return false;
} else {
//Now check if email is valid
if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.email_entry.email.value))) {
 alert("The email you entered is not a valid email address.");
 document.email_entry.email.focus();
 return false;
}
// phone
if(document.email_entry.phone.value == '') { 
  alert('Please fill out phone.');
   document.email_entry.phone.focus();
  return false;
}
// school description
if(document.email_entry.school_description.value == '') { 
  alert('Please fill out school description.');
   document.email_entry.school_description.focus();
  return false;
}
// city
if(document.email_entry.city.selectedIndex == 0 ) { 
  alert('Please fill out city.');
   document.email_entry.city.focus();
  return false;
}
// salary
if(document.email_entry.salary.value == '') { 
  alert('Please fill out salary.');
   document.email_entry.salary.focus();
  return false;
}
// organization
if ( document.email_entry.organization.selectedIndex == 0 )
{
 alert ( "Please select the organization." );
  document.email_entry.organization.focus();
 return false;
}
// number teachers
if ( document.email_entry.number_teachers.selectedIndex == 0 )
{
 alert ( "Please select number of teachers." );
  document.email_entry.number_teachers.focus();
 return false;
}

}

}
