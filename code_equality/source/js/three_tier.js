// JavaScript Document

function nullOptions(aMenu){
 var tot=aMenu.options.length
 for (i=0;i<tot;i++)
 {
 aMenu.options[i]=null
 }
 aMenu.options.length=0;
}

/* ####################### myTopics ####################### */
function MyTopics1(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Busanjin-gu","Busanjin-gu");
 options[2]=new Option("Dong-gu","Dong-gu");
 options[3]=new Option("Haeundae-gu","Haeundae-gu");
 options[4]=new Option("Jung-gu","Jung-gu");
 options[5]=new Option("Nam-gu","Nam-gu");
 options[6]=new Option("Seo-gu","Seo-gu");
 options[7]=new Option("Yeonje-gu","Yeonje-gu");
 }
}

function MyTopics2(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","");
 options[1]=new Option("Boeun-gu","Boeun-gu");
 options[2]=new Option("Cheongju","Cheongju");
 options[3]=new Option("Chungju","Chungju");
 options[4]=new Option("Okcheon-gun","Okcheon-gun");
 options[0].selected=true
 }
}

function MyTopics3(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Boryeong","Boryeong");
 options[2]=new Option("Cheonan","Cheonan");
 options[3]=new Option("Seosan","Seosan");
 options[0].selected=true
 }
}

function MyTopics4(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Ap-san","Ap-san");
 options[2]=new Option("Buk-gu","Buk-gu");
 options[3]=new Option("Dalseo-gu","Dalseo-gu");
 options[4]=new Option("Dalseong-gun","Dalseong-gun");
 options[5]=new Option("Dong-gu","Dong-gu");
 options[6]=new Option("Jung-gu","Jung-gu");
 options[7]=new Option("Nam-gu","Nam-gu");
 options[8]=new Option("Seo-gu","Seo-gu");
 options[9]=new Option("Suseong-gu","Suseong-gu");
 options[0].selected=true
 }
}

function MyTopics5(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Daedeok-gu","Daedeok-gu");
 options[2]=new Option("Seo-gu","Seo-gu");
 options[3]=new Option("Yuseong-gu","Yuseong-gu");
 options[0].selected=true
 }
}

function MyTopics6(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Sokcho","Sokcho");
 options[2]=new Option("Wonju","Wonju");
 options[0].selected=true
 }
}

function MyTopics7(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Gwangsan-gu","Gwangsan-gu");
 options[0].selected=true
 }
}

function MyTopics8(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Ansan","Ansan");
 options[2]=new Option("Anseong","Anseong");
 options[3]=new Option("Bucheon","Bucheon");
 options[4]=new Option("Dongducheon","Dongducheon");
 options[5]=new Option("Goyang","Goyang");
 options[6]=new Option("Gunpo","Gunpo");
 options[7]=new Option("Guri","Guri");
 options[8]=new Option("Gwangmyeong","Gwangmyeong");
 options[9]=new Option("Icheon","Icheon");
 options[10]=new Option("Osan","Osan");
 options[11]=new Option("Paju","Paju");
 options[12]=new Option("Pyeongtaek","Pyeongtaek");
 options[13]=new Option("Seongnam","Seongnam");
 options[14]=new Option("Siheung","Siheung");
 options[15]=new Option("Suwon","Suwon");
 options[16]=new Option("Uijeongbu","Uijeongbu");
 options[17]=new Option("Yongin","Yongin");
 options[0].selected=true
 }
}

function MyTopics9(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Downtown","Downtown");
 options[2]=new Option("Eastern Zone","Eastern Zone");
 options[0].selected=true
 }
}

function MyTopics10(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Andong","Andong");
 options[2]=new Option("Yecheon-gun","Yecheon-gun");
 options[0].selected=true
 }
}

function MyTopics11(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Chang-won","Chang-won");
 options[2]=new Option("Geoje","Geoje");
 options[3]=new Option("Jinju","Jinju");
 options[4]=new Option("Jiri-san","Jiri-san");
 options[5]=new Option("Masan","Masan");
 options[6]=new Option("Ulsan","Ulsan");
 options[0].selected=true
 }
}

function MyTopics12(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Bupyeong-gu","Bupyeong-gu");
 options[2]=new Option("Gyeyang-gu","Gyeyang-gu");
 options[3]=new Option("Namdong-gu","Namdong-gu");
 options[4]=new Option("Nam-gu","Nam-gu");
 options[0].selected=true
 }
}

function MyTopics13(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Jeju city","Jeju city");
 options[2]=new Option("Jungmun","Jungmun");
 options[3]=new Option("Seogwipo","Seogwipo");
 options[0].selected=true
 }
}

function MyTopics14(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Gunsan","Gunsan");
 options[2]=new Option("Jeonju","Jeonju");
 options[0].selected=true
 }
}

function MyTopics15(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Mokpo","Mokpo");
 options[0].selected=true
 }
}

function MyTopics16(aMenu){
 nullOptions(aMenu);
 with (aMenu){
 options[0]=new Option("Select area","none");
 options[1]=new Option("Dobong-gu","Dobong-gu");
 options[2]=new Option("Dongdaemun-gu","Dongdaemun-gu");
 options[3]=new Option("Dongjak-gu","Dongjak-gu");
 options[4]=new Option("Eunpyeong-gu","Eunpyeong-gu");
 options[5]=new Option("Gangbuk-gu","Gangbuk-gu");
 options[6]=new Option("Gangdong-gu","Gangdong-gu");
 options[7]=new Option("Gangnam-gu","Gangnam-gu");
 options[8]=new Option("Gangseo-gu","Gangseo-gu");
 options[9]=new Option("Geumcheon-gu","Geumcheon-gu");
 options[10]=new Option("Guro-gu","Guro-gu");
 options[11]=new Option("Gwanak-gu","Gwanak-gu");
 options[12]=new Option("Gwangjin-gu","Gwangjin-gu");
 options[13]=new Option("Han-river","Han-river");
 options[14]=new Option("Jongno-gu","Jongno-gu");
 options[15]=new Option("Jung-gu","Jung-gu");
 options[16]=new Option("Jungrang-gu","Jungrang-gu");
 options[17]=new Option("Mapo-gu","Mapo-gu");
 options[18]=new Option("Nowon-gu","Nowon-gu");
 options[19]=new Option("Seocho-gu","Seocho-gu");
 options[20]=new Option("Seodaemun-gu","Seodaemun-gu");
 options[21]=new Option("Songpa-gu","Songpa-gu");
 options[22]=new Option("Yangcheon-gu","Yangcheon-gu");
 options[23]=new Option("Yeongdeungpo-gu","Yeongdeungpo-gu");
 options[24]=new Option("Yongsan-gu","Yongsan-gu");
 options[0].selected=true
 }
}
/* ####################### go() ####################### */

function go(aMenu){
 if (aMenu.options.value!= "none")
  {
   location=aMenu.options[aMenu.selectedIndex].value
  }
 else
   aMenu.options[0].selected=true;
}
/* ####################### setUp ####################### */
function setUp(){
 if (navigator.appName.indexOf("Microsoft")>-1)
 {
 document.form.menuTopics.options[0].selected=true;
 document.form.menuSubjects.options[0].selected=true;
 }
}
/* ####################### change Subjects ####################### */
function changeSubjects(){
 aMenu3=document.form.menuTopics
 aMenu=document.form.menuSubjects

 with (aMenu3){
 switch (selectedIndex) {
 case 0:
  nullOptions(aMenu)
  history.go(0)
 break

 case 1:
  nullOptions(aMenu)
  MyTopics1(aMenu)
 break

 case 2:
  nullOptions(aMenu)
  MyTopics2(aMenu)
 break

 case 3:
  nullOptions(aMenu)
  MyTopics3(aMenu)
 break
 
 case 4:
  nullOptions(aMenu)
  MyTopics4(aMenu)
 break

 case 5:
  nullOptions(aMenu)
  MyTopics5(aMenu)
 break
 
 case 6:
  nullOptions(aMenu)
  MyTopics6(aMenu)
 break

 case 7:
  nullOptions(aMenu)
  MyTopics7(aMenu)
 break

 case 8:
  nullOptions(aMenu)
  MyTopics8(aMenu)
 break

 case 9:
  nullOptions(aMenu)
  MyTopics9(aMenu)
 break

 case 10:
  nullOptions(aMenu)
  MyTopics10(aMenu)
 break

 case 11:
  nullOptions(aMenu)
  MyTopics11(aMenu)
 break

 case 12:
  nullOptions(aMenu)
  MyTopics12(aMenu)
 break
 
 case 13:
  nullOptions(aMenu)
  MyTopics13(aMenu)
 break
 
 case 14:
  nullOptions(aMenu)
  MyTopics14(aMenu)
 break
 
 case 15:
  nullOptions(aMenu)
  MyTopics15(aMenu)
 break
 
 case 16:
  nullOptions(aMenu)
  MyTopics16(aMenu)
 break;
  }
 }
}

