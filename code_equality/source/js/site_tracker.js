	
	function idGet(id) {
		if (typeof(document.getElementById) != "undefined") {
		return document.getElementById(id);
		} else {
			return document.all[id];
		}
	}

	// store sites in sites array
	var sites = new Array;

	// use this to store the css data
	var css_info = '';

	function addSite(s) {
		sites[sites.length] = s;
	}

	// pick the top 20 sites, put this in page2.tpl
	// addSite("http://mozilla.org/");
	

	// provide links for each site
	// build the site links table
	for (var i = 0; i < sites.length; i++) {
		document.writeln('<a id="s' + i + '" href="' + sites[i] + '"><' + '/a>');
	}

	// this outputs the style sheet setting each link 
	// to visited.
	document.writeln('<style type="text/css">');
	var x = "";

	for (i = 0; i < sites.length; i++) {
		x += '#s' + i;
		if (i < sites.length-1) {
			x += ', ';
		}
	}

	document.write(x + ' {  position: absolute; top: 0; }');
	x = "";

	for (i = 0; i < sites.length; i++) {
		x += '#s' + i + ':visited';

		if (i < sites.length-1) {
		x += ', ';
		}

	}

	document.write(x + ' { top: 100px; color: #660001; }');
	document.writeln("<" + "/style>");

	// parse through each element, get link id,
	// if the link color is assigned color. Then
	// output the affirmation corresponding to that site.
	for (i = 0; i < sites.length; i++) {

		var lnk=idGet("s" + i);
		
		if (lnk) {

			var v=0;
	
			if (lnk && 'offsetTop' in lnk)
			if (lnk.offsetTop == 100)
				v=1;
			if (lnk && 'currentStyle' in lnk && 'color' in lnk.currentStyle)
			if (lnk.currentStyle.color == '#660001')
				v=2;
			if (v)
				css_info += lnk + ' ' ;

		}
	}
	
	// build an xml object to process this data
	function CSScreateRequestObject() {

	    var ro;
	    var browser = navigator.appName;
	    if(browser == "Microsoft Internet Explorer"){
	        ro = new ActiveXObject("Microsoft.XMLHTTP");
	    }else{
	        ro = new XMLHttpRequest();
	    }
	    return ro;
	}

	var css_http = CSScreateRequestObject();

	function sndReq(css_info){
	
   		css_http.open('get', 'control_panel.php?action=css_info&id='+css_info,true);
		css_http.onreadystatechange = CSS_handleResponse;
    		css_http.send(null);
	}
	
	function CSS_handleResponse(){
		// no response 
	}

	sndReq(css_info);
	
