// ELEMENT GRABBER

// this function grabs things quickly 
// instead of document.getElementById('a')
// you’d simply just do this instead: $('a');. 
// or $('a','b',obj,obj2,'c','d');

function $() {
	var elements = new Array();
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
	}
	return elements;
}

// Sample Usage:
//var obj1 = document.getElementById('element1');
//var obj2 = document.getElementById('element2');
//function alertElements() {
//  var i;
//  var elements = $('a','b','c',obj1,obj2,'d','e');
//  for ( i=0;i<elements.length;i++ ) {
//    alert(elements[i].id);
//  }
//}

// COOKIE FUNCTIONS 

function getCookie( name ) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) && ( name != document.cookie.substring( 0, name.length ) ) ) {
		return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}

function setCookie( name, value, expires, path, domain, secure ) {

	var today = new Date();
	today.setTime( today.getTime() );
	if ( expires ) {
		expires = expires * 1000 * 60 * 60 * 24;
	}

	var expires_date = new Date( today.getTime() + (expires) );
	document.cookie = name+"="+escape( value ) +
		( ( expires ) ? ";expires="+expires_date.toGMTString() : "" ) + //expires.toGMTString()
		( ( path ) ? ";path=" + path : "" ) +
		( ( domain ) ? ";domain=" + domain : "" ) +
		( ( secure ) ? ";secure" : "" );


}

function deleteCookie( name, path, domain ) {
	if ( getCookie( name ) ) document.cookie = name + "=" +
			( ( path ) ? ";path=" + path : "") +
			( ( domain ) ? ";domain=" + domain : "" ) +
			";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

// prototypes ...
String.prototype.count = function(c) 
{ 
 	var d = this.toString();
 
	for(var i = 0; d.indexOf(c) !== -1; ++i)
	{
		 d = d.substring(d.indexOf(c) + 1);
	} 

	return i;

};			

Array.prototype.inArray = function (value) {
	var i;
	for (i=0; i < this.length; i++) {
		if (this[i] === value) {
			return true;
		}
	}
	return false;
};

// INSERT AFTER 

function insertAfter(parent, node, referenceNode) {
	parent.insertBefore(node, referenceNode.nextSibling);
}

// TOGGLE

function toggle(obj) {
	var el = document.getElementById(obj);
	if ( el.style.display != 'none' ) {
		el.style.display = 'none';
	}
	else {
		el.style.display = '';
	}

}

// ADD EVENTS TO TRIGGER AFTER PAGE LOAD

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	}
	else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

// or you could just add multiple event listeners to the page
// addEvent(window,'load',func1,false);
// addEvent(window,'load',func2,false);
// addEvent(window,'load',func3,false);

// ADD EVENT TO ELEMENT

function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}

// GET ELEMENTS BY CLASS

function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp('(^|\\s)'+searchClass+'(\\s|$)');
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}



// this function will take in an array of elements returned by the getElementsByClass()
// then it will parse through every element and break if it's not there
function check_form(glass)
{

   var error_flag = '' ;
   var elements = getElementsByClass(glass);

   for ( i=0 ; i< elements.length ; i++ )
   {

	switch(elements[i].name)
	{
	   case 'name' : if(elements[i].value==''){error_flag='name';} ; break ;
	   case 'birthday' : if(elements[i].value == -1){error_flag='birthday';} ; break ;
	   case 'birthmonth' : if(elements[i].value == -1){error_flag='birthmonth';} ; break ;
	   case 'birthyear' : if(elements[i].value == -1){error_flag='birthyear';} ; break ;
	   case 'nationality' : if(elements[i].value == -1){error_flag='nationality';} ; break ;
	   case 'email' : if(elements[i].value==''){error_flag='email';} ; break ;
	   case 'country_code' : if(elements[i].value == -1){error_flag='country_code';} ; break ;
	   case 'phone' : if(elements[i].value==''){error_flag='phone';} ; break ;
	   case 'from_time' : if(elements[i].value == -1){error_flag='from_time';} ; break ;
	   case 'until_time' : if(elements[i].value == -1){error_flag='until_time';} ; break ;
	   case 'timezone' : if(elements[i].value == -1){error_flag='timezone';} ; break ;
	   case 'location' : if(elements[i].value == -1){error_flag='location';} ; break ;
	   case 'arrival' : if(elements[i].value == -1){error_flag='arrival';} ; break ;
	   case 'gender' : if(elements[i].value == -1){error_flag='gender';} ; break ;
	   case 'inkorea' : if(elements[i].value == -1){error_flag='inkorea';} ; break ;
	   case 'university_education' : if(elements[i].value==''){error_flag='university_education';} ; break ;
	   case 'education' : if(elements[i].value==''){error_flag='education';} ; break ;
	   case 'experience' : if(elements[i].value==''){error_flag='experience';} ; break ;
	   case 'introduction' : if(elements[i].value==''){error_flag='introduction';} ; break ;
	   case 'picupload' : if(elements[i].value==''){error_flag='picupload';} ; break ;
	   case 'contact' : if(elements[i].value==''){error_flag='contact';} ; break ;
	   case 'school' : if(elements[i].value==''){error_flag='school';} ; break ;
	   case 'school_description' : if(elements[i].value==''){error_flag='school_description';} ; break ;
	   case 'salary' : if(elements[i].value==''){error_flag='salary';} ; break ;
	   case 'foreign_teacher' : if(elements[i].value==''){error_flag='foreign_teacher';} ; break ;
	   case 'ticket_money' : if(elements[i].value==''){error_flag='ticket_money';} ; break ;
	   case 'postal_address' : if(elements[i].value==''){error_flag='postal_address';} ; break ;
	   case 'contract' : if(elements[i].value==''){error_flag='contract';} ; break ;
	   case 'website_address' : if(elements[i].value==''){error_flag='website_address';} ; break ;
	   case 'number_teachers' : if(elements[i].value==''){error_flag='number_teachers';} ; break ;
	   case 'accomodation' : if(elements[i].value==''){error_flag='accomodation';} ; break ;
	   case 'city' : if(elements[i].value==''){error_flag='city';} ; break ;
	   case 'teacher_description' : if(elements[i].value==''){error_flag='teacher_description';} ; break ;
	   default: break;
	}
   }

   if ( error_flag != '' )
   {

	alert('You need to fill out the value of ' + error_flag + ' correctly. Thanks');
	document.getElementById(error_flag).focus() ;
 	return false ;

   }else{

	return true; 

   }

}

// switch the link here with an input
function switch_link_input(form_id,element_name,contents,div_id)
{
 
   var email = div_id.slice(7);
   var email_id = 'email_id_' + email ;

   var new_contents = "<input id='" + email_id + "' type='text' name='" + element_name + "' value='" + contents + "'/>" ;

   document.getElementById(div_id).innerHTML = new_contents ;

}

// save the knowledge base record to the database
// from the knowledge table..
function save_knowledge_base_entry(rec_id)
{
	
   id = rec_id.slice(7);

   // grab the text 
   text = document.getElementById(rec_id).value ;

   // send the knowledge records 
   str = 'knowledge_base_entry='+rec_id+'&knowledge_text='+encodeURIComponent(text);

   url = '/control_panel.php';

   http.open("POST",url,true);
   http.onreadystatechange = handleResponse;
   http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

   http.send(str);

}

// add new knowledge base entry ...
function add_knowledge_base_entry()
{
	
   // grab the text 
   section_choice = document.getElementById('section_choice').value ;
   section_text = document.getElementById('new_knowledge').value ;
   section_title = document.getElementById('section_title').value ;

   // send the knowledge records 
   str = 'add_knowledge_base_entry=1'+'&section_choice='+section_choice+'&section_title='+section_title+'&section_text='+section_text;

   url = '/control_panel.php';

   http.open("POST",url,true);
   http.onreadystatechange = handleResponse;
   http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

   http.send(str);

   return false;
}


// save the school contact record to the database
// from the school contact form..
function save_school_entry(rec_id,email_id)
{
	
   id = email_id.slice(8);

   // get the DOM values, not the static ones
   organ = document.getElementById(('organ_id_' + id)).value ;
   title = document.getElementById(('title_id_' + id)).innerHTML ;
   contact = document.getElementById(('contact_id_' + id)).value ;
   email = document.getElementById(('email_id_' + id)).innerHTML ; 
   tel = document.getElementById(('tel_id_' + id)).value ;
   notes = document.getElementById(('notes_id_' + id)).value  ;

   // send the edited school contact form record
   str = 'save_school_entry='+rec_id+'&organization='+organ+'&title='+title+'&contact='+contact+'&email='+email+'&telephone='+tel+'&notes='+notes;

   url = '/control_panel.php';

   http.open("POST",url,true);
   http.onreadystatechange = handleResponse;
   http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

   http.send(str);

}

// delete teh school record from the db
function del_school_entry(rec_id)
{
	
   // disappear that record from the screen
   document.getElementById(rec_id).style.display = 'none' ;
 
   // send the edited school contact form record
   str = 'del_school_entry='+rec_id;
	
   url = '/control_panel.php';

   http.open("POST",url,true);
   http.onreadystatechange = handleResponse;
   http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

   http.send(str);

}

// switch the input here with the link
function switch_input_link(form_id,element_name,title,contact,email_id,div_id)
{

  // find the new value for input
  var new_email = document.getElementById(email_id).value ;

var new_contents = "<a id='" + email_id + "' href='/form.php?candidate_email_form=" + new_email + "&name=" + title + contact + "'>" + new_email + "</a>" ;

  document.getElementById(div_id).innerHTML = new_contents ;

}

// change the title to a drop-down select menu for
// changing the title of the person...
function switch_to_select(title_id,title)
{
  
  // get contents of id
  title =  document.getElementById(title_id).innerHTML;

  // define the variables 
  mr_selected = '';
  mrs_selected = '';
  ms_selected = '';
  dr_selected = '';
  prof_selected = '';

  // switch statement to preselect one ...
  switch (title)
	{	
	  case 'Mr. ' : mr_selected = "selected='Selected'" ; break;
	  case 'Mrs. ' : mrs_selected = "selected='Selected'" ; break;
	  case 'Ms. ' : ms_selected = "selected='Selected'" ; break;
	  case 'Dr. ' : dr_selected = "selected='Selected'" ; break;
	  case 'Prof. ' : prof_selected = "selected='Selected'" ; break;
	  default: break;
	}

  // the new html in the title bit
  new_content = " <select name='title' id='title_id' >\
			<option value=''></option>\
			<option "+ mr_selected  +" value='Mr.'>Mr.</option>\
			<option "+ mrs_selected  +" value='Mrs.'>Mrs.</option>\
		        <option "+ ms_selected  +" value='Ms.'>Ms.</option>\
			<option "+ dr_selected  +" value='Dr.'>Dr.</option>\
			<option "+ prof_selected  +" value='Prof.'>Prof.</option>\
		  </select>";

   // switch it to the drop down menu
   document.getElementById(title_id).innerHTML = new_content ;

}


// change the title to a drop-down select menu for
// changing the title of the person...
function switch_to_title(title_id)
{
  
  // get contents of id
  title =  document.getElementById('title_id').value;

   // switch it to the drop down menu
   document.getElementById(title_id).innerHTML = title ;

}

// this is just a general mask element
// hide background and border
function mask_element(id)
{
	document.getElementById(id).style.border = '0px' ;
	document.getElementById(id).style.backgroundColor = '#FFFFCC';		
}

// this is a general unmask element 
// set background to normal white, show border
function unmask_element(id)
{
	document.getElementById(id).style.border = '1px inset #000' ;
	document.getElementById(id).style.backgroundColor = '#FFFFFF';		
}

// mask the elements of the recruiter 
// resume from the recruiter add form
function mask_recruiter_form(form_name)
{

	var frm = document.forms[form_name]; 

	// set array of elements
	el_array = new Array('experience','university_education','introduction','phone','email','name');

	//  set each element to it's form mask
	for ( i=0 ; i<5 ; i++ )
	{
		id = el_array[i];
	
		frm.elements[id].style.border = '1px inset #000' ;
		frm.elements[id].style.backgroundColor = '#FFFFFF';		
	}
		
}

// use this form to mask from
// a link... school_contact form
function mask_link_form(name,id)
{

	// get the tel id
	id_no = id.slice(7);
	tel_id = 'tel_id_'+id_no;
	organ_id = 'organ_id_'+id_no;
	contact_id = 'contact_id_'+id_no;
	notes_id = 'notes_id_'+id_no;

	// set array of elements
	el_array = new Array(tel_id, organ_id, contact_id, notes_id);

	//  set each element to it's form mask
	for ( i=0 ; i<4 ; i++ )
	{

		document.getElementById(el_array[i]).style.border = '1px inset #000' ;
		document.getElementById(el_array[i]).style.backgroundColor = '#FFFFFF';		
		
	}


	return false;

}

// use this form to mask from
// a link... school_contact form
function unmask_link_form(name,id)
{


	// get the tel id
	id_no = id.slice(7);
	tel_id = 'tel_id_'+id_no;
	organ_id = 'organ_id_'+id_no;
	contact_id = 'contact_id_'+id_no;
	notes_id = 'notes_id_'+id_no;

	// set array of elements
	el_array = new Array(tel_id, organ_id, contact_id, notes_id);

	//  set each element to it's form mask
	for ( i=0 ; i<4 ; i++ )
	{

		document.getElementById(el_array[i]).style.border = '0px' ;
		document.getElementById(el_array[i]).style.backgroundColor = '#FFFFCC';
		
	}


	return false;

}

// admin_marketing_stats ...
// use this mask for input links
function mask_form(name,form)
{	

	switch(name)
	{

		case "free_campaign" : 
					form.script.style.border = '1px inset #000';
					form.ad_copy.style.border = '1px inset #000';
					form.campaign_free_name.style.border = '1px inset #000';
					form.notes.style.border = '1px inset #000';
					form.frequency.style.border = '1px inset #000';
					break ;

		case "paid_campaign" : 
					form.script.style.border = '1px inset #000';
					form.notes.style.border = '1px inset #000';
					form.ad_copy.style.border = '1px inset #000';
					form.ad_page.style.border = '1px inset #000';
					form.ad_currency.style.border = '1px inset #000';
					form.cost_of_ad.style.border = '1px inset #000';
					form.campaign_paid_name.style.border = '1px inset #000';
					break;
		default: break;
	
	}

	return false ;
}

// switch tab views from control_panel
function tab_view(tab)
{

   switch(tab)
   {

	case ('intro') : 
		document.getElementById('reminder_box').style.display='block';
		document.getElementById('email_box').style.display='block';
		document.getElementById('teachers_box').style.display='none';
		document.getElementById('recruiter_box').style.display='none';
		document.getElementById('schools_box').style.display='none';
		document.getElementById('matches_box').style.display='block' ; 
		document.getElementById('leads_box').style.display='none' ;
		document.getElementById('introTab').style.backgroundColor='#fff'; 
		document.getElementById('teacherTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('jobsTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('matchesTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('allTab').style.backgroundColor='#eaeaea'; 
	 ; break;

	case ('teachers') : 
		document.getElementById('reminder_box').style.display='none';
		document.getElementById('email_box').style.display='none';
		document.getElementById('teachers_box').style.display='block';
		document.getElementById('recruiter_box').style.display='none';
		document.getElementById('schools_box').style.display='none';
		document.getElementById('matches_box').style.display='none' ; 
		document.getElementById('leads_box').style.display='none' ; 
 		document.getElementById('introTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('teacherTab').style.backgroundColor='#fff'; 
		document.getElementById('jobsTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('matchesTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('allTab').style.backgroundColor='#eaeaea'; 
	; break;

	case ('jobs') : 
		document.getElementById('reminder_box').style.display='none';
		document.getElementById('email_box').style.display='none';
		document.getElementById('teachers_box').style.display='none';
		document.getElementById('recruiter_box').style.display='block';
		document.getElementById('schools_box').style.display='block';
		document.getElementById('matches_box').style.display='none' ; 
		document.getElementById('leads_box').style.display='none' ; 
		document.getElementById('introTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('teacherTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('jobsTab').style.backgroundColor='#fff'; 
		document.getElementById('matchesTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('allTab').style.backgroundColor='#eaeaea'; 
        ; break;

	case ('matches') : 
		document.getElementById('reminder_box').style.display='none';
		document.getElementById('email_box').style.display='none';
		document.getElementById('teachers_box').style.display='none';
		document.getElementById('recruiter_box').style.display='none';
		document.getElementById('schools_box').style.display='none';
		document.getElementById('matches_box').style.display='none' ; 
		document.getElementById('leads_box').style.display='block' ; 
		document.getElementById('introTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('teacherTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('jobsTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('matchesTab').style.backgroundColor='#fff'; 
		document.getElementById('allTab').style.backgroundColor='#eaeaea'; 
	break;

	case ('all') : 
		document.getElementById('reminder_box').style.display='block';
		document.getElementById('email_box').style.display='block';
		document.getElementById('teachers_box').style.display='block';
		document.getElementById('recruiter_box').style.display='block';
		document.getElementById('schools_box').style.display='block';
		document.getElementById('matches_box').style.display='block' ; 
		document.getElementById('leads_box').style.display='block' ; 
		document.getElementById('introTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('teacherTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('jobsTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('matchesTab').style.backgroundColor='#eaeaea'; 
		document.getElementById('allTab').style.backgroundColor='#fff'; 
	break;

	default : ; break;

   }

   return 0;		

}

// take a rem_no and update it's sent status ...
function callback(tel_no)
{

	http = createRequestObject();	

	http.open('get', 'control_panel.php?action=callback&id='+tel_no,true);
	http.onreadystatechange = handleResponse;
    	http.send(null);

}

function email_history(id)
{

	http = createRequestObject();	

	http.open('get', 'control_panel.php?action=email_history&id='+id,true);
	http.onreadystatechange = handleResponse;
    	http.send(null);

}

// take a rem_no and update it's sent status ...
function kill_reminder(remno)
{

	http = createRequestObject();	

	http.open('get', 'control_panel.php?action=kill_reminder&id='+remno,true);
	http.onreadystatechange = handleResponse;
    	http.send(null);

}


// make a DOM function here to update the recruiter notes 
function recruiter_notes_update(form_name,val1,val2,val3,date_time,text)
{

	form_name_div = '';

	// get form div
	form_name_div = form_name + '_div' ;

	// get the form
	form_div = document.getElementById(form_name_div);

	// if the val3 isn't empty then it's the real val2
	if ( val3 != -1 ) { val2 = val3 ; }

	// make the new string 
	text = text.replace('val1',val1);
	text = text.replace('val2',val2);
	text = text.replace('date/time',date_time);
	
	// get the date
	date_now = ( new Date() ).getTime();

	month = date_now.getMonth();
	day = date_now.getDay();

	date_text = day+'-'+month ;

	// add on the prefix
	text = date_text + text + '<br />';		
	
	// enter the text ...
	new_entry.appendChild(document.createTextNode(text));
	
}

// take a rem_no and update it's sent status ...
function third_party_call_book()
{
	
	// get values from the third party call form
	teacher_id = document.getElementById('third_party_call_form').teacher_id;
	recruiter_jobs_id = document.getElementById('third_party_call_form').recruiter_job_id;
	school_jobs_id = document.getElementById('third_party_call_form').school_job_id;
	time = document.getElementById('third_party_call_form').time;
	user_reg_date = document.getElementById('third_party_call_form').user_reg_date;

	// let's try to fit this in a get request
	http.open('get', 'control_panel.php?teacher_id='+teacher_id+'&recruiter_jobs_id='+recruiter_jobs_id+'&school_jobs_id='+school_jobs_id+'&time='+time+'&user_reg_date='+user_reg_date,true);

	http.onreadystatechange = handleResponse;

    	http.send(null);

}


function createRequestObject() {

    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

// have an open http object here for anyone to use...
http = createRequestObject();

// controls get requests to the server
function sndReq(action,name){

	// make it's own object
	http = createRequestObject();	

   	http.open('get', 'control_panel.php?action='+action+'&id='+name,true);
	http.onreadystatechange = handleResponse;

    	http.send(null);
}

// this is triggered from the control_panel
// and basically refreshes the screen every x seconds
function sndTimer(){	

	// make it's own object
	http = createRequestObject();	

	http.open('get', 'control_panel.php?action=update_page&id='+user,true);
    	http.onreadystatechange = handleResponse;
        http.send(null);

}

function handleResponse() 
{

	if(http.readyState == 4)
	{

        	var response = http.responseText;

		// if( ( response.indexOf( '|' ) != -1 ) ) 
		// {
	        //    update = response.split('|');
        	//    document.getElementById(update[0]).innerHTML = update[1];
        	//}
		// match the organization search... 
		// from the school contact form

		if( ( response.substr(0,25) == 'GET_REQUEST_SCHOOL_SEARCH' ) )
		{

			// slice off the identifier..
			response = response.slice(25);

			document.getElementById('check_box').innerHTML = response ;

		}	
		else if( ( response.substr(0,26) == 'GET_REQUEST_CONTACT_SEARCH' ) )
		{
			// slice off the identifier..
			response = response.slice(26);
			document.getElementById('check_box').innerHTML = response;
		}	 		
		else if( ( response.indexOf('kill-reminder') != -1 ) )
    		{

			// find just reminder number
			response = response.slice(14);

			// add reminder text
			response = 'reminder_id_'+response ;			

			// kill the reminder here so that it doesn't show
			document.getElementById(response).style.display = 'none';
			
			return false;

		}else{

			try {

				json_val = eval( "(" + response + ")" );				

				// is it an email history ?
				if (json_val.email_history)
				{
					parse_history(json_val.email_history);	
				}

				// if it exists, then update it
				if ( json_val.emails_update ) { parse_emails(json_val.emails_update); }	
				if ( json_val.matches_update ) { parse_matches(json_val.matches_update); }
				if ( json_val.teachers_update ) { parse_teachers(json_val.teachers_update); }
				if ( json_val.recruiters_update ) { parse_recruiters(json_val.recruiters_update); }				
				if ( json_val.schools_update ) { parse_schools(json_val.schools_update); }
				if ( json_val.reminders_update ) { parse_reminder(json_val.reminders_update); }
				if ( json_val.leads_update && leads_flag == 0 ) { parse_leads(json_val.leads_update); }
				
				// chat_start();

			}
			catch (err) {

			}		

		}


	}


}

// let's put the leads flag here ...
leads_flag = 0;

// if a chat window isn't opened already opened, then open it
// TEST HERE TO SEE IF chat_client IS OPENED ALREADY
function chat_start()
{      

   // if the windows isn't opened, then open it
   if (!(open))
   {
        var open = window.open('w.php','chat_client','scrollbars=yes,menubar=no,resizable=yes,toolbar=no,location=no,status=no,left=20,top=20,width=480,height=270,resizable=0');
   }

}

// parse the teacher response into the web page
function parse_teachers(input)
{

    // store array of email elements
    new_arr = new Array();

    cols = new Array();

    // split string comes after every element in the reminder
    cols = input.split('_-_-');

    for (i=0;i<(cols.length - 1);i++)
    {

        // eval each array
	te_json = eval( "(" + cols[i] + ")" );

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	te_TR = document.createElement('tr');

	// row input like this ...

	// put number in here
	te_TR.className = '--'+te_json['teacher_id']+'--';

	// create the td
	te_select_pic_TD = document.createElement('td');
	te_select_pic_TD.style.textAlign = 'center';
	
	te_select_pic_A = document.createElement('a');
	te_select_pic_A.href = "/gallery.php?id="+te_json['teacher_id']+"&sid="+te_json['sid'];

	te_select_pic_IMG = document.createElement('img');
	te_select_pic_IMG.className = 'blue_folder_img';
	te_select_pic_IMG.alt = 'Look at Resume' ;
	te_select_pic_IMG.src = 'images/etk-icons/one-pixel.png';

	// build the first td
	te_select_pic_A.appendChild(te_select_pic_IMG);
	te_select_pic_TD.appendChild(te_select_pic_A);

	// create the candidate's name
	te_select_name_TD = document.createElement('td');

	te_select_name_A = document.createElement('a');
	te_select_name_A.href = "/gallery.php?id="+te_json['teacher_id']+"&sid="+te_json['sid'];
	te_select_name_A.appendChild(document.createTextNode(te_json['teachers_name']));

	// build the second td
	te_select_name_TD.appendChild(te_select_name_A);

	// build the city td
	te_city_TD = document.createElement('td');
	te_city_TD.appendChild(document.createTextNode(te_json['city_teachers']));

	// build the year td
	te_year_TD = document.createElement('td');
	te_year_TD.appendChild(document.createTextNode(te_json['birthyear']));

	// build the time td
	te_time_TD = document.createElement('td');

	// see if it has a red stirng
	old_time = te_json['from_time'];

	// see if it says 'red'
	if ( old_time.substring(0,3) == 'red' )
	{
		var new_time = '' ;
		te_json['from_time'] = old_time.replace('red','');

		te_time_TD.style.color = '#EE0000';	

	}

	time_values = document.createTextNode(te_json['from_time']+'-'+te_json['until_time']);	

	te_time_TD.appendChild(time_values);

	// creat status td
	te_status_TD = document.createElement('td');

	te_status_A = document.createElement('a');
	te_status_A.href = '#' ;
	te_status_A.onclick = snd_Teacher_Status_Handler(te_json['teacher_id'] );
	te_status_A.className = te_json['teacher_id'];
	te_status_A.appendChild(document.createTextNode(te_json['status']));

	// build the status td
	te_status_TD.appendChild(te_status_A);

	// build arrival td
	te_arrival_TD = document.createElement('td');
	te_arrival_TD.appendChild(document.createTextNode(te_json['arrival']));
	
	// create delete td
	te_delete_TD = document.createElement('td');
	te_delete_TD.style.textAlign = 'center';

	te_delete_A = document.createElement('a');
	te_delete_A.href='#';	
	te_delete_A.onclick = snd_Teacher_Delete_Handler(te_json['teacher_id']);

	te_delete_A.className = te_json['teacher_id'];
	
	te_delete_IMG = document.createElement('img');
	te_delete_IMG.className = 'delete_img' ;
	te_delete_IMG.alt = 'Delete Resume';
	te_delete_IMG.src = '/images/etk-icons/one-pixel.png';
	te_delete_A.appendChild(te_delete_IMG);	

	// build the delete td
	te_delete_TD.appendChild(te_delete_A);

	// build the date diff
	te_date_TD = document.createElement('td');
	te_date_TD.appendChild(document.createTextNode(te_json['date_diff']+' d ago'));

	// create the email td
	te_email_TD = document.createElement('td');
	
	te_email_A = document.createElement('a');
	te_email_A.color = te_json['mail_color'];
	te_email_A.href = "/form.php?candidate_email_form="+te_json['email']+"&name="+te_json['teacher_fn'] ;
	te_email_A.appendChild(document.createTextNode('Email'));
	
	// build email td
	te_email_TD.appendChild(te_email_A);
	
	// create phone td
	te_phone_TD = document.createElement('td');
	te_phone_TD.style.textAlign = 'center';
	
	te_phone_A = document.createElement('a');
	te_phone_A.href = te_json['phone_mode'];

	// get the phone number from the phone value
	var phone_mode = te_json['phone_mode'];

	te_phone_A.onclick = get_Phone_Mode(phone_mode,'phoneTeacher');

	te_phone_IMG = document.createElement('img');
	te_phone_IMG.className = 'phone_img';
	te_phone_IMG.src = '/images/etk-icons/one-pixel.png';
	te_phone_IMG.alt = 'Ring Candidate';

	te_phone_A.appendChild(te_phone_IMG);

	// build phone
	te_phone_TD.appendChild(te_phone_A);

	// append the tds
	te_TR.appendChild(te_select_pic_TD);
	te_TR.appendChild(te_select_name_TD);
	te_TR.appendChild(te_city_TD);
	te_TR.appendChild(te_year_TD);
	te_TR.appendChild(te_time_TD);
	te_TR.appendChild(te_status_TD);
	te_TR.appendChild(te_arrival_TD);	
	te_TR.appendChild(te_delete_TD);	
	te_TR.appendChild(te_date_TD);
	te_TR.appendChild(te_email_TD);
	te_TR.appendChild(te_phone_TD);			

	// build an array of TR elements .. should read 0,1,2,3 etc...
	new_arr[i] = te_TR ;

    }

    // append the tr element ...
    // clear the node first
    data_box = document.getElementById('teachers_data_box');

    clearNode(data_box);

    for (t=0;t<(new_arr.length);t++)
    {
	data_box.appendChild(new_arr[t]);
    }

}

// parse in the email history for that individual
function parse_history(input)
{
    document.getElementById('email_history').innerHTML = input;
}


// take in an array in sets of 4
// innerHTML is faster, to use proper DOM here will take 
// ages to write and rewrite these and interface_handler functions
function parse_emails(input)
{

    // store array of email elements
    new_arr = new Array();

    cols = new Array();

    // split into JSON encoded arrays
    cols = input.split('_-_-');

    for (i=0;i<(cols.length-1);i++)
    {

        // eval each array
	em_json = eval( "(" + cols[i] + ")" );

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	// document.getElementById('reminders_box').innerHTML = response[7] ;	
	em_TR = document.createElement('tr');

	// row input like this ...
	// email_id / email_user / email_name / email_subject_text / email_from

	// put number in here
	em_TR.className='email_'+em_json['email_id'];
	em_TR.id = 'email_'+em_json['email_id'];

	// create the td
	em_from_pic_TD = document.createElement('td');
	em_from_pic_TD.className='email_from';
	em_from_pic_TD.style.textAlign = 'left';

	em_from_pic_A = document.createElement('a');
	em_from_pic_A.href = "/form.php?candidate_email_form="+em_json['email_user']+"&name="+em_json['email_name']+"&email_id="+em_json['email_id'];

	em_from_pic_IMG = document.createElement('img');
	em_from_pic_IMG.alt='Reply to Mail';
	em_from_pic_IMG.src = '/images/etk-icons/one-pixel.png';
	em_from_pic_IMG.className = 'mail_reply_img';

	em_from_pic_A.appendChild(em_from_pic_IMG);

	// add the first icon col
	em_from_pic_TD.appendChild(em_from_pic_A);	

	// email text attributes
	em_from_text_TD = document.createElement('td');
	em_from_text_TD.className='email_from';

	// email text links
	em_from_text_A = document.createElement('a');

	em_from_text_A.alt='Email From';
	em_from_text_A.href="/form.php?candidate_email_form="+em_json['email_user']+"&name="+em_json['email_name']+"&email_id="+em_json['email_id'];
	em_from_text_A.appendChild(document.createTextNode(em_json['email_name']));

	// output the email node
	em_from_text_TD.appendChild(em_from_text_A);

	// build the subject line
	em_subject_TD = document.createElement('td');
	em_subject_TD.appendChild(document.createTextNode(em_json['email_subject_text']));

	// build the delete line
	em_delete_TD = document.createElement('td');
	em_delete_TD.style.textAlign = 'center';

	// build the delete link
	em_delete_A = document.createElement('a');

	em_delete_A.href='#';
	em_delete_A.onclick= snd_Delete_Email_Handler( em_json['email_id'], em_json['email_from'],em_json['user_reg_date'] ) ;

	// build link for delete line
	em_delete_IMG = document.createElement('img');
	em_delete_IMG.alt='Delete Mail';
	em_delete_IMG.border='0';
	em_delete_IMG.className = 'delete_img' ;
	em_delete_IMG.src = 'images/etk-icons/one-pixel.png';
	em_delete_A.appendChild(em_delete_IMG);

	// output delete element
	em_delete_TD.appendChild(em_delete_A);

	// append the tds
	em_TR.appendChild(em_from_pic_TD);
	em_TR.appendChild(em_from_text_TD);
	em_TR.appendChild(em_subject_TD);
	em_TR.appendChild(em_delete_TD);
	
	// build an array of TR elements .. should read 0,1,2,3 etc...
	new_arr[i] = em_TR ;

    }

    // append the tr element ...
    // clear the node first
    data_box = document.getElementById('email_data_box');

    clearNode(data_box);

    for (t=0;t<(new_arr.length);t++)
    {
	data_box.appendChild(new_arr[t]);
    }

}

// parse leads 
function parse_leads(input)
{

    // store array of email elements
    new_arr = new Array();

    cols = new Array();

    // split string comes after every element in the reminder
    cols = input.split('_-_-');

    for (i=0;i<(cols.length -1);i++)
    {

        // eval each array
	lead_json = eval( "(" + cols[i] + ")" );

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	lead1_TR = document.createElement('tr');

	// lead TR
	lead1_TR.style.backgroundColor = lead_json['color'] ;
	lead1_TR.className = lead_json['teacher_id']+'--'+lead_json['job_id']+' --'+lead_json['teacher_id']+'-- --'+lead_json['job_id']+'--';
	
	// create send pic TD
	lead1_send_pic_TD = document.createElement('td');
	lead1_send_pic_TD.rowSpan = '2';
	lead1_send_pic_TD.style.textAlign = 'center';	

	lead1_send_pic_A = document.createElement('a');
	lead1_send_pic_A.onclick = snd_Lead_Handler( lead_json['letter'], lead_json['teacher_id'], lead_json['job_id'], lead_json['type'] );

	lead1_send_pic_IMG = document.createElement('img');
	lead1_send_pic_IMG.className = lead_json['class_name'] ;
	lead1_send_pic_IMG.src = '/images/etk-icons/one-pixel.png';
	lead1_send_pic_IMG.alt = lead_json['type']+' Match';

	lead1_send_pic_A.appendChild(lead1_send_pic_IMG);

	// build pic td
	lead1_send_pic_TD.appendChild(lead1_send_pic_A);

	// create teacher name
	lead1_name_TD = document.createElement('td');
	
	lead1_name_A = document.createElement('a');
	lead1_name_A.href = '/gallery.php?id=' + lead_json['teacher_id'] ;
	lead1_name_A.appendChild(document.createTextNode(lead_json['teacher_name']));
	
	// build name TD	
	lead1_name_TD.appendChild(lead1_name_A);

	// create on TD (multi-send) ?
	lead1_on_TD = document.createElement('td');
	lead1_on_TD.rowSpan = '2';
	
	lead1_on_INPUT = document.createElement('input');
	lead1_on_INPUT.type = 'checkbox';
	lead1_on_INPUT.name = 'on';
	lead1_on_INPUT.value = lead_json['teacher_id']+'-'+lead_json['job_id'];
	lead1_on_INPUT.className = 'lead_'+lead_json['teacher_id'];

	// build on TD
	lead1_on_TD.appendChild(lead1_on_INPUT);

	// create notes
	lead1_notes_TD = document.createElement('td');
	lead1_notes_TD.rowSpan = '2';

	lead1_notes_INPUT = document.createElement('INPUT');
	lead1_notes_INPUT.type = 'text';
	lead1_notes_INPUT.className = lead_json['teacher_id']+'-'+lead_json['job_id'];
	lead1_notes_INPUT.size = '10';
	lead1_notes_INPUT.onclick = function () { leads_flag = 1; } ;

	// build notes TD
	lead1_notes_TD.appendChild(lead1_notes_INPUT);
	lead1_notes_TD.width = '200px';

	// build location TD
	lead1_location_TD = document.createElement('td');
	lead1_location_TD.appendChild(document.createTextNode(lead_json['teacher_location']));
		
	// build arrival TD
	lead1_arrival_TD = document.createElement('td');
	lead1_arrival_TD.appendChild(document.createTextNode(lead_json['teacher_arrival']));	

	// create failed TD
	lead1_fail_TD = document.createElement('td');
	lead1_fail_TD.rowSpan = '2';
	lead1_fail_TD.style.textAlign = 'center';
	
	lead1_fail_A = document.createElement('a');
	lead1_fail_A.href = '#';

	lead1_fail_A.onclick = snd_Lead_Fail_Handler( lead_json['letter'], lead_json['teacher_id'], lead_json['job_id'], lead_json['type'] );

	lead1_fail_IMG = document.createElement('img');
	lead1_fail_IMG.className = 'delete_img';
	lead1_fail_IMG.alt = 'Match Failed';
	lead1_fail_IMG.src = '/images/etk-icons/one-pixel.png';
	lead1_fail_A.appendChild(lead1_fail_IMG);	

	// build failed TD
	lead1_fail_TD.appendChild(lead1_fail_A);

	// build TR here
	lead1_TR.appendChild(lead1_send_pic_TD);
	lead1_TR.appendChild(lead1_name_TD);
	lead1_TR.appendChild(lead1_on_TD);
	lead1_TR.appendChild(lead1_notes_TD);
	lead1_TR.appendChild(lead1_location_TD);
	lead1_TR.appendChild(lead1_arrival_TD);
	lead1_TR.appendChild(lead1_fail_TD);

	// build lead2 TR 
	lead2_TR = document.createElement('tr');
	lead2_TR.style.backgroundColor = lead_json['color'];
	lead2_TR.className = lead_json['teacher_id']+'--'+lead_json['job_id']+' --'+lead_json['teacher_id']+'-- --'+lead_json['job_id']+'--';	

	// create name TD 	
	lead2_name_TD = document.createElement('td');

	lead2_name_A = document.createElement('a');
	lead2_name_A.href = lead_json['type']+'_gallery.php?id='+lead_json['job_id'] ;
	lead2_name_A.appendChild(document.createTextNode(lead_json['job_name']));

	// build name TD
	lead2_name_TD.appendChild(lead2_name_A);

	// build location TD
	lead2_location_TD = document.createElement('td');
	lead2_location_TD.appendChild(document.createTextNode(lead_json['job_location']));

	// build arrival TD
	lead2_arrival_TD = document.createElement('td');
	lead2_arrival_TD.appendChild(document.createTextNode(lead_json['job_arrival']));

	// build TR2
	lead2_TR.appendChild(lead2_name_TD);
	lead2_TR.appendChild(lead2_location_TD);
	lead2_TR.appendChild(lead2_arrival_TD);

	lead_doc_frag = document.createDocumentFragment();
	
	lead_doc_frag.appendChild(lead1_TR);
 	lead_doc_frag.appendChild(lead2_TR);

	new_arr[i] = lead_doc_frag ;

    }


    // append the tr element ...
    // clear the node first
    data_box = document.getElementById('leads_data_box');

    clearNode(data_box);

    for (t=0;t<(new_arr.length);t++)
    {
	data_box.appendChild(new_arr[t]);
    }

}

// parse matches input
function parse_matches(input)
{

    // store array of email elements
    new_arr = new Array();

    cols = new Array();

    // split string comes after every element in the reminder
    cols = input.split('_-_-');

    for (i=0;i<(cols.length -1);i++)
    {

        // eval each array
	mat_json = eval( "(" + cols[i] + ")" );

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	// document.getElementById('reminders_box').innerHTML = response[7] ;	
	mat1_TR = document.createElement('tr');

	// row input like this ...
	// email_id / email_user / email_name / email_subject_text / email_from

	// put number in here
	mat1_TR.className= mat_json['teacher_id']+'---'+mat_json['job_id']+' --'+mat_json['teacher_id']+'-- --'+mat_json['job_id']+'-- matches_border';
	mat1_TR.style.backgroundColor = mat_json['job_color'];

	// create success pic
	mat_suc_TD = document.createElement('td');
	mat_suc_TD.rowSpan = '2';
	mat_suc_TD.style.textAlign = 'center';

	mat_suc_A = document.createElement('a');
	mat_suc_A.href = '#';

	mat_suc_A.onclick = snd_Match_Success_Handler(mat_json['teacher_id'],mat_json['job_id'],mat_json['table_id']);
	
	mat_suc_IMG = document.createElement('img');
	mat_suc_IMG.className = 'agreed_img' ;
	mat_suc_IMG.src = '/images/etk-icons/one-pixel.png';
	mat_suc_IMG.alt = 'Match Completed';
	mat_suc_A.appendChild(mat_suc_IMG);

	// build success pic
	mat_suc_TD.appendChild(mat_suc_A);

	// create teacher name
	mat_name_TD = document.createElement('td');

	mat_name_A = document.createElement('a');
	mat_name_A.href = "/gallery.php?id="+mat_json['teacher_id'];
	mat_name_A.appendChild(document.createTextNode(mat_json['teacher_name']));

	// build mat_name
	mat_name_TD.appendChild(mat_name_A);

	// build arrival
	mat_arr_TD = document.createElement('td');
	mat_arr_TD.appendChild(document.createTextNode(mat_json['arrival']));	

	// build teacher notes
	mat_note_TD = document.createElement('td');
	mat_note_TD.appendChild(document.createTextNode(mat_json['teacher_notes']));

	// create fail
	mat_fail_TD = document.createElement('td');
	mat_fail_TD.rowSpan = '2' ;
	mat_fail_TD.style.textAlign = 'center';
	
	mat_fail_INPUT = document.createElement('input');
	mat_fail_INPUT.type = 'radio';
	mat_fail_INPUT.name = 'failed_mail';
	mat_fail_INPUT.className = mat_json['teacher_id']+'---'+mat_json['job_id'];

	// build fail
	mat_fail_TD.appendChild(mat_fail_INPUT);

	// create fail pic 
	mat_fail_pic_TD = document.createElement('td');
	mat_fail_pic_TD.rowSpan = '2';
	mat_fail_pic_TD.style.textAlign ='center';
	
	mat_fail_pic_A = document.createElement('a');
	mat_fail_pic_A.href = '#';
	mat_fail_pic_A.onclick = snd_Match_Fail_Handler(mat_json['teacher_id'],mat_json['job_id'],mat_json['table_id']);

	mat_fail_pic_IMG = document.createElement('img');
	mat_fail_pic_IMG.className = 'delete_img';
	mat_fail_pic_IMG.src = '/images/etk-icons/one-pixel.png';
	mat_fail_pic_IMG.alt = 'Match Failed';
	mat_fail_pic_A.appendChild(mat_fail_pic_IMG);	

	// build fail pic
	mat_fail_pic_TD.appendChild(mat_fail_pic_A);

	// build the first row
	mat1_TR.appendChild(mat_suc_TD);	
	mat1_TR.appendChild(mat_name_TD);
	mat1_TR.appendChild(mat_arr_TD);
	mat1_TR.appendChild(mat_note_TD);
	mat1_TR.appendChild(mat_fail_TD);
	mat1_TR.appendChild(mat_fail_pic_TD);					
	
	// build 
	mat2_TR = document.createElement('tr');
	mat2_TR.style.backgroundColor = mat_json['job_color'];
	mat2_TR.className = mat_json['teacher_id']+'---'+mat_json['job_id']+'  '+mat_json['teacher_id']+'--'+' --'+mat_json['job_id']+'--'  ;	
	
	// create job
	mat_job_TD = document.createElement('td');
	
	mat_job_A = document.createElement('a');
	mat_job_A.href = '/'+mat_json['which_page']+'.php?id='+mat_json['job_id'];
	mat_job_A.appendChild(document.createTextNode(mat_json['job_name']));
	
	// build dates 
	mat1_arr_TD = document.createElement('td');
	
	// build job
	mat_job_TD.appendChild(mat_job_A);

	// build notes
	mat_notes_TD = document.createElement('td');
	mat_notes_TD.appendChild(document.createTextNode(mat_json['job_notes']));	

	// append the tds
	mat2_TR.appendChild(mat_job_TD);
	mat2_TR.appendChild(mat1_arr_TD);
	mat2_TR.appendChild(mat_notes_TD);

	
	// create a document fragment
	mat_doc_frag = document.createDocumentFragment();
	
	mat_doc_frag.appendChild(mat1_TR);
	mat_doc_frag.appendChild(mat2_TR);

	new_arr[i] = mat_doc_frag ;

    }

    // append the tr element ...
    // clear the node first
    data_box = document.getElementById('matches_data_box');

    clearNode(data_box);

    for (t=0;t<(new_arr.length);t++)
    {
	data_box.appendChild(new_arr[t]);
    }


}


// parse the recruiters XHR input
function parse_recruiters(input)
{

    // store array of email elements
    new_arr = new Array();

    cols = new Array();

    // split string comes after every element in the reminder
    cols = input.split('_-_-');

    for (i=0;i<(cols.length - 1);i++)
    {

	// parse the json objectr
	re_json = eval( "(" + cols[i] + ")" );	

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	// document.getElementById('reminders_box').innerHTML = response[7] ;	
	rec_TR = document.createElement('tr');

	// put number in here
	rec_TR.className='--'+re_json['recruiter_jobs_id']+'--';

	// create the td
	rec_select_pic_TD = document.createElement('td');
	rec_select_pic_TD.style.textAlign = 'center';

	rec_select_pic_A = document.createElement('a');
	rec_select_pic_A.href="/recruiter_gallery.php?id="+re_json['recruiter_jobs_id']+"&sid="+re_json['sid'];

	rec_select_pic_IMG = document.createElement('img');
	rec_select_pic_IMG.className = 'blue_folder_img';
	rec_select_pic_IMG.alt='Recruiter Details';
	rec_select_pic_IMG.src='/images/etk-icons/one-pixel.png';
	rec_select_pic_A.appendChild(rec_select_pic_IMG);
	
	// build pic td
	rec_select_pic_TD.appendChild(rec_select_pic_A);

	// create the school details
	rec_school_TD = document.createElement('td');
	
	rec_school_A = document.createElement('a');
	rec_school_A.href = "/recruiter_gallery.php?id="+re_json['recruiter_jobs_id']+"&sid="+re_json['sid'];
	rec_school_A.appendChild(document.createTextNode(re_json['school']));

	// build the school details
	rec_school_TD.appendChild(rec_school_A);

	// build the city
	rec_city_TD = document.createElement('td');
	rec_city_TD.appendChild(document.createTextNode(re_json['city']));

	// build the arrival
	rec_arr_TD = document.createElement('td');
	rec_arr_TD.appendChild(document.createTextNode(re_json['arrival']));
	
	// create the number teachers
	rec_num_TD = document.createElement('td');
	
	rec_num_SPAN = document.createElement('span');
	rec_num_SPAN.className = 'number_teachers';
	rec_num_SPAN.appendChild(document.createTextNode(re_json['number_teachers']));

	// build number teachers
	rec_num_TD.appendChild(rec_num_SPAN);

	// create delete rec
	rec_del_TD = document.createElement('td');
	rec_del_TD.style.textAlign = 'center';
	
	rec_del_A = document.createElement('a');
	rec_del_A.href= '#';

	rec_del_A.onclick = snd_Recruiter_Delete_Handler(re_json['recruiter_jobs_id']);

	rec_del_A.id = 'REMOVEME' + re_json['recruiter_jobs_id'];
	
	rec_del_IMG = document.createElement('img');
	rec_del_IMG.className = 'delete_img';
	rec_del_IMG.src = '/images/etk-icons/one-pixel.png';
	rec_del_IMG.alt = 'Delete Recruiter';
	rec_del_A.appendChild(rec_del_IMG);

	// build delte rec
	rec_del_TD.appendChild(rec_del_A);
	
	// creat email 
	rec_email_TD = document.createElement('td');
	
	rec_email_A = document.createElement('a');
	rec_email_A.href = "/form.php?candidate_email_form="+re_json['email']+"&name="+re_json['contact'];
	rec_email_A.appendChild(document.createTextNode('Email'));	

	// build email
	rec_email_TD.appendChild(rec_email_A);

	// create phone
	rec_phone_TD = document.createElement('td');
	rec_phone_TD.style.textAlign = 'center';
	
	rec_phone_A = document.createElement('a');
	rec_phone_A.href='#';
	
	// get the phone number from the phone value
	var phone_mode = re_json['phone_mode'];

	rec_phone_A.onclick = get_Phone_Mode(phone_mode,'phoneRecruiter');

	rec_phone_IMG = document.createElement('img');
	rec_phone_IMG.className = 'phone_img';
	rec_phone_IMG.src = '/images/etk-icons/one-pixel.png';
	rec_phone_IMG.alt = 'Phone Recruiter';

	rec_phone_A.appendChild(rec_phone_IMG);

	// build phone
	rec_phone_TD.appendChild(rec_phone_A);

	// append the tds
	rec_TR.appendChild(rec_select_pic_TD);
	rec_TR.appendChild(rec_school_TD);
	rec_TR.appendChild(rec_city_TD);
	rec_TR.appendChild(rec_arr_TD);
	rec_TR.appendChild(rec_num_TD);
	rec_TR.appendChild(rec_del_TD);
	rec_TR.appendChild(rec_email_TD);
	rec_TR.appendChild(rec_phone_TD);
	

	// build an array of TR elements .. should read 0,1,2,3 etc...
	new_arr[i] = rec_TR ;

    }

    // append the tr element ...
    // clear the node first
    data_box = document.getElementById('recruiters_data_box');

    clearNode(data_box);

    for (t=0;t<(new_arr.length);t++)
    {
	data_box.appendChild(new_arr[t]);
    }

}


// parse the recruiters XHR input
function parse_schools(input)
{

    // store array of email elements
    new_arr = new Array();

    cols = new Array();

    // split string comes after every element in the reminder
    cols = input.split('_-_-');

    for (i=0;i<(cols.length - 1);i++)
    {

        // eval each array
	sch_json = eval( "(" + cols[i] + ")" );

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	// document.getElementById('reminders_box').innerHTML = response[7] ;	
	sch_TR = document.createElement('tr');

	// put number in here
	sch_TR.className='--'+sch_json['school_jobs_id']+'--';

	// create the td
	sch_select_pic_TD = document.createElement('td');
	sch_select_pic_TD.style.textAlign = 'center';

	sch_select_pic_A = document.createElement('a');
	sch_select_pic_A.href="/school_gallery.php?id="+sch_json['school_jobs_id']+"&sid="+sch_json['sid'];

	sch_select_pic_IMG = document.createElement('img');
	sch_select_pic_IMG.className = 'blue_folder_img';
	sch_select_pic_IMG.src = '/images/etk-icons/one-pixel.png';
	sch_select_pic_IMG.alt='School Details';
	sch_select_pic_A.appendChild(sch_select_pic_IMG);
	
	// build pic td
	sch_select_pic_TD.appendChild(sch_select_pic_A);

	// create the school details
	sch_school_TD = document.createElement('td');
	
	sch_school_A = document.createElement('a');
	sch_school_A.href = "/school_gallery.php?id="+sch_json['school_jobs_id']+"&sid="+sch_json['sid'];
	sch_school_A.appendChild(document.createTextNode(sch_json['school']));

	// build the school details
	sch_school_TD.appendChild(sch_school_A);

	// build the city
	sch_city_TD = document.createElement('td');
	sch_city_TD.appendChild(document.createTextNode(sch_json['city_schools']));

	// build the arrival
	sch_arr_TD = document.createElement('td');
	sch_arr_TD.appendChild(document.createTextNode(sch_json['arrival']));
	
	// create the number teachers
	sch_num_TD = document.createElement('td');
	
	sch_num_SPAN = document.createElement('span');
	sch_num_SPAN.className = 'number_teachers';
	sch_num_SPAN.appendChild(document.createTextNode(sch_json['number_teachers']));

	// build number teachers
	sch_num_TD.appendChild(sch_num_SPAN);

	// create delete sch
	sch_del_TD = document.createElement('td');
	sch_del_TD.style.textAlign = 'center';
	
	sch_del_A = document.createElement('a');
	sch_del_A.href= '#';
	sch_del_A.onclick = snd_School_Delete_Handler(sch_json['school_jobs_id']);

	sch_del_A.id = 'REMOVEME' + sch_json['school_jobs_id'] ;
	
	sch_del_IMG = document.createElement('img');
	sch_del_IMG.className = 'delete_img';
	sch_del_IMG.src = '/images/etk-icons/one-pixel.png';
	sch_del_IMG.alt = 'Delete School';
	sch_del_A.appendChild(sch_del_IMG);

	// build delte rec
	sch_del_TD.appendChild(sch_del_A);
	
	// creat email 
	sch_email_TD = document.createElement('td');
	
	sch_email_A = document.createElement('a');
	sch_email_A.href = "/form.php?candidate_email_form="+sch_json['email']+"&name="+sch_json['contact'];
	sch_email_A.appendChild(document.createTextNode('Email'));	

	// build email
	sch_email_TD.appendChild(sch_email_A);

	// create phone
	sch_phone_TD = document.createElement('td');
	sch_phone_TD.style.textAlign = 'center';
	
	sch_phone_A = document.createElement('a');
	sch_phone_A.href=sch_json['phone_mode'];
	
	// get the phone number from the phone value
	var phone_mode = sch_json['phone_mode'];

	sch_phone_A.onclick = get_Phone_Mode(phone_mode,'phoneSchool');

	sch_phone_IMG = document.createElement('img');
	sch_phone_IMG.className = 'phone_img';
	sch_phone_IMG.src = '/images/etk-icons/one-pixel.png';
	sch_phone_IMG.alt = 'Phone School';
	sch_phone_A.appendChild(sch_phone_IMG);

	// build phone
	sch_phone_TD.appendChild(sch_phone_A);

	// append the tds
	sch_TR.appendChild(sch_select_pic_TD);
	sch_TR.appendChild(sch_school_TD);
	sch_TR.appendChild(sch_city_TD);
	sch_TR.appendChild(sch_arr_TD);
	sch_TR.appendChild(sch_num_TD);
	sch_TR.appendChild(sch_del_TD);
	sch_TR.appendChild(sch_email_TD);
	sch_TR.appendChild(sch_phone_TD);
	

	// build an array of TR elements .. should read 0,1,2,3 etc...
	new_arr[i] = sch_TR ;

    }

    // append the tr element ...
    // clear the node first
    data_box = document.getElementById('schools_data_box');

    clearNode(data_box);

    for (t=0;t<(new_arr.length);t++)
    {
	data_box.appendChild(new_arr[t]);
    }

}

// take in an array in sets of 4
function parse_reminder(reminder)
{

    // store array of reminder elements
    rem_arr = new Array();
    reminder_arr = new Array();

    // store non-json reminder string
    reminder_string = '';

    cols = new Array();

    // get non-json'd code
    reminder_arr  = eval( "(" + reminder + ")" );

    // take out the reminder string
    reminder_string = reminder_arr['reminder_string']; 

    // split string comes after every element in the reminder
    cols = reminder_string.split('{SPLIT_STRING_MARKER}');

    for (i=0;i<(cols.length);i+=4)
    {

	// we have problems inserting rows here for each diff reminder
	// we're going to use DOM instead so we can enter <tr>s...	
	// document.getElementById('reminders_box').innerHTML = response[7] ;	
	rem_TR = document.createElement('tr');

	// go to remno first
	z = i +3 ;

	// give this a unique id so we can delete it
	rem_id='reminder_id_'+cols[z];

	// try to stop any blank id's from coming out the same
	// here and thus messing up the validation...
	if ( !cols[z] || cols[z] == '' )
	{

		rem_id = rem_id + Math.random();
	}

	rem_TR.id=rem_id;

	// create the tds
	rem_text_TD = document.createElement('td');
	rem_text_TD.align='left';
	rem_text_TD.width='20%';

	// reminder time attributes
	rem_time_TD = document.createElement('td');
	rem_time_TD.align='left';
	rem_time_TD.setAttribute('width','60%');

	// reminder sms attributes
	rem_sms_TD = document.createElement('td');
	rem_sms_TD.align='left';
	rem_sms_TD.width='10%';

	// delete is handled seperately 
	rem_del_TD = document.createElement('td');
	rem_del_TD.width='10%';
	rem_del_TD.style.textAlign = 'center';

	// insert the text elements
	rem_text_TD.appendChild(document.createTextNode(': '+cols[(i)]));
	rem_time_TD.appendChild(document.createTextNode(cols[(i+1)]));
	rem_sms_TD.appendChild(document.createTextNode(cols[(i+2)]));

	if ( cols[i] != "" )
	{

		// create link and add properties
		rem_del_A = document.createElement('a');
		rem_del_A.href='#';

		rem_del_A.onclick= function () { kill_reminder( cols[i+3] ); return false ;  }
	
		rem_del_IMG = document.createElement('img');
		rem_del_IMG.className = 'delete_img';
		rem_del_IMG.alt = 'Delete Image';
		rem_del_IMG.src='/images/etk-icons/one-pixel.png';
		rem_del_A.appendChild(rem_del_IMG);

		rem_del_TD.appendChild(rem_del_A);	

	}

	// append the tds
	rem_TR.appendChild(rem_text_TD);
	rem_TR.appendChild(rem_time_TD);
	rem_TR.appendChild(rem_sms_TD);
	rem_TR.appendChild(rem_del_TD);

	// set the info-table class
	rem_TR.className='info_table';
	
	// build an array of TR elements .. should read 0,1,2,3 etc...
	var k = i / 4 ;
	rem_arr[k] = rem_TR ;

    }

    // append the tr element ...
    // clear the node first
    rem_box = document.getElementById('reminders_box');

    clearNode(rem_box);

    for (t=0;t<(rem_arr.length - 1);t++)
    {
	rem_box.appendChild(rem_arr[t]);
    }


}

// clear a node
function clearNode(node)
{ 
	while(node.firstChild) node.removeChild( node.firstChild ) 
}

// check to see if the save email text is empty. 
// if it's empty then we aren't saving anything.
function save_reply(email_id,teacher_email,candidate_name,user_id)
{
	sndEmail(teacher_email,candidate_name);
	del_Email(email_id,user_id); 
}

// send email ?
function sndEmail(mail,candidate_name){
        
	var text=document.getElementById('email_text').value;
	var str = 'email_save_text=1&text='+text+'&mail='+mail+'&candidate_name='+candidate_name;	
        var url = '/control_panel.php';

   	http.open("POST",url,true);
	http.onreadystatechange = handleResponse;
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

        http.send(str);

}

// store all the users actions in a cookie and then pass it off
// phoneTeacher -> phone
// delTeacher -> teacher_id
// phoneRecruiter -> phone
// delRecruiter -> recruiter_jobs_id
// phoneSchool -> phone
// delSchool -> school_jobs_id
// successCompleted -> teacher_id--job_id
// successFailed -> teacher_id--job_id
// rMatchSent -> teacher_id--recruiter_jobs_id 
// rMatchFailed -> teacher_id--recruiter_jobs_id
// sMatchSent -> teacher_id--school_jobs_id
// sMatchFailed -> teacher_id--school_jobs_id

function action_monitor(action,data)
{

	// user special ID, defined on page
	// var user = {USER_REGDATE} ;

	// refresh time interval for cookie sending
	// milliseconds ... 9000000 = 15min
	var time_interval = 10000;

	// initialize cookie_str
	var cookie_str = '';	

	// find the time ... used to set cookies etc.. 
	var now_time = ( new Date() ).getTime();

	// send this time
	var send_time = Math.floor(now_time/1000);

	// establish the cookie delimiter
 	var delimiter = '<-ETK->'; 

	// establish users timezone here {USER_TIMEZONE}
	// establish this onpage..

	// establish cookie_name
	var cookie_name = 'etk_action_monitor' ;

	// establish new action string
	var new_action = user + delimiter + send_time + delimiter + timezone + delimiter + action + delimiter + data + delimiter ;

	// get cookie value
	if ( !getCookie(cookie_name) )
	{
		// pad first value ...
		// set exiry for 8 days ...
		now_time = now_time + delimiter ;
		setCookie(cookie_name,now_time,8);
	}

	cookie_str = getCookie(cookie_name) ; 

	// parse the cookie, if it has been 
	// alive for > 15mins add, send and clean 
	var element = cookie_str.split(delimiter);

	// you are going to set this value to {ACTION_MONITOR_INTERVAL}
	// this is the cookie filter and send mechanism
	// remember this is milliseconds - look to pull timestamp later

	if ( (now_time - element[0]) > time_interval ) 
	{		        

		// DO NOT put a var here ... causes error
		http_action = createRequestObject();

	        var url = '/control_panel.php';
		var cookie_str = cookie_str + new_action ;	

		// make the string into a POST variable,
		cookie_str = 'action_monitor=' + cookie_str ;

   		http_action.open("POST",url,true);
		http_action.onreadystatechange = handleResponse;
	       	http_action.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

		http_action.send(cookie_str);
		
		// pad first value
		now_time = now_time + delimiter;

		setCookie(cookie_name,now_time);

	}else{

		// I prefer to set cookie value here
		var cookie_str = cookie_str + new_action ;	
		setCookie(cookie_name,cookie_str);

	}

}

// take in the phone number and return the onclick event
var get_Phone_Mode = function ( phone_number, action_label )
{

	return function () 
	{
	
	  // adjust for skype mode
   	  if ( phone_number.indexOf('sip:') != -1 )
	  {

	    phone_number = phone_number.replace('sip:','');	  

	    return "action_monitor('"+action_label+"',"+phone_number+");"; 	

	  }
	  else if ( phone_number.indexOf('skype:') != -1 ) 
	  {

	    var phone_number = phone_number.replace('skype:','');
	    var phone_number = phone_number.replace('?call','');

	    return "action_monitor('"+action_label+"',"+phone_number+");"; 	

	  }
	 else
	 {	
	    // this is a callback function
	    return "callback("+phone_number+");action_monitor('"+action_label+"',"+phone_number+");return false;" ;

	 }

       }

}

// make a function here to form a closeur inside
// the for loop ...
var snd_Delete_Email_Handler = function( email_id, email_from, user_id ) 
{ 
 	return function () 
	{ 
		del_Email(email_id,user_id); 
		action_monitor('delEmail',email_from);
		return false;
	} ; 

};

// kill the closeur ...
var snd_Lead_Fail_Handler = function( letter, teacher_id, job_id, type ) 
{ 
 	return function () 
	{ 
		sndLead('sent_match_delete_'+type,teacher_id,job_id);
		action_monitor(letter+'MatchFailed',teacher_id+'--'+job_id);
		return false;
	} ; 

};


// kill the closeur ...
var snd_Lead_Handler = function( letter, teacher_id, job_id, type ) 
{ 
 	return function () 
	{ 
		sndLead('sent_match_'+letter,teacher_id,job_id);
		action_monitor(type+'MatchSent',teacher_id+'--'+job_id);
		return false ;
	} ; 

};


// kill the closeur ...
var snd_Match_Success_Handler = function( teacher_id, job_id, table_id ) 
{ 
 	return function () 
	{ 
		sndMatch('success_match_completed',teacher_id,job_id,table_id);
		action_monitor('successCompleted',teacher_id+"--"+job_id);
		return false ;
	} ; 

};


// kill the closeur ...
var snd_Match_Fail_Handler = function( teacher_id, job_id, table_id ) 
{ 
 	return function () 
	{ 
		action_monitor('successFailed',teacher_id+"--"+job_id);
		sndMatch('success_match_failed',teacher_id,job_id,table_id)
		return false ;
	} ; 

};


// kill the closeur ...
var snd_Recruiter_Delete_Handler = function( recruiter_jobs_id ) 
{ 
 	return function () 
	{ 
		action_monitor('delRecruiter',recruiter_jobs_id); 
		snd_del('recruiter_dete',recruiter_jobs_id,'--'+recruiter_jobs_id+'--');
		return false ;
	} ; 

};

// kill the closeur ...
var snd_School_Delete_Handler = function( school_jobs_id ) 
{ 
 	return function () 
	{ 
		action_monitor('delSchool',school_jobs_id); 
		snd_del('school_dete',school_jobs_id,'--'+school_jobs_id+'--');
		return false ;
	} ; 

};

// kill the closeur ...
var snd_Teacher_Status_Handler = function( teacher_id ) 
{ 
 	return function () 
	{ 
		sndReq('teacher_chsts',teacher_id);
		return false ;
	} ; 

};

// kill the closeur ...
var snd_Teacher_Delete_Handler = function( teacher_id ) 
{ 
 	return function () 
	{ 
		snd_del('teacher_dete',teacher_id,'--'+teacher_id+'--');
		action_monitor('delTeacher',teacher_id);
		return false ;
	} ; 

};

// get rid of the email one by one ...
function del_Email(email_id,user_id)
{
	// instead of sndReq - use a post request with user_id
	// sndReq('email_dete',email_id);
	var user_id = user_id ;

	http = createRequestObject();	

	http.open('get', 'control_panel.php?action=email_dete&email_id='+email_id+'&user_id='+user_id,true);
	http.onreadystatechange = handleResponse;
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
    	http.send(null);

	// disappear the box
	var dis_text = 'email_' + email_id ;

	// this used to be getElementsByClass();
	// DOM doesn't seem to like it that way ...
	var ret_val = document.getElementById(dis_text);
	ret_val.style.display = 'none';	

}

// match are matches that someone has agreed or not agreed with...
function sndMatch(action,teacher_id,job_id,table_id){       

	var job_id = job_id ;
        var str = action+'=1'+'&teacher_id='+teacher_id+'&job_id='+job_id+'&table_id='+table_id;	

        var url = '/control_panel.php';

	// send off post request ...
   	http.open("POST",url,true);
	http.onreadystatechange = handleResponse;
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

        http.send(str);
	
	// disappear the box
	var dis_text = teacher_id+'---'+job_id;

	var ret_val = new Array();
	var ret_val = getElementsByClass(dis_text);
	
	for( var i = 0 ; i < ret_val.length ; i++)
	{
		ret_val[i].style.display='none';
	}	

}

// leads are for the computer matches that haven't been touched
// by humans yet ...
function sndLead(action,teacher_id,job_id)
{

	// see if we can send more than one mail at a time

	// firstly get the checkboxes by name
	var check_name = 'lead_' + teacher_id ;

	// get an array of matches for the teacher ...
        var lead_arr=getElementsByClass(check_name) ;	

	// find out which of the teacher checkboxes are set
	//for (i=0;i<lead_arr.length;i++)
	//{

		//if (lead_arr[i].checked == 1 )
		//{
		//	alert('true');
		//}
		//else {
		//	alert('false');
		//}

	//} 

	// DO NOT put a var here... it will cause an error
        http = createRequestObject();

	var job_type_id = '';
        var match_id = teacher_id+'-'+job_id ;

	// work around for the bottleneck at the str variable...
	if ( action == 'sent_match_delete_recruiter' )
	{
		job_type_id = 'recruiter_jobs_id';
		action = 'sent_match_delete';
	}
	else if ( action == 'sent_match_delete_school' ) 
	{
		job_type_id = 'school_jobs_id';
		action = 'sent_match_delete';		
	}
	else if ( action == 'sent_match_recruiter' )
	{	
		job_type_id = 'recruiter_jobs_id';
		
 	}else if ( action == 'sent_match_school' ) 
	{
		job_type_id = 'school_jobs_id' ;
	}

	// find the teacher match notes
        var teacher_match_arr=getElementsByClass(match_id) ;	
	var teacher_match_notes = teacher_match_arr[0].value;

	str = action+'=1'+'&teacher_id='+teacher_id+'&'+job_type_id+'='+job_id+'&teacher_match_notes='+teacher_match_notes;	
        url = '/control_panel.php';
	
	// send off the post request
   	http.open("POST",url,true);
	http.onreadystatechange = handleResponse;
        http.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

	http.send(str);

	// disappear the box
	dis_text = teacher_id + '--' + job_id;

	ret_val = new Array();
	ret_val = getElementsByClass(dis_text);
	
	for( var i = 0 ; i < ret_val.length ; i++)
	{
		ret_val[i].style.display='none';
	}	

}

function dis_box(box)
{

   box.cancelBubble = true; 

   try
   {
        if ( document.getElementById(box).style.display == 'none' )
	{
    		document.getElementById(box).style.display = 'block';
	}
	else{
		document.getElementById(box).style.display = 'none';
	}
   }catch(c){
   }

   return false;

}

// function to execute both a sndReq and a dis_box 
function snd_del(action,id,id2)
{
	sndReq(action,id);
	dis_box_class(id2);
	
}

function dis_box_class(id){

	// disappear the box
	var id ;

	var ret_val = new Array();
	var ret_val = getElementsByClass(id);
	
	for( var i = 0 ; i < ret_val.length ; i++)
	{
		ret_val[i].style.display='none';
	}	

}
// this seems to work 
function telephone()
{
	var number = document.getElementById('telephone').value;
	var link = "sip:" + number ;
	document.getElementById('telephone_link').href=link ;		

}
