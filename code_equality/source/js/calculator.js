
 function calculate(element,currency)
 {
	
	if (document.getElementById('salary')) {
		var salary = document.getElementById('salary').value;
		document.getElementById('salary_hidden').value= salary ;
	}else{
		var salary = document.getElementById('salary_hidden').value;
	}

	var selected_usd = "";
	var selected_gbp = "";
	var selected_can = "";
	var selected_nzd = "";
	var selected_eir = "";
	var selected_zar = "";
	var selected_aud = "";

	// rates
	var pension_rate = 0.045 ;
	var medical_rate = 0.037 ;
	var tax_rate = 0.02 ;
	
	// gross
	var gross_pension = salary * 0.045 ;
	var gross_medical = salary * 0.037 ;
	var gross_tax = ( salary - 600000 ) * 0.02 ; 
	var gross_salary= salary - gross_pension - gross_medical - gross_tax ;

	// living costs
	var entertainment = 250000 ;
	var light_and_heat = 80000;

	var balance ;
	var currency_amount = '0';
	
	// 0 -> 1st state 
	// initialize them if they aren't there ...
	if ( !(document.getElementById('china')) ){var china = 0;}
	if ( !(document.getElementById('thailand')) ){var thailand = 0;}
	if ( !(document.getElementById('japan')) ){var japan = 0;}
	if ( !(document.getElementById('gym')) ){var gym = 0;}
	if ( !(document.getElementById('martial_arts')) ){var martial_arts = 0;}
	if ( !(document.getElementById('korean_lessons')) ){var korean_lessons = 0;}

	// if we are processing an element then toggle it's value ... and set it
	switch(element)
	{
		case "china": if ( document.getElementById('china').value == 0 ) { china=80000;document.getElementById('china').value = china; thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ;}else{ china=0; document.getElementById('china').value = china; thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ;  } break;
		case "thailand": if ( document.getElementById('thailand').value == 0 ) { thailand=60000;document.getElementById('thailand').value = thailand;  china = document.getElementById('china').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ;  }else{ thailand=0; document.getElementById('thailand').value = thailand; china = document.getElementById('china').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ; } break;
		case "japan": if ( document.getElementById('japan').value == 0 ) { japan=30000;document.getElementById('japan').value = japan; thailand = document.getElementById('thailand').value ;china = document.getElementById('china').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ; }else{ japan=0; document.getElementById('japan').value = japan;  thailand = document.getElementById('thailand').value ;china = document.getElementById('china').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ;  } break;
		case "gym": if ( document.getElementById('gym').value == 0 ) { gym=50000;document.getElementById('gym').value = gym; thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; china = document.getElementById('china').value ; }else{ gym=0; document.getElementById('gym').value = gym; thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;korean_lessons = document.getElementById('korean_lessons').value ; china = document.getElementById('china').value ; } break;
		case "martial_arts": if ( document.getElementById('martial_arts').value == 0 ) { martial_arts=30000;document.getElementById('martial_arts').value = martial_arts;  thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;china = document.getElementById('china').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ; }else{ martial_arts=0; document.getElementById('martial_arts').value = martial_arts;  thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;china = document.getElementById('china').value ;korean_lessons = document.getElementById('korean_lessons').value ; gym = document.getElementById('gym').value ; } break;
		case "korean_lessons": if ( document.getElementById('korean_lessons').value == 0 ) { korean_lessons=30000;document.getElementById('korean_lessons').value = korean_lessons; thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;china = document.getElementById('china').value ; gym = document.getElementById('gym').value ; }else{ korean_lessons=0; document.getElementById('korean_lessons').value = korean_lessons;  thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;china = document.getElementById('china').value ; gym = document.getElementById('gym').value ;} break;
		case "all":  korean_lessons=document.getElementById('korean_lessons').value ; thailand = document.getElementById('thailand').value ;japan = document.getElementById('japan').value ;martial_arts = document.getElementById('martial_arts').value ;china = document.getElementById('china').value ; gym = document.getElementById('gym').value ; break;
		default: break;
	} 

	var balance = gross_salary - entertainment - light_and_heat - china - thailand - japan - gym - martial_arts - korean_lessons ;

 if (document.getElementById('currency'))
 {

	currency = document.getElementById('currency').value ;

   	switch(currency){
	 	case ('usd') : selected_usd = "selected=Selected" ; break;
	 	case ('gbp') : selected_gbp = "selected=Selected" ; break;
	 	case ('aud') : selected_aud = "selected=Selected" ; break;
	 	case ('can') : selected_can = "selected=Selected" ; break;
	 	case ('kwn') : selected_kwn = "selected=Selected" ; break;
	 	case ('nzd') : selected_nzd = "selected=Selected" ; break;
	 	case ('zar') : selected_zar = "selected=Selected" ; break;
	 	default:break;
	}
 }

	output = "<table >\
		<tr><th>Net Income:</th><td>"+salary+"</td></tr>\
		<tr><th colspan='2' >Deductions</th></tr>\
		<tr><td>Pension:</td><td>"+gross_pension+"</td></tr>\
		<tr><td>Medical:</td><td>"+gross_medical+"</td></tr>\
		<tr><td>Tax:</td><td>"+gross_tax+"</td></tr>\
		<tr><td>Gross Salary:</td><td>"+gross_salary+"</td></tr>\
		<tr><th colspan='2' >Living Costs:</th></tr>\
		<tr><td>Entertainment:</td><td>"+entertainment+"</td></tr>\
		<tr><td>Light+Heat+Utilities</td><td>"+light_and_heat+"</td></tr>\
		<tr><th colspan='2' >Holidays:</th></tr>\
		<tr><td><a href=\"javascript:calculate('china')\">China:</a></td>\
		    <td><input id='china' type='text' style='border:0px;' size='6' value="+china+" onKeyUp=\"calculate('all');\"  /></td></tr>\
		<tr><td><a href=\"javascript:calculate('thailand')\">Thailand:</a></td>\
		    <td><input id='thailand' type='text' style='border:0px;' size='6' value="+thailand+" onKeyUp=\"calculate('all');\" /></td></tr>\
		<tr><td><a href=\"javascript:calculate('japan')\">Japan:</a></td>\
		    <td><input id='japan' type='text' style='border:0px;' size='6' value="+japan+" onKeyUp=\"calculate('all');\" /></td></tr>\
		<tr><th colspan='2'>Extras:</th></tr>\
		<tr><td><a href=\"javascript:calculate('gym')\">Gym:</a></td>\
		    <td><input id='gym' type='text' style='border:0px;' size='6' value="+gym+" onKeyUp=\"calculate('all');\" /></td></tr>\
		<tr><td><a href=\"javascript:calculate('martial_arts')\">Martial Arts:</a></td>\
		    <td><input id='martial_arts' type='text' style='border:0px;' size='6' value="+martial_arts+" onKeyUp=\"calculate('all');\" /></td></tr>\
		<tr><td><a href=\"javascript:calculate('korean_lessons')\">Korean Lessons:</a></td>\
		    <td><input id='korean_lessons' type='text' style='border:0px;' size='6' value="+korean_lessons+" onKeyUp=\"calculate('all');\" /></td></tr>\
		<tr><th>Balance:</th><th id='balance'>"+balance+" </th><tr>\
		<tr><th><select id='currency' onChange='x_change();' >\
			  <option value=''>Korean Won</option>\
			  <option "+selected_usd+" value='usd'>US Dollars</option>\
			  <option "+selected_gbp+" value='gbp'>British Pounds </option>\
			  <option "+selected_aud+" value='aud'>Australian Dollars </option>\
			  <option "+selected_can+" value='can'>Canadian Dollars </option>\
			  <option "+selected_eir+" value='eir'>Irish Euros </option>\
			  <option "+selected_zar+" value='zar'>South African Rand </option>\
			  <option "+selected_nzd+" value='nzd'>New Zealand Dollars</option>\
			</select>\
		     </th>\
		     <td id='currency_amount' >"+currency_amount+"'</td><tr></table>\
	";

	document.getElementById('calculator').innerHTML=output;

	x_change();
 } 

