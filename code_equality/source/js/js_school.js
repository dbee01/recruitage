function validate() {
// contact
if(document.school_form.contact.value == '') { 
  alert('Please fill out contact.');
   document.school_form.contact.focus();
  return false;
}
// school
if(document.school_form.school.value == '') { 
  alert('Please fill out school.');
   document.school_form.school.focus();
  return false;
}
// monthtime
if ( document.school_form.monthtime.selectedIndex == 0 )
{
 alert ( "Please select your month option." );
  document.school_form.monthtime.focus();
 return false;
}
// month
if ( document.school_form.month.selectedIndex == 0 )
{
 alert ( "Please select your month option." );
  document.school_form.month.focus();
 return false;
}
//check email field (to make sure it's not blank)
if(document.school_form.email.value == '') {
 alert('You must enter in your email.');
 document.school_form.email.focus();
 return false;
} else {
//Now check if email is valid
if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.school_form.email.value))) {
 alert("The email you entered is not a valid email address.");
 document.school_form.email.focus();
 return false;
}
// phone
if(document.school_form.phone.value == '') { 
  alert('Please fill out phone.');
   document.school_form.phone.focus();
  return false;
}
// calltime
if(document.school_form.calltime.value == '') { 
  alert('Please fill out calltime.');
   document.school_form.calltime.focus();
  return false;
}
// school_description
if(document.school_form.school_description.value == '') { 
  alert('Please fill out school_description.');
   document.school_form.school_description.focus();
  return false;
}
// postal_address
if(document.school_form.postal_address.value == '') { 
  alert('Please fill out postal_address.');
   document.school_form.postal_address.focus();
  return false;
}
// city
if ( document.school_form.city.selectedIndex == -1 )
{
 alert ( "Please select your location option." );
  document.school_form.city.focus();
 return false;
}
// ticket_money
if ( document.school_form.ticket_money.selectedIndex == 0 )
{
 alert ( "Please select ticket money description ." );
  document.school_form.ticket_money.focus();
 return false;
}
// teacher_description
if(document.school_form.teacher_description.value == '') { 
  alert('Please fill out teacher description.');
   document.school_form.teacher_description.focus();
  return false;
}
// salary
if(document.school_form.salary.value == '') { 
  alert('Please fill out salary.');
   document.school_form.salary.focus();
  return false;
}
// ticket_money
if ( document.school_form.number_teachers.selectedIndex == 0 )
{
 alert ( "Please select number of teachers." );
  document.school_form.number_teachers.focus();
 return false;
}

//Get the value of which radio button is selected
// valRadioButton is a function I define below
var radioVal = valRadiobutton(document.school_form.radioGroup);
//make sure that they actually selected one of the radio buttons:
if (radioVal == '') {
 alert("Please select one of those radio buttons!");
 return false;
}


}
//if we made it this far - then everything must be filled out okay
 return;
} // end validate function

function filterFileType(picupload, ext) {
 if (picupload.value.indexOf('.' + ext) == -1) {
 return false;
}
 return true;
} // end function filterFileType
function valRadiobutton(radiobutton) {
 myOption = -1; 
 for (i=0; i< radiobutton.length; i++) {
 if (radiobutton[i].checked) {
 myOption = i;
}
}
if (myOption == -1) { 
 return "";
} else {
 return radiobutton[myOption].value
}
} // end function valRadioButton
