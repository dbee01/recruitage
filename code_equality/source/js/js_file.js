function validate()
{

 check_form('check');  

}


function validate1() {
// name
if(document.employee_entry.name.value == '') { 
  alert('Please fill out name.');
   document.employee_entry.name.focus();
  return false;
}
// birthyear
if ( document.employee_entry.birthyear.selectedIndex == 0 )
{
 alert ( "Please select your birthyear." );
  document.employee_entry.birthyear.focus();
 return false;
}
// nationality
if ( document.employee_entry.nationality.selectedIndex == 0 )
{
 alert ( "Please select your nationality." );
  document.employee_entry.nationality.focus();
 return false;
}
//check email field (to make sure it's not blank)
if(document.employee_entry.email.value == '') {
 alert('You must enter in your email.');
 document.employee_entry.email.focus();
 return false;
} else {
//Now check if email is valid
if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.employee_entry.email.value))) {
 alert("The email you entered is not a valid email address.");
 document.employee_entry.email.focus();
 return false;
}
// phone
if(document.employee_entry.phone.value == '') { 
  alert('Please fill out phone.');
   document.employee_entry.phone.focus();
  return false;
}
// country_code
if ( document.employee_entry.country_code.selectedIndex == 0 )
{
 alert ( "Please select your country code." );
  document.employee_entry.country_code.focus();
 return false;
}
// from_time
if(document.employee_entry.from_time.value == '') { 
  alert('Please fill out from time.');
   document.employee_entry.from_time.focus();
  return false;
}
// until_time
if(document.employee_entry.until_time.value == '') { 
  alert('Please fill out until time.');
   document.employee_entry.until_time.focus();
  return false;
}
// timezone
if(document.employee_entry.timezone.value == '') { 
  alert('Please fill out timezone.');
   document.employee_entry.timezone.focus();
  return false;
}
// gender
if ( document.employee_entry.gender.selectedIndex == 0 )
{
 alert ( "Please select your gender." );
  document.employee_entry.gender.focus();
 return false;
}
// inkorea
if ( document.employee_entry.inkorea.selectedIndex == 0 )
{
 alert ( "Please select your inkorea." );
  document.employee_entry.inkorea.focus();
 return false;
}
// city
if ( document.employee_entry.location.selectedIndex < 1 )
{
 alert ( "Please select your location option." );
  document.employee_entry.location.focus();
 return false;
}
// university_education
if(document.employee_entry.university_education.value == '') { 
  alert('Please fill out university eduction.');
   document.employee_entry.university_education.focus();
  return false;
}
// experience
if(document.employee_entry.experience.value == '') { 
  alert('Please fill out experience.');
   document.employee_entry.experience.focus();
  return false;
}
// calltime
if(document.employee_entry.introduction.value == '') { 
  alert('Please fill out introduction.');
   document.employee_entry.introduction.focus();
  return false;
}

// side note: the "\n" in the confirm window will make a new line
/*
if you're validating a FILE upload field - and want to make sure the extension matches what you expect, I create a function (look for it at the bottom) to check to make sure the file extension is correct
*/
 if ( (!filterFileType(document.employee_entry.picupload, "gif")) && (!filterFileType(document.employee_entry.picupload, "jpg") ) ) {
/* the "!" in the line above means "not" so it reverses the value of whatever is next to it. So, since an "if" statement will only run if whatever in the () is true, we want to check to see if the file in the upload field ends with .gif - if it doesn't the filterFileType would return false - the "!" makes the statment "true" thereform the if statement runs the next line */
  alert("A .jpg or .gif photo is required for resume entry, thanks");
  document.employee_entry.picupload.focus();
  return false;
}
//Get the value of which radio button is selected
// valRadioButton is a function I define below
var radioVal = valRadiobutton(document.employee_entry.radioGroup);
//make sure that they actually selected one of the radio buttons:
if (radioVal == '') {
 alert("Please select one of those radio buttons!");
 return false;
}


}
//if we made it this far - then everything must be filled out okay
 return;
} // end validate function

function filterFileType(picupload, ext) {
 if (picupload.value.indexOf('.' + ext) == -1) {
 return false;
}
 return true;
} // end function filterFileType
function valRadiobutton(radiobutton) {
 myOption = -1; 
 for (i=0; i< radiobutton.length; i++) {
 if (radiobutton[i].checked) {
 myOption = i;
								   	}
	}
if (myOption == -1) { 
 return "";
} else {
 return radiobutton[myOption].value
}
} // end function valRadioButton
