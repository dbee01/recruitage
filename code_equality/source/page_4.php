<?php 

/**
 * Fourth page
 *
 *                               page_4.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

// standard hack prevent 
define('IN_DECRUIT', true); 

$root_path = './'; 

// let's hard-cache all forms ...
header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

// include these scripts
include_once($root_path . 'extension.inc'); 
include_once($root_path . 'common.'.$phpEx); 

// standard session management 
$userdata = session_pagestart($user_ip, PAGE_PAGE4); 
init_userprefs($userdata); 

// assign template 
$template->set_filenames(array( 
        'body' => 'page_4.tpl') 
); 

// basic page values ...
$template->assign_vars(array(
			     'SITENAME'=>$board_config['sitename']
			     ));

// find the flag and the number ...
$country = $userdata['country'];

// find the flag that the user is coming from
$flag = flag($country);

// valid callback time ...
// set it as valid if the time is between 9AM and 12PM Seoul time
$time = new timezone() ;

// set the phone callback time
$valid = $time->time_valid();

$template->assign_vars( array(
	
	'FORM_LINK' => ('form.'.$phpEx),
        'PAGE_1_LINK' => append_sid('page_1.'.$phpEx),
	'PAGE_2_LINK' => append_sid('page_2.'.$phpEx),
	'PAGE_3_LINK' => append_sid('page_3.'.$phpEx),
	'PAGE_4_LINK' => append_sid('page_4.'.$phpEx),
	'PAGE_5_LINK' => append_sid('page_5.'.$phpEx),
	'CONTACT_LINK' => append_sid('page_6.'.$phpEx),
	
	'L_VISA_CONTENT_TITLE' => html_entity_decode($page_maker['page4_title']),
	'L_VISA_PARAGRAPH' => html_entity_decode($page_maker['page4_text']),

	'CALLBACK_VALID'=>$valid,
	'CONTACT_FLAG' => $flag,
	'L_EMAIL' => $lang['Email'],
	'L_PHONE' => $lang['Phone']

  ));

// parse the page
$template->pparse('body'); 

// close the $db
$db->sql_close();

?>