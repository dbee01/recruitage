<?php

/**
 * This page is used to present the tutorials and news from the front page
 *
 *                                front_form.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

// standard hack prevent
define('IN_DECRUIT', true);

$phpbb_root_path = './';

include_once($phpbb_root_path . 'extension.inc');
include_once($phpbb_root_path . 'common.'.$phpEx); 

// standard session management
$userdata = session_pagestart($user_ip, $page_template);
init_userprefs($userdata);

// set template
$template->set_filenames(array('body' => 'front_form.tpl') );

// find the flag and the number ...
$country = $userdata['country'];
$flag = flag($country);

// basic page values
$template->assign_vars(array(
			       'PAGE_1_LINK' => append_sid('page_1.'.$phpEx),
			       'PAGE_2_LINK' => append_sid('page_2.'.$phpEx),
			       'PAGE_3_LINK' => append_sid('page_3.'.$phpEx),
			       'PAGE_4_LINK' => append_sid('page_4.'.$phpEx),
			       'PAGE_5_LINK' => append_sid('page_5.'.$phpEx),
			       'CONTACT_LINK' => append_sid($lang['Contact_us_link']),
			       'CONTACT_FLAG' => $flag,
			       'CONTACT_EMAIL' => $page_maker['email'],
			       'CONTACT_PHONE' => $page_maker['phone'],
			       'USERNAME'=>$userdata['username'],
			       'SITENAME'=>$board_config['sitename']			       
			       ));

// get the first testamonials page
if(($_GET['mode']=="testamonials_page1"))
{

  $template->assign_block_vars('testamonials_page',array() ); 

  $template->assign_vars(array(
				 'TESTAMONIALS1_PIC'=>'images/large_graeme.jpg', 
				 'L_TESTAMONIALS1_TEXT'=>html_entity_decode($page_maker['testamonials1_text']),
				 'TESTAMONIALS2_PIC'=>'images/large_eric.jpg', 
				 'L_TESTAMONIALS2_TEXT'=>html_entity_decode($page_maker['testamonials2_text'])
				 ));
}

// show the jobs and news page 1
if(($_GET['mode']=="jobs_news1"))
{

  $template->assign_block_vars('jobs_news1',array() ); 

  $template->assign_vars(array(
				 'L_JOBS_NEWS_TITLE'=>$lang['Jobs_and_news'], 
				 'L_JOBS_NEWS_TEXT1'=>html_entity_decode($page_maker['news1_text'])
				 ));
}

// show the jobs and news page 2
if(($_GET['mode']=="jobs_news2"))
{

  $template->assign_block_vars('jobs_news2',array() ); 
  
  $template->assign_vars(array(
				 'L_JOBS_NEWS_TITLE'=>$lang['Jobs_and_news'], 
				 'L_JOBS_NEWS_TEXT2'=>html_entity_decode($page_maker['news2_text'])
				 ));
}

// show the jobs and news page 3
if(($_GET['mode']=="jobs_news3"))
{

  $template->assign_block_vars('jobs_news3',array() ); 

  $template->assign_vars(array(
				 'L_JOBS_NEWS_TITLE'=>$lang['Jobs_and_news'], 
				 'L_JOBS_NEWS_TEXT3'=>html_entity_decode($page_maker['news3_text'])
				 ));
}

// parse the page...
$template->pparse('body'); 

// close the db object						
$db->sql_close();

?>
