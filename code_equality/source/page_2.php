<?php 

/**
 * Second page
 *
 *                               page_2.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 * 
 *
 **/

// standard hack prevent 
define('IN_DECRUIT', true); 

$root_path = './'; 

// let's hard-cache all forms ...
header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

include_once($root_path . 'extension.inc'); 
include_once($root_path . 'common.'.$phpEx); 
include_once($root_path . 'includes/xml2array.'.$phpEx); 

// standard session management 
$userdata = session_pagestart($user_ip, PAGE_PAGE2); 
init_userprefs($userdata); 

// assign template 
$template->set_filenames(array( 
        'body' => 'page_2.tpl') 
); 

// find the flag and the number ...
$country = $userdata['country'];

// find the flag that the user is coming from
$flag = flag($country);

// basic page values ...
$template->assign_vars(array(
			     'SITENAME'=>$board_config['sitename']
			     ));

// parse the xml feed from the central bank
// maybe this should happen once a day only ?
$str = "http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml";
$strYourXML=file_get_contents($str);

// build xml2Array function
$objXML = new xml2Array();
$arrOutput = $objXML->parse($strYourXML);

$usd = $arrOutput[0]["children"][5]["children"][1]["children"][1]["attrs"]["RATE"];
$gbp = $arrOutput[0]["children"][5]["children"][1]["children"][15]["attrs"]["RATE"];
$aud = $arrOutput[0]["children"][5]["children"][1]["children"][45]["attrs"]["RATE"];
$can = $arrOutput[0]["children"][5]["children"][1]["children"][47]["attrs"]["RATE"];
$kwn = $arrOutput[0]["children"][5]["children"][1]["children"][55]["attrs"]["RATE"];
$nzd = $arrOutput[0]["children"][5]["children"][1]["children"][59]["attrs"]["RATE"];
$zar = $arrOutput[0]["children"][5]["children"][1]["children"][67]["attrs"]["RATE"];


// which office hours are valid
$time = new timezone();
$valid = $time->time_valid();

// assign variables to template
$template->assign_vars( array(
        	
	'FORM_LINK' => append_sid('form.'.$phpEx),
        'PAGE_1_LINK' => append_sid('page_1.'.$phpEx),
	'PAGE_2_LINK' => append_sid('page_2.'.$phpEx),
	'PAGE_3_LINK' => append_sid('page_3.'.$phpEx),
	'PAGE_4_LINK' => append_sid('page_4.'.$phpEx),
	'PAGE_5_LINK' => append_sid('page_5.'.$phpEx),
	'CONTACT_LINK' => append_sid('page_6.'.$phpEx),

	'CALLBACK_VALID'=>$valid,	

        'L_JOB_CONTENT_TITLE' => html_entity_decode($page_maker['page2_title']),
	'L_JOB_PARAGRAPH' => html_entity_decode($page_maker['page2_text']),

	'USD' => $usd,
	'GBP' => $gbp,
	'AUD' => $aud,
	'CAN' => $can,
	'KWN' => $kwn,
	'NZD' => $nzd,
	'ZAR' => $zar,

	'CONTACT_FLAG' => $flag,
	'L_EMAIL' => $lang['Email'],
	'L_PHONE' => $lang['Phone']

  ));

// this will search the users browser cache and will find out whether or not they have been 
// to our competitors site or not... 
for ($i=0 ; $i<20 ; $i++)
{

  $pick_no = "site_pick".$i ;

  // output only those sites which are picked in board_config
  if ($board_config[$pick_no])
    {
      $site_track = "addSite('".$board_config[$pick_no]."')";
      $template->assign_block_vars('site_tracker',array('SITE_TRACK'=>$site_track));
    }

}

// parse the body here...
$template->pparse('body'); 

// close the db
$db->sql_close();

?>