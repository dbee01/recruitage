<?php

/***************************************************************************
*                                extension.inc
*                            -------------------
*   begin                : Saturday, Feb 28, 2007
*   copyright            : (C) 2007 Recruitage.com
*   email                : daraburke78@gmail.com
*
***************************************************************************/

if ( !defined('IN_DECRUIT') )
{
	die("Hacking attempt");
}

//
// Change this if your extension is not .php!
//
$phpEx = "php";

$starttime = 0;

?>