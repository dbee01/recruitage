<?php

/**
 * This is a BIG one, so big in fact, that it had to be split into two ... form.php and form_select.php, form_select.php handles most of the admin page code. An easy way to navigate this form is to search for the  url switch that you are working with. So if you are working on the school contact form at the url ...a form.php?mode=school_contact_form ... then the best thing to do is to use your editor's search function to a locate the string  'school_contact_form' and you should get two specific areas, the $_GET output page and the $_POST form input catcher that corresponds to that page being submitted as a form. 
 * 
 *                                form.php
 *                            -------------------
 * @begin                : Saturday, Feb 28, 2007
 * @copyright            : (C) 2007 Recruitage.com
 * @email                : daraburke78@gmail.com
 *
 *
 **/

// standard hack prevent
define('IN_DECRUIT', true);

// buffer this page and use compression to output it and cut down on latency
ob_start('ob_gzhandler');

// set the base path...
$phpbb_root_path = './';

// it would be great to be able to hard-cache the whole form, but unfortunately some values change
// so we'll have to cache some parts and not cache others
//header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

// basic code time developement-only code
// BEGIN TIME CODE MEASURE //
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$begintime = $time;
// BEGIN TIME CODE MEASURE //

// we're going to need these..
include_once($phpbb_root_path . 'extension.inc');
include_once($phpbb_root_path . 'common.'.$phpEx); 
include_once($phpbb_root_path . 'includes/picture.'.$phpEx);
include_once($phpbb_root_path . 'includes/xpress_resume.'.$phpEx);

// standard session management
$userdata = session_pagestart($user_ip, PAGE_FORM);
init_userprefs($userdata);

// load up the template file, use same basic template for all forms
$template->set_filenames(array('body' => 'main_form.tpl') );

// security choke point here, check ALL $_GET and $_POST variables...
$_GET = array_map("input_check",$_GET);
$_POST = array_map("input_check",$_POST);

// which navbar do they need to see ?
include($phpbb_root_path . 'includes/navbar.' .$phpEx);

// basic page values ... common to all forms
$template->assign_vars(array(
			     'USERNAME'=>$userdata['username'],
			     'SITENAME'=>$board_config['sitename'],
			     'TEACHER_FORM_TITLE'=>$board_config['sitename'],
			     'TEACHER_FORM_HEADER'=>$board_config['site_desc'],
			     'RESET'=>$lang['Reset'],
			     'SUBMIT'=>$lang['Submit'],
			     'DELETE'=>$lang['Delete']
			     ));

// decide the navbar ...
if ( $userdata['session_logged_in'] && ($userdata['user_level'] == ADMIN ) )
{	
  $template->assign_block_vars('ADMIN_NAV_BAR',array() );
} 
else
{
  $template->assign_block_vars('USER_NAV_BAR',array() );
}

// header of the recruiter email
$email_header = $userdata['username'] . "... ".$board_config['sitename'];

// this form.php basically consists of many if/else statements, each relating to a specific page
// or form input ... 

// output the teacher resume form on request,
// we're also going to call this form the can0_upload_form, to be more specific
// candidate0_upload_form
if ( ( $_GET['mode'] == 'teacher_resume_form' ) && ( empty($_POST['name']) ) )
{

  // hard cache this page, it shouldn't change often...
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");
  
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(GUEST);

  $template->assign_block_vars('can0_upload_form',array() );

  // if this page is called with the title then put the name on the page. 
  // This is so I can work with others interface with other recruiters ...
  // http://www.mysite.com/form.php?mode=teacher_resume_form&name=John
  // gives john a page with 'John' on it, so that he can show people he works with us
  if(isset($_GET['title']))
    {
      $title_val=$_GET['title'];
      $template->assign_vars(array(
				   'TEACHER_FORM_TITLE'=>$form_title, 
				   'TEACHER_FORM_HEADER'=>$title_val,
				   'TEACHER_RESUME_FORM_TITLE'=>$lang['TEACHER_RESUME_FORM_TITLE'],
				   'TEACHER_RESUME_FORM_TEXT'=>$lang['TEACHER_RESUME_FORM_TEXT']
				   ));
    }
  else
    { 

      $template->assign_vars(array(
				   'TEACHER_FORM_TITLE'=>$form_title, 
				   'TEACHER_FORM_HEADER'=>$form_header,
				   'TEACHER_RESUME_FORM_TITLE'=>$lang['TEACHER_RESUME_FORM_TITLE'],
				   'TEACHER_RESUME_FORM_TEXT'=>$lang['TEACHER_RESUME_FORM_TEXT']
				   ));
    }	


  // check each value of this form eg. name, phone, date_of_birth, in order to make sure that the admin
  // actually wants that part of the form to be outputted...
  if (  $page_maker["can0_upload_name"]  )
    {

      // the _check postfix toggles the javascript settings. If 'check' then put in some 
      // client-side javascript validation for that value...
      ( $page_maker['can0_upload_name_check'] ) ? $check = 'check' : $check = 0 ;

      // assign the variables here... take any language strings from the language file
      $template->assign_block_vars('can0_upload_form.name',
				   array('L_NAME'=>$lang['Name'],
					 'TEACHER_FIRST_NAME' => $first_name,
					 'CHECK'=>$check
					 )
				   ); 
    }
 
  if ( $page_maker["can0_upload_birthday"] ) 
    {

      ( $page_maker['can0_upload_birthday_check']  ) ? $check = 'check' : $check = 0 ;

      // pull all standard form options and choices from the $form_section object
      $year_list = $form_section->get_number_options_one();
      $month_list = $form_section->get_month_list();
      $day_list = $form_section->get_number_options(1,30);

      $template->assign_block_vars('can0_upload_form.birthday',
				   array('L_BIRTHDAY'=>$lang['Birthday'],
					 'L_YEAR'=>$lang['Year'],
					 'L_MONTH'=>$lang['Month'],
					 'L_DAY'=>$lang['Day'],
					 'YEAR_LIST'=>$year_list,
					 'MONTH_LIST'=>$month_list,
					 'DAY_LIST'=>$day_list,
					 'CHECK'=>$check
					 )
				   ); 
    }
 
  if ( $page_maker["can0_upload_nationality"] )
    {

      ( $page_maker['can0_upload_nationality_check'] ) ? $check = 'check' : $check = 0 ;

      $nationality = $form_section->get_country_options() ;

      $template->assign_block_vars('can0_upload_form.nationality',
				   array('L_NATIONALITY'=>$lang['Nationality'],
					 'COUNTRY_LIST'=>$nationality,
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_email"] )
    {

      ( $page_maker['can0_upload_email_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.email',
				   array('L_EMAIL'=>$lang['Email'],
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_telephone"] )
    {

      ( $page_maker['can0_upload_telephone_check'] ) ? $check = 'check' : $check = 0 ;

      // take the telephone drop_down menu from it...
      $telephone_text = $form_section->get_telephone_codes();

      $template->assign_block_vars('can0_upload_form.telephone',
				   array('L_PHONE'=>$lang['Telephone'],
					 'L_OTHER'=>$lang['Other'],					    
					 'L_FROM_TIME'=>$lang['From_time'],
					 'TEACHER_PHONE'=>$row['phone'],
					 'TELEPHONE_TEXT'=>$telephone_text,
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_ringtime"] ) 
    {

      ( $page_maker['can0_upload_ringtime_check'] ) ? $check = 'check' : $check = 0 ;

      $ring_text = $form_section->get_timezone_options();

      $template->assign_block_vars('can0_upload_form.ringtime',
				   array('L_RINGTIME'=>$lang['Ringtime'],
					 'L_FROM_TIME'=>$lang['From_time'],
					 'L_UNTIL_TIME'=>$lang['Until_time'],
					 'L_TIMEZONE'=>$lang['Timezone'],
					 'L_BETWEEN'=>$lang['Between'],
					 'L_AND'=>$lang['And'],
					 'L_SELECT'=>$lang['Select'],
					 'CHECK'=>$check,
					 'RING_TEXT'=>$ring_text
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_location"] ) 
    {

      ( $page_maker['can0_upload_location_check'] ) ? $check = 'check' : $check = 0 ;

      $location_block = $form_section->get_location_options(); 

      $template->assign_block_vars('can0_upload_form.location', 
				   array(
					 'LOCATION_BLOCK'=>$location_block,
					 'L_LOCATION'=>$lang['Location'],
					 'CHECK'=>$check
					 ) );
		
    }


  if ( $page_maker["can0_upload_youtube"] ) 
    {

      ( $page_maker['can0_upload_youtube_check'] ) ? $check = 'check' : $check = 0 ;


      $template->assign_block_vars('can0_upload_form.youtube', 
				   array(
					 'L_LOCATION'=>$lang['Youtube_site'],
					 'CHECK'=>$check
					 ) );
		
    }

 
  if ( $page_maker["can0_upload_arrival"] )
    {

      ( $page_maker['can0_upload_arrival_check'] ) ? $check = 'check' : $check = 0 ;

      $teacher_arrival_list = $form_section->get_arrival_list();

      $template->assign_block_vars('can0_upload_form.arrival',
				   array('L_ARRIVAL'=>$lang['Arrival'],
					 'TEACHER_ARRIVAL_LIST'=>$teacher_arrival_list,
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_gender"] ) 
    {

      ( $page_maker['can0_upload_gender_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.gender',
				   array('L_GENDER'=>$lang['Gender'],
					 'L_SELECT'=>$lang['Select'],
					 'L_MALE'=>$lang['Male'],
					 'L_FEMALE'=>$lang['Female'],
					 'CHECK'=>$check
					 )
				   ); 
    }
  
  if ( $page_maker["can0_upload_inkorea"] )
    {

      ( $page_maker['can0_upload_inkorea_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.inkorea',
				   array('L_INKOREA'=>$lang['Inkorea'],
					 'L_SELECT'=>$lang['Select'],
					 'L_IS_INKOREA'=>$lang['Is_inkorea'],
					 'L_NOT_INKOREA'=>$lang['Not_inkorea'],
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_referer"] ) 
    {

      ( $page_maker['can0_upload_referer_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.referer',
				   array('L_REFERER'=>$lang['Referer'],
					 'TEACHER_REFERER'=>$advertise,
					 'CHECK'=>$check
					 )
				   ); 
    }
 
  if ( $page_maker["can0_upload_education"] )
    {

      ( $page_maker['can0_upload_education_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.education',
				   array('L_EDUCATION'=>$lang['Education'],
					 'CHECK'=>$check
					 )); 
    }

  if ( $page_maker["can0_upload_experience"] )
    {

      ( $page_maker['can0_upload_experience_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.experience',
				   array('L_EXPERIENCE'=>$lang['Experience'],
					 'CHECK'=>$check
					 )
				   ); 
    }

  if ( $page_maker["can0_upload_introduction"] ) 
    {
      $page_maker['can0_upload_introduction_check'] ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.introduction',
				   array('L_INTRODUCTION'=>$lang['Introduction'],
					 'CHECK'=>$check					     
					 )); 
    }


  if ( $page_maker["can0_upload_picture"] )
    {

      ( $page_maker['can0_upload_picture_check'] ) ? $check = 'check' : $check = 0 ;

      $template->assign_block_vars('can0_upload_form.picture',
				   array('L_PICTURE'=>$lang['Picture'],
					 'CHECK'=>$check)
				   ); 
    }
  

}
// this is just a basic thanks page, for when people fill out forms
// autocontrol is the control_panel_match functionality that handles responses by itself
else if ( $_GET['mode'] == 'thanks' )
{

  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(GUEST);

  $template->assign_block_vars('autocontrol_thanks',array() );  

  // the page_maker variable will take this value from the db. 
  // the value can be set by the admin from the admin control_panel
  $template->assign_vars(array(
			       'AUTOCONTROL_THANKS_TITLE'=>$lang['Thank_you'],
			       'AUTOCONTROL_THANKS_TEXT'=>$page_maker['autocontrol_thanks_text'],
			       'AUTOCONTROL_THANKS_CLOSE'=>$lang['Close']
			       ));

}
// bookmarks is where the user can store their saved email pages
else if ( $_GET['mode'] == 'bookmarks' )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  $template->assign_block_vars('bookmarks',array() );  

  // every value in $board_config that has the 'bookmark_' prefix, is a bookmark
  foreach ( $board_config as $key => $value)
    {

      if ( stristr($key,'bookmark_') ) 
	{

	  $key=str_replace('bookmark_','',$key);
	  $template->assign_block_vars('bookmarks.bookmark_records',array('TITLE'=>$key,'URL'=>$value) );        
	}

    }

}
// output the contact us form on request
else if ( $_GET['mode'] == 'contact_us_form' ) 
{

  // hard-cache this form cause it never changes.
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(GUEST);
	
  $template->assign_block_vars('contact_us_form',array(
						       'TEACHER_FORM_TITLE'=>$form_title, 
						       'TEACHER_FORM_HEADER'=>$form_header,
						       'TEACHER_FORM_NAME'=>$lang['Name'],
						       'TEACHER_FORM_EMAIL'=>$lang['Email'],
						       'TEACHER_FORM_SUBJECT'=>$lang['Subject'],
						       'TEACHER_FORM_TXT'=>$lang['Text'],
						       'TEACHER_FORM_HEADING'=>$page_maker['contact_us_heading'],
						       'TEACHER_FORM_TEXT'=>$page_maker['contact_us_text']
						       ) );

}
// output the timetable form on request 
else if ( $_GET['mode'] == 'timetable_form' )
{

  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);
  
  $template->assign_block_vars('timetable_form',array(
						      'TIMETABLE_TITLE'=>$lang['Timetable'],
						      'TIMETABLE_EXPLANATION'=>$lang['Timetable_explanation'],
						      'TIMETABLE_FROM_ZONE'=>$lang['From_zone'],
						      'TIMETABLE_FROM_TIME'=>$lang['From_time'],
						      'TIMETABLE_TO_ZONE'=>$lang['To_zone'],			      
						      ) );

  // we want to set the 'from zone' to be the user's default zone 
  // and set the time to be the local time in the user's zone
  $time= new timezone();

  // enter user's timezone, get back an option key for the timetable
  $timezone_select = $time->timezone_to_select($userdata['user_timezone']);

  // get the minute to nearest 5mins in user's timezone
  $min_select = $time->timezone_to_min_select($userdata['user_timezone']);

  // get the hour in user's timezone
  $hour_select = $time->timezone_to_hour_select($userdata['user_timezone']);

  // replace text to have a pre-selected hour in the timetable
  $min_timezone_home = str_replace($min_select,"selected='Selected'",$min_timezone_home);

  // replace text to have a pre-selected hour in the timetable
  $hours_timezone_home = str_replace($hour_select,"selected='Selected'",$hours_timezone_home);

  // replace text to have a pre-selected timezone in the time table
  $timezone_countries_home = str_replace($timezone_select,"selected='Selected'",$timezone_countries_home);

  $template->assign_vars(array(
			       'TIMEZONE_COUNTRIES_HOME'=>$timezone_countries_home,
			       'TIMEZONE_COUNTRIES'=>$timezone_countries,
			       'MIN_TIMEZONE_HOME'=>$min_timezone_home,
			       '24_HOURS_TIMEZONE_HOME'=>$hours_timezone_home,
			       '24_HOURS_TIMEZONE'=>$hours_timezone
			       ));

}

// output the reminder form on request
else if ( $_GET['mode'] == 'reminder' ) 
{

  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");
	
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  $template->assign_block_vars('reminder_form',array(
						     'REMINDER'=>$lang['Reminder_form'],
						     'USER_ID'=>$userdata['user_regdate']
						     ) );

  // if ADMIN, then this facility gives the admin the option of sending the reminder
  // and having it appear in one of the other recruiter's reminder tab. Kinda like a
  // staff communication service ...
  if ( $userdata['user_level'] == ADMIN ) 
   {

     $template->assign_block_vars('reminder_form.reminder_channel',array());

     // select all the users and give us the option of 'reminding' them ....
     $sql="SELECT username, user_regdate from users WHERE username != 'Anonymous'";

     if (! ($result = $db->sql_query($sql)) ) 
       {
	 message_die(CRITICAL_ERROR,'Problem with the tel_contact table','',__LINE__,__FILE__,$sql);
       }
		
     while ( $row = $db->sql_fetchrow($result) )
       {
	 
	 // enable the admin user to communicate with other users...
	 $template->assign_block_vars('reminder_form.reminder_channel.reminder_users',array(
						     'REM_USER_NAME'=>$row['username'],
						     'REM_USER_ID'=>$row['user_regdate']
						     ) );
       }

   }

}

// output the candidate email form on request,
// this will be used to contact one candidate specifically
else if ( !(empty($_GET['candidate_email_form'])) ) 
{

  // don't hard cache this page or the email history will be out of date
  // maybe hard cache this and get the email history by XHR ?
	
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  // this layer of security because I'll be outputting the text on 
  // the page. I want to clean it up a bit and have no htmlentities
  $_GET = array_map("gen_clean",$_GET);

  // if user show email is 1 then output proper email address.
  // otherwise just output the base64 non-visible version.
  if ( $userdata['user_show_email'] == 1  )
    {

      $email_address = $_GET['candidate_email_form'];
      
      // this is the address that we are going to show.
      $email_address_show = $email_address ;

    }
  else 
    {

      // store the encoded version
      $email_address_show = $_GET['candidate_email_form'];

      // work here with the decoded version, to get mail history etc..
      $email_address = base64_decode($_GET['candidate_email_form']);

      // disappear the email address
      $email_visible = "style='display:none;'" ;

    }

  // this is a workaround from control_panel to
  // allow access to candidate email form
  // to both teachers and to the schools 
  if ( preg_match('/<(.+?)>/',$email_address, $matches) )
    {

      $email = $matches[1];

    }
  else {

    $email = $email_address ;

    }
  
  // find the email id
  $email_id = $_GET['email_id'] ;

  // build the mailer object
  $mail = new imailer();

  // this is where I get the body of the email by it's id
  // this pulls out base64 code and I can't decode it...
  $body=$mail->email_reply( $email_id );

  // this belongs to imailer, not mail_manager
  $attachment=$mail->mail_parser($email_id);        

  // if this attachemnt doens't have an $attachment["content"], it'll print junk
  //      $mess=$attachment[0];
  //      $attachment["content"]=$mess;
  if (isset($attachment["content"]))
    {
      $body = $attachment["content"].' '.$body ;
    }

  // mail_mem will be used to talk to the db
  $mail_mem = new mail_manager();

  // this doesn't do anything yet, but it will
  // clean up the incoming message (probably)
  $body = $mail_mem->mail_process_in($body,$email);

  // this will fetch the mail history for that email address.
  $body_history = $mail_mem->cp_mail_reader($email);

  // include the email helper list 
  include($phpbb_root_path . 'includes/email_helper_list.'.$phpEx); 	

  $template->assign_block_vars('candidate_email_form',array(
							    'CANDIDATE_EMAIL_FORM'=>$lang['Candidate_email_form'],
							    'TIMEZONE'=>$lang['Timezone'],
							    'EMAIL_VISIBLE'=>$email_visible,
							    'TEACHER_EMAIL'=>$email_address_show,
							    'CANDIDATE_EMAIL_TEXT'=>$lang['Candidate_email_text'],
							    'CANDIDATE_EMAIL_EMAIL'=>$lang['Email'],
							    'CANDIDATE_EMAIL_SUBJECT'=>$lang['Subject'],
							    'CANDIDATE_EMAIL_TEXT'=>$lang['Text'],
							    'CANDIDATE_EMAIL_ATTACHMENT'=>$lang['Attachments'],
							    ) );

  
  // output the attachments if necessary
  for( $i=0 ; $i<count($attachment) ; $i++ )
    {

      if ($attachment[$i]["type"] == "application/")
	{

	  $template->assign_block_vars('candidate_email_form.email_attachment_document',
				       array('EMAIL_ATTACHMENT_DOCUMENT' => $attachment[$i]["filename"] ));

	}
      elseif ($attachment[$i]["type"] == "image/")
	{

	  $template->assign_block_vars('candidate_email_form.email_attachment_picture', 
				       array(  'EMAIL_ATTACHMENT_PICTURE' => $attachment[$i]["filename"]  ));

	}

    } 
	
  $template->assign_vars( 
			 array( 
			       'TEACHER_FORM_TITLE'=>$form_title, 
			       'TEACHER_FORM_HEADER'=>$form_header,
			       'RECRUITER_EMAIL_HEADER'=>$email_header,
			       'EMAIL_BODY_HISTORY' => $body_history,
			       'EMAIL_ID' => $email_id,
			       'EMAIL_BODY' => $body,
			       'TEACHER_EMAIL' => $email
			       ) 
			 );

}
// Just a plain email contact form
else if ($_GET['mode'] == 'email_form' ) 
{

  // hard cache this page, and update over XHR
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  $template->assign_block_vars('email_form',array(
						  'EMAIL_FORM'=>$lang['Email_form'],
						  'EMAIL_FORM_EMAIL'=>$lang['Email'],
						  'EMAIL_FORM_SUBJECT'=>$lang['Subject'],
						  'EMAIL_FORM_TEXT'=>$lang['Text']
						  ) );

  // if the user wants to see some contact search history
  // then look that up and output it ...
  if (isset($_GET['index']))
    {
	    
      @extract($_GET);

      $sql="SELECT * FROM tel_contact WHERE contact LIKE '$index%'";
      							  
      if (! ($result = $db->sql_query($sql)) ) 
	{
	  message_die(CRITICAL_ERROR,'Problem with the tel_contact table','',__LINE__,__FILE__,$sql);
	}
		
      while ( $row = $db->sql_fetchrow($result) )
	{
		
	  $template->assign_block_vars('email_form.contact_list',
				       array(
					     'EMAIL_FORM_CONTACT' => $row['contact'],
					     'EMAIL_FORM_DETAILS' => $row['notes'],
					     'EMAIL_FORM_NUMBER' => $row['telephone'],
					     'EMAIL_FORM_EMAIL' => $row['email']
					     )
				       );
	  
	}
    }
	
  // include contact search here ...
  $template->assign_vars(array('CONTACT_SEARCH'=>$contact_search));
 	
  // include the email helper list here ...
  include($phpbb_root_path . 'includes/email_helper_list.'.$phpEx); 	
	
}
// output the school form on request there are three different hooks in form.php to catch  three different school forms
// this is just the basic form
else if ( ($_GET['mode'] == 'school_form') && empty($_POST['contact']) )
{

  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  $template->assign_block_vars('job0_form',array(
						 'USER_REG_ID'=>$userdata['user_regdate']
						 )
			       );

  // this form picker will set the selected values, for when I  want to open this page
  // as an updateable form instead ...
  include($phpbb_root_path . 'includes/job0_form_picker.'.$phpEx); 	

  // output the location block options
  $location_block = $form_section->get_location_options();

  $template->assign_vars( 
			 array( 
			       'TEACHER_FORM_TITLE'=>$form_title, 
			       'TEACHER_FORM_HEADER'=>$form_header,
			       'SCHOOL_FORM_TITLE' => $lang['School_form_title'],
			       'SCHOOL_FORM_DESCRIPTION' => $lang['School_form_description']
			       )
			 );
}

// output the school contact form on request
else if ( $_GET['mode'] == 'school_contact_form' ) 
{
  	
  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  // stock form stuff
  $location_block = $form_section->get_location_options('') ;

  // put the contact thing in there
  $contact_search = $form_section->get_contact_search();

  $template->assign_block_vars('school_contact_form',array(
							   'SCHOOL_CONTACT_FORM'=>$lang['School_contact_form'],
							   'SCHOOL_CONTACT_TEXT'=>$lang['School_contact_text'],
							   'TELEPHONE'=>$lang['Telephone'],
							   'CONTACT'=>$lang['Contact'],
							   'ORGANIZATION'=>$lang['Organization'],
							   'EMAIL'=>$lang['Email'],
							   'L_SUBJECT'=>$lang['Subject'],
							   'L_LOCATION'=>$lang['Location'],
							   'EXPRESS_RESUME'=>$lang['Express_resume'],
							   'NOTES'=>$lang['Notes'],
							   'TEXT'=>$lang['Text']
							   ) );
  // assign the email helper text
  $template->assign_vars(array(
			       'REPLY_TEXT0'=>reply_text_replace($page_maker['reply_text0']),
			       'REPLY_TITLE0'=>$page_maker['reply_text0_title'],
			       'REPLY_TEXT1'=>reply_text_replace($page_maker['reply_text1']),
			       'REPLY_TITLE1'=>$page_maker['reply_text1_title'],
			       'REPLY_TEXT2'=>reply_text_replace($page_maker['reply_text2']),
			       'REPLY_TITLE2'=>$page_maker['reply_text2_title'],
			       'REPLY_TEXT3'=>reply_text_replace($page_maker['reply_text3']),
			       'REPLY_TITLE3'=>$page_maker['reply_text3_title'],
			       'REPLY_TEXT4'=>reply_text_replace($page_maker['reply_text4']),
			       'REPLY_TITLE4'=>$page_maker['reply_text4_title'],
			       'REPLY_TEXT5'=>reply_text_replace($page_maker['reply_text5']),
			       'REPLY_TITLE5'=>$page_maker['reply_text5_title'],
			       'TEACHER_FORM_TITLE'=>$form_title, 
			       'TEACHER_FORM_HEADER'=>$form_header,	
			       'LOCATION_BLOCK' => $location_block,
			       'TEACHER_ARRIVAL' => $teacher_arrival_list,
			       'RECRUITER_EMAIL_HEADER'=>$email_header,
			       'USERNAME'=>$userdata['username']
			       ));

  // search the tel_contact table to get contact history information, if the user wants it.
  if (isset($_GET['index']))
    {
	    
      @extract($_GET);

      $sql="SELECT * FROM tel_contact WHERE contact LIKE '$index%'";
	 
      if (! ($result = $db->sql_query($sql)) ) 
	{
	  message_die(CRITICAL_ERROR,'Problem with the tel_contact table','',__LINE__,__FILE__,$sql);
	}
		
      while ( $row = $db->sql_fetchrow($result) )
	{
		
	  $template->assign_block_vars('school_contact_form.contact_list',
				       array(
					     'EMAIL_FORM_CONTACT' => $row['contact'],
					     'EMAIL_FORM_DETAILS' => $row['notes'],
					     'EMAIL_FORM_NUMBER' => $row['telephone'],
					     'EMAIL_FORM_EMAIL' => $row['email']
					     )
				       );
	}
    }


  // include the email helper list here ...
  include($phpbb_root_path . 'includes/email_helper_list.'.$phpEx); 	

  // include contact search here ...
  $template->assign_vars(array('CONTACT_SEARCH'=>$contact_search));

} 

// output the recruiter form on request, let the recruiter upload jobs
else if($_GET['mode'] == 'recruiter_form' && empty($_POST['contact'])  )
{

  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(GUEST);

  // just make sure this block appears...
  $template->assign_block_vars('job1_form',array());

  // include the form options that the employer has to fill out
  include($phpbb_root_path . 'includes/job1_form_picker.'.$phpEx);

  $template->assign_vars(array(
			       'TEACHER_FORM_TITLE'=>$form_title, 
			       'TEACHER_FORM_HEADER'=>$form_header,
			       'LOCATION_BLOCK'=>$location_block
			       ));

}
// output the admin email temp to allow the admin to change the email helper text that the user can use
// to reply to candidates and clients ...
else if($_GET['mode'] == 'admin_email_temp'  )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // this is basically where I build the form info. $page_maker comes out of the registry and is user-specific
  // there's alot of str_replace() here, it would be easier to put this into an array 
  // no really sure what this code does, the text is storred in the db and needs to be parsed before it's output. 
  $template->assign_block_vars('admin_email_temp',array(
							'L_TITLE'=>$lang['Title'],
							'L_AUTOMATIC_EMAIL_RESPONSE'=>$lang['Automatic_email_response'],
							'L_AUTOMATIC_EMAIL_RESPONSE_TEXT'=>$lang['Automatic_email_response_text'],
							'REPLY_TEXT'=>$lang['Reply_text'],
							'REPLY_TEXT_TITLE'=>$lang['Reply_text_title'],
							'REPLY_TEXT0_TITLE'=>str_replace('_',' ',$page_maker['reply_text0_title']),
							'REPLY_TEXT1_TITLE'=>str_replace('_',' ',$page_maker['reply_text1_title']),
							'REPLY_TEXT2_TITLE'=>str_replace('_',' ',$page_maker['reply_text2_title']),
							'REPLY_TEXT3_TITLE'=>str_replace('_',' ',$page_maker['reply_text3_title']),
							'REPLY_TEXT4_TITLE'=>str_replace('_',' ',$page_maker['reply_text4_title']),
							'REPLY_TEXT5_TITLE'=>str_replace('_',' ',$page_maker['reply_text5_title']),
							'REPLY_TEXT6_TITLE'=>str_replace('_',' ',$page_maker['reply_text6_title']),
							'REPLY_TEXT7_TITLE'=>str_replace('_',' ',$page_maker['reply_text7_title']),
							'REPLY_TEXT8_TITLE'=>str_replace('_',' ',$page_maker['reply_text8_title']),
							'REPLY_TEXT9_TITLE'=>str_replace('_',' ',$page_maker['reply_text9_title']),
							'REPLY_TEXT10_TITLE'=>str_replace('_',' ',$page_maker['reply_text10_title']),
							'REPLY_TEXT11_TITLE'=>str_replace('_',' ',$page_maker['reply_text11_title']),
							'REPLY_TEXT0'=>str_replace('\n',"\n",$page_maker['reply_text0']),
							'REPLY_TEXT1'=>str_replace('\n',"\n",$page_maker['reply_text1']),
							'REPLY_TEXT2'=>str_replace('\n',"\n",$page_maker['reply_text2']),
							'REPLY_TEXT3'=>str_replace('\n',"\n",$page_maker['reply_text3']),
							'REPLY_TEXT4'=>str_replace('\n',"\n",$page_maker['reply_text4']),
							'REPLY_TEXT5'=>str_replace('\n',"\n",$page_maker['reply_text5']),
							'REPLY_TEXT6'=>str_replace('\n',"\n",$page_maker['reply_text6']),
							'REPLY_TEXT7'=>str_replace('\n',"\n",$page_maker['reply_text7']),
							'REPLY_TEXT8'=>str_replace('\n',"\n",$page_maker['reply_text8']),
							'REPLY_TEXT9'=>str_replace('\n',"\n",$page_maker['reply_text9']),
							'REPLY_TEXT10'=>str_replace('\n',"\n",$page_maker['reply_text10']),
							'REPLY_TEXT11'=>str_replace('\n',"\n",$page_maker['reply_text11']),
							'REPLY_LINK0'=>str_replace('_'," ",$page_maker['reply_link0']),
							'REPLY_LINK1'=>str_replace('_'," ",$page_maker['reply_link1']),
							'REPLY_LINK2'=>str_replace('_'," ",$page_maker['reply_link2']),
							'REPLY_LINK3'=>str_replace('_'," ",$page_maker['reply_link3']),
							'REPLY_LINK4'=>str_replace('_'," ",$page_maker['reply_link4']),
							'REPLY_LINK5'=>str_replace('_'," ",$page_maker['reply_link5']),
							'REPLY_LINK_TEXT0'=>str_replace('\n',"\n",$page_maker['reply_link_text0']),
							'REPLY_LINK_TEXT1'=>str_replace('\n',"\n",$page_maker['reply_link_text1']),
							'REPLY_LINK_TEXT2'=>str_replace('\n',"\n",$page_maker['reply_link_text2']),
							'REPLY_LINK_TEXT3'=>str_replace('\n',"\n",$page_maker['reply_link_text3']),
							'REPLY_LINK_TEXT4'=>str_replace('\n',"\n",$page_maker['reply_link_text4']),
							'REPLY_LINK_TEXT5'=>str_replace('\n',"\n",$page_maker['reply_link_text5'])
							));
 
}
// allow the admin to edit the html pages that are shown to user after form upload etc...
else if($_GET['mode'] == 'admin_auto_page'  )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  $template->assign_block_vars('admin_auto_page',array(
	         	       'L_VARIOUS_FORMS_TEXT'=>$lang['Various_forms_text'],
	             	       'L_VARIOUS_FORMS_TEXT_TEXT'=>$lang['Various_forms_text_text'],
		               'L_RESUME_UPLOAD_TEXT'=>$lang['Resume_upload_text'],
		     	       'RESUME_UPLOAD_TEXT'=>$page_maker['resume_upload_text'],
		      	       'L_RESUME_ERROR_TEXT'=>$lang['Resume_error_text'],
	      		       'RESUME_ERROR_TEXT'=>$page_maker['resume_error_text'],
	         	       'L_RECRUITER_FORM_TEXT'=>$lang['Recruiter_form_text'],
		  	       'RECRUITER_FORM_TEXT'=>$page_maker['recruiter_form_text'],
			       'L_RECRUITER_THANKS_TEXT'=>$lang['Recruiter_thanks_text'],
	        	       'RECRUITER_THANKS_TEXT'=>$page_maker['recruiter_thanks_text'],
		      	       'L_RECRUITER_ERROR_TEXT'=>$lang['Recruiter_error_text'],
			       'RECRUITER_ERROR_TEXT'=>$page_maker['recruiter_error_text'],
		  	       'L_SCHOOL_THANKS_TEXT'=>$lang['School_thanks_text'],
	        	       'SCHOOL_THANKS_TEXT'=>$page_maker['school_thanks_text'],
		      	       'L_SCHOOL_ERROR_TEXT'=>$lang['School_error_text'],
		     	       'SCHOOL_ERROR_TEXT'=>$page_maker['school_error_text'],
		     	       'L_CONTACT_US_HEADING'=>$lang['Contact_us_heading'],
		     	       'CONTACT_US_HEADING'=>$page_maker['contact_us_heading'],
		      	       'L_CONTACT_US_TEXT'=>$lang['Contact_us_text'],
			       'CONTACT_US_TEXT'=>$page_maker['contact_us_text']
						       ));

}

// allow the admin to edit various stock emails that are stored in the system and are sent out
// to notify people of jobs or of candidates and things like that...
else if($_GET['mode'] == 'admin_auto_email'  )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // output the basic elements here and replace <br> with ''
  $template->assign_block_vars('admin_auto_email',
       	       array(
		 'L_AUTOMATIC_EMAIL_TEXTS'=>$lang['Automatic_email_texts'],
		 'L_AUTOMATIC_EMAIL_TEXT'=>$lang['Automatic_email_text'],
		 'L_TEACHER_SCHOOL_POSRESPONSE_EMAIL'=>$lang['Teacher_school_posresponse_email'],
		 'TEACHER_SCHOOL_POSRESPONSE_EMAIL'=>str_replace('<br>','',$page_maker['teacher_school_posresponse_email']),
		 'L_TEACHER_RECRUITER_POSRESPONSE_EMAIL'=>$lang['Teacher_recruiter_posresponse_email'],
		 'TEACHER_RECRUITER_POSRESPONSE_EMAIL'=>str_replace('<br>','',$page_maker['teacher_recruiter_posresponse_email']),
		 'L_ETK_TEACHER_POSRESPONSE_EMAIL'=>$lang['Etk_teacher_posresponse_email'],
		 'ETK_TEACHER_POSRESPONSE_EMAIL'=>str_replace('<br>','',$page_maker['etk_teacher_posresponse_email']),
		 'L_ETK_TEACHER_SCHOOL_EMAIL'=>$lang['Etk_teacher_school_email'],
		 'ETK_TEACHER_SCHOOL_EMAIL'=>str_replace('<br>','',$page_maker['etk_teacher_school_email']), 
		 'L_ETK_TEACHER_RECRUITER_EMAIL'=>$lang['Etk_teacher_recruiter_email'],
		 'ETK_TEACHER_RECRUITER_EMAIL'=>str_replace('<br>','',$page_maker['etk_teacher_recruiter_email']),
		 'L_CANDIDATE_FAIL_EMAIL'=>$lang['Candidate_fail_email'],
		 'CANDIDATE_FAIL_EMAIL'=>str_replace('<br>','',$page_maker['candidate_fail_email']),
		 'L_EMPLOYER_REMINDER'=>$lang['Employer_reminder'],
		 'EMPLOYER_REMINDER'=>str_replace('<br>','',$page_maker['employer_reminder'])
		 )); 

}
// this is the main admin form page where they can choose which admin facility they'd like to update
else if($_GET['mode'] == 'admin_form'  )
{

  // hard cache this page
  header("Expires: Mon, 26 Jul 2097 05:00:00 GMT");

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  $template->assign_block_vars('admin_form',
	        array(
		      'L_AUTOMATIC_EMAIL_RESPONSE'=>$lang['Automatic_email_response'],
		      'L_AUTOMATIC_EMAIL_RESPONSE_TEXT'=>$lang['Automatic_email_response_text'],
		      'L_AUTOMATIC_EMAIL_TEXTS'=>$lang['Automatic_email_texts'],
		      'L_AUTOMATIC_EMAIL_TEXT'=>$lang['Automatic_email_text'],
		      'L_VARIOUS_FORMS_TEXT'=>$lang['Various_forms_text'],
		      'L_VARIOUS_FORMS_TEXT_TEXT'=>$lang['Various_forms_text_text'],
		      'L_AFTERCARE_TITLE'=>$lang['Aftercare_fixed_title'],
		      'L_AFTERCARE_TEXT'=>$lang['Aftercare_fixed_text'],
		      'L_AFTERCARE_TIME_TITLE'=>$lang['Aftercare_time_title'],
		      'L_AFTERCARE_TIME_TEXT'=>$lang['Aftercare_time_text'],
		      'L_WEBSITE_FORM_TITLE'=>$lang['Website_form_title'],
		      'L_WEBSITE_FORM_TEXT'=>$lang['Website_form_text']
		      ));
								  
     
}
// the aftercare section for when the form changes based on candidate (eg. birthday) ...
else if($_GET['mode'] == 'admin_time_aftercare'  )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  $template->assign_block_vars('admin_time_aftercare',
		     array(
			   'L_AFTERCARE_TIME_TITLE'=>$lang['Aftercare_time_title'],
			   'L_AFTERCARE_TIME_TEXT'=>$lang['Aftercare_time_text'],
			   'L_BIRTHDAY_MESSAGE'=>$lang['Birthday_message'],
			   'L_MESSAGE'=>$lang['MESSAGE'],
			   'L_MESSAGE_TITLE'=>$lang['Message_title'],
			   'L_MESSAGE_TEXT'=>$lang['Message_text'],
			   'L_FOUR_DAY'=>$lang['Four_day'],
			   'L_SAME_DAY'=>$lang['Same_day'],
			   'L_ONE_WEEK'=>$lang['One_week'],
			   'L_ONE_MONTH'=>$lang['One_month'],
			   'L_THREE_MONTH'=>$lang['Three_month'],
			   'L_SIX_MONTH'=>$lang['Six_month'],
			   'L_NINE_MONTH'=>$lang['Nine_month'],
			   'L_ELEVEN_MONTH'=>$lang['Eleven_month'],
			   'L_TWELVE_MONTH'=>$lang['Twelve_month'],
			   'BIRTHDAY_SUBJECT'=>$page_maker['birthday_subject'],
			   'BIRTHDAY_MESSAGE'=>$page_maker['birthday_message'],
			   'TIME_TITLE1'=>$page_maker['time_title1'],
			   'TIME_TEXT1'=>$page_maker['time_text1'],
			   'TIME_TITLE2'=>$page_maker['time_title2'],
			   'TIME_TEXT2'=>$page_maker['time_text2'],
			   'TIME_TITLE3'=>$page_maker['time_title3'],
			   'TIME_TEXT3'=>$page_maker['time_text3'],
			   'TIME_TITLE4'=>$page_maker['time_title4'],
			   'TIME_TEXT4'=>$page_maker['time_text4'],
			   'TIME_TITLE5'=>$page_maker['time_title5'],
			   'TIME_TEXT5'=>$page_maker['time_text5'],
			   'TIME_TITLE6'=>$page_maker['time_title6'],
			   'TIME_TEXT6'=>$page_maker['time_text6'],
			   'TIME_TITLE7'=>$page_maker['time_title7'],
			   'TIME_TEXT7'=>$page_maker['time_text7'],
			   'TIME_TITLE8'=>$page_maker['time_title8'],
			   'TIME_TEXT8'=>$page_maker['time_text8'],
			   'TIME_TITLE9'=>$page_maker['time_title9'],
			   'TIME_TEXT9'=>$page_maker['time_text9']
			   ));
								  
     
}
// output the admin aftercare part where the times are fixed ... (eg. Constitution day)
else if($_GET['mode'] == 'admin_form_aftercare'  )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // parse and output the values
  $template->assign_block_vars('admin_fixed_aftercare',
			array(
			      'MESSAGE'=>$lang['MESSAGE'],
			      'MESSAGE_TITLE'=>$lang['Message_title'],
			      'MESSAGE_TEXT'=>$lang['Message_text'],
			      'L_AFTERCARE_TITLE'=>$lang['Aftercare_fixed_title'],
			      'L_AFTERCARE_TEXT'=>$lang['Aftercare_fixed_text'],
			      'FIXED_DATE1'=>str_replace('-','/',$page_maker['fixed_date1']),
			      'FIXED_TITLE1'=>$page_maker['fixed_title1'],
			      'FIXED_TEXT1'=>$page_maker['fixed_text1'],
			      'FIXED_DATE2'=>str_replace('-','/',$page_maker['fixed_date2']),
			      'FIXED_TITLE2'=>$page_maker['fixed_title2'],
			      'FIXED_TEXT2'=>$page_maker['fixed_text2'],
			      'FIXED_DATE3'=>str_replace('-','/',$page_maker['fixed_date3']),
			      'FIXED_TITLE3'=>$page_maker['fixed_title3'],
			      'FIXED_TEXT3'=>$page_maker['fixed_text3'],
			      'FIXED_DATE4'=>str_replace('-','/',$page_maker['fixed_date4']),
			      'FIXED_TITLE4'=>$page_maker['fixed_title4'],
			      'FIXED_TEXT4'=>$page_maker['fixed_text4'],
			      'FIXED_DATE5'=>str_replace('-','/',$page_maker['fixed_date5']),
			      'FIXED_TITLE5'=>$page_maker['fixed_title5'],
			      'FIXED_TEXT5'=>$page_maker['fixed_text5'],
			      'FIXED_DATE6'=>str_replace('-','/',$page_maker['fixed_date6']),
			      'FIXED_TITLE6'=>$page_maker['fixed_title6'],
			      'FIXED_TEXT6'=>$page_maker['fixed_text6'],
			      'FIXED_DATE7'=>str_replace('-','/',$page_maker['fixed_date7']),
			      'FIXED_TITLE7'=>$page_maker['fixed_title7'],
			      'FIXED_TEXT7'=>$page_maker['fixed_text7'],
			      'FIXED_DATE8'=>str_replace('-','/',$page_maker['fixed_date8']),
			      'FIXED_TITLE8'=>$page_maker['fixed_title8'],
			      'FIXED_TEXT8'=>$page_maker['fixed_text8'],
			      'FIXED_DATE9'=>str_replace('-','/',$page_maker['fixed_date9']),
			      'FIXED_TITLE9'=>$page_maker['fixed_title9'],
			      'FIXED_TEXT9'=>$page_maker['fixed_text9'],
			      'FIXED_DATE10'=>str_replace('-','/',$page_maker['fixed_date10']),
			      'FIXED_TITLE10'=>$page_maker['fixed_title10'],
			      'FIXED_TEXT10'=>$page_maker['fixed_text10'],
			      'FIXED_DATE11'=>str_replace('-','/',$page_maker['fixed_date11']),
			      'FIXED_TITLE11'=>$page_maker['fixed_title11'],
			      'FIXED_TEXT11'=>$page_maker['fixed_text11'],
			      'FIXED_DATE12'=>str_replace('-','/',$page_maker['fixed_date12']),
			      'FIXED_TITLE12'=>$page_maker['fixed_title12'],
			      'FIXED_TEXT12'=>$page_maker['fixed_text12'],
			      'FIXED_DATE13'=>str_replace('-','/',$page_maker['fixed_date13']),
			      'FIXED_TITLE13'=>$page_maker['fixed_title13'],
			      'FIXED_TEXT13'=>$page_maker['fixed_text13'],
			      'FIXED_DATE14'=>str_replace('-','/',$page_maker['fixed_date14']),
			      'FIXED_TITLE14'=>$page_maker['fixed_title14'],
			      'FIXED_TEXT14'=>$page_maker['fixed_text14'],
			      'FIXED_DATE15'=>str_replace('-','/',$page_maker['fixed_date15']),
			      'FIXED_TITLE15'=>$page_maker['fixed_title15'],
			      'FIXED_TEXT15'=>$page_maker['fixed_text15']
			      ));

  $template->assign_vars(array('TEACHER_FORM_TITLE'=>$form_title));

}
// this part let's the admin change the text based on the user's ip address
else if($_GET['mode'] == 'admin_form_website' )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  $sql = "SELECT * FROM site_text_zone";

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR,'Cannot add a text zone','',__LINE__,__FILE__,$sql);
    }

  $template->assign_block_vars('admin_form_website',array(
							  'L_HEADING_TEXT'=>$lang['Heading_title'],
							  'L_PAGE1_TITLE'=>$lang['Page1_title'],
							  'L_PAGE1_TEXT'=>$lang['Page1_text'],
							  'L_PAGE2_TITLE'=>$lang['Page2_title'],
							  'L_PAGE2_TEXT'=>$lang['Page2_text'],
							  'L_PAGE3_TITLE'=>$lang['Page3_title'],
							  'L_PAGE3_TEXT'=>$lang['Page3_text'],
							  'L_PAGE4_TITLE'=>$lang['Page4_title'],
							  'L_PAGE4_TEXT'=>$lang['Page4_text'],
							  'L_PAGE5_TITLE'=>$lang['Page5_title'],
							  'L_PAGE5_TEXT'=>$lang['Page5_text'],
							  'L_NEWS1_TITLE'=>$lang['News1_title'],
							  'L_NEWS1_TEXT'=>$lang['News1_text'],
							  'L_NEWS2_TITLE'=>$lang['News2_title'],
							  'L_NEWS2_TEXT'=>$lang['News2_text'],
							  'L_NEWS3_TITLE'=>$lang['News3_title'],
							  'L_NEWS3_TEXT'=>$lang['News3_text'],
							  'TESTAMONIALS1_TITLE'=>$lang['Testamonials1_title'],
							  'TESTAMONIALS2_TITLE'=>$lang['Testamonials2_title'],
							  'L_EMAIL'=>$lang['Email'],
							  'L_PHONE'=>$lang['Phone']
							  ));

  while ($row = $db->sql_fetchrow($result))
    {

      @extract($row);

      // switch from the int to the zone name
      $zone_name = zone_code_to_name($zone_name);

      $template->assign_block_vars('admin_form_website.admin_form_text',
			 array(
			   'HEADING_TEXT'=>$heading_text,
			   'L_WEBSITE_FORM_TITLE'=>$lang['Website_form_title'],
			   'L_WEBSITE_FORM_TEXT'=>$lang['Website_form_text'],
			   'ZONE_NAME'=>$zone_name,
			   'PAGE1_TITLE'=>str_replace('<br />','',$page1_title),
			   'PAGE1_TEXT'=>str_replace('<br />','',$page1_text),
			   'PAGE2_TITLE'=>str_replace('<br />','',$page2_title),
			   'PAGE2_TEXT'=>str_replace('<br />','',$page2_text),
			   'PAGE3_TITLE'=>str_replace('<br />','',$page3_title),
			   'PAGE3_TEXT'=>str_replace('<br />','',$page3_text),
			   'PAGE4_TITLE'=>str_replace('<br />','',$page4_title),
			   'PAGE4_TEXT'=>str_replace('<br />','',$page4_text),
			   'PAGE5_TITLE'=>str_replace('<br />','',$page5_title),
			   'PAGE5_TEXT'=>str_replace('<br />','',$page5_text),
			   'NEWS1_TITLE'=>str_replace('<br />','',$news1_title),
			   'NEWS1_TEXT'=>str_replace('<br />','',$news1_text),
			   'NEWS2_TITLE'=>str_replace('<br />','',$news2_title),
			   'NEWS2_TEXT'=>str_replace('<br />','',$news2_text),
			   'NEWS3_TITLE'=>str_replace('<br />','',$news3_title),
			   'NEWS3_TEXT'=>str_replace('<br />','',$news3_text),
			   'TESTAMONIALS1_TEXT'=>$testamonials1_text,
			   'TESTAMONIALS2_TEXT'=>$testamonials2_text,
			   'EMAIL'=>$email,
			   'PHONE'=>$phone
			   ));

    }

  $template->assign_vars(array(
			       'TEACHER_FORM_TITLE'=>$form_title, 
			       ));


  // add the new resume ...
  $template->assign_block_vars('admin_form_website.add_text_zone',array());


}
// I guess this is going to be a search facility then ?
else if($_GET['mode'] == 'search_name' )
{
  //    message_die(GENERAL_MESSAGE,$output);
}
// handle the teacher resume form, input the values, adjust the photo size and then return ...
else if ( !( empty($_POST['name'] )) 
	  && ( $_GET['mode'] == 'teacher_resume_form') 
	  && !( isset($_POST['print_form'])) )
{

  @extract($_POST);

  // the refrerrer here is actually the IP address...
  $ip = $_SERVER['REMOTE_ADDR'];
  
  // parse these values ...
  if ( $phone ){  $phone=phone_cleaner($phone); }
  if ( $university_education ) {  $university_education=nl2br($university_education);  }
  if ( $experience ) { $experience=nl2br($experience); }
  if ( $introduction ) { $introduction=nl2br($introduction); }

  // date of birth here for the birthday reminder
  if ( $birthyear ) 
    { 
      $date_of_birth = $birthyear ."-".$birthmonth."-".$birthday; 

      // I'll store birthday as DATE
      unset($birthmonth);
      unset($birthday);

    }

  // process the applicant picture, store it 
  $uploadDir = 'uploads/';

  if ( $_FILES['picupload']['name'] )
    {  
      $uploadFile = $uploadDir . $_FILES['picupload']['name'];
      $uploadFilename = $_FILES['picupload']['name'];

      $pic = new picture('uploads/', $_FILES, '200', '320', 'picupload', $name);

      if (move_uploaded_file($_FILES['picupload']['tmp_name'], $uploadFile))
	{
	  $pic_upload = $pic->filename;
	}
    }

  // change the arrival to work...
  $_POST['arrival']=arrival_to_date($_POST['arrival']);

  // unset birthday and birthmonth,
  // won't need them
  unset($_POST['birthday']);
  unset($_POST['birthmonth']);

  // seperate the $_POST request into keys and values, for the sql statement that
  // we are going to be building dynamically.
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	case ('Submit') : continue ;
	case ('MAX_FILE_SIZE') : continue ;
	default : $col .= "$key,"; $data .= "'$value'," ; break;
	}
    }

  // take the comma off the end of the strings
  $col = substr($col,0,(strlen($col)-1)) ;
  $data = substr($data,0,(strlen($data)-1)) ;

  // insert into the database
  $sql = "INSERT INTO teachers ( ".$col.",submit_time,pic_upload,teacher_id,ip ) 
                 VALUES ( ".$data.",NOW(),'$pic_upload',UUID(),'$ip' );";

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the teacher resume table','',__LINE__,__FILE__,$sql);
    }
	
  // optional mail here to declare that there has been an input ... remove for the live system.
  mail($board_config['board_email'],"Resume input",$name,"From: WEB_CONTACT_FORM $name <$email>",SENDMAIL);

  // build the thank you template
  $template->assign_block_vars('resume_thank_you',array(
							'RESUME_THANK_YOU' =>$lang['Thank_you'],
							'RESUME_UPLOAD_TEXT'=>$page_maker['resume_upload_text'],
							'RESUME_CLOSE' =>$lang['Close']
							) );

}

// handle the teacher print form which enables the recruiter to adjust and change
// the teacher's resume values ... input the values, adjust the photo size 
// seems ok, teacher_resume_form change
else if ( !( empty($_POST['name']) ) && ( $_GET['mode'] == 'teacher_print_form') && isset( $_POST['print_form']  ) )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  @extract($_POST);

  // endecode basically parses the text, removes the \n and replaces it with a <br> 
  // and then does some other parsing work
  $university_education=endecode($university_education);
  $experience=endecode($experience);
  $introduction=endecode($introduction);
  $teacher_id=endecode($teacher_id);

  // only allow digits in the phone number
  $phone = preg_replace('/[^0-9]/', '', $phone);
 
  $start_date = $startyear."-".$startmonth."-".$startday ;

  // turn the arrival numerals into actual dates..
  $arrival = arrival_to_date($arrival);
  
  // adjust the calendar month by one
  $start_date_arr = explode('-',$fixed_date1);
  
  // bump up the month by one, the js calendar widget starts at 0=Jan
  $start_month = $start_date_arr[1] + 1;

  // start date is
  $start_date = $start_date_arr[0] + $start_month + $start_date_arr[2];

  // update the teachers table.
  $sql = "UPDATE teachers 
                        SET name='$name', birthyear='$birthyear', nationality='$nationality', email='$email', 
                        country_code='$country_code', phone='$phone', from_time='$from_time', until_time='$until_time', 
                        timezone='$timezone', start_date='$start_date', location='$location', arrival='$arrival', 
                        gender='$gender', inkorea='$inkorea', advertise='$advertise', 
                        university_education='$university_education', experience='$experience', 
                        introduction='$introduction' WHERE teacher_id='$teacher_id'";

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the teacher resume table','',__LINE__,__FILE__,$sql);
    }

  // if we need to upload a different pic, then process it here and upload it...
  if ( $_FILES['picupload']['name'] != '' )
    {

      $uploadDir = 'uploads/';
      $uploadFile = $uploadDir . $_FILES['picupload']['name'];

      $pic = new picture('uploads/', $_FILES, '200', '320', 'picupload', $name);
      $resize_pic = $pic->filename;
      
      if (move_uploaded_file($_FILES['picupload']['tmp_name'], $uploadFile))
	{
		  
	  $sql = "UPDATE teachers SET pic_upload = '$resize_pic' WHERE teacher_id = '$teacher_id'" ;

	  if (! ($result = $db->sql_query($sql)) ) 
	    {
	      message_die(CRITICAL_ERROR,'Problem updating the teacher resume table','',__LINE__,__FILE__,$sql);
	    }

	}
	
    }

  // redirect to another url
  $url = append_sid("/gallery.$phpEx",true);
  $url = $url."?id=$teacher_id";
  redirect($url);
	
}
// handles the contact us form from the front page
// mails the letter and then redirects to thanks you ... 
else if ( !(empty($_POST['contact_us'])) )
{
   
  @extract($_POST);

  $name = stripslashes($name);
  $email = stripslashes($email);
  $subject = stripslashes($subject);
  $text = stripslashes($text);

  mail('David@englishteachingkorea.com',$subject,$text,"From: WEB_CONTACT_FORM $name <$email>",SENDMAIL);

  $template->assign_block_vars('contact_thank_you',array(
							 'THANK_YOU'=>$lang['Thank_you'],
							 'CONTACT_THANKS_TEXT'=>$lang['Contact_thanks_text'],
							 'CLOSE' => $lang['Close']
							 ) ); 

}
// handle the candidate email form, check if date exists, if not 
// then email straight away, else store in db
// then redirect to control panel.
else if ( !(empty($_POST['candidate_email_form'])) && ( $_POST['submit'] == 'Submit' ) ) 
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);
	
  // add these headers to the email
  $headers = 'From: '. $userdata['username']  .'@englishteachingkorea.com' . "\r\n" .
    'Reply-To: ' . $userdata['username'] .'@englishteachingkorea.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

  @extract($_POST);

  // if user show email is 1 then output proper email address.
  // otherwise just output the base64 non-visible version.
  if ( !($userdata['user_show_email']) )
    {
      $email = base64_decode($row['email']);
    }

  // if their is a '/' in date, then we have been given an email + a date (eg. '23/12/08') 
  // if the date is attached, it means that we're not going to send it straight away. 
  // we are going to store the thing instead...
  if( strpos($date, '/') )
    {
		
      // set the email_from here, so that the email is sent from
      // this users account .... email format username + domain...
      $email_from = $userdata['username'].'@EnglishTeachingKorea.com' ;

      // store this email into memory so that we can view it in email history
      $sql="INSERT INTO mail (date, name, email, subject, text, email_from, headers) VALUES ('$date', '$name', '$email', '$subject', '$email_text', '$email_from', '$headers')";	

      if (!($result = $db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR, 'Cannot connect to database for mail stores', '', __LINE__,__FILE__,$sql);
	}	

    }
  else{	
	
    // OK, there is no date, so record the email, process it and send it out right now...
      			
    // store the email first and add the header
    $mail = new mail_manager();

    // store the mail in the mail memory db 
    $mail->mail_store($email_text, $email,'');

    // process the email text before sending. Add history
    $email_text = $mail->mail_process_out($email_text,$email);   

    // deocde the info before sending email
    $email_text=html_entity_decode($email_text);		  
    $subject=html_entity_decode($subject);

    // send the mail using the sendmail daemon
    mail($email,$subject,"$email_text",$headers,SENDMAIL);
	
  }

  redirect(append_sid("/control_panel.$phpEx", true));

}

// Handle the candidate email form, when the user reads the mail copy, then they 
// get a chance to delete the mail straight away, so they don't have to go back to the
// main page to do it. 
else if ( !(empty($_POST['candidate_email_form'])) && ( $_POST['delete'] == 'Delete' ) ) 
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);
	
  @extract($_POST);

  $inter = new interface_handler() ; 
  $inter->email_delete($email_id);

  redirect(append_sid("/control_panel.$phpEx", true));

}
// handles the school contact form from the control panel, 
// will mail or store the email, and will enter the telephone
// number into the db
else if ( isset($_POST['school_contact_form']) ) 
{
	
  $date=$_POST['date'];

  $headers = 'From: '.$userdata['username'].'@englishteachingkorea.com' . "\r\n" .
    'Reply-To: ' . $userdata['username'] . '@englishteachingkorea.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
      		 
  @extract($_POST);

  if( strpos($date, '/') )  // if there is a date, then enter into database
    {

      // set the email_from here, so that the email is sent from
      // this users account .... email format username + domain...
      $email_from = $userdata['username'].'@EnglishTeachingKorea.com' ;

      // insert this mail into the db
      $sql="INSERT INTO mail (date, name, email, email_from, subject, text, headers) VALUES ('$date', '$contact', '$email', '$email_from', '$subject', '$email_text', '$headers')";	

      if (!($result = $db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR, 'Cannot connect to database for mail stores', '', __LINE__,__FILE__,$sql);
	}	
    } // if there is a mail but there is no date then send it off
  else if($_POST['email_text'] != ""){	
		 
    mail($email,$subject,$email_text,$headers,SENDMAIL);
				
  } 

  // record who made the initial contact, this will be important for later on...
  $initial_contact = $userdata['username'];

  $sql="INSERT INTO tel_contact (contact, organization, location, telephone, email, xpress_resume, notes, initial_contact, date  ) 
               VALUES ( '$contact', '$organization', '$location', '$telephone', '$email_store', '$express_resume', '$notes','$initial_contact' ,CURDATE() )";	

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR, 'Cannot connect to database for mail stores', '', __LINE__,__FILE__,$sql);
    }	
	 
	 
  redirect(append_sid("/form.$phpEx?mode=school_contact_form", true));


}
// handles the mail form from the control panel, if there is a date - insert into db
// and let cron handle it later (school_cron.php). Else mail it off right now and
// redirect it to the control panel.
else if ( isset($_POST['email_form']) ) 
{

  $headers = 'From: ' . $userdata['username'] . '@englishteachingkorea.com' . "\r\n" .
    'Reply-To: ' . $userdata['username'] . '@englishteachingkorea.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

  if( strpos($date, '/') )  // if there is a date, then enter into database
    {
      @extract($_POST);

      // set the email_from here, so that the email is sent from
      // this users account .... email format username + domain...
      $email_from = $userdata['username'].'@EnglishTeachingKorea.com' ;
		
      $sql="INSERT INTO mail (date, name, email, subject, text, email_from, headers) VALUES ('$date', '$contact', '$email', '$subject', '$email_text', '$email_from', '$headers')";	

      if (!($result = $db->sql_query($sql)))
	{
	  message_die(CRITICAL_ERROR, 'Cannot connect to database for mail stores', '', __LINE__,__FILE__,$sql);
	}	
    } // if there is a mail then send it off right now
  else if($_POST['email_text'] != ""){	
	
    // html entity decode and stripslahes
    $_POST=array_map("output_check",$_POST);
			
    @extract($_POST);
	      			
    // store the email first and add the header
    $mail = new mail_manager();

    // store the mail in the mail memory db 
    $mail->mail_store($email_text, $email,'');

    // process the email text before sending. Add history
    $email_text = $mail->mail_process_out($email_text,$email);   

    // deocde the info before sending email
    $email_text=html_entity_decode($email_text);		  
    $subject=html_entity_decode($subject);

    mail($email,$subject,$email_text,$headers,SENDMAIL);

  } 
	 
  redirect(append_sid("/control_panel.$phpEx", true));


}

// timetable form, takes one zone/time and 
// translates it into another... will try to work
// this into the navbar somehow
else if ( isset($_POST['timetable']) )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  @extract($_POST);

  $from_time =  $from_hour.','.$from_min ;
  $from_day = date('D: ');

  // build a timezone object
  $time = new timezone();
  
  // get the to and from zones
  $from_zone=$time->zone_equiv($from_zone);
  $to_zone=$time->zone_equiv($to_zone);

  // find the corresponding time in one zone from the other...
  $equiv_time=$time->zone_switch($from_time,$from_zone,$to_zone);

  // parse the values
  $from_time=str_replace(',',':',$from_time);
  $from_zone=ereg_replace('.*/','',$from_zone);
  $to_zone=ereg_replace('.*/','',$to_zone);

  $template->assign_block_vars('return_timetable',array());

  $from_time = $from_day . $from_time ;

  $template->assign_vars( 
			 array( 
			       'FROM_TIME' => $from_time,
			       'FROM_ZONE' => $from_zone,
			       'TIME_ZONE' => $to_zone,
			       'EQUIV_TIME' => $equiv_time 
			       )
			 );

}

// handles the calendar reminder from the reminder page
else if ( isset($_POST['reminder']) )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  // if the email reminder option is set
  // enter the users email into the db
  if ($_POST['sms'])
    {
      $sms = $userdata['user_mobile'];
    }

  // if the email reminder option is set
  // enter the users email into the db
  if ($_POST['email'])
    {
      $email = $userdata['user_email'];
    }
  
  // unset the variables so they don't mess up 
  // with all the stuff below ...
  unset($_POST['email']);
  unset($_POST['sms']);

  /** 
   * I'm taking in every possible time option from the form here but only one will be entered into the db. Find the filled out one. Discard the rest.
   * 
   */
  function find_time($var)
    {
      if ( ( $var != 'reminder' ) && ( $var != 'Send' ) && ( $var != '' ) )
	{
	  return( $var );
	}
    }	
	
  // process each value through the find_time function..
  $rem_arr = array_filter($_POST, "find_time");

  // find reminder date and bump the month by one to accomodate js calendar
  $reminder_date=$rem_arr['reminder_date'];

  // which user is inputting this reminder  ?
  $rem_user = $rem_arr['user_id'];

  $rem_array=explode(',',$reminder_date);
  $rem_array[0] = $rem_array[0] + 1;
  $reminder_date = implode(',',$rem_array);

  // maybe this should be fixed ?
  if( 0 )
    {
      message_die(GENERAL_ERROR,'Pls only insert one reminder at a time ...');
    }
	
  // parse the values from the reminder .... reminder time, reminder text
  foreach ( $rem_arr as $options => $value ) 
    {
      $appoint_time = $options;
      $text = $value;
    }	

  // the java calendar is broken, it doesn't submit todays date by default
  $rem_time = $appoint_time . ',' . $reminder_date ; 

  // build timezone object
  $time = new timezone();

  // get corresponding time_stamp for that time in server timezone (I think)
  $timestamp = $time->return_stamp($rem_time);

  // has the reminder time passed already ?
  if ( strtotime(date('H:i')) < strtotime($timestamp) ) 
    { 
      message_die(GENERAL_MESSAGE,'That time has now passed, pick something for later on ...');				
    }

  // if the admin has put in a user reminder switch, then override the rem_user with that...
  // this allows the admin to set a reminder for someone else...
  if ( isset($user_reminder_switch) )
    {
      $rem_user = $user_reminder_switch ;
    }

  $sql="INSERT INTO reminder ( text , rem_time, sms, email, user_reg_date ) 
           VALUES ('$text', '$timestamp', '$sms', '$email', $rem_user )";	

  if (!($result = $db->sql_query($sql)))
    {
      message_die(CRITICAL_ERROR, 'Cannot connect to database for reminder', '', __LINE__,__FILE__,$sql);
    }	
	 
  // redirect the page
  redirect(append_sid("/control_panel.$phpEx", true));

}

// handle input from a standard school form, enter into db 
// then email to let us know that a new school job has arrived
// and output the school thanks form
else if ( ($_GET['mode'] == 'school_form') && !(empty($_POST['contact'])) && !(isset($_POST['print_form'])) )
{

  // newline to <br> the text input ...
  if ( isset ($_POST['school_description']) )
    {
      $_POST['school_description']=nl2br($_POST['school_description']);
    }

  if ( isset($_POST['teacher_description']) )
    {
      $_POST['teacher_description'] = nl2br($_POST['teacher_description']);
    }

  // uploads dir
  $uploadDir = 'uploads/';

  // if a picture is attached then upload it, if it's not - then don't include pic in the sql statement
  if ( isset($_FILES['picupload']['name']) )
    {
      $uploadPicFile = $uploadDir . $_FILES['picupload']['name'];

      $pic = new picture('uploads/', $_FILES, '200', '320', 'picupload');
      $resize_pic = $pic->filename;

      if ( move_uploaded_file($_FILES['picupload']['tmp_name'], $uploadPicFile))
	{
	  $_POST['picupload']=$resize_pic;
	}
    }

  // if a file exists, then upload it ...
  if ( isset($_FILES['contract']['name']) )
    {
      $uploadContractFile = $uploadDir . $_FILES['contract']['name'];

      if ( move_uploaded_file($_FILES['contract']['tmp_name'], $uploadContractFile) ) 
	{
	  $_POST['Contract']=$uploadContractFile ;
	}

    }

  // adjust the arrival ...
  $_POST['arrival'] = arrival_to_date($_POST['arrival']);
	   
  // build the sql statement dynamically
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	case ('Submit') : continue ;
	case ('MAX_FILE_SIZE') : continue ;
	default : $col .= "$key,"; $data .= "'$value'," ; break;
	}
    }
  
  // remove commas from the sql statement.
  $col = substr($col,0,(strlen($col)-1)) ;
  $data = substr($data,0,(strlen($data)-1)) ;

  // insert school job into database ...
  $sql = "INSERT INTO school_jobs ( school_jobs_id, ".$col." ) VALUES ( UUID(), ".$data." );";

  if ( !($result = $db->sql_query($sql)) )
    {

      mail($board_config['board_email'],"SCHOOL JOB INPUT ERROR",$name,"From: SCHOOL_JOB $name <$email>",SENDMAIL);

      $template->assign_block_vars('school_form_error',array(
							     'THANKS'=>$lang['Thank_you'],
							     'TEXT'=>$page_maker['school_error_text'],
							     'CLOSE'=>$lang['Close']
							     ) );  
    }	
  else
    { 

      mail($board_config['board_email'],"SCHOOL JOB INPUT",$name,"From: SCHOOL_JOB $name <$email>",SENDMAIL);

      $template->assign_block_vars('school_form_thanks',array(
							      'THANK_YOU'=>$lang['Thank_you'],
							      'TEXT'=>$page_maker['school_thanks_text'],
							      'CLOSE'=>$lang['Close']
							      ));
    }

  @extract($_POST);

  // work the title out here, if a title is sent, break it up into title and name
  $title = title_getter($contact);
  $name = name_getter($contact);

  // enter the school job details into the tel_contact box
  // so that whenever a new job gets entered, that job and contact get automatically
  // entered into the contacts ...
  $sql = "INSERT INTO tel_contact (telephone,organization,title,contact,location,email,date) 
            VALUES ('$phone','$school','$title','$name','$city','$email',NOW()) ";

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating tel_contact table','',__LINE__,__FILE__,$sql);
    }

}

// here is the print form update sheet to take values from the 
// school form print sheet and to change those values in the db
// it then redirects back to the school gallery page...
else if ( ($_GET['mode'] == 'school_form') && !(empty($_POST['contact'])) && isset($_POST['print_form']) )
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  @extract($_POST);
	
  // parse the <br> and newline etc...
  $school_description = endecode($school_description);
  $teacher_description = endecode($teacher_description);

  // upload dir
  $uploadDir = 'uploads/';

  // if we have included a picture, then process it and add to sql statement
  if(isset($_FILES['picupload']['name']))
    {
      $uploadPicFile = $uploadDir . $_FILES['picupload']['name'];
      move_uploaded_file($_FILES['picupload']['tmp_name'], $uploadPicFile);
		
      $pic = new picture('uploads/', $_FILES, '200', '320', 'picupload');
      $resize_pic = $pic->filename;
      $sql_pic = " , picupload='$resize_pic' " ;
   	
    }
  else
    {	
      $sql_pic=''; 
    }

  // if we have included a contract, then process it and add to sql statement
  if($_FILES['contract']['tmp_name'])
    {		   
      $uploadContract = $uploadDir . $_FILES['contract']['name'];
      move_uploaded_file($_FILES['contract']['tmp_name'], $uploadContract);  				   
		
      $sql_contract = " , contract='$uploadContract' " ;    
   
    }
  else
    {
      $sql_contract = "" ; 
    }

  // turn the arrival numerals into actual dates..
  $arrival = arrival_to_date($arrival);

  // update the school jobs...
  $sql = "UPDATE school_jobs SET contact='$contact',
 			 school='$school',arrival='$arrival',
 			  school_description='$school_description',
 			    accomodation='$accomodation', city='$location', 
 			     website_address='$website_address', email='$email', 
 			       kindergarten='$kindergarten', gender='$gender',
 				 elementary='$elementary', middle_school='$middle_school',
 				    high_school='$high_school', adults='$adults',  
 				       foreign_teacher='$foreign_teacher', ticket_money='$ticket_money', 
 					  teacher_description='$teacher_description', phone='$phone', 
 					       salary='$salary', number_teachers='$number_teachers'
 						    ". $sql_contract ."". $sql_pic ." 
 							WHERE school_jobs_id = '$id' ";
	
  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the school jobs table','',__LINE__,__FILE__,$sql);
    }
  else
    { 

      $url = append_sid("/school_gallery.$phpEx",true);
      $url = $url."?id=$id";
      redirect($url);
    }

}

// handle the input from a standard recruiter job upload form
// mail us an upload mail to let us know we have a new recruiter
// job and then redirect to recruiter thanks page
else if ( ($_GET['mode'] == 'recruiter_form') &&  !(empty($_POST['contact'])) && !(isset($_POST['print_form'])) ){

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(GUEST);

  // if the values are there, then set them 
  if ( isset($_POST['school_description']) )
    {
      $_POST['school_description'] = nl2br($_POST['school_description']);
    }

  if ( isset($_POST['teacher_description']))
    {
      $_POST['teacher_description'] = nl2br($_POST['teacher_description']);
    }

  // upload dir
  $uploadDir = 'uploads/';
	
  // upload the files if they are there...
  if (isset($_FILES['picupload']['name']))
    {
      $uploadPicFile = $uploadDir . $_FILES['picupload']['name'];	    
   	
      $pic = new picture('uploads/', $_FILES, '200', '320', 'picupload');
      $resize_pic = $pic->filename;
	    
      if ( move_uploaded_file($_FILES['picupload']['tmp_name'], $uploadPicFile) )
	{
	  $_POST['picupload']=$resize_pic;
	}
    }

  // if there is a contract attached then upload it
  if (isset($_FILES['contract']['name']))
    {
      $uploadContractFile = $uploadDir . $_FILES['contract']['name'];
	    
      if ( move_uploaded_file($_FILES['contract']['tmp_name'], $uploadContractFile) )
	{
	  $_POST['contract']=$uploadContractFile ;
	}

    }

  // adjust the arrival ...
  $_POST['arrival'] = arrival_to_date($_POST['arrival']);

  // build the sql statement dynamically
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	case ('Submit') : continue ;
	case ('MAX_FILE_SIZE') : continue ;
	default : $col .= "$key,"; $data .= "'$value'," ; break;
	}
    }

  // take the commas from the end of the statement
  $col = substr($col,0,(strlen($col)-1)) ;
  $data = substr($data,0,(strlen($data)-1)) ;

  $sql = "INSERT INTO recruiter_jobs ( recruiter_jobs_id, ".$col." ) VALUES ( UUID(), ".$data." );";

  if ( !($result = $db->sql_query($sql)) )
    {

      mail($board_config['board_email'],"Job input",$name,"From: RECRUITER_JOB_ERROR $name <$email>",SENDMAIL);
		
      $template->assign_block_vars('recruiter_form_error',array(
								'ERROR'=>$lang['Error'],
								'TEXT'=>$page_maker['recruiter_error_text'],
								'CLOSE'=>$lang['Close']
								) );  

    }	
  else
    { 		

      // mail here once you know it's uploaded
      mail($board_config['board_email'],"Job input",$name,"From: RECRUITER_JOB $name <$email>",SENDMAIL);

      $template->assign_block_vars('recruiter_form_thanks',array(
								 'THANK_YOU'=>$lang['Thank_you'],
								 'TEXT'=>$page_maker['recruiter_thanks_text'],
								 'CLOSE'=>$lang['Close']
								 ));
    }

}

// here is the print form update sheet to take values from the 
// recruiter form print sheet and to change those values in the db
// it then redirects back to the recruiter gallery page...
else if ( ($_GET['mode'] == 'recruiter_form') && !(empty($_POST['contact'])) && isset($_POST['print_form']) ){

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  @extract($_POST);

  // parse the text
  $school_description = endecode($school_description);
  $teacher_description = endecode($teacher_description);
	
  // upload dir
  $uploadDir = 'uploads/';
	
  // if the pic is included, then process it
  if(isset($_FILES['picupload']['name']))
    {
      $uploadFile = $uploadDir . $_FILES['picupload']['name'];	
      $pic = new picture('uploads/', $_FILES, '200', '320', 'picupload');
      $resize_pic = $pic->filename;
  
      $retval = move_uploaded_file($_FILES['picupload']['tmp_name'], $uploadFile); 
      $sql_pic = " , picupload='$resize_pic' " ;
   	
    }
  else
    {	
      $sql_pic=''; 
    }
   
  // if the contract is there then process it
  if($_FILES['contract']['tmp_name'])
    {		   
      $uploadContract = $uploadDir . $_FILES['contract']['name'];
      move_uploaded_file($_FILES['contract']['tmp_name'], $uploadContract);  				   
		
      $sql_contract = " , contract='$uploadContract' " ;    
   
    }
  else
    {
      $sql_contract = "" ; 
    }
 		 		
  // turn the arrival numerals into actual dates..
  $arrival = arrival_to_date($arrival);

  // update the recruiter jobs
  $sql = "UPDATE recruiter_jobs SET contact='$contact',
 						   school='$school',arrival='$arrival', email='$email',
 						    school_description='$school_description',
 						     accomodation='$accomodation', city='$location', 
 						      website_address='$website_address', 
 						       kindergarten='$kindergarten', gender='$gender',
 							 elementary='$elementary', middle_school='$middle_school',
 							  high_school='$high_school', adults='$adults',  
 							   foreign_teacher='$foreign_teacher', ticket_money='$ticket_money',                                                            teacher_description='$teacher_description', 
 						        	 salary='$salary', number_teachers='$number_teachers'
 								  ". $sql_contract ."". $sql_pic ." 
 								     WHERE recruiter_jobs_id = '$id' ";

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }
  else
    { 
      $url = append_sid("/recruiter_gallery.$phpEx",true);
      $url = $url."?id=$id";
      redirect($url);
    }

}
// take the admin_email_temp form and update the values ...
else if ( isset($_POST['reply_text0_title'] )){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(USER);

  // parse some of these values
  $str = array("\n","\r");
  $rep = array('\\\n','');

  for ($i=0 ; $i<12 ; $i++)
    {

      $text = 'reply_text'.$i ;
      $title = 'reply_text'.$i.'_title' ;

      $_POST[$text]=str_replace($str,$rep,$_POST[$text]);	    
      $_POST[$title]=str_replace(" ","_",$_POST[$title]);

    }

  for ($i=0 ; $i<=5 ; $i++)
    {

      $text = 'reply_link'.$i ;

      $_POST[$text]=str_replace(' ','_',$_POST[$text]);	    

    }

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build a dynamic sql statement
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }
	
  redirect(append_sid("/control_panel.$phpEx", true));

}
// take the admin_auto_email form and update the values ...
else if ( isset($_POST['teacher_school_posresponse_email'] )){
 	  
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);	
  
  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build the dynamic sql statement
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  //	message_die(GENERAL_MESSAGE,$sql);
  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the reg table','',__LINE__,__FILE__,$sql);
    }
	
  redirect(append_sid("/control_panel.$phpEx", true));

}
// take the admin_auto_page form and update the values ...
else if ( isset($_POST['resume_upload_text'] ))
{

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // textarea -> db -> html page
  $_POST=array_map("endecode",$_POST);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build the sql statement dynamcially
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas at the end of the statement
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }
	
  redirect(append_sid("/control_panel.$phpEx", true));

}
// delete a country from the text zone, each country has it's own geoip-based
// text associated with it. Remove a country from that list...
else if ( isset($_POST['heading_text']) && (isset($_POST['Delete']))  ){

  @extract($_POST);

  // convert from the zone name to the zone int
  $zone_code = zone_name_to_code($zone_name);

  $sql = "DELETE FROM site_text_zone WHERE zone_name = $zone_code" ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem deleting a zone from the zone table','',__LINE__,__FILE__,$sql);
    }

}
// take the admin_form_website form and update the country geoip text
else if ( isset($_POST['heading_text']) && !(isset($_POST['add_text_zone']))  ){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // this function kills <br>, turns \r\n to <br>...
  $_POST=array_map("endecode",$_POST); 

  // get the zone code from the name
  $zone_code = zone_name_to_code($_POST['zone_name']);

  // take the zone out 
  unset($_POST['zone_name']);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE site_text_zone SET ";
  
  // build sql statement dynamically
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas off the end of the sql statement
  $sql = substr($sql,0,(strlen($sql)-1)) . " WHERE zone_name = $zone_code " . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }

  redirect(append_sid("/control_panel.$phpEx", true));

}
// take the admin_form_website form and add a new country ...
else if ( isset($_POST['heading_text']) && (isset($_POST['add_text_zone']))  ){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // this function kills <br>, turns \r\n to <br>...
  $_POST=array_map("endecode",$_POST);

  // take out the hidden value, 
  // don't put it in the db
  unset($_POST['add_text_zone']);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "INSERT INTO site_text_zone ( ";

  // build a dynamic sql statement
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key,"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1))  ;

  $sql .= " ) VALUES ( ";

  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "'$value',"; break;
	}
    }

  $sql = substr($sql,0,(strlen($sql)-1)) . ' ) ;' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }
	
  redirect(append_sid("/control_panel.$phpEx", true));

}
// update the admin_time_aftercare for variable dates (eg. birthdays) 
else if ( isset($_POST['time_title1'] )){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build a dynamic sql statement
  foreach ( $_POST as $key => $value )
    {
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the reg table','',__LINE__,__FILE__,$sql);
    }
	
  redirect(append_sid("/control_panel.$phpEx", true));

}
// for admin_form_select and select which values to show, secure and client side validate on output form
else if ( isset($_POST['job0_upload_contact_sec']) ){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build sql statement dynamically
  foreach ( $_POST as $key => $value )
    {
	    
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}

    }

  // kill commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(GENERAL_ERROR,'Problem updating the registration table');
    }	

  redirect(append_sid("/control_panel.$phpEx", true));


}
// for admin_form_select select which values to show, secure and client-side validate on the teacher resume form
else if ( isset($_POST['job1_upload_contact_sec']) ){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build sql dynamically
  foreach ( $_POST as $key => $value )
    {
	    
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(GENERAL_ERROR,'Problem updating the registration table');
    }	

  redirect(append_sid("/control_panel.$phpEx", true));


}
// for admin_form_select select which values to show, secure and client-side validate on the teacher resume form
else if ( isset($_POST['can0_upload_name_sec']) ){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build the sql statement dynamcially
  foreach ( $_POST as $key => $value )
    {
	    
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(GENERAL_ERROR,'Problem updating the registration table');
    }	

  redirect(append_sid("/control_panel.$phpEx", true));


}
// update the values for the variable time aftercare ... admin_form_aftercare
else if ( isset($_POST['fixed_title1'] )){
 		
  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  // adjust some of the calendar values
  $_POST=js_calendar_store($_POST);

  // handy little form updater... just remember that the 
  // input values must match their table names ...
  $sql = "UPDATE registry SET ";

  // build sql statement dynamically
  foreach ( $_POST as $key => $value )
    {
	    
      switch ($key) 
	{
	case ('submit') : continue ;
	default : $sql .= "$key = '$value',"; break;
	}
    }

  // kill the commas
  $sql = substr($sql,0,(strlen($sql)-1)) . ';' ;

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }	

  redirect(append_sid("/control_panel.$phpEx", true));

}
// update the bookmark page ....
else if ( isset($_POST['bookmark_title'] )){

  // do an access check here to make sure they belong 
  // clearence levels (rising) 1) GUEST 2) USER 3)ADMIN
  access_check(ADMIN);

  @extract($_POST);

  // add the prefix bookmark so we know it's a bookmark
  $bookmark_title = "bookmark_".$bookmark_title ;	  
	  
  $sql = "INSERT INTO config (config_name,config_value) values ('$bookmark_title','$bookmark_url')";

  if (! ($result = $db->sql_query($sql)) ) 
    {
      message_die(CRITICAL_ERROR,'Problem updating the recruiter jobs table','',__LINE__,__FILE__,$sql);
    }	
	  
  redirect(append_sid("/control_panel.$phpEx", true));

}



// END TIMECODE MEASURE //
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$endtime = $time;
$totaltime = ($endtime - $begintime);
//echo 'PHP parsed this page in ' .$totaltime. ' seconds.'; $time = microtime();
// END TIMECODE MEASURE //

$template->pparse('body'); 

// flush the buffer
ob_end_flush();

// close the db						
$db->sql_close();

?>
