<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
<script LANGUAGE="javascript" src="js/js_SCHOOL.js"></script>
</head>


<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

  <form method="POST" ENCTYPE="multipart/form-data" action="form.php?mode=school_form" name="school_form" onSubmit="return validate();">
   <table border="0" width="495" align="center" cellspacing="8" cellpadding="2">
     <input type="hidden" name="print_form">
     <input type='hidden' name='id' value='{SCHOOL_JOBS_ID}' />

<!-- BEGIN job0_form -->

<!-- BEGIN contact -->
   <tr>
    <td width='50%'>{job0_form.contact.L_CONTACT}</td>
    <td><input type='text' maxlength='30' name='contact' value='{job0_form.contact.SCHOOL_CONTACT}' /></td>
   </tr>
<!-- END contact -->

<!-- BEGIN school -->
   <tr>
    <td width='50%'>{job0_form.school.L_SCHOOL}</td>
    <td><input type='text' maxlength='30' name='school' value='{job0_form.school.SCHOOL_NAME}' /></td>
   </tr>
<!-- END school -->

<!-- BEGIN email -->
  <tr {job0_form.email.SCHOOL_EMAIL_VISIBLE}>
    <td>{job0_form.email.L_EMAIL}</td>
    <td><input type='text' maxlength='45'  name='email' value='{job0_form.email.SCHOOL_EMAIL}' /></td>
  </tr>
<!-- END email -->

<!-- BEGIN phone -->
  <tr>
    <td>{job0_form.phone.L_PHONE}</td>
    <td><input type='text' maxlength='45' name='phone' value='{job0_form.phone.SCHOOL_PHONE}' /></td>
  </tr>
<!-- END phone -->

<!-- BEGIN school_description -->
  <tr>
    <td>{job0_form.school_description.L_SCHOOL_DESCRIPTION}</td>
    <td><textarea name='school_description' cols='50' rows='8' />{job0_form.school_description.SCHOOL_DESCRIPTION}</textarea></td>
  </tr>
<!-- END school_description -->

<!-- BEGIN location -->
  <tr>
   <td>{job0_form.location.L_LOCATION}</td>
   <td>
    <select id='location' name='location' >
	{LOCATION_BLOCK}
    </select>
   </td>
  </tr>
<!-- END location -->

<!-- BEGIN employee_ref -->
  <tr>
    <td>{job0_form.employee_ref.L_EMPLOYEE_REF}</td>
    <td><textarea name='foreign_teacher' cols='50' rows='2' />{job0_form.employee_ref.EMPLOYEE_REF}</textarea></td>
  </tr>
<!-- END employee_ref -->

<!-- BEGIN arrival -->
   </tr>
    <td>{job0_form.arrival.L_ARRIVAL}</td>
    <td>
     <select name='arrival' id='arrival'>
	{ARRIVAL_BLOCK}
     </select>
    </td>
  </tr>
<!-- END arrival -->

<!-- BEGIN salary -->
   <tr>
    <td>{job0_form.salary.L_SALARY}</td>
    <td>
     <select name='salary' id='salary'>
	{SALARY_BLOCK}
     </select>
     </td>
   </tr>

<!-- END salary -->
<!-- BEGIN num_teachers -->
  	<tr>
  	 <td>{job0_form.num_teachers.L_NUM_TEACHERS}</td>
  	 <td>
  	  <select name="number_teachers">
		{NUMBER_TEACHERS_BLOCK}
	  </select>
	 </td>
  	</tr>
<!-- END num_teachers -->

<!-- BEGIN accomodation -->
       <tr>
  	 <td>{job0_form.accomodation.L_ACCOMODATION}</td>
  	 <td><select name='accomodation'>
		{ACCOMODATION_BLOCK}
          </select>
        </td>
       </tr>
<!-- END accomodation --> 

<!-- BEGIN gender -->
	<tr>
	 <td>{job0_form.gender.L_GENDER}</td>
	 <td>
	   <select name="gender">
		 <option {SCHOOL_GENDER_SELECTED_0} value="Any" >Any</option>
	    <option {SCHOOL_GENDER_SELECTED_1} value="1" >Male</option>
	 	 <option {SCHOOL_GENDER_SELECTED_2} value="2" >Female</option>
		</select>
	 </td>
	</tr>
<!-- END gender -->

	<tr>
    <td colspan='2' align='center'>
     <input type="reset" value="Clear Application">&nbsp;&nbsp;
     <input type="submit" value="Send Application">
    </td>
   </tr>

<!-- END job0_form -->

   </table>
  <input type="hidden" name="MAX_FILE_SIZE" value="355000">
  </form>

</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
