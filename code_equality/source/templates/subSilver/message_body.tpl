<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
</head>


<body>
<div class='container'>
<div class='titleblock'>
<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>

  <table bgcolor='#FFFFCC' width='100%' height='100%' align='center' border='0' cellspacing='5' cellpadding='5'>
 	<tr><td align='center'><span class='error_title'>{MESSAGE_TITLE}</span></b></td></tr>
 	<tr><td align='center'><span class='error_message'>{MESSAGE_TEXT}</span></td></tr>
  </table>

<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
