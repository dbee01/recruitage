<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{TEACHER_FIRST_NAME}</title>

<link rel="shortcut icon" href="/uploads/{TEACHER_PIC_UPLOAD}" />
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

<script language='Javascript' type='text/javascript' src='js/common.js' ></script>

<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />

<!-- main calendar program -->
<script type="text/javascript" src="calendar/calendar.js"></script>

<!-- language for the calendar -->
<script type="text/javascript" src="calendar/calendar-en.js"></script>

<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript" src="calendar/calendar-setup.js"></script>


<script type="text/javascript">
// <![CDATA[

function handleResponse()
{
	// do nothing here in return of callback
}

function createRequestObject() {

    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

// take a rem_no and update it's sent status ...
function callback(tel_no)
{

	http = createRequestObject();	

	http.open('get', 'control_panel.php?action=callback&id='+tel_no,true);
	http.onreadystatechange = handleResponse;
    	http.send(null);

}

function validate()
{
	if(
		( document.getElementById('show_form').teacher_response_form.value != "1" ) 
		||
		( document.getElementById('show_form').teacher_response_form.value != "0" ) 
		) { 
	  alert('Please indicate yes or no on the form...');
	  return false;
	}
}

function display_form() {
	
	if(document.getElementById('form_status').innerHTML == 'SHOW FORM')
	{
		document.getElementById('show_form').style.display = 'block';	
		document.getElementById('form_status').innerHTML = 'HIDE FORM';
	} 
	else
	{
		document.getElementById('show_form').style.display = 'none';	
		document.getElementById('form_status').innerHTML = 'SHOW FORM';
	}
	
}

// ]]>
</script>

</head>


<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>{SITENAME} </h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<!-- BEGIN RECRUITER_RESPONSE_FORM -->

<table cellpadding='3' cellspacing='3' border='0' width='100%' >
<form name='recruiter_response_form' action="control_panel.php" 
       method="post" enctype="application/x-www-form-urlencoded"  
       onSubmit="return validate();"
       >

 <tr>
 	<td align='center' colspan='3' ><a id='form_status' href="javascript:display_form();" >HIDE FORM</a></td></tr>
  		 <input type='hidden' name='teacher_name' value='{TEACHER_FIRST_NAME}' />
  		 <input type='hidden' name='teacher_id' value='{TEACHER_ID}' />
  		 <input type='hidden' name='recruiter_jobs_id' value='{TEACHER_RECRUITER_JOBS_ID}' />
 	</td>
 </tr>
 <tr>
 <td align='center'>
  <table cellspacing='3' cellpadding='3' id='show_form'>
  <tr>
   <td align='left'>YES, I'm interested, pls arrange an interview with the teacher. </td>
   <td align='left'><input name='recruiter_response_form' value='1' type='radio' /> </td>
  </tr>
  <tr>
    <td align='left'>NO, I'd like to see more teaching applicants please.</td>
    <td align='left'><input name='recruiter_response_form' value='0' type='radio' /></td>
  </tr>
  <tr>
    <td colspan='1' align='left' valign='top'>
      Pls use this box to pass along any questions that you <br /> may have for this teacher, or any information that you would like to pass along.
    </td>
    <td><textarea name='job_notes' rows='6' cols='30' ></textarea></td>
  </tr>
  <tr><td align='center' colspan='2'><input type='reset' name='reset' value='Reset' /><input type='submit' name='submit' value='Submit' /></td></tr>
 </table>
 </td>
 </tr> 
 </form>
</table>
<br />
<br />

<!-- END RECRUITER_RESPONSE_FORM -->

<!-- BEGIN SCHOOL_RESPONSE_FORM -->

<table cellpadding='3' cellspacing='3' border='0' width='100%' >
<form name='school_response_form' action="control_panel.php" 
      method="post" enctype="application/x-www-form-urlencoded" 
      onSubmit="return validate();"
         >

 <tr>
 	<td align='center' colspan='3' ><a id='form_status' href="javascript:display_form();" >HIDE FORM</a></td></tr>
  		 <input type='hidden' name='teacher_name' value='{TEACHER_FIRST_NAME}' />
  		 <input type='hidden' name='teacher_id' value='{TEACHER_ID}' />
  		 <input type='hidden' name='school_jobs_id' value='{TEACHER_SCHOOL_JOBS_ID}' />
 	</td>
 </tr>
 <tr>
 <td align='center'>
  <table cellspacing='3' cellpadding='3' id='show_form'>
  <tr>
   <td align='left'>YES, I'm interested, pls arrange an interview with the teacher. </td>
   <td align='left'><input name='school_response_form' value='1' type='radio' /> </td>
  </tr>
  <tr>
    <td align='left'>NO, I'd like to see more teaching applicants please.</td>
    <td align='left'><input name='school_response_form' value='0' type='radio' /></td>
  </tr>
  <tr>
    <td colspan='1' align='left' valign='top'>
      Pls use this box to pass along any questions that you <br /> may have for this teacher, or any information that you would like to pass along.
    </td>
    <td><textarea name='job_notes' rows='6' cols='30' ></textarea></td>
  </tr>
  <tr><td align='center' colspan='2'><input type='reset' name='reset' value='Reset' /><input type='submit' name='submit' value='Submit' /></td></tr>
 </table>
 </td>
 </tr> 
 </form>
</table>
<br />
<br />

<!-- END SCHOOL_RESPONSE_FORM -->

<p class='style5' align='center'>{RESUME_UPDATE_LINK}</p>

<!-- Put in a hardcopy link here for users -->
{TEACHER_HARD_COPY_LINK}

<table bgcolor='#FFFFCC' width='100%' align='center' border='1' cellspacing='5' cellpadding='5'>

<!-- BEGIN can0_form -->

<!-- BEGIN resource_counter -->

   <tr>
    <td colspan='2' align='center' >
	{can0_form.resource_counter.GUIDE}&nbsp;&nbsp;
	{can0_form.resource_counter.PAGE2}&nbsp;&nbsp;
	{can0_form.resource_counter.GALLERY}&nbsp;&nbsp;
	{can0_form.resource_counter.GAMES}&nbsp;&nbsp;
    </td>		
  </tr>

<!-- END resource_counter -->


<!-- BEGIN candidate_picture -->
   <tr>
    <td colspan='2' align='center' >
	<img src='/uploads/{can0_form.candidate_picture.TEACHER_PIC_UPLOAD}' align="absmiddle" alt="teacher pic" height="250" width="200" /></td>
  </tr>
<!-- END candidate_picture -->


<!-- BEGIN youtube -->

   <tr>
    <td colspan='2' align='center' >
	{can0_form.youtube.YOUTUBE_UPLOAD}
    </td>
  </tr>

<!-- END youtube -->


<!-- BEGIN name -->
  <tr>
    <td width='15%'>{can0_form.name.L_NAME}</td>
    <td>{can0_form.name.TEACHER_NAME}</td>
  </tr>

<!-- END name -->
<!-- BEGIN birthday -->

  <tr>
    <td>{can0_form.birthday.L_BIRTHDAY}</td>
    <td>{can0_form.birthday.TEACHER_BIRTHYEAR}</td>
  </tr>

<!-- END birthday -->
<!-- BEGIN nationality -->

  <tr>
    <td>{can0_form.nationality.L_NATIONALITY}</td>
    <td>{can0_form.nationality.TEACHER_NATIONALITY}</td>
  </tr>

<!-- END nationality --> 
<!-- BEGIN email -->
  <tr>
    <td>{can0_form.email.L_EMAIL}</td>
    <td><a href='/form.php?candidate_email_form={can0_form.email.TEACHER_EMAIL}&name={can0_form.email.TEACHER_FIRST_NAME}'>Email</a></td>
  </tr>
<!-- END email -->

<!-- BEGIN referer -->
  <tr>
   <td>{can0_form.referer.L_REFERER}</td>
   <td>{can0_form.referer.TEACHER_REFERER}</td>
  </tr>
<!-- END referer -->

<!-- BEGIN telephone -->
  <tr>
    <td>{can0_form.telephone.L_PHONE}</td>
    <td>

	<a href="{can0_form.telephone.TEACHER_INTL_PHONE}">Phone</a>
	{can0_form.telephone.TEACHER_SMS}
	&nbsp;&nbsp;
	{can0_form.telephone.TEACHER_FROM_TIME}-{can0_form.telephone.TEACHER_UNTIL_TIME}&nbsp;
	{can0_form.telephone.TEACHER_TIMEZONE}

    </td>
  </tr>
<!-- END telephone -->

<!-- BEGIN location -->
   <tr>
    <td>{can0_form.location.L_LOCATION}</td>
    <td>{can0_form.location.TEACHER_LOCATION}</td>
  </tr>
<!-- END location -->

<!-- BEGIN arrival -->
  <tr>
    <td>{can0_form.arrival.L_ARRIVAL}</td>
    <td>{can0_form.arrival.TEACHER_ARRIVAL}</td>
  </tr>
<!-- END arrival -->

<!-- BEGIN start_date -->
  <tr>
    <td>{can0_form.start_date.L_START_DATE}</td> 
    <td >		
	{can0_form.start_date.TEACHER_START_DATE}
	      </td>
  </tr>
<!-- END start_date -->

<!-- BEGIN interest_select -->

  <tr> 
        <td>{can0_form.interest_select.INTEREST_SELECTION}</td>
	<td>

	<form action='/control_panel.php?' method='POST' >

		<input type='hidden' name='teacher_response_form' value='1' />
		<input type='hidden' name='teacher_id' value='{TEACHER_ID}' />
		<input type='hidden' name='from_gallery' />

		<select name='school_jobs_id' > 
		<option value='-1' >Interested In</option>
<!-- BEGIN school_interest_select -->

		<option  value='{can0_form.interest_select.school_interest_select.SCHOOL_JOBS_ID}'
		onclick='form.submit()'
		>
			{can0_form.interest_select.school_interest_select.SCHOOL}
		</option>

<!-- END school_interest_select -->

		</select>

	</form>

	&nbsp;
	&nbsp;

	<form action='/control_panel.php?' method='POST' >

		<input type='hidden' name='teacher_response_form' value='1' />
		<input type='hidden' name='teacher_id' value='{TEACHER_ID}' />
		<input type='hidden' name='from_gallery' />		

		<select name='recruiter_jobs_id' > 
		<option value='-1' >Interested In</option>


<!-- BEGIN recruiter_interest_select -->

		<option value='{can0_form.interest_select.recruiter_interest_select.RECRUITER_JOBS_ID}'
		onclick='form.submit()' >
			{can0_form.interest_select.recruiter_interest_select.SCHOOL}
		</option>

<!-- END recruiter_interest_select -->

		</select>

	</form>

	</td>

  </tr>

  <tr>

   <td>{can0_form.interest_select.L_BOOK_CALL}</td>

   <td>

	<form action='/control_panel.php?' method='POST' id='third_party_call_form' >

	<input type='hidden' name='teacher_id' value='{TEACHER_ID}' />
	<input type='hidden' name='user_reg_date' values='{USER_REG_DATE}' />

	<select name='school_jobs_id' > 
	<option value='-1' >Third Party Call</option>

<!-- BEGIN school_interest_select -->

	<option  value='{can0_form.interest_select.school_interest_select.SCHOOL_JOBS_ID}'>
		{can0_form.interest_select.school_interest_select.SCHOOL}
	</option>

<!-- END school_interest_select -->

	</select>

	&nbsp;
	&nbsp;

	<select name='recruiter_jobs_id' > 
	<option value='-1' >Third party call</option>


<!-- BEGIN recruiter_interest_select -->

	<option value='{can0_form.interest_select.recruiter_interest_select.RECRUITER_JOBS_ID}'>
			{can0_form.interest_select.recruiter_interest_select.SCHOOL}
	</option>

<!-- END recruiter_interest_select -->

	</select>


     <div class="book-call" id="calendar-container"></div>

        <script type="text/javascript">

	  function dateChanged(calendar) {
    		// Beware that this function is called even if the end-user only
    		// changed the month/year.  In order to determine if a date was
   	        // clicked you can use the dateClicked property of the calendar:
    		if (calendar.dateClicked) {
      		// OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
    		  var y = calendar.date.getFullYear();
      		  var m = calendar.date.getMonth();     // integer, 0..11
      		  var d = calendar.date.getDate();      // integer, 1..31
      		// redirect...
     		 document.getElementById('reminder_date').value = m  + "," + d + "," + y ; 
    	  	}
  	  };

	  Calendar.setup(
    		  	{
     	 		flat         : "calendar-container", // ID of the parent element
      			flatCallback : dateChanged,           // our callback function
			showsTime    : true 
    	  		}
  		);

  	  function check_date()
  		{
		if(document.getElementById('reminder_date').value == "" )
		{
			alert("Please enter a date ...");
			return false;
		}

  	  }	

  	</script>

        </div>

	<input type="button" name="book_call" value="book_call" 
               onclick="third_party_call_book();
	                recruiter_notes_update('recruiter_notes',
						document.getElementById('teacher_name').value,
						document.getElementById('school_jobs_id').value,
						document.getElementById('recruiter_jobs_id').value,
						document.getElementById('reminder_date').value,
						'Call booked between val1 and val2 for date/time' )" 
        /> 

        </form>

   </td>
  </tr>
  <tr>
   <td>Vanity URL</td>
   <td><input type='text' name='vanity_url' onkeypress="vanity_update(document.getElementById('teacher_name'),this.value);" /></td>
  </tr>

<!-- END interest_select -->

<!-- BEGIN recruiter_notes -->

  <tr>
   <td  id='recruiter_notes' >{can0_form.recruiter_notes.L_RECRUITER_NOTES}</td>
   <td class="recruiter_notes_box" >

     <div class="recruiter_notes_text" >
      {can0_form.recruiter_notes.TEACHER_RECRUITER_NOTES}
     </div>

    <form action="gallery.php" method="post" enctype="application/x-www-form-urlencoded">
      <input type='text' name='recruiter_notes' /> 
      <input type='submit' name='submit' value='submit' /><input type='hidden' name='teacher_id' value='{can0_form.recruiter_notes.TEACHER_ID}' /> 
    </form>

    <form action="gallery.php?id={can0_form.recruiter_notes.TEACHER_ID}" method="get" enctype="application/x-www-form-urlencoded"> 
      <input type='hidden' name='id' value='{can0_form.recruiter_notes.TEACHER_ID}' />
      <input type='hidden' name='Xpress_resume' value='Xpress_resume' />
      <input type='submit' name='Xpress_resume' value='Xpress_resume' />
    </form>

   </td>
  </tr>
<!-- END recruiter_notes -->

<!-- BEGIN gender -->
  <tr>
    <td>{can0_form.gender.L_GENDER}</td>
    <td>{can0_form.gender.TEACHER_GENDER}</td>
  </tr>
<!-- END gender -->

<!-- BEGIN inkorea -->
  <tr>
    <td>{can0_form.inkorea.L_INKOREA}</td>
	<td>{can0_form.inkorea.TEACHER_INKOREA}</td>
  </tr>
<!-- END inkorea -->

<!-- BEGIN education -->
  <tr>
    <td>{can0_form.education.L_EDUCATION}</td>
    <td>{can0_form.education.TEACHER_UNIVERSITY_EDUCATION}</td>
  </tr>
<!-- END education -->

<!-- BEGIN experience -->
  <tr>
    <td>{can0_form.experience.L_EXPERIENCE}</td>
    <td>{can0_form.experience.TEACHER_EXPERIENCE}</td>
  </tr>
<!-- END experience -->

<!-- BEGIN introduction -->
  <tr>
    <td>{can0_form.introduction.L_INTRODUCTION}</td>
    <td>{can0_form.introduction.TEACHER_INTRODUCTION}</td>
  </tr>
<!-- END introduction -->

<!-- END can0_form -->

  </table>
</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
