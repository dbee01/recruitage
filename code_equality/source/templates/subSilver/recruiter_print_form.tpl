<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
<script LANGUAGE="javascript" src="js/js_recruiter.js"></script>
</head>


<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

  <form method="POST" ENCTYPE="multipart/form-data" action="form.php?mode=recruiter_form" name="recruiter_form" onSubmit="return validate();self.close()">
  <table border="0" width="495" align="center" cellspacing="8" cellpadding="2">
   <input type="hidden" name="print_form">

<!-- BEGIN job1_form -->

<!-- BEGIN contact -->
   <tr>
    <td width='50%'>{job1_form.contact.L_CONTACT}</td>
    <td><input type='text' maxlength='30' name='contact' value='{job1_form.contact.RECRUITER_CONTACT}' /></td>
   </tr>
<!-- END contact -->

<!-- BEGIN school -->
   <tr>
    <td width='50%'>{job1_form.school.L_SCHOOL}</td>
    <td><input type='text' maxlength='30' name='school' value='{job1_form.school.RECRUITER_NAME}' /></td>
   </tr>
<!-- END school -->

<!-- BEGIN email -->
   <tr {job0_form.email.RECRUITER_EMAIL_VISIBLE} >
    <td>{job1_form.email.L_EMAIL}</td>
    <td><input type='text' name='email' cols='50' rows='7' value='{job1_form.email.RECRUITER_EMAIL}' /></td>
  </tr>
<!-- END email -->

<!-- BEGIN arrival -->
  <tr>
    <td>{job1_form.arrival.L_ARRIVAL}</td>
    <td>
    <select name='arrival' id='arrival'>
	{ARRIVAL_BLOCK}
		 </select>
    </td>
  </tr>
<!-- END arrival -->

<!-- BEGIN school_description -->
  <tr>
    <td>{job1_form.school_description.L_SCHOOL_DESCRIPTION}</td>
    <td><textarea name='school_description' cols='50' rows='30' />{job1_form.school_description.RECRUITER_DESCRIPTION}</textarea></td>
  </tr>
<!-- END school_description -->

<!-- BEGIN picture -->
  <tr>
   <td >{job1_form.picture.L_PICTURE}
     <input type='hidden' name='MAX_FILE_SIZE' value='100000' /></td>
	<td><input type='file' name='picupload' value='{job1_form.picture.RECRUITER_PICUPLOAD}'>
		{job1_form.picture.RECRUITER_PICUPLOAD}</td>
  </tr>
<!-- END picture -->

<!-- BEGIN location -->
  <tr>
   <td>{job1_form.location.L_LOCATION}</td>
   <td>
    <select id='location' name='location' >
      {LOCATION_BLOCK}
    </select>
  </td>
  </tr>
<!-- END location -->

<!-- BEGIN website_address -->
  <tr>
    <td>{job1_form.website_address.L_WEBSITE_ADDRESS}</td>
    <td><input type='text' name='website_address' cols='50' rows='7' value='{job1_form.website_address.RECRUITER_WEBSITE}' /></td>
  </tr>
<!-- END website_address -->

<!-- BEGIN students -->
  <tr>
  	 <td>{job1_form.students.L_STUDENTS}</td>	
  	 <td>
  	  <table cellspacing='0' cellpadding='0'>
  	   <tr><td width='50%'>Kindergarten:</td><td><input type="checkbox" name="kindergarten" value='1' 
		{RECRUITER_STUDENTS_KINDERGARTEN_SELECTED}  /></td></tr>
  	   <tr><td width='50%'>Elementary:</td><td><input type="checkbox" name="elementary" value='1' 
		{RECRUITER_STUDENTS_ELEMENTARY_SELECTED}  /></td></tr>
  	   <tr><td width='50%'>Middle School:</td><td><input type="checkbox" name="middle_school" value='1' 
		{RECRUITER_STUDENTS_MIDDLE_SCHOOL_SELECTED}  /></td></tr>
  	   <tr><td width='50%'>High School:</td><td><input type="checkbox" name="high_school" value='1' 
		{RECRUITER_STUDENTS_HIGH_SCHOOL_SELECTED}  /></td></tr>
  	   <tr><td width='50%'>Adults:</td><td><input type="checkbox" name="adults" value='1' 
		{RECRUITER_STUDENTS_ADULTS_SELECTED} /></td></tr>
  	  </table>
  	 </td>
   </tr>
<!-- END students -->

<!-- BEGIN ticket_money -->
  	<tr>
  	 <td>{job1_form.ticket_money.L_TICKET_MONEY}</td>
  	 <td>
  	 <input type="radio" name="ticket_money" value="yes" > Yes&nbsp;
	 <input type="radio" name="ticket_money" value="no" > No
  	 </td>
  	</tr>
<!-- END ticket_money -->

<!-- BEGIN applicant_description -->
  <tr>
    <td>{job1_form.applicant_description.L_APPLICANT_DESCRIPTION}</td>
    <td><textarea name='foreign_teacher' cols='50' rows='2' />
	{job1_form.applicant_description.RECRUITER_FOREIGN_TEACHER}</textarea></td>
  </tr>
<!-- END applicant_description -->

<!-- BEGIN accomodation -->
  <tr>
    <td>{job1_form.accomodation.L_ACCOMODATION}</td>
    <td><select name='accomodation'>
	{ACCOMODATION_BLOCK}
     </select>
    </td>
  </tr>
<!-- END accomodation -->

<!-- BEGIN teacher_description -->
   <tr>
    <td>{job1_form.teacher_description.L_TEACHER_DESCRIPTION}</td>
    <td><textarea name='teacher_description' cols='50' rows='7' >{job1_form.teacher_description.TEACHER_DESCRIPTION}
	</textarea></td>
   </tr>
<!-- END teacher_description -->

<!-- BEGIN contract -->
   <tr>
    <td>{job1_form.contract.L_CONTRACT}</td>
    <td><input type="file" size="30" name="contract" value="{job1_form.contract.RECRUITER_CONTRACT}"></td>
   </tr>
<!-- END contract -->

<!-- BEGIN salary -->
   <tr>
    <td>{job1_form.salary.L_SALARY}</td>
    <td>
     <select name='salary' id='salary'>
	{SALARY_BLOCK}
     </select>
     </td>
    </tr>
<!-- END salary -->

<!-- BEGIN num_teachers -->
  	<tr>
  	 <td>{job1_form.num_teachers.L_NUM_TEACHERS}</td>
  	 <td>
  	  <select name="number_teachers">
	       {NUMBER_TEACHERS_BLOCK}
	  </select>
	 </td>
  	</tr>
<!-- END num_teachers -->

<!-- BEGIN gender -->
	<tr>
	 <td>{job1_form.gender.L_GENDER}</td>
	 <td>
	   <select name="gender">
	     <option {RECRUITER_GENDER_SELECTED_0} value="Any" >Any</option>
	     <option {RECRUITER_GENDER_SELECTED_1} value="1" >Male</option>
	     <option {RECRUITER_GENDER_SELECTED_2} value="2" >Female</option>
	   </select>
	 </td>
	</tr>
<!-- END gender -->

	<tr>
    <td colspan='2' align='center'><input type='hidden' name='id' value='{RECRUITER_JOBS_ID}' />
     <input type="reset" value="Clear Application">&nbsp;&nbsp;
     <input type="submit" value="Send Application">
    </td>
   </tr>
<!-- END job1_form -->
   </table>
  <input type="hidden" name="MAX_FILE_SIZE" value="355000">
  </form>

</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
