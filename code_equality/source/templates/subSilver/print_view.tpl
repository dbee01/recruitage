<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>EnglishTeachingKorea</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<style type='text/css'>
.name-title{ font-size:30px; text-align : right ; font-family:georgia; }
.fixed{ font-size:15px;  font-family:serif; vertical-align: top; }
.dynamic{ font-size:18px; font-family: arial; vertical-align:top; }
</style>
</head>

<body>

<table bgcolor='#FFFFFF' width='100%' cellspacing='5' cellpadding='5' border='0'>
 <tr>
  <td>
   <table bgcolor='#FFFFCC' width='90%' align='center' border='0' cellspacing='5' cellpadding='5'>

	<tr>
	   <td><span style='color:blue;font-size:20px;font-weight:bold;'>EnglishTeachingKorea.com</span>
	   <br><span style='color:black;font-size:14px;'>admin@englishteachingkorea.com<br>Tel: 010 3040 3172 </span></td>
	   <td><span class='name-title'>{TEACHER_NAME}</span> </td>
	</tr>
   </table>

  </td>
 </tr>

<!-- BEGIN can0_form -->

 <tr>
  <td>
   <table bgcolor='#FFFFCC' width='90%' align='center' border='0' cellspacing='5' cellpadding='5'>

<!-- BEGIN name -->
	 <tr> <td width='20%' class='fixed' >Name: </td>  <td class='dynamic' width='60%'>{can0_form.name.TEACHER_NAME}</td>
<!-- END name -->

<!-- BEGIN candidate_picture -->
   <td rowspan='5' align='center' ><img alt="teacher pic" src="uploads/{can0_form.candidate_picture.TEACHER_PIC_UPLOAD}" align="absmiddle" height="250" width="200" /></td>  </tr>
<!-- END candidate_picture -->

<!-- BEGIN birthday -->
         <tr> <td class='fixed' >Year of birth:</td>  <td class='dynamic' >{can0_form.birthday.TEACHER_BIRTHYEAR}</td></tr>
<!-- END birthday -->

<!-- BEGIN nationality -->
	 <tr> <td class='fixed' >Citizenship:</td>  <td class='dynamic' >{can0_form.nationality.TEACHER_NATIONALITY}</td>  </tr>
<!-- END nationality -->

<!-- BEGIN location -->
	 <tr> <td class='fixed' >Preferred Location:</td>  <td class='dynamic' >{can0_form.location.TEACHER_LOCATION}</td>  </tr>
<!-- END location -->

<!-- BEGIN start_date --> 
    <tr> <td class='fixed' >Date available to start:</td><td class='dynamic' >{TEACHER_MONTHTIME}&nbsp;&nbsp;{TEACHER_MONTH}</td></tr>
<!-- END start_date -->

   </table>
  </td>
 </tr>
 <tr>
  <td>
   <table bgcolor='#FFFFCC' width='90%' align='center' border='0' cellspacing='5' cellpadding='5'>

<!-- BEGIN gender -->
    <tr><td class='fixed'  width='20%'>Gender:</td><td class='dynamic' >{can0_form.gender.TEACHER_GENDER}</td></tr>
<!-- END gender -->

<!-- BEGIN inkorea -->
    <tr><td class='fixed' >Current Location:</td><td class='dynamic' >{can0_form.inkorea.TEACHER_INKOREA}</td></tr>
<!-- END inkorea -->

<!-- BEGIN education -->
    <tr><td class='fixed' >University Education:</td><td class='dynamic' >{can0_form.education.TEACHER_UNIVERSITY_EDUCATION}</td></tr>
<!-- END education -->

<!-- BEGIN experience -->
    <tr><td class='fixed' >Experience:</td><td class='dynamic' >{can0_form.experience.TEACHER_EXPERIENCE}</td></tr>
<!-- END experience -->

<!-- BEGIN introduction -->
    <tr><td class='fixed' >Personal Introduction:</td><td class='dynamic' >{can0_form.introduction.TEACHER_INTRODUCTION}</td></tr>
<!-- END introduction -->

   </table>
  </td>
 </tr>

<!-- END can0_form -->

 <tr>
   <td>
    <table>
     <tr>
      <td>
	<p class='style14'>&copy; 2005 EnglishTeachingKorea.com </p>
	<p>&nbsp;</p>
      </td>

     </tr>
    </table>
   </td>
 </tr>
</table>

</body>
</html>


