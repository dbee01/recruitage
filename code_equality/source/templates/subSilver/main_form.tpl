<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />

<meta http-equiv='Content-Type' content='text/html; charset=euc-kr' />
<meta http-equiv="Pragma" content="No-Cache" />

<script src="js/common.js" type="text/javascript"></script>
<script src="js/js_school.js" type="text/javascript" ></script>
<script src="js/prototype.js" type="text/javascript" ></script>
<script src="js/scriptaculous.js" type="text/javascript" ></script>

<script type="text/javascript" src="js/js_recruiter.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/js_file.js"></script>

 <!-- calendar stylesheet -->
 <link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />

 <!-- main calendar program -->
 <script type="text/javascript" src="calendar/calendar.js"></script>

 <!-- language for the calendar -->
 <script type="text/javascript" src="calendar/calendar-en.js"></script>

 <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
 <script type="text/javascript" src="calendar/calendar-setup.js"></script>
 
</head>

<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

 <div id="header">
	<h1 class='style9' align='right'>{SITENAME}</h1>
	<p class='style1 style13'>{TEACHER_FORM_HEADER}</p>
 </div>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<!-- BEGIN can0_upload_form -->

  <p class='style5' align='center'>{TEACHER_RESUME_FORM_TITLE}</p>

  <p class='style5' align='justify'>{TEACHER_RESUME_FORM_TEXT}</p>
  <br />

<form action='form.php?mode=teacher_resume_form&rsub' enctype='multipart/form-data' method='post' 
	name='employee_entry' onSubmit="return check_form('check');">
<table bgcolor='#FFFFCC' width='100%' align='center' border='1' cellspacing='5' cellpadding='5'>

<!-- BEGIN name -->
  <tr>
    <td width='50%'>{can0_upload_form.name.L_NAME}</td>
    <td><input type='text' maxlength='30' id='name' name='name'  class='{can0_upload_form.name.CHECK}'  /></td>
  </tr>
<!-- END name -->

<!-- BEGIN birthday -->
  <tr>
    <td>{can0_upload_form.birthday.L_BIRTHDAY}</td>
    <td>
    <select name='birthday' id='birthday' class='{can0_upload_form.birthday.CHECK}'  >
      <option value='-1'>Day</option>
	{can0_upload_form.birthday.DAY_LIST}
    </select>&nbsp;
    <select name='birthmonth' id='birthmonth'  class='{can0_upload_form.birthday.CHECK}'  >
	<option value='-1'>{can0_upload_form.birthday.L_MONTH}</option>
	{can0_upload_form.birthday.MONTH_LIST}
    </select>&nbsp;
    <select  name='birthyear' id='birthyear' class='{can0_upload_form.birthday.CHECK}'  >
	 <option value='-1'>{can0_upload_form.birthday.L_YEAR}</option>
	 {can0_upload_form.birthday.YEAR_LIST}
    </select>

	</td>
  </tr>
<!-- END birthday -->

<!-- BEGIN nationality -->
  <tr> 
   <td>Nationality</td> 
   <td> 
    <select id='nationality'  name='nationality' class='{can0_upload_form.nationality.CHECK}' >
	{can0_upload_form.nationality.COUNTRY_LIST}
    </select>
   </td>
  </tr>
<!-- END nationality -->

<!-- BEGIN email -->
  <tr>
    <td>{can0_upload_form.email.L_EMAIL}</td>
    <td><input type='text' maxlength='45'  name='email' id='email' class='{can0_upload_form.email.CHECK}'  /></td>
  </tr>
<!-- END email -->

<!-- BEGIN telephone -->
  <tr>
    <td>{can0_upload_form.telephone.L_PHONE}</td>
    <td><select name='country_code' id='country_code'  class='{can0_upload_form.telephone.CHECK}'  >
	 		<option value=''>{can0_upload_form.telephone.L_OTHER}</option>
			{can0_upload_form.telephone.TELEPHONE_TEXT}
	 	  </select>&nbsp;&nbsp;
	 		<input type='text' maxlength='45' name='phone' id='phone'  class='{can0_upload_form.telephone.CHECK}' ></td>
  </tr>
<!-- END telephone -->
<!-- BEGIN ringtime -->
  <tr>
    <td>{can0_upload_form.ringtime.L_RINGTIME}</td>
    <td><label>{can0_upload_form.ringtime.L_BETWEEN}&nbsp;&nbsp;&nbsp;&nbsp;
    		<select name='from_time' id='from_time'  class='{can0_upload_form.ringtime.CHECK}' >
	 		<option value='-1'>{can0_upload_form.ringtime.L_SELECT}</option>
	 		<option value='1'>01:00</option>
	 		<option value='2'>02:00</option>
	 		<option value='3'>03:00</option>
	 		<option value='4'>04:00</option>
	 		<option value='5'>05:00</option>
	 		<option value='6'>06:00</option>
	 		<option value='7'>07:00</option>
	 		<option value='8'>08:00</option>
	 		<option value='9'>09:00</option>
			<option value='10'>10:00</option>
			<option value='11'>11:00</option>
			<option value='12'>12:00</option>	 		
	 		<option value='13'>13:00</option>
	 		<option value='14'>14:00</option>
	 		<option value='15'>15:00</option>
	 		<option value='16'>16:00</option>
	 		<option value='17'>17:00</option>
	 		<option value='18'>18:00</option>
	 		<option value='19'>19:00</option>
	 		<option value='20'>20:00</option>
	 		<option value='21'>21:00</option>
	 		<option value='22'>22:00</option>
	 		<option value='23'>23:00</option>
	 		<option value='24'>24:00</option>
	 	  </select></label>&nbsp;&nbsp;<label>{can0_upload_form.ringtime.L_AND}
	 	  <select name='until_time' id='until_time'  class='{can0_upload_form.ringtime.CHECK}'  >
	 		<option value='-1'>{can0_upload_form.ringtime.L_SELECT}</option>
	 		<option value='1'>01:00</option>
	 		<option value='2'>02:00</option>
	 		<option value='3'>03:00</option>
	 		<option value='4'>04:00</option>
	 		<option value='5'>05:00</option>
	 		<option value='6'>06:00</option>
	 		<option value='7'>07:00</option>
	 		<option value='8'>08:00</option>
	 		<option value='9'>09:00</option>
			<option value='10'>10:00</option>
			<option value='11'>11:00</option>
			<option value='12'>12:00</option>	 		
	 		<option value='13'>13:00</option>
	 		<option value='14'>14:00</option>
	 		<option value='15'>15:00</option>
	 		<option value='16'>16:00</option>
	 		<option value='17'>17:00</option>
	 		<option value='18'>18:00</option>
	 		<option value='19'>19:00</option>
	 		<option value='20'>20:00</option>
	 		<option value='21'>21:00</option>
	 		<option value='22'>22:00</option>
	 		<option value='23'>23:00</option>
	 		<option value='24'>24:00</option>
	 	  </select></label>&nbsp;&nbsp;<br /><label>{can0_upload_form.ringtime.L_TIMEZONE}
    	          <select name='timezone' id='timezone' class='{can0_upload_form.ringtime.CHECK}' >
			{can0_upload_form.ringtime.RING_TEXT}
	 	  </select></label>
	 </td>
   </tr>
<!-- END ringtime -->

<!-- BEGIN location -->
      <tr> 
	<td>{can0_upload_form.location.L_LOCATION}</td> 
	<td><select name='location' id='location' class='{can0_upload_form.location.CHECK}' >
		{can0_upload_form.location.LOCATION_BLOCK}
	 </select>
	</td>
      </tr>
<!-- END location -->

<!-- BEGIN youtube -->
      <tr> 
	<td>{can0_upload_form.youtube.L_LOCATION}</td> 
	<td><input type='text' name='youtube' id='youtube' class='{can0_upload_form.youtube.CHECK}' />
	</td>
      </tr>
<!-- END youtube -->

<!-- BEGIN arrival -->
  <tr>
    <td>{can0_upload_form.arrival.L_ARRIVAL}
	*The visa process takes around ONE MONTH.
         Pls state the earliest date you can have your documents ready to send to Korea before you come over.<br>
         For more info pls click<a href='http://www.englishteachingkorea.com/visa.php'> here</a>... :</td>
    <td>
        <select name='arrival' id='arrival' class='{can0_upload_form.arrival.CHECK}'  > 
           {can0_upload_form.arrival.TEACHER_ARRIVAL_LIST}
	</select>
    </td>
  </tr>
<!-- END arrival -->

<!-- BEGIN gender -->
  <tr>
    <td>{can0_upload_form.gender.L_GENDER}</td>
    <td> <select name='gender' id='gender'  class='{can0_upload_form.gender.CHECK}'  >
	 <option value='-1'>{can0_upload_form.gender.L_SELECT}</option>
	 <option value='1'>{can0_upload_form.gender.L_MALE}</option>
	 <option value='2'>{can0_upload_form.gender.L_FEMALE}</option>
	 </select>
    </td>
  </tr>
<!-- END gender -->

<!-- BEGIN inkorea -->
  <tr>
    <td>{can0_upload_form.inkorea.L_INKOREA}</td>

    <td> <select name='inkorea' id='inkorea'  class='{can0_upload_form.inkorea.CHECK}'  >
	 <option value='-1'>{can0_upload_form.inkorea.L_SELECT}</option>
	 <option value='1'>{can0_upload_form.inkorea.L_IS_INKOREA}</option>
	 <option value='2'>{can0_upload_form.inkorea.L_NOT_INKOREA}</option>
         </select>
    </td>

  </tr>
<!-- END inkorea -->

<!-- BEGIN education -->
  <tr>
    <td>{can0_upload_form.education.L_EDUCATION}</td>
    <td><textarea name='university_education' id='university_education' cols='50' rows='7' class='{can0_upload_form.education.CHECK}' >
	</textarea>
    </td>
  </tr>
<!-- END education -->
<!-- BEGIN experience -->
  <tr>
    <td>{can0_upload_form.experience.L_EXPERIENCE}</td>
    <td><textarea name='experience' id='experience' cols='50' rows='7' class='{can0_upload_form.experience.CHECK}' ></textarea></td>
  </tr>
<!-- END experience -->
<!-- BEGIN introduction -->
  <tr>
    <td>{can0_upload_form.introduction.L_INTRODUCTION}</td>
    <td><textarea name='introduction' id='introduction' cols='50' rows='7' class='{can0_upload_form.introduction.CHECK}' ></textarea></td>
  </tr>
<!-- END introduction -->
<!-- BEGIN picture -->
   <tr>
    <td >{can0_upload_form.picture.L_PICTURE}:&nbsp;<input type='hidden' name='MAX_FILE_SIZE' value='1000000' /></td>
    <td><input type='file' name='picupload' id='picupload' class='{can0_upload_form.picture.CHECK}' ></td>
  </tr>
<!-- END picture -->
  <tr>
    <td colspan=2 align='center'><input type='reset' name='Reset' value='{RESET}' >&nbsp;&nbsp;<input type='submit' name='Submit' value='{SUBMIT}'></td>
  </tr>
  </table>
 </form>

  
  <p>&nbsp;</p>
<!-- END can0_upload_form -->

<!-- BEGIN resume_thank_you -->
  <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',15000); </SCRIPT>
  <p class='style5' align='center'>{resume_thank_you.RESUME_THANK_YOU}</p>

  <p class='style5' align='justify'> </p>

  <p class='style5' align='justify'>{resume_thank_you.RESUME_UPLOAD_TEXT}</p>
  <br />
 
  <p class='style5' align='justify'>{resume_thank_you.RESUME_CLOSE}</p>
  <br />

  <p>&nbsp;</p>

<!-- END resume_thank_you -->

<!-- BEGIN autocontrol_thanks -->
  <script language='JavaScript'> setTimeout('self.close()',10000); </script>
  <p class='style5' align='center'>{AUTOCONTROL_THANKS_TITLE}</p>

    <p class='style5' align='justify'></p>

  <p class='style5' align='justify'>{AUTOCONTROL_THANKS_TEXT}</p>
  <br />

 
  <p class='style5' align='justify'>{AUTOCONTROL_THANKS_CLOSE}</p>
  <br />
 
   <p>&nbsp;</p>

<!-- END autocontrol_thanks -->


<!-- BEGIN resume_error -->
     <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',10000); </SCRIPT>
  <p class='style5' align='center'>{resume_error.RESUME_ERROR_TITLE}</p>

  <p class='style5' align='justify'>{resume_error.RESUME_ERROR_TEXT}</p>

  <p class='style5' align='justify'>{resume_error.RESUME_ERROR_CLOSE}</p>
  <br />
  
   <p>&nbsp;</p>

<!-- END resume_error -->

<!-- BEGIN contact_us_form -->

<p class='style5' align='center'>{contact_us_form.TEACHER_FORM_HEADING}</p>

  <p class='style5' align='justify'>{contact_us_form.TEACHER_FORM_TEXT}</p>

  <br />

<form action='form.php' method='post' enctype="application/x-www-form-urlencoded">
<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
  <tr>
    <td width='50%'>{contact_us_form.TEACHER_FORM_NAME}</td>
    <td><input type='text' name='email_name' size='20' maxlength='20' /></td>
  </tr>
  <tr>
    <td width='50%'>{contact_us_form.TEACHER_FORM_EMAIL}</td>
    <td><input type='text' name='email' size='30' maxlength='30' /></td>
  </tr>
  <tr>
    <td width='50%'>{contact_us_form.TEACHER_FORM_SUBJECT}</td>
    <td><input type='text' name='subject' size='30' maxlength='30' />
    <input type='hidden' name='contact_us' value='contact_us' /></td>
  </tr>
  <tr>
    <td width='50%'>{contact_us_form.TEACHER_FORM_TXT}</td>
    <td><textarea name='text' name='text' cols='50' rows='10'></textarea></td>
  </tr>
  <tr>
    <td colspan='2' align='center' >
	<input type='reset' name='reset' value='{RESET}' />
	<input type='submit' name='submit' value='{SUBMIT}' /> 
    </td>
  </tr>
</table>
</form>

<!-- END contact_us_form -->

<!-- BEGIN timetable_form -->

<p class='style5' align='center'>{timetable_form.TIMETABLE_TITLE}</p>

  <p class='style5' align='justify'>{timetable_form.TIMETABLE_EXPLANATION}</p>

  <br />

<form action='form.php' method='post' enctype="application/x-www-form-urlencoded">
<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
    
    <tr>
     <td>
	 <label>{timetable_form.TIMETABLE_FROM_ZONE}
	       <select name='from_zone' id='from_zone'>
	    	{TIMEZONE_COUNTRIES_HOME}
	       </select>
 	 </label>
     </td>
     <td>
	<label>{timetable_form.TIMETABLE_FORM_TIME}
    		<select name='from_hour' id='from_hour'>
		  {24_HOURS_TIMEZONE_HOME}
	        </select>
	</label><input type='hidden' name='timetable' value='timetable' /> 
     </td>
     <td>
    		<select name='from_min' id='from_min'>
	 		<option value='00'>[Select]</option>
			{MIN_TIMEZONE_HOME}
	        </select> 
	
     </td>	
     <td><label>{timetable_form.TIMETABLE_T0_ZONE}
       <select name='to_zone' id='to_zone'>
	{TIMEZONE_COUNTRIES}
       </select>
	</label>
     </td>
    </tr>
    <tr>
     <td colspan='2' align='center' >
	<input type='reset' name='reset' value='{RESET}' />
	<input type='submit' name='submit' value='{SUBMIT}' /> 
     </td>
    </tr>
   </table>
  </form>

<!-- END timetable_form -->

<!-- BEGIN return_timetable -->

<p class='style5' align='center'>RETURN</p>

<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
  <tr>
    <td>{FROM_TIME} : {FROM_ZONE} => {EQUIV_TIME} : {TIME_ZONE} </td>
  </tr>
</table>

<!-- END return_timetable -->

<!-- BEGIN email_form -->

<p class='style5' align='center'>{email_form.EMAIL_FORM}</p>

<form action='form.php' method='post' enctype="application/x-www-form-urlencoded">
<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
	{CONTACT_SEARCH}
   <tr>
    <td width='100%' colspan='2'>
        <span id="check_box" ></span>
    </td>
  </tr>
  <tr>
    <td width='50%'>{email_form.EMAIL_FORM_EMAIL}</td>
    <td>
	<input type="text" id="email" autocomplete="off" name="email" />
	<div id="email_choices" class="autocomplete">
	</div>

	<script type="text/javascript" language="javascript" >
	// <![CDATA[
	new Ajax.Autocompleter("email", "email_choices", "control_panel.php", { paramName: "email_search"  } );
	// ]]>
	</script>
    </td>
  </tr>
  <tr>
    <td width='50%'>{email_form.EMAIL_FORM_SUBJECT}</td>
    <td><input type='text' name='subject' size='30' maxlength='30' />
    <input type='hidden' name='email_form' value='email_form' /></td>
  </tr>
  <tr>
    <td width='50%'>{email_form.EMAIL_FORM_TEXT}</td>
    <td><textarea name='email_text' id="email_text" cols='50' rows='10'></textarea></td>
  </tr>
  <tr>
    <td colspan='2' align='center' >
	<input type='reset' name='reset' value='{RESET}' />
	<input type='submit' name='submit' value='{SUBMIT}' /> </td>
  </tr>
</table>
</form>

<!-- END email_form -->

<!-- BEGIN reminder_form -->

<p class='style5' align='center'>{reminder_form.REMINDER}</p>

  <br />

<form action='form.php' method='post' enctype="application/x-www-form-urlencoded"  onSubmit="return check_date();"  >

 <table width='100%' align='center' border='1px single #000000' cellspacing='5' cellpadding='5' >
  <tr>
    <td width='35%'>
	<table align='center'>
	 <tr>
	   <th colspan='2'><b>Reminder Methods</b></th>
	 </tr>
         <tr>
	   <td class='table_color'>Control Panel:</td>
	   <td>YES</td>
         </tr>
         <tr>
 	   <td class='table_color'>SMS Text Message:</td>
	   <td><input type='checkbox' name='sms' /></td>
         </tr>
         <tr class='table_color'>
	   <td>Email:</td>
           <td><input type='checkbox' name='email' /></td>
         </tr>
         <tr><td colspan='2'>&nbsp;</td></tr>
	</table>
    </td>
    <td align='center'>

     <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>

	  <script type="text/javascript">

	  function dateChanged(calendar) {
    		// Beware that this function is called even if the end-user only
    		// changed the month/year.  In order to determine if a date was
   	        // clicked you can use the dateClicked property of the calendar:
    		if (calendar.dateClicked) {
      		// OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
    		  var y = calendar.date.getFullYear();
      		  var m = calendar.date.getMonth();     // integer, 0..11
      		  var d = calendar.date.getDate();      // integer, 1..31
      		// redirect...
     		 document.getElementById('reminder_date').value = m  + "," + d + "," + y ; 
    	  	}
  	  };

	  Calendar.setup(
    		  	{
     	 		flat         : "calendar-container", // ID of the parent element
      			flatCallback : dateChanged           // our callback function
    	  		}
  		);

  	  function check_date()
  		{
		if(document.getElementById('reminder_date').value == "" )
		{
			alert("Please enter a date ...");
			return false;
		}

  	  }	

  	</script>

	</div>

   </td>
  </tr>

<!-- BEGIN reminder_channel -->

  <tr>
    <td colspan='2' align='center' >

 	<select name='user_reminder_switch' >

<!-- BEGIN reminder_users -->

	  <option value='{reminder_form.reminder_channel.reminder_users.REM_USER_ID}' >{reminder_form.reminder_channel.reminder_users.REM_USER_NAME}</option>

<!-- END reminder_users -->

	</select>
    </td>
  </tr>

<!-- END reminder_channel -->

</table>

<table bgcolor='#FFFFCC' width='100%' align='center' border='1' cellspacing='5' cellpadding='5'>
 <input type='hidden' name='reminder_date' id='reminder_date' value='' />
 <input type='hidden' name='user_id' value='{reminder_form.USER_ID}' />
 <input type='hidden' name='reminder' value='reminder' />
  <tr>
    <td width='20%'>9:00AM</td> 
    <td><input type='text' name='9,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td>9:30AM</td> 
    <td><input type='text' name='9,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td >10:00AM</td> 
    <td><input type='text' name='10,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >10:30AM</td> 
    <td><input type='text' name='10,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >11:00AM</td> 
    <td><input type='text' name='11,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >11:30AM</td> 
    <td><input type='text' name='11,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td >12:00AM</td> 
    <td><input type='text' name='12,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >12:30AM</td> 
    <td><input type='text' name='12,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >1:00PM</td> 
    <td><input type='text' name='13,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td >1:30PM</td> 
    <td><input type='text' name='13,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td>2:00PM</td> 
    <td><input type='text' name='14,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td>2:30PM</td> 
    <td><input type='text' name='14,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
   <tr>
    <td >3:00PM</td> 
    <td><input type='text' name='15,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >3:30PM</td> 
    <td><input type='text' name='15,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >4:00PM</td> 
    <td><input type='text' name='16,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>4:30PM</td> 
    <td><input type='text' name='16,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >5:00PM</td> 
    <td><input type='text' name='17,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>5:30PM</td> 
    <td><input type='text' name='17,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >6:00PM</td> 
    <td><input type='text' name='18,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >6:30PM</td> 
    <td><input type='text' name='18,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>7:00PM</td> 
    <td><input type='text' name='19,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>7:30PM</td> 
    <td><input type='text' name='19,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
   </td>
  </tr>
  <tr>
    <td >8:00PM</td> 
    <td><input type='text' name='20,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>8:30PM</td> 
    <td><input type='text' name='20,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>9:00PM</td> 
    <td><input type='text' name='21,00,0' size='50' maxlength='70' /></td>
	 <td><input type='submit' name='submit' value='Send' /></td>  
  </tr>
  <tr>
    <td >9:30PM</td> 
    <td><input type='text' name='21,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>10:00PM</td> 
    <td><input type='text' name='22,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
   </td>
  </tr>
  <tr>
    <td>10:30PM</td> 
    <td><input type='text' name='22,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >11:00PM</td> 
    <td><input type='text' name='23,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >11:30PM</td> 
    <td><input type='text' name='23,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>12:00PM</td> 
    <td><input type='text' name='24,00,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >12:30PM</td> 
    <td><input type='text' name='24,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td >00:00AM</td> 
    <td><input type='text' name='0,0,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
  <tr>
    <td>0:30AM</td> 
    <td><input type='text' name='0,30,0' size='50' maxlength='70' /></td>
    <td><input type='submit' name='submit' value='Send' /></td>
  </tr>
</table>
</form>

<!-- END reminder_form -->

<!-- BEGIN candidate_email_form -->


<script type="text/javascript" >

// place email helper text
// this has to be parsed on this page
// this throws up errors when not on the
// exact page that it's meant to be on

function place_email(text) {
  
	var email_text = ''; 
	var {REPLY_TITLE0} = "{REPLY_TEXT0}";
	var {REPLY_TITLE1} = "{REPLY_TEXT1}" ;
	var {REPLY_TITLE2} = "{REPLY_TEXT2}";
	var {REPLY_TITLE3} = "{REPLY_TEXT3}";
	var {REPLY_TITLE4} = "{REPLY_TEXT4}";
	var {REPLY_TITLE5} = "{REPLY_TEXT5}";
	var {REPLY_TITLE6} = "{REPLY_TEXT6}";
	var {REPLY_TITLE7} = "{REPLY_TEXT7}" ;
	var {REPLY_TITLE8} = "{REPLY_TEXT8}";
	var {REPLY_TITLE9} = "{REPLY_TEXT9}";
	var {REPLY_TITLE10} = "{REPLY_TEXT10}";
	var {REPLY_TITLE11} = "{REPLY_TEXT11}";
	
	switch(text)
	{
		case '{REPLY_TITLE0}' : email_text = {REPLY_TITLE0} ; break ;
		case '{REPLY_TITLE1}' : email_text = {REPLY_TITLE1} ; break ;
		case '{REPLY_TITLE2}' : email_text = {REPLY_TITLE2} ; break ;	
		case '{REPLY_TITLE3}' : email_text = {REPLY_TITLE3} ; break ;
		case '{REPLY_TITLE4}' : email_text = {REPLY_TITLE4} ; break ;
		case '{REPLY_TITLE5}' : email_text = {REPLY_TITLE5} ; break ;
		case '{REPLY_TITLE6}' : email_text = {REPLY_TITLE6} ; break ;
		case '{REPLY_TITLE7}' : email_text = {REPLY_TITLE7} ; break ;
		case '{REPLY_TITLE8}' : email_text = {REPLY_TITLE8} ; break ;	
		case '{REPLY_TITLE9}' : email_text = {REPLY_TITLE9} ; break ;
		case '{REPLY_TITLE10}' : email_text = {REPLY_TITLE10} ; break ;
		case '{REPLY_TITLE11}' : email_text = {REPLY_TITLE11} ; break ;		
		default : break; 
	}

	document.getElementById('email_text').value = email_text;	

}

function place_link(text) {
  
	var email_text = ''; 
	var box_text = '';

	var {REPLY_LINK0} = "{REPLY_LINK_TEXT0}";
	var {REPLY_LINK1} = "{REPLY_LINK_TEXT1}" ;
	var {REPLY_LINK2} = "{REPLY_LINK_TEXT2}";
	var {REPLY_LINK3} = "{REPLY_LINK_TEXT3}";
	var {REPLY_LINK4} = "{REPLY_LINK_TEXT4}";
	var {REPLY_LINK5} = "{REPLY_LINK_TEXT5}";
	
	switch(text)
	{
		case '{REPLY_LINK0}' : email_text = {REPLY_LINK0} ; break ;
		case '{REPLY_LINK1}' : email_text = {REPLY_LINK1} ; break ;
		case '{REPLY_LINK2}' : email_text = {REPLY_LINK2} ; break ;	
		case '{REPLY_LINK3}' : email_text = {REPLY_LINK3} ; break ;
		case '{REPLY_LINK4}' : email_text = {REPLY_LINK4} ; break ;
		case '{REPLY_LINK5}' : email_text = {REPLY_LINK5} ; break ;
		default : break; 
	}

	box_text=document.getElementById('email_text');
	var caret_pos =  getCaretPosition(box_text);
	setCaretPosition(box_text,caret_pos,email_text);	

}

// CaretPosition object
function CaretPosition()
{
 var start = null;
 var end = null;
}

function getCaretPosition(oField)
{

 // Initialise the CaretPosition object
 var oCaretPos = new CaretPosition();

 // IE support
 if(document.selection)
 {
  // Focus on the text box
  oField.focus();

  // This returns us an object containing
  // information about the currently selected text
  var oSel = document.selection.createRange();

  // Find out the length of the selected text
  // (you'll see why below)
  var selectionLength = oSel.text.length;

  // Move the selection start to 0 position.
  //
  // This is where it gets interesting, and this is why
  // some have claimed you can't get the caret positions
  // in IE.
  //
  // IE has no 'selectionStart' or 'selectionEnd' property,
  // so we can not get or set this value directly. We can
  // only move the caret positions relative to where they
  // currently are (this should make more sense when you read
  // the next line of code).
  //
  // Note, that even though we have moved the start
  // position on our object in memory, this is not reflected
  // in the browser until we call oSel.select() (which we're
  // not going to do here).
  //
  // Also note, the start position will never be a negative
  // number, no matter how far we try to move it back.
  oSel.moveStart ('character', -oField.value.length);

  // This is where it should start to make sense. We now know
  // our start caret position is the length of the currently
  // selected text minus the original selection length
  // (think about it).
  oCaretPos.start = oSel.text.length - selectionLength;

  // Since the start of the selection is at the start of the
  // text, we know that the length of the selection is also
  // the index of the end caret position.
  oCaretPos.end = oSel.text.length;
 }
 // Firefox support
 else if(oField.selectionStart || oField.selectionStart == '0')
 {
  // This is a whole lot easier in Firefox
  oCaretPos.start = oField.selectionStart;
  oCaretPos.end = oField.selectionEnd;
 }

 // Return results
 return (oCaretPos.start);

}


function setCaretPosition(oField, iCaretStart, emailText)
{

 // IE Support
 if (document.selection)
 {

  // Focus on the text box
  oField.focus();

  // This returns us an object containing
  // information about the currently selected text
  var oSel = document.selection.createRange();

  // Move selection start and end to 0 position
  oSel.moveStart ('character', -oField.value.length);
 
  // Move selection start and end to desired position
  oSel.moveStart ('character', iCaretStart);
  //oSel.moveEnd ('character', 0);
  oSel.pasteHTML(emailText);

  oField.select() ;

 }

 // Firefox support
 else if(oField.selectionStart || oField.selectionStart == '0')
 {

   // the text that is in the email box
   var text = oField.value;

   // the text before the caret
   var prevText = oField.value.substring(0, iCaretStart);

   // the text after the caret
   var nextText = oField.value.substring(iCaretStart);

   // the new email box text
   oField.value = prevText + '\n' + emailText + '\n' +  nextText ;

   oField.focus();

 }

}

</script>

<p class='style5' align='center'>{candidate_email_form.CANDIDATE_EMAIL_FORM}</p>

<a href="" onclick="window.open('form.php?mode=timetable_form','','scrollbars=yes, menubar=no, resizable=yes,toolbar=no,location=no,status=no');">{candidate_email_form.TIMEZONE}</a>

  <p class='style5' align='center'>{candidate_email_form.CANDIDATE_EMAIL_TEXT}</p>
  <br />
	
<form action='form.php' method='post' enctype="application/x-www-form-urlencoded">
<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
      
	<tr>
	 <td>Calendar Delay</td>
	 <td><input type="text" name="date" id="f_date_b" /><button type="reset" id="f_trigger_b">...</button>
	    <script type="text/javascript">
    	  Calendar.setup({
           inputField     :    "f_date_b",      // id of the input field
           ifFormat       :    "%Y/%m/%d",       // format of the input field
           showsTime      :    true,            // will display a time selector
     	     button         :    "f_trigger_b",   // trigger for the calendar (button ID)
      	  singleClick    :    false,           // double-click mode
     		  step           :    1                // show all years in drop-down boxes (instead of every other year as default)
  		  });
	</script>
	 </td>
	</tr>

  <tr {candidate_email_form.EMAIL_VISIBLE} >
    <td width='50%'>{candidate_email_form.CANDIDATE_EMAIL_EMAIL}</td>
    <td><input type='text' name='email' size='30' value='{candidate_email_form.TEACHER_EMAIL}' /></td>
  </tr>

  <tr>
    <td width='50%'>{candidate_email_form.CANDIDATE_EMAIL_SUBJECT}</td>
    <td><input type='text' name='subject' size='30' value='David ... {SITENAME}' />
    	  <input type='hidden' name='candidate_email_form' value='candidate_email_form' />
	  <input type='hidden' name='candidate_name' value='{CANDIDATE_NAME}' /> 
    </td>
  </tr>
  <tr><td><a href="#" onclick="email_history('{candidate_email_form.TEACHER_EMAIL}');return false;" >Email Text</a></td></tr>
  <tr>
    <td colspan='2' >
	<div id="email_history" style="display:block;" >

	</div>
    </td>
  </tr>

  <tr>
    <td colspan='2' >
	<div id="email_body" style="display:block;" >
	   {EMAIL_BODY}
	</div>
    </td>
  </tr>
  <tr>
    <td colspan='2' align='center' >
     <a href="javascript:place_email('{REPLY_TITLE0}');">{REPLY_TITLE0}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE1}');">{REPLY_TITLE1}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE2}');">{REPLY_TITLE2}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE3}');">{REPLY_TITLE3}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE4}');">{REPLY_TITLE4}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE5}');">{REPLY_TITLE5}</a>&nbsp;&nbsp;
    </td>
  </tr>
  <tr>
    <td colspan='2' align='center' >
     <a href="javascript:place_email('{REPLY_TITLE6}');">{REPLY_TITLE6}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE7}');">{REPLY_TITLE7}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE8}');">{REPLY_TITLE8}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE9}');">{REPLY_TITLE9}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE10}');">{REPLY_TITLE10}</a>&nbsp;&nbsp;
     <a href="javascript:place_email('{REPLY_TITLE11}');">{REPLY_TITLE11}</a>&nbsp;&nbsp;
    </td>
  </tr>
  <tr>
    <td colspan='2' align='center' >
     <a href="javascript:place_link('{REPLY_LINK0}');">{REPLY_LINK0}</a>&nbsp;&nbsp;
     <a href="javascript:place_link('{REPLY_LINK1}');">{REPLY_LINK1}</a>&nbsp;&nbsp;
     <a href="javascript:place_link('{REPLY_LINK2}');">{REPLY_LINK2}</a>&nbsp;&nbsp;
     <a href="javascript:place_link('{REPLY_LINK3}');">{REPLY_LINK3}</a>&nbsp;&nbsp;
     <a href="javascript:place_link('{REPLY_LINK4}');">{REPLY_LINK4}</a>&nbsp;&nbsp;
     <a href="javascript:place_link('{REPLY_LINK5}');">{REPLY_LINK5}</a>&nbsp;&nbsp;
    </td>
  </tr>
  <tr>
    <td width='50%'>{candidate_email_form.CANDIDATE_EMAIL_TEXT}</td>
    <td><textarea name='email_text' id='email_text' cols='50' rows='10'  ></textarea></td>
  </tr>

<!-- BEGIN email_attachment_picture -->
  <tr>
    <td>{candidate_email_form.CANDIDATE_EMAIL_ATTACHMENT}</td>
    <td><img src='uploads/{candidate_email_form.email_attachment_picture.EMAIL_ATTACHMENT_PICTURE}' /></td>
  </tr>
<!-- END email_attachment_picture -->

<!-- BEGIN email_attachment_document -->
  <tr>
    <td>{candidate_email_form.CANDIDATE_EMAIL_ATTACHMENT}</td>
    <td>
	<a href="uploads/{candidate_email_form.email_attachment_document.EMAIL_ATTACHMENT_DOCUMENT}" >
		 {candidate_email_form.email_attachment_document.EMAIL_ATTACHMENT_DOCUMENT}
	</a>
    </td>
  </tr>
<!-- END email_attachment_document -->

  <tr>
    <td colspan='2' align='center' >
	<input type='submit' name='delete' value='{DELETE}' />
	<input type='hidden' name='email_id' value='{EMAIL_ID}' />
	<input type='reset' name='reset' value='Save/Delete' 
               onclick="save_reply('{EMAIL_ID}','{TEACHER_EMAIL}','{CANDIDATE_NAME}')" />
	<input type='submit' name='submit' value='{SUBMIT}' />
    </td>
  </tr>
</table>
	
</form>

<!-- END candidate_email_form -->

<!-- BEGIN school_contact_form -->


<script type="text/javascript" >

// place email helper text
// this has to be parsed on this page
// this throws up errors when not on the
// exact page that it's meant to be on

function place_email(text) {
  
	var email_text = ''; 
	var {REPLY_TITLE0} = "{REPLY_TEXT0}";
	var {REPLY_TITLE1} = "{REPLY_TEXT1}" ;
	var {REPLY_TITLE2} = "{REPLY_TEXT2}";
	var {REPLY_TITLE3} = "{REPLY_TEXT3}";
	var {REPLY_TITLE4} = "{REPLY_TEXT4}";
	var {REPLY_TITLE5} = "{REPLY_TEXT5}";
	var {REPLY_TITLE6} = "{REPLY_TEXT6}";
	var {REPLY_TITLE7} = "{REPLY_TEXT7}" ;
	var {REPLY_TITLE8} = "{REPLY_TEXT8}";
	var {REPLY_TITLE9} = "{REPLY_TEXT9}";
	var {REPLY_TITLE10} = "{REPLY_TEXT10}";
	var {REPLY_TITLE11} = "{REPLY_TEXT11}";
	
	switch(text)
	{
		case '{REPLY_TITLE0}' : email_text = {REPLY_TITLE0} ; break ;
		case '{REPLY_TITLE1}' : email_text = {REPLY_TITLE1} ; break ;
		case '{REPLY_TITLE2}' : email_text = {REPLY_TITLE2} ; break ;	
		case '{REPLY_TITLE3}' : email_text = {REPLY_TITLE3} ; break ;
		case '{REPLY_TITLE4}' : email_text = {REPLY_TITLE4} ; break ;
		case '{REPLY_TITLE5}' : email_text = {REPLY_TITLE5} ; break ;
		case '{REPLY_TITLE6}' : email_text = {REPLY_TITLE6} ; break ;
		case '{REPLY_TITLE7}' : email_text = {REPLY_TITLE7} ; break ;
		case '{REPLY_TITLE8}' : email_text = {REPLY_TITLE8} ; break ;	
		case '{REPLY_TITLE9}' : email_text = {REPLY_TITLE9} ; break ;
		case '{REPLY_TITLE10}' : email_text = {REPLY_TITLE10} ; break ;
		case '{REPLY_TITLE11}' : email_text = {REPLY_TITLE11} ; break ;		
		default : break; 
	}
	
	document.getElementById('email_text').value = email_text;	

}

function place_link(text) {
  
	var email_text = ''; 
	var box_text = '';

	var {REPLY_LINK0} = "{REPLY_LINK_TEXT0}";
	var {REPLY_LINK1} = "{REPLY_LINK_TEXT1}" ;
	var {REPLY_LINK2} = "{REPLY_LINK_TEXT2}";
	var {REPLY_LINK3} = "{REPLY_LINK_TEXT3}";
	var {REPLY_LINK4} = "{REPLY_LINK_TEXT4}";
	var {REPLY_LINK5} = "{REPLY_LINK_TEXT5}";
	
	switch(text)
	{
		case '{REPLY_LINK0}' : email_text = {REPLY_LINK0} ; break ;
		case '{REPLY_LINK1}' : email_text = {REPLY_LINK1} ; break ;
		case '{REPLY_LINK2}' : email_text = {REPLY_LINK2} ; break ;	
		case '{REPLY_LINK3}' : email_text = {REPLY_LINK3} ; break ;
		case '{REPLY_LINK4}' : email_text = {REPLY_LINK4} ; break ;
		case '{REPLY_LINK5}' : email_text = {REPLY_LINK5} ; break ;
		default : break; 
	}
	
	box_text = document.getElementById('email_text').value ;
	box_text = box_text + '\r\n\r\n' + email_text + '\r\n\r\n' ;	

	document.getElementById('email_text').value = box_text ;
}

</script>


<p class='style5' align='center'>{school_contact_form.SCHOOL_CONTACT_FORM}</p>

  <p class='style5' align='center'>{school_contact_form.SCHOOL_CONTACT_TEXT}</p>
  <br />


<form action='form.php' method='post' enctype="application/x-www-form-urlencoded">
<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'> 
{CONTACT_SEARCH}
  <tr>
   <td width='100%' colspan='2'>
        <span id="check_box" ></span>
   </td>
  </tr>
  <tr>
    <td align='center'>{school_contact_form.TELEPHONE}</td>
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.CONTACT}</td>
    <td>
	<input type="text" id="contact" autocomplete="off" name="contact" />
	<div id="contact_choices" class="autocomplete">
	</div>

	<script type="text/javascript" language="javascript">
	// <![CDATA[
	new Ajax.Autocompleter("contact", "contact_choices", "control_panel.php", { paramName: "contact_search"  } );
	// ]]>
	</script>
    </td>
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.ORGANIZATION}</td>
    <td>
	<input type="text" id="organization" autocomplete="off" name="organization" />
	<div id="organization_choices" class="autocomplete">
	</div>

	<script type="text/javascript" language="javascript" >
	// <![CDATA[
	new Ajax.Autocompleter("organization", "organization_choices", "control_panel.php", { paramName: "organization_search"  } );
	// ]]>
	</script>
    </td>
  </tr>
    <tr> 
	<td>{school_contact_form.L_LOCATION}</td> 
	<td><select name='location' id='location' >
  		{LOCATION_BLOCK}
            </select>
	</td>
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.TELEPHONE}</td>
    <td>
	<input type="text" id="telephone" autocomplete="off" name="telephone" />
	<div id="telephone_choices" class="autocomplete">
	</div>

	<script type="text/javascript" language="javascript" >
	// <![CDATA[
	new Ajax.Autocompleter("telephone", "telephone_choices", "control_panel.php", {minChars: 5, paramName: "telephone_search"  } );
	// ]]>
	</script>
	<script type='text/Javascript' >

	</script>
	<a href='javascript:telephone();' id='telephone_link' >Call</a>
    </td>    
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.EMAIL}</td>
    <td>
	<input type="text" id="email_store" autocomplete="off" name="email_store" />
	<div id="email_choices" class="autocomplete">
	</div>

	<script type="text/javascript" language="javascript">
	// <![CDATA[
	new Ajax.Autocompleter("email_store", "email_choices", "control_panel.php", { paramName: "email_search"  } );
	// ]]>
	</script>
    </td>
  </tr>
  <tr>
     <td width='50%'>{school_contact_form.EXPRESS_RESUME}</td>
     <td >
	<select name='express_resume' id='express_resume'>
	 <option value='0'>No</option>
	 <option value='1'>Yes</option>
	</select>
     </td>
  </tr>
   <tr>
    <td width='50%'>{school_contact_form.NOTES}</td>
    <td><textarea name='notes' cols='50' rows='10'></textarea></td>
  </tr>
  <tr>
    <td align='center'>{school_contact_form.EMAIL}</td>
  </tr>
	<tr>
	 <td>Calendar Delay</td>
	 <td><input type="text" name="date" id="f_date_b" /><button type="reset" id="f_trigger_b">...</button>
	    <script type="text/javascript">
    	  Calendar.setup({
           inputField     :    "f_date_b",      // id of the input field
           ifFormat       :    "%Y/%m/%d",       // format of the input field
           showsTime      :    true,            // will display a time selector
     	     button         :    "f_trigger_b",   // trigger for the calendar (button ID)
      	  singleClick    :    false,           // double-click mode
     		  step           :    1                // show all years in drop-down boxes (instead of every other year as default)
  		  });
	</script>
	 
    </td>
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.EMAIL}</td>
    <td><input type='text' name='email' size='30' maxlength='30' /></td>
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.L_SUBJECT}</td>
    <td><input type='text' name='subject' size='30' maxlength='30' value='{RECRUITER_EMAIL_HEADER}' />
    	 
    </td>
  </tr>
  <tr>
    <td width='50%'>{school_contact_form.TEXT} <input type='hidden' name='school_contact_form' /></td>
    <td><textarea name='email_text' id='email_text' cols='50' rows='10'></textarea></td>
  </tr>
  <tr>
    <td colspan='2' align='center' >
	<input type='reset' name='reset' value='{RESET}' />
	<input type='submit' name='submit' value='{SUBMIT}' />  
    </td>
  </tr>

</table>
	
</form>

<!-- END school_contact_form -->

<!-- BEGIN contact_thank_you -->

  <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',5000); </SCRIPT>

  <p class='style5' align='center'>{contact_thank_you.THANK_YOU}</p>

  <p class='style5' align='justify'>{contact_thank_you.CONTACT_THANKS_TEXT}</p>
 
  <p class='style5' align='justify'>{contact_thank_you.CLOSE}</p>
  
  <p>&nbsp;</p>

<!-- END contact_thank_you -->

<!-- BEGIN job0_form -->

    <tr><td colspan='2'>SCHOOL FORM</td></tr>
  <form method="post" enctype="multipart/form-data" action="form.php?mode=school_form"  style="background-color:#FFFFCC" name="school_form" 
        onsubmit="return check_form('check');">
  <table border="0" width="495" align="center" cellspacing="8" cellpadding="2" >
   <tr><td colspan='2'>&nbsp;</td></tr>
<!-- BEGIN name -->
    <tr>
    <td width='50%'>{job0_form.name.L_NAME}<input type='hidden' name='user_reg_id' value='{job0_form.USER_REG_ID}' /></td>
    <td><input type='text' maxlength='30' name='contact' id='contact' class='{job0_form.name.CHECK}' ></td>
   </tr>
<!-- END name -->
<!-- BEGIN school -->
   <tr>
    <td width='50%'>{job0_form.school.L_SCHOOL}</td>
    <td><input type='text' maxlength='30' name='school' id='school' class='{job0_form.school.CHECK}' ></td>
   </tr>
<!-- END school -->
<!-- BEGIN arrival -->
   <tr>
    <td>{job0_form.arrival.L_ARRIVAL}</td>
    <td>
      <select name='arrival' id='arrival' class='{job0_form.arrival.CHECK}' /> 
	{job0_form.arrival.ARRIVAL_BLOCK}
      </select>
    </td>
  </tr>
<!-- END arrival -->
<!-- BEGIN email -->
  <tr>
    <td>{job0_form.email.L_EMAIL}</td>
    <td><input type='text' maxlength='45'  name='email' id='email' class='{job0_form.email.CHECK}' /></td>
  </tr>
<!-- END email -->
<!-- BEGIN phone -->
  <tr>
    <td>{job0_form.phone.L_PHONE}</td>
    <td><input type='text' maxlength='45' name='phone' id='phone' class='{job0_form.phone.CHECK}' /></td>
  </tr>
<!-- END phone -->
<!-- BEGIN school_description -->
  <tr>
    <td>
	<table cellspacing='0' cellpadding='0'>
	 <tr><td><b>{job0_form.school_description.L_SCHOOL_DESCRIPTION}</b></td></tr>
	 <tr><td>* Work shift ? Work days ? Holidays ? Overtime ?</td></tr>
	 <tr><td>* School area ? Subway ? Shops ? University ? Hiking ? Gym ?</td></tr>
 	 <tr><td>* Apartment description ? Washing machine ? Far from school ?</td></tr>
	 <tr><td>* Airfare ? Severance ? Medical ?</td></tr>
	</table>
    </td>
    <td><textarea name='school_description' id='school_description' cols='50' rows='7' class='{job0_form.school_description.CHECK}'  ></textarea>
    </td>
  </tr>
<!-- END school_description -->
<!-- BEGIN salary -->
        <tr>
          <td>{job0_form.salary.L_SALARY}</td>
          <td>
	     <select name='salary' id='salary' class='{job0_form.salary.CHECK}' >
	      {job0_form.salary.SALARY_BLOCK}
	     </select>
  	   </td>
  	 </tr>
<!-- END salary -->
<!-- BEGIN employee_ref -->
  <tr>
    <td>{job0_form.employee_ref.L_EMPLOYEE_REF}
    <br><span class='optional'>(Optional)</span></td>
    <td><textarea name='foreign_teacher' id='foreign_teacher' cols='50' rows='4' class='{job0_form.employee_ref.CHECK}' ></textarea></td>
  </tr>
<!-- END employee_ref -->

<!-- BEGIN students -->
  <tr>
  	 <td>{job0_form.students.L_STUDENTS}</td>	
  	 <td>
  	  <table cellspacing='0' cellpadding='0'>
  	   <tr><td width='50%'>{job0_form.students.L_KINDERGARTEN}</td>
		<td><input type="checkbox" name="kindergarten" value='1' ></td></tr>
  	   <tr><td width='50%'>{job0_form.students.L_ELEMENTARY}</td>
		<td><input type="checkbox" name="elementary" value='1' ></td></tr>
  	   <tr><td width='50%'>{job0_form.students.L_MIDDLE_SCHOOL}:</td>
		<td><input type="checkbox" name="middle_school" value='1'></td></tr>
  	   <tr><td width='50%'>{job0_form.students.L_HIGH_SCHOOL}</td>
		<td><input type="checkbox" name="high_school" value='1' ></td></tr>
  	   <tr><td width='50%'>{job0_form.students.L_ADULTS}</td>
		<td><input type="checkbox" name="adults" value='1' ></td></tr>
  	  </table>
  	 </td>
   </tr>
<!-- END students -->

<!-- BEGIN ticket_money -->
  	<tr>
  	 <td>{job0_form.ticket_money.L_TICKET_MONEY}</td>
  	 <td>
  	 <input type="radio" name="ticket_money" id="ticket_money" value="yes"  class='{job0_form.ticket_money.CHECK}' />
	 {job0_form.ticket_money.YES}&nbsp;
	 <input type="radio" name="ticket_money" id="ticket_money" value="no" class='{job0_form.ticket_money.CHECK}' /> 
	{job0_form.ticket_money.NO}
  	 </td>
  	</tr>
<!-- END ticket_money -->
<!-- BEGIN postal_address -->
      <tr> 
	<td>{job0_form.postal_address.L_POSTAL_ADDRESS}</td> 
        <td><textarea name='postal_address' id="postal_address" cols='50' rows='7'  class='{job0_form.postal_address.CHECK}' ></textarea></td>
	</td>
      </tr>
<!-- END postal_address -->
<!-- BEGIN location -->
      <tr> 
	<td>{job0_form.location.L_LOCATION}</td> 
	<td><select name='city' id='city' class='{job0_form.location.CHECK}' >
      	   {job0_form.location.LOCATION_BLOCK}
	</select>
	</td>
      </tr>
<!-- END location -->
<!-- BEGIN picture -->
  <tr>
   <td >{job0_form.picture.L_PICTURE} 
     <input type='hidden' name='MAX_FILE_SIZE' value='100000' class='{job0_form.picture.CHECK}' /></td>
	<td><input type='file' name='picupload' id='picupload' ></td>
  </tr>
<!-- END picture -->
<!-- BEGIN contract -->
  	<tr>
  	  <td>{job0_form.contract.L_CONTRACT}</td>
  	  <td><input type="file" size="30" name="contract" id="contract" class='{job0_form.contract.CHECK}' /></td>
  	</tr>
<!-- END contract -->
<!-- BEGIN website_address -->
  <tr>
    <td>{job0_form.website_address.L_WEBSITE_ADDRESS}</td>
    <td><input type='text' name='website_address' id="website_address" cols='50' rows='1'  class='{job0_form.website_address.CHECK}' ></td>
  </tr>
<!-- END website_address -->

<!-- BEGIN num_teachers -->
  	 <tr>
  	 <td>{job0_form.num_teachers.L_NUM_TEACHERS}</td>
  	 <td>
  	  <select name="number_teachers" id="number_teachers" class='{job0_form.num_teachers.CHECK}' >
		<option value="1" >1</option>
		<option value="2" >2</option>
		<option value="3" >3</option>
		<option value="4" >4</option>
		<option value="5" >5</option>
		<option value="6" >6</option>
		<option value="7" >7</option>
		<option value="8" >8</option>
		<option value="9" >9</option>
		<option value="10" >10</option>
	  </select>
	 </td>
  	</tr>
<!-- END num_teachers -->

<!-- BEGIN accomodation -->
  	<tr>
  	 <td>{job0_form.accomodation.L_ACCOMODATION}</td>
  	 <td><select name='accomodation' id='accomodation' class='{job0_form.accomodation.CHECK}' >
	   <option value='-1'>{job0_form.accomodation.L_SELECT}</option>
	   <option value='1'>{job0_form.accomodation.L_SINGLE}</option>
           <option value='2'>{job0_form.accomodation.L_SHARED}</option>	 
          </select>
         </td>
        </tr>
<!-- END accomodation -->

<!-- BEGIN gender -->
	<tr>
	 <td>{job0_form.gender.L_GENDER}<br></td>
	 <td>
	   <select name="gender" class='{job0_form.gender.CHECK}' >
		 <option value="Any" >Any</option>
	         <option value="1" >Male</option>
	 	 <option value="2" >Female</option>
		</select>
	 </td>
	</tr>
<!-- END gender -->
    <tr>
    <td colspan='2' align='center'>
     <input type="reset" value="Clear Application">&nbsp;&nbsp;
     <input type="submit" value="Send Application">
    </td>
   </tr>
   </table>
  </form>

<!-- END job0_form -->

<!-- BEGIN school_form_thanks -->
  <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',15000); </SCRIPT>
  <p class='style5' align='center'>{school_form_thanks.THANKS}</p>

  <p class='style5' align='justify'>{school_form_thanks.TEXT} </p>
  
  <p class='style5' align='justify'>{school_form_thanks.CLOSE}</p>
  

<!-- END school_form_thanks -->

<!-- BEGIN school_form_error -->
     <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',10000); </SCRIPT>
     
  <p class='style5' align='center'>{school_form_error.THANKS}</p>

  <p class='style5' align='center'>{school_form_error.TEXT}</p>
  
  <p class='style5' align='center'>{school_form_error.CLOSE}</p>
  <br />
  
   <p>&nbsp;</p>

<!-- END school_form_error -->

<!-- BEGIN job1_form -->
<tr ><td>RECRUITER FORM</td><tr>
  <form method="post" ENCTYPE="multipart/form-data" action="form.php?mode=recruiter_form" style="background-color:#FFFFCC" name="recruiter_form" onSubmit="return check_form('check');">
 <table border="0" width="495" align="center" cellspacing="8" cellpadding="2">
   <tr><td colspan='2'>&nbsp;</td></tr>
<!-- BEGIN contact -->	
   <tr>
    <td width='50%'>{job1_form.contact.L_CONTACT}</td>
    <td><input type='text' maxlength='30' name='contact' id='contact' class='{job1_form.contact.CHECK}' /></td>
   </tr>
<!-- END contact -->

<!-- BEGIN school -->
   <tr>
    <td width='50%'>{job1_form.school.L_SCHOOL}</td>
    <td><input type='text' maxlength='30' name='school' id='school' class='{job1_form.school.CHECK}' /></td>
   </tr>
<!-- END school -->

<!-- BEGIN email -->
  <tr>
    <td width='50%'>{job1_form.email.L_EMAIL}</td>
    <td><input type='text' maxlength='30' name='email' id='email' class='{job1_form.email.CHECK}' /></td>
  </tr>
<!-- END email -->

<!-- BEGIN arrival -->
   <tr>
    <td>{job1_form.arrival.L_ARRIVAL}</td>
    <td>
      <select name='arrival' id='arrival'  class='{job1_form.arrival.CHECK}'>
	{job1_form.arrival.ARRIVAL_LIST}
      </select>
    </td>
  </tr>
<!-- END arrival -->
<!-- BEGIN school_description -->
  <tr>
    <td>{job1_form.school_description.L_SCHOOL_DESCRIPTION}</td>
    <td><textarea name='school_description' id='school_description' cols='40' rows='7' class='{job1_form.school_description.CHECK}' />
	</textarea></td>
  </tr>
<!-- END school_description -->
<!-- BEGIN picture -->
  <tr>
   <td >{job1_form.picture.L_PICTURE} 
     <input type='hidden' name='MAX_FILE_SIZE' value='100000' class='{job1_form.picture.CHECK}' /></td>
	<td><input type='file' name='picupload' id='picupload' ></td>
  </tr>
<!-- END picture -->
<!-- BEGIN location -->
      <tr> 
	<td>{job1_form.location.L_LOCATION}</td> 
	<td><select name='city' id='location' class='{job1_form.location.CHECK}' >
      	   {job1_form.location.LOCATION_BLOCK}
	</select>
	</td>
      </tr>
<!-- END location -->
<!-- BEGIN website_address -->
  <tr>
    <td>{job1_form.website_address.L_WEBSITE_ADDRESS}</td>
    <td><input type='text' name='website_address' id='website_address' cols='50' rows='7'  class='{job1_form.website_address.CHECK}' ></td>
  </tr>
<!-- END website_address -->
<!-- BEGIN students -->
  <tr>
  	 <td>{job1_form.students.L_STUDENTS}</td>	
  	 <td>
  	  <table cellspacing='0' cellpadding='0'>
  	   <tr><td width='50%'>{job1_form.students.L_KINDERGARTEN}</td>
		<td><input type="checkbox" name="kindergarten" value='1' ></td></tr>
  	   <tr><td width='50%'>{job1_form.students.L_ELEMENTARY}</td>
		<td><input type="checkbox" name="elementary" value='1' ></td></tr>
  	   <tr><td width='50%'>{job1_form.students.L_MIDDLE_SCHOOL}:</td>
		<td><input type="checkbox" name="middle_school" value='1'></td></tr>
  	   <tr><td width='50%'>{job1_form.students.L_HIGH_SCHOOL}</td>
		<td><input type="checkbox" name="high_school" value='1' ></td></tr>
  	   <tr><td width='50%'>{job1_form.students.L_ADULTS}</td>
		<td><input type="checkbox" name="adults" value='1' ></td></tr>
  	  </table>
  	 </td>
   </tr>
<!-- END students -->

<!-- BEGIN ticket_money -->
  	<tr>
  	 <td>{job1_form.ticket_money.L_TICKET_MONEY}</td>
  	 <td>
  	 <input type="radio" name="ticket_money" value="yes"  class='{job1_form.ticket_money.CHECK}' />
	 {job1_form.ticket_money.YES}&nbsp;
	 <input type="radio" name="ticket_money" value="no" class='{job1_form.ticket_money.CHECK}' /> 
	{job1_form.ticket_money.NO}
  	 </td>
  	</tr>
<!-- END ticket_money -->

<!-- BEGIN accomodation -->
  	<tr>
  	 <td>{job1_form.accomodation.L_ACCOMODATION}</td>
  	 <td><select name='accomodation' id='accomodation' class='{job1_form.accomodation.CHECK}' >
	   	<option value='-1'>{job1_form.accomodation.L_SELECT}</option>
	   	<option value='1'>{job1_form.accomodation.L_SINGLE}</option>
           	<option value='2'>{job1_form.accomodation.L_SHARED}</option>	 
           </select>
    	</td>
  	</tr>
<!-- END accomodation -->

<!-- BEGIN contract -->
  	<tr>
  	  <td>{job1_form.contract.L_CONTRACT}</td>
  	  <td><input type="file" size="30" name="contract" id="contract" class='{job1_form.contract.CHECK}' /></td>
  	</tr>
<!-- END contract -->

<!-- BEGIN teacher_description -->
        <tr>
    	  <td>{job1_form.teacher_description.L_TEACHER_DESCRIPTION}</td>
          <td><textarea name='teacher_description' id="teacher_description" cols='40' rows='7' class='{job1_form.teacher_description.CHECK}' />
	      </textarea></td>
   	</tr>
<!-- END teacher_description -->

<!-- BEGIN salary -->
   	<tr>
    	  <td>{job1_form.salary.L_SALARY}</td>
          <td>
           <select name='salary' id='salary' class='{job1_form.salary.CHECK}' >
		{job1_form.salary.SALARY_BLOCK}
	   </select>
  	 </td>
  	</tr>
<!-- END salary -->
<!-- BEGIN num_teachers -->
  	<tr>
  	 <td>{job1_form.num_teachers.L_NUM_TEACHERS}</td>
  	 <td>
  	  <select name="number_teachers" id="number_teachers" class='{job1_form.num_teachers.CHECK}' >
		<option value='1'>1</option>
		<option value='2'>2</option>
		<option value='3'>3</option>
		<option value='4'>4</option>
		<option value='5'>5</option>
		<option value='6'>6</option>
		<option value='7'>7</option>
		<option value='8'>8</option>
		<option value='9'>9</option>
		<option value='10'>10</option>
	  </select>
	 </td>
  	</tr>
<!-- END num_teachers -->
<!-- BEGIN gender -->
	<tr>
	 <td>{job1_form.gender.L_GENDER}</td>
	 <td>
	   <select name="gender" id="gender" class='{job1_form.gender.CHECK}' >
	     <option value='Any'>{job1_form.gender.L_ANY}</option>
	     <option value='1' >{job1_form.gender.L_MALE}</option>
	     <option value='2' >{job1_form.gender.L_FEMALE}</option>
	    </select>
	 </td>
	</tr>
<!-- END gender  -->
   <tr>
    <td colspan='2' align='center'>
     <input type="reset" value="{RESET}">&nbsp;&nbsp;
     <input type="submit" value="{SUBMIT}">
    </td>
   </tr>
   </table>
  <input type="hidden" name="MAX_FILE_SIZE" value="355000">
  </form>
<!-- END job1_form -->

<!-- BEGIN recruiter_form_thanks -->
  <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',15000); </SCRIPT>
  <p class='style5' align='center'>{recruiter_form_thanks.THANK_YOU}</p>

  <p class='style5' align='justify'>{recruiter_form_thanks.TEXT}</p>
 
  <p class='style5' align='justify'>{recruiter_form_thanks.CLOSE}</p>
  <br />
  
  <p>&nbsp;</p>

<!-- END recruiter_form_thanks -->

<!-- BEGIN recruiter_form_error -->
     <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',10000); </SCRIPT>
     
  <p class='style5' align='center'>{recruiter_form_error.ERROR}</p>

  <p class='style5' align='center'>{recruiter_form_error.TEXT}</p>
  
  <p class='style5' align='center'>{recruiter_form_error.CLOSE}</p>
  <br />
  
   <p>&nbsp;</p>

<!-- END recruiter_form_error -->

<!-- BEGIN admin_form_website -->

        <script language="javascript" type="text/javascript" src="js/tinymce/tiny_mce.js"></script>

	<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "simple",
		editor_selector : "mceEditor"

	});
	</script>

         <table>
	 <tr><th colspan='2'>{admin_form_website.L_WEBSITE_FORM_TITLE}</th></tr>
	 <tr><td colspan='2'>{admin_form_website.L_WEBSITE_FORM_TEXT}<br /><br /> 
	 </td></tr>
	</table>
	
<!-- BEGIN admin_form_text -->

	<form action='form.php' enctype='multipart/form-data' method='post'>
 
	<table id='website_website' >
	 <tr>
	    <td  colspan='2' >

		&nbsp;&nbsp;&nbsp;&nbsp;
		<a onclick="return dis_box('{admin_form_website.admin_form_text.ZONE_NAME}');" href="#">
		   {admin_form_website.admin_form_text.ZONE_NAME}
		</a>
	    </td>
	 </tr>

         <tr>
	   <td>
	    <div id='{admin_form_website.admin_form_text.ZONE_NAME}' style='display:none;' >

	     <table>

	  	<tr><td>{admin_form_website.L_HEADING_TEXT}</td><input type='hidden' name='zone_name' value='{admin_form_website.admin_form_text.ZONE_NAME}' />
	            <td><textarea name='heading_text' rows="1" class="mceEditor" cols="80" >{admin_form_website.admin_form_text.HEADING_TEXT}</textarea></td></tr>
	  	<tr><td>{admin_form_website.L_PAGE1_TITLE}</td>
	            <td><textarea name='page1_title' rows="1" class="mceEditor" cols="80" >{admin_form_website.admin_form_text.PAGE1_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE1_TEXT}</td>
		    <td><textarea name='page1_text' rows='8' class="mceEditor" cols='80' >{admin_form_website.admin_form_text.PAGE1_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE2_TITLE}</td>
		    <td><textarea name='page2_title' rows='1' cols='80' >{admin_form_website.admin_form_text.PAGE2_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE2_TEXT}</td>
		    <td><textarea name='page2_text' rows='8' cols='80' >{admin_form_website.admin_form_text.PAGE2_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE3_TITLE}</td>
		    <td><textarea name='page3_title' rows='1' cols='80' >{admin_form_website.admin_form_text.PAGE3_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE3_TEXT}</td>
		    <td><textarea name='page3_text' rows='8' cols='80' >{admin_form_website.admin_form_text.PAGE3_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE4_TITLE}</td>
		    <td><textarea name='page4_title' rows='1' cols='80' >{admin_form_website.admin_form_text.PAGE4_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE4_TEXT}</td>
		    <td><textarea name='page4_text' rows='8' cols='80' >{admin_form_website.admin_form_text.PAGE4_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE5_TITLE}</td>
		    <td><textarea name='page5_title' rows='1' cols='80' >{admin_form_website.admin_form_text.PAGE5_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE5_TEXT}</td>
		    <td><textarea name='page5_text' rows='8' cols='80' >{admin_form_website.admin_form_text.PAGE5_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS1_TITLE}</td>
	            <td><textarea name='news1_title' rows='1' cols='80' >{admin_form_website.admin_form_text.NEWS1_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS1_TEXT}</td>
	            <td><textarea name='news1_text' rows='8' cols='80' >{admin_form_website.admin_form_text.NEWS1_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS2_TITLE}</td>
                    <td><textarea name='news2_title' rows='1' cols='80' >{admin_form_website.admin_form_text.NEWS2_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS2_TEXT}</td>
	            <td><textarea name='news2_text' rows='8' cols='80' >{admin_form_website.admin_form_text.NEWS2_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS3_TITLE}</td>
	            <td><textarea name='news3_title' rows='1' cols='80' >{admin_form_website.admin_form_text.NEWS3_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS3_TEXT}</td>
	            <td><textarea name='news3_text' rows='8' cols='80' >{admin_form_website.admin_form_text.NEWS3_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.TESTAMONIALS1_TITLE}</td>
    	            <td><textarea name='testamonials1_text' rows='8' cols='80' >{admin_form_website.admin_form_text.TESTAMONIALS1_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.TESTAMONIALS2_TITLE}</td>
                    <td><textarea name='testamonials2_text' rows='8' cols='80' >{admin_form_website.admin_form_text.TESTAMONIALS2_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_EMAIL}</td>
    	            <td><textarea name='email' rows='1' cols='80' >{admin_form_website.admin_form_text.EMAIL}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PHONE}</td>
	            <td><textarea name='phone' rows='1' cols='80' >{admin_form_website.admin_form_text.PHONE}</textarea></td></tr>

	        <tr>
		    <td colspan='2' align='center'>
		     <input type='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;<input type='submit' name='Delete' value='{DELETE}' />&nbsp;&nbsp;&nbsp;<input type='submit' value='{SUBMIT}' />
		    </td>
	        </tr>
            </table>

	</div>

	</td>
       </tr>
     </table>
       
    </form>

<!-- END admin_form_text -->


<!-- BEGIN add_text_zone -->

	<form action='form.php' enctype='multipart/form-data' method='post'>

	<table id='website_website' >
	 <tr>
	    <td  colspan='2' >

		&nbsp;&nbsp;&nbsp;&nbsp;
		<a onclick="return dis_box('add_text_zone');" href="#">
			Add Text Zone
		</a>
	    </td>
	 </tr>

         <tr>

	   <td>

	    <div id='add_text_zone' style='display:none;' >

	     <table>

		<tr>
	          <td>Text Zone</td><input type='hidden' name='add_text_zone' />
		  <td colspan="1" align='left' align='center' >
		      <select name='zone_name' id='text_zone' >
	 		<option value='0'>Default</option>
	 		<option value='1'>Australia</option>
	 		<option value='2'>Canada</option>
	 		<option value='3'>China</option>
	 		<option value='4'>United Kingdom</option>
	 		<option value='5'>India</option>
	 		<option value='6'>Indonesia</option>
	 		<option value='7'>Ireland</option>
	 		<option value='8'>Mexico</option>
	 		<option value='9'>Phillipines</option>
	 		<option value='10'>Thailand</option>	 		
	 		<option value='11'>United States</option>
	 		<option value='12'>New Zealand</option>
	 		<option value='13'>South Africa</option>
	 		<option value='14'>Spain</option>
	 		<option value='15'>South Korea</option>
	 	       </select>	
	  	    </td>
		</tr>	
	  	<tr><td>{admin_form_website.HEADING_TEXT}</td>
	            <td><textarea name='heading_text' rows='1' cols='80' >{admin_form_website.HEADING_TEXT}</textarea></td></tr>
	  	<tr><td>{admin_form_website.L_PAGE1_TITLE}</td>
	            <td><textarea name='page1_title' rows='1' cols='80' >{admin_form_website.PAGE1_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE1_TEXT}</td>
		    <td><textarea name='page1_text' rows='8' cols='80' >{admin_form_website.PAGE1_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE2_TITLE}</td>
		    <td><textarea name='page2_title' rows='1' cols='80' >{admin_form_website.PAGE2_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE2_TEXT}</td>
		    <td><textarea name='page2_text' rows='8' cols='80' >{admin_form_website.PAGE2_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE3_TITLE}</td>
		    <td><textarea name='page3_title' rows='1' cols='80' >{admin_form_website.PAGE3_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE3_TEXT}</td>
		    <td><textarea name='page3_text' rows='8' cols='80' >{admin_form_website.PAGE3_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE4_TITLE}</td>
		    <td><textarea name='page4_title' rows='1' cols='80' >{admin_form_website.PAGE4_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE4_TEXT}</td>
		    <td><textarea name='page4_text' rows='8' cols='80' >{admin_form_website.PAGE4_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE5_TITLE}</td>
		    <td><textarea name='page5_title' rows='1' cols='80' >{admin_form_website.PAGE5_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PAGE5_TEXT}</td>
		    <td><textarea name='page5_text' rows='8' cols='80' >{admin_form_website.PAGE5_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS1_TITLE}</td>
	            <td><textarea name='news1_title' rows='1' cols='80' >{admin_form_website.NEWS1_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS1_TEXT}</td>
	            <td><textarea name='news1_text' rows='8' cols='80' >{admin_form_website.NEWS1_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS2_TITLE}</td>
                    <td><textarea name='news2_title' rows='1' cols='80' >{admin_form_website.NEWS2_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS2_TEXT}</td>
	            <td><textarea name='news2_text' rows='8' cols='80' >{admin_form_website.NEWS2_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS3_TITLE}</td>
	            <td><textarea name='news3_title' rows='1' cols='80' >{admin_form_website.NEWS3_TITLE}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_NEWS3_TEXT}</td>
	            <td><textarea name='news3_text' rows='8' cols='80' >{admin_form_website.NEWS3_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.TESTAMONIALS1_TITLE}</td>
    	            <td><textarea name='testamonials1_text' rows='8' cols='80' >{admin_form_website.TESTAMONIALS1_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.TESTAMONIALS2_TITLE}</td>
                    <td><textarea name='testamonials2_text' rows='8' cols='80' >{admin_form_website.TESTAMONIALS2_TEXT}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_EMAIL}</td>
    	            <td><textarea name='email' rows='1' cols='80' >{admin_form_website.EMAIL}</textarea></td></tr>
	        <tr><td>{admin_form_website.L_PHONE}</td>
	            <td><textarea name='phone' rows='1' cols='80' >{admin_form_website.PHONE}</textarea></td></tr>

	        <tr><td colspan='2' align='center'>
		    <input type='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;<input type='submit' value='{SUBMIT}' />
		    </td>
	        </tr>
            </table>

	    </div>

	   </td>
	</tr>

	</table>

	</form>

<!-- END add_text_zone -->

<!-- END admin_form_website -->

<!-- BEGIN admin_form_select -->

	<table><tr><th>Form Selection:</th></tr><tr><td>Select the values of the forms that your users will see.</td></tr></table>

	<form action='form.php' enctype='multipart/form-data' method='post'>
	<br /><br />
        <a onclick="javascript:dis_box('teacher_resume_box');" href="#">TEACHERS</a>
	<table><tr><td>Select the setting of your teachers resume form.</td></tr></table>
        <div id='teacher_resume_box'  style="display:none;" >

        <table align='center' width='80%' border='0px'>
	
	 <tr><th>Setting:</th><th>Appear:</th><th>Obligatory:</th><th>Security Level:</th></tr>
	 <tr><th></th><th align='left'>ON&nbsp;&nbsp;&nbsp;&nbsp;OFF</th><th align='left'>ON&nbsp;&nbsp;&nbsp;&nbsp;OFF</th><th align='left'></th></tr>

	 <tr>
	   <td>{admin_form_select.L_RESOURCE}</td>
	   <td>
	       <input type='radio' name='can0_upload_resource' value='1' {RESOURCE_ON} />&nbsp;
	       <input type='radio' name='can0_upload_resource' value='0' {RESOURCE_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='can0_upload_resource_check' value='1' {RESOURCE_CHECK_ON} />&nbsp;
	     <input type='radio' name='can0_upload_resource_check' value='0' {RESOURCE_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_resource_sec'>
		<option {RESOURCE_SEC_0} value='0' >0</option>
		<option {RESOURCE_SEC_1} value='1' >1</option>
		<option {RESOURCE_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_NAME}</td>
	   <td>
	       <input type='radio' name='can0_upload_name' value='1' {NAME_ON} />&nbsp;
	       <input type='radio' name='can0_upload_name' value='0' {NAME_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='can0_upload_name_check' value='1' {NAME_CHECK_ON} />&nbsp;
	     <input type='radio' name='can0_upload_name_check' value='0' {NAME_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_name_sec'>
		<option {NAME_SEC_0} value='0' >0</option>
		<option {NAME_SEC_1} value='1' >1</option>
		<option {NAME_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_BIRTHDAY}</td>
	   <td>
	       <input type='radio' name='can0_upload_birthday' value='1' {BIRTHDAY_ON} />&nbsp;
	       <input type='radio' name='can0_upload_birthday' value='0' {BIRTHDAY_OFF} />	
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_birthday_check' value='1' {BIRTHDAY_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_birthday_check' value='0' {BIRTHDAY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_birthday_sec'>
		<option value='0' {BIRTHDAY_SEC_0} >0</option>
		<option value='1' {BIRTHDAY_SEC_1} >1</option>
		<option value='2' {BIRTHDAY_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_NATIONALITY}</td>
	   <td>
	       <input type='radio' name='can0_upload_nationality' value='1' {NATIONALITY_ON} />&nbsp;
	       <input type='radio' name='can0_upload_nationality' value='0' {NATIONALITY_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_nationality_check' value='1' {NATIONALITY_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_nationality_check' value='0' {NATIONALITY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_nationality_sec'>
		<option value='0' {NATIONALITY_SEC_0} >0</option>
		<option value='1' {NATIONALITY_SEC_1} >1</option>
		<option value='2' {NATIONALITY_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_EMAIL}</td>
	   <td>
	       <input type='radio' name='can0_upload_email' value='1' {EMAIL_ON} />&nbsp;
	       <input type='radio' name='can0_upload_email' value='0' {EMAIL_OFF} />	
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_email_check' value='1' {EMAIL_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_email_check' value='0' {EMAIL_CHECK_OFF}  />
	   </td>
	   <td>
	     <select name='can0_upload_email_sec'>
		<option value='0' {EMAIL_SEC_0} >0</option>
		<option value='1' {EMAIL_SEC_1} >1</option>
		<option value='2' {EMAIL_SEC_2} >2</option>
	     </select>
	   </td>
         </tr
	 <tr>
	   <td>{admin_form_select.L_TELEPHONE}</td>
	   <td>
	       <input type='radio' name='can0_upload_telephone' value='1' {TELEPHONE_ON} />&nbsp;
	       <input type='radio' name='can0_upload_telephone' value='0' {TELEPHONE_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_telephone_check' value='1' {TELEPHONE_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_telephone_check' value='0' {TELEPHONE_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_telephone_sec'>
		<option value='0' {TELEPHONE_SEC_0} >0</option>
		<option value='1' {TELEPHONE_SEC_1} >1</option>
		<option value='2' {TELEPHONE_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_RINGTIME}</td>
	   <td>
	       <input type='radio' name='can0_upload_ringtime' value='1' {RINGTIME_ON} />&nbsp;
	       <input type='radio' name='can0_upload_ringtime' value='0' {RINGTIME_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_ringtime_check' value='1' {RINGTIME_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_ringtime_check' value='0' {RINGTIME_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_ringtime_sec'>
		<option value='0' {RINGTIME_SEC_0} >0</option>
		<option value='1' {RINGTIME_SEC_1} >1</option>
		<option value='2' {RINGTIME_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_LOCATION}</td>
	   <td>
	       <input type='radio' name='can0_upload_location' value='1' {LOCATION_ON} />&nbsp;
	       <input type='radio' name='can0_upload_location' value='0' {LOCATION_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='can0_upload_location_check' value='1' {LOCATION_CHECK_ON} />&nbsp;
	     <input type='radio' name='can0_upload_location_check' value='0' {LOCATION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_location_sec'>
		<option {LOCATION_SEC_0} value='0' >0</option>
		<option {LOCATION_SEC_1} value='1' >1</option>
		<option {LOCATION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_YOUTUBE}</td>
	   <td>
	       <input type='radio' name='can0_upload_youtube' value='1' {YOUTUBE_ON} />&nbsp;
	       <input type='radio' name='can0_upload_youtube' value='0' {YOUTUBE_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_youtube_check' value='1' {YOUTUBE_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_youtube_check' value='0' {YOUTUBE_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_arrival_sec'>
		<option value='0' {YOUTUBE_SEC_0} >0</option>
		<option value='1' {YOUTUBE_SEC_1} >1</option>
		<option value='2' {YOUTUBE_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_ARRIVAL}</td>
	   <td>
	       <input type='radio' name='can0_upload_arrival' value='1' {ARRIVAL_ON} />&nbsp;
	       <input type='radio' name='can0_upload_arrival' value='0' {ARRIVAL_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_arrival_check' value='1' {ARRIVAL_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_arrival_check' value='0' {ARRIVAL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_arrival_sec'>
		<option value='0' {ARRIVAL_SEC_0} >0</option>
		<option value='1' {ARRIVAL_SEC_1} >1</option>
		<option value='2' {ARRIVAL_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_GENDER}</td>
	   <td>	       
		<input type='radio' name='can0_upload_gender' value='1' {GENDER_ON} />&nbsp;
	        <input type='radio' name='can0_upload_gender' value='0' {GENDER_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_gender_check' value='1' {GENDER_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_gender_check' value='0' {GENDER_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_gender_sec'>
		<option value='0' {GENDER_SEC_0} >0</option>
		<option value='1' {GENDER_SEC_1} >1</option>
		<option value='2' {GENDER_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_INKOREA}</td>
	   <td>
	       <input type='radio' name='can0_upload_inkorea' value='1' {INKOREA_ON} />&nbsp;
	       <input type='radio' name='can0_upload_inkorea' value='0' {INKOREA_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_inkorea_check' value='1' {INKOREA_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_inkorea_check' value='0' {INKOREA_CHECK_OFF} />	
	   </td>
	   <td>
	     <select name='can0_upload_inkorea_sec'>
		<option value='0' {INKOREA_SEC_0} >0</option>
		<option value='1' {INKOREA_SEC_1} >1</option>
		<option value='2' {INKOREA_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_EDUCATION}</td>
	   <td>
	       <input type='radio' name='can0_upload_education' value='1' {EDUCATION_ON} />&nbsp;
	       <input type='radio' name='can0_upload_education' value='0' {EDUCATION_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_education_check' value='1' {EDUCATION_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_education_check' value='0' {EDUCATION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_education_sec'>
		<option value='0' {EDUCATION_SEC_0} >0</option>
		<option value='1' {EDUCATION_SEC_1} >1</option>
		<option value='2' {EDUCATION_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_EXPERIENCE}</td>
	   <td>
	       <input type='radio' name='can0_upload_experience' value='1' {EXPERIENCE_ON} />&nbsp;
	       <input type='radio' name='can0_upload_experience' value='0' {EXPERIENCE_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_experience_check' value='1' {EXPERIENCE_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_experience_check' value='0' {EXPERIENCE_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_experience_sec'>
		<option value='0' {EXPERIENCE_SEC_0} >0</option>
		<option value='1' {EXPERIENCE_SEC_1} >1</option>
		<option value='2' {EXPERIENCE_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_INTRODUCTION}</td>
	   <td>
	       <input type='radio' name='can0_upload_introduction' value='1' {INTRODUCTION_ON} />&nbsp;
	       <input type='radio' name='can0_upload_introduction' value='0' {INTRODUCTION_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_introduction_check' value='1' {INTRODUCTION_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_introduction_check' value='0' {INTRODUCTION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_introduction_sec'>
		<option value='0' {INTRODUCTION_SEC_0} >0</option>
		<option value='1' {INTRODUCTION_SEC_1} >1</option>
		<option value='2' {INTRODUCTION_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	   <td>{admin_form_select.L_PICTURE}</td>
	   <td>
	       <input type='radio' name='can0_upload_picture' value='1' {PICTURE_ON} />&nbsp;
	       <input type='radio' name='can0_upload_picture' value='0' {PICTURE_OFF} />
	   </td>
	   <td>
	       <input type='radio' name='can0_upload_picture_check' value='1' {PICTURE_CHECK_ON} />&nbsp;
	       <input type='radio' name='can0_upload_picture_check' value='0' {PICTURE_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='can0_upload_picture_sec'>
		<option value='0' {PICTURE_SEC_0} >0</option>
		<option value='1' {PICTURE_SEC_1} >1</option>
		<option value='2' {PICTURE_SEC_2} >2</option>
	     </select>
	   </td>
         </tr>
	 <tr>
	 	<td colspan='2' align='center' >
				<input type='reset' name='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;
			       <input type='submit' name='submit' value='{SUBMIT}' />
		</td>
	  </tr>
	 </table>
	</div>

	<form action='form.php' enctype='multipart/form-data' method='post'>
	<br /><br />
        <a href="#" onclick="javascript:dis_box('school_box');" >SCHOOL JOBS</a>
	<table><tr><td>Select the setting of your school jobs form.</td></tr></table>
        <div id='school_box' style="display:none;" >

        <table align='center' width='80%' border='0px'>
	
	 <tr><th>Setting:</th><th>Appear:</th><th>Obligatory:</th><th>Security Level:</th></tr>
	 <tr><th></th><th align='left'>ON&nbsp;&nbsp;&nbsp;&nbsp;OFF</th><th align='left'>ON&nbsp;&nbsp;&nbsp;&nbsp;OFF</th>
			<th align='left'></th></tr>

	 <tr>
	   <td>{admin_form_select.L_CONTACT}</td>
	   <td>
	       <input type='radio' name='job0_upload_contact' value='1' {JOB0_CONTACT_ON} />&nbsp;
	       <input type='radio' name='job0_upload_contact' value='0' {JOB0_CONTACT_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_contact_check' value='1' {JOB0_CONTACT_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_contact_check' value='0' {JOB0_CONTACT_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_contact_sec'>
		<option {JOB0_CONTACT_SEC_0} value='0' >0</option>
		<option {JOB0_CONTACT_SEC_1} value='1' >1</option>
		<option {JOB0_CONTACT_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_SCHOOL}</td>
	   <td>
	       <input type='radio' name='job0_upload_school' value='1' {JOB0_SCHOOL_ON} />&nbsp;
	       <input type='radio' name='job0_upload_school' value='0' {JOB0_SCHOOL_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_school_check' value='1' {JOB0_SCHOOL_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_school_check' value='0' {JOB0_SCHOOL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_school_sec'>
		<option {JOB0_SCHOOL_SEC_0} value='0' >0</option>
		<option {JOB0_SCHOOL_SEC_1} value='1' >1</option>
		<option {JOB0_SCHOOL_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>
	
	 <tr>
	   <td>{admin_form_select.L_ARRIVAL}</td>
	   <td>
	       <input type='radio' name='job0_upload_arrival' value='1' {JOB0_ARRIVAL_ON} />&nbsp;
	       <input type='radio' name='job0_upload_arrival' value='0' {JOB0_ARRIVAL_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_arrival_check' value='1' {JOB0_ARRIVAL_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_arrival_check' value='0' {JOB0_ARRIVAL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_arrival_sec'>
		<option {JOB0_ARRIVAL_SEC_0} value='0' >0</option>
		<option {JOB0_ARRIVAL_SEC_1} value='1' >1</option>
		<option {JOB0_ARRIVAL_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_EMAIL}</td>
	   <td>
	       <input type='radio' name='job0_upload_email' value='1' {JOB0_EMAIL_ON} />&nbsp;
	       <input type='radio' name='job0_upload_email' value='0' {JOB0_EMAIL_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_email_check' value='1' {JOB0_EMAIL_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_email_check' value='0' {JOB0_EMAIL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_email_sec'>
		<option {JOB0_EMAIL_SEC_0} value='0' >0</option>
		<option {JOB0_EMAIL_SEC_1} value='1' >1</option>
		<option {JOB0_EMAIL_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_SCHOOL_DESCRIPTION}</td>
	   <td>
	       <input type='radio' name='job0_upload_school_description' value='1' {JOB0_SCHOOL_DESCRIPTION_ON} />&nbsp;
	       <input type='radio' name='job0_upload_school_description' value='0' {JOB0_SCHOOL_DESCRIPTION_OFF} />	
	   </td>
	   <td>
            <input type='radio' name='job0_upload_school_description_check' value='1' {JOB0_SCHOOL_DESCRIPTION_CHECK_ON} />&nbsp;
	    <input type='radio' name='job0_upload_school_description_check' value='0' {JOB0_SCHOOL_DESCRIPTION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_school_description_sec'>
		<option {JOB0_SCHOOL_DESCRIPTION_SEC_0} value='0' >0</option>
		<option {JOB0_SCHOOL_DESCRIPTION_SEC_1} value='1' >1</option>
		<option {JOB0_SCHOOL_DESCRIPTION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_CITY}</td>
	   <td>
	       <input type='radio' name='job0_upload_city' value='1' {JOB0_CITY_ON} />&nbsp;
	       <input type='radio' name='job0_upload_city' value='0' {JOB0_CITY_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_city_check' value='1' {JOB0_CITY_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_city_check' value='0' {JOB0_CITY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_city_sec'>
		<option {JOB0_CITY_SEC_0} value='0' >0</option>
		<option {JOB0_CITY_SEC_1} value='1' >1</option>
		<option {JOB0_CITY_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_WEBSITE_ADDRESS}</td>
	   <td>
	       <input type='radio' name='job0_upload_website_address' value='1' {JOB0_WEBSITE_ADDRESS_ON} />&nbsp;
	       <input type='radio' name='job0_upload_website_address' value='0' {JOB0_WEBSITE_ADDRESS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_website_address_check' value='1' {JOB0_WEBSITE_ADDRESS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_website_address_check' value='0' {JOB0_WEBSITE_ADDRESS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_website_address_sec'>
		<option {JOB0_WEBSITE_ADDRESS_SEC_0} value='0' >0</option>
		<option {JOB0_WEBSITE_ADDRESS_SEC_1} value='1' >1</option>
		<option {JOB0_WEBSITE_ADDRESS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_ACCOMODATION}</td>
	   <td>
	       <input type='radio' name='job0_upload_accomodation' value='1' {JOB0_ACCOMODATION_ON} />&nbsp;
	       <input type='radio' name='job0_upload_accomodation' value='0' {JOB0_ACCOMODATION_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_accomodation_check' value='1' {JOB0_ACCOMODATION_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_accomodation_check' value='0' {JOB0_ACCOMODATION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_accomodation_sec'>
		<option {JOB0_ACCOMODATION_SEC_0} value='0' >0</option>
		<option {JOB0_ACCOMODATION_SEC_1} value='1' >1</option>
		<option {JOB0_ACCOMODATION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_FOREIGN_TEACHER}</td>
	   <td>
	       <input type='radio' name='job0_upload_foreign_teacher' value='1' {JOB0_FOREIGN_TEACHER_ON} />&nbsp;
	       <input type='radio' name='job0_upload_foreign_teacher' value='0' {JOB0_FOREIGN_TEACHER_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_foreign_teacher_check' value='1' {JOB0_FOREIGN_TEACHER_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_foreign_teacher_check' value='0' {JOB0_FOREIGN_TEACHER_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_foreign_teacher_sec'>
		<option {JOB0_FOREIGN_TEACHER_SEC_0} value='0' >0</option>
		<option {JOB0_FOREIGN_TEACHER_SEC_1} value='1' >1</option>
		<option {JOB0_FOREIGN_TEACHER_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_CONTRACT}</td>
	   <td>
	       <input type='radio' name='job0_upload_contract' value='1' {JOB0_CONTRACT_ON} />&nbsp;
	       <input type='radio' name='job0_upload_contract' value='0' {JOB0_CONTRACT_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_contract_check' value='1' {JOB0_CONTRACT_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_contract_check' value='0' {JOB0_CONTRACT_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_contract_sec'>
		<option {JOB0_CONTRACT_SEC_0} value='0' >0</option>
		<option {JOB0_CONTRACT_SEC_1} value='1' >1</option>
		<option {JOB0_CONTRACT_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_PICUPLOAD}</td>
	   <td>
	       <input type='radio' name='job0_upload_picupload' value='1' {JOB0_PICUPLOAD_ON} />&nbsp;
	       <input type='radio' name='job0_upload_picupload' value='0' {JOB0_PICUPLOAD_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_picupload_check' value='1' {JOB0_PICUPLOAD_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_picupload_check' value='0' {JOB0_PICUPLOAD_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_picupload_sec'>
		<option {JOB0_PICUPLOAD_SEC_0} value='0' >0</option>
		<option {JOB0_PICUPLOAD_SEC_1} value='1' >1</option>
		<option {JOB0_PICUPLOAD_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_TICKET_MONEY}</td>
	   <td>
	       <input type='radio' name='job0_upload_ticket_money' value='1' {JOB0_TICKET_MONEY_ON} />&nbsp;
	       <input type='radio' name='job0_upload_ticket_money' value='0' {JOB0_TICKET_MONEY_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_ticket_money_check' value='1' {JOB0_TICKET_MONEY_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_ticket_money_check' value='0' {JOB0_TICKET_MONEY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_ticket_money_sec'>
		<option {JOB0_TICKET_MONEY_SEC_0} value='0' >0</option>
		<option {JOB0_TICKET_MONEY_SEC_1} value='1' >1</option>
		<option {JOB0_TICKET_MONEY_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_SALARY}</td>
	   <td>
	       <input type='radio' name='job0_upload_salary' value='1' {JOB0_SALARY_ON} />&nbsp;
	       <input type='radio' name='job0_upload_salary' value='0' {JOB0_SALARY_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_salary_check' value='1' {JOB0_SALARY_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_salary_check' value='0' {JOB0_SALARY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_salary_sec'>
		<option {JOB0_SALARY_SEC_0} value='0' >0</option>
		<option {JOB0_SALARY_SEC_1} value='1' >1</option>
		<option {JOB0_SALARY_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_STUDENTS}</td>
	   <td>
	       <input type='radio' name='job0_upload_students' value='1' {JOB0_STUDENTS_ON} />&nbsp;
	       <input type='radio' name='job0_upload_students' value='0' {JOB0_STUDENTS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_students_check' value='1' {JOB0_STUDENTS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_students_check' value='0' {JOB0_STUDENTS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_students_sec'>
		<option {JOB0_STUDENTS_SEC_0} value='0' >0</option>
		<option {JOB0_STUDENTS_SEC_1} value='1' >1</option>
		<option {JOB0_STUDENTS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_NUMBER_TEACHERS}</td>
	   <td>
	       <input type='radio' name='job0_upload_number_teachers' value='1' {JOB0_NUMBER_TEACHERS_ON} />&nbsp;
	       <input type='radio' name='job0_upload_number_teachers' value='0' {JOB0_NUMBER_TEACHERS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_number_teachers_check' value='1' {JOB0_NUMBER_TEACHERS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_number_teachers_check' value='0' {JOB0_NUMBER_TEACHERS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_number_teachers_sec'>
		<option {JOB0_NUMBER_TEACHERS_SEC_0} value='0' >0</option>
		<option {JOB0_NUMBER_TEACHERS_SEC_1} value='1' >1</option>
		<option {JOB0_NUMBER_TEACHERS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_TEACHER_DESCRIPTION}</td>
	   <td>
	       <input type='radio' name='job0_upload_teacher_description' value='1' {JOB0_TEACHER_DESCRIPTION_ON} />&nbsp;
	       <input type='radio' name='job0_upload_teacher_description' value='0' {JOB0_TEACHER_DESCRIPTION_OFF} />	
	   </td>
	   <td>
          <input type='radio' name='job0_upload_teacher_description_check' value='1' {JOB0_TEACHER_DESCRIPTION_CHECK_ON} />&nbsp;
          <input type='radio' name='job0_upload_teacher_description_check' value='0' {JOB0_TEACHER_DESCRIPTION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_teacher_description_sec'>
		<option {JOB0_TEACHER_DESCRIPTION_SEC_0} value='0' >0</option>
		<option {JOB0_TEACHER_DESCRIPTION_SEC_1} value='1' >1</option>
		<option {JOB0_TEACHER_DESCRIPTION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_GENDER}</td>
	   <td>
	       <input type='radio' name='job0_upload_gender' value='1' {JOB0_GENDER_ON} />&nbsp;
	       <input type='radio' name='job0_upload_gender' value='0' {JOB0_GENDER_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_gender_check' value='1' {JOB0_GENDER_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_gender_check' value='0' {JOB0_GENDER_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_gender_sec'>
		<option {JOB0_GENDER_SEC_0} value='0' >0</option>
		<option {JOB0_GENDER_SEC_1} value='1' >1</option>
		<option {JOB0_GENDER_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_PHONE}</td>
	   <td>
	       <input type='radio' name='job0_upload_phone' value='1' {JOB0_PHONE_ON} />&nbsp;
	       <input type='radio' name='job0_upload_phone' value='0' {JOB0_PHONE_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_phone_check' value='1' {JOB0_PHONE_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_phone_check' value='0' {JOB0_PHONE_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_phone_sec'>
		<option {JOB0_PHONE_SEC_0} value='0' >0</option>
		<option {JOB0_PHONE_SEC_1} value='1' >1</option>
		<option {JOB0_PHONE_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>


	 <tr>
	   <td>{admin_form_select.L_POSTAL_ADDRESS}</td>
	   <td>
	       <input type='radio' name='job0_upload_postal_address' value='1' {JOB0_POSTAL_ADDRESS_ON} />&nbsp;
	       <input type='radio' name='job0_upload_postal_address' value='0' {JOB0_POSTAL_ADDRESS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job0_upload_postal_address_check' value='1' {JOB0_POSTAL_ADDRESS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job0_upload_postal_address_check' value='0' {JOB0_POSTAL_ADDRESS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job0_upload_postal_address_sec'>
		<option {JOB0_POSTAL_ADDRESS_SEC_0} value='0' >0</option>
		<option {JOB0_POSTAL_ADDRESS_SEC_1} value='1' >1</option>
		<option {JOB0_POSTAL_ADDRESS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>


	 <tr>
	 	<td colspan='2' align='center' >
				<input type='reset' name='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;
			       <input type='submit' name='submit' value='{SUBMIT}' />
		</td>
	  </tr>
	 </table>
	</div>

	</form>

	<form action='form.php' enctype='multipart/form-data' method='post'>
	<br /><br />
        <a href="#" onclick="javascript:dis_box('recruiter_box');"  >RECRUITER JOBS</a
	<table><tr><td>Select the setting of your recruiter jobs form.</td></tr></table>
        <div id='recruiter_box' style="display:none;" >

        <table align='center' width='80%' border='0px'>
	
	 <tr><th>Setting:</th><th>Appear:</th><th>Obligatory:</th><th>Security Level:</th></tr>
	 <tr><th></th><th align='left'>ON&nbsp;&nbsp;&nbsp;&nbsp;OFF</th><th align='left'>ON&nbsp;&nbsp;&nbsp;&nbsp;OFF</th>          <th align='left'></th></tr>

	 <tr>
	   <td>{admin_form_select.L_CONTACT}</td>
	   <td>
	       <input type='radio' name='job1_upload_contact' value='1' {JOB1_CONTACT_ON} />&nbsp;
	       <input type='radio' name='job1_upload_contact' value='0' {JOB1_CONTACT_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_contact_check' value='1' {JOB1_CONTACT_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_contact_check' value='0' {JOB1_CONTACT_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_contact_sec'>
		<option {JOB1_CONTACT_SEC_0} value='0' >0</option>
		<option {JOB1_CONTACT_SEC_1} value='1' >1</option>
		<option {JOB1_CONTACT_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_SCHOOL}</td>
	   <td>
	       <input type='radio' name='job1_upload_school' value='1' {JOB1_SCHOOL_ON} />&nbsp;
	       <input type='radio' name='job1_upload_school' value='0' {JOB1_SCHOOL_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_school_check' value='1' {JOB1_SCHOOL_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_school_check' value='0' {JOB1_SCHOOL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_school_sec'>
		<option {JOB1_SCHOOL_SEC_0} value='0' >0</option>
		<option {JOB1_SCHOOL_SEC_1} value='1' >1</option>
		<option {JOB1_SCHOOL_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>
	
	 <tr>
	   <td>{admin_form_select.L_ARRIVAL}</td>
	   <td>
	       <input type='radio' name='job1_upload_arrival' value='1' {JOB1_ARRIVAL_ON} />&nbsp;
	       <input type='radio' name='job1_upload_arrival' value='0' {JOB1_ARRIVAL_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_arrival_check' value='1' {JOB1_ARRIVAL_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_arrival_check' value='0' {JOB1_ARRIVAL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_arrival_sec'>
		<option {JOB1_ARRIVAL_SEC_0} value='0' >0</option>
		<option {JOB1_ARRIVAL_SEC_1} value='1' >1</option>
		<option {JOB1_ARRIVAL_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_EMAIL}</td>
	   <td>
	       <input type='radio' name='job1_upload_email' value='1' {JOB1_EMAIL_ON} />&nbsp;
	       <input type='radio' name='job1_upload_email' value='0' {JOB1_EMAIL_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_email_check' value='1' {JOB1_EMAIL_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_email_check' value='0' {JOB1_EMAIL_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_email_sec'>
		<option {JOB1_EMAIL_SEC_0} value='0' >0</option>
		<option {JOB1_EMAIL_SEC_1} value='1' >1</option>
		<option {JOB1_EMAIL_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_SCHOOL_DESCRIPTION}</td>
	   <td>
	       <input type='radio' name='job1_upload_school_description' value='1' {JOB1_SCHOOL_DESCRIPTION_ON} />&nbsp;
	       <input type='radio' name='job1_upload_school_description' value='0' {JOB1_SCHOOL_DESCRIPTION_OFF} />	
	   </td>
	   <td>
            <input type='radio' name='job1_upload_school_description_check' value='1' {JOB1_SCHOOL_DESCRIPTION_CHECK_ON} />&nbsp;
	    <input type='radio' name='job1_upload_school_description_check' value='0' {JOB1_SCHOOL_DESCRIPTION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_school_description_sec'>
		<option {JOB1_SCHOOL_DESCRIPTION_SEC_0} value='0' >0</option>
		<option {JOB1_SCHOOL_DESCRIPTION_SEC_1} value='1' >1</option>
		<option {JOB1_SCHOOL_DESCRIPTION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_CITY}</td>
	   <td>
	       <input type='radio' name='job1_upload_city' value='1' {JOB1_CITY_ON} />&nbsp;
	       <input type='radio' name='job1_upload_city' value='0' {JOB1_CITY_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_city_check' value='1' {JOB1_CITY_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_city_check' value='0' {JOB1_CITY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_city_sec'>
		<option {JOB1_CITY_SEC_0} value='0' >0</option>
		<option {JOB1_CITY_SEC_1} value='1' >1</option>
		<option {JOB1_CITY_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_WEBSITE_ADDRESS}</td>
	   <td>
	       <input type='radio' name='job1_upload_website_address' value='1' {JOB1_WEBSITE_ADDRESS_ON} />&nbsp;
	       <input type='radio' name='job1_upload_website_address' value='0' {JOB1_WEBSITE_ADDRESS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_website_address_check' value='1' {JOB1_WEBSITE_ADDRESS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_website_address_check' value='0' {JOB1_WEBSITE_ADDRESS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_website_address_sec'>
		<option {JOB1_WEBSITE_ADDRESS_SEC_0} value='0' >0</option>
		<option {JOB1_WEBSITE_ADDRESS_SEC_1} value='1' >1</option>
		<option {JOB1_WEBSITE_ADDRESS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_ACCOMODATION}</td>
	   <td>
	       <input type='radio' name='job1_upload_accomodation' value='1' {JOB1_ACCOMODATION_ON} />&nbsp;
	       <input type='radio' name='job1_upload_accomodation' value='0' {JOB1_ACCOMODATION_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_accomodation_check' value='1' {JOB1_ACCOMODATION_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_accomodation_check' value='0' {JOB1_ACCOMODATION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_accomodation_sec'>
		<option {JOB1_ACCOMODATION_SEC_0} value='0' >0</option>
		<option {JOB1_ACCOMODATION_SEC_1} value='1' >1</option>
		<option {JOB1_ACCOMODATION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_FOREIGN_TEACHER}</td>
	   <td>
	       <input type='radio' name='job1_upload_foreign_teacher' value='1' {JOB1_FOREIGN_TEACHER_ON} />&nbsp;
	       <input type='radio' name='job1_upload_foreign_teacher' value='0' {JOB1_FOREIGN_TEACHER_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_foreign_teacher_check' value='1' {JOB1_FOREIGN_TEACHER_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_foreign_teacher_check' value='0' {JOB1_FOREIGN_TEACHER_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_foreign_teacher_sec'>
		<option {JOB1_FOREIGN_TEACHER_SEC_0} value='0' >0</option>
		<option {JOB1_FOREIGN_TEACHER_SEC_1} value='1' >1</option>
		<option {JOB1_FOREIGN_TEACHER_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_CONTRACT}</td>
	   <td>
	       <input type='radio' name='job1_upload_contract' value='1' {JOB1_CONTRACT_ON} />&nbsp;
	       <input type='radio' name='job1_upload_contract' value='0' {JOB1_CONTRACT_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_contract_check' value='1' {JOB1_CONTRACT_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_contract_check' value='0' {JOB1_CONTRACT_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_contract_sec'>
		<option {JOB1_CONTRACT_SEC_0} value='0' >0</option>
		<option {JOB1_CONTRACT_SEC_1} value='1' >1</option>
		<option {JOB1_CONTRACT_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_PICUPLOAD}</td>
	   <td>
	       <input type='radio' name='job1_upload_picupload' value='1' {JOB1_PICUPLOAD_ON} />&nbsp;
	       <input type='radio' name='job1_upload_picupload' value='0' {JOB1_PICUPLOAD_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_picupload_check' value='1' {JOB1_PICUPLOAD_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_picupload_check' value='0' {JOB1_PICUPLOAD_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_picupload_sec'>
		<option {JOB1_PICUPLOAD_SEC_0} value='0' >0</option>
		<option {JOB1_PICUPLOAD_SEC_1} value='1' >1</option>
		<option {JOB1_PICUPLOAD_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_TICKET_MONEY}</td>
	   <td>
	       <input type='radio' name='job1_upload_ticket_money' value='1' {JOB1_TICKET_MONEY_ON} />&nbsp;
	       <input type='radio' name='job1_upload_ticket_money' value='0' {JOB1_TICKET_MONEY_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_ticket_money_check' value='1' {JOB1_TICKET_MONEY_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_ticket_money_check' value='0' {JOB1_TICKET_MONEY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_ticket_money_sec'>
		<option {JOB1_TICKET_MONEY_SEC_0} value='0' >0</option>
		<option {JOB1_TICKET_MONEY_SEC_1} value='1' >1</option>
		<option {JOB1_TICKET_MONEY_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_SALARY}</td>
	   <td>
	       <input type='radio' name='job1_upload_salary' value='1' {JOB1_SALARY_ON} />&nbsp;
	       <input type='radio' name='job1_upload_salary' value='0' {JOB1_SALARY_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_salary_check' value='1' {JOB1_SALARY_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_salary_check' value='0' {JOB1_SALARY_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_salary_sec'>
		<option {JOB1_SALARY_SEC_0} value='0' >0</option>
		<option {JOB1_SALARY_SEC_1} value='1' >1</option>
		<option {JOB1_SALARY_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_STUDENTS}</td>
	   <td>
	       <input type='radio' name='job1_upload_students' value='1' {JOB1_STUDENTS_ON} />&nbsp;
	       <input type='radio' name='job1_upload_students' value='0' {JOB1_STUDENTS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_students_check' value='1' {JOB1_STUDENTS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_students_check' value='0' {JOB1_STUDENTS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_students_sec'>
		<option {JOB1_STUDENTS_SEC_0} value='0' >0</option>
		<option {JOB1_STUDENTS_SEC_1} value='1' >1</option>
		<option {JOB1_STUDENTS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_NUMBER_TEACHERS}</td>
	   <td>
	       <input type='radio' name='job1_upload_number_teachers' value='1' {JOB1_NUMBER_TEACHERS_ON} />&nbsp;
	       <input type='radio' name='job1_upload_number_teachers' value='0' {JOB1_NUMBER_TEACHERS_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_number_teachers_check' value='1' {JOB1_NUMBER_TEACHERS_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_number_teachers_check' value='0' {JOB1_NUMBER_TEACHERS_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_number_teachers_sec'>
		<option {JOB1_NUMBER_TEACHERS_SEC_0} value='0' >0</option>
		<option {JOB1_NUMBER_TEACHERS_SEC_1} value='1' >1</option>
		<option {JOB1_NUMBER_TEACHERS_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_TEACHER_DESCRIPTION}</td>
	   <td>
	       <input type='radio' name='job1_upload_teacher_description' value='1' {JOB1_TEACHER_DESCRIPTION_ON} />&nbsp;
	       <input type='radio' name='job1_upload_teacher_description' value='0' {JOB1_TEACHER_DESCRIPTION_OFF} />	
	   </td>
	   <td>
          <input type='radio' name='job1_upload_teacher_description_check' value='1' {JOB1_TEACHER_DESCRIPTION_CHECK_ON} />&nbsp;
          <input type='radio' name='job1_upload_teacher_description_check' value='0' {JOB1_TEACHER_DESCRIPTION_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_teacher_description_sec'>
		<option {JOB1_TEACHER_DESCRIPTION_SEC_0} value='0' >0</option>
		<option {JOB1_TEACHER_DESCRIPTION_SEC_1} value='1' >1</option>
		<option {JOB1_TEACHER_DESCRIPTION_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	   <td>{admin_form_select.L_GENDER}</td>
	   <td>
	       <input type='radio' name='job1_upload_gender' value='1' {JOB1_GENDER_ON} />&nbsp;
	       <input type='radio' name='job1_upload_gender' value='0' {JOB1_GENDER_OFF} />	
	   </td>
	   <td>
             <input type='radio' name='job1_upload_gender_check' value='1' {JOB1_GENDER_CHECK_ON} />&nbsp;
	     <input type='radio' name='job1_upload_gender_check' value='0' {JOB1_GENDER_CHECK_OFF} />
	   </td>
	   <td>
	     <select name='job1_upload_gender_sec'>
		<option {JOB1_GENDER_SEC_0} value='0' >0</option>
		<option {JOB1_GENDER_SEC_1} value='1' >1</option>
		<option {JOB1_GENDER_SEC_2} value='2' >2</option>
	     </select>
	   </td>
         </tr>

	 <tr>
	 	<td colspan='2' align='center' >
				<input type='reset' name='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;
			       <input type='submit' name='submit' value='{SUBMIT}' />
		</td>
	  </tr>
	 </table>
	</div>

	</form>

<!-- END admin_form_select -->

<!-- BEGIN admin_email_temp -->
	<form action='form.php' enctype='multipart/form-data' method='post'>
        <table>
	 <tr><th colspan='2'>{admin_email_temp.L_AUTOMATIC_EMAIL_RESPONSE}</th></tr>
	 <tr><td colspan='2'>{admin_email_temp.L_AUTOMATIC_EMAIL_RESPONSE_TEXT}<br /><br /> 
	 </td></tr>
	</table>
	<table id='automatic_email_response' >
         <tr><td><input type='text' name='reply_text0_title' value='{admin_email_temp.REPLY_TEXT0_TITLE}'  /></td>
	      <td><textarea name='reply_text0' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT0}</textarea></td></tr>
         <tr><td><input type='text' name='reply_text1_title' value='{admin_email_temp.REPLY_TEXT1_TITLE}'  /></td>
	      <td><textarea name='reply_text1' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT1}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text2_title' value='{admin_email_temp.REPLY_TEXT2_TITLE}'  /></td>
	      <td><textarea name='reply_text2' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT2}</textarea></td></tr>
         <tr><td><input type='text' name='reply_text3_title' value='{admin_email_temp.REPLY_TEXT3_TITLE}'  /></td>
	      <td><textarea name='reply_text3' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT3}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text4_title' value='{admin_email_temp.REPLY_TEXT4_TITLE}'  /></td>
	      <td><textarea name='reply_text4' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT4}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text5_title' value='{admin_email_temp.REPLY_TEXT5_TITLE}' /></td>
	      <td><textarea name='reply_text5' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT5}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text6_title' value='{admin_email_temp.REPLY_TEXT6_TITLE}' /></td>
	      <td><textarea name='reply_text6' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT6}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text7_title' value='{admin_email_temp.REPLY_TEXT7_TITLE}' /></td>
	      <td><textarea name='reply_text7' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT7}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text8_title' value='{admin_email_temp.REPLY_TEXT8_TITLE}' /></td>
	      <td><textarea name='reply_text8' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT8}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text9_title' value='{admin_email_temp.REPLY_TEXT9_TITLE}' /></td>
	      <td><textarea name='reply_text9' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT9}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text10_title' value='{admin_email_temp.REPLY_TEXT10_TITLE}' /></td>
	      <td><textarea name='reply_text10' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT10}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_text11_title' value='{admin_email_temp.REPLY_TEXT11_TITLE}' /></td>
	      <td><textarea name='reply_text11' rows='8' cols='60' >{admin_email_temp.REPLY_TEXT11}</textarea></td></tr>
	 <tr><td><input type='text' name='reply_link0' value='{admin_email_temp.REPLY_LINK0}' /></td>
	      <td><input name='reply_link_text0' size='60' value='{admin_email_temp.REPLY_LINK_TEXT0}' /></td></tr>
	 <tr><td><input type='text' name='reply_link1' value='{admin_email_temp.REPLY_LINK1}' /></td>
	      <td><input name='reply_link_text1' size='60' value='{admin_email_temp.REPLY_LINK_TEXT1}' /></td></tr>
	 <tr><td><input type='text' name='reply_link2' value='{admin_email_temp.REPLY_LINK2}' /></td>
	      <td><input name='reply_link_text2' size='60' value='{admin_email_temp.REPLY_LINK_TEXT2}' /></td></tr>
	 <tr><td><input type='text' name='reply_link3' value='{admin_email_temp.REPLY_LINK3}' /></td>
	      <td><input name='reply_link_text3' size='60' value='{admin_email_temp.REPLY_LINK_TEXT3}' /></td></tr>
	 <tr><td><input type='text' name='reply_link4' value='{admin_email_temp.REPLY_LINK4}' /></td>
	      <td><input name='reply_link_text4' size='60' value='{admin_email_temp.REPLY_LINK_TEXT4}' /></td></tr>
	 <tr><td><input type='text' name='reply_link5' value='{admin_email_temp.REPLY_LINK5}' /></td>
	      <td><input name='reply_link_text5' size='60' value='{admin_email_temp.REPLY_LINK_TEXT5}' /></td></tr>
	 <tr>
	     <td colspan='2' align='center' >
		<input type='reset' name='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;
  	        <input type='submit' name='submit' value='{SUBMIT}' />
	     </td>
	  </tr>
	</table>
       </form>

<!-- END admin_email_temp -->

<!-- BEGIN admin_action_monitor -->

	<form action='form_select.php?mode=admin_action_monitor' name="day_form" enctype='multipart/form-data' method='get'>
	<table id='admin_action_monitor' width='90%' >
	  <tr><th align='left'>{admin_action_monitor.L_ADMIN_ACTION_MONITOR}</th>
	      <td rowspan='2' ><input type='hidden' name='mode' value='admin_action_monitor' />
		<select name="dates" onchange="document.day_form.submit()" >
		  <option value="-1">Days</option>
		  <option {STAMP_SELECTED0} value="{admin_action_monitor.STAMP_DAY0}">{admin_action_monitor.L_TODAY}</option>
		  <option {STAMP_SELECTED1} value="{admin_action_monitor.STAMP_DAY1}">{admin_action_monitor.L_YESTERDAY}</option>
		  <option {STAMP_SELECTED2} value="{admin_action_monitor.STAMP_DAY2}">{admin_action_monitor.DAY_2}</option>
		  <option {STAMP_SELECTED3} value="{admin_action_monitor.STAMP_DAY3}">{admin_action_monitor.DAY_3}</option>
		  <option {STAMP_SELECTED4} value="{admin_action_monitor.STAMP_DAY4}">{admin_action_monitor.DAY_4}</option>
		  <option {STAMP_SELECTED5} value="{admin_action_monitor.STAMP_DAY5}">{admin_action_monitor.DAY_5}</option>
		  <option {STAMP_SELECTED6} value="{admin_action_monitor.STAMP_DAY6}">{admin_action_monitor.DAY_6}</option>
		  <option {STAMP_SELECTED7} value="{admin_action_monitor.STAMP_DAY7}">{admin_action_monitor.DAY_7}</option>
		  <option {STAMP_SELECTED8} value="{admin_action_monitor.STAMP_DAY8}">{admin_action_monitor.DAY_8}</option>
		  <option {STAMP_SELECTED9} value="{admin_action_monitor.STAMP_DAY9}">{admin_action_monitor.DAY_9}</option>
		</select >
		&nbsp;&nbsp;
		<select name="users"   >
		  <option value="-1">User</option>

<!-- BEGIN admin_monitor_users -->		 

		  {admin_action_monitor.admin_monitor_users.USERS}

<!-- END admin_monitor_users -->

		</select >
	     </td>
	  </tr>
	  <tr><td align='left' >{admin_action_monitor.L_ADMIN_ACTION_MONITOR_TEXT}</td></tr>
	  <tr><td colspan='2'>&nbsp;</td></tr>
	  <tr>
	   <td colspan='2' >
	    <table width='80%' align='center' >
	     <tr><th>USER</th><th>TIME</th><th>ACTION</th><th>LINK</th></tr>

<!-- BEGIN admin_action_stats -->

             <tr>
	      <td>{admin_action_monitor.admin_action_stats.USER}</td>
	      <td>{admin_action_monitor.admin_action_stats.TIME}</td>
	      <td>{admin_action_monitor.admin_action_stats.ACTION}</td>
	      <td>{admin_action_monitor.admin_action_stats.LINK}</td>
             </tr>

<!-- END action_monitor_stats -->
	    </table>
	  </td>
	 </tr>
	</table>
      </form>

<!-- END admin_action_monitor -->

<!-- BEGIN admin_marketing_stats -->

	<table width='100%'>
	 <tr><th align='left' colspan='4'>{admin_marketing_stats.L_STATS}</th></tr>
	 <tr><td width='50%'>{admin_marketing_stats.L_PAGE1}</td><td>{admin_marketing_stats.PAGE1}</td><td>{admin_marketing_stats.L_HITS}</td><td>{admin_marketing_stats.HITS}</td></tr>	
	 <tr><td width='50%'>{admin_marketing_stats.L_PAGE2}</td><td>{admin_marketing_stats.PAGE2}</td><td>{admin_marketing_stats.L_GUIDE}</td><td>{admin_marketing_stats.GUIDE_HITS}</td></tr>	
	 <tr><td width='50%'>{admin_marketing_stats.L_PAGE3}</td><td>{admin_marketing_stats.PAGE3}</td><td>{admin_marketing_stats.L_CHAT}</td><td>{admin_marketing_stats.CHAT_HITS}</td></tr>	
	 <tr><td width='50%'>{admin_marketing_stats.L_PAGE4}</td><td>{admin_marketing_stats.PAGE4}</td><td>{admin_marketing_stats.L_GAMES}</td><td>{admin_marketing_stats.GAMES_HITS}</td></tr>	
	 <tr><td width='50%'>{admin_marketing_stats.L_PAGE5}</td><td>{admin_marketing_stats.PAGE5}</td><td>{admin_marketing_stats.L_MOVIE}</td><td>{admin_marketing_stats.MOVIE_HITS}</td></tr>
	 <tr><td width='50%'>{admin_marketing_stats.L_PAGE1}</td><td>{admin_marketing_stats.PAGE1}</td><td>{admin_marketing_stats.L_GALLERY}</td><td>{admin_marketing_stats.GALLERY_HITS}</td></tr>		
	</table>
	<br />
	<br />
	<table width='100%'>
	  <tr><th colspan='2' align='left'>SPIDERS</th></tr>
	  <tr><td>{admin_marketing_stats.L_ALTA_VISTA_UK}</td><td>{admin_marketing_stats.ALTA_VISTA}</td></tr>
	  <tr><td>{admin_marketing_stats.L_ALEXA}</td><td>{admin_marketing_stats.ALEXA}</td></tr>
	  <tr><td>{admin_marketing_stats.L_DAUM}</td><td>{admin_marketing_stats.DAUM}</td></tr>
	  <tr><td>{admin_marketing_stats.L_EXCITE}</td><td>{admin_marketing_stats.EXCITE}</td></tr>
	  <tr><td>{admin_marketing_stats.L_GOOGLE}</td><td>{admin_marketing_stats.GOOGLE}</td></tr>
	  <tr><td>{admin_marketing_stats.L_INKTOMI}</td><td>{admin_marketing_stats.INKTOMI}</td></tr>
	  <tr><td>{admin_marketing_stats.L_MSNBOT}</td><td>{admin_marketing_stats.MSNBOT}</td></tr>
	</table>
	<br />
	<br />
	<table width='100%'>
	  <tr><th colspan='2' align='left'>SEARCH ENGINE LINKS</th></tr>
	  <tr><td>{admin_marketing_stats.L_GOOGLE}</td><td>{admin_marketing_stats.GOOGLE_LINKS}</td></tr>
	  <tr><td>{admin_marketing_stats.L_YAHOO}</td><td>{admin_marketing_stats.YAHOO_LINKS}</td></tr>
	  <tr><td>{admin_marketing_stats.L_MSN}</td><td>{admin_marketing_stats.MSN_LINKS}</td></tr>
	  <tr><td>{admin_marketing_stats.L_DAUM}</td><td>{admin_marketing_stats.DAUM_LINKS}</td></tr>
	</table>
	<br />
	<br />
	<table width='100%'>
	  <tr><th colspan='2' align='left'>TOP LINKS</th></tr>

<!-- BEGIN top_links -->
	  <tr><td width='85%'><a href='{admin_marketing_stats.top_links.TOP_LINK}' >{admin_marketing_stats.top_links.TOP_LINK}</a></td><td>{admin_marketing_stats.top_links.TOP_LINK_COUNT}</td></tr>
<!-- END top_links -->

	</table>
	<br />
	<br />
	<table >
	  <tr><th>Monthly Chart Usage</th></tr>
	  <tr><td><img src='includes/graphs.php?mode=log_track' border='1'  /></td></tr>  
	</table>
	<br />
	<br />
	<table width="100%">
	  <tr><th colspan='4'>SEO Results</th></tr>
	  <tr><td width="50%">{admin_marketing_stats.GOOGLE_SERP1}</td><td>{admin_marketing_stats.GOOGLE_SERP2}</td>
	  <tr><td>{admin_marketing_stats.YAHOO_SERP1}</td><td>{admin_marketing_stats.YAHOO_SERP2}</td>
	  <tr><td>{admin_marketing_stats.MSN_SERP1}</td><td>{admin_marketing_stats.MSN_SERP2}</td>
	  <tr>
	   <td><img src='includes/graphs.php?mode=seo_track&keywords=English+Teaching+Korea' border='1'  /></td>
	   <td><img src='includes/graphs.php?mode=seo_track&keywords=Teaching+English+Korea' border='1'  /></td>
	  </tr>
	</table>
	<br />
	<br />
	<table >
	  <tr> 
            <th colspan='2' >Comptetior Site Tracking 
	    </th>
	  </tr>

<!-- BEGIN site_tracker -->
	  <tr><td>{admin_marketing_stats.site_tracker.SITE_TRACK}</td><td>{admin_marketing_stats.site_tracker.SITE_NUMBER}</td></tr>
<!-- END site_tracker -->

	</table>
	
<!-- END admin_marketing_stats -->


<!-- BEGIN add_recruiter -->
	
<!-- BEGIN recruiter_resume -->


    <table width='100%' align='center' >

      <tr>
	<td>
	  <a href='#' onclick="return dis_box('{add_recruiter.recruiter_resume.RECRUITER_NAME}');" >
	     {add_recruiter.recruiter_resume.RECRUITER_NAME}
	  </a>
	</td>
      </tr>

      <tr>
        <td width='100%'>
	 <div id='{add_recruiter.recruiter_resume.RECRUITER_NAME}' style='display:none;' >

        <form action='form_select.php' enctype='multipart/form-data' method='post' 
		name='{add_recruiter.recruiter_resume.RECRUITER_FORM_NAME}' >
         <table width='100%' align='center' border='0' cellspacing='5' cellpadding='5'>
            <tr>
              <td colspan='2' align='center' ><img src='' /></td>
            </tr>
            <tr>
              <td width='50%'>{add_recruiter.L_NAME}</td>
    	      <td><input type='text' maxlength='30' id='name' name='name' style='border:0px;' 
                         value='{add_recruiter.recruiter_resume.RECRUITER_NAME}' /></td>
            </tr>
	    <tr>
		<td width='10%'>{add_recruiter.L_ACTIVE}</td>
		<td><input type='checkbox' name='active' {add_recruiter.recruiter_resume.RECRUITER_ACTIVE} /></td>	
      	    </tr>
	    <tr>
		<td>{add_recruiter.L_TELEPHONE}</td>
		<td>
			{add_recruiter.L_CALL_TO}<input type='checkbox' name='user_call_to'  {add_recruiter.recruiter_resume.RECRUITER_CALL_TO} />&nbsp;&nbsp;
			{add_recruiter.L_SIP}<input type='checkbox' name='user_sip'  {add_recruiter.recruiter_resume.RECRUITER_SIP} />&nbsp;&nbsp;
			{add_recruiter.L_CALLBACK}<input type='checkbox' name='user_callback'  {add_recruiter.recruiter_resume.RECRUITER_CALLBACK} />&nbsp;&nbsp;
		</td>
	    </tr>
	    <tr>
		<td>{add_recruiter.L_ANONYMOUS}</td>
		<td>
			{add_recruiter.L_SHOW_EMAIL}<input type='radio' name='user_show_email' value='on'  {add_recruiter.recruiter_resume.RECRUITER_SHOW_EMAIL_YES}  />&nbsp;&nbsp;
			{add_recruiter.L_NO_EMAIL}<input type='radio' name='user_show_email' value='off' {add_recruiter.recruiter_resume.RECRUITER_SHOW_EMAIL_NO} />&nbsp;&nbsp;
			<br /><br />
			{add_recruiter.L_SHOW_NUMBER}<input type='radio' name='user_show_number' value='on' {add_recruiter.recruiter_resume.RECRUITER_SHOW_NUMBER_YES} />&nbsp;&nbsp;
			{add_recruiter.L_NO_NUMBER}<input type='radio' name='user_show_number' value='off' {add_recruiter.recruiter_resume.RECRUITER_SHOW_NUMBER_NO} />&nbsp;&nbsp;
		</td>
	    </tr>
	    <tr>
		<td width='10%'>{add_recruiter.L_LAST_LOGIN}</td>
		<td>
		 {add_recruiter.recruiter_resume.RECRUITER_LAST_LOGIN}
		</td>	
      	    </tr>
	    <tr>	
	      <td>Timezone</td>
	      <td>
		<select name='timezone' >
	 		<option value='-1'>Select</option>
	 		<option value='TZ=America/Anchorage' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED1} >
				UTC-9 Anchorage
			</option>
	 	      <option value='TZ=America/Los_Angeles' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED2} >
				UTC-8 Los Angeles, Dawson
			</option>
	 	   	<option value='TZ=America/Edmonton' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED3} >
				UTC-7 Denver, Edmonton
			</option>
	 	   	<option value='TZ=America/Winnipeg' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED4} >
				UTC-6 Winnipeg, Dallas, Chicago
			</option>
	 	   	<option value='TZ=America/Toronto' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED5} >
				UTC-5 New York, Toronto
			</option>
	 	   	<option value='TZ=America/St_Johns' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED6} >
				UTC-4 Newfoundland, St. John's
			</option>
			<option value='TZ=Europe/London' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED7} >
				UTC London, Dublin 
			</option>
	 		<option value='TZ=Europe/Berlin' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED8} >
				UTC+1 Berlin, Rome
			</option>
	 		<option value='TZ=Asia/Bangkok' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED9} >
				UTC+7 Bangkok
			</option>
	 		<option value='TZ=Australia/Perth' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED10} >
				UTC+8 Perth, Beijing
			</option>
	 		<option value='TZ=Asia/Seoul' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED11} >
				UTC+9 Seoul, Alice Springs
			</option>
	 		<option value='TZ=Australia/Sydney' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED12} >
				UTC+10 Sydney, Guam
			</option>
	 		<option value='TZ=Pacific/Auckland' {add_recruiter.recruiter_resume.RECRUITER_TIMEZONE_SELECTED13} >
				UTC+12 Auckland, Fiji
			</option>
		</select>
	      </td>
	    </tr>
	    <tr>
		<td>{add_recruiter.L_LEVEL}</td>
		<td>
		   <select name='level' >
			<option value='0' {add_recruiter.recruiter_resume.RECRUITER_LEVEL_CHECKED0} >GUEST</option>
			<option value='1' {add_recruiter.recruiter_resume.RECRUITER_LEVEL_CHECKED1} >USER</option>
			<option value='2' {add_recruiter.recruiter_resume.RECRUITER_LEVEL_CHECKED2} >ADMIN</option>
		   </select>
		</td>	
      	    </tr>
		<input type='hidden' name='recruiter_resume_edit' 
			value='{add_recruiter.recruiter_resume.RECRUITER_ID}'  />
            <tr>
              <td>{add_recruiter.L_EMAIL}</td>
              <td><input type='text' maxlength='45'  name='email' id='email' style='border:0px;' value='{add_recruiter.recruiter_resume.RECRUITER_EMAIL}'  /></td>
            </tr>
            <tr>
              <td>{add_recruiter.L_PHONE}</td>
              <td><input type='text' maxlength='45'  name='phone' id='phone' style='border:0px;' value='{add_recruiter.recruiter_resume.RECRUITER_MOBILE}' /></td>
            </tr>
            <tr>
              <td>{add_recruiter.L_INTRODUCTION}</td>
              <td><textarea name='introduction' id='introduction' cols='50' rows='7' style='border:0px;' >{add_recruiter.recruiter_resume.RECRUITER_INTRODUCTION}</textarea></td>
            </tr>
            <tr>
              <td>{add_recruiter.L_EXPERIENCE}</td>
              <td><textarea name='experience' id='experience' cols='50' rows='7' style='border:0px;'>{add_recruiter.recruiter_resume.RECRUITER_EXPERIENCE}</textarea></td>
            </tr>
            <tr>
             <td>{add_recruiter.L_EDUCATION}</td>
	     <td>
               <textarea name='university_education' id='university_education' cols='50' rows='7' style='border:0px;' >{add_recruiter.recruiter_resume.RECRUITER_EDUCATION}</textarea>
              </td>
            </tr>
            <tr>
              <td colspan=2 align='center'>
		<input type='button' name='Edit' 
		onclick="mask_recruiter_form('{add_recruiter.recruiter_resume.RECRUITER_FORM_NAME}');" value='Edit' />
		&nbsp;&nbsp;
		<input type='submit' name='Delete' value='Delete' />
		&nbsp;&nbsp;
		<input type='reset' name='Reset' value='Reset' />
		&nbsp;&nbsp;
		<input type='submit' name='Submit' value='Submit' />
	      </td>
  	    </tr>
  	    </table>
 	  </form> 	  

	 </div>
	</td>
      </tr>

    </table>


<!-- END recruiter_resume -->

<!-- BEGIN add_resume -->

    <table width='100%' align='center' >
      <tr>
	<td>
	  <a href='#' onclick="return dis_box('add_recruiter');" >
	     Add Recruiter
	  </a>
	</td>
      </tr>

      <tr>
        <td width='100%'>
	 <div id='add_recruiter' style='display:none;' >

        <form action='form_select.php' enctype='multipart/form-data' method='post'>
         <table width='100%' align='center' border='0' cellspacing='5' cellpadding='5'>
            <tr>
              <td colspan='2' align='center' ><img src='' /></td>
            </tr>
            <tr>
              <td width='50%'>Name</td>
    	      <td><input type='text' maxlength='30' id='name' name='name'  /></td>
            </tr><input type='hidden' name='recruiter_resume_add' />
            <tr>
              <td>Email</td>
              <td><input type='text' maxlength='45'  name='email' id='email'   /></td>
            </tr>
            <tr>
              <td>Password</td>
              <td><input type='password' name='password' id='password'   /></td>
            </tr>
	    <tr>
		<td>{add_recruiter.L_TELEPHONE}</td>
		<td>
			{add_recruiter.L_CALL_TO}<input type='checkbox' name='call_to' />&nbsp;&nbsp;
			{add_recruiter.L_SIP}<input type='checkbox' name='sip' />&nbsp;&nbsp;
			{add_recruiter.L_CALLBACK}<input type='checkbox' name='callback' />&nbsp;&nbsp;
		</td>
	    </tr>
	    <tr>
		<td>{add_recruiter.L_ANONYMOUS}</td>
		<td>
			{add_recruiter.L_SHOW_EMAIL}<input type='radio' name='show_email' />&nbsp;&nbsp;
			{add_recruiter.L_NO_EMAIL}<input type='radio' name='show_email' />&nbsp;&nbsp;
			<br /><br />
			{add_recruiter.L_SHOW_NUMBER}<input type='radio' name='show_number' />&nbsp;&nbsp;
			{add_recruiter.L_NO_NUMBER}<input type='radio' name='show_number' />&nbsp;&nbsp;
		</td>
	    </tr>
	    <tr>	
	      <td>Timezone</td>
	      <td>
		<select name='timezone' >
	 		<option value='-1'>Select</option>
	 		<option value='TZ=America/Anchorage' >UTC-9 Anchorage</option>
	 	   	<option value='TZ=America/Los_Angeles' >UTC-8 Los Angeles, Dawson</option>
	 	   	<option value='TZ=America/Edmonton' >UTC-7 Denver, Edmonton</option>
	 	   	<option value='TZ=America/Winnipeg' >UTC-6 Winnipeg, Dallas, Chicago</option>
	 	   	<option value='TZ=America/Toronto' >UTC-5 New York, Toronto</option>
	 	   	<option value='TZ=America/St_Johns' >UTC-4 Newfoundland, St. John's</option>
			<option value='TZ=Europe/London' >UTC London, Dublin </option>
	 		<option value='TZ=Europe/Berlin' >UTC+1 Berlin, Rome</option>
	 		<option value='TZ=Asia/Bangkok' >UTC+7 Bangkok</option>
	 		<option value='TZ=Australia/Perth' >UTC+8 Perth, Beijing</option>
	 		<option value='TZ=Asia/Seoul' >UTC+9 Seoul, Alice Springs</option>
	 		<option value='TZ=Australia/Sydney' >UTC+10 Sydney, Guam</option>
	 		<option value='TZ=Pacific/Auckland' >UTC+12 Auckland, Fiji</option>
		</select>
	      </td>
	    </tr>
            <tr>
              <td>Phone</td>
              <td><input type='text' maxlength='45'  name='phone' id='phone'   /></td>
            </tr>
            <tr>
              <td>Introduction</td>
              <td><textarea name='introduction' id='introduction' cols='50' rows='7'  ></textarea></td>
            </tr>
            <tr>
              <td>Experience</td>
              <td><textarea name='experience' id='experience' cols='50' rows='7'  ></textarea></td>
            </tr>
            <tr>
             <td>Education</td>
	     <td>
               <textarea name='university_education' id='university_education' cols='50' rows='7'  ></textarea>
              </td>
            </tr>
            <tr>
             <td colspan=2 align='center'><input type='reset' name='Reset' value='Reset' >&nbsp;&nbsp;<input type='submit' name='Submit' value='Submit'></td>

  	     </tr>
  	    </table>
 	  </form> 	  

	 </div>
	</td>
      </tr>

    </table>


<!-- END add_resume -->


<!-- END add_recruiter -->

<!-- BEGIN admin_marketing_campaign -->

    <table width='100%' align='center' >

      <tr><td><a href='#' onclick="return dis_box('free_campaign');" >FREE CAMPAIGN</a></td></tr>
      <tr>
       <td width='100%'>
	<div id='free_campaign' style='display:none;' >

<!-- BEGIN marketing_free_campaign -->

       <table width='100%'>

	<tr>
         <td>	
          <a href='#' onclick="dis_box('{admin_marketing_campaign.marketing_free_campaign.CAMPAIGN_FREE_DIV_NAME}');return false;" >
	    {admin_marketing_campaign.marketing_free_campaign.CAMPAIGN_FREE_NAME}
          </a>
	 </td>
	 <td>&nbsp;</td>
 	</tr>

        <tr  >
	 <td>&nbsp;</td>
  	 <td id='{admin_marketing_campaign.marketing_free_campaign.CAMPAIGN_FREE_DIV_NAME}' style='display:none;'  >
          <form method='post' action='form_select.php' name='free_campaign' >
	   <table width='90%' align='left'>
	    <tr>
	     <th align='left' >{admin_marketing_campaign.L_CAMPAIGN}:</th>
	     <td align='left' colspan='5' >
		<input type='hidden' name='campaign_change' />
		<input type='hidden' name='free_campaign_change' />
		<input type='hidden' name='campaign_id' value='{admin_marketing_campaign.marketing_free_campaign.CAMPAIGN_ID}'  /> 
		<input type='text' name='campaign_free_name' style='border:0;' value='{admin_marketing_campaign.marketing_free_campaign.CAMPAIGN_FREE_NAME}' />

	     </td>
	 </tr>
	 <tr>
	     <td valign='top' align='right' colspan='6'>
		<span >{admin_marketing_campaign.marketing_free_campaign.L_FREQUENCY}</span><br />
		<select name='frequency' style='border:0px;' >
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED1} value='1'>Daily</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED2} value='2'>Second Day</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED3} value='3'>Third Day</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED4} value='4'>Fourth Day</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED5} value='5'>Fifth Day</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED6} value='6'>Sixth Day</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED7} value='7'>Weekly</option>
		 <option {admin_marketing_campaign.marketing_free_campaign.FREQUENCY_SELECTED8} value='8'>Monthly</option>
		</select>
	     </td>
	 </tr>

	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	     <td>{admin_marketing_campaign.marketing_free_campaign.L_SCRIPT}</td>
	     <td colspan='5'><input type='text' name='script' style='border:0px;' size='50' value='{admin_marketing_campaign.marketing_free_campaign.SCRIPT}' /></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td width='25%'>{admin_marketing_campaign.L_HIT_RETURN}</td>
	  <td align='left' width='25%'>{admin_marketing_campaign.marketing_free_campaign.HIT_RETURN_RESULT}</td>
	  <td width='25%' align='left'></td>
	  <td width='25%' ></td><td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_HIT_RESUME}</td>
	  <td>{admin_marketing_campaign.marketing_free_campaign.HIT_RESUME_RESULT}</td>
	  <td></td>
	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_MATCH}</td>
 	  <td>{admin_marketing_campaign.marketing_free_campaign.SUCCESS_MATCH_RESULT}</td>
	  <td></td>
 	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_FAILED}</td>
 	  <td>{admin_marketing_campaign.marketing_free_campaign.SUCCESS_FAILED_RESULT}</td>
	  <td></td>
	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td valign='top' >{admin_marketing_campaign.L_NOTES}</td>
	  <td colspan='5' >
	   <textarea cols='45' rows='3' name='notes' style='border:0px;' >{admin_marketing_campaign.marketing_free_campaign.NOTES}</textarea>
	  </td>
	 </tr>
	 <tr>
	  <td valign='top' >{admin_marketing_campaign.marketing_free_campaign.L_AD_TITLE}</td>
	  <td colspan='5' >
	   <input name='ad_title' style='border:0px;' size='30' value='{admin_marketing_campaign.marketing_free_campaign.AD_TITLE}' />
	  </td>
	 </tr>
	 <tr>
	  <td valign='top' >{admin_marketing_campaign.marketing_free_campaign.L_AD_COPY}</td>
	  <td colspan='5' >
	   <textarea cols='60' rows='20' name='ad_copy' style='border:0px;' >{admin_marketing_campaign.marketing_free_campaign.AD_COPY}</textarea>
	  </td>
	 </tr>
  	 <tr><td colspan='6'>&nbsp;</td></tr>
	 <tr>
	   <td colspan='2'><input type='submit' name='free_campaign_edit' value='Submit' /></td>
	   <td colspan='2'><input type='submit' name='free_campaign_delete' value='Delete' /></td>
	   <td colspan='2'><input type='button' name='change' value='Edit' onclick="return mask_form('free_campaign',this.form);" /></td>
	   <td colspan='2'>&nbsp;</td>
	 </tr>
	</table>
       </form>

      </td>
     </tr>
     </table>   
<!-- END marketing_free_campaign -->

      </div>
     </td>
   </tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td><a href='#' onclick="return dis_box('paid_campaign');" >PAID CAMPAIGN</a></td></tr>
   <tr>
    <td>
    <div id="paid_campaign" style='display:none' >

<!-- BEGIN marketing_paid_campaign -->


       <table width='100%'>

	<tr>
         <td>	
          <a href='#' onclick="dis_box('{admin_marketing_campaign.marketing_paid_campaign.CAMPAIGN_PAID_DIV_NAME}');return false;" >
	    {admin_marketing_campaign.marketing_paid_campaign.CAMPAIGN_PAID_NAME}
          </a>
	 </td>
	 <td>&nbsp;</td>
 	</tr>

        <tr  >
	 <td>&nbsp;</td>
  	 <td id='{admin_marketing_campaign.marketing_paid_campaign.CAMPAIGN_PAID_DIV_NAME}' style='display:none;'  >

         <form method='post' action='form_select.php' name='free_campaign' > 
	 <table width='90%' align='left'>
	 <tr>
	     <th align='left' >{admin_marketing_campaign.L_CAMPAIGN}:</th>
	     <td align='left' colspan='5' >
		<input type='hidden' name='campaign_change' />
		<input type='hidden' name='paid_campaign_change' /> 
		<input type='hidden' name='campaign_id' value='{admin_marketing_campaign.marketing_paid_campaign.CAMPAIGN_ID}'  /> 
		<input type='text' name='campaign_paid_name' style='border:0;' value='{admin_marketing_campaign.marketing_paid_campaign.L_CAMPAIGN_PAID_NAME}' />
	     </td>
	 </tr>
	 <tr>
	     <td valign='top' align='right' colspan='6'>
		<span >{admin_marketing_campaign.L_COST_OF_AD}</span><br />
		<select name='ad_currency' >
		  <option {admin_marketing_campaign.marketing_paid_campaign.COST_SELECTED1} value='1'>$</option>
		  <option {admin_marketing_campaign.marketing_paid_campaign.COST_SELECTED2} value='2'>€</option>
		  <option {admin_marketing_campaign.marketing_paid_campaign.COST_SELECTED3} value='3'>₩</option>
		</select>&nbsp;&nbsp;		
		<input type='text' name='cost_of_ad' style='border:0px;' size='6' value='{admin_marketing_campaign.marketing_paid_campaign.COST_OF_AD}' />
	     </td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	   <td>{admin_marketing_campaign.marketing_paid_campaign.L_AD_PAGE}</td>
           <td colspan='5'>
   	   <input type='text' name='ad_page' style='border:0px;' size='30' value='{admin_marketing_campaign.marketing_paid_campaign.AD_PAGE_RESULT}' />
	   </td>
	 </tr>
	 <tr>
	     <td>{admin_marketing_campaign.L_SCRIPT}</td>
	     <td colspan='5'><input type='text' style='border:0px;' name='script' size='30' 
		value='{admin_marketing_campaign.marketing_paid_campaign.SCRIPT}' /></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td width='25%'>{admin_marketing_campaign.L_HIT_RETURN}</td>
	  <td align='left' width='25%'>{admin_marketing_campaign.marketing_paid_campaign.HIT_RETURN_RESULT}</td>
	  <td width='25%' align='left'>{admin_marketing_campaign.L_COST_PER_HIT}</td>
	  <td width='25%' >{admin_marketing_campaign.marketing_paid_campaign.COST_PER_HIT}</td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_HIT_RESUME}</td>
	  <td>{admin_marketing_campaign.marketing_paid_campaign.HIT_RESUME_RESULT}</td>
	  <td>{admin_marketing_campaign.L_COST_PER_RESUME}</td>
	  <td>{admin_marketing_campaign.marketing_paid_campaign.COST_PER_RESUME}</td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_MATCH}</td>
 	  <td>{admin_marketing_campaign.marketing_paid_campaign.SUCCESS_MATCH_RESULT}</td>
	  <td>{admin_marketing_campaign.L_COST_PER_SUCCESS_MATCH}</td>
 	  <td>{admin_marketing_campaign.marketing_paid_campaign.COST_PER_SUCCESS_MATCH}</td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_FAILED}</td>
 	  <td>{admin_marketing_campaign.marketing_paid_campaign.SUCCESS_FAILED}</td>
	  <td>{admin_marketing_campaign.L_PLACED}</td>
	  <td>{admin_marketing_campaign.marketing_paid_campaign.COST_PER_PLACED}</td>
	  <td colspan='2'></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td valign='top' >{admin_marketing_campaign.L_NOTES}</td>
	  <td colspan='5' >
	   <textarea cols='45' name='notes' style='border:0px;' rows='3'>{admin_marketing_campaign.marketing_paid_campaign.NOTES}</textarea>
	  </td>
	 </tr>
  	 <tr><td colspan='6'>&nbsp;</td></tr>
	 <tr>
	   <td><input type='submit' name='paid_campaign_edit' value='Submit'  /></td>
	   <td><input type='submit' name='paid_campaign_delete' value='Delete' /></td>
	   <td><input type='button' name='change' value='Edit' onclick="return mask_form('paid_campaign',this.form);"  /></td>
	   <td colspan='4'>&nbsp;</td>
	 </tr>
	</table>
      </form>

      </td>
     </tr>
     </table>   
<!-- END marketing_paid_campaign -->

       </div>	
     </td>
   </tr>	
   <tr><td>&nbsp;</td></tr>
   <tr><td><a href='#' onclick="return dis_box('new_free_campaign');" >ADD FREE CAMPAIGN</a></td></tr>
   <tr>
     <td>
      <div id="new_free_campaign" style="display:none;" >
	<hr />
       <form method='post' action='form_select.php' name='free_campaign' >
	<table width='90%' align='left'>
	 <tr>
	     <th align='left' >{admin_marketing_campaign.L_CAMPAIGN}:</th>
	     <td align='left' colspan='5' >
		<input type='hidden' name='campaign_change' />
		<input type='hidden' name='free_campaign_add' /> 
		<input type='text' style='' name='campaign_free_name' value='{admin_marketing_campaign.L_CAMPAIGN_FREE_NAME}' />

	     </td>
	 </tr>
	 <tr>
	     <td valign='top' align='right' colspan='6'>
		<span>{admin_marketing_campaign.L_FREQUENCY}</span><br />
		<select name='frequency' >
		 <option value='1'>Daily</option>
		 <option value='2'>Second Day</option>
		 <option value='3'>Third Day</option>
		 <option value='4'>Fourth Day</option>
		 <option value='5'>Fifth Day</option>
		 <option value='6'>Sixth Day</option>
		 <option value='7'>Weekly</option>
		 <option value='8'>Monthly</option>
		</select>
	     </td>
	 </tr>

	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td valign='top' >{admin_marketing_campaign.L_NOTES}</td>
	  <td colspan='5' ><textarea cols='45' rows='3' name='notes' ></textarea></td>
	 </tr>
	 <tr>
	     <td>{admin_marketing_campaign.L_SCRIPT}</td>
	     <td colspan='5'><input type='text' name='script' size='30' /></td>
	 </tr>
	 <tr>
	     <td>{admin_marketing_campaign.L_AD_TITLE}</td>
	     <td colspan='5'><input type='text' name='ad_title' size='30' /></td>
	 </tr>
	 <tr>
	     <td>{admin_marketing_campaign.L_AD_COPY}</td>
	     <td colspan='5'><textarea cols='60' rows='20' name='ad_copy' ></textarea></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td width='25%'>{admin_marketing_campaign.L_HIT_RETURN}</td>
	  <td align='left' width='25%'></td>
	  <td width='25%' align='left'></td>
	  <td width='25%' ></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_HIT_RESUME}</td>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_MATCH}</td>
 	  <td></td>
	  <td></td>
 	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_FAILED}</td>
 	  <td></td>
	  <td></td>
	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	   <td><input type='submit' name='submit' value='Submit' /></td>
	   <td colspan='4'>&nbsp;</td>
	 </tr>
	</table>
       </form> 
      </div>
     </td>
   </tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td><a href='#' onclick="return dis_box('new_paid_campaign');" >ADD PAID CAMPAIGN</a></td></tr>
   <tr>
    <td>
    <div id="new_paid_campaign" style='display:none' >
	<hr />
       <form method='post' action='form_select.php' name='new_free_campaign' >
	<table width='90%' align='left'>
	 <tr>
	     <th align='left' >{admin_marketing_campaign.L_CAMPAIGN}:</th>
	     <td align='left' colspan='5' >
		<input type='hidden' name='campaign_change' />
		<input type='hidden' name='paid_campaign_add' /> 
		<input type='text' name='campaign_paid_name' value='{admin_marketing_campaign.CAMPAIGN_PAID_NAME}' />
	     </td>
	 </tr>
	 <tr>
	     <td valign='top' align='right' colspan='6'>
		<span>{admin_marketing_campaign.L_COST_OF_AD}</span><br />
		<select name='ad_currency' >
		  <option value='1'>$</option>
		  <option value='2'>€</option>
		  <option value='3'>₩</option>
		</select>&nbsp;&nbsp;		
		<input type='text' name='cost_of_ad' size='6' />
	     </td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	     <td>{admin_marketing_campaign.L_AD_PAGE}</td>
	     <td colspan='5'><input type='text' name='ad_page' size='30' /></td>
	 </tr>
	 <tr>
	     <td>{admin_marketing_campaign.L_SCRIPT}</td>
	     <td colspan='5'><input type='text' name='script' size='30' /></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td width='25%'>{admin_marketing_campaign.L_HIT_RETURN}</td>
	  <td align='left' width='25%'></td>
	  <td width='25%' align='left'></td>
	  <td width='25%' ></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_HIT_RESUME}</td>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_MATCH}</td>
 	  <td></td>
	  <td></td>
 	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr>
	  <td>{admin_marketing_campaign.L_SUCCESS_FAILED}</td>
 	  <td></td>
	  <td></td>
	  <td></td>
	  <td colspan='2'></td>
	 </tr>
	 <tr><td colspan='6' >&nbsp;</td></tr>
	 <tr>
	  <td valign='top' >{admin_marketing_campaign.L_NOTES}</td>
	  <td colspan='5' ><textarea cols='45' name='notes' rows='3' ></textarea></td>
	 </tr>
  	 <tr><td colspan='6'>&nbsp;</td></tr>
	 <tr>
	   <td><input type='submit' name='submit' value='Submit'  /></td>
	   <td colspan='4'>&nbsp;</td>
	 </tr>
	</table>
      </form>
       </div>	
    </td>
   </tr>

  </table>

<!-- END admin_marketing_campaign -->

<!-- BEGIN admin_auto_email -->
	<form action='form.php' enctype='multipart/form-data' method='post'>
	<table>
	 <tr><th colspan='2'>{admin_auto_email.L_AUTOMATIC_EMAIL_TEXTS}</th></tr>
	 <tr><td colspan='2'>{admin_auto_email.L_AUTOMATIC_EMAIL_TEXT}<br /><br /> 
	 </td></tr>
	</table>
	<table id='automatic_email_text'  >
	 <tr>
	     <td width='20%'>{admin_auto_email.L_TEACHER_SCHOOL_POSRESPONSE_EMAIL}</td>
	     <td><textarea name='teacher_school_posresponse_email' rows='8' cols='60' >{admin_auto_email.TEACHER_SCHOOL_POSRESPONSE_EMAIL}</textarea>
	     </td>
	  </tr>
	  <tr>
	     <td>{admin_auto_email.L_TEACHER_RECRUITER_POSRESPONSE_EMAIL} </td>
	     <td><textarea name='teacher_recruiter_posresponse_email' rows='8' cols='60'>{admin_auto_email.TEACHER_RECRUITER_POSRESPONSE_EMAIL}</textarea></td>
	  </tr>
	  <tr>
	     <td>{admin_auto_email.L_ETK_TEACHER_POSRESPONSE_EMAIL}</td>
   	     <td>
<textarea name='etk_teacher_posresponse_email' rows='8' cols='60'>{admin_auto_email.ETK_TEACHER_POSRESPONSE_EMAIL}</textarea>
	     </td>
 	  </tr>
	  <tr>
	     <td>{admin_auto_email.L_ETK_TEACHER_SCHOOL_EMAIL}</td>
   	     <td><textarea name='etk_teacher_school_email' rows='8' cols='60' >{admin_auto_email.ETK_TEACHER_SCHOOL_EMAIL}</textarea></td>
	  </tr>
	  <tr>
	     <td>{admin_auto_email.L_ETK_TEACHER_RECRUITER_EMAIL}</td>
   	     <td>
<textarea name='etk_teacher_recruiter_email' rows='8' cols='60' >{admin_auto_email.ETK_TEACHER_RECRUITER_EMAIL}</textarea>
	     </td>
	  </tr>
	  <tr>
	     <td>{admin_auto_email.L_CANDIDATE_FAIL_EMAIL}</td>
   	     <td>
		<textarea name='candidate_fail_email' rows='8' cols='60'>{admin_auto_email.CANDIDATE_FAIL_EMAIL}</textarea>
	     </td>
          </tr>
	  <tr>
	     <td>{admin_auto_email.L_EMPLOYER_REMINDER}</td>
   	     <td>
		<textarea name='employer_reminder' rows='8' cols='60'>{admin_auto_email.EMPLOYER_REMINDER}</textarea>
	     </td>
          </tr>
	  <tr>
	     <td colspan='2' align='center' >
	       <input type='reset' name='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;
  	       <input type='submit' name='submit' value='{SUBMIT}' />
	     </td>
	  </tr>
	</table>
       </form>
<!-- END admin_auto_email -->

<!-- BEGIN admin_auto_page -->
	<form action='form.php' enctype='multipart/form-data' method='post'>
	<table  >
	 <tr><th colspan='2'>{admin_auto_page.L_VARIOUS_FORMS_TEXT}</th></tr>
	 <tr><td colspan='2'>{admin_auto_page.L_VARIOUS_FORMS_TEXT_TEXT}<br /><br /> 
	 </td></tr>
	</table>
	 <table id='various_forms_text' >
	 <tr><td>{admin_auto_page.L_RESUME_UPLOAD_TEXT}</td>
   	     <td><textarea name='resume_upload_text' rows='8' cols='60'>{admin_auto_page.RESUME_UPLOAD_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_RESUME_ERROR_TEXT}</td>
   	     <td><textarea name='resume_error_text' rows='8' cols='60'>{admin_auto_page.RESUME_ERROR_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_RECRUITER_FORM_TEXT}</td>
   	     <td><textarea name='recruiter_form_text' rows='8' cols='60'>{admin_auto_page.RECRUITER_FORM_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_RECRUITER_THANKS_TEXT}</td>
   	     <td><textarea name='recruiter_thanks_text' rows='8' cols='60' >{admin_auto_page.RECRUITER_THANKS_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_RECRUITER_ERROR_TEXT}</td>
   	     <td><textarea name='recruiter_error_text' rows='8' cols='60' >{admin_auto_page.RECRUITER_ERROR_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_SCHOOL_THANKS_TEXT}</td>
   	     <td><textarea name='school_thanks_text' rows='8' cols='60'>{admin_auto_page.SCHOOL_THANKS_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_SCHOOL_ERROR_TEXT}</td>
   	     <td><textarea name='school_error_text' rows='8' cols='60'>{admin_auto_page.SCHOOL_ERROR_TEXT}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_CONTACT_US_HEADING}</td>
   	     <td><textarea name='contact_us_heading' rows='1' cols='60'>{admin_auto_page.CONTACT_US_HEADING}</textarea></td>
	 </tr>
	 <tr><td>{admin_auto_page.L_CONTACT_US_TEXT}</td>
   	     <td><textarea name='contact_us_text' rows='8' cols='60'>{admin_auto_page.CONTACT_US_TEXT}</textarea></td>
	 </tr>
	  <tr>
		<td colspan='2' align='center' >
				<input type='reset' name='reset' value='{RESET}' />&nbsp;&nbsp;&nbsp;
			       <input type='submit' name='submit' value='{SUBMIT}' />
		</td>
	  </tr>
	 </table>
	</form>

<!-- END admin_auto_page -->

<!-- BEGIN admin_form -->

        <table>
	 <tr><th colspan='2'>{admin_form.L_AFTERCARE_TITLE}</th></tr>
	 <tr><td colspan='2'>{admin_form.L_AFTERCARE_TEXT}<br /><br /> 
		<a href="form.php?mode=admin_form_aftercare">DISPLAY</a>
	 </td></tr>
	</table>

        <table>
	 <tr><th colspan='2'>{admin_form.L_AFTERCARE_TIME_TITLE}</th></tr>
	 <tr><td colspan='2'>{admin_form.L_AFTERCARE_TIME_TEXT}<br /><br /> 
		<a href="form.php?mode=admin_time_aftercare">DISPLAY</a>
	 </td></tr>
	</table>

        <table>
	 <tr><th colspan='2'>{admin_form.L_AUTOMATIC_EMAIL_RESPONSE}</th></tr>
	 <tr><td colspan='2'>{admin_form.L_AUTOMATIC_EMAIL_RESPONSE_TEXT}<br /><br /> 
		<a href="form.php?mode=admin_email_temp">DISPLAY</a>
	 </td></tr>
	</table>

        <table>
	 <tr><th colspan='2'>{admin_form.L_AUTOMATIC_EMAIL_TEXTS}</th></tr>
	 <tr><td colspan='2'>{admin_form.L_AUTOMATIC_EMAIL_TEXT}<br /><br /> 
		<a href="form.php?mode=admin_auto_email">DISPLAY</a>
	 </td></tr>
	</table>

        <table>
	 <tr><th colspan='2'>{admin_form.L_VARIOUS_FORMS_TEXT}</th></tr>
	 <tr><td colspan='2'>{admin_form.L_VARIOUS_FORMS_TEXT_TEXT}<br /><br /> 
		<a href="form.php?mode=admin_auto_page">DISPLAY</a>
	 </td></tr>
	</table>

        <table>
	 <tr><th colspan='2'>{admin_form.L_WEBSITE_FORM_TITLE}</th></tr>
	 <tr><td colspan='2'>{admin_form.L_WEBSITE_FORM_TEXT}<br /><br /> 
		<a href="form.php?mode=admin_form_website">DISPLAY</a>
	 </td></tr>
	</table>

<!-- END admin_form -->

<!-- BEGIN admin_time_aftercare -->
	
        <table>
	 <tr><th colspan='2'>{admin_time_aftercare.L_AFTERCARE_TIME_TITLE}</th></tr>
	 <tr><td colspan='2'>{admin_time_aftercare.L_AFTERCARE_TIME_TEXT}<br /><br /> 
	 </td></tr>
	</table>

	<form action='form.php' enctype='multipart/form-data' method='post'>
	 <table>
	  <tr><th>{admin_time_aftercare.L_BIRTHDAY_MESSAGE}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td>
	    <textarea name='birthday_subject' rows='1' cols='50' >{admin_time_aftercare.BIRTHDAY_SUBJECT}</textarea> 
	   </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td>
	    <textarea name='birthday_message' rows='4' cols='50' >{admin_time_aftercare.BIRTHDAY_MESSAGE}</textarea>
	   </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>
 
	<form action='form.php' enctype='multipart/form-data' method='post'>
	 <table>
	  <tr><th>{admin_time_aftercare.L_FOUR_DAY}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title1' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE1}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text1' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT1}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>


	 <table>
	  <tr><th>{admin_time_aftercare.L_SAME_DAY}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title2' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE2}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text2' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT2}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_ONE_WEEK}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title3' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE3}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text3' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT3}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_ONE_MONTH}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title4' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE4}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text4' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT4}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_THREE_MONTH}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title5' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE5}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text5' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT5}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_SIX_MONTH}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title6' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE6}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text6' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT6}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_NINE_MONTH}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
  	    </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title7' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE7}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text7' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT7}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_ELEVEN_MONTH}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>
	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title8' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE8}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text8' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT8}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>

	 <table>
	  <tr><th>{admin_time_aftercare.L_TWELVE_MONTH}</th></td>
          <tr> 
	    <td></td>
	    <td><label>{admin_time_aftercare.L_MESSAGE_TITLE}</label></td> 
	  </tr>

	  <tr>
	    <td valign='top' rowspan='4' >
	   </td>
	  </tr>
	  <tr>
	   <td><textarea name='time_title9' rows='1' cols='50' >{admin_time_aftercare.TIME_TITLE9}</textarea> </td>
	  </tr>
 	  <tr>
	   <td><label>{admin_time_aftercare.L_MESSAGE_TEXT}</label></td>
	  </tr>
	  <tr>
	   <td><textarea name='time_text9' rows='4' cols='50' >{admin_time_aftercare.TIME_TEXT9}</textarea></td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>
	</form>
<!-- END admin_time_aftercare -->

<!-- BEGIN admin_fixed_aftercare -->

        <table>
	 <tr><th colspan='2'>{admin_fixed_aftercare.L_AFTERCARE_TITLE}</th></tr>
	 <tr><td colspan='2'>{admin_fixed_aftercare.L_AFTERCARE_TEXT}<br /><br /> 
	 </td></tr>
	</table>

	<div id='aftercare_fixed_form'  >

	<form action='form.php' enctype='multipart/form-data' method='post'>
	 <table>
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 1</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
		  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date1').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE1}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date1').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>	
		<input type='hidden' id="fixed_date1" name='fixed_date1' value='' />
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title1' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE1}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text1' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT1}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 2</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript">
				  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date2').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE2}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date2').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date2" name='fixed_date2' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title2' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE2}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text2' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT2}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 3</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date3').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE3}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date3').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date3" name='fixed_date3' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title3' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE3}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text3' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT3}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 4</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
		  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date4').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE4}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date4').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }
		</script>
		<input type='hidden' id="fixed_date4" name='fixed_date4' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title4' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE4}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text4' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT4}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 5</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date5').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE5}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date5').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date5" name='fixed_date5' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title5' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE5}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text5' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT5}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 6</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript">
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date6').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE6}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date6').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date6" name='fixed_date6' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title6' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE6}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text6' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT6}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 7</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date7').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE7}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date7').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date7" name='fixed_date7' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title7' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE7}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text7' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT7}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 8</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript">
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date8').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE8}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date8').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date8" name='fixed_date8' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title8' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE8}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text8' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT8}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 9</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date9').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE9}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date9').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }
		</script>
                <input type='hidden' id="fixed_date9" name='fixed_date9' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title9' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE9}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text9' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT9}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 10</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date10').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE10}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date10').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>	
		<input type='hidden' id="fixed_date10" name='fixed_date10' value='' />
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title10' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE10}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text10' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT10}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	
         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 11</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
		
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date11').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE11}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date11').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date11" name='fixed_date11' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title11' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE11}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text11' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT11}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 12</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
     
      document.getElementById('fixed_date12').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE12}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date12').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>	
		<input type='hidden' id="fixed_date12" name='fixed_date12' value='' />
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title12' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE12}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text12' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT12}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 13</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date13').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE13}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date13').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }
		</script>	
		<input type='hidden' id="fixed_date13" name='fixed_date13' value='' />
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title13' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE13}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text13' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT13}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 14</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >
		  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth() ;     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date14').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE14}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date14').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>
		<input type='hidden' id="fixed_date14" name='fixed_date14' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title14' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE14}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text14' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT14}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>

         <table cellspacing='25' >
	  <div id="website_work">
	  <tr><th>{admin_fixed_aftercare.MESSAGE} 15</th></td>
          <tr>
   	   <td align='center' rowspan='2' >
             <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>
		<script type="text/javascript" >

  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth() ;     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect..

      document.getElementById('fixed_date15').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{admin_fixed_aftercare.FIXED_DATE15}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date15').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }
		</script>
		<input type='hidden' id="fixed_date15" name='fixed_date15' value='' />	
	      </td>
	      <td>
		<label>{admin_fixed_aftercare.MESSAGE_TITLE}
		  <textarea name='fixed_title15' rows='1' cols='50' >{admin_fixed_aftercare.FIXED_TITLE15}</textarea>
		</label>
	      </td>
	  </tr>
 	  <tr>
	    <td><label>{admin_fixed_aftercare.MESSAGE_TEXT}
		<textarea name='fixed_text15' rows='4' cols='50' >{admin_fixed_aftercare.FIXED_TEXT15}</textarea>
		</label>
	    </td>
	  </tr>
	  <tr>
            <td colspan='2' align='center' >
		<input type='reset' value='reset' />&nbsp;&nbsp;&nbsp;<input type='submit' value='submit' />
	    </td>
	  </tr>
	  </div>
	 </table>
	</form>
  
     </div>

<!-- END admin_fixed_aftercare -->

<!-- BEGIN knowledge_base -->



     <div style='bold' >{knowledge_base.KNOWLEDGE_BASE_TITLE}</div>
     <table class='bookmark-table' bgcolor='#FFFFCC' width='100%' >
	<tr><td colspan='2'>&nbsp;</td></tr>
<!-- BEGIN knowledge_base_section -->
	<tr>
	  <td colspan='2' class='knowledge-title' >
		<b>&nbsp;&nbsp;{knowledge_base.knowledge_base_section.SECTION}</b>
	  </td>
 	</tr>
<!-- BEGIN knowledge_base_records -->
	<tr>
	    <td class="knowledge-records" colspan='2' >
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a onclick="return dis_box('{knowledge_base.knowledge_base_section.knowledge_base_records.TEXT_ID}');" href="#">
		   * {knowledge_base.knowledge_base_section.knowledge_base_records.TITLE}
		</a>
	    </td>
	 </tr>
	  <tr>
	    <td class="knowledge-text" colspan='2' >
              <div id='{knowledge_base.knowledge_base_section.knowledge_base_records.TEXT_ID}'
		style='display:none;' >
	     	<table width='100%'>
		 <tr>
		  <td align='right'>
		<textarea name='text' id='{knowledge_base.knowledge_base_section.knowledge_base_records.REC_ID_TEXT}' rows='20' cols='85' class='textarea_node' >{knowledge_base.knowledge_base_section.knowledge_base_records.TEXT}</textarea>&nbsp;&nbsp;
		  </td>		 
		 </tr>
		 <tr>
                  <td colspan='2' align='right'>
                  <a href='#' onclick="unmask_element('{knowledge_base.knowledge_base_section.knowledge_base_records.REC_ID_TEXT}');
                  return false;" />Edit</a>
                &nbsp;&nbsp;
                  <a href='#' onclick="mask_element('{knowledge_base.knowledge_base_section.knowledge_base_records.REC_ID_TEXT}');
	          save_knowledge_base_entry('{knowledge_base.knowledge_base_section.knowledge_base_records.REC_ID_TEXT}');
        	  return false;" >Save</a>&nbsp;&nbsp;
                  </td>
		 </tr>
		</table>
	  	</div>
	    </td>
	  </tr>
<!-- END knowledge_base_records -->
	<tr><td  colspan='2'>&nbsp;</td></tr>
<!-- END knowledge_base_section -->
	<tr>
	 <td>
 	   <b>&nbsp;&nbsp;
	    <a href="#" onclick="return dis_box('new_section');" >New Section</a>
	   </b>
         </td>
	 <td></td>
	</tr>
	<tr>
	 <td>&nbsp;</td>
	 <td>
          <div id='new_section' style='display:none;' >
	    <table width='100%' >
	      <tr>
	       <td>Section</td>
 	       <td>
	  	<select name='section_choice' id='section_choice' >
<!-- BEGIN knowledge_add_section -->
	   	 <option value='{knowledge_base.knowledge_add_section.ADD_SECTION}'>
			{knowledge_base.knowledge_add_section.ADD_SECTION}
		</option>
<!-- END knowledge_add_section -->
	  	</select>
	       </td>
	      </tr>
	      <tr>
		<td>Title</td>
		<td><input type='text' name='section_title' id='section_title' /></td>
	      </tr>
	      <tr>
	       <td>&nbsp;</td>
	       <td >
            <textarea name='new_knowledge' id='new_knowledge' rows='5' cols='40' ></textarea>
	       </td>
              </tr>
	      <tr>
		<td></td>
		<td align='right'>
                 <a href="#" onclick="add_knowledge_base_entry();return dis_box('new_section')" >Save</a>
		</td>
	      </tr>
	    </table>
	  </div>
	 </td>
	</tr>	
	</table>
       </div>
 
<!-- END knowledge_base -->


<!-- BEGIN recruitment_agreement -->

     <div style='' >{recruitment_agreement.L_RECRUITMENT_AGREEMENT}</div>
     <div style=''>This agreement is dated the {recruitment_agreement.RECRUITMENT_DATE} and is between {recruitment_agreement.COMPANY_NAME} - hereby known as the employer - and {recruitment_agreement.RECRUITER_NAME} - hereby known as the employee.</div>
	<br />
     <table class='bookmark-table' bgcolor='#FFFFCC' width='100%' >
	<tr>
	 <td colspan='2'>
	  <ul>
	   <li>Non-Disclosure: The employee agrees to maintain the uptmost confidentiality with regards to all aspects of their employment with the employer. 
	       This includes details of clients, applicants as well as (but not limited to), information about the business process and software use by them in their day-to-day work.</li>
	   <li>Information Attainment: The employee agrees that all information in the database and collected by the employee on behalf of the employer - belongs exclusively to the employer and that the
	       information, details, contacts cannot be exchanged, sold, copied or in any other way used without the express permission of the employer. </li>
	   <li>Working Hours and Conditions: The employee agrees to be available during normal work hours (2-9), but contact employers and take calls on the phone. </li>
	   <li>Cessation of Employment: Should the employee wish to finish employment with the employer, the employee will be entitled to any compensation due on a recruitment placing - only if that
	       applicant has already applied for and recieved their visa for employment with that client. In that case, since most of the work will have already been completed by the employee - they should
	       be due compensation. This is to be be in effect, only if the employee has give two weeks notice to the employer stating that they will be leaving the company.</li>
	   <li>Termination of Employment: The employer has the right to terminate employment immediately, should the employee fail to carry out their duties in a proper manner.
	       Appropriate behaviour in this case will be determined by the employer, and will include (but not be limited to), being cordial to the clients and applicants, being honest, being available
	       to field calls etc...</li>
	  <li>Eligibility of Employment: The employee agrees not to work for any other recruitment agency or pursue any other recruitment based activity while working with the employer.</li>
	  <li>Work Monitoring: The employee agrees that their work will be monitored, including possibly - emails, interface work, time spent at the interface, phone calls, level of service.</li>
	  <li>Area of Juristication: Should this agreement be in legal dispute. All court proceedings and motions will be filed and settled in the Irish Republic.</li>
	  </ul>
	 </td>
	</tr>
	<tr>
	   <td colspan='2' align='center' >
					    {recruitment_agreement.L_AGREE}<input type='radio' name='agree' onclick="location.href='http://www.{SITENAME}/control_panel.php'" />&nbsp;&nbsp;
					    {recruitment_agreement.L_NOT_AGREE}<input type='radio' name='agree' />
	   </td>
	</tr>
     </table>
     
<!-- END recruitment_agreement -->

<!-- BEGIN bookmarks -->

     <table class='bookmark-table' >
	<tr><th colspan='2'>Website Bookmarks:</th></tr>
	<tr><td colspan='2'></td></tr>
<!-- BEGIN bookmark_records -->
	<tr>
	    <td class="bookmark-title" >
		<a href='{bookmarks.bookmark_records.URL}'>{bookmarks.bookmark_records.TITLE}</a>
	    </td>
	</tr>
	<tr><td class="bookmark-url">{bookmarks.bookmark_records.URL}</td></tr>
<!-- END bookmark_records -->
	<tr><td colspan='2'>&nbsp;</td></tr>
	<tr><th colspan='2'>Website Uploads:</th></tr>
	<tr>
	  <td>
	  <form action='form.php?mode=bookmark' enctype='multipart/form-data' method='post' >
	  <table >
	   <tr><td width='20%'>Title:</td><td><input type='text' name='bookmark_title' /></td></tr>
	   <tr><td>Web Page:</td><td><input type='text' name='bookmark_url' /></td></tr>
	   <tr><td colspan='2' align='center' >
		<input type='reset' name='reset' value='Reset' />&nbsp;&nbsp;
		<input type='submit' name='submit' value='Submit' />
		</td>
	   </tr>
	  </table>
          </form>
	  </td>
	</tr>
     </table>

<!-- END bookmarks -->

<!-- BEGIN sms_interface -->

<script type='text/javascript' src='js/dialpad.js'></script>

  <form name="board" action='#' method='post' onsubmit='callit();return false;' >
   <table align='center' >
     <tr><th>{sms_interface.L_SMS_INTERFACE}</th></tr>
     <tr><td id='sms_response' >&nbsp;</td></tr>
     <tr><td><textarea rows='10' cols='45' name='sms_text' id='sms_text' ></textarea></td></tr>
     <tr><td>&nbsp;</td></tr>
     <tr>
      <td>

	<center>
	<table cellspacing="0" cellpadding="0" width="135" bgcolor="" >

	<tr align="center">
	<td><input type="button" value="    1    " onclick="but1(); window.focus()" /></td>
	<td><input type="button" value="    2    " onclick="but2(); window.focus()" /></td>
	<td><input type="button" value="    3    " onclick="but3(); window.focus()" /></td>
	</tr>

	<tr align="center">
	<td><input type="button" value="    4    " onclick="but4(); window.focus()" /></td>
	<td><input type="button" value="    5    " onclick="but5(); window.focus()" /></td>
	<td><input type="button" value="    6    " onclick="but6(); window.focus()" /></td>
	</tr>

	<tr align="center">
	<td><input type="button" value="    7    " onclick="but7(); window.focus()" /></td>
	<td><input type="button" value="    8    " onclick="but8(); window.focus()" /></td>
	<td><input type="button" value="    9    " onclick="but9(); window.focus()" /></td>
	</tr>

	<tr align="center">
	<td colspan="3">
	<center>
   	    <table border="0" cellspacing="0" cellpadding="0" width="100%" >
	    <tr>
	    <td align="center" width="50%"><input type="button" value="    0    " onclick="but0(); window.focus()" /></td>
	    <td align="center" width="50%"><input type="button" value="Reset" onclick="clearit(); window.focus()" /></td>
            </tr>
            </table>
	</center>
 	</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
	  <td colspan="3" align='center' >
		<select name='country_code' id='country_code' >
			{sms_interface.TELEPHONE_CODE_BLOCK}
	 	  </select>	
	  </td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
	  <td align="center" colspan="3" >
		<input type="text" name="pn" id='tel_no' size="14" value='{sms_interface.SMS_NUMBER}' />
	  </td>	
	</tr>
	</table>
	</center>

    </td>
    </tr>
   </table>
  </form>

<!-- END sms_interface -->

<!-- BEGIN history -->

       <div style='clear:both;'>
        <table class='info_table_holder' >
          <tr>
           <td >
             <a href='#' onclick=\"return dis_box('teachers_box');\" >TEACHERS</a><table id='teachers_update_marker' class='info_table table_color' >
          <tr style='background-color:yellow;' ><th></th><th>Name:</th><th>City:</th><th>Age:</th><th>Call Time:</th><th>Status:</th><th>Submitted:</th><th>Email:</th><th>Phone:</th></tr>   

<!-- BEGIN teachers_history -->

     <tr class='--$teacher_id--' style='background-color:{history.teachers_history.AFTERCARE_COLOR};' >
      <td><a href='/gallery.php?id={history.teachers_history.TEACHER_ID}&amp;sid=$sid'>
            <img  src='images/etk-icons/stock_search.png'  style='border:0px;' alt='Look at Resume' /></a></td>
      <td><a href='/gallery.php?id={history.teachers_history.TEACHER_ID}&amp;sid=$sid'>{history.teachers_history.NAME}</a></td>
      <td>{history.teachers_history.LOCATION}</td>
      <td>{history.teachers_history.BIRTHYEAR}</td>
      <td>{history.teachers_history.FROM_TIME}-{history.teachers_history.UNTIL_TIME}</td>
      <td>{history.teachers_history.ARRIVAL}</td>
      <td>{history.teachers_history.SUBMITTED}</td>
      <td><a href='/form.php?candidate_email_form={history.teachers_history.EMAIL}&amp;name={history.teachers_history.TEACHER_FIRST_NAME}'>Email</a></td>
      <td>{history.teachers_history.PHONE_MODE}<img style='border:0px;height:27px;width:27px;' src='images/etk-icons/phone-small.gif' alt='Ring Candidate' /></a></td>
     </tr>

<!-- END teachers_history -->


	 </table>
 	</div>

<!-- END history -->


</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>
 
 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-3";
  urchinTracker();
 </script>

</body>

</html>
