<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<script LANGUAGE="javascript" src="js_recruiter.js"></script>
</head>


<body>
<div class='container'>
<div class='titleblock'>
<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>Recruiters helping recruiters </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<!-- BEGIN RECRUITER_FORM -->
  <form method="POST" ENCTYPE="multipart/form-data" action="recruiter_form.php" name="recruiter_form" onSubmit="return validate();">
	<table border="0" width="495" align="center" cellspacing="8" cellpadding="2">
	
	<tr>
    <td width='50%'>*Contact Name:</td>
    <td><input type='text' maxlength='30' name='contact' /></td>
   </tr>
   <tr>
    <td width='50%'>*Contact Email:</td>
    <td><input type='text' maxlength='30' name='email' /></td>
   </tr>
	<tr>
    <td width='50%'>*Name of School:</td>
    <td><input type='text' maxlength='30' name='school' /></td>
   </tr>
	<tr>
    <td>*Teacher Start Date:</td>
    <td>
      <select name='arrival' id='arrival'>
		 <option value='-1'>[Select]</option>
		 <option value='1'>Start of January</option>
		 <option value='2'>Middle of January</option>
		 <option value='3'>End of January</option>
		 <option value='4'>Start of February</option>
		 <option value='5'>Middle of February</option>
		 <option value='6'>End of February</option>
		 <option value='7'>Start of March</option>
		 <option value='8'>Middle of March</option>
		 <option value='9'>End of March</option>
		 <option value='10'>Start of April</option>
		 <option value='11'>Middle of April</option>
		 <option value='12'>End of April</option>
		 <option value='13'>Start of May</option>
		 <option value='14'>Middle of May</option>
		 <option value='15'>End of May</option>
		 <option value='16'>Start of June</option>
		 <option value='17'>Middle of June</option>
		 <option value='18'>End of June</option>
		 <option value='19'>Start of July</option>
		 <option value='20'>Middle of July</option>
		 <option value='21'>End of July</option>
		 <option value='22'>Start of August</option>
		 <option value='23'>Middle of August</option>
		 <option value='24'>End of August</option>
		 <option value='25'>Start of September</option>
		 <option value='26'>Middle of September</option>
		 <option value='27'>End of September</option>
		 <option value='28'>Start of October</option>
		 <option value='29'>Middle of October</option>
		 <option value='30'>End of October</option>
		 <option value='31'>Start of November</option>
		 <option value='32'>Middle of November</option>
		 <option value='33'>End of November</option>
		 <option value='34'>Start of December</option>
		 <option value='35'>Middle of December</option>
		 <option value='36'>End of December</option>
		 </select>
    </td>
  </tr>
  <tr>
    <td>*School Description:</td>
    <td><textarea name='school_description' cols='50' rows='7' /></textarea></td>
  </tr>
  <tr>
   <td >Picture of school/students:&nbsp;
     <input type='hidden' name='MAX_FILE_SIZE' value='100000' /></td>
	<td><input type='file' name='picupload' ></td>
  </tr>
  <tr>
    <td>City</td>
    <td>
    <select name='city' id='city'>
	   <option value='-1'>[Select]</option>
	   <option value='1'>Any</option>
	 	<option value='2'>Seoul</option>
	 	<option value='3'>Daegu</option>
	 	<option value='4'>Busan</option>
	 	<option value='5'>Incheon</option>
	 	<option value='6'>Gwangju</option>
	 	<option value='7'>Ulsan</option>
	 	<option value='8'>Ilsan</option>
      <option value='9'>Other</option>
	  </select>
	 </td>
  </tr>	
  <tr>
    <td>Website Address:</td>
    <td><input type='text' name='website_address' cols='50' rows='7' ></td>
  </tr>
  <tr>
  	 <td>Type of students:</td>	
  	 <td>
  	  <table cellspacing='0' cellpadding='0'>
  	   <tr><td width='50%'>Kindergarten:</td><td><input type="checkbox" name="kindergarten" value='1' ></td></tr>
  	   <tr><td width='50%'>Elementary:</td><td><input type="checkbox" name="elementary" value='1' ></td></tr>
  	   <tr><td width='50%'>Middle School:</td><td><input type="checkbox" name="middle_school" value='1'></td></tr>
  	   <tr><td width='50%'>High School:</td><td><input type="checkbox" name="high_school" value='1' ></td></tr>
  	   <tr><td width='50%'>Adults:</td><td><input type="checkbox" name="adults" value='1' ></td></tr>
  	  </table>
  	 </td>
   </tr>
  	<tr>
  	 <td>Will they provide airfare for teachers before they arrive in Korea ?</td>
  	 <td>
  	 <input type="radio" name="ticket_money" value="yes" > Yes&nbsp;
	 <input type="radio" name="ticket_money" value="no" > No
  	 </td>
  	</tr>
  	<tr>
    <td>Foreign Teacher Contact Details:</td>
    <td><textarea name='foreign_teacher' cols='50' rows='2' /></textarea></td>
  </tr>
  <tr>
  	 <td>Accomodation Type:</td>
  	 <td><select name='accomodation'>
	   <option value='-1'>[Select]</option>
	   <option value='1'>Single</option>
      <option value='2'>Shared</option>	 
     </select>
    </td>
  </tr>
  	<tr>
  	  <td>Please provide copy of contract if possible.</td>
  	  <td><input type="file" size="30" name="contract"></td>
  	</tr>
   <tr>
    <td>Teacher Description:</td>
    <td><textarea name='teacher_description' cols='50' rows='7' ></textarea></td>
   </tr>
   <tr>
    <td>*Base Salary:</td>
    <td>
     </select>
     <select name='salary' id='salary'>
      <option value='-1'>[Select]</option>
	   <option value='1'>1.7 million won</option>
	   <option value='2'>1.8 million won</option>
	   <option value='3'>1.9 million won</option>
	   <option value='4'>2.0 million won</option>
	   <option value='5'>2.1 million won</option>
	   <option value='6'>2.2 million won</option>
	   <option value='7'>2.3 million won</option>
	   <option value='8'>2.4 million won</option>
	   <option value='9'>2.5 million won</option>
	   <option value='10'>2.6 million won</option>
	   <option value='11'>2.7 million won</option>
	   <option value='12'>2.8 million won</option>
	  </select>
  	 </td>
  	</tr>
  	<tr>
  	 <td>*Teachers needed</td>
  	 <td>
  	  <select name="number_teachers">
		<option value="Number not selected" >
		<option selected value='1'>1</option>
		<option value='2'>2</option>
		<option value='3'>3</option>
		<option value='4'>4</option>
		<option value='5'>5</option>
		<option value='6'>6</option>
		<option value='7'>7</option>
		<option value='8'>8</option>
		<option value='9'>9</option>
		<option value='10'>10</option>
	  </select>
	 </td>
  	</tr>
	<tr>
	 <td>Teacher Preference</td>
	 <td>
	   <select name="gender">
		 <option value='Any' >Any</option>
	    <option value='1' >Male</option>
	 	 <option value='2' >Female</option>
		</select>
	 </td>
	</tr>
	<tr>
    <td colspan='2' align='center'>
     <input type="reset" value="Clear Application">&nbsp;&nbsp;
     <input type="submit" value="Send Application">
    </td>
   </tr>
   </table>
  <input type="hidden" name="MAX_FILE_SIZE" value="355000">
  </form>
<!-- END RECRUITER_FORM -->

<!-- BEGIN RECRUITER_FORM_THANKS -->
  <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',15000); </SCRIPT>
  <p class='style5' align='center'>THANK YOU </p>

  <p class='style5' align='justify'>Your job has been successfully added to our database. 
                                    <br> We will be in touch as soon as we have found a suitable candidate. 
                                    </p>
  
  <p class='style5' align='justify'>If you have any further questions pls contact us and we'll do our best to respond in
  a timely manner.</p>
  <br />
 
  <p class='style5' align='justify'>This window should close in 15 secs ...</p>
  <br />
  
  <p>&nbsp;</p>

<!-- END RECRUITER_FORM_THANKS -->

<!-- BEGIN RECRUITER_FORM_ERROR -->
     <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',10000); </SCRIPT>
     
  <p class='style5' align='center'>There has been an error uploading your job to our database.<br> <br>
  								            Please contact our <a href='/form.php?mode=contact_us_form'>administrator</a>
  								            <br>Thank You</p>
  
  <p class='style5' align='center'>This window should close in 10 secs ...</p>
  <br />
  
   <p>&nbsp;</p>

<!-- END RECRUITER_FORM_ERROR -->

</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
