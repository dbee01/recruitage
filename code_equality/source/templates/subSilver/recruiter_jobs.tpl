<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

</head>


<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

   <p class='style5' align='center'>JOB GALLERY </p>

<table bgcolor='#FFFFCC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
  <tr>
    <td width='15%'>*Contact:</td>
    <td>{RECRUITER_CONTACT}</td>
  </tr>
  <tr>
    <td>*School:</td>
    <td>{RECRUITER_SCHOOL}</td>
  </tr>
  <tr>
    <td>*JOB START</td>
    <td>{RECRUITER_MONTHTIME}&nbsp;&nbsp;{RECRUITER_MONTH}</td>
  </tr>
   <tr>
    <td>School Description:</td>
    <td>{RECRUITER_SCHOOL_DESCRIPTION}</td>
  </tr>
  <tr>
    <td>*City:</td>
    <td>{RECRUITER_CITY}</td>
  </tr>
  <tr>
   <td>*Website Address:</td>
	<td>{RECRUITER_WEBSITE_ADDRESS}</td>
  </tr>
  <tr>
    <td>*Students:</td>
    <td>{RECRUITER_KINDERGARTEN},&nbsp;{RECRUITER_ELEMENTARY},&nbsp;{RECRUITER_MIDDLE_SCHOOL},&nbsp;{RECRUITER_HIGH_SCHOOL},&nbsp;{RECRUITER_ADULTS}</td>
  </tr>
  <tr>
    <td>*Contract:</td>
    <td><a href="{RECURITER_CONTRACT}">Contract Link</a></td>
  </tr>
  <tr>
    <td>*Pic_Upload:</td>
    <td><img src="./uploads/{RECURITER_PICUPLOAD}" align="absmiddle" alt="" height="" width="" /></td>
  </tr>
  <tr>
    <td>*Ticket_Money:</td>
    <td>{RECURITER_TICKET_MONEY}</td>
  </tr>
  <tr>
    <td>*Teacher Description:</td>
    <td>{RECRUITER_TEACHER_DESCRIPTION}</td>
  </tr>
  <tr>
    <td>*Salary:</td>
    <td>{RECRUITER_SALARY}</td>
  </tr>
  <tr>
    <td>*Gender:</td>
    <td>{RECRUITER_GENDER}</td>
  </tr>
  <tr>
    <td>*Number Teachers:</td>
    <td>{RECRUITER_NUMBER_TEACHERS}</td>
  </tr>
  </table>
</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
