<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{RECRUITER_NAME}</title>

<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />

<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />

<!-- main calendar program -->
<script type="text/javascript" src="calendar/calendar.js"></script>

<!-- language for the calendar -->
<script type="text/javascript" src="calendar/calendar-en.js"></script>

<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript" src="calendar/calendar-setup.js"></script>

<script type="text/javascript">
// <![CDATA[

function validate()
{
	if(
		( document.getElementById('show_form').teacher_response_form.value != "1" ) 
		||
		( document.getElementById('show_form').teacher_response_form.value != "0" ) 
		) { 
	  alert('Please indicate yes or no on the form...');
	  return false;
	}
}

function display_form() {
	
	if(document.getElementById('form_status').innerHTML == 'SHOW FORM')
	{
		document.getElementById('show_form').style.display = 'block';	
		document.getElementById('form_status').innerHTML = 'HIDE FORM';
	} 
	else
	{
		document.getElementById('show_form').style.display = 'none';	
		document.getElementById('form_status').innerHTML = 'SHOW FORM';
	}
	
}
// ]]>
</script>
</head>


<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<!-- BEGIN TEACHER_RESPONSE_FORM -->

<table cellpadding='3' cellspacing='3' border='0' width='100%' >
<form name='teacher_response_form' action="control_panel.php" method="post" 
        enctype="application/x-www-form-urlencoded" onSubmit="return validate();" >

 <tr>
 	<td align='center' colspan='3' ><a id='form_status' href="javascript:display_form();" >HIDE FORM</a></td></tr>
  		 <input type='hidden' name='teacher_name' value='{RECRUITER_TEACHER_NAME}' />
  		 <input type='hidden' name='teacher_id' value='{RECRUITER_TEACHER_ID}' />
  		 <input type='hidden' name='recruiter_jobs_id' value='{RECRUITER_JOBS_ID}' />
 	</td>
 </tr>
 <tr>
 <td align='center'>
  <table cellspacing='3' cellpadding='3' id='show_form'>
  <tr>
   <td align='left'>YES, I'm interested, pls arrange an interview with the school. </td>
   <td align='left'><input name='teacher_response_form' value='1' type='radio' /> </td>
  </tr>
  <tr>
    <td align='left'>NO, I'd like to see more teaching schools please.</td>
    <td align='left'><input name='teacher_response_form' value='0' type='radio' /></td>
  </tr>
  <tr>
    <td colspan='1' align='left' valign='top'>
      Pls use this box to pass along any questions that you <br /> may have for this teacher, or any information that you would like to pass along.
    </td>
    <td><textarea name='teacher_notes' rows='6' cols='30' ></textarea></td>
  </tr>
  <tr><td align='center' colspan='2'><input type='reset' name='reset' value='Reset' /><input type='submit' name='submit' value='Submit' /></td></tr>
 </table>
 </td>
 </tr> 
 </form>
</table>
<br />
<br />

<!-- END TEACHER_RESPONSE_FORM -->

<p class='style5' align='center'><a href="" onclick="window.open('{RECRUITER_PRINT_FORM}','','scrollbars=yes,menubar=no, resizable=yes,toolbar=no,location=no,status=no');">{RECRUITER_NAME}</a></p>

<table bgcolor='#FFFFCC' width='100%' align='center' border='1' cellspacing='5' cellpadding='5'>

<!-- BEGIN job1_form -->

<!-- BEGIN school -->
   <tr>
    <td>{job1_form.school.L_SCHOOL}</td>
    <td>{job1_form.school.RECRUITER_NAME}</td>
   </tr>
<!-- END school -->

<!-- BEGIN contact -->
   <tr>
    <td>{job1_form.contact.L_CONTACT}</td>
    <td>{job1_form.contact.RECRUITER_CONTACT}'</td>
   </tr>
<!-- END contact -->

<!-- BEGIN salary -->
   <tr>
    <td>{job1_form.salary.L_SALARY}</td>
    <td>{job1_form.salary.SALARY}</td>
   </tr>
<!-- END salary -->

<!-- BEGIN arrival -->
   <tr>
    <td>{job1_form.arrival.L_ARRIVAL}</td>
    <td>{job1_form.arrival.ARRIVAL}</td>
  </tr>
<!-- END arrival -->

<!-- BEGIN email -->
   <tr>
    <td>{job1_form.email.L_EMAIL}</td>
    <td><a href='/form.php?candidate_email_form={job1_form.email.RECRUITER_EMAIL}&name={job1_form.email.RECRUITER_FIRST_NAME}'>Email</a></td>
   </tr>
<!-- END email -->
 
<!-- BEGIN location -->
  <tr>
   <td>{job1_form.location.L_LOCATION}</td>
   <td> {job1_form.location.LOCATION}</td>
  </tr>
<!-- END location -->

<!-- BEGIN school_description -->
   <tr>
    <td>{job1_form.school_description.L_SCHOOL_DESCRIPTION}</td>
    <td>{job1_form.school_description.RECRUITER_DESCRIPTION}</td>
   </tr>
<!-- END school_description -->

<!-- BEGIN interest_select -->

  <tr>  <td>{job1_form.interest_select.INTEREST_SELECTION}</td>
	<td>

	<form action='/control_panel.php?' method='POST' >

		<input type='hidden' name='recruiter_response_form' value='1' />
		<input type='hidden' name='recruiter_jobs_id' value='{RECRUITER_JOBS_ID}' />
		<input type='hidden' name='from_recruiter_gallery' />

		<select name='teacher_id' > 
		<option value='-1' >Interested In</option>

<!-- BEGIN teacher_interest_select -->

		<option  value='{job1_form.interest_select.teacher_interest_select.TEACHER_ID}'
		onclick='form.submit()'>
			{job1_form.interest_select.teacher_interest_select.TEACHER_NAME}
		</option>

<!-- END teacher_interest_select -->

		</select>

	</form>

	</td>

  </tr>

  <tr>

   <td>{job1_form.interest_select.L_BOOK_CALL}</td>

   <td>

	<form action='/control_panel.php?' method='POST' id='third_party_call_form' >

	<input type='hidden' name='user_reg_date' values='{USER_REG_DATE}' />
	<input type='hidden' name='recruiter_jobs_id' value='{RECRUITER_JOBS_ID}' />

	<select name='teacher_id' > 
	<option value='-1' >Interested In</option>

<!-- BEGIN teacher_interest_select -->

	<option  value='{job1_form.interest_select.teacher_interest_select.TEACHER_ID}'>
			{job1_form.interest_select.teacher_interest_select.TEACHER_NAME}
	</option>

<!-- END teacher_interest_select -->

	</select>


     <div class="book-call" id="calendar-container"></div>

        <script type="text/javascript">

	  function dateChanged(calendar) {
    		// Beware that this function is called even if the end-user only
    		// changed the month/year.  In order to determine if a date was
   	        // clicked you can use the dateClicked property of the calendar:
    		if (calendar.dateClicked) {
      		// OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
    		  var y = calendar.date.getFullYear();
      		  var m = calendar.date.getMonth();     // integer, 0..11
      		  var d = calendar.date.getDate();      // integer, 1..31
      		// redirect...
     		 document.getElementById('reminder_date').value = m  + "," + d + "," + y ; 
    	  	}
  	  };

	  Calendar.setup(
    		  	{
     	 		flat         : "calendar-container", // ID of the parent element
      			flatCallback : dateChanged,           // our callback function
			showsTime    : true								
    	  		}
  		);

  	  function check_date()
  		{
		if(document.getElementById('reminder_date').value == "" )
		{
			alert("Please enter a date ...");
			return false;
		}

  	  }	

  	</script>

        </div>

	<input type="button" name="book_call" value="book_call" 
               onclick="third_party_call_book();
	                recruiter_notes_update('recruiter_notes',
						document.getElementById('teacher_name').value,
						'-1',
						document.getElementById('recruiter_jobs_id').value,
						document.getElementById('reminder_date').value,
						'Call booked between val1 and val2 for date/time' )" 
        /> 

        </form>

   </td>
  </tr>


<!-- END interest_select -->


<!-- BEGIN recruiter_notes -->

    <tr>

      <td>{job1_form.recruiter_notes.L_RECRUITER_NOTES}</td>

      <td class="recruiter_notes_box" >

	<div class="recruiter_notes_text" >
		{job1_form.recruiter_notes.RECRUITER_NOTES}<br />
	</div>

        <form action="recruiter_gallery.php" method="post" enctype="application/x-www-form-urlencoded">
	  <input type='text' name='recruiter_notes' />
          <input type='submit' name='submit' value='submit' /><input type='hidden' name='recruiter_jobs_id' value='{RECRUITER_JOBS_ID}' /> 
        </form>

      </td>

    </tr>

<!-- END recruiter_notes -->
 
<!-- BEGIN applicant_description -->
  <tr>
    <td>{job1_form.applicant_description.L_APPLICANT_DESCRIPTION}</td>
    <td>{job1_form.applicant_description.RECRUITER_FOREIGN_TEACHER}</td>
  </tr>
<!-- END applicant_description -->

<!-- BEGIN website_address -->
  <tr>
    <td>{job1_form.website_address.L_WEBSITE_ADDRESS}</td>
    <td>{job1_form.website_address.RECRUITER_WEBSITE}</td>
  </tr>
<!-- END website_address -->


<!-- BEGIN accomodation -->
  <tr>
    <td>{job1_form.accomodation.L_ACCOMODATION}</td>
    <td>{job1_form.accomodation.ACCOMODATION}</td>
  </tr>
<!-- END accomodation -->

<!-- BEGIN teacher_description -->
   <tr>
    <td>{job1_form.teacher_description.L_TEACHER_DESCRIPTION}</td>
    <td>{job1_form.teacher_description.TEACHER_DESCRIPTION}</td>
   </tr>
<!-- END teacher_description -->

<!-- BEGIN contract -->
   <tr>
    <td>{job1_form.contract.L_CONTRACT}</td>
    <td><a href="uploads/{job1_form.contract.RECRUITER_CONTRACT}>Contract</a></td>
   </tr>
<!-- END contract -->

<!-- BEGIN num_teachers -->
  	<tr>
  	 <td>{job1_form.num_teachers.L_NUM_TEACHERS}</td>
  	 <td>{job1_form.num_teachers.NUM_TEACHERS}</td>
  	</tr>
<!-- END num_teachers -->

<!-- BEGIN gender -->
	<tr>
	 <td>{job1_form.gender.L_GENDER}</td>
	 <td>{job1_form.gender.GENDER}</td>
	</tr>
<!-- END gender -->

<!-- BEGIN students -->
   <tr>
    <td>{job1_form.students.L_STUDENTS}</td>
    <td>{job1_form.students.KINDERGARTEN}&nbsp;
	{job1_form.students.ELEMENTARY}&nbsp;
	{job1_form.students.MIDDLE_SCHOOL}&nbsp;
	{job1_form.students.HIGH_SCHOOL}&nbsp;
	{job1_form.students.ADULTS}
     </td>
  </tr>
 
<!-- END students -->

<!-- BEGIN picture -->
  <tr>
    <td>{job1_form.picture.L_PICTURE}</td>
    <td><img src='{job1_form.picture.RECRUITER_PICUPLOAD}' /></td>
  </tr>
<!-- END picture -->

<!-- END job1_form -->

  </table>
</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
