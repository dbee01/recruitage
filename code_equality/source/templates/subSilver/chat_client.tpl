<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<head></head>

<body>

  <h>{CHAT_TITLE}</h>
  <div id="chatwindow" style="width:450px;height:180px;overflow-y:scroll;border:1px solid #aaaaaa; padding:10px;white-space:pre;" >

  </div>

  <br />

  <label style='font-size:0.8em;' >Message:
     <input id="chatmsg" type="text" size="36" style="border:1px solid #aaaaaa;" onkeyup="keyup(event.keyCode);">
  </label> 

  <input type="button" value="ok" onclick="submit_msg()" style="cursor:pointer;border:1px solid gray;">

  {CLEAR_TEXT}

  <!-- These have to be down here or the DOM won't parse properly -->
  <script src="js/common.js" type="text/javascript"></script>
  <script src="js/chat.js" type="text/javascript"></script>

</body>
</htm>
