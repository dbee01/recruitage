<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>EnglishTeachingKorea</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />


function display_form() {
	
	if(document.getElementById('form_status').innerHTML == 'SHOW FORM')
	{
		document.getElementById('show_form').style.display = 'block';	
		document.getElementById('form_status').innerHTML = 'HIDE FORM';
	} 
	else
	{
		document.getElementById('show_form').style.display = 'none';	
		document.getElementById('form_status').innerHTML = 'SHOW FORM';
	}
	
}
// ]]>

</script>

</head>


<body>
<div class='container'>
<div class='titleblock'>
<h1 class='style9' align='right'>EnglishTeachingKorea.com </h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<p class='style5' align='center'><a href="" onclick="window.open('{SCHOOL_PRINT_FORM}','','scrollbars=yes,menubar=no, resizable=yes,toolbar=no,location=no,status=no');">{SCHOOL_NAME}</a></p>

<form action='form.php?mode=admin_form' enctype='multipart/form-data' method='post' name='admin_form'>
 <table bgcolor='#FFFFCC' width='100%' align='center' border='1' cellspacing='5' cellpadding='5'>
    <tr>
    <td width='15%' >School Name:</td>
    <td>{SCHOOL_NAME}</td>
  </tr>
  <tr>
    <td >School Contact:</td>
    <td>{SCHOOL_CONTACT}</td>
  </tr>
  <!-- BEGIN ADMIN_LOG_IN -->  
  <tr>
    <td>School Address:</td>
    <td>{SCHOOL_POSTAL_ADDRESS}</td>
  </tr>
   <tr>
    <td>Email:</td>
    <td><a href='/form.php?candidate_email_form={SCHOOL_EMAIL}&name={SCHOOL_FIRST_NAME}'>{SCHOOL_EMAIL}</a></td>
   </tr>
   <tr>
    <td>Phone:</td>
    <td>{SCHOOL_PHONE}</td>
  </tr>
  </tr>
 <form action="school_gallery.php" method="post" enctype="application/x-www-form-urlencoded">
  <tr><td>Recruiter Notes:</td>
      <td><textarea name="recruiter_notes" cols="80" rows="10" >{SCHOOL_RECRUITER_NOTES}</textarea><br />
     <input type='submit' name='submit' value='submit' /><input type='hidden' name='school_jobs_id' value='{SCHOOL_JOBS_ID}' /> </td>
    </tr>
   </form>
<!-- END ADMIN_LOG_IN -->  
  <tr>
    <td>Salary:</td>
    <td>{SCHOOL_SALARY}</td>
  </tr>
    <tr>
    <td>City:</td>
    <td>{SCHOOL_CITY}</td>
  </tr>
  <tr>
    <td>Foreign Teacher Contact:</td>
    <td>{SCHOOL_FOREIGN_TEACHER}</td>
  </tr>
  <tr>
    <td>School Website:</td>
    <td>&nbsp;{SCHOOL_WEBSITE}</td>
  </tr>
  <tr>
    <td>School Description:</td>
    <td>{SCHOOL_DESCRIPTION}</td>
  </tr>
   <tr>
    <td>Students:</td>
    <td>{SCHOOL_KINDERGARTEN}&nbsp;{SCHOOL_ELEMENTARY}&nbsp;{SCHOOL_MIDDLE_SCHOOL}&nbsp;{SCHOOL_HIGH_SCHOOL}&nbsp;{SCHOOL_ADULTS}</td>
  </tr>
  <tr>
    <td>Start Date:</td>
    <td>{SCHOOL_ARRIVAL}</td>
  </tr>
  <tr>
    <td>Contract:</td>
    <td><a href='/uploads/{SCHOOL_CONTRACT}'>{SCHOOL_CONTRACT}</a></td>
  </tr>
  <tr>
    <td>Pics:</td>
    <td><img src='/uploads/{SCHOOL_PICUPLOAD}' /></td>
  </tr>
  <tr>
    <td>Number of teachers:</td>
    <td>{SCHOOL_NUMBER_TEACHERS}</td>
  </tr>
  </table>
</form>
</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 EnglishTeachingKorea.com </p>

<p>&nbsp;</p>

</div>

</div>

</div>

 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-1";
  urchinTracker();
 </script>

</body>
</html>
