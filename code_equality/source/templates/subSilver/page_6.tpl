<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<title>English Teaching Korea Korean Life </title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" >
<link rel="stylesheet" href="css_page/second.css" type="text/css" />

<!--[if IE]> <link rel="stylesheet" href="css_page/secondIE.css" type="text/css" > <![endif]-->
<!--[if IE 7 ]> <link rel="stylesheet" href="css_page/secondIE7.css" type="text/css" > <![endif]-->

<script type='text/javascript' src='js/callme_now.js' ></script>

<script type='text/javascript'>
// place all the on-page variables here....
	var valid = {CALLBACK_VALID};
    
</script>

</head>
<body>

<div id="pagewidth" >

  <div id="header" > 
 
    <div class="navbar"> 
        <ul id="nav">
            <li id="nav_home"><a href="{PAGE_1_LINK}">{PAGE_1_NAME}</a></li>
            <li id="nav_jobs"><a href="{PAGE_2_LINK}">{PAGE_2_NAME}</a></li>
            <li id="nav_korea"><a href="{PAGE_3_LINK}">{PAGE_3_NAME}</a></li>
            <li id="nav_visa"><a href="{PAGE_4_LINK}">{PAGE_4_NAME}</a></li>
            <li id="nav_links"><a href="{PAGE_5_LINK}">{PAGE_5_NAME}</a></li>
            <li id="nav_contact"><a href="{CONTACT_LINK}">{PAGE_5_NAME}</a></li>
        </ul>
    </div>

    <div class="logo">&nbsp;</div>

    <div id="top_bar">
     &nbsp;
    </div>

  </div>

  <div id="wrapper" class="clearfix" > 

  <div id="maincol" >

   <p class='style5' align='center'>{TEACHER_FORM_HEADING}</p>
   <p class='style5' align='justify'></p>

   <br />

   <form action='form.php' method='post' enctype="application/x-www-form-urlencoded">
     <table bgcolor='#FFFFDC' width='80%' align='center' border='1' cellspacing='5' cellpadding='5'>
       <tr>
        <td width='50%'>{TEACHER_FORM_NAME}</td>
        <td><input type='text' name='email_name' size='20' maxlength='20' /></td>
       </tr>
       <tr>
        <td width='50%'>{TEACHER_FORM_EMAIL}</td>
        <td><input type='text' name='email' size='30' maxlength='30' /></td>
       </tr>
       <tr>
        <td width='50%'>{TEACHER_FORM_SUBJECT}</td>
        <td><input type='text' name='subject' size='30' maxlength='30' />
        <input type='hidden' name='contact_us' value='contact_us' /></td>
       </tr>
       <tr>
        <td width='50%'>{TEACHER_FORM_TXT}</td>
        <td><textarea name='text' name='text' cols='50' rows='10'></textarea></td>
       </tr>
       <tr>
        <td colspan='2' align='center' >
	 <input type='reset' name='reset' value='{RESET}' />
	 <input type='submit' name='submit' value='{SUBMIT}' /> 
        </td>
       </tr>
     </table>
    </form>
    <br />
    <br />
  </div>

  <div id="leftcol">

   <div id="top_image">&nbsp;</div>

   <div id="dave_contact" >
      <div id="dave_contact_pic" ><img src='images/david-contact.jpg' alt='David' /></div>
      <div id="dave_contact_text" >{TEACHER_FORM_TEXT}</div>
   </div>

   <div id="bottom_image" >&nbsp;</div>

  </div>

 </div>

  <div id="footer" >
    <div class="bottom_bar">
      <div class="contact">
       <span><a class="contact_flag" href='login.php'><img src="{CONTACT_FLAG}" alt="country_flag" /></a></span>    
	 <div class="contact_details" >
	   <a href='page_6.php' >{L_EMAIL}</a><br />
           <a href='https://myaccount.voipbuster.com/images/callmenowbutton.gif' onclick='callMeNow(); return false' >{L_PHONE}</a>
	 </div>
      </div>

      <div class='copyright' >
        <p class='style14'> {SITENAME} &copy; 2005 </p>
      </div>

    </div>
  </div>

</div>
 
 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-3";
  urchinTracker();
 </script>

</body>

</html>
