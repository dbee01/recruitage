<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<title>{SITENAME} Visa</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" >
<link rel="stylesheet" href="css_page/second.css" type="text/css" />

<!--[if IE]> <link rel="stylesheet" href="css_page/secondIE.css" type="text/css" > <![endif]-->
<!--[if IE 7 ]> <link rel="stylesheet" href="css_page/secondIE7.css" type="text/css" > <![endif]-->

<script type='text/javascript' src='js/callme_now.js' ></script>

<script type='text/javascript'>
// place all the on-page variables here....
	var valid = {CALLBACK_VALID};
    
</script>

</head>
<body>

<div id="pagewidth" >

  <div id="header" > 
     
    <div class="navbar"> 
        <ul id="nav">
            <li id="nav_home"><a href="{PAGE_1_LINK}">{PAGE_1_NAME}</a></li>
            <li id="nav_jobs"><a href="{PAGE_2_LINK}">{PAGE_2_NAME}</a></li>
            <li id="nav_korea"><a href="{PAGE_3_LINK}">{PAGE_3_NAME}</a></li>
            <li id="nav_visa"><a href="{PAGE_4_LINK}">{PAGE_4_NAME}</a></li>
            <li id="nav_links"><a href="{PAGE_5_LINK}">{PAGE_5_NAME}</a></li>
            <li id="nav_contact"><a href="{CONTACT_LINK}">{PAGE_5_NAME}</a></li>
        </ul>
    </div>
    
    <div class="logo">&nbsp;</div>

    <div id="top_bar">&nbsp;</div>

  </div>

  <div id="wrapper" class="clearfix" > 

  <div id="maincol" >
    <div id="visa_pic">&nbsp;</div>
    <div id="main_text" >
    <h3>{L_VISA_CONTENT_TITLE}</h3>
     <p>{L_VISA_PARAGRAPH}</p>
    </div>
  </div>

  <div id="leftcol" >
   <table align='center'>
    <tr><th>Application Process:<br /><br /></th></tr>
    <tr>
     <td align='center'>
      <img src="images/visa.gif" alt="visa diagram" />
     </td>
    </tr>
   </table>
  </div>

 </div>

  <div id="footer" >
    <div class="bottom_bar">
      <div class="contact">
       <span><a class="contact_flag" href='login.php'><img src="{CONTACT_FLAG}" alt="country_flag" /></a></span>    
	 <div class="contact_details" >
	   <a href='page_6.php' >{L_EMAIL}</a><br />
           <a href='https://myaccount.voipbuster.com/images/callmenowbutton.gif' onclick='callMeNow(); return false' >{L_PHONE}</a>
	 </div>
      </div>

      <div class='copyright' >
        <p class='style14'> {SITENAME} &copy; 2005 </p>
      </div>

    </div>
  </div>

</div>
 
 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-3";
  urchinTracker();
 </script>

</body>

</html>
