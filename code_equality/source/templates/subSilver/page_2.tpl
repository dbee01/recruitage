<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<title>{SITENAME} Jobs</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" >
<link rel="stylesheet" href="css_page/second.css" type="text/css" >

<!--[if IE]> <link rel="stylesheet" href="css_page/secondIE.css" type="text/css" > <![endif]-->
<!--[if IE 7 ]> <link rel="stylesheet" href="css_page/secondIE7.css" type="text/css" > <![endif]-->


<script type='text/javascript' src='js/calculator.js' ></script>
<script type='text/javascript' src='js/site_tracker.js' ></script>
<script type='text/javascript' src='js/callme_now.js' ></script>

<script type='text/javascript'>
// place all the on-page variables here....
	var valid = {CALLBACK_VALID};
    
</script>


<script type='text/javascript'>

// onpage site tracker stuff
	// pick the top 20 sites...
<!-- BEGIN site_tracker -->
	{site_tracker.SITE_TRACK}
<!-- END site_tracker -->

// onpage calculator stuff
function x_change(){

 if ( !(document.getElementById('balance')) ){
	return;
 }
 else {
	amount = document.getElementById('balance').innerHTML;
 }

 if ( !(document.getElementById('currency')) ){ return; }
 else { currency = document.getElementById('currency').value; }

 var usd = {USD};
 var gbp = {GBP};
 var aud = {AUD};
 var can = {CAN};
 var kwn = {KWN};
 var nzd = {NZD};
 var zar = {ZAR};

 var eur_value = amount / kwn ;

 switch(currency)
 {
   case 'usd' : amount = eur_value * usd  ; break ;
   case 'gbp' : amount = eur_value * gbp  ; break ;
   case 'aud' : amount = eur_value * aud  ; break ;
   case 'can' : amount = eur_value * can  ; break ;
   case 'kwn' : amount = eur_value * kwn  ; break ;
   case 'nzd' : amount = eur_value * nzd  ; break ;
   case 'zar' : amount = eur_value * zar  ; break ;
   case 'eir' : amount = eur_value  ; break ;
   default: break ; 
 }

 amount=Math.floor(amount);

 document.getElementById('currency_amount').innerHTML = amount ;
	
}
</script>
</head>
<body>

<div id="pagewidth" >

  <div id="header" > 

     <div class="navbar"> 
        <ul id="nav">
            <li id="nav_home"><a href="{PAGE_1_LINK}">{PAGE_1_NAME}</a></li>
            <li id="nav_jobs"><a href="{PAGE_2_LINK}">{PAGE_2_NAME}</a></li>
            <li id="nav_korea"><a href="{PAGE_3_LINK}">{PAGE_3_NAME}</a></li>
            <li id="nav_visa"><a href="{PAGE_4_LINK}">{PAGE_4_NAME}</a></li>
            <li id="nav_links"><a href="{PAGE_5_LINK}">{PAGE_5_NAME}</a></li>
            <li id="nav_contact"><a href="{CONTACT_LINK}">{PAGE_5_NAME}</a></li>
        </ul>
    </div>

    <div class="logo">&nbsp;</div>
        
    <div id="top_bar">
     &nbsp;
    </div>

  </div>

  <div id="wrapper" class="clearfix" > 

  <div id="maincol" >
    <div id="jobs_pic"></div>
    <div id="main_text" >
      <h3>{L_JOB_CONTENT_TITLE}</h3>
      <p>{L_JOB_PARAGRAPH}</p>
      <p><span style='font-size:0.8em'>* Pick your earning bracket from the drop-down menu to the left. Likely monthly costs are entered for you. Press on the corresponding link, to toggle optional monthly costs. Finally, convert your monthly take-home pay after costs into your own currency by selecting your currency from the drop-down menu.</span></p>
      <p><span style='font-size:0.8em'>** Holiday costs include flight, hotel and spending money from an English speaking travel agent in Korea.</span></p>
      <p><span style='font-size:0.8em'>*** Currency conversions are today's rates from the European Central Bank.</span></p>
    </div>
  </div>

  <div id="leftcol" > <br />
    <form action='post' ><div><input type='hidden' id="salary_hidden" /></div>

       <div id="top_image" >&nbsp;</div>

       <div id="calculator" > 
     	<table>
      	  <tr>
	    <th>Salary</th>
	    <td><select id="salary" onChange="calculate('none');" >
		 <option value="1800000" >1.8 million</option>
		 <option value="1900000" >1.9 million</option>
		 <option value="2000000">2.0 million</option>
		 <option value="2100000">2.1 million</option>
		 <option value="2200000">2.2 million</option>
		 <option value="2300000">2.3 million</option>
		 <option value="2400000">2.4 million</option>
		 <option value="2500000">2.5 million</option>
		</select>
	    </td>
	  </tr>
        </table>

       </div>

       <div id="bottom_image" >&nbsp;</div>

   </form>
  </div>

 </div>

  <div id="footer" >
    <div class="bottom_bar">
      <div class="contact">
       <span><a class="contact_flag" href='login.php'><img src="{CONTACT_FLAG}" alt="country_flag" /></a></span>    
	 <div class="contact_details" >
	   <a href='page_6.php' >{L_EMAIL}</a><br />
           <a href='https://myaccount.voipbuster.com/images/callmenowbutton.gif' onclick='callMeNow(); return false' >{L_PHONE}</a>
	 </div>
      </div>

      <div class='copyright' >
        <p class='style14'> {SITENAME} &copy; 2005 </p>
      </div>

    </div>
  </div>


</div>
<script type='text/javascript' src='js/site_tracker.js' ></script>

 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-3";
  urchinTracker();
 </script>

</body>
</html>
