<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<script language="javascript" src="js/email_entry.js"></script>


<!-- skype link maker -->
<script type="text/javascript">

function link_maker(number)
{

	alert('number is' . number);		
   number = 'skype:+' . number ;

   document.getElementById('phone_link').href=number;
}

</script> 

 <!-- calendar stylesheet -->
 <link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />

 <!-- main calendar program -->
 <script type="text/javascript" src="calendar/calendar.js"></script>

 <!-- language for the calendar -->
 <script type="text/javascript" src="calendar/calendar-en.js"></script>

 <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
 <script type="text/javascript" src="calendar/calendar-setup.js"></script>

 <script type="text/javascript">
// <![CDATA[

function display(obj) {

   var max_teachers = 30;   
	txt = obj.options[obj.selectedIndex].value;
	
	for( j=2; j<= max_teachers ; j++ )
	{
		document.getElementById(j).style.display = 'none';	
	}

	for( i=2; i <= txt; i++ )
	{

			document.getElementById(i).style.display = 'block';	

	}

}
// ]]>
</script>
</head>


<body>
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>EnglishTeachingKorea.com </h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<!-- BEGIN JOB_CALENDAR_FORM -->

<p class='style5' align='center'>{JOB_CALENDAR_TITLE}</p>

<p class='style5' align='center'>{JOB_CALENDAR_DESCRIPTION}</p>
  
<form action='job_calendar.php' method='post' enctype="application/x-www-form-urlencoded">
<table bgcolor='#FFFFCC' width='100%' align='center' border='0' cellspacing='5' cellpadding='5'>
 <tr>
 <td>
  <table width=80%' align='center'>
  <tr>
    <td width='30%'>*{JOB_CALENDAR_SCHOOL}:</td>
    <td><input type='text' name='school' /></td>
  </tr>
  <tr>
    <td >*{JOB_CALENDAR_CONTACT}:</td>
    <td><input type='text' name='contact' /></td>
  </tr>
   <tr>
    <td >*{JOB_CALENDAR_EMAIL}:</td>
    <td><input type='text' name='email' /></td>
  </tr>
    <tr>
    <td>Organization:</td>
    <td>
	<select name='organization'>
	 <option selected='Selected' value='-1'>{SELECT}</option>
	  {ORGANIZATION_BLOCK}
        </select>
    </td>
  </tr>
  <tr>
   <td>*{JOB_CALENDAR_NUMBER_TEACHERS}:</td>
   <td>
    <select id='number_teachers'  name='number_teachers' onchange="display(this);">
     <option>{SELECT}:</option>
     <option value=1>1</option>
     <option value=2>2</option>
     <option value=3>3</option>
     <option value=4>4</option> 
     <option value=5>5</option> 
     <option value=6>6</option> 
     <option value=7>7</option> 
     <option value=8>8</option> 
     <option value=9>9</option> 
     <option value=10>10</option> 
     <option value=11>11</option> 
     <option value=12>12</option> 
     <option value=13>13</option> 
     <option value=14>14</option> 
     <option value=15>15</option> 
     <option value=16>16</option> 
     <option value=17>17</option> 
     <option value=18>18</option> 
     <option value=19>19</option> 
     <option value=20>20</option> 
     <option value=21>21</option> 
     <option value=22>22</option> 
     <option value=23>23</option> 
     <option value=24>24</option> 
     <option value=25>25</option> 
     <option value=26>26</option> 
     <option value=27>27</option> 
     <option value=28>28</option> 
     <option value=29>29</option> 
     <option value=30>30</option> 
    </select>
  </td>
  </tr>
    <tr id=1 style="display: block;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 1</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_1' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_1" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
 </tr>
  <tr id=2 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 2</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_2' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_2" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
 </tr> 	
 <tr id=3 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 3</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_3' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td>
 	      <td><input type="text" name="leaving_date_3" id="f_date_c" readonly="1" />
 	          <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"	onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
  </td>
 </tr> 	
 <tr id=4 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 4</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_4' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_4" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 	
   <tr id=5 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 5</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_5' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_5" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
  <tr id=6 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 6</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_6' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_6" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=7 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 7</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_7' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_7" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=8 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 8</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_8' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_8" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=9 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 9</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_9' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_9" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=10 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 10</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_10' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_10" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=11 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 11</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_11' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_11" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=12 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 12</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_12' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_12" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=13 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 13</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_13' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_13" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
 <tr id=14 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 14</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_14' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_14" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=15 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 15</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_15' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_15" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=16 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 16</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_16' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_16" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=17 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 17</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_17' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_17" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=18 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 18</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_18' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_18" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=19 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 19</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_19' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_19" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=20 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 20</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_20' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_20" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=21 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 21</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_21' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_21" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=22 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 22</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_22' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_22" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=23 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 23</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_23' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_23" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=24 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 24</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_24' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_24" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=25 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 25</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_25' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_25" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=26 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 26</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_26' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_26" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=27 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 27</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_27' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_27" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=28 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 28</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_28' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_28" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=29 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 29</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_29' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_29" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=30 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 30</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_30' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_30" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  <tr>
    <td colspan='2' align='center' ><input type='reset' name='reset' value='Reset' /><input type='submit' name='submit' value='Send' /> </td>
  </tr>
</table>
 </td>
 </tr>
</table>	
</form>
<!-- END J0B_CALENDAR_FORM -->

<!-- BEGIN JOB_CALENDAR_EMAIL_FORM -->

<p class='style5' align='center'>{JOB_CALENDAR_EMAIL_TITLE}</p>

 <p class='style5' align='center'>{JOB_CALENDAR_EMAIL_DESCRIPTION}</p> 
  
<form action='job_calendar.php' method='post' enctype="application/x-www-form-urlencoded" name='email_entry' onSubmit='return validate();' >
<table bgcolor='#FFFFCC' width='100%' align='center' border='0' cellspacing='5' cellpadding='5'>
  <tr>
    <td width='30%'>*{JOB_CALENDAR_SCHOOL}:</td>
    <td><input type='text' name='school' /></td>
  </tr>
  <tr>
    <td >*{JOB_CALENDAR_CONTACT}:</td>
    <td><input type='text' name='contact' /></td>
	<input type='hidden' name='email_campaign' >
  </tr>
   <tr>
    <td >*{JOB_CALENDAR_EMAIL}:</td>
    <td><input type='text' name='email' /></td>
  </tr>
  <tr>
    <td>*{JOB_CALENDAR_PHONE}:</td>
    <td>
	<input type='text' maxlength='45' id='phone' name='phone'>
    </td>
  </tr>
  <tr>
   <td>*{JOB_CALENDAR_SCHOOL_DESCRIPTION}:<br /><br />
	{JOB_CALENDAR_SCHOOL_DESCRIPTION_TEXT}
    </td>
    <td><textarea name='school_description' cols='50' rows='7' ></textarea></td>
  </tr>
  <tr>
    <td>{JOB_CALENDAR_CITY}</td>
    <td>
    <select name='city' id='city'>
	{LOCATION_BLOCK}
	  </select>
	 </td>
  </tr>
  <tr>
    <td>*{JOB_CALENDAR_BASE_SALARY}:</td>
    <td>
     </select>
     <select name='salary' id='salary'>
	{SALARY_BLOCK}
	  </select>
  	 </td>
  </tr>
  <tr>
    <td>{JOB_CALENDAR_ORGANIZATION}:</td>
    <td>
	<select name='organization'>
	 <option value='-1'>{SELECT}</option>
	{ORGANIZATION_BLOCK}
         </select>
    </td>
  </tr>
  <tr>
   <td>*{JOB_CALENDAR_NUMBER_TEACHERS}:</td>
   <td>
    <select id='number_teachers'  name='number_teachers' onchange="display(this);">
     <option>Please select:</option>
     <option value=1>1</option>
     <option value=2>2</option>
     <option value=3>3</option>
     <option value=4>4</option> 
     <option value=5>5</option> 
     <option value=6>6</option> 
     <option value=7>7</option> 
     <option value=8>8</option> 
     <option value=9>9</option> 
     <option value=10>10</option> 
     <option value=11>11</option> 
     <option value=12>12</option> 
     <option value=13>13</option> 
     <option value=14>14</option> 
     <option value=15>15</option> 
     <option value=16>16</option> 
     <option value=17>17</option> 
     <option value=18>18</option> 
     <option value=19>19</option> 
     <option value=20>20</option> 
     <option value=21>21</option> 
     <option value=22>22</option> 
     <option value=23>23</option> 
     <option value=24>24</option> 
     <option value=25>25</option> 
     <option value=26>26</option> 
     <option value=27>27</option> 
     <option value=28>28</option> 
     <option value=29>29</option> 
     <option value=30>30</option> 
    </select>
  </td>
  </tr>
    <tr id=1 style="display: block;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 1</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_1' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_1" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
 </tr>
  <tr id=2 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 2</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_2' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_2" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
 </tr> 	
 <tr id=3 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 3</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_3' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td>
 	      <td><input type="text" name="leaving_date_3" id="f_date_c" readonly="1" />
 	          <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"	onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
  </td>
 </tr> 	
 <tr id=4 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 4</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_4' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_4" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 	
   <tr id=5 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 5</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_5' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_5" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
  <tr id=6 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 6</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_6' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_6" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=7 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 7</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_7' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_7" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=8 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 8</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_8' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_8" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=9 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 9</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_9' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_9" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=10 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 10</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_10' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_10" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=11 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 11</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_11' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_11" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=12 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 12</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_12' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_12" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=13 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 13</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_13' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_13" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
 <tr id=14 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 14</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_14' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_14" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=15 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 15</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_15' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_15" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=16 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 16</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_16' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_16" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=17 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 17</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_17' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_17" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=18 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 18</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_18' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_18" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=19 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 19</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_19' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_19" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=20 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 20</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_20' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_20" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=21 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 21</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_21' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_21" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=22 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 22</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_22' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_22" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=23 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 23</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_23' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_23" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=24 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 24</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_24' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_24" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=25 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 25</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_25' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_25" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=26 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 26</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_26' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_26" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=27 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 27</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_27' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_27" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=28 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 28</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_28' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_28" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=29 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 29</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_29' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_29" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  </tr> 
   <tr id=30 style="display: none;">
   <td colspan='2'>
    <table cellspacing='5' cellpadding='5' width='100%' align='center'>
 	  <tr><th colspan='2'>{JOB_CALENDAR_TEACHER_NUMBER} 30</th></tr>
     <tr><td>&nbsp;&nbsp;*{JOB_CALENDAR_TEACHER_NAME}:</td><td><input type='text' name='leaving_name_30' /></td></tr>
 	  <tr><td>&nbsp;&nbsp;{JOB_CALENDAR_TEACHER_LEAVING}:</td><td><input type="text" name="leaving_date_30" id="f_date_c" readonly="1" />
 	           <img src="images/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid red;" title="Date selector"
   				onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />

		<script type="text/javascript">
		    Calendar.setup({
		        inputField     :    "f_date_c",     // id of the input field
		        ifFormat       :    "%Y-%m-%d",      // format of the input field
		        button         :    "f_trigger_c",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
	  	</script> 
      </td>
     </tr>
    </table>
   </td>
  <tr>
    <td colspan='2' align='center' ><input type='reset' name='reset' value='Reset' /><input type='submit' name='submit' value='Send' /> </td>
  </tr>
</table>
	
</form>
<!-- END JOB_CALENDAR_EMAIL_FORM -->

<!-- BEGIN JOB_CALENDAR_THANK_YOU -->

  <SCRIPT LANGUAGE='JavaScript'> setTimeout('self.close()',5000); </SCRIPT>

  <p class='style5' align='center'>THANK YOU </p>

  <p class='style5' align='justify'>We will email you at the optimum time to start your teacher search. </p>

  <br />
 
  <p class='style5' align='justify'>This window should close in 5 secs ...</p>
  <br />
  
  <p>&nbsp;</p>

<!-- END JOB_CALENDAR_THANK_YOU -->


</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 EnglishTeachingKorea.com </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
