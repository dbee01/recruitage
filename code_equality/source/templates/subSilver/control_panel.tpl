<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>{SITENAME} - Control_Panel.php</title>

<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
<script language='Javascript' type='text/javascript' src='js/common.js' ></script>

<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<!-- define some on page variable stuff here -->
<script language='Javascript' type='text/javascript' >
	setInterval("sndTimer()",{SCREEN_REFRESH_RATE});
        user = '{USER_REGDATE}' ;
	timezone='{USER_TIMEZONE}';	
</script> 

</head>

<body onload="sndTimer();" >
<div class='container'>
<div class='titleblock'>

<div id="logo">&nbsp;</div>
 
<h1 class='style9' align='right'>{SITENAME}</h1>
<p class='style1 style13'>{USERNAME} helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<div id="tabs">

  <ul>
  <li id='introTab' style='{INTRO_TAB_SELECTED}' ><a href="#" onclick="tab_view('intro'); return false;" >intro</a></li>
  <li id='teacherTab' style='{TEACHERS_TAB_SELECTED}' ><a href="#" onclick="tab_view('teachers'); return false;" >teachers</a></li>
  <li id='jobsTab' style='{JOBS_TAB_SELECTED}' ><a href="#" onclick="tab_view('jobs'); return false;" > jobs</a></li>       
  <li id='matchesTab' style='{LEADS_TAB_SELECTED}' ><a href="#" onclick="tab_view('matches'); return false;" >leads</a></li>
  <li id='allTab' style='{ALL_TAB_SELECTED}' ><a href="#" onclick="tab_view('all'); return false;" >all</a></li>
 </ul>

<table><tr><td>&nbsp;</td></tr></table>

<div id='reminder_box'  style="{REMINDERS_DISPLAY}" class='main_box' >
 <table width='100%'  >
  <thead>
  	{REMINDERS_HEAD}
  </thead>
  <tbody id='reminders_box' class='table_color' >
	<tr class="info_table" >
           <td width="20%" >&nbsp;</td><td width="60%" >&nbsp;</td><td width="10%" >&nbsp;</td><td width="10%" >&nbsp;</td>
        </tr>
	<tr class="info_table" >
           <td width="20%" >&nbsp;</td><td width="60%" >&nbsp;</td><td width="10%" >&nbsp;</td><td width="10%" >&nbsp;</td>
        </tr>
	<tr class="info_table" >
           <td width="20%" >&nbsp;</td><td width="60%" >&nbsp;</td><td width="10%" >&nbsp;</td><td width="10%" >&nbsp;</td>
        </tr>
  </tbody>
  </table>
</div>

<div id='email_box' style="{EMAILS_DISPLAY}" class='main_box' >
 <table width="100%" class='info_title'>
  <thead>
	{EMAILS_HEAD}
  </thead>
  <tbody id='email_data_box' class='info_table table_color'  >

  </body>
 </table>
</div>

<div id='teachers_box' style="{TEACHERS_DISPLAY}" class='main_box' > 
 <table class='info_title' >
  <thead>
	{TEACHERS_HEAD}
  </thead>
  <tbody id='teachers_data_box' class='info_table table_color'  >

  </body>
 </table>
</div>

<div id="schools_box" style="{SCHOOLS_DISPLAY}" class='main_box' >
 <table class='info_title' >
  <thead>
	{SCHOOLS_HEAD}	
  </thead>
  <tbody id='schools_data_box' class='info_table table_color'  >

  </body>
 </table>
</div>

<div id="recruiter_box" style="{RECRUITERS_DISPLAY}" class='main_box' >	
 <table class='info_title' >
  <thead>
	{RECRUITERS_HEAD}	
  </thead>
  <tbody id='recruiters_data_box' class='info_table table_color'  >

  </body>
 </table>
</div>

<div id='matches_box' style="{MATCHES_DISPLAY}" class='main_box' >  
 <table class='info_title' >
  <thead>
	{MATCHES_HEAD}
  </thead>
  <tbody id='matches_data_box' class='info_table table_color'  >

  </body>
 </table>
</div>

<div id='leads_box' style="{LEADS_DISPLAY}" class="main_box" >
 <table class='info_title' class='main_box' >
  <thead>
	{LEADS_HEAD}	
  </thead>
  <tbody id='leads_data_box' class='info_table table_color'  >

  </body>
 </table>
</div>


</div>
</blockquote>
<p>

</p>


<div class='footer'>
<div class='right style11'>
<p class='style14'>Copyright&copy;2005 {SITENAME} </p>

<p>&nbsp;</p>

</div>

</div>

</div>
</div>

 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>

 <script type="text/javascript">
  _uacct = "UA-1567415-3";
  urchinTracker();
 </script>

</body>
</html>
