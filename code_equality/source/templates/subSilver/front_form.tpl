<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<title>Links Page - {SITENAME}</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" >
<link rel="stylesheet" href="css_page/front_form.css" type="text/css" >

<!--[if IE]> <link rel="stylesheet" href="css_page/front_formIE.css" type="text/css" > <![endif]-->
<!--[if IE 7 ]> <link rel="stylesheet" href="css_page/front_formIE7.css" type="text/css" > <![endif]-->

</head>
<body>

<div id="pagewidth" >

  <div id="header" > 

    <div class="logo">&nbsp;</div>
 
    <div class="navbar"> 
        <ul id="nav">
            <li id="nav_home"><a href="{PAGE_1_LINK}">{PAGE_1_NAME}</a></li>
            <li id="nav_jobs"><a href="{PAGE_2_LINK}">{PAGE_2_NAME}</a></li>
            <li id="nav_korea"><a href="{PAGE_3_LINK}">{PAGE_3_NAME}</a></li>
            <li id="nav_visa"><a href="{PAGE_4_LINK}">{PAGE_4_NAME}</a></li>
            <li id="nav_links"><a href="{PAGE_5_LINK}">{PAGE_5_NAME}</a></li>
            <li id="nav_contact"><a href="{CONTACT_LINK}">{PAGE_5_NAME}</a></li>
        </ul>
    </div>
    
    <div id="top_bar">&nbsp;</div>

  </div>

  <div id="wrapper" class="clearfix" > 

  <div id="maincol" >

<!-- BEGIN testamonials_page -->

   <div class='testamonials'>

    <div id="links_pic1">
	<img src='{TESTAMONIALS1_PIC}' alt='testamonials pic1' >
    </div>

    <div class="main_text" >
          <div class='title'><h2>{L_TESTAMONIALS1_TITLE}</h2></div>
	  {L_TESTAMONIALS1_TEXT}
    </div>

  </div>

  <div class="testamonials">

    <div id="links_pic2"><img alt='testamonial pic2' src='{TESTAMONIALS2_PIC}' ></div>

    <div class="main_text" >
          <div class='title'><h2>{L_TESTAMONIALS2_TITLE}</h2></div>
	  {L_TESTAMONIALS2_TEXT}
    </div>

 </div>

<!-- END testamonials_page -->

<!-- BEGIN jobs_news1 -->

   <div id='testamonials'>
    <div class="main_text" >
          <div class='title'><h2>{L_JOBS_NEWS_TITLE1}</h2></div>
	  {L_JOBS_NEWS_TEXT1}
    </div>
  </div>

<!-- END jobs_news1 -->

<!-- BEGIN jobs_news2 -->

  <div id="testamonials">
    <div class="main_text" >
          <div class='title'><h2>{L_JOBS_NEWS_TITLE2}</h2></div>
	  {L_JOBS_NEWS_TEXT2}
    </div>
 </div>

<!-- END jobs_news2 -->

<!-- BEGIN jobs_news3 -->

  <div id="testamonials">
    <div class="main_text" >
          <div class='title'><h2>{L_JOBS_NEWS_TITLE3}</h2></div>
	  {L_JOBS_NEWS_TEXT3}
    </div>
 </div>

<!-- END jobs_news3 -->

    <div id="leftcol" >
	    <div class="links_pic">&nbsp;</div>
    </div>

  </div>

  <div id="footer" >
    <div class="bottom_bar">

      <div class="contact">

       <span>
             <a class="contact_flag" href='control_panel.php'>
               <img alt='flag' src="{CONTACT_FLAG}" >
             </a>
       </span>    
	     
       <div class="contact_details">{CONTACT_EMAIL}<br >{CONTACT_PHONE}</div>

      </div>

    </div>
  </div>

 </div>
</div>
</body>

</html>
