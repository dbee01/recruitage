<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<title>{SITENAME}</title>
<link rel='stylesheet' type='text/css' href='css_page/form_style.css' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<script LANGUAGE="javascript" src="js/js_school.js"></script>
<link rel='stylesheet' type='text/css' href='css_page/chromestyle.css' />
<script type='text/css'>
.optional{
color:red;
font-size:8px;
}
</script>

 <!-- calendar stylesheet -->
 <link rel="stylesheet" type="text/css" media="all" href="calendar/calendar-win2k-cold-1.css" title="win2k-cold-1" />

 <!-- main calendar program -->

 <script type="text/javascript" src="calendar/calendar.js"></script>

 <!-- language for the calendar -->
 <script type="text/javascript" src="calendar/calendar-en.js"></script>

 <!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
 <script type="text/javascript" src="calendar/calendar-setup.js"></script>

</head>


<body>

<div class='container'>

<div class='titleblock'>

<div id="logo">&nbsp;</div>

<h1 class='style9' align='right'>EnglishTeachingKorea.com </h1>
<p class='style1 style13'>Teachers helping teachers </p>

</div>

{NAVBAR}

<div class='content'>
<blockquote>

<form action='form.php?mode=teacher_print_form' enctype='multipart/form-data' method='post' name='employee_entry' onSubmit='return validate();'>
<table bgcolor='#FFFFCC' width='100%' align='center' border='1' cellspacing='5' cellpadding='5'>

<!-- BEGIN can0_form -->

<!-- BEGIN name -->
  <tr>
    <td width='50%'>{can0_form.name.L_NAME}</td>
    <td><input type='text' maxlength='30' name='name' value='{can0_form.name.TEACHER_NAME}' ></td>
  </tr>
<!-- END name -->

<!-- BEGIN birthday -->
  <tr>
    <td>{can0_form.birthday.L_BIRTHDAY}</td>
    <td>
    <select  name='birthyear' id='birthyear'>

	<option value='-1'>[Select]</option>
        <option {TEACHER_BIRTHDAY_SELECTED_88} value='88'>1987</option>
        <option {TEACHER_BIRTHDAY_SELECTED_87} value='87'>1986</option>
        <option {TEACHER_BIRTHDAY_SELECTED_86} value='86'>1985</option>
        <option {TEACHER_BIRTHDAY_SELECTED_85} value='85'>1984</option>
        <option {TEACHER_BIRTHDAY_SELECTED_84} value='84'>1983</option>
        <option {TEACHER_BIRTHDAY_SELECTED_83} value='83'>1982</option>
        <option {TEACHER_BIRTHDAY_SELECTED_82} value='82'>1981</option>      
        <option {TEACHER_BIRTHDAY_SELECTED_81} value='81'>1980</option>
        <option {TEACHER_BIRTHDAY_SELECTED_80} value='80'>1979</option>
        <option {TEACHER_BIRTHDAY_SELECTED_79} value='79'>1978</option>
        <option {TEACHER_BIRTHDAY_SELECTED_78} value='78'>1977</option>
        <option {TEACHER_BIRTHDAY_SELECTED_77} value='77'>1976</option>
        <option {TEACHER_BIRTHDAY_SELECTED_76} value='76'>1975</option>
        <option {TEACHER_BIRTHDAY_SELECTED_75} value='75'>1974</option>
        <option {TEACHER_BIRTHDAY_SELECTED_74} value='74'>1973</option>
        <option {TEACHER_BIRTHDAY_SELECTED_73} value='73'>1972</option>
        <option {TEACHER_BIRTHDAY_SELECTED_72} value='72'>1971</option>
        <option {TEACHER_BIRTHDAY_SELECTED_71} value='71'>1970</option>
        <option {TEACHER_BIRTHDAY_SELECTED_70} value='70'>1969</option>
        <option {TEACHER_BIRTHDAY_SELECTED_69} value='69'>1968</option>
        <option {TEACHER_BIRTHDAY_SELECTED_68} value='68'>1967</option>
        <option {TEACHER_BIRTHDAY_SELECTED_67} value='67'>1966</option>
        <option {TEACHER_BIRTHDAY_SELECTED_66} value='66'>1965</option>
        <option {TEACHER_BIRTHDAY_SELECTED_65} value='65'>1964</option>
        <option {TEACHER_BIRTHDAY_SELECTED_64} value='64'>1963</option>
        <option {TEACHER_BIRTHDAY_SELECTED_63} value='63'>1962</option>
        <option {TEACHER_BIRTHDAY_SELECTED_62} value='62'>1961</option>
        <option {TEACHER_BIRTHDAY_SELECTED_61} value='61'>1960</option>
        <option {TEACHER_BIRTHDAY_SELECTED_60} value='60'>1959</option>
        <option {TEACHER_BIRTHDAY_SELECTED_59} value='59'>1958</option>
        <option {TEACHER_BIRTHDAY_SELECTED_58} value='58'>1957</option>
        <option {TEACHER_BIRTHDAY_SELECTED_57} value='57'>1956</option>
        <option {TEACHER_BIRTHDAY_SELECTED_56} value='56'>1955</option>
        <option {TEACHER_BIRTHDAY_SELECTED_55} value='55'>1954</option>
        <option {TEACHER_BIRTHDAY_SELECTED_54} value='54'>1953</option>
        <option {TEACHER_BIRTHDAY_SELECTED_53} value='53'>1952</option>
        <option {TEACHER_BIRTHDAY_SELECTED_52} value='52'>1951</option>
        <option {TEACHER_BIRTHDAY_SELECTED_51} value='51'>1950</option>
        <option {TEACHER_BIRTHDAY_SELECTED_50} value='50'>1949</option>
        <option {TEACHER_BIRTHDAY_SELECTED_49} value='49'>1948</option>
        <option {TEACHER_BIRTHDAY_SELECTED_48} value='48'>1947</option>
        <option {TEACHER_BIRTHDAY_SELECTED_47} value='47'>1946</option>
        <option {TEACHER_BIRTHDAY_SELECTED_46} value='46'>1945</option>
        <option {TEACHER_BIRTHDAY_SELECTED_45} value='45'>1944</option>
        <option {TEACHER_BIRTHDAY_SELECTED_44} value='44'>1943</option>
        <option {TEACHER_BIRTHDAY_SELECTED_43} value='43'>1942</option>
        <option {TEACHER_BIRTHDAY_SELECTED_42} value='42'>1941</option>
        <option {TEACHER_BIRTHDAY_SELECTED_41} value='41'>1940</option>
        <option {TEACHER_BIRTHDAY_SELECTED_40} value='40'>1939</option>
        <option {TEACHER_BIRTHDAY_SELECTED_39} value='39'>1938</option>
        <option {TEACHER_BIRTHDAY_SELECTED_38} value='38'>1937</option>
        <option {TEACHER_BIRTHDAY_SELECTED_37} value='37'>1936</option>
        <option {TEACHER_BIRTHDAY_SELECTED_36} value='36'>1935</option>
        <option {TEACHER_BIRTHDAY_SELECTED_35} value='35'>1934</option>
        <option {TEACHER_BIRTHDAY_SELECTED_34} value='34'>1933</option>
        <option {TEACHER_BIRTHDAY_SELECTED_33} value='33'>1932</option>
        <option {TEACHER_BIRTHDAY_SELECTED_32} value='32'>1931</option>
        <option {TEACHER_BIRTHDAY_SELECTED_31} value='31'>1930</option>
        <option {TEACHER_BIRTHDAY_SELECTED_30} value='30'>1929</option>
        <option {TEACHER_BIRTHDAY_SELECTED_29} value='29'>1928</option>
        <option {TEACHER_BIRTHDAY_SELECTED_28} value='28'>1927</option>
        <option {TEACHER_BIRTHDAY_SELECTED_27} value='27'>1926</option>
        <option {TEACHER_BIRTHDAY_SELECTED_26} value='26'>1925</option>
        <option {TEACHER_BIRTHDAY_SELECTED_25} value='25'>1924</option>
        <option {TEACHER_BIRTHDAY_SELECTED_24} value='24'>1923</option>
        <option {TEACHER_BIRTHDAY_SELECTED_23} value='23'>1922</option>
        <option {TEACHER_BIRTHDAY_SELECTED_22} value='22'>1921</option>
        <option {TEACHER_BIRTHDAY_SELECTED_21}value='21'>1920</option>
        <option {TEACHER_BIRTHDAY_SELECTED_20} value='20'>1919</option>
        <option {TEACHER_BIRTHDAY_SELECTED_19} value='19'>1918</option>
        <option {TEACHER_BIRTHDAY_SELECTED_18} value='18'>1917</option>
        <option {TEACHER_BIRTHDAY_SELECTED_17} value='17'>1916</option>
        <option {TEACHER_BIRTHDAY_SELECTED_16} value='16'>1915</option>
        <option {TEACHER_BIRTHDAY_SELECTED_15} value='15'>1914</option>
        <option {TEACHER_BIRTHDAY_SELECTED_14} value='14'>1913</option>
        <option {TEACHER_BIRTHDAY_SELECTED_13} value='13'>1912</option>
        <option {TEACHER_BIRTHDAY_SELECTED_12} value='12'>1911</option>
        <option {TEACHER_BIRTHDAY_SELECTED_11} value='11'>1910</option>
        <option {TEACHER_BIRTHDAY_SELECTED_10} value='10'>1909</option>
	<option {TEACHER_BIRTHDAY_SELECTED_9} value='9'>1908</option>
	<option {TEACHER_BIRTHDAY_SELECTED_8} value='8'>1907</option>
	<option {TEACHER_BIRTHDAY_SELECTED_7} value='7'>1906</option>
	<option {TEACHER_BIRTHDAY_SELECTED_6} value='6'>1905</option>
	 <option {TEACHER_BIRTHDAY_SELECTED_5} value='5'>1904</option>
    </select>

	</td>
  </tr>
<!-- END birthday -->

<!-- BEGIN nationality -->
  <tr>
    <td>{can0_form.nationality.L_NATIONALITY}</td>
    <td><select id='Nationality'  name='nationality'>
	{NATIONALITY_BLOCK}
	 </select>
	 </td>
  </tr>
<!-- END nationality -->

<!-- BEGIN email -->
  <tr {can0_form.email.TEACHER_EMAIL_VISIBLE} >
    <td>{can0_form.email.L_EMAIL}</td>
    <td><input type='text' maxlength='45'  name='email' value='{can0_form.email.TEACHER_EMAIL}' ></td>
  </tr>
<!-- END email -->

<!-- BEGIN telephone -->
  <tr>
    <td>{can0_form.telephone.L_PHONE}</td>
    <td><select name='country_code' id='country_code'>
	{COUNTRY_CODES_BLOCK}	
 	  </select>&nbsp;&nbsp;
	<input type='text' maxlength='45' name='phone' value='{can0_form.telephone.TEACHER_PHONE}'></td>
  </tr>
  <tr>
    <td>{can0_form.telephone.L_FROM_TIME} </td>
    <td><label>{can0_form.telephone.L_BETWEEN}&nbsp;&nbsp;&nbsp;&nbsp;
    		<select name='from_time' id='from_time'>
			{FROM_TIME_BLOCK}
	 	  </select></label>&nbsp;&nbsp;<label>{can0_form.telephone.L_AND}
	 	  <select name='until_time' id='until_time'>
			{UNTIL_TIME_BLOCK}
	 	  </select></label>&nbsp;&nbsp;<br><label>{can0_form.telephone.L_TIMEZONE}
    	  <select name='timezone' id='timezone'>
	 		<option value='0'>{can0_form.telephone.L_SELECT}</option>
			{TELEPHONE_TIMEZONE_BLOCK}
	 	  </select></label>
	 </td>
   </tr>
<!-- END telephone -->

<!-- BEGIN location -->
   <tr>
    <td>{can0_form.location.L_LOCATION}</td>
    <td>
     <select id='location' name='location' >
	{LOCATION_BLOCK}
     </select>
    </td>
  </tr>
<!-- END location -->

<!-- BEGIN start_date -->
   <tr>
    <td>{can0_form.start_date.L_START_DATE}</td>
    <td>
       <div style="margin-left: 1em; margin-bottom: 1em;" id="calendar-container"></div>

<script type="text/javascript" >
	
  function dateChanged(calendar) {
    // Beware that this function is called even if the end-user only
    // changed the month/year.  In order to determine if a date was
    // clicked you can use the dateClicked property of the calendar:
    if (calendar.dateClicked) {
      // OK, a date was clicked, redirect to /yyyy/mm/dd/index.php
      var y = calendar.date.getFullYear();
      var m = calendar.date.getMonth();     // integer, 0..11
      var d = calendar.date.getDate();      // integer, 1..31
      // redirect...
      document.getElementById('fixed_date1').value = y  + "-" + m + "-" + d ; 
    }
  };

  Calendar.setup(
    {
      flat         : "calendar-container", // ID of the parent element
      flatCallback : dateChanged,           // our callback function
      date : '{can0_form.start_date.TEACHER_START_DATE}'
    }
  );

  function check_date()
  {
	if(document.getElementById('fixed_date1').value == "" )
	{
		alert("Please enter a date ...");
		return false;
	}

  }

		</script>	
		<input type='hidden' id="fixed_date1" name='fixed_date1' value='' />
   </td>
  </tr>

<!-- END start_date -->

<!-- BEGIN arrival -->

  <tr>
    <td>{can0_form.arrival.L_ARRIVAL}
	*The visa process takes around ONE MONTH.
         Pls state the earliest date you can have your documents ready to send to Korea before you come over.<br>
         For more info pls click<a href='http://www.englishteachingkorea.com/visa.php'> here</a>... :</td>
    <td>
    
         <select name='arrival' id='arrival'>
	{ARRIVAL_BLOCK}
        </select>

    </td>
  </tr>
<!-- END arrival -->

<!-- BEGIN gender -->
  <tr>
    <td>{can0_form.gender.L_GENDER}</td>
    <td> <select name='gender' id='gender'>
	 <option value='-1'>[Select]</option>

	 <option {TEACHER_GENDER_SELECTED_1} value='1'>Male</option>
	 <option {TEACHER_GENDER_SELECTED_2} value='2'>Female</option>
	 </select>
	 </td>
  </tr>
<!-- END gender -->

<!-- BEGIN inkorea -->
  <tr>
    <td>{can0_form.inkorea.L_INKOREA}</td>

    <td> <select name='inkorea' id='inkorea'>
	 <option value='-1'>{can0_form.inkorea.L_SELECT}</option>
	 <option {TEACHER_INKOREA_SELECTED_1} value='1'>In Korea</option>
	 <option {TEACHER_INKOREA_SELECTED_2} value='2'>Not in Korea</option>
    </select>                       
	</td>

  </tr>
<!-- END inkorea -->

<!-- BEGIN education -->
  <tr>
    <td>{can0_form.education.L_EDUCATION}</td>
    <td><textarea name='university_education' cols='50' rows='7' >{can0_form.education.TEACHER_UNIVERSITY_EDUCATION}</textarea></td>
  </tr>
<!-- END education -->

<!-- BEGIN experience -->
  <tr>
    <td>{can0_form.experience.L_EXPERIENCE}</td>

    <td><textarea name='experience' cols='50' rows='7' >{can0_form.experience.TEACHER_EXPERIENCE}</textarea></td>
  </tr>
<!-- END experience -->

<!-- BEGIN introduction -->
  <tr>
    <td>{can0_form.introduction.L_INTRODUCTION}</td>
    <td><textarea name='introduction' cols='50' rows='7' >{can0_form.introduction.TEACHER_INTRODUCTION}</textarea></td>
  </tr>
<!-- END introduction -->
<!-- BEGIN candidate_picture -->
   <tr>
    <td >{can0_form.candidate_picture.UPLOAD0_TITLE}<input type='hidden' name='MAX_FILE_SIZE' value='1000000' /></td>
	<td><input type='file' name='picupload' value="{can0_form.candidate_picture.TEACHER_PIC_UPLOAD}" ></td>
  </tr>
<!-- END candidate_picture -->

  <tr> <input type='hidden' name='print_form'><input type='hidden' name='teacher_id' value='{TEACHER_ID}'>
    <td colspan=2 align='center'><input type='reset' name='Reset'>&nbsp;&nbsp;<input type='submit' name='Submit' value='Submit Form'></td>
  </tr>

<!-- END can0_form -->

  </table>
 </form>
  
  <p>&nbsp;</p>

</blockquote>
<p>

</p>

</div>

<div class='footer'>
<div class='right style11'>
<p class='style14'>&copy; 2005 EnglishTeachingKorea.com </p>

<p>&nbsp;</p>

</div>

</div>

</div>

</body>
</html>
