<?php

@extract($_POST);

header("Location: /demo.html");

# set up general write to check permissions
$filename = 'xmlresults.xml';
$xmlopener = "<?xml version=\"1.0\"?>\n";

if (!$fp = fopen($filename, 'a+')) {
  print "Cannot open file ($filename)";
  exit;
}

$xmlFileContents = '';

// xml dtd
$xmlOpener = "<?xml version=\"1.0\"?>\n"; 

// document root
$openDocRoot = "<recruitage>\n";
$closeDocRoot = "</recruitage>\n";

// build the xml file
$xmlFileContents .= $xmlOpener ;
$xmlFileContents .= $openDocRoot ;

// take out the submit value ...
unset($_POST['submit']);

foreach ( $_POST as $name => $value )
{

  $name = htmlentities($name,ENT_QUOTES);
  $value = htmlentities($value,ENT_QUOTES);
  
  $xmlFileContents .= "<$name>$value</$name>\n";

}

$xmlFileContents .= $closeDocRoot ;

if (!fwrite($fp, $xmlFileContents)) {
  print "Cannot write to file ($filename)";
  exit;
}

//remembering to close the file.
fclose($fp);

echo 'Thanks';

?>